<?php
include '../includes/application_top.php';
include '../Operations/reports_save.php';
include 'functions.php';
//include the class until I can find what is causing the problem with abstract classes not loading
include '../includes/classes/income.php';

/**
 * @return array
 */
function setDate()
{
// Allow from accountant download
    if (isset($_POST['year']) && isset($_POST['month'])) {
        $_GET['date'] = sprintf("%04d-%02d", $_POST['year'], $_POST['month']);
    }
    $current_time = time();
    $d = date('Y-m', $current_time);

    if (isset($_GET['date'])) {
        $current_time = mktime(0, 0, 0, substr($_GET['date'], 5, 2), 15, substr($_GET['date'], 0, 4));
    } else {
        $current_time = mktime(0, 0, 0, substr($d, 5, 2), 15, substr($d, 0, 4));
    }

    $d = date('Y-m', $current_time);

    return array($current_time, $d);
}

/**
 * @param $d
 * @param $config
 *
 * @return mixed
 */
function setTourismBoardPaymentInConfig($d, $config)
{
    /*
     * Our contract with Ryujin is changing as of April 1st.  At present, we pay 500 yen to the Tourism Board.  As of April
     * 1st, that will change to 1,000 yen.  Please make the required changes making sure that previous months' numbers are
     * not affected.
     */
    if (SYSTEM_SUBDOMAIN_REAL == 'RYUJIN') {
        $currentMonth = DateTime::createFromFormat("Y-m", $d);
        $priceChangeMonth = DateTime::createFromFormat("Y-m-d", "2015-04-01");

        if ($currentMonth < $priceChangeMonth) $config['tb_payment'] = 500;

    }

    return $config;
}

/**
 * @return array
 */
function getConfigForPOST()
{
    global $config;
    if (isset($_POST['site_id'])) {
        $site_id = $_POST['site_id'];
        $_POST['action'] = 'CSV';
        // load site config
        $config = BJHelper::getConfiguration($site_id);

    }

    return $config;
}

/**
 * @param $site_id
 *
 * @return string
 */
function getSubdomain($site_id)
{
    $places = BJHelper::getPlaces();
    $subdomain = 'unknown';
    foreach ($places as $place) {
        if ($place['id'] == $site_id) {
            $subdomain = $place['subdomain'];
        }
    }

    return $subdomain;
}

/**
 * @param $site_id
 * @param $d
 * @param $showSql
 * @param $config
 *
 * @return array
 */
function getAllOnsiteRates($site_id, $d, $showSql, $config)
{
//SELECTING present OnSite Rates
//Select all of the rates for a particular month by grabbing all of the unique rates for each registered jump.
//This will stay up to date even if the rates change.
//These are stored in onsite_rates();
    $sql = "
SELECT distinct Rate 
FROM customerregs1
WHERE site_id = '" . $site_id . "' AND BookingDate LIKE '$d-%' AND NoOfJump > 0 AND DeleteStatus = 0 AND Checked = 1 AND CollectPay = 'Onsite'
ORDER BY Rate DESC;
";

    if ($showSql) echo $sql . "<br>";
    $onsite_rates = array();
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        if (!$row['Rate']) continue;
        $onsite_rates[] = $row['Rate'];
    }
    mysql_free_result($res);
    if (!in_array($config['first_jump_rate'], $onsite_rates)) {
        $onsite_rates[] = $config['first_jump_rate'];
        rsort($onsite_rates);
    }
// add secondjump rate to reates if not there
    if (!in_array($config['second_jump_rate'], $onsite_rates)) {
        $onsite_rates[] = $config['second_jump_rate'];
        rsort($onsite_rates);
    }
    $onsite_rates = array_filter($onsite_rates);

    return $onsite_rates;//strip out null values
}

/**
 * @param $site_id
 * @param $d
 * @param $showSql
 * @param $config
 *
 * @return array
 */
function getAllOffsiteRates($site_id, $d, $showSql, $config)
{
// SELECTING present OffSite Rates
// Same as above but for some reason does not add second jump rate
    $sql = "
SELECT distinct Rate 
FROM customerregs1
WHERE site_id = '" . $site_id . "' AND BookingDate like '$d-%' and NoOfJump > 0 and DeleteStatus = 0 and Checked = 1 and CollectPay = 'Offsite'
ORDER BY Rate DESC;
";
    if ($showSql) echo $sql . "<br>";

    $offsite_rates = array();
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        if ($row['Rate'] == 0) continue;
        $offsite_rates[] = $row['Rate'];
    }
    mysql_free_result($res);
    if (empty($offsite_rates)) {
        $offsite_rates[] = $config['first_jump_rate'];

        return $offsite_rates;
    }

    return $offsite_rates;
}

/**
 * @param $site_id
 * @param $d
 * @param $showSql
 *
 * @return array
 */
function getTShirtPrices($site_id, $d, $showSql)
{
// get current t-shirt prices
    $sql = "SELECT DISTINCT msi.item_price FROM merchandise_items mi, merchandise_sales ms, merchandise_sales_items msi
WHERE 
mi.name LIKE 'Shirt%'
AND mi.id = msi.item_id
AND msi.sales_id = ms.id
AND ms.site_id = '" . $site_id . "'
AND ms.sale_time LIKE '$d-%';";

    if ($showSql) echo $sql . "<br>";

    $res = mysql_query($sql) or die(mysql_error());
    $tprices = array();
    while ($row = mysql_fetch_assoc($res)) {
        $tprices[] = $row['item_price'];
    }
    $sql = "SELECT DISTINCT (tshirt / tshirt_qty) as tshirt_price 
FROM customerregs1 
WHERE site_id = '" . $site_id . "' AND BookingDate like '$d-%' and NoOfJump > 0 and DeleteStatus = 0 and Checked = 1 and tshirt_qty > 0";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        $tprices[] = $row['tshirt_price'];
    }
    $tprices = array_unique($tprices);
    $tprices = array_filter($tprices);

    return $tprices;
}

/************************At this point we finish getting prices and we start generating fields to store data ***********/
/**
 * @param $tprices
 * @param $current_time
 * @param $onsite_rates
 * @param $offsite_rates
 *
 * @return array
 */
function createIncomeSheetDataStructure($tprices, $current_time, $onsite_rates, $offsite_rates)
{
//convert each price to a string... (Unnecessary)
    $tprices = array_map(function ($item) {
        return sprintf("%d", $item);
    }, $tprices);
// EOF tshirt prices
    $month_days = date('t', $current_time);

    $fields = array_map(function ($item) {
        return 'a' . $item;
    }, $onsite_rates);
//add a to each onsite rate so it is a field

    $fields = array_merge($fields, array(
        'a_photo',
        'a_cancel',
        //'a_topay',
        'a_total',
    ));
    foreach ($tprices as $tprice) {
        //$fields[] = 'b' . $tprice;
    }

    $fields[] = 'b_tshirt_total_qty';
    $fields[] = 'b_tshirt_total';
    $fields = array_merge($fields, array(
        'b_other',
        'b_other_total',
        'b_total'
    ));
    $fields = array_merge($fields, array_map(function ($item) {
        return 'c' . $item;
    }, $offsite_rates));
    $fields = array_merge($fields, array(
        'c_cancel',
        'c_total',
        'ab_total',
        /*  'paying_jump_no',
            'max_yen_jumpers',*/
        'insurance_no',
        'tboard_day_total',
        'a_topay',//to pay agent, this is money for other activities that we collect money for and pass on to agent. Eg someone pays for a rafting and bungy package
        'b_topay',//to pay bungy
        'p_topay',//photo payment for the 9000, 8000, 7000 yen jumpers
        //'tboard_weekly',
        'daily_banked',
        'insurance_no_total'
    ));

//not necessary?

    $d = date('Y-m', $current_time);

    return array($tprices, $month_days, $fields, $d);

    /*
    //we have calculated
    tprices
    ( [0] => 2000 [1] => 2500 )

    offsite rates
    ( [0] => 7500 [1] => 7000 [2] => 6500 [3] => 5000 [4] => 4000 )

    onsite rates
    ( [0] => 7500 [1] => 7000 [2] => 6500 [3] => 5000 [4] => 4000 [5] => 1500 )

    fields
    ( [0] => a7500 [1] => a7000 [2] => a6500 [3] => a5000 [4] => a4000 [5] => a1500 [6] => a_photo [7] => a_cancel [8] => a_topay [9] => a_total [10] => b_tshirt_total_qty [11] => b_tshirt_total [12] => b_other [13] => b_other_total [14] => b_total [15] => c7500 [16] => c7000 [17] => c6500 [18] => c5000 [19] => c4000 [20] => c_cancel [21] => c_total [22] => ab_total [23] => insurance_no [24] => tboard_day_total [25] => tboard_weekly [26] => daily_banked [27] => insurance_no_total )
    ****A Section
    a7500
    a7000
    a6500
    a5000
    a4000
    a1500
    a_photo
    a_cancel
    a_topay
    a_total

    ****B Section
    b_tshirt_total_qty  = # Shirts
    b_tshirt_total      = 'Total Shirts'
    b_other             = 'Other'
    b_other_total       = 'Other Total'
    b_total             = 'Goods Total'

    ****C Section
    c7500
    c7000
    c6500
    c5000
    c4000
    c_cancel
    c_total

    ab_total
    insurance_no
    tboard_day_total
    tboard_weekly
    daily_banked
    insurance_no_total

    $d
    2014-08

    $current_time
    1408028400
    */
}

/************************Finish generating fields for data now start calculating********************************************/
/**
 * @param $onsite_rates
 * @param $config
 * @param $tprices
 * @param $offsite_rates
 * @param $site_id
 * @param $d
 * @param $showSql
 * @param $month_days
 * @param $fields
 *
 * @return array
 */
function calculateIncomeFromCustomerRegs1($onsite_rates, $config, $tprices, $offsite_rates, $site_id, $d, $showSql, $month_days, $fields)
{
    /*
    This query gets all of the income from customerregs1
    Note that in August 2014 no shirts were bought with registrations
    so for the purpose of what I am the T-Shirt totals bug (0000603 on
    Mantis) this is irrelevant

    Note that in the spreadsheet (income_fixed.php.xlsx) we start numbering queries from here and are not. Below is query 1.


    //SELECT
    //	cr.BookingDate, /* A Section */
//SUM(IF(Rate = '7500' AND CollectPay = 'Onsite', NoOfJump, 0)) AS a7500,
//	SUM(IF(Rate = '4000' AND CollectPay = 'Onsite', NoOfJump, 0)) + SUM(IF(2ndj > 0, 2ndj / 4000, 0)) AS a4000,
//
//	SUM(IF(Agent NOT LIKE '%Bungy%' AND Agent NOT LIKE '%NULL%' AND Agent NOT LIKE '', IF(RateToPay * RateToPayQTY > 0, RateToPay * RateToPayQTY, 0), 0)) AS a_topay,
//	#SUM(IF(Agent NOT LIKE '%Bungy%' AND Agent NOT LIKE '%NULL%' AND Agent NOT LIKE '', RateToPay * RateToPayQTY,  0)) AS a_topay,
//
//	SUM(IF(Agent LIKE '%Bungy%', IF(RateToPay * RateToPayQTY > 0, RateToPay * RateToPayQTY, 0), 0)) AS b_topay,
//
//	SUM(IF(CollectPay = 'Onsite', CAST(Rate * NoOfJump AS UNSIGNED), 0) + CAST(photos_qty * 500 AS UNSIGNED)+ `2ndj`) AS a_total, /* B Section */ 		           	SUM(tshirt_qty) AS b_tshirt_total_qty, SUM(tshirt) AS b_tshirt_total, /*SUM(photos / 2500 + video / 1 + gopro / 1) AS a_photo,*/
//	( SUM(IF(other > 0, other / 100, 0)) ) AS b_other,
//	SUM(other) AS b_other_total, SUM(photos_qty + video_qty + gopro_qty) AS a_photo, /* C Section */
//	SUM(IF(Rate = '7500' AND CollectPay = 'Offsite', NoOfJump, 0)) AS c7500,
//	SUM(IF(CollectPay = 'Offsite', NoOfJump * Rate, 0)) AS c_total from customerregs1 AS cr
//
//WHERE
//	site_id = '1'
//    AND BookingDate LIKE '2014-08-25'
//	/*These 3 cause a_topay to be empty*/
//	#AND NoOfJump > 0
//	#AND DeleteStatus = 0
//	#AND Checked = 1
//	GROUP BY
//		BookingDate
//
//ORDER BY
//	BookingDate ASC;
// 	site_id = '1';
// ------------------------------------
// AND BookingDate LIKE '2014-08-25'
// AND (
//     NoOfJump > 0
//     AND DeleteStatus = 0
//     AND Checked = 1
// ) OR (
// Agent NOT LIKE '%Bungy%'
// AND Agent NOT LIKE '%NULL%'
// AND Agent NOT LIKE ''
// 	)
// 	GROUP BY
// 		BookingDate
//

    $sql = "SELECT
cr.BookingDate,
	/* A Section */ \n";
    foreach ($onsite_rates as $rate) {
        $sql .= " sum(IF(Rate = '$rate' AND CollectPay = 'Onsite', NoOfJump, 0)) ";
        if ($rate == $config['second_jump_rate']) {
            $sql .= "+ SUM(IF(2ndj > 0, 2ndj / {$config['second_jump_rate']}, 0))";
        }
        $sql .= " as a$rate, \n";
    }
    #AND CollectPay = 'Onsite' was added on October 11th 2016 to the a_topay (agent to pay) total as it should only count Onsite payments
    $sql .= "
    SUM(IF(Agent NOT LIKE '%Bungy%' AND CollectPay = 'Onsite' AND Agent NOT LIKE '%NULL%' AND Agent NOT LIKE '', IF(RateToPay * RateToPayQTY > 0, RateToPay * RateToPayQTY, 0), 0)) AS a_topay,
	SUM(IF(Agent LIKE '%Bungy%', IF(RateToPay * RateToPayQTY > 0, RateToPay * RateToPayQTY, 0), 0)) AS b_topay,
	sum(IF(CollectPay = 'Onsite', CAST(Rate * NoOfJump AS UNSIGNED), 0) + CAST(photos_qty * 500 AS UNSIGNED)+ `2ndj`) as a_total,
	/* B Section */
	";
    foreach ($tprices as $tprice) {
        //$sql .= "sum(IF(tshirt / tshirt_qty = $tprice, tshirt_qty, 0)) as b$tprice,\n";
    }
    $sql .= "sum(tshirt_qty) as b_tshirt_total_qty,\n";
    $sql .= "
sum(tshirt) as b_tshirt_total,
	/*sum(photos / {$config['price_photo']} + video / {$config['price_video']} + gopro / {$config['price_gopro']}) as a_photo,*/
	(
	 sum(IF(other > 0, other / 1000, 0))
	) as b_other,
	sum(other) as b_other_total,
	sum(photos_qty + video_qty + gopro_qty) as a_photo,
	/* C Section */
	";
    foreach ($offsite_rates as $rate) {
        $sql .= " sum(IF(Rate = '$rate' AND CollectPay = 'Offsite', NoOfJump, 0)) as c$rate, \n";
    }
    $sql .= "
sum(IF(CollectPay = 'Offsite', NoOfJump * Rate, 0)) as c_total
from customerregs1 as cr
WHERE site_id = '" . $site_id . "' AND BookingDate like '$d-%' and NoOfJump > 0 and DeleteStatus = 0 and Checked = 1
GROUP BY BookingDate
ORDER BY BookingDate ASC;
";
//example query
//SELECT cr.BookingDate, /* A Section */ sum(IF(Rate = '7500' AND CollectPay = 'Onsite', NoOfJump, 0)) as a7500, sum(IF(Rate = '7000' AND CollectPay = 'Onsite', NoOfJump, 0)) as a7000, sum(IF(Rate = '6500' AND CollectPay = 'Onsite', NoOfJump, 0)) as a6500, sum(IF(Rate = '5000' AND CollectPay = 'Onsite', NoOfJump, 0)) as a5000, sum(IF(Rate = '4000' AND CollectPay = 'Onsite', NoOfJump, 0)) + SUM(IF(2ndj > 0, 2ndj / 4000, 0)) as a4000, sum(IF(Rate = '1500' AND CollectPay = 'Onsite', NoOfJump, 0)) as a1500, sum(IF(CollectPay = 'Onsite', IF(RateToPay * RateToPayQTY > 0, RateToPay * RateToPayQTY, 0), 0)) as a_topay, sum(IF(CollectPay = 'Onsite', CAST(Rate * NoOfJump AS UNSIGNED), 0) + CAST(photos_qty * 500 AS UNSIGNED)+ `2ndj` + IF(RateToPay * RateToPayQTY > 0, RateToPay * RateToPayQTY, 0)) as a_total, /* B Section */ sum(tshirt_qty) as b_tshirt_total_qty, sum(tshirt) as b_tshirt_total, /*sum(photos / 2500 + video / 1 + gopro / 1) as a_photo,*/ ( sum(IF(other > 0, other / 100, 0)) ) as b_other, sum(other) as b_other_total, sum(photos_qty + video_qty + gopro_qty) as a_photo, /* C Section */ sum(IF(Rate = '7500' AND CollectPay = 'Offsite', NoOfJump, 0)) as c7500, sum(IF(Rate = '7000' AND CollectPay = 'Offsite', NoOfJump, 0)) as c7000, sum(IF(Rate = '6500' AND CollectPay = 'Offsite', NoOfJump, 0)) as c6500, sum(IF(Rate = '5000' AND CollectPay = 'Offsite', NoOfJump, 0)) as c5000, sum(IF(Rate = '4000' AND CollectPay = 'Offsite', NoOfJump, 0)) as c4000, sum(IF(CollectPay = 'Offsite', NoOfJump * Rate, 0)) as c_total from customerregs1 as cr WHERE site_id = '1' AND BookingDate like '2014-08-%' and NoOfJump > 0 and DeleteStatus = 0 and Checked = 1 GROUP BY BookingDate ORDER BY BookingDate ASC;
//results of this query in income_fixed.xls tab 1
//echo $sql;
    if ($showSql) echo $sql . "<br>";

    $res = mysql_query($sql) or (die(mysql_error()));
    $all_data = array();
//stash the results in to an array that looks like the spreadsheet
    while ($row = mysql_fetch_assoc($res)) {
        $all_data[$row['BookingDate']] = $row;
        //Deji This is a bugfix sum we store the total number of t-shirts and other items stored in customerregs1
        $all_data[$row['BookingDate']]['b_customerregs1_total'] = $all_data[$row['BookingDate']]['b_tshirt_total'] + $all_data[$row['BookingDate']]['b_other_total'];
    }
    mysql_free_result($res);

// fill all dates with zeros if no result
    for ($i = 1; $i <= $month_days; $i++) {
        $cd = $d . '-' . sprintf("%02d", $i);
        if (!array_key_exists($cd, $all_data)) {
            $all_data[$cd] = array();
        }
        //copy the fields array from above and set it to zero
        foreach ($fields as $field) {
            if (!array_key_exists($field, $all_data[$cd])) {
                $all_data[$cd][$field] = 0;
            }
        }
    }

    ksort($all_data);

    return array($rate, $all_data, $field);
}

/**
 * @param $site_id
 * @param $d
 * @param $showSql
 * @param $all_data
 *
 * @return array
 */
function calculateCancellationsOnsite($site_id, $d, $showSql, $all_data)
{
// cancellations onsite
//see results in in query 2 on the spreadsheet
    $sql = "SELECT
  BookingDate,
  SUM(CancelFeeQTY) as qty,
  SUM(CancelFee * CancelFeeQTY) as total
  FROM customerregs1
  WHERE site_id = '" . $site_id . "'
      AND BookingDate like '$d-%'
      AND Checked = 1
      AND CancelFeeCollect = 'Onsite'
      AND NoOfJump > 0
  GROUP BY BookingDate ORDER BY BookingDate ASC;";
//echo $sql;
    if ($showSql) echo $sql . "<br>";

    $res = mysql_query($sql) or (die(mysql_error()));
    while ($row = mysql_fetch_assoc($res)) {
        $all_data[$row['BookingDate']]['a_cancel'] = $row['total'];
        $all_data[$row['BookingDate']]['a_total'] += $row['total'];
    }
    mysql_free_result($res);

    return $all_data;
}

/**
 * @param $site_id
 * @param $d
 * @param $showSql
 * @param $all_data
 *
 * @return array
 */
function calculateCancellationsOffsite($site_id, $d, $showSql, $all_data)
{
// cancellations offsite
//see results in in query 3 on the spreadsheet
    $sql = "SELECT BookingDate,
  SUM(CancelFeeQTY) as qty,
  SUM(CancelFee * CancelFeeQTY) as total FROM customerregs1
    WHERE site_id = '" . $site_id . "'
    AND BookingDate like '$d-%'
    AND CancelFeeCollect = 'Offsite'
    GROUP BY BookingDate
    ORDER BY BookingDate ASC;";
    if ($showSql) echo $sql . "<br>";

    $res = mysql_query($sql) or (die(mysql_error()));

    while ($row = mysql_fetch_assoc($res)) {
        $all_data[$row['BookingDate']]['c_cancel'] = $row['total'];
        $all_data[$row['BookingDate']]['c_total'] += $row['total'];
    }

    mysql_free_result($res);

    return $all_data;
}
/**
 * @param $site_id
 * @param $d
 * @param $showSql
 * @param $all_data
 * @param $config
 * @param $tprices
 *
 * @return array
 */
function calculateMerchandiseIncome($site_id, $d, $showSql, $all_data, $config, $tprices)
{
// merchandise items
//query 4
    $sql = "
SELECT sales_date,
       sum(sale_total_tshirt) as total_tshirt, 
       sum(sale_total_qty_tshirt) as total_qty_tshirt,
       sum(sale_total_2nd_jump) as total_2nd_jump, 
       sum(sale_total_qty_2nd_jump) as total_qty_2nd_jump,
       sum(sale_total_photo) as total_photo, 
       sum(sale_total_qty_photo) as total_qty_photo,
       sum(sale_total) as total, 
       sum(sale_total_qty) as total_qty
       FROM (
		       SELECT 
		       date_format(sale_time, '%Y-%m-%d') as 'sales_date', 
		       merchandise_sales.*
		       FROM merchandise_sales
		       WHERE site_id = '" . $site_id . "' AND sale_time like('$d-%')
		       ORDER BY 'sales_date'
	    ) as t1
       GROUP BY sales_date
       ORDER BY sales_date;
       ";
    if ($showSql) echo $sql . "<br>";

    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        if (!array_key_exists($row['sales_date'], $all_data)) {
            $all_data[$row['sales_date']] = array();
            $all_data[$row['sales_date']]['b_other'] = 0;
            $all_data[$row['sales_date']]['b_other_total'] = 0;
            $all_data[$row['sales_date']]['a' . $config['second_jump_rate']] = 0;
            $all_data[$row['sales_date']]['a_photo'] = 0;
            //$all_data[$row['sales_date']]['b_photo_collect'] = 0;
            $all_data[$row['sales_date']]['a_total'] = 0;
            $all_data[$row['sales_date']]['b_tshirt_total_qty'] = 0;
            $all_data[$row['sales_date']]['b_tshirt_total'] = 0;
            foreach ($tprices as $tprice) {
                //$all_data[$row['sales_date']]['b'. sprintf("%d", $tprice)] = 0;
            }
        }
        //The calculation of b_other and b_other_total differ from daily_sales_reports
        //This is how it is calculated in daily_sales_report.php
        //$totals['b']['qty'] += $row['qty'] - $row['qty_2nd_jump'] - $row['qty_photo'];//if we remove jumps and photos we get the number of shirts sold, stickers and food items sold
        //$totals['b']['total'] += $row['total'] - $row['total_2nd_jump'] - $row['total_photo'];//this is the total revenue for the above items
        //However daily_sales_report also adds on the customerregs1 shirt sales
        //echo$row['sales_date']." - ".$all_data[$row['sales_date']]['b_other']."<br>";
        $all_data[$row['sales_date']]['b_other'] += $row['total_qty'] - $row['total_qty_2nd_jump'] - $row['total_qty_tshirt'] - $row['total_qty_photo'];
        $all_data[$row['sales_date']]['b_other_total'] += $row['total'] - $row['total_2nd_jump'] - $row['total_tshirt'] - $row['total_photo'];

        //we are gonna stash the sales total in a temporary variable and then unset it once we have copied it to where we need it (see further down)
        $all_data[$row['sales_date']]['b_merchandise_sales_total'] += $row['total'];
        //here we subtract the total tshirts and second jump as they are added to other parts of the spreadsheet.
        //We have to do this because the way the data is stored makes things difficult.
        $all_data[$row['sales_date']]['b_merchandise_sales_total'] = $all_data[$row['sales_date']]['b_merchandise_sales_total'] - $row['total_2nd_jump'] - $row['total_photo'];

        //TSHIRT CORRECT TOTALS
        $all_data[$row['sales_date']]['b_tshirt_total_qty'] += $row['total_qty_tshirt'];
        $all_data[$row['sales_date']]['b_tshirt_total'] += $row['total_tshirt'];

        $all_data[$row['sales_date']]['a' . $config['second_jump_rate']] += $row['total_qty_2nd_jump'];
        $all_data[$row['sales_date']]['a_photo'] += $row['total_qty_photo'];
        //$all_data[$row['sales_date']]['b_photo_collect'] += $row['total_qty_photo'] * 500;
        $all_data[$row['sales_date']]['a_total'] += $row['total_2nd_jump'] + $row['total_qty_photo'] * 500;
    }
    mysql_free_result($res);

    return $all_data;
}


/**
 * @param $current_time
 * @param $site_id
 * @param $d
 * @param $showSql
 *
 * @return int
 */
function calculateInsuranceNumberTotalFromPreviousMonth($current_time, $site_id, $d, $showSql)
{
// insurance no total from previous month
    $d_start = date("Y-m-01", mktime(0, 0, 0, 1, 1, date("Y", $current_time)));
// only this year records
//query 6
    $sql = "SELECT sum(j_insurance_no) as insurance_no_total from daily_reports where site_id = '" . $site_id . "' AND `date` < '$d-01' AND `date` >= '$d_start' order by `date` DESC LIMIT 1;";
    if ($showSql) echo $sql . "<br>";

    $res = mysql_query($sql) or die(mysql_error());
    $insurance_no_run_total = 0;
    if ($row = mysql_fetch_assoc($res)) {
        $insurance_no_run_total = $row['insurance_no_total'];

        return $insurance_no_run_total;
    }

    return $insurance_no_run_total;
}

/**
 * @param $site_id
 * @param $d
 * @param $showSql
 * @param $all_data
 *
 * @return array
 */
function getIncomeRecords($site_id, $d, $showSql, $all_data)
{
    //this appears to not be used anymore
    //income records
    //query 7
    $sql = "SELECT * from income where site_id = '" . $site_id . "' AND `date` like '$d-%'";
    if ($showSql) echo $sql . "<br>";

    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        if (!array_key_exists($row['date'], $all_data)) {
            $all_data[$row['date']] = $row;
        } else {
            $all_data[$row['date']] = array_merge($all_data[$row['date']], $row);
        }
        $all_data[$row['date']]['insurance_no'] = 0;
    }

    return $all_data;
}

/**
 * @param $config
 * @param $site_id
 * @param $d
 * @param $showSql
 * @param $all_data
 *
 * @return array
 */
function calculateInsuranceNumberFromDailyReports($config, $site_id, $d, $showSql, $all_data)
{
// select insurance no from daily reports
//query 8
//TODO this code is duplicated in the Income class, remove this query
    $sql = "
	SELECT
		custreg.BookingDate as date,
		sum(
			CAST((custreg.NoOfJump /*+ IFNULL(ms.total_2nd, 0)*/) AS SIGNED)
			#+ CAST((custreg.2ndj_qty/* + IFNULL(ms.total_2nd, 0)*/) AS SIGNED)  /*2ndj_qty commented out to prevent double counting of 2nd jumps*/
			- CAST(IF(custreg.Agent like '___Bungy' AND custreg.CollectPay = 'Offsite', custreg.NoOfJump, 0) AS SIGNED)/*j_test_jumps*/
			- CAST(IF(custreg.Rate = '{$config['second_jump_rate']}', custreg.NoOfJump, 0) AS SIGNED)/*j_2nd_jumps */
			- CAST(IFNULL(nj.non_jump, 0) AS SIGNED) /*j_non_jumps*/
		) AS j_insurance_no

	FROM customerregs1 AS custreg

	LEFT JOIN (
		SELECT DISTINCT w.id, cr.BookingDate, COUNT(w.id) as non_jump, cr.CustomerRegID as nonJumpRegID
		FROM customerregs1 cr, waivers w, non_jumpers
		WHERE
		cr.site_id = $site_id
		AND cr.BookingDate LIKE '$d-%'
		AND cr.Checked = 1
		AND cr.DeleteStatus = 0
		AND cr.CustomerRegID = w.bid
		AND w.id = non_jumpers.waiver_id
		GROUP BY cr.BookingDate
	) nj ON nj.BookingDate = custreg.BookingDate AND custreg.CustomerRegID = nonJumpRegID
  /*
  currently I am ignoring the sales from the merchandise tables as this gives the wrong answer in some cases
  I think because we end up subtracting the value again in the daily_report.php calculation
	LEFT JOIN (
		SELECT DATE(sale_time) AS sale_date, IFNULL(SUM(sale_total_qty_2nd_jump), 0) AS total_2nd
        FROM merchandise_sales
        WHERE site_id = 1 AND sale_time LIKE '$d-%'
        GROUP BY sale_date
	) AS ms ON ms.sale_date =  custreg.BookingDate
  */
	WHERE site_id = $site_id
		AND custreg.BookingDate LIKE '$d-%'
		AND custreg.Checked = 1
		AND custreg.DeleteStatus = 0
	GROUP BY custreg.BookingDate;
";
//The insurace total used to come from daily_reports, but this would only be updated if the daily_reports page has been
//updated instead I have modified the calculation from daily_report to calculated insurance totals dynamically in the
//sql query above.
//$sql = "SELECT `date`, j_insurance_no FROM daily_reports where site_id = '" . $site_id . "' AND `date` like '$d-%';";

    if ($showSql) echo $sql . "<br>";

    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        $all_data[$row['date']]['insurance_no'] = $row['j_insurance_no'];
    }

    return $all_data;
}

function handlePOST($site_id, $record_type, $current_time, $onsite_rates, $tprices, $offsite_rates, $d, $subdomain, $all_data, $fields)
{
    $action = $_POST['action'];
    if (empty($action)) {
        $action = $_GET['action'];
    }
    switch (TRUE) {
        case ($action == 'update'):
            foreach ($_POST['insurance_no'] as $key => $value) {
                $record = array(
                    'date'               => $_POST['date'] . '-' . sprintf("%02d", $key),
                    'insurance_no'       => $value,
                    'tboard_day_total'   => $_POST['tboard_day_total'][$key],
                    'insurance_no_total' => $_POST['insurance_no_total'][$key]
                );
                if (array_key_exists('id', $_POST) && $_POST['id'][$key] > 0) {
                    $record['id'] = $_POST['id'][$key];
                }
                $record['site_id'] = $site_id;
                save_report($record_type, $record);
                //Save the report to the income table
                //We also need to
            }
            Header("Location: $record_type.php?date=" . date("Y-m", $current_time));
            die();
            break;
        case ($action == 'Delete'):
            $sql = "delete from income where site_id = '" . $site_id . "' AND id = '{$_GET['id']}'";
            mysql_query($sql) or die(mysql_error());
            Header("Location: $record_type.php?date=" . date("Y-m", $current_time));
            die();
            break;
        case ($action == 'CSV'):
            $current_year = date("Y", $current_time);
            $headers = array(
                'Date'
            );
            $headers = array_merge($headers, $onsite_rates);
            $headers = array_merge($headers, array(
                'PHOTO (500)',
                'Cancel',
                'Onsite Total',
            ));
            foreach ($tprices as $tprice) {
                //$headers[] = $tprice . ' Shirts';
            }
            $headers[] = '# Shirts';
            $headers[] = 'Total Shirts';
            $headers = array_merge($headers, array(
                'Other',
                'Other total',
                'Goods Total'
            ));
            $headers = array_merge($headers, $offsite_rates);
            $headers = array_merge($headers, array(
                'Cancel',
                'Offsite Total',
                'DAILY CASH TOTAL',
                'Insurance No',
                'Tourism Board Daily Total',
                'To Pay - Agent',
                'To Pay - Bungy',
                'To Pay - Photo',
                'Daily Amount To Be Banked',
                'Insurance No Running Total'
            ));
            $d = str_replace("-", '', $d);
            $filename = "income-$subdomain-$d.csv";
            Header('Content-Type: text/csv; name="' . $filename . '"');
            Header('Content-Disposition: attachment; filename="' . $filename . '"');
            $fp = fopen('php://output', "w");
            $delimiter = ',';
            fputcsv($fp, $headers, $delimiter);
            $totals = array();
            foreach ($all_data as $current_date => $record) {
                $csv_row = array();
                $csv_row[] = $current_date;
                foreach ($fields as $field) {
                    $value = 0;
                    if (array_key_exists($field, $all_data[$current_date])) {
                        $value = $all_data[$current_date][$field];
                    }
                    //$value = sprintf("%.0d", $value);
                    if (!empty($value)) {
                        if (!array_key_exists($field, $totals)) {
                            $totals[$field] = 0;
                        }
                        $totals[$field] += $value;
                    }
                    if ($field == 'insurance_no_total') {
                        $totals[$field] = $value;
                    }

                    $csv_row[] = $value;
                }
                fputcsv($fp, $csv_row, $delimiter);
            }
            // totals row
            $csv_row = array();
            $csv_row[] = 'Total:';
            foreach ($fields as $field) {
                $csv_row[] = sprintf("%.02f", ($totals[$field]) ? $totals[$field] : 0);
            }
            fputcsv($fp, $csv_row, $delimiter);
            die();
            break;
    }
}

/**
 * @param $month_days
 * @param $current_time
 * @param $fields
 * @param $all_data
 * @param $tprices
 * @param $row
 * @param $onsite_rates
 * @param $site_id
 * @param $freeOfChargeAndNonJumps
 * @param $config
 * @param $tb_weekly
 * @param $in_last_total
 *
 * @return array
 */
function doCalculations($month_days, $current_time, $fields, $all_data, $tprices, $row, $onsite_rates, $site_id, $freeOfChargeAndNonJumps, $config, $tb_weekly, $in_last_total, $insurance_no_run_total)
{
    $tb_weekly = 0;
    $in_last_total = $insurance_no_run_total;
    for ($day = 1; $day <= $month_days; $day++) {
        $current_date = date("Y-m-", $current_time) . sprintf("%02d", $day);
        foreach ($fields as $field) {
            if (array_key_exists($current_date, $all_data)) {
                if ($field == 'b_total') {
                    //$all_data[$current_date]['b_total'] = $all_data[$current_date]['b_other_total'];
                    foreach ($tprices as $tprice) {
                        //$all_data[$current_date]['b_total'] += $all_data[$current_date]['b'.$tprice] * $tprice;
                    }

                    //$all_data[$current_date]['b_total'] = $all_data[$current_date]['b_tshirt_total'];
                    //The total is calculated by adding the total from merchandise_sales and to the items sold in customerregs1
                    $all_data[$current_date]['b_total'] = $all_data[$current_date]['b_merchandise_sales_total']
                        + $all_data[$current_date]['b_customerregs1_total'];
                    //- $all_data[$current_date]['b_tshirt_total']
                    //- $all_data[$current_date][''];
                    //subtract second jump total and photos total

                    unset($all_data[$row['sales_date']]['b_merchandise_sales_total']);
                    unset($all_data[$row['sales_date']]['b_customerregs1_total']);//stash in a temporary variable then unset it
                }
                if ($field == 'ab_total') {
                    $all_data[$current_date]['ab_total'] = $all_data[$current_date]['a_total'] + $all_data[$current_date]['b_total'];
                }

                if ($field == 'paying_jump_no') {
                    $all_data[$current_date]['paying_jump_no'] = 0;
                    foreach ($onsite_rates as $rate) {
                        $all_data[$current_date]['paying_jump_no'] += $all_data[$current_date]['a' . $rate];
                    }
                }

                if ($field == 'max_yen_jumpers') {
                    $all_data[$current_date]['max_yen_jumpers'] = ($all_data[$current_date]['a_total'] + $all_data[$current_date]['c_total']) / 7500;
                }

                if ($field == 'tboard_day_total') {
                    //There is a piece of javascript that recalculates the tboard_day_total value... why???? I don't know.
                    //$freeOfChargeDeduction = Income::freeOfChargeDeduction($site_id, $freeOfChargeAndNonJumps[$current_date], $all_data[$current_date]);
                    //$all_data[$current_date]['tboard_day_total'] = $all_data[$current_date]['insurance_no'] * $config['tb_payment'] - $freeOfChargeDeduction;
                    $all_data[$current_date]['tboard_day_total'] = calculateTourismBoardMerchandisePayments($current_date, $site_id) + Income::getInsuranceTotalIncomeSheet($site_id, $freeOfChargeAndNonJumps[$current_date], $all_data[$current_date], $config['tb_payment']);

                    $tb_weekly += $all_data[$current_date]['tboard_day_total'];
                }

                if ($field == 'tboard_weekly') {
                    $all_data[$current_date]['tboard_weekly'] = 0;
                    if (in_array($day, array(10, 20, $month_days))) {
                        $all_data[$current_date]['tboard_weekly'] = $tb_weekly;
                        $tb_weekly = 0;
                    }
                }

                if ($field == 'daily_banked') {
                    $all_data[$current_date]['daily_banked'] = $all_data[$current_date]['ab_total']; // - $all_data[$current_date]['tboard_day_total'];
                }

                if (array_key_exists($field, $all_data[$current_date])) {
                    $all_data[$current_date][$field] = sprintf("%.0d", $all_data[$current_date][$field]);
                }
            }
        }
        $all_data[$current_date]['insurance_no_total'] = $in_last_total;
        if (array_key_exists('insurance_no', $all_data[$current_date])) {
            $all_data[$current_date]['insurance_no_total'] += $all_data[$current_date]['insurance_no'];
        }
        $in_last_total = $all_data[$current_date]['insurance_no_total'];
    }

    return array($day, $current_date, $field, $all_data, $rate);
}

function calculateTourismBoardMerchandisePayments($date, $site_id)
{
    //For Ryujin, people pay for bridge access. It is 1000 yen, and 310 goes to the tourism board
    //we have started calculating this from July 2016 so we filter out previous dates to prevent
    //Return zero for other sites

    if ($site_id != 3) return 0;//nothing for non Ryujin

    $sql = "
        SELECT IFNULL(SUM(quantity),0) AS quantity FROM (
            (
            SELECT 
                SUM(`quantity`) AS quantity, 
                DATE(`sale_time`) AS `date` 
            FROM
                merchandise_sales_items msi
            LEFT JOIN 
                merchandise_sales ms ON (msi.sales_id = ms.id)
            WHERE 
                item_id = 85 AND 
                DATE(`sale_time`) = '$date' AND 
                DATE(`sale_time`) >= '2016-07-01' AND #start including bridge access from here
                ms.site_id = $site_id 
            )
        
            UNION
            (
            SELECT 
                ROUND(SUM(other)/1000) AS quantity, #other is used to 
                BookingDate AS `date`
            FROM customerregs1 
            WHERE 
                site_id = $site_id AND 
                BookingDate = '$date' AND 
                BookingDate >= '2016-07-01' AND #start including bridge access from here
                other > 0
            )
         ) AS t
    ";

    $results = queryForRows($sql);

    $value = 0;
    if(array_key_exists('0', $results)) {
        $value = $results[0]['quantity'] * 310;
    }

    return $value;
}

function drawRows($month_days, $current_time, $all_data, $fields, $edit_disabled)
{
    $inputs = array(
            /*
            'insurance_no',
            'tboard_day_total',
            'tboard_weekly',
            'insurance_no_total'
            */
    );

    $tb_weekly = 0;
    //pr($all_data);

    for ($day = 1; $day <= $month_days; $day++) {
        $current_date = date("Y-m-", $current_time) . sprintf("%02d", $day);
        echo "\t<tr class=\"" . (($day % 2) ? 'even' : 'odd') . "\">\n";
        echo "\t\t<td>$day</td>\n";
        echo "\t\t<td>" . date('D', mktime(0, 0, 0, date('m', $current_time), $day, date('Y', $current_time))) . "<input type=\"hidden\" name=\"id[$day]\" value=\"{$all_data[$current_date]['id']}\"></td>\n";
        //echo $current_date;

        //pr($all_data[$current_date]);
        foreach ($fields as $field) {
            $shouldBoldEntries = $field == "a_total" || $field == "b_total" || $field == "c_total" || $field == "tboard_day_total" || $field == "daily_banked";
            $style = '';
            if ($shouldBoldEntries) {//use this to make things bold
                $style = " style=\"font-weight: bold;\"";
            }

            $value = '';
            if (array_key_exists($current_date, $all_data) && array_key_exists($field, $all_data[$current_date])) {
                $value = $all_data[$current_date][$field];
            }

            if (in_array($field, $inputs)) {
                //if ($field != 'insurance_no')
                $style .= ' readonly';
                echo "\t\t<td custom-day=\"$day\" custom-field=\"$field\"><input name=\"{$field}[$day]\" class=\"$field\" value=\"$value\"$style></td>\n";
            } else {
                echo "\t\t<td custom-day=\"$day\" custom-field=\"$field\"$style>$value</td>\n";
            }
        }
        if (!$edit_disabled) {
            echo "\t\t<td><a href=\"income_edit.php?date=$current_date\">Edit</a></td>\n";
        }
        ?>
        </tr>
        <?php
    }
}

function _drawCalculateRows($month_days, $current_time, $all_data, $config, $fields, $edit_disabled)
{
    $inputs = array(
    );

    $totals = array();
    $tb_weekly = 0;
    //pr($all_data);
    for ($day = 1; $day <= $month_days; $day++) {
        $current_date = date("Y-m-", $current_time) . sprintf("%02d", $day);
        echo "\t<tr class=\"" . (($day % 2) ? 'even' : 'odd') . "\">\n";
        echo "\t\t<td>$day</td>\n";
        if (!array_key_exists($current_date, $all_data) || !array_key_exists('id', $all_data[$current_date])) {
            $all_data[$current_date]['id'] = 0;
        }
        echo "\t\t<td>" . date('D', mktime(0, 0, 0, date('m', $current_time), $day, date('Y', $current_time))) . "<input type=\"hidden\" name=\"id[$day]\" value=\"{$all_data[$current_date]['id']}\"></td>\n";
        //echo $current_date;

        //pr($all_data[$current_date]);
        unset($all_data[$current_date]['tboard_weekly']);//This column has been removed so unset it and remove it from fields and table headers
        $all_data[$current_date]['p_topay'] = (((int)$all_data[$current_date]['a_photo']) * $config['price_photo']) - (((int)$all_data[$current_date]['a_photo']) * 500);//Added a To-Photo pay column which will calculate the total amount of photos
        foreach ($fields as $field) {
            $shouldBoldEntries = $field == "a_total" || $field == "b_total" || $field == "c_total" || $field == "tboard_day_total" || $field == "daily_banked";
            $style = '';
            if ($shouldBoldEntries) {//use this to make things bold
                $style = " style=\"font-weight: bold;\"";
            }

            $value = '';
            if (array_key_exists($current_date, $all_data) && array_key_exists($field, $all_data[$current_date])) {
                $value = $all_data[$current_date][$field];
            }

            //Sum up totals for all values
            if (!empty($value)) {
                if (!array_key_exists($field, $totals)) {
                    $totals[$field] = 0;
                }
                $totals[$field] += $value;
            }

            if ($field == 'insurance_no_total') {
                $totals[$field] = $value;
            }

            if ($field == 'daily_banked') {
                $value = $all_data[$current_date]['a_topay'] + $all_data[$current_date]['b_topay'] + $all_data[$current_date][$field];
                //the total for daily_banked has already been calculated so just add agent to pay, bungy to pay to the total
                $totals['daily_banked'] += $all_data[$current_date]['a_topay'] + $all_data[$current_date]['b_topay'];
            }

            if (in_array($field, $inputs)) {
                //if ($field != 'insurance_no')
                $style .= ' readonly';
                echo "\t\t<td custom-day=\"$day\" custom-field=\"$field\"><input name=\"{$field}[$day]\" class=\"$field\" value=\"$value\"$style></td>\n";
            } else {
                echo "\t\t<td custom-day=\"$day\" custom-field=\"$field\"$style>$value</td>\n";
            }
        }
        if (!$edit_disabled) {
            echo "\t\t<td><a href=\"income_edit.php?date=$current_date\">Edit</a></td>\n";
        }
        ?>
        </tr>
        <?php
    }

    return array($totals, $all_data);
}

/**
 * @param $month_days
 * @param $current_time
 * @param $all_data
 * @param $config
 * @param $fields
 * @param $inputs
 * @param $edit_disabled
 *
 * @return array
 */
function calculateRows($month_days, $current_time, $all_data, $config, $fields)
{
    $inputs = array(/*
            'insurance_no',
            'tboard_day_total',
            'tboard_weekly',
            'insurance_no_total'
            */
    );

    $totals = array();
    $tb_weekly = 0;
    //pr($all_data);
    for ($day = 1; $day <= $month_days; $day++) {
        $current_date = date("Y-m-", $current_time) . sprintf("%02d", $day);
        if (!array_key_exists($current_date, $all_data) || !array_key_exists('id', $all_data[$current_date])) {
            $all_data[$current_date]['id'] = 0;
        }

        unset($all_data[$current_date]['tboard_weekly']);//This column has been removed so unset it and remove it from fields and table headers
        $all_data[$current_date]['p_topay'] = (((int)$all_data[$current_date]['a_photo']) * $config['price_photo']) - (((int)$all_data[$current_date]['a_photo']) * 500);//Added a To-Photo pay column which will calculate the total amount of photos
        foreach ($fields as $field) {

            $value = 0;
            //
            if (array_key_exists($current_date, $all_data) && array_key_exists($field, $all_data[$current_date])) {
                $value = $all_data[$current_date][$field];
            }

            //Sum up totals for all values
            if (!empty($value)) {
                if (!array_key_exists($field, $totals)) {
                    $totals[$field] = 0;
                }
                $totals[$field] += $value;
            }

            if ($field == 'insurance_no_total') {
                $totals[$field] = $value;
            }

            if ($field == 'daily_banked') {
                $all_data[$current_date][$field] = $all_data[$current_date]['a_topay'] + $all_data[$current_date]['b_topay'] + $all_data[$current_date][$field];
                //the total for daily_banked has already been calculated so just add agent to pay, bungy to pay to the total
                $totals['daily_banked'] += $all_data[$current_date]['a_topay'] + $all_data[$current_date]['b_topay'];
            }

        }
    }

    return array($totals, $all_data);
}

$edit_disabled = $user->hasRole('Site Manager');
if($user->getUserName() == 'sugai'){
 $edit_disabled = 1; 
}
// EOFfrom accountant
//the next two lines are not needed?
$site_id = CURRENT_SITE_ID;

list($current_time, $d) = setDate();

$showSql = false;
$record_type = 'income';
$config = getConfigForPOST();
$config = setTourismBoardPaymentInConfig($d, $config);
//This is not necessary as we could just SYSTEM_SUBDOMAIN or the $subdomain variable defined in application_top.php
$subdomain = getSubdomain($site_id);
$onsite_rates = getAllOnsiteRates($site_id, $d, $showSql, $config);
$offsite_rates = getAllOffsiteRates($site_id, $d, $showSql, $config);
$tprices = getTShirtPrices($site_id, $d, $showSql);
list($tprices, $month_days, $fields, $d) = createIncomeSheetDataStructure($tprices, $current_time, $onsite_rates, $offsite_rates);
list($rate, $all_data, $field) = calculateIncomeFromCustomerRegs1($onsite_rates, $config, $tprices, $offsite_rates, $site_id, $d, $showSql, $month_days, $fields);
$all_data = calculateCancellationsOnsite($site_id, $d, $showSql, $all_data);
$all_data = calculateCancellationsOffsite($site_id, $d, $showSql, $all_data);
$all_data = calculateMerchandiseIncome($site_id, $d, $showSql, $all_data, $config, $tprices);
$insurance_no_run_total = calculateInsuranceNumberTotalFromPreviousMonth($current_time, $site_id, $d, $showSql);
$all_data = getIncomeRecords($site_id, $d, $showSql, $all_data);
$all_data = calculateInsuranceNumberFromDailyReports($config, $site_id, $d, $showSql, $all_data);
$freeOfChargeAndNonJumps = Income::getFreeOfChargeDeductionDataIncomeSheet($site_id, $d, $all_data);

//free of charge jumps...
//It has been requested that we insurance cost of free of charge jumps based on a set of rules that is specific to each
//jump site. This is done in the form of a deduction from the total Tourism board payment

/*
function getFreeOfChargeDeductionData($site_id, $d, $all_data) {
    //jumps have these types in the foc column
    //These are the reason why the jump is free
    //bungy = staff jump, promo = promo jump, Media is media jump
    //Cancel = a previously cancelled jump that got a free ticket as someone did not show
    //NJ is a non jump that got free ticket to go again another time
    $freeOfChargeTypesQuery = "
        SELECT
            SUM(IF(foc LIKE 'Bungy%', NoOfJump, 0)) AS bungy,
            SUM(IF(foc LIKE 'Promo%', NoOfJump, 0)) AS promo,
            SUM(IF(foc LIKE 'Media%', NoOfJump, 0)) AS media,
            SUM(IF(foc LIKE 'Cancel%', NoOfJump, 0)) AS cancel,
            SUM(IF(foc LIKE 'NJ%', NoOfJump, 0)) AS non_jump,
            BookingDate AS date

        FROM customerregs1

        WHERE BookingDate LIKE '$d%'
            AND site_id = $site_id
            AND Rate = 0
            AND Checked = 1
            AND DeleteStatus = 0
            AND NoOfJump > 0
        GROUP BY BookingDate
        ORDER BY BookingDate;
    ";

    $freeOfChargeTypesResults = queryForRows($freeOfChargeTypesQuery);

    //transform in to a date indexed array of free of charge jumps and their types
    $freeOfChargeTypes = [];
    foreach ($freeOfChargeTypesResults as $freeOfChargeTypesResult) {
        $freeOfChargeTypes[$freeOfChargeTypesResult['date']] = $freeOfChargeTypesResult;
    }

    //create an array with all of the promo jumps and non jumps
    $freeOfChargeTypesNonJumps = [];//an array of all free of charge types and non jumps
    foreach ($all_data as $dateKey => $dayData) {
        $promo = 0;
        $media = 0;
        $bungy = 0;
        $nonJumps = 0;
        $cancels = 0;

        if(array_key_exists($dateKey,$freeOfChargeTypes)) {
            $promo   = $freeOfChargeTypes[$dateKey]['promo'];
            $media   = $freeOfChargeTypes[$dateKey]['media'];
            $bungy   = $freeOfChargeTypes[$dateKey]['bungy'];
            $cancels = $freeOfChargeTypes[$dateKey]['cancel'];
            $nonJumps= $freeOfChargeTypes[$dateKey]['non_jump'];
        }

        $freeOfChargeTypesNonJumps[$dateKey] = [
            "promo"     => $promo,
            "media"     => $media,
            "bungy"     => $bungy,
            "nonJump"  => $nonJumps,
            "cancel"   => $cancels,
        ];
    }

    return $freeOfChargeTypesNonJumps;
}

function freeOfChargeDeduction($site_id, $freeOfChargeAndNonJumps, $day){
    //Minakami OR Sarugakyo
    $deduction = 0;
    if($site_id == 1 || $site_id == 2) {
        $cancel = $freeOfChargeAndNonJumps['cancel'];
        $promo = $freeOfChargeAndNonJumps["promo"];
        $nonJumps = $freeOfChargeAndNonJumps["nonJump"];
        $media = $freeOfChargeAndNonJumps["media"];
        $bungy = $freeOfChargeAndNonJumps["bungy"];

        $deduction = $promo * 1500 + $media * 1500 + $bungy * 1000 + $cancel * 0 + $nonJumps * 0;

    }

    //the deduction is zero for other sites
    return $deduction;
}

function getInsuranceTotal($site_id, $freeOfChargeAndNonJumps, $day, $tbPayment){
    $insuranceTotal = 0;
    if($site_id != 3) {//There is a special calculation for Ryujin
        $freeOfChargeDeduction = freeOfChargeDeduction($site_id, $freeOfChargeAndNonJumps, $day);
        $insuranceTotal = $day['tboard_day_total'] = $day['insurance_no'] * $tbPayment - $freeOfChargeDeduction;

    } else if($site_id == 3) {
        //Comission is only paid for jumpers that pay 15000, 14000, 13000

        foreach($day as $dayKey => $value) {
            if(preg_match("/^[ac]\d+/", $dayKey, $matches)) {//match numbers starting with a or c
                $rateKey = $matches[0];
                $rate = ltrim($rateKey, "a");
                //if the jump costs these prices only do we add to the insurance total
                if(in_array($rate, [15000, 14000, 13000]))
                    $insuranceTotal += $tbPayment * $day[$dayKey];
            }
        }
    }

    return $insuranceTotal;
}

*/

// EOF insurance no from daily reports

//**********************END Of all queries, now we start calculating the values***************************************/
// calculated fields
list($day, $current_date, $field, $all_data, $rate) = doCalculations($month_days, $current_time, $fields, $all_data, $tprices, $row, $onsite_rates, $site_id, $freeOfChargeAndNonJumps, $config, $tb_weekly, $in_last_total, $insurance_no_run_total);
//recalculate b_total as the values do not add up

foreach ($all_data as $key => &$day_data) {
    $day_data['b_total'] = $day_data['b_other_total'] + $day_data['b_tshirt_total'];
}

list($totals, $all_data) = calculateRows($month_days, $current_time, $all_data, $config, $fields);

/**
 * @param $site_id
 * @param $record_type
 * @param $current_time
 * @param $onsite_rates
 * @param $tprices
 * @param $offsite_rates
 * @param $d
 * @param $subdomain
 * @param $all_data
 * @param $fields
 */
handlePOST($site_id, $record_type, $current_time, $onsite_rates, $tprices, $offsite_rates, $d, $subdomain, $all_data, $fields);

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Income</title>
    <?php include "../includes/head_scripts.php"; ?>
    <link rel="stylesheet" href="css/income_php.css" charset="UTF-8"/>
    <script>
        $(document).ready(function () {
            //disabled as we are no longer allowing editing on insurance number in the spreadsheet
            //the rest of the js is now not used.
            /*
             $(".insurance_no").keyup(function () {
             recalculate_daily_tourism($(this));
             recalculate_tourism();
             recalculate_running_total();
             });

             $(".tboard_day_total").change(function () {
             recalculate_tourism();
             });

             $(".insurance_no").each(function () {
             recalculate_daily_tourism($(this));
             });

             recalculate_tourism();

             recalculate_running_total();
             */

        });
        function recalculate_daily_tourism(el) {
            var day = el.parent().attr('custom-day');
            try {
                $("input[name='tboard_day_total[" + day + "]']").val(<?= $config['tb_payment']; ?> * parseInt(el.val())
            )
                ;
            } catch (e) {
            }
        }
        function recalculate_tourism() {
            var total = 0, last_day = 0;
            $(".tboard_day_total").each(function () {
                try {
                    total += parseInt($(this).val());
                    var day = $(this).parent().attr('custom-day');
                    if (day == 10 || day == 20) {
                        $("input[name='tboard_weekly[" + day + "]']").val(total);
                        total = 0;
                    }
                    last_day = day;
                } catch (e) {
                }
            });
            $("input[name='tboard_weekly[" + last_day + "]']").val(total);
            var total_tourism = 0;
            $("tr.even td[custom-field=tboard_day_total], tr.odd td[custom-field=tboard_day_total]").each(function () {
                var value = 0;
                try {
                    value = parseInt($(this).children('input').val());
                } catch (e) {
                }
                total_tourism += value;
            });
            $("tr.totals th[custom-field=tboard_day_total]").html(total_tourism);
            $("tr.totals th[custom-field=tboard_weekly]").html(total_tourism);

        }
        function recalculate_running_total() {
            var total = parseInt($('#prev_month_run_total').val()) || 0;
            $(".insurance_no").each(function () {
                try {
                    var day = $(this).parent().attr('custom-day');
                    total += parseInt($(this).val());
                    $("input[name='insurance_no_total[" + day + "]']").val(total);
                } catch (e) {
                    alert('error' + e.message);
                }
            });
        }
    </script>
</head>
<body>
<?php include 'includes/main_menu.php';
//calculate the colspans for the tables
//4 Comes from the Photo (500), Cancel, To Pay and Onsite Total Columns
$aOnsiteColumnCount = sizeof($onsite_rates) + 3;//rates columns, Cancel, Onsite Total
$bGoodsColumnCount = 5;//5 columns under B (#Shirts, Total Shirts, Other, Other Total, Goods Total)
$cOffsiteColumnCount = sizeof($offsite_rates) + 2;//3 columns under C (7500, Cancel, Offsite Total)
$aPlusBColumnCount = 1;//Daily Cash Total

$incomeReceivedColumnCount = $aOnsiteColumnCount + $bGoodsColumnCount + $cOffsiteColumnCount + $aPlusBColumnCount;
$delete = 1;

?>

<form method="post">
    <input type="hidden" id="prev_month_run_total" name="prev_month_run_total"
           value="<?= $insurance_no_run_total; ?>">
    <table id="income-table">
        <tr>
            <th rowspan="3" colspan="2">
                <?= str_replace('-', '<br />', date("M-Y", $current_time)); ?><br/>
                <a href="?date=<?= date("Y-m", $current_time - 30 * 24 * 60 * 60); ?>" style="float: left;">&lt;&lt;&lt;</a>
                <a href="?date=<?= date("Y-m", $current_time + 30 * 24 * 60 * 60); ?>" style="float: right;">&gt;&gt;&gt;</a>
            </th>
            <th colspan="<?= $incomeReceivedColumnCount; ?>">INCOME RECEIVED</th>
            <th rowspan="3"><span class="vertical-align">Insurance No</span></th>
            <th rowspan="3"><span class="vertical-align">Tourism Board<br/>Daily Total</span></th>
            <th rowspan="3"><span class="vertical-align">To Pay - Agent</span></th>
            <th rowspan="3"><span class="vertical-align">To Pay - Bungy</span></th>
            <th rowspan="3"><span class="vertical-align">To Pay - Photo</span></th>
            <!--<th rowspan="3"><span class="vertical-align">Tourism Board<br/>Weekly Payment</span></th>-->
            <th rowspan="3"><span class="vertical-align">Daily Amount<br/>To Be Banked</span></th>
            <th rowspan="3"><span class="vertical-align">Insurance No<br/>Running Total</span></th>
            <?php if (!$edit_disabled) { ?>
                <th rowspan="3"><span class="vertical-align">Actions</span></th>
            <?php } ?>
        </tr>
        <tr>
            <th colspan="<?= $aOnsiteColumnCount; ?>">A<br>ONSITE</th>
            <th colspan="<?= $bGoodsColumnCount; ?>">B<br>GOODS</th>
            <th colspan="<?= $cOffsiteColumnCount; ?>">C<br>OFFSITE</th>
            <th>A+B</th>
        </tr>
        <tr>
            <!-- ONSITE -->
            <?php foreach ($onsite_rates as $rate) { ?>
                <th><span class="va2"><?= $rate; ?></span></th>
            <?php } ?>
            <th><span class="va2">Photo<br/>(500)</span></th>
            <th><span class="va2">Cancel</span></th>
            <th>Onsite<br/>Total</th>
            <!-- GOODS -->
            <th><span class="va2"># Shirts</span></th>
            <th><span class="va2">Total<br>Shirts</span></th>
            <th><span class="va2">Other</span></th>
            <th>Other<br/>Total</th>
            <th>Goods<br/>Total</th>
            <!-- OFFSITE -->
            <?php foreach ($offsite_rates as $rate) { ?>
                <th><span class="va2"><?= $rate; ?></span></th>
            <?php } ?>
            <th><span class="va2">Cancel</span></th>
            <th><b>Offsite<br/>Total</b><br/><span class="small-header">(to collect)</span></th>
            <!-- A+B -->
            <th><b>DAILY<br/>CASH<br/>TOTAL</b></th>
        </tr>
        <!--tr class="last-month-totals">
						<td colspan="24">last month rest</td>
						<td custom-day="0" custom-field="tboard_day_total"><?php //echo $tboard_day_total_rest; ?></td>
						<td colspan="2">l/m rest</td>
						<td custom-day="0" custom-field="insurance_no_total"><?php //echo $insurance_no_total_rest; ?></td>
						</tr-->
        <?php

        drawRows($month_days, $current_time, $all_data,  $fields, $edit_disabled);
        //_drawCalculateRows($month_days, $current_time, $all_data, $config, $fields, $edit_disabled);

        echo "\t<tr class=\"totals\">\n";
        echo "\t\t<th colspan=\"2\">Total</th>\n";
        foreach ($fields as $field) {
            $value = 0;
            if (array_key_exists($field, $totals)) {
                $value = $totals[$field];
            }
            echo "\t\t<th custom-field='$field'>$value</th>\n";
        }
        echo "\t\t<th>&nbsp;</th>\n";
        echo "</tr>";
        ?>
    </table>
    <input type="hidden" name="date" value="<?= date('Y-m', $current_time); ?>">
    <button name="action" value="update">Update</button>
</form>
<form>
    <input type="hidden" name="date" value="<?= date('Y-m', $current_time); ?>">
    <button name='action' value="CSV">Download CSV</button>
</form>
<button id="back" onClick="javascript: document.location='/Bookkeeping/'">Back</button>
<?php include("ticker.php"); ?>
</body>
</html>
