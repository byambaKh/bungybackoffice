<?php
include "../includes/application_top.php";

if (!in_array(SYSTEM_SUBDOMAIN_REAL, array('MAIN', 'TEST'))) {
    Header("Location: /Bookkeeping/");
    die();
};

$user = new BJUser();
if(!$user->hasRole(array("SysAdmin", "General Manager", "Financial"))){
   header("Location: ../index.php");
}

mysql_set_charset('utf8');

$startDate = date("Y-m-01");
$endDate = date("Y-m-t");

if (isset($_GET['startDate']) && preg_match("/^([\d]{4}-[\d]{2}-[\d]{2})$/", $_GET['startDate'])) {
    $startDate = $_GET['startDate'];
}
if (isset($_GET['endDate']) && preg_match("/^([\d]{4}-[\d]{2}-[\d]{2})$/", $_GET['endDate'])) {
    $endDate = $_GET['endDate'];
}
$referer = $_ENV['HTTP_REFERER'];
if (isset($_GET['referer'])) {
	$referer = $_GET['referer'];
};

?>
<!DOCTYPE html>
<html>
    <head>
<?php include '../includes/header_tags.php'; ?>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=8">
        <title>Distribution Summary</title>
<?php include '../includes/head_scripts.php'; ?>
        <script type="text/javascript">
$(document).ready(function () {
	$('.datepicker').datepicker({
		showAnim: 'fade',
		numberOfMonths: 1,
		//dateFormat: 'dd/mm/yy',
		dateFormat: 'yy-mm-dd',
		currentText: 'Today'
	});
	//$('.datepicker').change(function () {$(this).parents('form').submit();});
	$('input[name=startDate]').change(function () {
		$('input[name=endDate]').val($('input[name=startDate]').val());
	});
	$('input[name=endDate]').change(function () {
		if ($('input[name=endDate]').val() < $('input[name=startDate]').val()) {
			$('input[name=startDate]').val($('input[name=endDate]').val());
		};
	});
	$('#back').click(function () {
		document.location = $('input[name=referer]').val();
	});
});

        </script>
<style>
body, table td {
	text-align: left;
}
table {
	margin-left: auto;
	margin-right: auto;
}
#back {
	float: left;
}
#table-mk, #table-sg, #table-ib, #table-ky {
	border-collapse: collapse;
	border: 1px solid black;
	width: 700px;
}
table th {
	background-color: silver;
}
#table-mk th, #table-sg th, #table-ib th, #table-mk td, #table-sg td, #table-ib td, #table-ky td, {
	border: 1px solid black;
	min-width: 100px;
	text-align: center;
	padding: 2px;
}
.description, .site {
	text-align: left !important;
}
#table-mk .site {
	background-color: green;
}
#table-sg .site {
	background-color: red;
}
#table-ib .site {
	background-color: blue;
}
#table-ky .site {
    background-color: #800080;
}
.cost_in, .cost_out {
	width: 100px !important;
}
</style>
    </head>
    <body>
<form>
<table>
<tr><td colspan="2">
<br />
	<h1>Distribution Summary</h1>
</td></tr>
<tr><td>
Start date: <input name="startDate" value="<?php echo $startDate; ?>" class="datepicker">
End date: <input name="endDate" value="<?php echo $endDate; ?>" class="datepicker">
<input type='hidden' name='referer' value='<?php echo htmlentities($referer); ?>'>
</td><td>
	<button type="submit" name="action" value="csv">Create Report</button>
<td></tr>
<tr><td colspan="2">&nbsp;<td></tr>
<tr><td style="text-align: right;" colspan="2">
<button id="back" type="button">Back</button>
</td></tr>
<tr><td colspan="2">
<br />
<?php
	$sql = "SELECT 
				SUM(cost_out * mk / 100) as cost_mk, 
				SUM(cost_out * ib / 100) as cost_ib, 
				SUM(cost_out * sg / 100) as cost_sg, 
				SUM(cost_out * ky / 100) as cost_ky,
				SUM(cost_in * mk / 100) as in_mk,
				SUM(cost_in * ib / 100) as in_ib, 
				SUM(cost_in * sg / 100) as in_sg,
				SUM(cost_in * ky / 100) as in_ky,
				description
		FROM (
			(SELECT `out` as cost_out, `in` as cost_in, mk, sg, ib, ky, description FROM banking WHERE `date` >= '$startDate' and `date` <= '$endDate' and site_id = '".CURRENT_SITE_ID."' and cf != 1)
			UNION
			(SELECT `cost` as cost_out, 0 as cost_in, mk, sg, ib, ky, description FROM expenses WHERE `date` >= '$startDate' and `date` <= '$endDate' and site_id = '".CURRENT_SITE_ID."')
		) t
		GROUP BY description;
		
	";
	$res = mysql_query($sql) or die(mysql_error());
	$all_data = array();
	while ($row = mysql_fetch_assoc($res)) {
		$all_data[$row['description']] = $row;
	};
	if (!empty($all_data)) {
		foreach (array('mk', 'sg', 'ib', 'ky') as $dest) {
			$table_header_sent = false;
			foreach ($all_data as $descr => $data) {
				if (!empty($descr) && (!empty($data['cost_' . $dest]) || !empty($data['in_' . $dest]))) {
					if (!$table_header_sent) {
						echo "<table id='table-$dest'>";
						echo "<tr><th class='site' colspan=\"3\">".strtoupper($dest)."</th></tr>";
						echo "<tr><th class='description'>Description</th><th>COST/OUT</th><th>IN</th></tr>";
						$table_header_sent = true;
					};
					echo "
	<tr>
		<td class='description'>{$descr}</td>
		<td class='cost_out'>".(!empty($data['cost_' . $dest]) ? round($data['cost_' . $dest]) : '')."</td>
		<td class='cost_in'>".(!empty($data['in_' . $dest]) ? round($data['in_' . $dest]) : '')."</td>
	</tr>
					";
				};
			};
			if ($table_header_sent) {
				echo "</table><br /><br />";
			};
		};
	};
?>
</td></tr>
</table>
</form>

</body>
</html>
