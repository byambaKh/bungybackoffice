<?php
include '../includes/application_top.php';
include '../Operations/reports_save.php';
include 'checkin_config.php';
include 'functions.php';

if ($user->hasRole('Site Manager')) {
    Header("Location: income.php");
};
$record_type = 'customerregs1';
$current_time = time();
$d = date('Y-m', $current_time);
if (isset($_GET['date'])) {
    $current_time = mktime(0, 0, 0, substr($_GET['date'], 5, 2), substr($_GET['date'], 8, 2), substr($_GET['date'], 0, 4));
} else {
    $current_time = mktime(0, 0, 0, substr($d, 5, 2), substr($d, 8, 2), substr($d, 0, 4));
};
$d = date('Y-m-d', $current_time);
$current_date = $d;
$sql = "SELECT
    *
    from customerregs1
    WHERE site_id = '" . CURRENT_SITE_ID . "' AND BookingDate = '$d' AND ((NoOfJump > 0 AND DeleteStatus = 0 AND Checked = 1)/*regular bookings*/ OR (NoOfJump=0 AND DeleteStatus = 1 AND Checked = 0 AND CancelFeeCollect='Offsite'))/*partially cancelled bookings*/
    ORDER BY BookingTime ASC;
  ";
$res = mysql_query($sql) or (die(mysql_error()));
$all_data = array();
while ($row = mysql_fetch_assoc($res)) {
    $row['CustomerFirstName'] = strtoupper($row['CustomerFirstName']);
    $row['CustomerLastName'] = strtoupper($row['CustomerLastName']);
    $row['RomajiName'] = strtoupper($row['RomajiName']);
    if ($row['RateToPay'] < 0) {
        $row['RateToPay'] = 0;
        mysql_query('UPDATE customerregs1 SET RateToPay = 0 WHERE CustomerRegID = ' . $row['CustomerRegID']);
    };
    $all_data[$row['CustomerRegID']] = $row;
};
mysql_free_result($res);
$sql = "SELECT
      id,
      date_format(sale_time, '%h:%i %p') as sales_time,
      sale_total_qty_photo as 'photo_qty',
      sale_total_photo as 'photo',
      sale_total_qty_2nd_jump as '2ndj_qty',
      sale_total_2nd_jump as '2ndj',
      sale_total_qty_tshirt as tshirt_qty,
      sale_total_tshirt as tshirt,
      sale_total - sale_total_2nd_jump - sale_total_tshirt - sale_total_photo as other,
      sale_total_qty - sale_total_qty_2nd_jump - sale_total_qty_tshirt - sale_total_qty_photo as other_qty
    FROM merchandise_sales
    WHERE 
      site_id = '" . CURRENT_SITE_ID . "'
      AND sale_time like '$d%'
  ";
$res = mysql_query($sql) or (die(mysql_error()));
$merchandise_data = array();
while ($row = mysql_fetch_assoc($res)) {
    $merchandise_data[] = $row;
};
mysql_free_result($res);
if (empty($merchandise_data)) {
    $merchandise_data[] = array(
        '2ndj' => 0,
        'tshirt' => 0,
        'other' => 0
    );
};
$fields = array(
    'BookingTime',
    'NoOfJump',
    'RomajiName',
    'CustomerLastName',
    'CustomerFirstName',
    'ContactNo',
    'CustomerEmail',
    'TransportMode',
    'BookingType',
    'Rate',
    'CollectPay',
    'RateToPay',
    'RateToPayQTY',
    'Notes',
    'Agent',
);
foreach ($add_items as $key => $data) {
    if (in_array($key, array('video', 'gopro'))) continue;
    if ($key != 'other') $fields[] = $key . '_qty';
    $fields[] = $key;
};
$fields = array_merge($fields, array(
    'CancelFeeCollect',
    'CancelFeeQTY',
    'CancelFee'
));

// load drop-downs from file
$values = array();
foreach ($fields as $field) {
    switch ($field) {
        case 'CollectPay';
        case 'CancelFeeCollect';
            $lists = BJHelper::getList('general', 'collectpay');
            $values[$field] = $lists['general']['collectpay'];
            break;
        case 'Rate';
        case 'RateToPay';
            $values[$field] = BJHelper::getRatesList();
            break;
        case 'CancelFee';
            $lists = BJHelper::getList('general', 'cancel_rates');
            $values[$field] = $lists['general']['cancel_rates'];
            break;
        case 'BookingTime';
            $values[$field] = BJHelper::getBookingTimes();
            break;
        case 'NoOfJump';
        case 'RateToPayQTY';
        case 'CancelFeeQTY';
            $lists = BJHelper::getList('general', 'jump_number');
            $values[$field] = $lists['general']['jump_number'];
            break;
        case 'Agent':
            $values[$field] = BJHelper::getAgents();
            break;
        case 'BookingType':
            $values[$field] = BJHelper::getBookingTypeList();
            break;
        case 'TransportMode';
            $lists = BJHelper::getList('general', 'transportmode');
            $values[$field] = $lists['general']['transportmode'];
            break;
    };
};
// EOF load drop downs

$action = $_POST['action'];
if (empty($action)) {
    $action = $_GET['action'];
};
switch (TRUE) {
    case ($action == 'update'):
        $data = $_POST['i'];
        foreach ($data as $id => $row) {
            $row['other_qty'] = $row['other'];
            db_perform($record_type, $row, 'update', 'CustomerRegID=' . $id);
        };
        Header("Location: income_edit.php?date=$current_date");
        break;
    case ($action == 'Add'):
        $data = $_POST['i'];
        $data['CustomerFirstName'] = strtoupper($data['CustomerFirstName']);
        $data['CustomerLastName'] = strtoupper($data['CustomerLastName']);
        $data['RomajiName'] = $data['CustomerLastName'] . ' ' . $data['CustomerFirstName'];
        $data['Checked'] = 1;
        $data['site_id'] = CURRENT_SITE_ID;
        $data['other_qty'] = $data['other'];
        db_perform($record_type, $data);
        Header("Location: income_edit.php?date=$current_date");
        break;
    case ($action == 'Delete'):
        $sql = "delete from customerregs1 where site_id = '" . CURRENT_SITE_ID . "' AND CustomerRegID = '{$_GET['id']}'";
        mysql_query($sql) or die(mysql_error());
        Header("Location: income_edit.php?date=$current_date");
        die();
        break;
    case ($action == 'DeleteSale'):
        $sql = "delete from merchandise_sales where id = '{$_GET['id']}'";
        mysql_query($sql) or die(mysql_error());
        $sql = "delete from merchandise_sales_items where sales_id = '{$_GET['id']}'";
        mysql_query($sql) or die(mysql_error());
        Header("Location: income_edit.php?date=$current_date");
        die();
        break;
};
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="sjis">
    <title>Edit Income for <?php echo $current_date; ?></title>
    <?php include "../includes/head_scripts.php"; ?>
    <script language="javascript" src="/js/tooltip.js"></script>
    <style>
        /* TOOLTIP */
        #tt {
            position: absolute;
            display: block;
            background: url(/img/tt_left.gif) top left no-repeat
        }

        #tttop {
            display: block;
            height: 5px;
            margin-left: 5px;
            background: url(/img/tt_top.gif) top right no-repeat;
            overflow: hidden
        }

        #ttcont {
            display: block;
            padding: 2px 12px 3px 7px;
            margin-left: 5px;
            background: #666;
            color: #FFF
        }

        #ttbot {
            display: block;
            height: 5px;
            margin-left: 5px;
            background: url(/img/tt_bottom.gif) top right no-repeat;
            overflow: hidden
        }

        /* EOF TOOLTIP */
        .vertical-align {
            display: block;
            margin: 0px;
            padding: 0px;
            font-size: 11px;
            font-family: Arial;
            font-weight: bold;
            width: 150px;
            left: -40px;
            position: relative;
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            -o-transform: rotate(90deg);
            transform: rotate(90deg);
        }

        .va2 {
            display: block;
            margin: 0px;
            padding: 0px;
            font-size: 10px;
            font-family: Arial;
            -webkit-transform: rotate(-90deg);
            -moz-transform: rotate(-90deg);
            -ms-transform: rotate(-90deg);
            -o-transform: rotate(-90deg);
            transform: rotate(-90deg);
        }

        .small-header {
            font-size: 10px;
        }

        #income-table {
            border: none;
            background-color: black;
            font-size: 11px;
            font-family: Arial;
            text-align: center;
        }

        #income-table th {
            /*border: 1px solid black;
            border-collapse: collapse;*/
            background-color: silver;;
            color: white;
            overflow: hidden;
            max-width: 60px;
            font-size: 11px;
            font-family: Arial;
        }

        select {
            width: 99%;
            height: 99%;
            max-width: 100px;
            font-size: 10px;
            border: 1px solid gray;
        }

        .BookingType, .CollectPay, .Rate, .Agent {
            min-width: 60px;
        }

        .RateToPayQTY, .CancelFeeCollect, .CancelFee, .CancelFeeQTY {
            min-width: 40px;
        }

        .Notes {
            min-width: 100px;
        }

        #income-table td input {
            width: 99%;
            height: 99%;
            max-width: 100px;
            font-size: 10px;
            border: 1px solid gray;
            text-align: center;
        }

        .even, .even * {
            background-color: white;
        }

        .odd, .odd * {
            background-color: silver;
        }

        #income-table .totals th {
            font-weight: bold;
            color: black;
            background-color: white;
        }

        #income-table .last-month-totals td {
            background-color: #0F0;
        }

        .add-new-record td {
            background-color: silver;
        }
    </style>
    <script>
        $(document).ready(function () {
            $(".delete").click(function () {
                var id = $(this).attr('rel');
                if (window.confirm('Are you sure you want to delete?')) {
                    document.location = 'income_edit.php?action=Delete&id=' + id + '&date=<?php echo $current_date; ?>';
                }
                ;
                return false;
            });
            $(".delete-sale").click(function () {
                var id = $(this).attr('rel');
                if (window.confirm('Are you sure you want to delete this sale?')) {
                    document.location = 'income_edit.php?action=DeleteSale&id=' + id + '&date=<?php echo $current_date; ?>';
                }
                ;
                return false;
            });
        });
    </script>
</head>
<body>
<?php include 'includes/main_menu.php'; ?>
<form method="post" action="?date=<?php echo $current_date; ?>">
    <table id="income-table">
        <tr>
            <th colspan="<?php echo sizeof($fields) + 2; ?>">
                <h1>Income Records for <?php echo $current_date; ?></h1><br/>
                <a href="?date=<?php echo $prev_date = date("Y-m-d", $current_time - 24 * 60 * 60); ?>"
                   style="float: left;">&lt;&lt;&lt; <?php echo $prev_date; ?></a>
                <a href="?date=<?php echo $next_date = date("Y-m-d", $current_time + 24 * 60 * 60); ?>"
                   style="float: right;"><?php echo $next_date; ?> &gt;&gt;&gt;</a>
            </th>
        </tr>
        <tr>
            <th>Book ID</th>
            <th>Booking Time</th>
            <th>Jumps No</th>
            <th>Romaji Name</th>
            <th>Last Name</th>
            <th>First Name</th>
            <th>Contact No</th>
            <th>Email</th>
            <th>Transport</th>
            <th>Type</th>
            <th>Rate</th>
            <th>Collect Pay</th>
            <th>RateToPay</th>
            <th>RTP QTY</th>
            <th>Notes</th>
            <th>Agent</th>
            <?php foreach ($add_items as $key => $item) { ?>
                <?php if (in_array($key, array('video', 'gopro'))) continue; ?>
                <th colspan="<?php echo ($key != 'other') ? 2 : 1; ?>"><?php echo $item['title']; ?></th>
            <?php }; ?>
            <th colspan="3">Cancellation</th>
            <th>Action</th>
        </tr>
        <?php
        $total_fields = array(
            'NoOfJump',
            'Rate',
            'RateToPay',
            'CancelFee',
        );
        foreach ($add_items as $key => $data) {
            if (in_array($key, array('video', 'gopro'))) continue;
            if ($key != 'other') $total_fields[] = $key . '_qty';
            $total_fields[] = $key;
        };
        $totals = array();
        // fill totals array with zeros
        foreach ($total_fields as $field) {
            $totals[$field] = 0;
        };
        if (!empty($merchandise_data)) {
            $i = 1;
            foreach ($merchandise_data as $row) {
                echo "\t<tr class=\"" . (($i % 2) ? 'even' : 'odd') . "\">\n";
                echo "\t\t<td align=\"center\">{$row['id']}</td>\n";
                echo "\t\t<td align=\"center\">{$row['sales_time']}</td>\n";
                echo "\t\t<td colspan=\"14\" align=\"center\">Merchandise sales</td>\n";
                echo "\t\t<td align=\"center\">{$row['photo_qty']}</td>\n";
                echo "\t\t<td align=\"center\">{$row['photo']}</td>\n";
                //echo "\t\t<td align=\"center\" colspan='2'>&nbsp;</td>\n";
                //echo "\t\t<td align=\"center\" colspan='2'>&nbsp;</td>\n";
                echo "\t\t<td align=\"center\">{$row['2ndj_qty']}</td>\n";
                echo "\t\t<td align=\"center\">{$row['2ndj']}</td>\n";
                echo "\t\t<td align=\"center\">{$row['tshirt_qty']}</td>\n";
                echo "\t\t<td align=\"center\">{$row['tshirt']}</td>\n";
                echo "\t\t<td align=\"center\">{$row['other']}</td>\n";
                echo "\t\t<td colspan=\"3\">&nbsp;</td>\n";
                echo '<td><input type="button" value="Delete" rel="' . $row['id'] . '" class="delete-sale"></td>';
                echo "\t</tr>\n";
                // add to totals
                $totals['photos'] += $row['photo'];
                $totals['photos_qty'] += $row['photo_qty'];
                $totals['tshirt'] += $row['tshirt'];
                $totals['tshirt_qty'] += $row['tshirt_qty'];
                $totals['2ndj'] += $row['2ndj'];
                $totals['2ndj_qty'] += $row['2ndj_qty'];
                $totals['other'] += $row['other'];
                $i++;
            };
        };
        foreach ($all_data as $id => $row) {
            echo "\t<tr class=\"" . (($i % 2) ? 'even' : 'odd') . "\">\n";
            echo "\t\t<td>$id</td>\n";
            foreach ($fields as $f) {
                $input_field = "<input name='i[$id][$f]' value='{$row[$f]}' class='$f' onmouseover=\"tooltip.show(this.value);\" onmouseout=\"tooltip.hide();\">";
                if (array_key_exists($f, $values)) {
                    $input_field = draw_pull_down_menu("i[$id][$f]", $values[$f], $row[$f], 'class="' . $f . '" onmouseover="tooltip.show(\'' . $row[$f] . '\');" onmouseout="tooltip.hide();"');
                };
                echo "\t\t<td>$input_field</td>\n";
                if (in_array($f, $total_fields)) {
                    if (!array_key_exists($f, $totals)) {
                        $totals[$f] = 0;
                    };
                    switch (TRUE) {
                        case ($f == 'RateToPay'):
                            $totals[$f] += $row['RateToPay'] * $row['RateToPayQTY'];
                            break;
                        case ($f == 'Rate'):
                            $totals[$f] += $row['Rate'] * $row['NoOfJump'];
                            break;
                        case ($f == 'CancelFee'):
                            $totals[$f] += $row['CancelFee'] * $row['CancelFeeQTY'];
                            break;
                        default:
                            $totals[$f] += $row[$f];
                            break;
                    };
                };
            };
            ?>
            <td><input type="button" value="Delete" rel="<?php echo $id; ?>" class="delete"></td>
            <?php
            echo "\t</tr>\n";
            $i++;
        };
        echo "\t<tr class=\"totals\">\n";
        echo "\t\t<th>Total</th>\n";
        foreach ($fields as $field) {
            $value = '-';
            if (array_key_exists($field, $totals)) {
                $value = $totals[$field];
            };
            echo "\t\t<th>$value</th>\n";
        };
        echo "\t\t<th>&nbsp;</th>\n";
        echo "</tr>";
        ?>
        <tr>
            <td colspan="<?php echo sizeof($fields) + 2; ?>">
                <input type="hidden" name="date" value="<?php echo date('Y-m-d', $current_time); ?>">
                <button name="action" value="update">Update</button>
            </td>
        </tr>
</form>
<tr>
    <th id="add-new" colspan="<?php echo sizeof($fields) + 2; ?>">Add new record</th>
</tr>
<form method="POST">
    <tr class="add-new-record">
        <td>&nbsp;</td>
        <?php
        // New record defaultvalues
        $record = array(
            'NoOfJump' => 1,
            'TransportMode' => 'Car',
            'BookingType' => 'Site',
            'Rate' => '7500',
            'CollectPay' => 'Onsite',
            'Agent' => 'NULL'
        );
        foreach ($fields as $key => $field) {
            $input_field = draw_input_field($field, $field, $record, '', false);
            if (array_key_exists($field, $values)) {
                $input_field = draw_pull_down_menu('i[' . $field . ']', $values[$field], $record[$field], 'id="' . $field . '"');
            };
            echo "\t<td id=\"$field-new\" class=\"$field\">" . $input_field . "</td>\n";
        };
        ?>
        <td id="action-add">
            <input type="submit" name="action" value="Add">
            <input type="hidden" name="i[BookingDate]" value="<?php echo date('Y-m-d', $current_time); ?>">
        </td>
    </tr>
    </table>
</form>
<button onClick="javascript: document.location='/Bookkeeping/income.php?date=<?php echo date('Y-m', $current_time); ?>'"
        id="back" type="button">Back
</button>
<?php include("ticker.php"); ?>
</body>
</html>
