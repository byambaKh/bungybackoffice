
function row_update() {
    var id = $(this).attr('rel');
    var post_data = {};
    post_data.id = id;
    $(this).parent().parent().parent().children("td.value").each(function () {
        var field = $(this).attr('rel');
        post_data[field] = $(this).children('[name=' + field + ']').prop('value');
        if (field == 'shop_name' && $(this).children('[name=' + field + '_new]').prop('value') != '') {
            post_data[field + '_new'] = $(this).children('[name=' + field + '_new]').prop('value');
        }
        ;
        if (field == 'company' && $(this).children('[name=' + field + '_new]').prop('value') != '') {
            post_data[field + '_new'] = $(this).children('[name=' + field + '_new]').prop('value');
        }
        ;
    });
    $.ajax({
        url: 'ajax_handler.php?action=update_row&type=' + _bankingTable,
        method: 'POST',
        data: post_data,
        context: $(this)
    }).done(function () {
        $(this).parent().parent().parent().children("td.value").each(function () {
            var field = $(this).attr('rel');
            if ((field == 'shop_name' || field == 'company') && $(this).children('[name=' + field + '_new]').prop('value') != '') {
                $(this).html($(this).children('[name=' + field + '_new]').prop('value'));
                fvalues[field].push({
                    'id': $(this).html(),
                    'text': $(this).html()
                });
                fvalues[field] = fvalues[field].sort(function (item1, item2) {
                    return (item1.text > item2.text);
                });
            } else {
                $(this).html($(this).children('[name=' + field + ']').prop('value'));
            }
            ;
        });
        $(this).prop('value', 'Change').removeClass('update').addClass('change').unbind('click').click(row_change);
        $(this).parent().children(".cancel").prop('value', 'Delete').removeClass('cancel').addClass('delete').unbind('click').click(row_delete);
        update_balance();
    });
}

function row_delete() {
    var id = $(this).attr('rel');
    if (window.confirm("Are you sure to delete this record?")) {
        //document.location = "?action=Delete&id=" + id;
        var post_data = {"id": id};
        $.ajax({
            url: 'ajax_handler.php?action=delete_row&type=' + _bankingTable,
            method: 'POST',
            data: post_data,
            context: $(this)
        }).done(function () {
            $(this).parent().parent().parent().remove();
            update_balance();
        }).fail(function () {
            alert('Record delete failed.');
        });
    }
    ;
}

function row_cancel() {
    var id = $(this).attr('rel');
    $(this).parent().parent().parent().children("td.value").each(function () {
        var field = $(this).attr('rel');
        $(this).html($(this).children('input[type=hidden]').prop('value'));
    });
    $(this).parent().children(".update").prop('value', 'Change').removeClass('update').addClass('change').unbind('click').click(row_change);
    $(this).prop('value', 'Delete').removeClass('cancel').addClass('delete').unbind('click').click(row_delete);
}

function row_change() {
    var id = $(this).attr('rel');
    $(this).parent().parent().parent().children("td.value").each(function () {
        var input_field = '';
        var field = $(this).attr('rel');
        var value = $(this).html();
        if (typeof field != 'undefined' && fvalues[field].length > 0) {
            input_field = $('<select>').prop('name', field);
            for (fv in fvalues[field]) {
                if (fvalues[field].hasOwnProperty(fv)) {
                    input_field.append(
                        $('<option>').prop('value', fvalues[field][fv].id).text(fvalues[field][fv].text)
                    );
                }
                ;
            }
            ;
            //note we are using replace(/&amp/, "&") because html() converts & to &amp; which prevents it matching
            //the values in fvalues
            input_field.prop('value', $(this).html().replace(/&amp;/, "&"));
        } else {
            input_field = $('<input>').prop('name', field).prop('value', $(this).html().replace(/&amp;/, "&"));
            if (['mk', 'sg', 'ib', 'ky'].indexOf(field) != -1) {
                input_field.css('text-align', 'center');
            }
        }
        ;
        $(this).html(input_field);
        $(this).append(
            $('<input>').prop('type', 'hidden').prop('name', field + '_old').prop('value', value)
        );
        if (field == 'shop_name' || field == 'company') {
            $(this).append($('<br />')).append(
                $('<input>').prop('name', field + '_new')
            );
        }
        ;
    });
    $(this).prop('value', 'Update').removeClass('change').addClass('update').unbind('click').click(row_update);
    $(this).parent().children(".delete").prop('value', 'Cancel').removeClass('delete').addClass('cancel').unbind('click').click(row_cancel);
    $('.date, .date input, input[name=date]').datepicker({
        showAnim: 'fade',
        numberOfMonths: 1,
        dateFormat: 'yy-mm-dd',
        defaultDate: _defaultDate,
        currentText: 'Today',
        maxDate: new Date(_maxDate)
    });

}
;

//calculates balance dynamically
function update_balance() {
    //var balance = 0; //calculated in PHP
    $("#banking-table tr").each(function () {
        var r = {};
        r['in'] = $(this).children('[rel=in]').html();
        r.out = $(this).children('[rel=out]').html();
        r.international_remit = $(this).children('[rel=international_remit]').html();

        if (typeof r['in'] != 'undefined' && r['in'] != '') {
            balance += parseInt(r['in']);
        } ;
        if (typeof r.out != 'undefined' && r.out != '') {
            balance -= parseInt(r.out);
        }
        ;
        if (typeof r.international_remit != 'undefined' && r.international_remit != '') {
            balance -= parseInt(r.international_remit);
        }
        ;
        $(this).children('[rel=balance]').html(balance);
    });
}
;

/*
 * Sets some default values when the "Bank Fees" button is pressed
 */
function input_bank_fees() {
    var dateNow = new Date();

    var year = dateNow.getFullYear();
    var month = dateNow.getMonth() + 1;//returns 0 for jan! why????//need to zero pad
    var date = dateNow.getDate();//need to zero pad

    var dateNowString = year + '-' + month + '-' + date;

    $('#date').val(dateNowString);
    $('#notes').val('Bank Charges');
    $('[name="i[company]"]').val('Yucho Bank');
    $('[name="i[description]"]').val('Bank Charges');
    $('[name="i[analysis]"]').val('Misc. Bank Fees');
    $('[name="i[contax]"]').val('YES');
    $('[name="i[who]"]').val('Dave');
}


$(document).ready(function () {
    $('.date, .date input').datepicker({
        showAnim: 'fade',
        numberOfMonths: 1,
        dateFormat: 'yy-mm-dd',
        defaultDate: _defaultDate,
        currentText: 'Today',
        maxDate: new Date(_maxDate)
    });
    $(".delete").click(row_delete);
    $('.change').unbind('click').click(row_change);
    update_balance();

    window.ac_values = [];
    for (i in fvalues.company) {
        if (fvalues.company.hasOwnProperty(i)) {
            window.ac_values.push(fvalues.company[i].text);
        }
    }

    $("input#company-new").autocomplete({
        disabled: false,
        minLength: 1,
        source: window.ac_values
    });
});
/*
 * Sets some default values when the Sales button is pressed
 */
function show_sales_calendar() {
    var d = $('#notes').datepicker({
        showAnim: 'fade',
        numberOfMonths: 1,
        dateFormat: 'yy-mm-dd',
        defaultDate: _defaultDate,
        currentText: 'Today',
        maxDate: new Date(_maxDate),
        onSelect: function (dstring) {
            $("#company-new select").val("BUNGY JAPAN");
            $("#who-new select").val("Dave");
            $("#description-new select").val("SALES DEPOSIT");
            $("#analysis-new select").val("Sales Deposit - Daily");
            $(this).val('Sales ' + /-([\d]{2})-/.exec(dstring)[1] + '/' + /-([\d]{2})$/.exec(dstring)[1]);
            $(this).datepicker('destroy');
            $(this).focus();
        }
    });
    d.datepicker('show');
}
