<?
include '../includes/application_top.php';

$cDate = date("Y-m-d");

$user = new BJUser();
if (!$user->hasRole(array("SysAdmin", "General Manager", "Site Manager", "Price Waterhouse Cooper"))) {
    header("Location: ../index.php");
}
?>
<!DOCTYPE html>
<html>
<head>
    <?php include 'includes/header_tags.php'; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=8">

    <?php include '../includes/head_scripts.php'; ?>
    <title>Bookkeeping</title>
    <script type="text/javascript" charset="utf-8">
        // Buttons actions
        $(document).ready(function () {
            $('#analysis').on('click', function () {
                document.location = '/Analysis/';
            });
            $('#inventory-transfers').on('click', function () {
                document.location = 'inventory_transfers.php';
            });
            $('#expenses').on('click', function () {
                document.location = 'expenses.php';
            });
            $('#banking').click(function () {
                document.location = 'banking.php';
            });
            $('#income').click(function () {
                document.location = 'income.php';
            });
            $('#payroll').click(function () {
                document.location = 'payroll.php';
            });
            $('#foc-jumps').click(function () {
                document.location = 'foc_jumps.php';
            });
            $('#agent').click(function () {
                document.location = 'agent.php';
            });
            $('#oDate').datepicker({
                showAnim: 'fade',
                numberOfMonths: 1,
                //dateFormat: 'dd/mm/yy',
                dateFormat: 'yy-mm-dd',
                currentText: 'Today',
                altField: '#alternate',
                altFormat: 'yy-mm-dd',
                minDate: '0d',
                maxDate: new Date(<?php echo $maxDate?>),
                onSelect: function (d, inst) {
                    document.location = '/Operations/operationActivities.php?oDate=' + encodeURIComponent(d);
                }
            });
        });
        function procBack() {
            document.location = '/';
            return;
        }
    </script>
    <style>
    </style>
</head>
<body>
<?php include '../includes/main_menu.php'; ?>
<br>
<?php
if (array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])) {
    echo "<div id='info_message'>{$_SESSION['message']}</div>";
    unset($_SESSION['message']);
};
?>
<form method="post" name="lsform" onsubmit="return OnSubmitForm();">

    <table border="0" align="center" width="420px">
        <tbody>
        <tr>
        </tr>
        <tr>
        </tr>
        <tr align="center" bgcolor="#000000">
            <td colspan="2"><font color="#FFFFFF"><b>Please select the activity from below:</b></font></td>
        </tr>


        <?php $user = new BJUser() ?>
        <tr bgcolor="#FF0000">
            <td align="center">
                <input style="WIDTH: 150px; HEIGHT: 25px; background-color:#FFFF66;border-style:ridge;border-color:#000000;" name="expenses" id="expenses" value="Expenses" type="button">
            </td>
        </tr>
        <tr bgcolor="#FF0000">
            <td align="center">
                <input style="WIDTH: 150px; HEIGHT: 25px; background-color:#FFFF66;border-style:ridge;border-color:#000000;" name="income" id="income" value="Income" type="button">
            </td>
        </tr>
        <tr bgcolor="#FF0000">
            <td align="center">
                <input style="WIDTH: 150px; HEIGHT: 25px; background-color:#FFFF66;border-style:ridge;border-color:#000000;" name="payroll" id="payroll" value="Payroll" type="button">
            </td>
        </tr>

        <?php if (!$user->hasRole(array("Site Manager"))): ?>
            <tr bgcolor="#FF0000">
                <td align="center">
                    <input style="WIDTH: 150px; HEIGHT: 25px; background-color:#FFFF66;border-style:ridge;border-color:#000000;" name="banking" id="banking" value="Banking" type="button">
                </td>
            </tr>
            <tr bgcolor="#FF0000">
                <td align="center">
                    <input style="WIDTH: 150px; HEIGHT: 25px; background-color:#FFFF66;border-style:ridge;border-color:#000000;" name="agent" id="agent" value="Agent" type="button">
                </td>
            </tr>
            <tr bgcolor="#FF0000">
                <td align="center">
                    <input style="WIDTH: 150px; HEIGHT: 25px; background-color:#FFFF66;border-style:ridge;border-color:#000000;" name="analysis" id="analysis" value="Analysis" type="button">
                </td>
            </tr>
            <tr bgcolor="#FF0000">
                <td align="center">
                    <input style="WIDTH: 150px; HEIGHT: 25px; background-color:#FFFF66;border-style:ridge;border-color:#000000;" name="foc_jumps" id="foc-jumps" value="FOC Jumps" type="button">
                </td>
            </tr>
            <tr bgcolor="#FF0000">
                <td align="center">
                    <input style="WIDTH: 150px; HEIGHT: 25px; background-color:#FFFF66;border-style:ridge;border-color:#000000;" name="inventory_transfers" id="inventory-transfers" value="Inventory Transfers" type="button">
                </td>
            </tr>
        <?php endif; ?>
        <tr bgcolor="#000000">
            <td align="center" colspan="2">
                <input style="WIDTH: 80px; HEIGHT: 25px; background-color:#FFFF66;border-style:ridge;border-color:#FF0000;" id="back" value="Back" onclick="procBack();" type="button">
            </td>
        </tr>

        </tbody>
    </table>

</form>
<?php
include("ticker.php");
?>
