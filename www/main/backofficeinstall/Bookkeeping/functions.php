<?php
    function getTotalWork($in, $out) {
        list($in_h, $in_m) = explode(':', $in);
        list($out_h, $out_m) = explode(':', $out);
        $diff = $out_h - $in_h + ($out_m - $in_m) / 60;
        if ($diff >= 9) {
            $diff -= 0.25;
        };
        $diff -= 0.75;
        if ($in == $out) $diff = 0;
        return number_format($diff, 2);
    }

	function getValueFromQuery($sql, $valueName){
		$queryResults = queryForRows($sql);
		$value = 0;
		if(isset($queryResults[0])){
			$value = $queryResults[0][$valueName];
            if($value == NULL) $value = 0;
		}

		return $value;
	}
?>
