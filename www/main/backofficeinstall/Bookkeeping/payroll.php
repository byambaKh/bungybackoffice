<?php
	include '../includes/application_top.php';
	include '../Operations/reports_save.php';
	include 'functions.php';

    if ($user->hasRole('Staff +') && $_SESSION['payroll_password'] != $config['payroll_password']) {
        auth_user();
        die();
    };

	$current_time = time();
	$d = date('Y-m', $current_time);
	if (isset($_GET['date'])) {
		$current_time = mktime(0, 0, 0, substr($_GET['date'], 5, 2), 15, substr($_GET['date'], 0, 4));
	} else {
		$current_time = mktime(0, 0, 0, substr($d, 5, 2), 15, substr($d, 0, 4));
	};
	$record_type = 'payroll';

	$headers = array(
		"Day",
		"STAFF",
		"IN",
		"OUT",
		"Total Work",
	);
	$fields = array_map(function ($item) {
		if ($item == 'IN') $item = 'Start';
		if ($item == 'OUT') $item = 'Finish';
		return strtolower(
			preg_replace(
				"/(_)$/",
				"",
				preg_replace("([^\w]+)", "_", $item)
			)
		);
	}, $headers);
    foreach ($fields as $field) {
        $group = 'payroll';
        $needed_field = $field;
        $list = BJHelper::getList($group, $needed_field);
        if (!empty($list)) {
            $values[$field] = $list[$group][$needed_field];
        };
        if ($field == 'staff') {
            $values[$field] = BJHelper::getStaffList('StaffListName');
        };
    };

	// check if needed table exists
	
	$action = $_POST['action'];
	if (empty($action)) {
		$action = $_GET['action'];
	};
	switch (TRUE) {
		case ($action == 'Add'):
			$_POST['i']['date'] = date("Y-m-", $current_time) . $_POST['i']['day'];
			unset($_POST['i']['day']);
			$_POST['i']['site_id'] = CURRENT_SITE_ID;
			save_report($record_type, $_POST['i']);
			Header("Location: $record_type.php?date=" . date("Y-m", $current_time));
			die();
			break;
		case ($action == 'Update'):
			$_POST['i']['date'] = date("Y-m-", $current_time) . $_POST['i']['day'];
			unset($_POST['i']['day']);
			save_report($record_type, $_POST['i']);
			Header("Location: $record_type.php");
			die();
			break;
		case ($action == 'Delete'):
			$sql = "delete from $record_type where site_id = '".CURRENT_SITE_ID."' AND id = '{$_GET['id']}'";
			mysql_query($sql) or die(mysql_error());
			Header("Location: $record_type.php");
			die();
			break;
		case ($action == 'CSV'):
    		$d = date('Y-m', $current_time);
    		$sql = "select * from $record_type WHERE site_id = '".CURRENT_SITE_ID."' AND `date` like '$d%' order by `date` ASC;";
			Header('Content-Type: text/csv; name="'.$record_type.$d.'.csv"');
			Header('Content-Disposition: attachment; filename="'.$record_type.$d.'.csv"');
    		$res = mysql_query($sql);
			echo '"' . implode('","', $headers) . '"' . "\r\n";
			$balance = 0;
    		while ($row = mysql_fetch_assoc($res)) {
				$row['total_work'] = getTotalWork($row['start'], $row['finish']);
				$row['day'] = preg_replace("/([\d]{4}-[\d]{2}-[0]?)/", "", $row['date']);
				foreach ($fields as $field) {
					echo '"' . $row[$field] . '"' . ($field == $fields[sizeof($fields)-1] ? '' : ',');
				};
				echo "\r\n";
			};
			die();
			break;
		case ($action == 'remove-all'):
		case ($action == 'Delete All'):
			$sql = "delete from $record_type where site_id = '".CURRENT_SITE_ID."' AND date like '{$_GET['date']}-%'";
			mysql_query($sql) or die(mysql_error());
			Header("Location: $record_type.php?date=" . $_GET['date']);
			die();
			break;
		case ($action == 'upload'):
			if (isset($_FILES['csv']) && is_array($_FILES['csv']) && $_FILES['csv']['error'] == 0) {
				$filename = date("Y-m") . "_upload.csv";
				if (move_uploaded_file($_FILES['csv']['tmp_name'], 'temp/' . $filename)) {
					// process uploaded file
					$filename = 'temp/' . $filename;
					$fp = fopen($filename, "r");
					$hs = fgetcsv($fp);
					$map = array();
					foreach ($headers as $key => $header) {
						foreach ($hs as $key2 => $h) {
							if ($h == $header) {
								$map[$fields[$key]] = $key2;
							};
						};
					};
					//$d = date("Y-m-", $current_time);
					//$sql = "delete from $record_type WHERE `date` like '$d%';";
					//mysql_query($sql);
					if (!empty($map)) {
						while ($row = fgetcsv($fp)) {
							$db_row = array();
							foreach ($map as $field => $value) {
								if ($field != 'day') {
									$db_row[$field] = $row[$value];
								} else {
									$db_row['date'] = date("Y-m", $current_time). '-' . $row[$value]; 
								};
							};
							$db_row['site_id'] = CURRENT_SITE_ID;
							save_report($record_type, $db_row);
						};
						fclose($fp);
					} else {
						fclose($fp);
						// Most possible time TXT file uploaded
						$fp = fopen($filename, "r");
						$slots = array();
						while ($row = fgetcsv($fp, 1024, "\t")) {
							if (sizeof($row) != 7) continue;
							$user = $row[4];
							$time = date_parse($row[1]);
							$diff = ($time['minute'] % 15);
							$time['minute'] -= $diff;
							if ($diff > 7) {
								$time['minute'] += 15;
							};
							$time = mktime($time['hour'], $time['minute'], $time['second'], $time['month'], $time['day'], $time['year']);
							$slots[date("Y-m-d", $time)][$user][] = $time;
						};
						fclose($fp);
						foreach ($slots as $slot_date => $slot) {
							foreach ($slot as $user => $record) {
								sort($record);
								$db_row = array(
									'start'		=> date("H:i", $record[0]),
									'finish'	=> date("H:i", $record[sizeof($record)-1]),
									'staff' 	=> $user,
									'date'		=> $slot_date,
									'site_id'	=> CURRENT_SITE_ID
								);
								save_report($record_type, $db_row);
							};
						};
					};
				};
			}
			Header("Location: $record_type.php?date=" . date("Y-m", $current_time));
			die();
	};
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Payroll</title>
<?php include "../includes/head_scripts.php"; ?>
<style>
h1 {
	margin: 0;
	background-color: yellow;
	text-align: center;
	width: 100%;
}
#payroll-table {
	background-color: black;
}
#payroll-table td {
	background-color: white;
}
#payroll-table .row-error td {
	background-color: red;
}
#payroll-table th {
	background-color: black;
	color: white !important;
}
#payroll-table th#out-header {
	background-color: red;
	color: white;
}
#payroll-table th#in-header {
	background-color: #00ff00;
	color: white;
}
#payroll-table td {
	vertical-align: top;
	text-align: center;
}
#payroll-header td {
	background-color: yellow;
}
#monthly-totals {
	float: right;
}
</style>
</head>
<body>
<?php include 'includes/main_menu.php'; ?>
<script>
	var current_date = "<?php echo date("Y-m", $current_time); ?>";

	function row_update() {
		var id = $(this).attr('rel');
		var post_data = {};
		post_data.id = id;
		$(this).parent().parent().parent().children("td.value").each(function () {
			var field = $(this).attr('rel');
			post_data[field] = $(this).children('[name='+field+']').prop('value');
		});
		if (post_data['start'] != post_data['finish']) {
			$(this).parent().parent().parent().removeClass('row-error');
		} else {
			$(this).parent().parent().parent().addClass('row-error');
		};
		$.ajax({
			url: 'ajax_handler.php?action=update_row&type=<?php echo $record_type; ?>&date=' + current_date,
			method: 'POST', 
			data: post_data,
			context: $(this)
		}).done(function () {
			$(this).parent().parent().parent().children("td.value").each(function () {
				var field = $(this).attr('rel');
				$(this).html($(this).children('[name='+field+']').prop('value'));
			});
// update total work
			try {
				var st = $(this).parent().parent().parent().children("td.start").html().split(':');
				var fi = $(this).parent().parent().parent().children("td.finish").html().split(':');
				var diff = parseInt(fi[0],10) - parseInt(st[0],10) + (parseInt(fi[1],10) - parseInt(st[1],10)) / 60;
				if (diff >= 9) {
					diff -= 0.25;
				};
				diff -= 0.75;
				$(this).parent().parent().parent().children("td.total_work").html(diff.toFixed(2));
			} catch (e) {
				$(this).parent().parent().parent().children("td.total_work").html('N/A');
			}
// EOF total work
			$(this).prop('value', 'Change').removeClass('update').addClass('change').unbind('click').click(row_change);
			$(this).parent().children(".cancel").prop('value', 'Delete').removeClass('cancel').addClass('delete').unbind('click').click(row_delete);
		});
	}
	function row_delete() {
		var id = $(this).attr('rel');
		if (window.confirm("Are you sure to delete this record?")) {
			//document.location = "?action=Delete&id=" + id;
			var post_data = {"id":id};
			$.ajax({
				url: 'ajax_handler.php?action=delete_row&type=<?php echo $record_type; ?>&date=' + current_date,
				method: 'POST', 
				data: post_data,
				context: $(this)
			}).done(function () {
				$(this).parent().parent().parent().remove();
			}).fail(function () {
				alert('Record delete failed.');
			});
		};
	}
	function row_cancel() {
		var id = $(this).attr('rel');
		$(this).parent().parent().parent().children("td.value").each(function () {
			var field = $(this).attr('rel');
			$(this).html($(this).children('input[type=hidden]').prop('value'));
		});
		$(this).parent().children(".update").prop('value', 'Change').removeClass('update').addClass('change').unbind('click').click(row_change);
		$(this).prop('value', 'Delete').removeClass('camcel').addClass('delete').unbind('click').click(row_delete);
	}
	function row_change() {
		var id = $(this).attr('rel');
		$(this).parent().parent().parent().children("td.value").each(function () {
			var input_field = '';
			var field = $(this).attr('rel');
			var value = $(this).html();
			if (typeof field != 'undefined' && fvalues[field].length > 0) {
				input_field = $('<select>').prop('name', field);
				for (fv in fvalues[field]) {
					if (fvalues[field].hasOwnProperty(fv)) {
						input_field.append(
							$('<option>').prop('value', fvalues[field][fv].id).text(fvalues[field][fv].text)
						);
					};	
				};
				input_field.prop('value', $(this).html());
			} else {
				input_field = $('<input>').prop('name', field).prop('value', $(this).html());
			};
			$(this).html(input_field);
			$(this).append(
				$('<input>').prop('type', 'hidden').prop('name', field + '_old').prop('value', value)
			);
		});
		$(this).prop('value', 'Update').removeClass('change').addClass('update').unbind('click').click(row_update);
		$(this).parent().children(".delete").prop('value', 'Cancel').removeClass('delete').addClass('cancel').unbind('click').click(row_cancel);
    	$('.date, .date input, input[name=date]').datepicker({
                showAnim: 'fade',
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd',
                currentText: 'Today',
                maxDate: new Date(<?php echo $maxDate?>)
    	});

	};
$(document).ready(function () {
	$('.date, .date input').datepicker({
                showAnim: 'fade',
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd',
                currentText: 'Today',
                maxDate: new Date(<?php echo $maxDate?>)
	});
	$(".delete").click(row_delete);
	$(".remove-all").click(function () {
		return window.confirm('Are you sure to delete all this month records?');
	});
	$('.change').unbind('click').click(row_change);
	$('#monthly-totals').click(function () {
        var monthly_totals_window = window.open('payroll_totals.php?date=<?php echo date("Y-m", $current_time); ?>', 'monthly_totals_window', 'height=500,width=900,location=0,menubar=0,status=0,toolbar=0,scrollbars=1');
        monthly_totals_window.focus();
	});
});
	var fvalues = {};
<?php
	foreach ($fields as $field) {
		echo "\tfvalues.$field = " . json_encode(array_key_exists($field, $values) ? $values[$field] : array()) . ";\n";
	};
?>
</script>
<table id="payroll-table" align="center">
<tr>
	<td colspan="<?php echo sizeof($fields) + 1; ?>">
		<table border="0" cellspacing="0" cellpadding="0" width="100%" id="payroll-header">
			<tr>
				<td colspan="2"><h1> Payroll </h1></td>
			</tr>
			<tr>
				<td colspan="2"><h2><?php echo strtoupper(date("F Y", $current_time)); ?></h2></td>
			</tr>
			<tr>
				<td style="text-align: left;"><a href="?date=<?php echo date("Y-m", $current_time - 30*24*60*60); ?>">&lt;&lt;&lt;&lt;&lt;&lt; Prev</a></td>
				<td style="text-align: right;"><a href="?date=<?php echo date("Y-m", $current_time + 30*24*60*60); ?>">Next &gt;&gt;&gt;&gt;&gt;&gt;</a></td>
			</tr>
		</table>
	</td>
<tr>
<?php
	foreach ($fields as $key => $field) {
		echo "\t<th id=\"$field-header\">{$headers[$key]}</th>\n";
	};
?>
		<th id="actions-header">Actions</th>
</tr>
<?php
	$d = date("Y-m-", $current_time);
	$sql = "select * from $record_type WHERE site_id = '".CURRENT_SITE_ID."' AND `date` like '$d%' order by `date` ASC;";
	if ($res = mysql_query($sql)) {
		while ($row = mysql_fetch_assoc($res)) {
			$row['day'] = preg_replace("/([\d]{4}-[\d]{2}-[0]?)/", "", $row['date']);
?>
<tr<?php echo ($row['start'] == $row['finish'])?' class="row-error"':''; ?>>
<?php
			foreach ($fields as $key => $field) {
				if ($field == 'total_work') $row[$field] = getTotalWork($row['start'], $row['finish']);
				echo "\t<td id=\"$field-{$row['id']}\" class=\"" . (($field == 'total_work') ? '' : 'value') . " $field\" rel=\"$field\">{$row[$field]}</td>\n";
			};
?>
		<td id="action-change-delete"><nobr><input type="button" value="Change" rel="<?php echo $row['id']; ?>" class="change"><input type="button" value="Delete" rel="<?php echo $row['id']; ?>" class="delete"></nobr></td>
</tr>
<?php
		};
	} else {
?>
<tr>
	<td id="no-record-exists" colspan="<?php echo sizeof($fields) + 1; ?>">No data available</th>
</tr>
<?php
	};
?>
<tr>
	<th id="add-new" colspan="<?php echo sizeof($fields) + 1; ?>"><form method="POST" style="display: inline-block;"><input type=hidden name=date value="<?php echo date("Y-m", $current_time); ?>"><button name='action' value="remove-all" class='remove-all' type='submit'>Delete All</button></th>
</tr>
<form method="POST">
<tr>
<?php
	foreach ($fields as $key => $field) {
		$record = array();
		$input_field = draw_input_field($field, $field, $record, '', false);
		if (array_key_exists($field, $values)) {
			$input_field = draw_pull_down_menu('i[' . $field . ']', $values[$field]);
		};
		if ($field == 'total_work') $input = '';
		echo "\t<td id=\"$field-new\" class=\"$field\">" . $input_field . "</td>\n";
	};
?>
	<td id="action-add"><input type="submit" name="action" value="Add"></td>
</tr>
</form>
<tr>
	<th id="csv-header" colspan="<?php echo sizeof($fields) + 1; ?>">CSV Functions</th>
</tr>
<tr>
	<td id="csv-td" colspan="<?php echo sizeof($fields) + 1; ?>">
		<table width="100%" cellspacing="0" cellpadding="0" border="0">
			<tr>
				<td style="text-align: left;">
<form action="?date=<?php echo date("Y-m", $current_time); ?>&action=upload" enctype="multipart/form-data" method="POST">
	<input type=file name="csv"><br />
	<button name='action' value="upload">Upload</button>
</form>
				</td>
				<td style="text-align: right;">
<form>
	<input type=hidden name=date value="<?php echo date("Y-m", $current_time); ?>">
	<button name='action' value="CSV" style="float: right;">Download</button><br clear="both"/>
	<button id="monthly-totals">Monthly Totals</button>
</form>
				</td>
			</tr>
		</table>
<?php
	$user = new BJUser();
	$back_location = '/Bookkeeping/';
	if ($user->hasRole('Staff +')) {
		$back_location = '/';
	};
?>
<button id="back" onClick="javascript: document.location='<?php echo $back_location; ?>'">Back</button>
	</td>
</tr>
</table>
<?php include ("ticker.php"); ?>
</body>
</html>
