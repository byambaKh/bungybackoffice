<?php
	include '../includes/application_top.php';
	include '../Operations/reports_save.php';
	include 'functions.php';

    $user = new BJUser();
    if(!$user->hasRole(array("SysAdmin", "General Manager", "Financial"))){
        header("Location: ../index.php");
    }

	$record_type = 'income';
    $current_time = time();
    $d = date('Y-m', $current_time);
    if (isset($_GET['date'])) {
        $current_time = mktime(0, 0, 0, substr($_GET['date'], 5, 2), 15, substr($_GET['date'], 0, 4));
    } else {
        $current_time = mktime(0, 0, 0, substr($d, 5, 2), 15, substr($d, 0, 4));
    };
    $d = date('Y-m', $current_time);
	$sql = "SELECT DISTINCT Agent FROM customerregs1 
		WHERE 
			site_id = '".CURRENT_SITE_ID."'
			AND BookingDate like '$d-%' and 
		  #Agent <> 'NULL' AND
			(CollectPay = 'Offsite' OR (CollectPay = 'OnSite' AND RateToPay > 0 AND RateToPayQTY > 0)) AND
			NoOfJump > 0 AND 
			DeleteStatus = 0 AND 
			Checked = 1
			ORDER BY Agent ASC;";
	$res = mysql_query($sql) or die(mysql_error());
	$agents = array();
	$csv_agents = array();
	while ($row = mysql_fetch_assoc($res)) {
		$agents[] = $row['Agent'];
		$csv_agents[] = $row['Agent'] . ' to Collect';
		$csv_agents[] = $row['Agent'] . ' to Pay';
	};
	mysql_free_result($res);
// SELECTING present OffSite Rates
    $sql = "
SELECT distinct Rate
FROM customerregs1
WHERE 
	site_id = '".CURRENT_SITE_ID."'
	AND BookingDate like '$d-%' 
	and NoOfJump > 0 
	and DeleteStatus = 0 
	and Checked = 1 and 
	CollectPay = 'Offsite'
ORDER BY Rate DESC;
    ";
    $offsite_rates = array();
    $res = mysql_query($sql) or die(mysql_error());
    while($row = mysql_fetch_assoc($res)) {
        if ($row['Rate'] == 0) continue;
        $offsite_rates[] = $row['Rate'];
    };
    mysql_free_result($res);
    if (empty($offsite_rates)) {
        $offsite_rates[] = $config['first_jump_rate'];
    };

	$all_data = array();
	if (!empty($agents)) {
		$sql = "SELECT 
			cr.BookingDate,
/* A Section */
		sum(RateToPay*RateToPayQTY) as to_pay,
/* C Section */ ";
		foreach ($offsite_rates as $orate) {
			$sql .= "\n\t\tsum(IF(Rate = $orate AND CollectPay = 'Offsite', NoOfJump, 0)) as c$orate, ";
		};
		$sql .= "\n\t\tSUM(IF(CancelFeeCollect = 'Offsite', CancelFee * CancelFeeQTY, 0)) as c_cancel,
		SUM(IF(CollectPay = 'Offsite', NoOfJump * Rate, 0)+If(CancelFeeCollect = 'Offsite', CancelFee* CancelFeeQTY, 0)) as c_total";
		foreach ($agents as $agent) {
			$sql .= ",\n\t\tsum(IF(Agent = '$agent' AND CollectPay = 'Offsite', NoOfJump * Rate, 0)+If(Agent = '$agent' AND CancelFeeCollect = 'Offsite', CancelFee* CancelFeeQTY, 0)) as '$agent to Collect'";
			$sql .= ",\n\t\tsum(IF(Agent = '$agent' AND CollectPay = 'OnSite', RateToPay * RateToPayQTY, 0)) as '$agent to Pay'";
		};
		$sql .= "
		from customerregs1 as cr
		WHERE 
			site_id = '".CURRENT_SITE_ID."'
			AND BookingDate like '$d-%' 
			and NoOfJump > 0 
			and DeleteStatus = 0 
			and Checked = 1 
			#and Agent <> 'NULL'
		GROUP BY BookingDate
		ORDER BY BookingDate ASC;
	";
		$res = mysql_query($sql) or (die(mysql_error()));
		while ($row = mysql_fetch_assoc($res)) {
			$all_data[$row['BookingDate']] = $row;
		};
		mysql_free_result($res);
	};
	// income records
	$action = $_POST['action'];
	if (empty($action)) {
		$action = $_GET['action'];
	};
	switch (TRUE) {
		case ($action == 'CSV'):
			$current_year = date("Y", $current_time);
			Header('Content-Type: text/csv; name="agent-'.$d.'.csv"');
			Header('Content-Disposition: attachment; filename="agent-'.$d.'.csv"');
			$headers = array(
				'Date',
				'To Pay',
			);
			foreach ($offsite_rates as $orate) {
				array_push($headers, $orate);
			};
			$headers = array_merge($headers, array(
				'Offsite total'
			));
			echo '"' . implode('","', array_merge($headers, $csv_agents)) . '"' . "\r\n";
			foreach ($all_data as $current_date => $record) {
				echo '"' . implode('","', $record) . '"' . "\r\n";
			};
			die();
			break;
	};
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Agents</title>
<?php include "../includes/head_scripts.php"; ?>
<style>
.vertical-align {
	display: block;
	margin: 0px;
	padding: 0px;
	font-size: 11px;
	font-family: Arial;
	font-weight: bold;
	width: 150px;
	left: -35px;
	position: relative;
-webkit-transform: rotate(-90deg);	
-moz-transform: rotate(-90deg);
-ms-transform: rotate(-90deg);
-o-transform: rotate(-90deg);
transform: rotate(-90deg);
}
.va2 {
	display: block;
	margin: 0px;
	padding: 0px;
	font-size: 10px;
	font-family: Arial;
-webkit-transform: rotate(-90deg);	
-moz-transform: rotate(-90deg);
-ms-transform: rotate(-90deg);
-o-transform: rotate(-90deg);
transform: rotate(-90deg);
}
.small-header {
	font-size: 10px;
}
#income-table {
	border:none;
	background-color: black;
	font-size: 11px;
	font-family: Arial;
	text-align: center;
}
#income-table th {
	/*border: 1px solid black;
	border-collapse: collapse;*/
	background-color: silver;;
	color: white;
	overflow: hidden;
	max-width: 60px;
	font-size: 11px;
	font-family: Arial;
	height: 50px;
}
#income-table td input {
	width: 99%;
	height: 99%;
	font-size: 10px;
	border: 1px solid gray;
	text-align: center;
}
.even, .even * {
	background-color: white;
}
.odd, .odd * {
	background-color: silver;
}
#income-table .totals th {
	font-weight: bold;
	color: black;
	background-color: white;
	height: 30px;
}
#income-table .last-month-totals td {
	background-color: #0F0;
}
#income-table .agent-header th {
	background-color: magenta;
	font-size: 18px;
	padding: 5px;
	line-height: 20px;
	height: 20px;
}
.collect, .topay {
	min-width: 40px;
	max-width: 45px;
}
.agent-invoice-header {
	cursor: pointer;
}
.collect {
	background-color: #9f9 !important;
}
.topay {
	background-color: #f99 !important;
}
.odd .collect {
	background-color: #6c6 !important;
}
.odd .topay {
	background-color: #c66 !important;
}
</style>
</head>
<body>
<?php include '../includes/main_menu.php'; ?>
<form method="post">
<table id="income-table">
<tr class="agent-header">
	<th colspan="<?php echo (sizeof($agents) * 2 + sizeof($offsite_rates) + 5); ?>">Agent</th>
</tr>
<tr class="agent-header">
	<th colspan="<?php echo (sizeof($agents) * 2 + sizeof($offsite_rates) + 5); ?>">
		<?php echo date("F Y", $current_time); ?>
		<table border="0" width="100%">
		<tr class="agent-header">
			<th align="left"><a href="?date=<?php echo date("Y-m", $current_time - 30*24*60*60); ?>" style="float: left;">&lt;&lt;&lt; Prev</a></th>
			<th align="right"><a href="?date=<?php echo date("Y-m", $current_time + 30*24*60*60); ?>" style="float: right;">Next &gt;&gt;&gt;</a></th>
		</tr>
		</table>
	</th>
</tr>
<tr>
	<th colspan="2" rowspan="3">&nbsp;</th>
	<th rowspan="2" style="height: 100px;">A<br>ONSITE</th>	
	<th colspan="<?php echo sizeof($offsite_rates) + 2; ?>" rowspan="2" style="height: 100px;">C<br>OFFSITE</th>	
<?php foreach ($agents as $agent) { ?>
	<th rowspan="2" class="agent-invoice-header" colspan="2" onClick="javascript: document.location='../AgentInvoice/agent_invoice.php?date=<?php echo date("Y-m", $current_time); ?>&agent=<?php echo rawurlencode($agent); ?>';"><span class="vertical-align"><?php echo $agent; ?></span></th>
<?php }; ?>
</tr>
<tr></tr>
<tr>
	<th><span class="va2">To Pay</span></th>
<!-- OFFSITE -->
	<?php foreach ($offsite_rates as $orate) { ?>
	<th><span class="va2"><?php echo $orate; ?></span></th>
	<?php }; ?>
	<th><span class="va2">Cancel</span></th>
	<th><b>Offsite<br />Total</b><br /><span class="small-header">(to collect)</span></th>
<!-- A+B -->
<?php foreach ($agents as $agent) { ?>
	<th class="collect">to<br />Collect</th>
	<th class="topay">to<br />Pay</th>
<?php }; ?>
</tr>
<?php
	$month_days = date('t', $current_time);
	$fields = array(
		'to_pay',
	);
	foreach ($offsite_rates as $orate) {
		array_push($fields, 'c' . $orate);
	};
	$fields = array_merge($fields, array(
		'c_cancel',
		'c_total'
	));
	$fields = array_merge($fields, $csv_agents);
	$totals = array();
	for ($day = 1; $day <= $month_days; $day++) {
		$current_date = date("Y-m-", $current_time) . sprintf("%02d", $day);
		echo "\t<tr class=\"".(($day % 2) ? 'even' : 'odd')."\">\n";
		echo "\t\t<td>$day</td>\n";
		if (!array_key_exists($current_date, $all_data) || !array_key_exists('id', $all_data[$current_date])) {
			$all_data[$current_date]['id'] = 0;
		};
		echo "\t\t<td>".date('D', mktime(0,0,0,date('m', $current_time), $day, date('Y', $current_time)))."<input type=\"hidden\" name=\"id[$day]\" value=\"{$all_data[$current_date]['id']}\"></td>\n";
		//echo $current_date;
		foreach ($fields as $field) {
			$style= '';
			if (preg_match('/total/is', $field)) {
				$style = " style=\"font-weight: bold;\"";
			};
			$value = '0';
			if (array_key_exists($current_date, $all_data)) {
				if (array_key_exists($field, $all_data[$current_date])) {
					$value = sprintf("%.0d", $all_data[$current_date][$field]);
				};
			};
			if (!empty($value)) {
				if (!array_key_exists($field, $totals)) {
					$totals[$field] = 0;
				};
				$totals[$field] += $value;
			};
			if (preg_match('/To Collect/i', $field)) {
				$style .= ' class="collect"';
			};
			if (preg_match('/To Pay/i', $field)) {
				$style .= ' class="topay"';
			};
			echo "\t\t<td custom-day=\"$day\" custom-field=\"$field\"$style>$value</td>\n";
		};
?>
	</tr>
<?php
	};
	echo "\t<tr class=\"totals\">\n";
	echo "\t\t<th colspan=\"2\">Total</th>\n";
	foreach ($fields as $field) {
		$value = 0;
		$style = '';
		if (array_key_exists($field, $totals)) {
			$value = $totals[$field];
		};
		if (preg_match('/To Collect/i', $field)) {
			$style .= ' class="collect"';
		};
		if (preg_match('/To Pay/i', $field)) {
			$style .= ' class="topay"';
		};
		echo "\t\t<th$style>$value</th>\n";
	};
	echo "</tr>";
?>
</table>
</form>
<form>
<input type="hidden" name="date" value="<?php echo date('Y-m', $current_time); ?>">
<button name='action' value="CSV">Download CSV</button>
</form>
<button id="back" onClick="javascript: document.location='/Bookkeeping/'">Back</button>
<?php include ("ticker.php"); ?>
</body>
</html>
