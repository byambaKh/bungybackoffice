<?php
	include '../includes/application_top.php';

	$current_time = time();
	$d = date('Y-m', $current_time);
	if (isset($_GET['date'])) {
		$current_time = mktime(0, 0, 0, substr($_GET['date'], 5, 2), 15, substr($_GET['date'], 0, 4));
	} else {
		$current_time = mktime(0, 0, 0, substr($d, 5, 2), 15, substr($d, 0, 4));
	};
// total work function
	function getTotalWork($in, $out) {
		list($in_h, $in_m) = explode(':', $in);
		list($out_h, $out_m) = explode(':', $out);
		$diff = $out_h - $in_h + ($out_m - $in_m) / 60;
		if ($diff >= 9) {
			$diff -= 0.25;
		};
		$diff -= 0.75;
		if ($in == $out) $diff = 0;
		return number_format($diff, 2);
	}

	$headers = array(
		"Staff",
		"Total",
	);
	$fields = array_map(function ($item) {
		if ($item == 'IN') $item = 'Start';
		if ($item == 'OUT') $item = 'Finish';
		return strtolower(
			preg_replace(
				"/(_)$/",
				"",
				preg_replace("([^\w]+)", "_", $item)
			)
		);
	}, $headers);

	// check if needed table exists
	
	$action = $_POST['action'];
	if (empty($action)) {
		$action = $_GET['action'];
	};
	switch (TRUE) {
		case ($action == 'CSV'):
    		$d = date('Y-m', $current_time);
			$filename="payrol_monthly_";
    		$sql = "select * from payroll WHERE site_id = '".CURRENT_SITE_ID."' AND `date` like '$d%' order by `date` ASC;";
			Header('Content-Type: text/csv; name="'.$filename.$d.'.csv"');
			Header('Content-Disposition: attachment; filename="'.$filename.$d.'.csv"');
    		$res = mysql_query($sql);
			echo '"' . implode('","', $headers) . '"' . "\r\n";
			$staff_names = array();
    		while ($row = mysql_fetch_assoc($res)) {
				$row['total_work'] = getTotalWork($row['start'], $row['finish']);
				$row['day'] = preg_replace("/([\d]{4}-[\d]{2}-[0]?)/", "", $row['date']);
				if (!array_key_exists($row['staff'], $staff_names)) $staff_names[$row['staff']] = 0;
				$staff_names[$row['staff']] += $row['total_work'];
			};
			ksort($staff_names);
			foreach ($staff_names as $name => $value) {
				$value = number_format($value, 2);
				echo "\"$name\",\"$value\"";
				echo "\r\n";
			};
			die();
			break;
	};
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Payroll Monthly Totals</title>
<?php include "../includes/head_scripts.php"; ?>
<style>
h1 {
	margin: 0;
	background-color: yellow;
	text-align: center;
	width: 100%;
}
#payroll-table {
	background-color: black;
}
#payroll-table td {
	background-color: white;
}
#payroll-table .row-error td {
	background-color: red;
};
#payroll-table th {
	background-color: black;
	color: white !important;
}
#payroll-table th#out-header {
	background-color: red;
	color: white;
}
#payroll-table th#in-header {
	background-color: #00ff00;
	color: white;
}
#payroll-table td {
	vertical-align: top;
	text-align: center;
}
#payroll-header td {
	background-color: yellow;
}
#monthly-totals {
	float: right;
}
#staff-header, #total-header {
	color: white !important;
}
</style>
</head>
<body>
<script>
$(document).ready(function () {
    $("#close").on('click', function () {
        window.opener.focus();
        window.close();
    });
});
</script>
<table id="payroll-table" align="center">
<tr>
	<td colspan="<?php echo sizeof($fields); ?>">
		<table border="0" cellspacing="0" cellpadding="0" width="100%" id="payroll-header">
			<tr>
				<td colspan="2"><h1> Payroll Monthly Totals</h1></td>
			</tr>
			<tr>
				<td colspan="2"><h2><?php echo strtoupper(date("F Y", $current_time)); ?></h2></td>
			</tr>
			<tr>
				<td style="text-align: left;"><a href="?date=<?php echo date("Y-m", $current_time - 30*24*60*60); ?>">&lt;&lt;&lt;&lt;&lt;&lt; Prev</a></td>
				<td style="text-align: right;"><a href="?date=<?php echo date("Y-m", $current_time + 30*24*60*60); ?>">Next &gt;&gt;&gt;&gt;&gt;&gt;</a></td>
			</tr>
		</table>
	</td>
<tr>
<?php
	foreach ($fields as $key => $field) {
		echo "\t<th id=\"$field-header\">{$headers[$key]}</th>\n";
	};
?>
</tr>
<?php
	$d = date("Y-m-", $current_time);
	$sql = "select * from payroll WHERE site_id = '".CURRENT_SITE_ID."' AND `date` like '$d%' order by `date` ASC;";
	if ($res = mysql_query($sql)) {
		$staff_totals = array();
		while ($row = mysql_fetch_assoc($res)) {
			$row['day'] = preg_replace("/([\d]{4}-[\d]{2}-[0]?)/", "", $row['date']);
			if (!array_key_exists($row['staff'], $staff_totals)) $staff_totals[$row['staff']] = 0;
			$staff_totals[$row['staff']] += getTotalWork($row['start'], $row['finish']);
		};
		ksort($staff_totals);
		foreach ($staff_totals as $name => $value) {
?>
<tr>
	<td class='staff'><?php echo $name; ?></td>
	<td class='totals'><?php echo number_format($value, 2); ?></td>
</tr>
<?php
		};
	} else {
?>
<tr>
	<td id="no-record-exists" colspan="<?php echo sizeof($fields); ?>">No data available</th>
</tr>
<?php
	};
?>
<tr>
	<th id="csv-header" colspan="<?php echo sizeof($fields); ?>">CSV Functions</th>
</tr>
<tr>
	<td id="csv-td" colspan="<?php echo sizeof($fields); ?>">
<form>
	<input type=hidden name=date value="<?php echo date("Y-m", $current_time); ?>">
	<button name='action' value="CSV" style="float: right;">Download</button><br clear="both"/>
</form>
<button id="close">Close</button>
	</td>
</tr>
</table>
<?php include ("ticker.php"); ?>
</body>
</html>
