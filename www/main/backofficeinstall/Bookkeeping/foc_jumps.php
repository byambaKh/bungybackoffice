<?php
	include '../includes/application_top.php';
	include '../Operations/reports_save.php';
	include 'functions.php';
	$current_time = time();
	$d = date('Y', $current_time);
	$fields = array(
		'BookingDate'	=> 'Date',
		'BookingTime'	=> 'Time',
		'RomajiName'	=> 'Name',
		'NoOfJump'		=> '#',
		'foc'			=> 'FOC*'
	);

    $user = new BJUser();
if(!$user->hasRole(array("SysAdmin", "General Manager", "Financial"))){

        header("Location: ../index.php");
    }
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>FOC jumps</title>
<?php include "../includes/head_scripts.php"; ?>
<style>
h1 {
	margin: 0;
	background-color: red;
	text-align: center;
	width: 100%;
}
#foc-table {
	width: 500px;
	border-collapse: collapse;
}
#foc-table td {
	text-align: center;
	padding: 2px;
	border: 1px solid black;
	font-size: 14px;
	font-family: Arial;
}
#foc-table th {
	text-align: center;
	padding: 2px;
	border: 1px solid black;
	font-size: 14px;
	font-family: Arial;
	font-weight: bold;
}
.row-red td.noofjump, .row-red td.foc {
	background-color: red;
}
.row-green td.noofjump, .row-green td.foc {
	background-color: green;
}
#foc-table td.no-padding {
	padding: 0px !important;
}
.noofjump {
	width: 30px;
}
#foc-table td#back-td {
	border: none !important;
	text-align: left;
}
</style>
</head>
<body>
<?php include 'includes/main_menu.php'; ?>
<script>
</script>
<br />
<table id="foc-table" align="center">
<tr>
	<td colspan="<?php echo sizeof($fields); ?>" class="no-padding">
		<h1><?php echo SYSTEM_SUBDOMAIN; ?> FOC Jumps</h1>
	</td>
<tr>
<?php
	foreach ($fields as $key => $field) {
		$id = strtolower($key);
		echo "\t<th id=\"$id-header\">{$field}</th>\n";
	};
?>
</tr>
<?php
	$d = date("Y-", $current_time);
	$sql = "select * from customerregs1 WHERE site_id = '".CURRENT_SITE_ID."' AND Rate = 0 and Checked = 1 and DeleteStatus = 0 and NoOfJump > 0 and `BookingDate` like '$d%' order by `BookingDate` ASC;";
	if ($res = mysql_query($sql)) {
		while ($row = mysql_fetch_assoc($res)) {
			$row_class="";
			switch ($row['foc']) {
				case '':
				case 'Cancel':
				case 'NJ':
					$row_class='';
					break;
				case 'Promo':
				case 'Media':
					$row_class='row-red';
					break;
				case 'Bungy':
					$row_class='row-green';
					break;
			};
?>
<tr class="<?php echo $row_class; ?>">
<?php
			foreach ($fields as $field => $header) {
				$class = strtolower($field);
				echo "\t<td class=\"value $class\" rel=\"$class\">{$row[$field]}</td>\n";
			};
?>
</tr>
<?php
		};
	} else {
echo mysql_error();
?>
<tr>
	<td id="no-record-exists" colspan="<?php echo sizeof($fields); ?>">No data available</th>
</tr>
<?php
	};
?>
<tr>
	<td id="back-td" colspan="<?php echo sizeof($fields); ?>">
        <br />
        <button id="back" onClick="javascript: document.location='/Bookkeeping/'">Back</button>
	</td>
</tr>
</table>

<?php include ("ticker.php"); ?>
</body>
</html>
