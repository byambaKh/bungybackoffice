<?php
include '../includes/application_top.php';
include '../Operations/reports_save.php';
include 'functions.php';

mysql_set_charset("utf8");//json_decode fails if this is removed
function handlePermissions()
{
    $user = new BJUser();
    if (!$user->hasRole(array("SysAdmin", "General Manager", "Financial", "Price Waterhouse Cooper"))) {
        header("Location: ../index.php");
    }
}

function chooseBankAccount()
{
    $bankAccount = 'all';//default is all. Others are Gunma, Mizho, JNB

    if (in_array($_GET['bank_account'], ['jnb', 'mizuho', 'gunma', 'yucho'])) {
        if (isset($_GET['bank_account']))
            $bankAccount = $_GET['bank_account'];
    }

    $bankNames = getBankNames();
    $bankDisplayName = $bankNames[$bankAccount];

    return [$bankAccount, $bankDisplayName];
}

function getBankNames()
{
    $bankNames =
        [
            'yucho'  => 'Yucho (ゆうちょ銀行)',
            'jnb'    => 'Japan Net Bank (ジャパンネット銀行)',
            'mizuho' => 'Mizuho (みずほ銀行)',
            'gunma'  => 'Gunma (群馬銀行)',
            'all'    => 'All Bank Accounts'
        ];

    return $bankNames;
}

function currentTime()
{
    $current_time = time();
    $d = date('Y-m', $current_time);
    if (isset($_GET['date'])) {
        $current_time = mktime(0, 0, 0, substr($_GET['date'], 5, 2), 15, substr($_GET['date'], 0, 4));
    } else {
        $current_time = mktime(0, 0, 0, substr($d, 5, 2), 15, substr($d, 0, 4));
    }

    return $current_time;
}

function generateHeadersAndFields()
{
    $headers = array(
        "Date",
        "OUT",
        "IN",
        "INTERNATIONAL REMIT",
        "BALANCE",
        "Fee",
        "MK",
        "SG",
        "IB",
        "KY",
        "Who",
        "Notes",
        "Company",
        "Description",
        "Analysis",
        "CONTAX",
    );
    $fields = array_map(function ($item) {
        return strtolower(
            preg_replace(
                "/(_)$/",
                "",
                preg_replace("([^\w]+)", "_", $item)
            )
        );
    }, $headers);

    //$headers[4] = 'Fee';

    return array($headers, $fields);
}

function generateValuesFromFields($fields)
{
    $values = [];
    foreach ($fields as $field) {
        $group = 'banking';
        $needed_field = $field;
        if ($field == 'company') {
            $group = 'general';
        }
        if ($field == 'contax') {
            $group = 'general';
        }
        if ($field == 'analysis') {
            $group = 'general';
        }
        $list = BJHelper::getList($group, $needed_field);
        if (!empty($list)) {
            $values[$field] = $list[$group][$needed_field];
        }
        if ($field == 'who') {
            $values[$field] = BJHelper::getStaffList('StaffListName');
        }
    }
    $agents = BJHelper::getAgents(true);

    $sort_function = function ($a, $b) {
        return (strcasecmp($a['text'], $b['text']) >= 0);
    };

    usort($agents, $sort_function);
    array_unshift($agents, array(
        'id'     => 'undefined',
        'text'   => '=== Agents ===',
        'status' => 0
    ));
    $agents[] = array(
        'id'     => 'undefined',
        'text'   => '=== Staff ===',
        'status' => 0
    );
    $agents = array_merge($agents, $values['who']);
    $agents[] = array(
        'id'     => 'undefined',
        'text'   => '=== Other ===',
        'status' => 0
    );
    usort($values['company'], $sort_function);
    $values['company'] = array_merge($agents, $values['company']);

    return $values;
}

function getBankingRows($current_time, $bankAccount, $fields)
{
    /*
    * 2014-12-02: Ayodeji Osokoya Banking C/F http://standardmove.com/projects/view.php?id=668
    * In the current database a carry forward is calculated for each month. This is the balance of the previous month.
    * This was calculated on the first entry in the new month and stored. The problem was this value was calculated once and
    * any changes to the previous month would not be updated.
    * These rows are at the top of the banking page in grey. In the database they are the rows with cf = 1
    *
    * Due to this change, there is a discrepancey between the carry forwards and the value you get when you sum in - out
    * across all payments. On main, the carry forward from November 2014 is 108276713 but December starts at 108226213
    * To solve this an adjustment has been added at date 0000:00:00 of 50500
    *
    * The same has been done on Minakami where an adjustment of 2703566 has been added to the beginning as some weirdness
    * with manually entered carry forwards is causing the initial balance to not match up.
    */
    $bankFilter = bankFilter($bankAccount);

    $dateCurrentMonth = date('Y-m-', $current_time);
    $siteId = CURRENT_SITE_ID;

    $sql = "
        SELECT
        *
        FROM banking
          WHERE site_id = $siteId
          AND cf = 0
          AND `date` LIKE '$dateCurrentMonth%'
          $bankFilter
        ORDER BY `date`, id ASC;
    ";
    $allRows = queryForRows($sql);

    $datePreviousMonth = date("Y-m", $current_time) . " -1 month"; //this will return something like "2014-11-2 -1 month"if the date is 2014-12-2
    $dateEndOfPreviousMonth = date('Y-m-t', strtotime($datePreviousMonth));//t is the number of days in the month
    $carryForwardArray = calculateCarryForward($current_time, $dateEndOfPreviousMonth, $bankFilter);
    array_unshift($allRows, $carryForwardArray);//prepend the carry forward entry

    //echo "--getBankingRows--\n\n";

    $balance = 0;
    foreach ($allRows as $rowIndex => &$row) {
        if($rowIndex == 0) $balance = $row['balance'];//start with the balance from the first row (cf)

        foreach($fields as $key => $field) {
            list($balance, $row) = calculateMissingBalancesAndCarryForwards($row, $field, $balance);
        }
    }

    return $allRows;
}

/**
 * Fill in the row with all of the missing values that are stored in the database
 *
 * This used to be part of drawAllRows but was moved out due to needing identical calculations for both CSV and HTML output.
 * @param $row
 * @param $field
 * @param $balance
 *
 * @return mixed
 */
function calculateMissingBalancesAndCarryForwards($row, $field, $balance)
{
    //if the row is a carry forward and the field is not in, out or balance, set it to C/F
    if ($row['cf'] == 1 && !in_array($field, array('out', 'in', 'balance', 'notes', 'international_remit'))) {
        $row[$field] = 'C/F';
    }

    //the balance is not stored for all fields,
    if ($field == 'balance') {
        $balance = $row['balance'] = $balance + $row['in'] - $row['out'] - $row['international_remit'];
    }

    return [$balance, $row];
}

/**
 * @param $bankAccount
 *
 * @return string
 */
function bankFilter($bankAccount)
{
    $bankFilter = "";
    if ($bankAccount != 'all')
        $bankFilter = "AND bank_account = '$bankAccount'";

    return $bankFilter;
}


/**
 * @param $current_time
 * @param $dateEndOfPreviousMonth
 * @param $bankFilter
 *
 * @return array
 */
function calculateCarryForward($current_time, $dateEndOfPreviousMonth, $bankFilter)
{
    //sum all of the ins and outs in the
    $carryForwardQuery = "
        SELECT
          SUM(`in`-`out`-`international_remit`) AS carryForward
        FROM banking
          WHERE
            site_id = " . CURRENT_SITE_ID . "
            AND `date` <= LAST_DAY('$dateEndOfPreviousMonth')
            AND cf = 0
            $bankFilter
        GROUP BY site_id;
    ";
    $carryForwardRows = queryForRows($carryForwardQuery);
    $carryForward = $carryForwardRows[0]['carryForward'];

    //insert the carry forward as the first entry
    $carryForwardArray = [
        'id'          => '',//carry forward is calculated so it is not editable
        'date'        => date('Y-m', $current_time) . "-01",
        'out'         => 0,
        'in'          => 0,
        'international_remit' => 0,
        'balance'     => empty($carryForward) ? 0 : $carryForward,
        'charge'      => 'C/F',
        'who'         => 'C/F',
        'notes'       => 'Auto Calculated Carry Forward',
        'company'     => 'C/F',
        'yayoi'       => 'C/F',
        'analysis'    => 'C/F',
        'contax'      => 'C/F',
        'cf'          => 1,
        'description' => 'C/F',
        'site_id'     => 'C/F',
        'mk'          => 'C/F',
        'sg'          => 'C/F',
        'ib'          => 'C/F',
        'ky'          => 'C/F',
        'company_new' => 'C/F',
    ];

    return $carryForwardArray;
}

function handlePOST($values, $record_type, $current_time, $headers, $fields, $bankAccount)
{
    $action = $_POST['action'];
    if (empty($action)) {
        $action = $_GET['action'];
    }

    switch (TRUE) {
        case ($action == 'Add'):
            if (array_key_exists('shop_name_new', $_POST['i']) && !empty($_POST['i']['shop_name_new'])) {
                $_POST['i']['shop_name'] = $_POST['i']['shop_name_new'];

                //Prevents entering duplicate agent names. I would prefer to add this to the addCompanyName code but
                //it might affect other things.
                //if it shop name exists in the list do not add it as a new agent
                //note shop name is not displayed anymore
                BJHelper::addCompanyNameUniquely($_POST['i']['shop_name'], $values['company']);//contains all agents, users, companies
            }

            if (array_key_exists('company_new', $_POST['i']) && !empty($_POST['i']['company_new'])) {
                $_POST['i']['company'] = $_POST['i']['company_new'];

                //if company name exists in the list do not add it as a new agent
                BJHelper::addCompanyNameUniquely($_POST['i']['company_new'], $values['company']);
            }

            if (array_key_exists('shop_name_new', $_POST['i'])) unset($_POST['i']['shop_name_new']);

            if (array_key_exists('company_new', $_POST['i'])) unset($_POST['i']['company_new']);
            $d = substr($_POST['i']['date'], 0, 7);
            //2014-12-02 Ayodeji Osokoya We are no longer calculating carry forwards and storing them
            //They are now calculated on the fly See the fix below. Search for "Banking C/F"
            /*
            $sql = "SELECT * FROM $record_type WHERE site_id = '" . CURRENT_SITE_ID . "' AND cf = 1 AND `date` LIKE '$d%';";
            $res = mysql_query($sql) or die(mysql_error());
            if (mysql_num_rows($res) == 0) {
            // Check if previous month have records and get one last as CF
                $last_month_date = date("Y-m-", mktime(0, 0, 0, substr($_POST['i']['date'], 5, 2) - 1, 15, substr($_POST['i']['date'], 0, 4)));
                $sql = "SELECT sum(`in`-`out`) as balance, 1 FROM $record_type WHERE site_id = '" . CURRENT_SITE_ID . "' AND `date` LIKE '$last_month_date%' GROUP BY 2;";
                $res = mysql_query($sql) or die(mysql_error());
                $data = array(
                    'site_id' => CURRENT_SITE_ID,
                    'date' => $d . '-01',
                    'cf' => 1,
                    'in' => 0,
                    'out' => ''
                );
                if ($row = mysql_fetch_assoc($res)) {
                    $data['in'] = $row['balance'];
                };
                db_perform($record_type, $data);
                $_POST['i']['cf'] = 0;
            } else {
                $_POST['i']['cf'] = 0;
            };*/
            $_POST['i']['site_id'] = CURRENT_SITE_ID;
            save_report($record_type, $_POST['i']);
            Header("Location: $record_type.php?date=" . date("Y-m", $current_time) . "&bank_account=$bankAccount");
            die();
            break;

        case ($action == 'Update'):
            $_POST['i']['site_id'] = CURRENT_SITE_ID;
            save_report($record_type, $_POST['i']);
            Header("Location: $record_type.php?date=" . date("Y-m", $current_time));
            die();
            break;

        case ($action == 'Delete'):
            $sql = "DELETE FROM banking WHERE site_id = '" . CURRENT_SITE_ID . "' AND id = '{$_GET['id']}'";
            mysql_query($sql) or die(mysql_error());
            Header("Location: $record_type.php?date=" . date("Y-m", $current_time) . "&bank_account=$bankAccount");
            die();
            break;

        case ($action == 'CSV'):
            $current_month = date("Y-m", $current_time);
            Header('Content-Type: text/csv; name="banking' . $current_month . '.csv"');
            Header('Content-Disposition: attachment; filename="banking' . $current_month . '.csv"');
            echo '"' . implode('","', $headers) . '"' . "\r\n";

            $allRows = getBankingRows($current_time, $bankAccount, $fields);


            $fields[] = 'bank_account';//include the bank account field
            foreach ($allRows as $row) {
                foreach ($fields as $field) {

                    echo '"' . $row[$field] . '"' . ($field == $fields[sizeof($fields) - 1] ? '' : ',');
                }
                echo "\r\n";
            }

            die();
            break;
    }
}

function drawInputRow($fields, $values, $current_time, $bankAccount)
{
    ?>
    <tr>
        <th id="add-new" colspan="<?= sizeof($fields) + 1; ?>">Add new record</th>
    </tr>
    <tr>
        <form method="POST" action="?date=<?= date('Y-m', $current_time); ?>&bank_account=<?= $bankAccount ?>">
            <?
            foreach ($fields as $key => $field) {
                $record = array();
                $style = '';
                if (in_array($field, array('mk', 'sg', 'ib', 'ky'))) {
                    $record[$field] = 0;
                    $style = ' style="text-align: center !important;"';
                };
                $input_field = draw_input_field($field, $field, $record, $style, false);
                if (array_key_exists($field, $values)) {
                    $input_field = draw_pull_down_menu('i[' . $field . ']', $values[$field]);
                };
                if ($field == 'shop_name' || $field == 'company') {
                    $input_field .= '<br />' . draw_input_field($field . '_new', $field . '-new', $record, '', false);
                };
                if ($field == 'notes') {
                    $input_field .= '<br /><button name="sales" class="date" type="button" onClick="show_sales_calendar();">Sales</button><div style="display: none;"><input type="hidden" id="sales-date"></div>';
                };
                if ($field == 'charge') {
                    $input_field .= '<br /><button name="charge" class="date" type="button" onClick="input_bank_fees();">Bank Fees</button><div style="display: none;"><input type="hidden" id="sales-date"></div>';
                };
                echo "\t<td id=\"$field-new\" class=\"$field\">" . $input_field . "</td>\n";
            };
            ?>

            <td id="action-add">
                <input type="hidden" name="i[bank_account]" value="<?= $bankAccount ?>">
                <input type="submit" name="action" value="Add">
            </td>
    </tr>
    </form>
    <?
}

function drawAllRows($allRows, $fields)
{
    //echo "--drawAllRows--\n\n";
    //pr($allRows);
    $balance = 0;
    foreach ($allRows as $row) {
        ?>
        <tr<?php if ($row['cf']) echo " class=\"cf\""; ?>>
            <?php
            foreach ($fields as $key => $field) {
                ///list($balance, $row) = calculateMissingBalancesAndCarryForwards($row, $field, $balance);
                $cellValue = $row["$field"];
                if (in_array($field, array('in', 'out', 'balance'))) {
                    //javascript recalculation screws up the balances
                    //$cellValue = formatNumber($row[$field]);
                }
                echo "\t<td id=\"$field-{$row['id']}\" class=\"value $field\" rel=\"$field\">$cellValue</td>\n";
            }
            ?>
            <?php 
                    $curUser = new BJUser();
                    if($curUser->getUserName() == 'sugai'): ?>

                <td>
                <nobr>
                    <input type="button" value="Change" rel="" class="change" hidden="hidden">
                    <input type="button" value="Delete" rel="" class="delete" hidden="hidden">
                </nobr>
            </td>

                <?php else: ?>
            <td id="action-change-delete">
                <nobr>
                    <input type="button" value="Change" rel="<?php echo $row['id']; ?>" class="change">
                    <input type="button" value="Delete" rel="<?php echo $row['id']; ?>" class="delete">
                </nobr>
            </td>
            <?php endif; ?>
        </tr>
        <?php
    }
}

handlePermissions();
list($bankAccount, $bankName, $headerColor) = chooseBankAccount();
$record_type = 'banking';
$current_time = currentTime();
list($headers, $fields) = generateHeadersAndFields();
$values = generateValuesFromFields($fields);
handlePOST($values, $record_type, $current_time, $headers, $fields, $bankAccount);
$allRows = getBankingRows($current_time, $bankAccount, $fields);

$balanceInitial = $allRows[0]['balance'];

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Banking</title>
    <?php include "../includes/head_scripts.php"; ?>
    <link rel="stylesheet" type="text/css" href="css/banking_php.css" media="all">
</head>
<body>
<?php include '../includes/main_menu.php'; ?>
<?php include 'distribution.js.php'; ?>
<script>
    //define all variables used from php before including banking.js
    var balance = <?= $balanceInitial; ?>;
    var _record_type = '<?= $record_type; ?>';
    var _defaultDate = '<?= date("Y-m-01", $current_time); ?>';
    var _maxDate = '<?= $maxDate?>';
    var fvalues = {};
    var _bankingTable = '<?= $record_type ?>';
    <?
    foreach ($fields as $field) {
        echo "\tfvalues['$field'] = " . json_encode(array_key_exists($field, $values) ? $values[$field] : array()) . ";\n";
    }
    ?>
</script>
<script src="js/banking.js" type=""></script>
<table id="banking-table">
    <tr>
        <td colspan="<?= sizeof($fields) + 1; ?>">
            <table border="0" cellspacing="0" cellpadding="0" width="100%" id="banking-header-<?= $bankAccount ?>">
                <tr>
                    <td colspan="2"><h1> Banking - <?= $bankName ?> </h1></td>
                </tr>
                <tr>
                    <td colspan="2"><h2><?= strtoupper(date("F Y", $current_time)); ?></h2></td>
                </tr>
                <tr>
                    <td style="text-align: left;">
                        <a href="?date=<?= date("Y-m", $current_time - 30 * 24 * 60 * 60) . "&bank_account=$bankAccount"; ?>">&lt;&lt;&lt;&lt;&lt;&lt; Prev</a>
                    </td>
                    <td style="text-align: right;">
                        <a href="?date=<?= date("Y-m", $current_time + 30 * 24 * 60 * 60) . "&bank_account=$bankAccount"; ?>">Next &gt;&gt;&gt;&gt;&gt;&gt;</a>
                    </td>
                </tr>
                <tr>
                    <td style="text-align: left;"></td>
                    <td style="text-align: right; width:25%;">
                        <select id="bank-account-select" onchange="location = this.options[this.selectedIndex].value">
                            <option value="#"> --- Change Bank Account ---</option>
                            <option value="?bank_account=all&date=<?= date('Y-m', $current_time); ?>">All Bank Accounts</option>
                            <option value="?bank_account=gunma&date=<?= date('Y-m', $current_time); ?>">Gunma (群馬銀行)</option>
                            <option value="?bank_account=jnb&date=<?= date('Y-m', $current_time); ?>">Japan Net Bank (ジャパンネット銀行)</option>
                            <option value="?bank_account=mizuho&date=<?= date('Y-m', $current_time); ?>">Mizuho (みずほ銀行)</option>
                            <option value="?bank_account=yucho&date=<?= date('Y-m', $current_time); ?>">Yucho (ゆうちょ銀行)</option>
                        </select>
                    </td>
                </tr>
            </table>
        </td>
    <tr>
        <?php
        foreach ($fields as $key => $field) {
            echo "\t<th id=\"$field-header\">{$headers[$key]}</th>\n";
        };
        ?>
        <th id="actions-header">Actions</th>
    </tr>
    <?php drawAllRows($allRows, $fields); ?>
    <? if ($bankAccount != 'all') drawInputRow($fields, $values, $current_time, $bankAccount); ?>
</table>
<form method="POST" action="?date=<?= date('Y-m', $current_time); ?>&bank_account=<?= $bankAccount ?>">
    <input type="hidden" name="date" value="<?= date('Y-m', $current_time); ?>">
    <button name='action' value="CSV">Download CSV</button>
    <button name='distribution' type='button' value="CSV" id="distribution">Distribution Summary</button>
</form>
<button id="back" onClick="javascript: document.location='<?= (CURRENT_SITE_ID == 0) ? '/Analysis/' : '/Bookkeeping/'; ?>'"> Back</button>
<?php include("ticker.php"); ?>
</body>
</html>
