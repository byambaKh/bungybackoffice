<?php

?>
<html>
	<head>
		<title>Testing Auto-Complete</title>
		<script src='http://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js'></script>
		<script src='http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js'></script>
		<link rel='stylesheet' href='http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css' type='text/css' media='all'/>
		<link rel='stylesheet' href='/css/main_menu.css' type='text/css' media='all'/>
		<script>
			$(document).ready(function(){
				var autoCompleteData = [
					'aaaa0',
					'bbbb1',
					'cccc2',
					'dddd3',
					'eeee4',
					'ffff5',
					'gggg6',
					'hhhh7',
					'test8',
					'test9',
					'test0',
					'test1',
					'test2',
					'test3',
					'test4'
				];

				$('#testing').autocomplete({ 
					minLength: 0,
					source: autoCompleteData 
				});
			});

			//$('#testing').autocomplete( 'enable' );
		</script>
	</head>
	<body>
	<form>
		<input id='testing' type='text' placeholder='Search...'>
	</form>
	
	</body>
</html>

