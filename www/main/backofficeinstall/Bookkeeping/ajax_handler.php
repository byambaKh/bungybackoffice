<?php
include "../includes/application_top.php";
include '../Operations/reports_save.php';
include 'functions.php';

//Check if the user is logged in before giving out data
$user = new BJUser();
if (!$user->isUser()) die();

$action = $_GET['action'];
$type = $_GET['type'];
if (empty($type)) {
    $type = 'expenses';
};
if ($type == 'payroll') {
    $_POST['date'] = $_GET['date'] . '-' . $_POST['day'];
    unset($_POST['day']);
};
//logDataToFile("NOW", "/Bookkeeping/ajaxBug.log");
//logDataToFile($_POST, "/Bookkeeping/expense_update_bug.log");
switch (TRUE) {
    case ($action == 'update_row') :
        if (array_key_exists('shop_name_new', $_POST) && !empty($_POST['shop_name_new'])) {
            $_POST['shop_name'] = $_POST['shop_name_new'];
            BJHelper::addCompanyName($_POST['shop_name_new']);
        };
        $_POST['site_id'] = CURRENT_SITE_ID;
        save_report($type, $_POST);
        //logDataToFile($_POST, "/Bookkeeping/expense_update_bug.log");
        $result = array('result' => 'success');
        break;
    case ($action == 'delete_row') :
        $sql = "DELETE FROM $type WHERE id = {$_POST['id']};";
        //logDataToFile($sql, "/Bookkeeping/ajaxBug.log");
        mysql_query($sql);
        $result = array('result' => 'success');
        break;
};
$result['request'] = array(
    'action' => $action,
    'type' => $type,
    'post' => $_POST
);
echo json_encode($result);
?>
