<?php
	include '../includes/application_top.php';
	$current_time = time();
	$d = date('Y', $current_time);
	$fields = array(
		'it_date'		=> 'Date',
		'inventory'	=> 'Inventory Distribution',
		'payment'	=> 'Payment Transfer',
		'balance'	=> 'Outstanding Balance'
	);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Inventory Transfers</title>
<?php include "../includes/head_scripts.php"; ?>
<style>
h1 {
	margin: 0;
	background-color: red;
	text-align: center;
	width: 100%;
	padding-top: 5px;
	padding-bottom: 5px;
}
#foc-table {
	border-collapse: collapse;
	border: 2px solid black;
}
#foc-table1 {
	width: 440px;
}
#foc-table td {
	text-align: center;
	padding: 5px;
	border: 1px solid black;
	font-size: 14px;
	font-family: Arial;
}
#foc-table th {
	text-align: center;
	border: 2px solid black;
	font-size: 14px;
	font-family: Arial;
	font-weight: bold;
	padding: 5px;
}
.row-red td.noofjump, .row-red td.foc {
	background-color: red;
}
.row-green td.noofjump, .row-green td.foc {
	background-color: green;
}
#foc-table td.no-padding {
	padding: 0px !important;
}
.noofjump {
	width: 30px;
}
#foc-table td#back-td {
	border: none !important;
	text-align: left;
}
.row0 td {
	background-color: #eee;
}
</style>
</head>
<body>
<?php include '../includes/main_menu.php'; ?>
<br />
<table id="foc-table" align="center">
<tr>
	<th colspan="<?php echo sizeof($fields); ?>" class="no-padding" style="background-color: red;">
		<h1><?php echo SYSTEM_SUBDOMAIN; ?> Inventory  Transfers</h1>
	</th>
<tr>
<?php
	foreach ($fields as $key => $field) {
		$id = strtolower($key);
		echo "\t<th id=\"$id-header\">{$field}</th>\n";
	};
?>
</tr>
<?php
	$d = date("Y-", $current_time);
	$sql = "
		SELECT `date` as it_date, sum(inventory) as inventory, sum(payment) as payment FROM (
			(
				SELECT 
					created as 'date',
					sum(CAST(unit_cost as decimal(10,0)) * CAST(units as decimal(10, 0))) as inventory,
					0 as payment
				FROM inventory_items_transactions
				WHERE type_id = 2 AND site_id = '" . CURRENT_SITE_ID . "'
				GROUP BY `date`
				ORDER BY `date` ASC
			) UNION (
				SELECT 
					`date`,
					0 as inventory,
					sum(`out`) as payment
				FROM banking
				WHERE 
					site_id = '" . CURRENT_SITE_ID . "'
					AND notes = 'Inventory Transfer'
				GROUP BY `date`
				ORDER BY `date` ASC
			)
		) as t1 GROUP BY it_date ORDER BY it_date ASC;
	";
	$balance = 0;
	$i = 0;
	if ($res = mysql_query($sql) or die(mysql_error())) {
		while ($row = mysql_fetch_assoc($res)) {
			$row_class = 'row' . $i;
			$i = (int)!$i;
			$balance += $row['inventory'] - $row['payment'];
			$row['balance'] = $balance;
?>
<tr class="<?php echo $row_class; ?>">
<?php
			foreach ($fields as $field => $header) {
				$class = strtolower($field);
				echo "\t<td class=\"value $class\" rel=\"$class\">{$row[$field]}</td>\n";
			};
?>
</tr>
<?php
		};
	} else {
echo mysql_error();
?>
<tr>
	<td id="no-record-exists" colspan="<?php echo sizeof($fields); ?>">No data available</th>
</tr>
<?php
	};
?>
</tr>
</table>
<table id="foc-table1" align="center">
<tr>
	<td id="back-td" colspan="<?php echo sizeof($fields); ?>">
<br />
<button id="back" onClick="javascript: document.location='/Bookkeeping/'">Back</button>
	</td>
</tr>
</table>
<?php include ("ticker.php"); ?>
</body>
</html>
