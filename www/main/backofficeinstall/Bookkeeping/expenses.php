<?php
include '../includes/application_top.php';
include '../Operations/reports_save.php';
include 'functions.php';
//json encode fails
//http://stackoverflow.com/questions/9098507/why-is-this-php-call-to-json-encode-silently-failing-inability-to-handle-singl
mysql_set_charset("utf8");

$user = new BJUser();

$record_type = 'expenses';
$current_time = time();
$d = date('Y-m', $current_time);
if (isset($_GET['date'])) {
    $current_time = mktime(0, 0, 0, substr($_GET['date'], 5, 2), 15, substr($_GET['date'], 0, 4));
} else {
    $current_time = mktime(0, 0, 0, substr($d, 5, 2), 15, substr($d, 0, 4));
};
$d = date('Y-m', $current_time);

if($subdomain == 'main'){
    $headers = array( "PURCHASER", "No #", "DATE", "COST", "MK", "SG", "IB", "KY", "DESCRIPTION", "Shop Name", "Notes", "ANALYSIS", "Personal Notes", "Con Tax", );
    $fields = array("purchaser", "no", "date", "cost", "mk", "sg", "ib", "ky", "description", "shop_name", "notes", "analysis", "personal_notes", "con_tax", );
} else {
    $headers = array( "PURCHASER", "No #", "DATE", "COST", "MK", "SG", "IB", "DESCRIPTION", "Shop Name", "Notes", "ANALYSIS", "Personal Notes", "Con Tax", );
    $fields = array("purchaser", "no", "date", "cost", "mk", "sg", "ib", "description", "shop_name", "notes", "analysis", "personal_notes", "con_tax", );
}
/*
$fields = array_map(function ($item) {
    return strtolower(
        preg_replace(
            "/(_)$/",
            "",
            preg_replace("([^\w]+)", "_", $item)
        )
    );
}, $headers);
*/

foreach ($fields as $field) {//Generating the fields for drop downs and preset values in fields such as Analysis, Con Tax
    $group = 'expenses';
    $needed_field = $field;
    if ($field == 'shop_name') {
        $group = 'general';
        $needed_field = 'company';
    };
    if ($field == 'con_tax') {
        $group = 'general';
        $needed_field = 'contax';
    };
    if ($field == 'analysis') {
        $group = 'general';
    };
    $list = BJHelper::getList($group, $needed_field);//gets the lists such as analysis, agents etc
    if (!empty($list)) {
        $values[$field] = $list[$group][$needed_field];
    };
    if ($field == 'purchaser') {
        $values[$field] = BJHelper::getStaffList('StaffListName');
    };
};
$agents = BJHelper::getAgents(true);
$sort_function = function ($a, $b) {
    return (strcasecmp($a['text'], $b['text']) >= 0);
};
usort($agents, $sort_function);
//prepend Agents title entry on to the drop down list for agents
array_unshift($agents, array( 'id' => 'undefined', 'text' => '=== Agents ===', 'status' => 0 ));
//Append Staff title entry on to the drop down list for Agents
$agents[] = array( 'id' => 'undefined', 'text' => '=== Staff ===', 'status' => 0 );

$agents = array_merge($agents, $values['purchaser']);

$agents[] = array( 'id' => 'undefined', 'text' => '=== Other ===', 'status' => 0 );
usort($values['shop_name'], $sort_function);
$values['shop_name'] = array_merge($agents, $values['shop_name']);

/**********************************Finish Generating lists for drop downs etc*****************************************/

$action = $_POST['action'];
if (empty($action)) {
    $action = $_GET['action'];
};
switch (TRUE) {
    case ($action == 'Add'):
        if (array_key_exists('shop_name_new', $_POST['i']) && !empty($_POST['i']['shop_name_new'])) {
            $_POST['i']['shop_name'] = $_POST['i']['shop_name_new'];
            //BJHelper::addCompanyNameUniquely($_POST['i']['shop_name_new'], $agents);
            BJHelper::addCompanyNameUniquely($_POST['i']['shop_name_new'], $values['shop_name']);
        };

        $currentDateString = date("Y-m");

        $_POST['i']['site_id'] = CURRENT_SITE_ID;
        //if there is no date set, set the date to today
        $_POST['i']['date'] = ($_POST['i']['date'] != "") ? $_POST['i']['date'] : date('Y-m-d');

        save_report('expenses', $_POST['i']);
        $_GET['date'] = substr($_POST['i']['date'], 0, 7);
        Header("Location: expenses.php?date=" . $_GET['date']);
        die();
        break;
    case ($action == 'Update'):
        save_report('expenses', $_POST['i']);
        Header("Location: expenses.php");
        die();
        break;
    case ($action == 'Delete'):
        $sql = "delete from expenses where site_id = '" . CURRENT_SITE_ID . "' AND id = '{$_GET['id']}'";
        mysql_query($sql) or die(mysql_error());
        Header("Location: expenses.php");
        die();
        break;
    case ($action == 'CSV'):
        $d = date('Ym', $current_time);
        Header('Content-Type: text/csv; name="expenses' . $d . '.csv"');
        Header('Content-Disposition: attachment; filename="expenses' . $d . '.csv"');
        $d = date('Y-m-', $current_time);
        $sql = "select * from $record_type where site_id = '" . CURRENT_SITE_ID . "' AND `date` like '$d%' order by id ASC;";
        $res = mysql_query($sql);
        echo '"' . implode('","', $headers) . '"' . "\r\n";
        while ($row = mysql_fetch_assoc($res)) {
            foreach ($fields as $field) {
                echo '"' . $row[$field] . '"' . ($field == $fields[sizeof($fields) - 1] ? '' : ',');
            };
            echo "\r\n";
        };
        die();
        break;
    case($action == 'swReup'):
        $currentDate = new DateTime();
        $currentDateString = $currentDate->format("Y-m-d");

        $data = array(
            'date' =>  $currentDateString,
            'cost' => $_POST['swReupValue'],
            'who' => $_SESSION['myusername'],
            'site_id' => CURRENT_SITE_ID
        );

        db_perform('expenses_staff_welfare', $data, 'insert');
        //Header("Location: expenses.php");
        break;
};
$pc_reup = $pcReupsPrior = $pcReupsThisMonth = $expensesPrior = $expensesThisMonth = 0;

//Get all of the petty cash updates for the current month
$sql = "SELECT SUM(`in`-`out`) as total FROM banking WHERE site_id = '" . CURRENT_SITE_ID . "' and `date` like '$d%' and notes = 'PC';";
//$sql = "SELECT SUM(`in`-`out`) as total FROM banking WHERE site_id = '" . CURRENT_SITE_ID . "' AND `date` >= '2013-09-01' AND `date` < '$d-01' AND notes = 'PC';";
//SELECT SUM(`in`-`out`) as total FROM banking WHERE site_id = '2' AND notes = 'pc' AND `date` >= '2013-09-01' AND `date` <= '2014-12-01';
//echo "$sql <br>";
$res = mysql_query($sql) or die(mysql_error());
if ($row = mysql_fetch_assoc($res)) {
    $pcReupsThisMonth = -$row['total'];//these are outgoing payments in the banking column so we make ie minus
};

//History...
//2013-09-01 is the date that the expenses system seemed to come in to action.
//There are prior PC reups that were paid and were probably handled differently but including them would cause problems
//get all of the prior PC reups
$sql = "SELECT SUM(`in`-`out`) as total FROM banking WHERE site_id = '" . CURRENT_SITE_ID . "' AND `date` >= '2013-09-01' AND `date` < '$d-01' AND notes = 'PC';";
$res = mysql_query($sql) or die(mysql_error());
if ($row = mysql_fetch_assoc($res)) {
    $pcReupsPrior = -$row['total'];//Get all prior PC reups
};

//Get all of the expenses for this month
$sql = "SELECT SUM(cost) as total FROM expenses WHERE site_id = '" . CURRENT_SITE_ID . "' AND `date` LIKE '$d-%';";
$res = mysql_query($sql) or die(mysql_error());
if ($row = mysql_fetch_assoc($res)) {
    $expensesThisMonth = $row['total'];
};
//Get all of the prior expenses
$sql = "SELECT SUM(cost) as total FROM expenses WHERE site_id = '" . CURRENT_SITE_ID . "' AND `date` < '$d-01';";
$res = mysql_query($sql) or die(mysql_error());
if ($row = mysql_fetch_assoc($res)) {
    $expensesPrior = $row['total'];
};

$pc_reup = $pcReupsThisMonth;
$petty_balance = $pcReupsPrior - $expensesPrior;

//calculate all of the staff welfare payments


#Get all of the reups for the previous months
$sql = "SELECT SUM(cost) as total FROM expenses_staff_welfare WHERE site_id = ".CURRENT_SITE_ID." AND `date` < '$d-01'";
$previousMonthsReups = getValueFromQuery($sql, 'total');

#GET all of the purchases for the previous months
$sql = "SELECT SUM(total) as final_total FROM (
	(SELECT SUM(`cost`) AS total FROM expenses WHERE description = 'Welfare: Party, Meal, Staff gifts, Office coffee etc.' AND site_id = '" . CURRENT_SITE_ID . "' AND `date` < '$d-01')
	UNION
	(SELECT SUM(`out`-`in`) AS total FROM banking WHERE description = 'Welfare: Party, Meal, Staff gifts, Office coffee etc.' AND site_id = '" . CURRENT_SITE_ID . "' AND `date` < '$d-01')
) as allPurchases;"; //Note we do out - in as purchases being added up while anything going 'in' reduces the amount of money used to purchase
$previousMonthPurchases = getValueFromQuery($sql, 'final_total');

//add/subtract them

#SW Re-up
	#GET all of the reups for this month
$sql = "SELECT SUM(cost) as staffWelfareReupTotal FROM expenses_staff_welfare WHERE site_id = ".CURRENT_SITE_ID." AND `date` LIKE '$d%'";
$staffWelfareReupsTotal = getValueFromQuery($sql, 'staffWelfareReupTotal');
#Total Purchases
#Get all of purchase of this month

$sql = "SELECT SUM(total) as staffWelfarePurchasesTotal FROM (
	(SELECT SUM(`cost`) AS total FROM expenses WHERE description = 'Welfare: Party, Meal, Staff gifts, Office coffee etc.' AND site_id = '" . CURRENT_SITE_ID . "' AND `date` LIKE '$d%')
	UNION
	(SELECT SUM(`out`-`in`) AS total FROM banking WHERE description = 'Welfare: Party, Meal, Staff gifts, Office coffee etc.' AND site_id = '" . CURRENT_SITE_ID . "' AND `date` LIKE '$d%')
) as allPurchases;"; //Note we do out - in as purchases being added up while anything going 'in' reduces the amount of money used to purchase

$staffWelfarePurchasesTotal = getValueFromQuery($sql, 'staffWelfarePurchasesTotal');

$staffWelfareStartingTotal = $previousMonthsReups - $previousMonthPurchases;
$staffWelfareRemaining = $staffWelfareStartingTotal + $staffWelfareReupsTotal - $staffWelfarePurchasesTotal;

#remaining
	#This months Reups + starting Value - purchases this month

/*
Sum all of the expenses_staff_welfare
SELECT SUM(cost) FROM expenses_staff_welfare WHERE site_id = CURRENT_SITE_ID;

$sql = "SELECT SUM(`in`-`out`) AS staffWelfareTotal FROM banking WHERE site_id = '" . CURRENT_SITE_ID . "' AND (analysis LIKE 'Staff Welfare%') AND `date` < '$d-01';";
//I think it should be calculated per month????
*/

//Calculate the Total expenses_staff_welfare
//Find the all of the staff welfare purchases and and staff welfare reups for the start of the month
/*
$sql = "SELECT SUM(`in`-`out`) AS staffWelfareTotal FROM banking WHERE site_id = '" . CURRENT_SITE_ID . "' AND (analysis LIKE 'Staff Welfare%' OR notes LIKE 'SW%') AND `date` < '$d-01';";
$results = mysql_query($sql);
while(($staffWelfareTotals[] = mysql_fetch_assoc($results)) || array_pop($staffWelfareTotals));

if(count($staffWelfareTotals)) {
    $staffWelfareStartingTotal = $staffWelfareTotals[0]['staffWelfareTotal'];
} else $staffWelfareStartingTotal = 0;

//Find all of the reups this month
$sql = "SELECT SUM(`in`-`out`) AS staffWelfareReupsTotal FROM banking WHERE site_id = '" . CURRENT_SITE_ID . "' AND `date` LIKE '$d%' AND notes = 'SW';";
$results = mysql_query($sql);
while(($staffWelfareReupsTotals[] = mysql_fetch_assoc($results)) || array_pop($staffWelfareReupsTotals));
if(count($staffWelfareReupsTotals)) {
    $staffWelfareReupsTotal = $staffWelfareReupsTotals[0]['staffWelfareReupsTotal'];
} else $staffWelfareReupsTotal = 0;

//all staffWelfarePurchases from banking and expenses
$sql = "SELECT SUM(total) as staffWelfarePurchasesTotal FROM (

	(SELECT SUM(`cost`) AS total FROM expenses WHERE analysis = 'Welfare: Party, Meal, Staff gifts, Office coffee etc.' AND site_id = '" . CURRENT_SITE_ID . "' AND `date` LIKE '$d%')

	UNION
	(SELECT SUM(`out`-`in`) AS total FROM banking WHERE analysis = 'Welfare: Party, Meal, Staff gifts, Office coffee etc.' AND site_id = '" . CURRENT_SITE_ID . "' AND `date` LIKE '$d%')

) as allPurchases;";
//Note we do out - in as purchases being added up while anything going 'in' reduces the amount of money used to purchase

$results = mysql_query($sql);
while(($staffWelfarePurchasesTotals[] = mysql_fetch_assoc($results)) || array_pop($staffWelfarePurchasesTotals));
if(count($staffWelfarePurchasesTotals)){
    $staffWelfarePurchasesTotal = $staffWelfarePurchasesTotals[0]['staffWelfarePurchasesTotal'];
} else $staffWelfarePurchasesTotal = 0;

if($staffWelfareStartingTotal   == null) $staffWelfareStartingTotal   = 0;
if($staffWelfareReupsTotal      == null) $staffWelfareReupsTotal      = 0;
if($staffWelfarePurchasesTotal  == null) $staffWelfarePurchasesTotal  = 0;
*/


echo '';
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Expenses</title>
    <?php include "../includes/head_scripts.php"; ?>
    <link rel="stylesheet" type="text/css" href="css/expenses_php.css" media="all"/>
</head>
<body>
<?php include 'includes/main_menu.php'; ?>
<?php include 'distribution.js.php'; ?>
<script>
<?php
//$staffWelfareStartingTotal   = 12345;
//$staffWelfareReupsTotal      = 432;
//$staffWelfarePurchasesTotal  = 1000;
?>
var staffWelfareStartingTotal   = <?php echo $staffWelfareStartingTotal ?>;
var staffWelfareReupsTotal      = <?php echo $staffWelfareReupsTotal ?>;
var staffWelfarePurchasesTotal  = <?php echo $staffWelfarePurchasesTotal ?>;

function row_update() {
    var id = $(this).attr('rel');
    var post_data = {};
    post_data.id = id;
    $(this).parent().parent().parent().children("td.value").each(function () {
        var field = $(this).attr('rel');
        post_data[field] = $(this).children('[name=' + field + ']').prop('value');
        if (field == 'shop_name' && $(this).children('[name=' + field + '_new]').prop('value') != '') {
            post_data[field + '_new'] = $(this).children('[name=' + field + '_new]').prop('value');
        }
        ;
    });
    $.ajax({
        url: 'ajax_handler.php?action=update_row',
        method: 'POST',
        data: post_data,
        context: $(this)
    }).done(function () {
        $(this).parent().parent().parent().children("td.value").each(function () {
            var field = $(this).attr('rel');
            if (field == 'shop_name' && $(this).children('[name=' + field + '_new]').prop('value') != '') {
                $(this).html($(this).children('[name=' + field + '_new]').prop('value'));
                fvalues[field].push({
                    'id': $(this).html(),
                    'text': $(this).html()
                });
                fvalues[field] = fvalues[field].sort(function (item1, item2) {
                    return (item1.text > item2.text);
                });
            } else {
                $(this).html($(this).children('[name=' + field + ']').prop('value'));
            }
            ;
        });
        $(this).prop('value', 'Change').removeClass('update').addClass('change').unbind('click').click(row_change);
        $(this).parent().children(".cancel").prop('value', 'Delete').removeClass('cancel').addClass('delete').unbind('click').click(row_delete);
        update_totals();
    });
    //reloading the page causes the update to fail randomly on the remote server??
    //the ajax request does not even seem to reach the server
    //location.reload();
}
function row_delete() {
    var id = $(this).attr('rel');
    if (window.confirm("Are you sure to delete this record?")) {
        //document.location = "?action=Delete&id=" + id;
        var post_data = {"id": id};
        $.ajax({
            url: 'ajax_handler.php?action=delete_row',
            method: 'POST',
            cache: false,
            data: post_data,
            context: $(this)
        }).done(function () {
            $(this).parent().parent().parent().remove();
            update_totals();
            //location.reload(); //this causes the ajax call to fail if placed at the end of the function
        }).fail(function () {
            alert('Record delete failed.');
        });
    }
    ;
}
function row_cancel() {
    var id = $(this).attr('rel');
    $(this).parent().parent().parent().children("td.value").each(function () {
        var field = $(this).attr('rel');
        $(this).html($(this).children('input[type=hidden]').prop('value'));
    });
    $(this).parent().children(".update").prop('value', 'Change').removeClass('update').addClass('change').unbind('click').click(row_change);
    $(this).prop('value', 'Delete').removeClass('camcel').addClass('delete').unbind('click').click(row_delete);
}

function row_change() {
    var id = $(this).attr('rel');
    $(this).parent().parent().parent().children("td.value").each(function () {
        var input_field = '';
        var field = $(this).attr('rel');
        var value = $(this).text();
        if (typeof field != 'undefined' && fvalues[field].length > 0) {
            input_field = $('<select>').prop('name', field).prop('id', field + '_edit');
            for (fv in fvalues[field]) {
                if (fvalues[field].hasOwnProperty(fv)) {
                    input_field.append(
                        $('<option>').attr('value', fvalues[field][fv].id).text(fvalues[field][fv].text)
                    );
                }
                ;
            }
            ;
            input_field.prop('value', $(this).text());
        } else {
            input_field = $('<input>').prop('name', field).prop('value', $(this).text()).prop('id', field + '_edit');
            if (['mk', 'sg', 'ib', 'ky'].indexOf(field) != -1) {
                input_field.css('text-align', 'center');
            }
        }
        ;
        $(this).html(input_field);
        $(this).append(
            $('<input>').prop('type', 'hidden').prop('name', field + '_old').prop('value', value)
        );
        if (field == 'shop_name') {
            $(this).append($('<br />')).append(
                $('<input>').prop('name', field + '_new').prop('id', field + '_edit')
            );
            $("input#shop_name_edit").autocomplete({
                minLength: 1,
                source: window.ac_values
            });
        }
        ;
    });
    $(this).prop('value', 'Update').removeClass('change').addClass('update').unbind('click').click(row_update);
    $(this).parent().children(".delete").prop('value', 'Cancel').removeClass('delete').addClass('cancel').unbind('click').click(row_cancel);
    $('.date, .date input, input[name=date]').datepicker({
        showAnim: 'fade',
        numberOfMonths: 1,
        dateFormat: 'yy-mm-dd',
        currentText: 'Today',
        maxDate: new Date(<?php echo $maxDate?>)
    });
}
;
function update_totals() {
    //why bother doing this in JS when you can do it in php?????

    var grand_total = 0;
    $("#expenses-table tr").each(function () {
        var cost = $(this).children('[rel=cost]').html();
        if (typeof cost != 'undefined') {
            grand_total += parseInt(cost);
        }
        ;
    });
    $('#totals-table tr').remove();
    $('#totals-table').append(
        $('<tr id="header-row">').append(
            $('<td>').html('Petty Cash Balance').attr('colspan', 2)
        )
    );
    $('#totals-table').append(
        $('<tr>').append(
            $('<td>').html('&nbsp;').attr('colspan', 2).addClass('noborder')
        )
    );
    $('#totals-table').append(
        $('<tr>').append(
            $('<th>').html('Starting')
        ).append(
            $('<td>').html('<?php echo $petty_balance; ?>')
        )
    );
    $('#totals-table').append(
        $('<tr>').append(
            $('<th>').html('+ PC Re-up')
        ).append(
            $('<td>').html('<?php echo $pc_reup; ?>')
        )
    );
    $('#totals-table').append(
        $('<tr>').append(
            $('<th>').html('- Total Expenses')
        ).append(
            $('<td>').html(grand_total)
        )
    );
    $('#totals-table').append(
        $('<tr>').append(
            $('<th>').html('Remaining')
        ).append(
            $('<td>').html(<?php echo $petty_balance; ?> +<?php echo $pc_reup; ?> - grand_total)
        )
    );

    //$("#staff-welfare-starting").html(staffWelfareStartingTotal);
    //$("#staff-welfare-reup").html(staffWelfareReupsTotal);
    //$("#staff-welfare-total-purchases").html(staffWelfarePurchasesTotal);
    //$("#staff-welfare-remaining").html(staffWelfareStartingTotal + staffWelfareReupsTotal - staffWelfarePurchasesTotal);
    //location.reload();
}
;
$(document).ready(function () {
    $('.date, .date input').datepicker({
        showAnim: 'fade',
        numberOfMonths: 1,
        dateFormat: 'yy-mm-dd',
        currentText: 'Today',
        defaultDate: '<?php echo date("Y-m-01", $current_time); ?>',
        maxDate: new Date(<?php echo $maxDate?>)
    });
    $(".delete").click(row_delete);
    $('.change').unbind('click').click(row_change);
    update_totals();
    window.ac_values = [];
    for (i in fvalues.shop_name) {
        if (fvalues.shop_name.hasOwnProperty(i)) {
            window.ac_values.push(fvalues.shop_name[i].text);
        }
        ;
    }
    ;
    $("input#shop_name-new").autocomplete({
        disabled: false,
        minLength: 1,
        source: window.ac_values
    });
});
var fvalues = {};
<?php
    foreach ($fields as $field) {
        echo "\tfvalues.$field = " . json_encode(array_key_exists($field, $values) ? $values[$field] : array()) . ";\n";
    };
?>
</script>
<table id="expenses-table">
    <tr>
        <td colspan="<?php echo sizeof($fields) + 1; ?>">
            <table border="0" cellspacing="0" cellpadding="0" width="100%" id="expenses-header">
                <tr>
                    <td colspan="2"><h1> Expenses </h1></td>
                </tr>
                <tr>
                    <td colspan="2"><h2><?php echo strtoupper(date("F Y", $current_time)); ?></h2></td>
                </tr>
                <tr>
                    <td style="text-align: left;"><a href="?date=<?php echo date("Y-m", $current_time - 30 * 24 * 60 * 60); ?>">&lt;&lt;&lt;&lt;&lt;&lt; Prev</a></td>
                    <td style="text-align: right;"><a href="?date=<?php echo date("Y-m", $current_time + 30 * 24 * 60 * 60); ?>">Next &gt;&gt;&gt;&gt;&gt;&gt;</a> </td>
                </tr>
            </table>
        </td>
    <tr>
        <?php
        foreach ($fields as $key => $field) {
            echo "\t<th id=\"$field-header\">{$headers[$key]}</th>\n";
        };
        ?>
        <th id="actions-header">Actions</th>
    </tr>
    <?php
    $d = date('Y-m-', $current_time);
    $sql = "select * from $record_type WHERE site_id = '" . CURRENT_SITE_ID . "' AND `date` LIKE '$d%' order by id ASC;";
    //echo "$sql <br>";
    $res = mysql_query($sql);
    $grand_total = 0;
    while ($row = mysql_fetch_assoc($res)) {
        $grand_total += $row['cost'];
        ?>
        <tr>
            <?php
            foreach ($fields as $key => $field) {
                echo "\t<td id=\"$field-{$row['id']}\" class=\"value $field\" rel=\"$field\">{$row[$field]}</td>\n";
            };
            ?>
            <?php if($user->getUserName() == 'sugai'): ?>

                <td>
                <nobr>
                    <input type="button" value="Change" rel="" class="change" hidden="hidden">
                    <input type="button" value="Delete" rel="" class="delete" hidden="hidden">
                </nobr>
            </td>

                <?php else: ?>
            <td id="action-change-delete">
                <nobr>
                    <input type="button" value="Change" rel="<?php echo $row['id']; ?>" class="change">
                    <input type="button" value="Delete" rel="<?php echo $row['id']; ?>" class="delete">
                </nobr>
            </td>
            <?php endif; ?>
        </tr>
    <?php
    };
    // update petty cash balance record
    //If you are going to update the record every time the pages loads
    //why even bother storing the old value???
    //petty cash table is useless
        //get rid of it
        //calculate the starting value for the month dynamically
    //
    /*
    $data = array(
        'balance' => $petty_balance + $pc_reup - $grand_total
    );
    $sql = "SELECT id FROM petty_cash WHERE site_id = '" . CURRENT_SITE_ID . "' and month = '" . str_replace("-", '', $d) . "'";
    echo "$sql <br>";
    $res = mysql_query($sql) or die(mysql_error());
    $action = 'insert';
    $where = '';
    if ($row = mysql_fetch_assoc($res)) {
        $action = 'update';
        $where = 'id=' . $row['id'];
    } else {
        $data['month'] = str_replace("-", '', $d);
        $data['site_id'] = CURRENT_SITE_ID;
    };
    db_perform('petty_cash', $data, $action, $where);
    */
    ?>
    <tr>
        <th id="add-new" colspan="<?php echo sizeof($fields) + 1; ?>">Add new record</th>
    </tr>
    <form method="POST">
        <tr>
            <?php
            foreach ($fields as $key => $field) {
                $record = array();
                $style = '';
                if (in_array($field, array('mk', 'sg', 'ib', 'ky'))) {
                    $record[$field] = 0;
                    $style = ' style="text-align: center !important;"';
                };
                $input_field = draw_input_field($field, $field, $record, $style, false);
                if (array_key_exists($field, $values)) {
                    $input_field = draw_pull_down_menu('i[' . $field . ']', $values[$field], '', 'id="' . $field . '"');
                };
                if ($field == 'shop_name') {
                    $input_field .= '<br />' . draw_input_field($field . '_new', $field . '-new', $record, '', false);
                };
                echo "\t<td id=\"$field-new\" class=\"$field\">" . $input_field . "</td>\n";
            };
            ?>
            <?php if($user->getUserName() == 'sugai'): ?>
                <td id="action-add"><input type="submit" name="action" value="Add" hidden="hidden"></td>
            <?php else: ?>
            <td id="action-add"><input type="submit" name="action" value="Add"></td>
            <?php endif; ?>
        </tr>
    </form>
</table>
<form>
    <input type="hidden" name="date" value="<?php echo date('Y-m', $current_time); ?>">
    <button name='action' value="CSV">Download CSV</button>
    <button name='distribution' type='button' value="CSV" id="distribution">Distribution Summary</button>
</form>
<table id="totals-table" border="0">
</table>

<table id="staff-welfare-reup-table" border="0" style="/*float: right;*/"><!--Staff Welfare Re Up Table-->
    <tbody>
        <tr id="header-row"><th colspan="3" style="text-align: center;">Staff Welfare Balance</td></tr>
        <tr>
            <td class="noborder" colspan="3">&nbsp;</td>
        </tr>
        <tr>
            <th>Starting</th>
            <td id="staff-welfare-starting"><?php echo $staffWelfareStartingTotal ?></td>
            <td></td>
        </tr>

        <tr>
            <th>+ SW Re-up</th>
            <td id="staff-welfare-total-reup"><?php echo $staffWelfareReupsTotal ?></td>
            <td>
                <?php if ($user->hasRole(array('SysAdmin'))): ?>
                <form action="expenses.php?action=swReup" method="POST"><input style="text-align: right;" type="text" name='swReupValue' value="0"><input type="submit" value="+Re-up" id="staff-welfare-reup-button"></form>
                <?php endif ?>
            </td>
        </tr>
        <tr>
            <th>- Total Purchases </th>
            <td id="staff-welfare-total-purchases"><?php echo $staffWelfarePurchasesTotal ?></td>
            <td></td>
        </tr>
        <tr>
            <th>Remaining</th>
            <td id="staff-welfare-remaining"><?php echo $staffWelfareRemaining?></td>
            <td></td>
        </tr>
    </tbody>
</table>

<div>
    <button id="back" onClick="javascript: document.location='<?php echo (CURRENT_SITE_ID == 0) ? '/Analysis/' : '/Bookkeeping/'; ?>'">Back</button>
</div>
<?php include("ticker.php"); ?>
</body>
</html>
