function connectionSpeedMeasurer(imageAddr, downloadSize, progressId, serverName) {
	var oProgress = document.getElementById(progressId);
	oProgress.innerHTML = "Loading the image, please wait...";

	var oProgress = document.getElementById(progressId);
	var startTime, endTime;
	var download = new Image();

	download.onload = function () {
		endTime = (new Date()).getTime();
		console.log("Onload");
		return showResults();
	}
	
	download.onerror = function (err, msg) {
		oProgress.innerHTML = "Invalid image, or error downloading";
	}
	
	startTime = (new Date()).getTime();
	var cacheBuster = "?nnn=" + startTime;
	download.src = imageAddr + cacheBuster;

	function showResults() {
		var duration = (endTime - startTime) / 1000;
		var bitsLoaded = downloadSize * 8;
		var speedBps = (bitsLoaded / duration).toFixed(2);
		var speedKbps = (speedBps / 1024).toFixed(2);
		var speedMbps = (speedKbps / 1024).toFixed(2);
		
		var trafficLight = '';

		if(speedMbps <= 0.7) {
			trafficLight = '/img/speedred.png';

		} else if(speedMbps <= 1.5) {
			trafficLight = '/img/speedyellow.png';

		} else {
			trafficLight = '/img/speedgreen.png';

		}

		oProgress.innerHTML = serverName + " " + 
		   //speedBps + " bps<br />"   + 
		   //speedKbps + " kbps<br />" + 
		   //duration + "s<br />" +
		   //bitsLoaded + "bits<br />" +
		   speedMbps + " Mbps" + " <img src='"+ trafficLight +"'>";
		console.log("showResults");
		return speedMbps;
	}
	console.log("Measure Connection Speed");
}


function measureSpeed() {
	var icdSpeed = connectionSpeedMeasurer("http://minakami.bungyjapan.com/Reception/imageupload.jpg", 9651979, "progress1", "bungyjapan.com Speed:");
	var awsSpeed = connectionSpeedMeasurer("http://minakami.bungyjapan.jp/imageupload.jpg", 9651979, "progress2", "Connection Speed:");

	//TODO because these functions happen asynchronously, the return values are not set
	//comparing the values is not simple
}

$(document).ready(function () {
	$("#speed-test").click(function() {measureSpeed()});
});

//The following divs need to be included for this script to work
/*
<button id="speed-test">Speed Test</button>
<div id="progress1"></div>
<br>
<div id="progress2"></div>
<br>
<div id="speed-test-results"></div>
 */


