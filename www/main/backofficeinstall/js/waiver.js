function scrollend(e) {
    document.lastScroll = null;
}

function scrollmove(e) {

    var targetEvent = e.originalEvent;
    if (typeof document.lastScroll == 'undefined' || document.lastScroll === null) {
        document.lastScroll = targetEvent.pageY;
    }

    var el = $(this).children();
    var old_scroll = $(el).prop('scroll');
    if (typeof old_scroll == 'undefined') {
        old_scroll = 0;
    }

    var scroll = old_scroll - targetEvent.pageY + document.lastScroll;
    if (scroll < 0) {
        scroll = 0;
    }

    if (scroll > ($(el).height() - $(el).parent().height())) {
        scroll = $(el).height() - $(el).parent().height();
    }
    ;

    $(el).css('webkitTransform', 'translate(0px,-' + scroll + 'px)');
    $(el).prop('scroll', scroll);
    document.lastScroll = targetEvent.pageY;
}

function bjmovestart(e) {
    document.bj_last_coords = e;
    bjmove(e);
}
function bjmove(e) {
    if (typeof document.bj_last_coords == 'undefined' || document.bj_last_coords === null) {
        document.bj_last_coords = e;
    }
    var c = $('#waiver-signature')[0];
    var ctx = c.getContext('2d');
    var rect = document.getElementById('waiver-signature').getBoundingClientRect();
    ctx.beginPath();
    ctx.moveTo(document.bj_last_coords.pageX - rect.left - window.scrollX, document.bj_last_coords.pageY - rect.top - window.scrollY);
    ctx.lineWidth = 15;
    ctx.lineCap = 'round';
    ctx.strokeStyle = 'blue';
    ctx.lineTo(e.pageX - rect.left - window.scrollX, e.pageY - rect.top - window.scrollY);
    ctx.stroke();
    document.bj_last_coords = e;
    document.bj_signature = true;
}

function bjmoveend(e) {
    var c = $('#waiver-signature')[0];
    var ctx = c.getContext('2d');
    document.bj_last_coords = null;
}

function save_var(name, value) {
    if (typeof document.bj_waiver == 'undefined' || document.bj_waiver == null) {
        document.bj_waiver = new Object();
    }
    ;
    document.bj_waiver[name] = value;
}

function submitForm(parameters) {
    if (!parameters) parameters = "";
    else parameters = "?" + parameters;

    document.waiverForm.action = "/selfCheckIn/controllerSelfCheckIn.php" + parameters;
    document.waiverForm.submit();
}

function goToStepWithUrl(url) {
    document.location = url;
}

function goToNextStep() {
    console.log("Next Step: " + getNextStep());
    console.log(pageSequence);
    document.location = getNextStep();
    //goToStepWithUrl(getNextStep());//Try this later as it should work the same
}

function goToPreviousStep() {
    //window.history.back();
    goToStepWithUrl(getPreviousStep());
    //document.location = getPreviousStep();
    //goToStepWithUrl(getNextStep());//Try this later as it should work the same
}

function saveWaiverDataToSessionAndGoToNextStep(waiverData) {
    //Todo check for valid save
    console.log("saveWaiverDataToSessionAndGoToNextStep is depricated replace all calls with saveWaiverDataToSession(data); goToNextStep();");
    saveWaiverDataToSession(waiverData);
    goToNextStep();

}

function getNextStep() {
    var nextPageIndex = indexOfPageInPageSequence(pageName) + 1;
    var nextPageUrl = pageSequence[nextPageIndex].pageUrl;
    return nextPageUrl;
}

function indexOfPageInPageSequence(pageName) {
    for (var i = 0; i < pageSequence.length; i++) {
        if (pageSequence[i].pageName == pageName) return i;
    }
    return null;
}

function getPreviousStep() {
    var previousPageIndex = indexOfPageInPageSequence(pageName) - 1;
    var previousPageUrl = pageSequence[previousPageIndex].pageUrl;
    return previousPageUrl;
}

function getCurrentPage() {
    var sPath = window.location.pathname;
    //find the last forward slash ie /Reception/xyz.js returns xyz.js
    sPath = sPath.substring(sPath.lastIndexOf('/') + 1);
    //get rid of GET parameters ie ?host=xyz&money=123
    var getParameters = sPath.indexOf("?");
    if (getParameters) {
        return sPath.substring(0, sPath.indexOf("?"));
    }
    return sPath;
}

function save_data() {
    return $.ajax(
        '/selfCheckIn/ajax.php?action=save-waiver',
        {
            data: document.bj_waiver,
            dataType: 'json',
            type: 'POST',
            async: false,
            cache: false,
            success: function (data) {
                return true;
            }
        }
    );
}

function removePaymentPagesFromPageSequence(pageSequence) {
    //once payment is complete, remove payment pages from the process so the back button skips those pages

    if (indexOfPageInPageSequence("buyPhotos"))      pageSequence.splice(indexOfPageInPageSequence("buyPhotos"), 1);
    if (indexOfPageInPageSequence("transaction"))    pageSequence.splice(indexOfPageInPageSequence("transaction"), 1);
    if (indexOfPageInPageSequence("action"))         pageSequence.splice(indexOfPageInPageSequence("action"), 1);
    //Or we could insert a stub page saying payment complete, please see staff if you would like to modify etc
}

function saveWaiverDataToSession(waiverData) {
    //This function is a modification to save what was being stored in document.bj_waiver to $_SESSION['bj_wavier']
    //As the original waiver system store state in document.bj_waiver, but no we are using separate pages
    //storing state in the page is not an option and we will store it on the server in $_SESSION
    return $.ajax(
        '/selfCheckIn/ajax.php?action=saveWaiverDataToSession',
        {
            data: waiverData,
            dataType: 'json',
            type: 'POST',
            async: false,
            cache: false,
            success: function (data) {
                return true;
            }
        }
    );
}

function clearSessionWaiverData() {
    //This function is a modification to save what was being stored in document.bj_waiver to $_SESSION['bj_wavier']
    //As the original waiver system store state in document.bj_waiver, but no we are using separate pages
    //storing state in the page is not an option and we will store it on the server in $_SESSION
    return $.ajax(
        '/selfCheckIn/ajax.php?action=clearSessionWaiverData',
        {
            data: null,
            dataType: 'json',
            type: 'POST',
            async: false,
            cache: false,
            success: function (data) {
                return true;
            }
        }
    );
}

function fill_names() {
    $.ajax(
        '/selfCheckIn/ajax.php?action=get-names',
        {
            dataType: 'json',
            cache: false,
            success: function (data) {
                console.log(data);
                if (data['error']) {
                    show_info(data['error']);
                } else {
                    if (pc_version == 0) {
                        $('#step2 ul > li').remove();
                    } else {
                        $('#step2 select > option').remove();
                    }
                    ;
                    //$('#step2 select').prop('multiple', false);
                    if (data['data'] && data['data'].length) {
                        for (var i in data['data']) {
                            if (!data['data'].hasOwnProperty(i)) continue;
                            if (pc_version == 0) {
                                var o = $('<li></li>');
                                o.prop('id', data['data'][i].id);
                                o.html(data['data'][i].name);
                                o.bind('click touchend MSPointerUp pointerup', function (e) {
                                    e.preventDefault();
                                    $('#step2 li').removeClass('selected');
                                    $(this).addClass('selected');
                                });
                                $('#step2 ul').append(
                                    o
                                );
                            } else {
                                var o = $('<option></option>')
                                $('#step2 select').append(
                                    o.prop('value', data['data'][i].id).prop('text', data['data'][i].name)
                                );
                            }
                            ;
                        }
                    }
                    ;
                    //$('#step2 select').prop('multiple', true);
                }
            },
        }
    );
}

function selectLanguage(lang) {
    lang = typeof lang !== 'undefined' ? lang : 'en';

    document.bungywaiver_lang = lang;
    //if the language is undefined, default to English
    if (lang == '') {
        document.bungywaiver_lang = "en";
        lang = "en"
    }

    console.log('.lang_' + lang);
    //$('.lang_' + lang).css('display', 'block');
    $('.lang_' + lang).show();
    //saveWaiverDataToSession({language: lang});
}

function show_info(msg) {
    var lang = {
        en: {
            "bookingname": "You must select the name under which your booking was made.",
            "validphoto": "You must enter a valid <br /><b>photo number</b><br /> or click<br /> the <b>\"No photos purchased\"</b> button.",
            "yeargreater": "The year must be greater than 1900.",
            "validyear": "The year must be greater than 1900.",
            "validmonth": "Please enter your month of birth.",
            "validday": "Please enter your day of birth.",
            "validday31": "Please enter your day of birth.",
            "validlast": "You must enter your last name to continue.",
            "validfirst": "You must enter your first name to continue.",
            "validphone": "Your phone number must contain from 9 to 11 digits.",
            "validemail": "Please enter valid email",
            "validsignature": 'Please draw your signature.',
            "errorsaving": 'some issues saving you credentials',
            "openslot":'Please select a time to jump'
        },
        ja: {
            "bookingname": "代表者様のお名前を<br />選択してください。",
            "validphoto": "お写真の番号を入力するか「ご購入されない方はこちら」のボタンを押してください。",
            "yeargreater": "１９００年以降でご入力ください。",
            "validyear": "１９００年以降でご入力ください。",
            "validmonth": "誕生月を入力してください。",
            "validday": "誕生日を入力してください。",
            "validday31": "誕生日を入力してください。",
            "validlast": "指で直接ご署名ください。",
            "validfirst": "名前を入力してください。",
            "validphone": "9文字～１１文字でご入力ください。",
            "validemail": '(例)  xxx@website.co.jp ',
            "validsignature": '指で直接ご署名ください。',
            "errorsaving": 'some issues saving you credentials',
            "openslot":'ご希望の時間帯を選んでください。'
        }
    }
    if (lang.hasOwnProperty(document.bungywaiver_lang) && lang[document.bungywaiver_lang].hasOwnProperty(msg)) {
        msg = lang[document.bungywaiver_lang][msg];
    }
    ;
    $('#info-dialog').html(msg);
    $('#info-dialog').dialog({
        buttons: [
            {
                text: (document.bungywaiver_lang == 'en') ? "Close" : "OK",
                width: 650,
                MSPointerUP: function () {
                    $('#info-dialog').dialog("close");
                },
                pointerup: function () {
                    $('#info-dialog').dialog("close");
                },
                click: function () {
                    $('#info-dialog').dialog("close");
                },
                touchend: function () {
                    $('#info-dialog').dialog("close");
                }
            }
        ],
        dialogClass: "alert",
        draggable: false,
        modal: true,
        position: {my: "center", at: "center", of: window},
        resizable: false,
        minWidth: 800,
        maxWidth: 1200,
        //html : msg,
        title: (document.bungywaiver_lang == 'en') ? "Information" : "注意"
    });
}
