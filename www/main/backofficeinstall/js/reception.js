function silentPrintIE(){
	if (navigator.appName == "Microsoft Internet Explorer") { 
		var PrintCommand = '<object ID="PrintCommandObject" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></object>';
		document.body.insertAdjacentHTML('beforeEnd', PrintCommand); 
		PrintCommandObject.ExecWB(6, -1); PrintCommandObject.outerHTML = ""; 
	} 
	else { 
		window.print();
	} 
}

function sleep(milliseconds) {
	var start = new Date().getTime();
	for (var i = 0; i < 1e7; i++) {
		if ((new Date().getTime() - start) > milliseconds){
			break;
		}
	}
}

function printToPrinter(printer, iframe){
	jsPrintSetup.setPrinter(printer);
	jsPrintSetup.setOption('marginTop', 1);
	jsPrintSetup.setOption('marginBottom', 1);
	jsPrintSetup.setOption('marginLeft', 1);
	jsPrintSetup.setOption('marginRight', 1);
	// set page header
	jsPrintSetup.setOption('headerStrLeft', '');
	jsPrintSetup.setOption('headerStrCenter', '');
	jsPrintSetup.setOption('headerStrRight', '&PT');
	// set empty page footer
	jsPrintSetup.setOption('footerStrLeft', '');
	jsPrintSetup.setOption('footerStrCenter', '');
	jsPrintSetup.setOption('footerStrRight', '');

	jsPrintSetup.setSilentPrint(true);
	jsPrintSetup.printWindow(window.frames[iframe]);
	jsPrintSetup.setSilentPrint(false);
}

function printTicketAndCertificate(ticketPrinter, certificatePrinter){
	printToPrinter(ticketPrinter, "ticketmini");
	setTimeOut(printToPrinter(certificatePrinter, "certificate"),5000);
}

function printForDemo(){
	//Sample code which demonstrate using of JSPrintSetup to setup print margins and call unattended print method (without print dialog).
	// set portrait orientation

	//jsPrintSetup.setPrinter(ticketPrinter);
	jsPrintSetup.setOption('marginTop', 1);
	jsPrintSetup.setOption('marginBottom', 1);
	jsPrintSetup.setOption('marginLeft', 1);
	jsPrintSetup.setOption('marginRight', 1);
	// set page header
	jsPrintSetup.setOption('headerStrLeft', 'Bungy Japan');
	jsPrintSetup.setOption('headerStrCenter', '');
	jsPrintSetup.setOption('headerStrRight', '&PT');
	// set empty page footer
	jsPrintSetup.setOption('footerStrLeft', '');
	jsPrintSetup.setOption('footerStrCenter', '');
	jsPrintSetup.setOption('footerStrRight', '');
	// Suppress print dialog

	jsPrintSetup.setSilentPrint(true);

	jsPrintSetup.print();
	//jsPrintSetup.print();
	jsPrintSetup.setSilentPrint(false);

	//sleep(20000);
	//console.log(jsPrintSetup.getPrintersList());
        //setTimeout(printCertificate(certificatePrinter), 5000);
	//silentPrintFF2();

}

function testPrint(ticketPrinter, certificatePrinter){
    jsPrintSetup.setPrinter(ticketPrinter);
    jsPrintSetup.setOption('marginTop', 1);
    jsPrintSetup.setOption('marginBottom', 1);
    jsPrintSetup.setOption('marginLeft', 1);
    jsPrintSetup.setOption('marginRight', 1);
    // set page header
    jsPrintSetup.setOption('headerStrLeft', 'Bungy Japan Ticket');
    jsPrintSetup.setOption('headerStrCenter', 'Bungy Japan Ticket');
    jsPrintSetup.setOption('headerStrRight', 'Bungy Japan Ticket');
    // set empty page footer
    jsPrintSetup.setOption('footerStrLeft', 'Bungy Japan Ticket');
    jsPrintSetup.setOption('footerStrCenter', 'Bungy Japan Ticket');
    jsPrintSetup.setOption('footerStrRight', 'Bungy Japan Ticket');
    // Suppress print dialog

    jsPrintSetup.setSilentPrint(true);

    jsPrintSetup.print();
    jsPrintSetup.setSilentPrint(false);

    setTimeout(function(){
        jsPrintSetup.setPrinter(certificatePrinter);
        jsPrintSetup.setOption('marginTop', 1);
        jsPrintSetup.setOption('marginBottom', 1);
        jsPrintSetup.setOption('marginLeft', 1);
        jsPrintSetup.setOption('marginRight', 1);
        // set page header
        jsPrintSetup.setOption('headerStrLeft', 'Bungy Japan Certificate');
        jsPrintSetup.setOption('headerStrCenter', 'Bungy Japan Certificate');
        jsPrintSetup.setOption('headerStrRight', 'Bungy Japan Certificate');
        // set empty page footer
        jsPrintSetup.setOption('footerStrLeft', 'Bungy Japan Certificate');
        jsPrintSetup.setOption('footerStrCenter', 'Bungy Japan Certificate');
        jsPrintSetup.setOption('footerStrRight', 'Bungy Japan Certificate');
        // Suppress print dialog

        jsPrintSetup.setSilentPrint(true);

        jsPrintSetup.print();
        jsPrintSetup.setSilentPrint(false);

    }, 5000);

}


function printTicket(ticketPrinter, certificatePrinter){
	//Sample code which demonstrate using of JSPrintSetup to setup print margins and call unattended print method (without print dialog).
	// set portrait orientation

	jsPrintSetup.setPrinter(ticketPrinter);
	jsPrintSetup.setOption('marginTop', 1);
	jsPrintSetup.setOption('marginBottom', 1);
	jsPrintSetup.setOption('marginLeft', 1);
	jsPrintSetup.setOption('marginRight', 1);
	// set page header
	jsPrintSetup.setOption('headerStrLeft', 'Bungy Japan');
	jsPrintSetup.setOption('headerStrCenter', '');
	jsPrintSetup.setOption('headerStrRight', '&PT');
	// set empty page footer
	jsPrintSetup.setOption('footerStrLeft', '');
	jsPrintSetup.setOption('footerStrCenter', '');
	jsPrintSetup.setOption('footerStrRight', '');
	// Suppress print dialog

	jsPrintSetup.setSilentPrint(true);

    console.log(window.frames['ticketmini']);
	jsPrintSetup.printWindow(window.frames['ticketmini']);
	//jsPrintSetup.print();
	jsPrintSetup.setSilentPrint(false);

	//sleep(20000);
	//console.log(jsPrintSetup.getPrintersList());
	setTimeout(printCertificate(certificatePrinter), 5000);
	//silentPrintFF2();

}

function printCertificate(certificatePrinter){
	jsPrintSetup.refreshOptions();
	jsPrintSetup.setSilentPrint(true);

	//jsPrintSetup.setPrinter('Canon LBP3100/LBP3108/LBP3150');
	jsPrintSetup.setPrinter(certificatePrinter);
	// set top margins in millimeters
	jsPrintSetup.setOption('marginTop', 1);
	jsPrintSetup.setOption('marginBottom', 1);
	jsPrintSetup.setOption('marginLeft', 1);
	jsPrintSetup.setOption('marginRight', 1);
	// set page header
	jsPrintSetup.setOption('headerStrLeft', '');
	jsPrintSetup.setOption('headerStrCenter', '');
	jsPrintSetup.setOption('headerStrRight', '&PT');
	// set empty page footer
	jsPrintSetup.setOption('footerStrLeft', '');
	jsPrintSetup.setOption('footerStrCenter', '');
	jsPrintSetup.setOption('footerStrRight', '');
	// Suppress print dialog
	jsPrintSetup.setSilentPrint(true);
	//jsPrintSetup.print();
	//would be a good idea to specify frame names in the function call instead of 
	//hard coding them
    console.log(window.frames['certificate']);
	jsPrintSetup.printWindow(window.frames['certificate']);

	// Do Print
	// Restore print dialog
	jsPrintSetup.setSilentPrint(false);
} 
