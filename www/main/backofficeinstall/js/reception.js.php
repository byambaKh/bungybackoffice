<script>
function procConvert(){//a copy of procConvert from moneyRecievedIE.php
    document.bookingForm.action = "convert_to_group.php?regid=<?php echo $regid; ?>";
    document.bookingForm.submit();
    return true;
}  

function procBack(){
	<?php if(!array_key_exists('converted_to_group', $_SESSION) || $_SESSION['converted_to_group'] != $regid) { ?>
		document.bookingForm.action = "dailyViewIE.php";
		document.bookingForm.submit();
		return true;
	<?php } else { ?>
		return convertGroupBookingToNormal();
	<?php }; ?>
}

function convertBookingToGroupBooking(redirect){
    document.bookingForm.action = "convert_to_group.php?regid=<?php echo $regid; ?>&redirect=" + redirect;
    document.bookingForm.submit();
    return true;
}

function convertGroupBookingToNormal(){
    document.bookingForm.action = "convert_to_normal.php?regid=<?php echo $regid; ?>";
    document.bookingForm.submit();
    return true;
}
</script>

