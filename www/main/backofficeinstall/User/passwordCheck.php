<?php
//if the length is less than $length return false
function isLongEnough($password, $length){
	return mb_strlen($password) >= $length;
}

//if the password does not contain numbers and letters return false
function containsNumbers($password){
    if(preg_match('/[0-9]/', $password)) return true;
    return false;
}

function containsLetters($password){
    if(preg_match('/[A-Za-z]/', $password)) return true;
    return false;
}

function containsNumbersAndLetters($password){
	return containsNumbers($password) && containsLetters($password);
}

//check it is not the same as username
//Block passwords like Bungy for username bungy
function doesNotContainWord($checkedString, $bannedWord){
	$checkedString = strtolower($checkedString);
	$bannedWord = strtolower($bannedWord);
    $position = strpos($checkedString, $bannedWord);

	if($position === FALSE) return true;
	else return false;
}

function doesNotContainUsername($password, $username){
	return doesNotContainWord($password, $username);
}

//calls all of the specified functions and returns true or false and a message
function checkPassword($password, $username, $minLength){
	return array(
		"containsNumbers"			=> containsNumbers($password),
		"containsLetters"			=> containsLetters($password),
		"isLongEnough"				=> isLongEnough($password, $minLength),
		"doesNotContainUsername"	=> doesNotContainUsername($password, $username)
	);
}

////$username	= "Bungy";
////$password	= "bungybungybungy";
///$minLength  = 12;

//echo (checkLength($password, $minLength) ? "Pass" : "Fail")."\n";
//echo (checkLength("asdfasdf", $minLength) ? "Pass" : "Fail")."\n";
//echo (checkLength("adsf", $minLength) ? "Pass" : "Fail")."\n";

//echo (doesNotContainUsername("bungyBungyBUNGY", "bungy") ? "Pass" : "Fail")."\n";
//echo (doesNotContainUsername("bungeeey", "bungy") ? "Pass" : "Fail")."\n";

//print_r(checkPassword("Bungy", "bungy", $minLength));

//print_r(checkPassword("BungyBunngy12", "bungy", $minLength));


?>
