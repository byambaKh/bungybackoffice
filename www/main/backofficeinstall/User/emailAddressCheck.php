<?php
function isValidEmailAddress($emailAddress){
    if(filter_var($emailAddress, FILTER_VALIDATE_EMAIL)) return true;
}

function isUniqueEmailAddress($emailAddress){
    //checks for duplicate email addresses in the database
    //TODO impliment this later on as we don't need it now
    return true;
}

function emailAddressesMatch($email1, $email2){
    if($email1 == $email2) return true;
    return false;
}

function checkEmailAddress($emailAddress1, $emailAddress2){
    return array(
        'isValidEmailAddress'   => isValidEmailAddress($emailAddress1),
        'isUniqueEmailAddress'  => isUniqueEmailAddress($emailAddress1),
        'emailAddressesMatch'   => emailAddressesMatch($emailAddress1, $emailAddress2)
    );
}

function userHasEmailAddress($userId){//check if the user has an email address
    $queryUser = "SELECT * FROM users WHERE UserID = $userId";
    $resource = mysql_query($queryUser);

    if($row = mysql_fetch_assoc($resource)){
        if($row['emailAddress1']) return true;
    }

    return false;
}
//TESTS
//print_r(checkEmailAddress("deji@bungyjapan.com", "deji@bungyjapan.com"));//all should be true
//print_r(checkEmailAddress("deji@bungyjapan.com", "deji@google.com"));//mismatch
//print_r(checkEmailAddress("deji.com", "deji.com"));//not an email
?>
