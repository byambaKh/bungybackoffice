<?php
$preventPasswordChangeRedirect = true;

include '../includes/application_top.php';
require_once('passwordCheck.php');
require_once('emailAddressCheck.php');

/*TODO put these in a central config file*/
$noReplyMailAddress = "noreply@bungyjapan.com";
$supportEmailAddress = "deji@standardmove.com";

$inEnglish = $lang == 'en';

$minimumPasswordLength = 8;
$alreadyHaveAnEmailAddress = userHasEmailAddress($_SESSION['uid']);
if (isset($_POST['new_password'])) {

    $validEmail = false;
    //if we don't have an email address, this part of the update will be skipped as it will not show in the form
    if (!$alreadyHaveAnEmailAddress) {
        $emailValidity = checkEmailAddress($_POST['emailAddress1'], $_POST['emailAddress2']);

        $isValidEmailAddress = $emailValidity['isValidEmailAddress'];
        $isUniqueEmailAddress = $emailValidity['isUniqueEmailAddress'];
        $emailAddressesMatch = $emailValidity['emailAddressesMatch'];

        if (!$isValidEmailAddress) $_SESSION['password_error'] = $inEnglish ? "Error. You must supply a valid email address." : "エラー：あなたは有効な電子メールアドレスを選択しなければいけません。";
        if (!$isUniqueEmailAddress) $_SESSION['password_error'] = $inEnglish ? "Error. The email address supplied is already in use. Please try another one" : "エラー：選択した電子メールアドレスが既に使用済みの場合は他のアドレスを選び直して下さい。";
        if (!$emailAddressesMatch) $_SESSION['password_error'] = $inEnglish ? "Error. Both email addresses must match." : "エラー：両方の電子メールアドレスが一致しなければなりません。";

        $validEmail = $isValidEmailAddress && $isUniqueEmailAddress && $emailAddressesMatch;
    }

    $passwordValidity = checkPassword($_POST['new_password'], $_SESSION['myusername'], $minimumPasswordLength);

    $isLongEnough = $passwordValidity['isLongEnough'];
    $doesNotContainUsername = $passwordValidity['doesNotContainUsername'];
    $containsNumbers = $passwordValidity['containsNumbers'];
    $containsLetters = $passwordValidity['containsLetters'];
    $doesNotMatchOldPassword = $_POST['new_password'] != $_POST['old_password'];

    if ((!$containsNumbers) || (!$containsLetters)) $_SESSION['password_error'] = $inEnglish ? "Error. Your new password must contain both numbers and letters." : "エラー：あなたの新しいパスワードは数字と文字の両方を含まなければなりません。";
    if (!$isLongEnough) $_SESSION['password_error'] = $inEnglish ? "Error. Your new password must be $minimumPasswordLength characters or more in length." : "あなたの新しいパスワードは{$minimumPasswordLength}文字数がもっと必要です。";
    if (!$doesNotContainUsername) $_SESSION['password_error'] = $inEnglish ? "Error. Your new password must not contain your username." : "エラー：あなたの新しいパスワードはあなたのユーザーネイムを含んではなりません。";
    if (!$doesNotMatchOldPassword) $_SESSION['password_error'] = $inEnglish ? "Error. Your new password must not be the same as the old password." : "エラー：あなたの新しいパスワードは古いパスワードと同じではいけません。";

    $validPassword = $containsNumbers && $containsLetters && $doesNotContainUsername && $isLongEnough && $doesNotMatchOldPassword;

    if ($validPassword && $validEmail || $validPassword && $alreadyHaveAnEmailAddress) {
        $sql = "SELECT * FROM users WHERE username = '" . db_input($_SESSION['myusername']) . "';";
        $res = mysql_query($sql) or die(mysql_error());
        if (($row = mysql_fetch_assoc($res)) && $row['UserPwd'] == $_POST['old_password']) {
            $uid = $row['UserID'];
        } else {
            $_SESSION['password_error'] = "Error. Wrong old password!";
            Header("Location: /User/change_password.php");
            die();
        };
        if ($_POST['new_password'] != $_POST['new_password_repeat']) {
            $_SESSION['password_error'] = "Error. Both new passwords must match!";
            Header("Location: /User/change_password.php");
            die();
        };
        if ($alreadyHaveAnEmailAddress)
            $data = array('UserPwd' => $_POST['new_password']);
        else
            $data = array('UserPwd' => $_POST['new_password'], 'emailAddress1' => $_POST['emailAddress1']);

        $user = new BJUser();
        $user->setShouldUpdatePassword(false);

        db_perform('users', $data, 'update', 'UserID=' . $uid);
        $_SESSION['password_success'] = "Password changed successfully!";

        $headers = "From: $noReplyMailAddress" . "\r\n" . "Reply-To: $noReplyMailAddress" . "\r\n" . "X-Mailer: PHP/" . phpversion();

        //if the user is not submitting an email address, use what is stored in the database
        $userEmail = $_POST['emailAddress1'] ? $_POST['emailAddress1'] : $row['emailAddress1'];

        if ($inEnglish)
            mail("$userEmail, $supportEmailAddress", "Bungy Japan Password Update", "Dear {$_SESSION['myusername']},\nThank you for updating your password. \nRegards,\nBungy Japan", $headers);
        else
            mail("$userEmail, $supportEmailAddress", "バンジージャパンパスワード更新", "親愛なる{$_SESSION['myusername']}さん\nあなたのパスワードを更新して下さってありがとうございます。\n敬具\nバンジージャパン ", $headers);

        Header("Location: /User/change_password.php");
        die();
    }
};
?>
<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=8">
    <link rel="stylesheet" href="/Booking/css/style.css" type="text/css"/>
    <title><?php echo ($lang == 'en') ? 'Change Password for ' . $_SESSION['myusername'] : sprintf("「%s」のパスワード変更", $_SESSION['myusername']); ?></title>
</head>
<body>
<style>
    .error {
        color: red;
        width: 100%;
        text-align: center;
        font-size: 20px;
    }

    .success {
        color: green;
        width: 100%;
        text-align: center;
        font-size: 20px;
    }

    div {
        text-align: center;
        font-family: Verdana;
        margin-right: auto;
        margin-left: auto;
        padding-right: auto;
        padding-left: auto;
    }

    div table {
        background-color: #B22222;
        font-family: Verdana;
        border-collapse: collapse;
        border: solid 1px #B22222;
        margin-right: auto;
        margin-left: auto;
    }

    div table td {
        padding: 5px;
    }

    ul {
        padding-left: 2em;
        color: white;
    }

    p {
        color: white;
    }

    h3 {
    }
</style>
<form method="post">
    <?php
    if (isset($_SESSION['password_error'])) {
        echo "<div id='password_error' class='error'>" . $_SESSION['password_error'] . '</div>';
    };
    if (isset($_SESSION['password_success'])) {
        echo "<div id='password_success' class='success'>" . $_SESSION['password_success'] . '</div>';
    };
    ?>
    <div>
        <br/>
        <?php if (isset($_SESSION['password_success']) && isset($_SESSION['shouldRedirectTo'])): ?>
            <script>
                setTimeout(function () {
                    document.location = "<?php echo $_SESSION['shouldRedirectTo'];?>";
                }, 5000);

            </script>
            <?php unset($_SESSION['shouldRedirectTo']) ?>
            <?php
            //if password_success and shouldRedirect are true, only show the success message then redirect
            //if only password success is true, then set it to false so that the rest of the page shows
            //as we will not be redirecting anywhere
            ?>
        <?php elseif (isset($_SESSION['password_success'])): unset($_SESSION['password_success']) ?>
        <?php endif ?>

        <?php if (!isset($_SESSION['password_success'])): ?>
            <table border="0">
                <tbody>
                <tr>
                    <td align="center" bgcolor="#000000" colspan="6">
                        <h3><font
                                color="#FFFFFF"><?php echo ($lang == 'en') ? 'Change Password for ' . $_SESSION['myusername'] : sprintf("「%s」のパスワード変更", $_SESSION['myusername']); ?></font>
                        </h3>
                    </td>
                </tr>
                <tr>
                    <td bgcolor="#000" colspan="2">
                        <br>
                        <?php if($inEnglish): ?>
                            <p>IMPORTANT! Your new password must:</p>
                            <ul>
                                <li>Be <?php echo $minimumPasswordLength ?> or more characters long.</li>
                                <li>Contain both numbers and letters.</li>
                                <li>Must NOT contain your username.</li>
                            </ul>
                        <?php else: ?>
                            <p>重要！あなたの新しいパスワードは:</p>
                            <ul>
                                <li>長さ<?php echo $minimumPasswordLength ?>文字または<?php echo $minimumPasswordLength ?>文字以上でなくてはいけません。</li>
                                <li>数字と文字の両方を含まなければいけません。</li>
                                <li>あなたのユーザーネイムを含んではいけません。</li>
                            </ul>
                        <?php endif ?>
                        <br>
                    </td>
                </tr>
                <tr>
                    <td align="right" bgcolor="#B22222" valign="middle"><font
                            color="#FFFFFF"><?php echo ($lang == 'en') ? 'Old Password' : '前回迄のパスワードを入力'; ?>:</font>
                    </td>
                    <td align="center" bgcolor="#B22222" valign="middle"><input type="password" name="old_password">
                    </td>
                </tr>
                <tr>
                    <td align="right" bgcolor="#B22222" valign="middle"><font
                            color="#FFFFFF"><?php echo ($lang == 'en') ? 'New Password' : '新しいパスワードを入力'; ?>:</font></td>
                    <td align="center" bgcolor="#B22222" valign="middle"><input type="password" name="new_password">
                    </td>
                </tr>
                <tr>
                    <td align="right" bgcolor="#B22222" valign="middle"><font
                            color="#FFFFFF"><?php echo ($lang == 'en') ? 'Re-Type New Password' : '新しいパスワードを再入力'; ?>
                            :</font></td>
                    <td align="center" bgcolor="#B22222" valign="middle"><input type="password"
                                                                                name="new_password_repeat"></td>
                </tr>
                <?php if (!$alreadyHaveAnEmailAddress): ?>
                    <tr>
                        <td align="right" bgcolor="#B22222" valign="middle"><font
                                color="#FFFFFF"><?php echo ($lang == 'en') ? 'Email Address' : 'Eメール'; ?>:</font></td>
                        <td align="center" bgcolor="#B22222" valign="middle"><input type="text" name="emailAddress1">
                        </td>
                    </tr>
                    <tr>
                        <td align="right" bgcolor="#B22222" valign="middle"><font
                                color="#FFFFFF"><?php echo ($lang == 'en') ? 'Email Address Again' : 'Eメールもう一回'; ?>
                                :</font></td>
                        <td align="center" bgcolor="#B22222" valign="middle"><input type="text" name="emailAddress2">
                        </td>
                    </tr>
                <?php endif ?>
                <tr>
                    <td align="left" bgcolor="#000000" valign="middle">
                        <input id="back" type="button" style="width:76;background-color:#FFFF66" name="back"
                               value="<?php echo ($lang == 'en') ? 'Home' : '<< 戻る'; ?>"
                               onClick="document.location='/';">
                    </td>
                    <td align="right" bgcolor="#000000" valign="middle">
                        <input type="submit" style="width:140;background-color:#FFFF66" name="change_password"
                               value="<?php echo ($lang == 'en') ? 'Change Password' : '登録'; ?>">
                    </td>
                </tr>
                <tr>
                    <td colspan="2" align="left" bgcolor="#000000" valign="middle">
                        <input id="" type="button" style="width:76;background-color:#FFFF66" name="Logout"
                               value='Logout'
                               onClick="document.location='/?logout=true';">

                        <?php //echo "<a href='/?logout=true'>Logout</a>"; ?>
                    </td>
                </tr>
                </tbody>
            </table>
        <?php endif ?>
    </div>
</form>
</body>
</html>
<?php
//clear all warning and success messages
if (isset($_SESSION['password_success'])) unset($_SESSION['password_success']);
if (isset($_SESSION['password_error'])) unset($_SESSION['password_error']);
?>
