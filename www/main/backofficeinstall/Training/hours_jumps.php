<?php
	function getTotalTrHours($uid, $month) {
		$sql = "SELECT * FROM roster_function_all;";
		$res = mysql_query($sql) or die(mysql_error());
		$functions = array();
		while ($row = mysql_fetch_array($res)) {
			$name = '';
			switch ($row['name']) {
				case 'RFT':
					$name = 'Raft';
					break;
				case 'REC':
					$name = 'Reception';
					break;
				case 'CS':
				case 'CSR':
				case 'CSB':
					$name = 'Customer Service';
					break;
				case 'JO':
				case 'JO1':
				case 'JO2':
					$name = 'Jump Operator';
					break;
				case 'JM':
				case 'JM1':
				case 'JM2':
					$name = 'Jump Master';
					break;
				case 'JC':
					$name = 'Site Manager';
					break;
				case 'P&V':
				case 'P&V1':
				case 'P&V2':
					$name = 'Photographer';
					break;
			};
			if (!empty($name)) {
				if (!array_key_exists($name, $functions)) {
					$functions[$name] = array();
				};
				$functions[$name][] = $row['id'];
			};
		};
		$total = array();
		foreach ($functions as $func_name => $ids) {
			$total[$func_name] = array('hours' => 0, 'jumps' => 0);
			if (empty($ids)) {
				continue;
			};
			$roster_month = str_replace('-', '', $month);
			$sql = "
				SELECT rdf.*, rt.roster_date as real_date
				FROM roster_staff rs
				LEFT JOIN roster_position rp on (rp.site_id = '".CURRENT_SITE_ID."' and rp.roster_month = '$roster_month' and rs.place = rp.place)
				LEFT JOIN roster_target rt on (rt.site_id =  '".CURRENT_SITE_ID."' and rt.roster_date like '$month-%' and DATE_FORMAT(rt.roster_date, '%e') = rp.roster_day)
				LEFT JOIN roster_daily_templates rdt on (rdt.site_id =  '".CURRENT_SITE_ID."' and rt.jumps >= rdt.min_jumps and rt.jumps <= rdt.max_jumps)
				LEFT JOIN roster_daily_pos rdp on (rdp.site_id = '".CURRENT_SITE_ID."' and rdt.id = rdp.template_id and rdp.position_id = rp.position_id)
				LEFT JOIN roster_daily_funcs rdf on (rdf.site_id = '".CURRENT_SITE_ID."' and rdf.template_id = rdt.id and (rt.roster_date = rdf.roster_date OR rdf.roster_date = '0000-00-00') and rdp.place = rdf.place)
				WHERE rs.site_id = '".CURRENT_SITE_ID."' and rs.staff_id = '$uid' and rs.roster_month = '$roster_month' and rdf.roster_func_id in (".implode(', ', $ids).") 
				ORDER BY real_date ASC, rdf.roster_date DESC
			";
			$res = mysql_query($sql) or die(mysql_error());
			$without_template = false;
			$last_date = $month . '-01';
			$jump_times = array();
			while ($row = mysql_fetch_assoc($res)) {
				if ($row['real_date'] != $last_date) {
					$without_template = false;
					$last_date = $row['real_date'];
				};
				if ($row['roster_date'] != '0000-00-00') {
					$without_template = true;
				};
				if ($without_template && $row['roster_date'] == '0000-00-00') {
					continue;
				};
				$total[$func_name]['hours'] += 0.25;
				$time = $row['roster_time'];
				$time = preg_replace("/([\d]{2}):(15)/", "\\1:00", $time);
				$time = preg_replace("/([\d]{2}):(45)/", "\\1:30", $time);
				$jump_times[$row['real_date']][] = $time;
			};
			foreach ($jump_times as $jdate => $jtimes) {
				$jtimes = array_unique($jtimes);	
				sort($jtimes);
				$jtimes = array_map(function ($item) {
					if ($item < '12:00') {
						$item .= ' AM';
					} else {
						$item .= ' PM';
					};
					return $item;
				}, $jtimes);
				$sql = "SELECT sum(NoOfJump) as total from customerregs1 
						WHERE site_id = '".CURRENT_SITE_ID."' and
							BookingDate = '$jdate'
							and BookingTime in ('".implode("', '", $jtimes)."')
							and DeleteStatus = 0
							and Checked = 1 
				";
				$res = mysql_query($sql) or die(mysql_error());
				if ($row = mysql_fetch_assoc($res)) {
					$total[$func_name]['jumps'] += $row['total'];
				};
			};
		};
		return $total;
	}
?>
