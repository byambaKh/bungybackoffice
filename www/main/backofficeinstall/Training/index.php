<?php
include '../includes/application_top.php';
include '../Operations/reports_save.php';
include 'training_fields.php';
include '../Bookkeeping/functions.php';
include 'hours_jumps.php';

$debug = false;
// be sure we works with main DB
mysql_set_charset('utf8');
mysql_query("SET NAMES 'utf8'");
$can_edit = false;
if ($user->hasRole(array('Site Manager', 'General Manager', 'SysAdmin'))){
    $can_edit = true;
} else {
    $_SESSION['tid'] = $_SESSION['uid'];
    $tid = $_SESSION['tid'];//trainee id
};
$record_type = 'training';
$current_time = time();
$d = date('Y-m', $current_time);
if (isset($_GET['date'])) {
    $current_time = mktime(0, 0, 0, substr($_GET['date'], 5, 2), 15, substr($_GET['date'], 0, 4));
} else {
    $current_time = mktime(0, 0, 0, substr($d, 5, 2), 15, substr($d, 0, 4));
};
//this looks dangerous. Why would we set the session tid from GET???? Setting a session variable from GET looks risky.
if ($can_edit && isset($_GET['tid'])) {
    $_SESSION['tid'] = $_GET['tid'];
};
$fields = array_map(function ($item) {
    return strtolower(
        preg_replace(
            "/(_)$/",
            "",
            preg_replace("([^\w]+)", "_", $item)
        )
    );
}, array_keys($headers));
// Prepare month drop down
$month_values = array();
for ($j = 0; $j > -2; $j--) {
    for ($i = (!$j) ? date("m", time()) : 12; $i > 0; $i--) {
        $mtime = mktime(0, 0, 0, $i, 15, $j + date("Y"));
        $month_values[] = array(
            'id'   => date("Y-m", $mtime),
            'text' => date("M Y", $mtime)
        );
    };
};
// get staff list
if ($can_edit) {
    $iDStaffPlus = BJHelper::getAccessTypeID('staff');
    $iDStaff = BJHelper::getAccessTypeID('staffplus');
    $iDSiteManager = BJHelper::getAccessTypeID('smanager');

    $sql = "SELECT distinct users.UserID, users.* FROM users, user_roles WHERE user_roles.access_type_id IN ($iDStaff, $iDStaffPlus, $iDSiteManager) AND user_roles.user_id = users.UserID and (user_roles.site_id = '" . CURRENT_SITE_ID . "' OR user_roles.site_id = 0);";
    if ($debug) echo "$sql<br><br>";
    $res = mysql_query($sql) or die(mysql_error());
    $staff_list = array();
    $first_id = null;
    $current_id_found = false;
    while ($row = mysql_fetch_assoc($res)) {
        if (!isset($_SESSION['tid'])) {
            $_SESSION['tid'] = $row['UserID'];
        };
        if (is_null($first_id)) {
            $first_id = $row['UserID'];
        };
        if ($_SESSION['tid'] == $row['UserID']) {
            $current_id_found = true;
        };
        $staff_list[] = array(
            'id'   => $row['UserID'],
            'text' => $row['StaffListName'] ? $row['StaffListName'] : $row['UserName']
        );
    };
    if (!$current_id_found) {
        $_SESSION['tid'] = $first_id;
        $tid = $_SESSION['tid'];
    };
};

if (isset($_GET['tid']) && $can_edit) $tid = $_GET['tid'];//Only allow management+ to specify trainer ids by GET

// get current level for user
$sql = "SELECT * FROM training_users WHERE uid = '$tid';";
if ($debug) echo "$sql<br><br>";
$res = mysql_query($sql) or die(mysql_error());
$current_level = 'REC 2';
if ($row = mysql_fetch_assoc($res)) {
    $current_level = $row['current_level'];
};
// eof month
$form_fields = array();
foreach ($headers as $key => $sub_fields) {
    $field = $to_field($key);
    if (in_array($field, array('month', 'comments'))) {
        $form_fields[] = $field;
        continue;
    };
    foreach ($sub_fields as $sub_field) {
        $form_fields[] = $field . '_' . strtolower($sub_field);
    };
};
$sql = "SELECT * FROM training_settings WHERE id = '" . CURRENT_SITE_ID . "'";
if ($debug) echo "$sql<br><br>";
$res = mysql_query($sql) or die(mysql_error());
$settings = array();
if (!($settings = mysql_fetch_assoc($res))) {
    foreach ($form_fields as $field) {
        $settings[$field] = 1000000;
    };
};
$values = array();
$values['month'] = $month_values;
// check if needed table exists

$action = $_POST['action'];
if (empty($action)) {
    $action = $_GET['action'];
};
if ($can_edit) {
    switch (TRUE) {
        case ($action == 'update_level'):
            $data = array(
                'uid'           => $_POST['tid'],
                'current_level' => $_POST['current_level']
            );
            $sql = "SELECT * FROM training_users WHERE uid = '{$_POST['tid']}'";
            if ($debug) echo "$sql<br><br>";
            $res = mysql_query($sql) or die(mysql_error());
            if ($row = mysql_fetch_assoc($res)) {
                $action = 'update';
                $where = 'id=' . $row['id'];
            } else {
                $action = 'insert';
                $where = '';
            };
            db_perform('training_users', $data, $action, $where);
            Header("Location: /Training/?tid=" . $_SESSION['tid']);
            die();
            break;
        case ($action == 'Add'):
            $_POST['i']['date'] = $_POST['i']['month'] . '-01';
            $_POST['i']['uid'] = $tid;
            $_POST['i']['site_id'] = CURRENT_SITE_ID;
            save_report($record_type, $_POST['i']);
            Header("Location: /Training/?tid=" . $_SESSION['tid']);
            die();
            break;
        case ($action == 'Update'):
            $_POST['i']['uid'] = $tid;
            $_POST['i']['site_id'] = CURRENT_SITE_ID;
            save_report($record_type, $_POST['i']);
            Header("Location: /Training/?tid=" . $_SESSION['tid']);
            die();
            break;
        case ($action == 'Delete'):
            $sql = "delete from training where id = '{$_GET['id']}'";
            mysql_query($sql) or die(mysql_error());
            Header("Location: /Training/?tid=" . $_SESSION['tid']);
            die();
            break;
        case ($action == 'CSV'):
            $current_month = date("Y-m", $current_time);
            Header('Content-Type: text/csv; name="training' . $current_month . '.csv"');
            Header('Content-Disposition: attachment; filename="training' . $current_month . '.csv"');
            echo '"' . implode('","', $headers) . '"' . "\r\n";
            //for ($i = 1; $i <= 12; $i++) {
            //$d = $current_year . '-' . sprintf("%02d-", $i);
            $sql = "select * from training WHERE `date` like '$current_month%' order by `date` ASC;";
            if ($debug) echo "$sql<br><br>";
            $res = mysql_query($sql);
            $balance = 0;
            while ($row = mysql_fetch_assoc($res)) {
                foreach ($fields as $field) {
                    echo '"' . $row[$field] . '"' . ($field == $fields[sizeof($fields) - 1] ? '' : ',');
                };
                echo "\r\n";
            };
            //};
            die();
            break;
    };
};
// Check all needed data in DB
//Calculate all hours for each section (Raft, Reception, Customer Service)
//the results of this get reinsterted in to 'training' deleting a row from training will result in
//This calculates the dates worked each month, and dates tests taken
//then they are reinserted in to training...
$sql = "
		SELECT roster_month, SUM(`work`) AS `work` FROM (
			(
				SELECT rs.roster_month, sum(rp.position_id) AS `work`
				FROM roster_staff rs 
				LEFT JOIN roster_position rp ON (rp.site_id = '" . CURRENT_SITE_ID . "' AND rs.place = rp.place AND rs.roster_month = rp.roster_month)
				WHERE rs.site_id = '" . CURRENT_SITE_ID . "' AND rs.staff_id = '$tid' GROUP BY rs.roster_month
			) UNION (
				SELECT DATE_FORMAT(ttr.take_date, '%Y%m') AS roster_month, COUNT(ttr.test_score) AS `work`
				FROM training_manuals tm 
				LEFT JOIN training_tests tt ON (tm.id = tt.manual_id)
				LEFT JOIN training_tests_results ttr ON (tt.id = ttr.test_id AND ttr.user_id = '$tid')
				WHERE tm.site_id = '" . CURRENT_SITE_ID . "' AND take_date IS NOT NULL GROUP BY SUBSTRING(take_date, 0, 7)
			)
		) AS t GROUP BY roster_month;";
/*GROUP BY Substring so that the date returned is not null, take_date IS NOT NULL prevents 0000-00-00 date from being returned as part of the dates*/
if ($debug) echo "$sql<br><br>";
//echo "$sql<br><br>";
$res = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_assoc($res)) {
    // check if training record exists for this month
    if (!empty($row['work'])) {
        $tmonth = preg_replace("/([\d]{4})([\d]{2})/", "\\1-\\2", $row['roster_month']);
        //The training table looks like it is not in use, mostly empty
        $sql = "SELECT * FROM training WHERE site_id = '" . CURRENT_SITE_ID . "' and month = '$tmonth' and uid = '$tid';";
        if ($debug) echo "$sql<br><br>";
        $tres = mysql_query($sql) or die(mysql_error());
        if (mysql_num_rows($tres) == 0) {
            $data = array(
                'site_id' => CURRENT_SITE_ID,
                'uid'     => $tid,
                'date'    => $tmonth . '-01',
                'month'   => $tmonth
            );
            db_perform('training', $data);
        };
    };
};
// EOF check
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Training</title>
    <?php include "../includes/head_scripts.php"; ?>
    <style>
        h1 {
            margin: 30px;
            text-align: center;
            width: 100%;
            color: black;
        }

        #training-table {
            border-collapse: collapse;
            width: 98%;
            margin-right: 1%;
            margin-left: 1%;
            font-family: Arial;
            font-size: 10pt;
        }

        #training-table input, #training-table select {
            width: 90%;
            border: none;
        }

        #training-table th {
            border: 2px solid black;
            white-space: nowrap;
        }

        #training-table td {
            border: 1px solid black;
            text-align: center;
            color: black;
        }

        .no-borders {
            border: none !important;
        }

        .no-border-bottom {
            border-bottom: none !important;
        }

        .small-borders-top-right {
            border-top: 1px solid black !important;
            border-right: 1px solid black !important;
        }

        .small-borders-top-left {
            border-top: 1px solid black !important;
            border-left: 1px solid black !important;
        }

        .small-border-right {
            border-right: 1px solid black !important;
        }

        .small-border-left {
            border-left: 1px solid black !important;
        }

        .double-border-right {
            border-right: 2px solid black !important;
        }

        .double-border-left {
            border-left: 2px solid black !important;
        }

        .edit {
            width: 70px !important;
        }

        .actions {
            background-color: yellow;
        }

        .actions button, .actions input {
            background-color: yellow;
            border: none;
            width: 70px !important;
        }

        td.month.value {
            text-align: right !important;
            white-space: nowrap;
            background-color: silver;
            color: black !important;
        }

        td.month.value option, #month-new option {
            text-align: right !important;
        }

        td input {
            text-align: center !important;
        }

        .current-level {
            text-align: left !important;
            color: black;
            font-size: 12px;
            padding-bottom: 10px;
        }

        div#current-level-title {
            font-family: arial;
            float: left;
            font-size: 16px;
            color: black;
        }

        #current-level {
            font-family: arial;
            display: block;
            width: 120px !important;
            margin-left: 20px;
            border: 1px solid black !important;
            float: left;
            height: 20px;
            font-size: 16px;
            text-align: center;
        }

        select#current-level {
            font-size: 14px !important;
        }

        #update-level {
            font-family: arial;
            height: 20px;
            width: 50px;
            float: left;
            background-color: yellow;
            color: black;
            border: 1px solid black;
            margin-left: 10px;
        }

        .header-title {
            background-color: red;
            text-align: left;
            font-family: arial;
            font-size: 20px;
            color: black;
            vertical-align: middle;
        }

        .header-title select {
            width: 150px !important;
            border: 1px solid black !important;
            font-size: 22px;
            font-family: arial;
            margin-left: 20px;
        }

        .sub-header th {
            font-family: arial;
            font-size: 9px;
            font-weight: normal;
        }

        .total-header {
            background-color: silver !important;
        }

        .green {
            background-color: lightgreen;
        }

        .red {
            background-color: red;
        }

        .totals-row th {
        <?php if (!$can_edit) { ?> color: red !important;
        <?php }; ?>
        }
    </style>
</head>
<body>
<?php include 'includes/main_menu.php'; ?>
<script>
    <?php if ($can_edit) { ?>
    <?php
        for ($i = 1; $i < 13; $i++) {
        };
    ?>
    var training_settings = <?php echo json_encode($settings); ?>;
    var month_names = <?php echo json_encode($month_names); ?>;
    function row_update() {
        var id = $(this).attr('rel');
        var post_data = {};
        post_data.id = id;
        $(this).parent().parent().parent().children("td.value").each(function () {
            var field = $(this).attr('rel');
            post_data[field] = $(this).children('[name=' + field + ']').prop('value');
            if (field == 'shop_name' && $(this).children('[name=' + field + '_new]').prop('value') != '') {
                post_data[field + '_new'] = $(this).children('[name=' + field + '_new]').prop('value');
            }
            ;
            if (field == 'company' && $(this).children('[name=' + field + '_new]').prop('value') != '') {
                post_data[field + '_new'] = $(this).children('[name=' + field + '_new]').prop('value');
            }
            ;
        });
        $.ajax({
            url: 'ajax_handler.php?action=update_row&type=<?php echo $record_type; ?>',
            method: 'POST',
            data: post_data,
            context: $(this)
        }).done(function () {
            $(this).parent().parent().parent().children("td.value").each(function () {
                var field = $(this).attr('rel');
                if ((field == 'shop_name' || field == 'company') && $(this).children('[name=' + field + '_new]').prop('value') != '') {
                    $(this).html($(this).children('[name=' + field + '_new]').prop('value'));
                    fvalues[field].push({
                        'id': $(this).html(),
                        'text': $(this).html()
                    });
                    fvalues[field] = fvalues[field].sort(function (item1, item2) {
                        return (item1.text > item2.text);
                    });
                } else {
                    $(this).html($(this).children('[name=' + field + ']').prop('value'));
                    if (field == 'month') {
                        var val = $(this).html();
                        for (var i in fvalues[field]) {
                            if (!fvalues[field].hasOwnProperty(i)) continue;
                            if (fvalues[field][i].id == val) {
                                $(this).html(fvalues[field][i].text);
                            }
                            ;
                        }
                        ;
                    }
                    ;
                }
                ;
                var fval = $(this).html();
                var re = /(_theory)$/;
                if (re.test(field)) {
                    fval = fval.replace('%', '');
                }
                ;
//alert(field + ' ' + training_settings.hasOwnProperty(field));
                try {
                    if (training_settings.hasOwnProperty(field) && parseFloat(training_settings[field]) <= parseFloat(fval)) {
                        $(this).addClass('green');
                    } else {
                        $(this).removeClass('green');
                    }
                    ;
                } catch (e) {
                    $(this).removeClass('green');
                }
                ;
            });
            $(this).prop('value', 'Edit').removeClass('update').addClass('edit').unbind('click').click(row_edit);
            $(this).parent().children(".cancel").prop('value', 'Delete').removeClass('cancel').addClass('delete').unbind('click').click(row_delete);
            update_balance();
        });
    }
    function row_delete() {
        var id = $(this).attr('rel');
        if (window.confirm("Are you sure to delete this record?")) {
            //document.location = "?action=Delete&id=" + id;
            var post_data = {"id": id};
            $.ajax({
                url: 'ajax_handler.php?action=delete_row&type=<?php echo $record_type; ?>',
                method: 'POST',
                data: post_data,
                context: $(this)
            }).done(function () {
                $(this).parent().parent().parent().remove();
                update_balance();
            }).fail(function () {
                alert('Record delete failed.');
            });
        }
        ;
    }
    function row_cancel() {
        var id = $(this).attr('rel');
        $(this).parent().parent().parent().children("td.value").each(function () {
            var field = $(this).attr('rel');
            $(this).html($(this).children('input[type=hidden]').prop('value'));
        });
        $(this).parent().children(".update").prop('value', 'Edit').removeClass('update').addClass('edit').unbind('click').click(row_edit);
        $(this).prop('value', 'Delete').removeClass('camcel').addClass('delete').unbind('click').click(row_delete);
    }
    function row_edit() {
        var id = $(this).attr('rel');
        $(this).parent().parent().parent().children("td.value").each(function () {
            var input_field = '';
            var field = $(this).attr('rel');
            var value = $(this).html();
            if (typeof field != 'undefined' && fvalues[field].length > 0) {
                input_field = $('<select>').prop('name', field);
                var text = value;
                for (fv in fvalues[field]) {
                    if (fvalues[field].hasOwnProperty(fv)) {
                        if (fvalues[field][fv].text == text) {
                            value = fvalues[field][fv].id;
                        }
                        ;
                        input_field.append(
                            $('<option>').prop('value', fvalues[field][fv].id).text(fvalues[field][fv].text)
                        );
                    }
                    ;
                }
                ;
                input_field.val(value);
            } else {
                input_field = $('<input>').prop('name', field).prop('value', $(this).html());
            }
            ;
            $(this).html(input_field);
            $(this).append(
                $('<input>').prop('type', 'hidden').prop('name', field + '_old').prop('value', value)
            );
            if (field == 'shop_name' || field == 'company') {
                $(this).append($('<br />')).append(
                    $('<input>').prop('name', field + '_new')
                );
            }
            ;
        });
        $(this).prop('value', 'Update').removeClass('edit').addClass('update').unbind('click').click(row_update);
        $(this).parent().children(".delete").prop('value', 'Cancel').removeClass('delete').addClass('cancel').unbind('click').click(row_cancel);
        $('.date, .date input, input[name=date]').datepicker({
            showAnim: 'fade',
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
            defaultDate: '<?php echo date("Y-m-01", $current_time); ?>',
            currentText: 'Today',
            maxDate: new Date(<?php echo $maxDate?>)
        });

    }
    ;
    <?php
        $total_fields = $form_fields;
        array_pop($total_fields);
        array_shift($total_fields);
        $total_fields = array_filter($total_fields, function ($item) {
            return !preg_match("/(theory)$/", $item);
        });
        $total_fields = array_map(function ($item) {return '.totals-row .' . $item;}, $total_fields);
    ?>
    function update_balance() {
        $("<?php echo implode(', ', $total_fields); ?>").each(function () {
            var balance = 0;
            var rel = $(this).attr('rel');
            $("tr.records td." + rel).each(function () {
                var value = 0;
                value = $(this).html().replace(new RegExp("/([^\d^\.]+)/"), '');
                if (value == '') value = 0;
                try {
                    value = parseFloat(value);
                } catch (e) {
                    value = 0;
                } finally {
                    balance += value;
                }
                ;
            });
            $(this).html(balance);
        });
    }
    ;
    $(document).ready(function () {
        $('.date, .date input').datepicker({
            showAnim: 'fade',
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
            defaultDate: '<?php echo date("Y-m-01", $current_time); ?>',
            currentText: 'Today',
            maxDate: new Date(<?php echo $maxDate?>)
        });
        $(".delete").click(row_delete);
        $('.edit').unbind('click').click(row_edit);
        update_balance();

    });
    var fvalues = {};
    <?php
        foreach ($form_fields as $field) {
            echo "\tfvalues['$field'] = " . json_encode(array_key_exists($field, $values) ? $values[$field] : array()) . ";\n";
        };
    ?>
    function show_sales_calendar() {
        var d = $('#notes').datepicker({
            showAnim: 'fade',
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
            defaultDate: '<?php echo date("Y-m-01", $current_time); ?>',
            currentText: 'Today',
            maxDate: new Date(<?php echo $maxDate?>),
            onSelect: function (dstring) {
                $("#company-new select").val("BUNGY JAPAN");
                $("#who-new select").val("Dave");
                $("#description-new select").val("SALES DEPOSIT");
                $("#analysis-new select").val("Sales Deposit - Daily");
                $(this).val('Sales ' + /-([\d]{2})-/.exec(dstring)[1] + '/' + /-([\d]{2})$/.exec(dstring)[1]);
                $(this).datepicker('destroy');
                $(this).focus();
            }
        });
        d.datepicker('show');
    }
    function change_uid() {
        document.location = '?tid=' + $("#tid").val();
    }
    <?php }; ?>
</script>
<table id="training-table">
    <tr>
        <td colspan="<?php echo sizeof($form_fields) + 1; ?>" class="no-borders" style="text-align: left;">
            <button type="button" onClick="javascript:document.location='manuals.php';">Manuals</button>
            <button type="button" onClick="javascript:document.location='tests.php';">Theory Tests</button>
            <?php if ($can_edit) { ?>
                <button type="button" onClick="javascript:document.location='settings.php';">Settings</button>
            <?php }; ?>
            <br/>
            <br/>
        </td>
    <tr>
    <tr>
        <th colspan="<?php echo sizeof($form_fields) + 1; ?>" class="header-title">Training Summary
            <?php
            if ($can_edit) {
                echo draw_pull_down_menu('tid', $staff_list, $tid, "id='tid' onChange=\"change_uid();\"");
            }
            ?>
        </th>
    <tr>
    <tr>
        <td colspan="<?php echo sizeof($form_fields) + 1; ?>" class="no-borders">&nbsp;</td>
    <tr>
    <tr>
        <td colspan="<?php echo sizeof($form_fields) + 1; ?>" class="no-borders current-level">
            <form method="post">
                <div id="current-level-title">Current Level:</div>
                <?php if ($can_edit) {
                    $levels = array(
                        "REC 2",
                        "REC 1",
                        "RAFT 3",
                        "RAFT 2",
                        "JO 2/RAFT 1",
                        "JO 1",
                        "JM 2",
                        "JM 1"
                    );
                    $levels_options = array();
                    foreach ($levels as $l) {
                        $levels_options[] = array(
                            'id'   => $l,
                            'text' => $l
                        );
                    };
                    echo draw_pull_down_menu('current_level', $levels_options, $current_level, 'id="current-level"');
                    echo '<button type="submit" name="button" value="update_level" id="update-level">Update</button>';
                    echo '<input type="hidden" name="tid" value="' . $tid . '">';
                    echo '<input type="hidden" name="action" value="update_level">';
                } else {
                    echo "<div id='current-level'>$current_level</div>";
                }; ?>
            </form>
        </td>
    </tr>
    <?php
    foreach ($headers as $header => $fields) {
        $span = '';
        if (in_array($header, array('Month', 'Comments'))) {
            $span = ' rowspan="2"';
            $class = "";
        } else {
            $span = ' colspan="' . sizeof($fields) . '"';
            $class = " class=\"no-border-bottom\"";
        };
        $field = $to_field($header);
        echo "\t<th id=\"$field-header\"$span$class>$header</th>\n";
    };
    if ($can_edit) {
        echo "\t<th id=\"actions-header\" rowspan=\"2\" class=\"no-borders\"></th>\n";
    };
    ?>
    </tr>
    <tr class='sub-header'>
        <?php
        foreach ($headers as $header => $fields) {
            if (in_array($header, array('Month', 'Comments'))) {
                continue;
            };
            $field = $to_field($header);
            foreach ($fields as $sub_field) {
                echo "\t<th id=\"$field-" . strtolower($sub_field) . "-header\" class=\"small-borders-top-right\">$sub_field</th>\n";
            };
        };
        ?>
    </tr>
    <?php
    $d = date('Y-', $current_time);
    $d = '';
    //The months in the table are generated from the entries in the training column
    //Quite oddly at the top of this file, data is inserted in to this table before here. If you delete a from
    //training and reload the page, it is reinserted
    $sql = "select * from training WHERE site_id = '" . CURRENT_SITE_ID . "' and uid = $tid AND `month` like '$d%' order by `date` ASC;";
    if ($debug) echo "$sql<br><br>";
    if ($res = mysql_query($sql)) {
        $totals = array();
        while ($row = mysql_fetch_assoc($res)) {
            if ($total_hours = getTotalTrHours($tid, $row['month'])) {
//print_r($total_hours);
                foreach ($total_hours as $fname => $total) {
                    $row[$to_field($fname . ' hours')] = $total['hours'];
                    $row[$to_field($fname . ' jumps')] = $total['jumps'];
                };
            };
            ?>
            <tr class='records'>
                <?php
                foreach ($form_fields as $key => $field) {
                    $class = '';
                    if (preg_match("/(theory)/", $field)) {
                        $class .= ' double-border-left';
// select test result
                        //repeat the query for each possible test ($manual_title)
                        $manual_title = ucfirst(str_replace("_", " ", str_replace('_theory', '', $field)));
                        //the problem is here... The manual titles are wrong
                        //NOTE that the manual title must be the same as the title of the column in this table for the test results to appear
                        $sql = "SELECT ttr.test_score FROM training_manuals tm LEFT JOIN training_tests tt on (tm.id = tt.manual_id) LEFT JOIN training_tests_results ttr on (tt.id = ttr.test_id and user_id = '$tid' and take_date like '{$row['month']}-%') WHERE tm.site_id = '" . CURRENT_SITE_ID . "' and tm.title_en = '$manual_title' and ttr.test_score IS NOT NULL ORDER BY ttr.id DESC LIMIT 1;";
                        if ($debug) echo "$sql<br><br>";
                        //echo "$sql<br><br>";
                        $tres = mysql_query($sql) or die(mysql_error());
                        if ($trow = mysql_fetch_assoc($tres)) {
                            $row[$field] = $trow['test_score'] . '%';
                            if ($settings[$field] <= $trow['test_score']) {
                                $class .= ' green';
                            } else {
                                $class .= ' red';
                            };
                        } else {
                            $row[$field] = 'N/A';
                        };
// eof test result
                    };
                    if (preg_match("/(jumps|practical)/", $field)) {
                        $class .= ' double-border-right';
                    };
                    if (preg_match("/(month|comments)/", $field)) {
                        $class .= ' double-border-right double-border-left';
                    } else {
                        if (!array_key_exists($field, $totals)) {
                            $totals[$field] = 0;
                        };
                        $totals[$field] += $row[$field];
                    };
                    $text = $row[$field];
                    if ("month" == $field) {
                        list($year, $month) = explode('-', $text);
                        $text = date("M Y", mktime(0, 0, 0, $month, 15, $year));
                    };
                    if (is_numeric($row[$field]) && $settings[$field] <= $row[$field] && !in_array($field, array('date', 'comments'))) {
                        $class .= ' green';
                    };

                    if (!preg_match("/(_theory)$/", $field)) {
                        $class .= ' value';
                    };
                    echo "\t<td id=\"$field-{$row['id']}\" class=\"$field$class\" rel=\"$field\">{$text}</td>\n";
                };
                if ($can_edit) {
                    ?>
                    <th class="actions">
                        <nobr><input type="button" value="Edit" rel="<?php echo $row['id']; ?>" class="edit"></nobr>
                    </th>
                <?php }; ?>
            </tr>
        <?php
        };
    };
    ?>
    <?php if ($can_edit) { ?>
        <form method="POST">
            <tr id="add-new-record">
                <?php
                foreach ($form_fields as $key => $field) {
                    $record = array();
                    $input_field = draw_input_field($field, $field, $record, '', false);
                    if (array_key_exists($field, $values)) {
                        $input_field = draw_pull_down_menu('i[' . $field . ']', $values[$field]);
                    };
                    if ($field == 'month') {
                        $input_field = draw_pull_down_menu('i[' . $field . ']', $month_values);
                    };
                    $class = '';
                    if (preg_match("/(theory)/", $field)) {
                        $class .= ' double-border-left';
                    };
                    if (preg_match("/(jumps|practical)/", $field)) {
                        $class .= ' double-border-right';
                    };
                    if (preg_match("/(month|comments)/", $field)) {
                        $class .= ' double-border-right double-border-left';
                    };
                    if (preg_match("/(theory)$/", $field)) {
                        $input_field = '';
                    };
                    echo "\t<td id=\"$field-new\" class=\"$field$class\">" . $input_field . "</td>\n";
                };
                ?>
                <th class="actions" id="action-add"><input type="submit" name="action" value="Add"></th>
            </tr>
        </form>
    <?php }; ?>
    <tr class="totals-row">
        <?php
        foreach ($form_fields as $key => $field) {
            $text = $totals[$field];
            $class = '';
            if (in_array($field, array('month', 'comments'))) {
                $text = 'TOTAL';
                $class = ' total-header';
            };
            if (preg_match("/(theory|hours)/", $field)) {
                $class .= ' small-border-right';
            };
            if (preg_match("/(jumps|practical|hours)/", $field)) {
                $class .= ' small-border-left';
            };
            if (preg_match("/(theory)/", $field)) {
                $text = '';
            };
            echo "\t<th id=\"$field-total\" class=\"value $field$class\" rel=\"$field\">{$text}</th>\n";
        };
        ?>
        <th class='no-borders'>&nbsp;</th>
    </tr>
    <tr>
        <th class='no-borders'><br/>
            <button id="_back" onClick="javascript: document.location='/'">Back</button>
            <?php echo helpButton(); ?>
        </th>
    </tr>
</table>
</form>
<?php include("ticker.php"); ?>
</body>
</html>
