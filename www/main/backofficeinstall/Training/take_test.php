<?php
include '../includes/application_top.php';
// be sure we works with main DB
mysql_query("SET NAMES 'utf8';");
$lang = $_GET['lang'];

$can_edit = false;
if ($user->hasRole(array('General Manager', 'SysAdmin'))) {
    $can_edit = true;
};
if ($_GET['preview'] == 1) {
    $can_edit = false;
};
if ($can_edit) {
    $action = $_POST['action'];
    $db_action = false;
    $where = '';
    $lang = $_GET['lang'];
    switch (TRUE) {
        case ($action == 'update'):
        case ($action == 'Update'):
            $where = 'id=' . $_POST['i']['id'];
            $db_action = 'update';
        case ($action == 'add'):
        case ($action == 'Add'):
            $db_action = $db_action ? $db_action : 'insert';
            $data = $_POST['i'];
            $test_id = $data['test_id'];
            unset($data['id']);
            if ($data['type'] == 3) {
                $data['a1'] = '';
                $data['a2'] = '';
                $data['a3'] = '';
                $data['a4'] = '';
            };
            db_perform('training_questions', $data, $db_action, $where);
            Header("Location: /Training/test.php?id=" . $test_id . '&lang=' . $lang);
            die();
            break;
        case ($action == 'Delete'):
        case ($action == 'delete'):
            $sql = "delete from training_questions where id = '{$_POST['i']['id']}'";
            mysql_query($sql) or die(mysql_error());
            Header("Location: /Training/test.php?id=" . $_POST['i']['test_id'] . '&lang=' . $lang);
            die();
            break;
    };
};
$sql = "SELECT * FROM training_tests WHERE id = '{$_GET['id']}'";
$res = mysql_query($sql) or die(mysql_error());
if ($row = mysql_fetch_assoc($res)) {
    $test = $row;
} else {
    die('No test id provided');
};
$sql = "SELECT * FROM training_manuals WHERE id = '{$test['manual_id']}'";
$res = mysql_query($sql) or die(mysql_error());
if ($row = mysql_fetch_assoc($res)) {
    $manual = $row;
} else {
    die('No test id provided');
};
$questions = array();
$sql = "SELECT * FROM training_questions WHERE test_id = '{$test['id']}' AND lang = '$lang' ORDER BY question ASC";
$res = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_assoc($res)) {
    $questions[] = $row;
};
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $manual['title_' . $lang] ?></title>
    <?php include "../includes/head_scripts.php"; ?>
    <style>
        h1 {
            margin: 30px;
            text-align: center;
            width: 100%;
            color: black;
        }

        #manuals-table th.header-title {
            text-align: center;
        }

        #manuals-table {
            margin-left: auto;
            margin-right: auto;
            width: 1000px;
            border-collapse: collapse;
        }

        #manuals-table th {
            font-size: 30px;
            padding: 10px;
            text-align: left;
            text-decoration: underline;
        }

        #manuals-table td {
            vertical-align: top;
            line-height: 25px;
            padding-top: 5px;
        }

        #manuals-table input {
            width: 90%;
        }

        #manuals-table input[type='radio'], #manuals-table input[type='checkbox'] {
            width: 20px !important;
        }

        #manuals-table textarea {
            width: 98%;
        }

        textarea {
            height: 50px;
            width: 99%;
        }

        #manuals-table td.actions input, #manuals-table td.actions button {
            width: 98% !important;
        }

        .actions {
            width: 70px;
        }

        .new-title {
            font-weight: bold;
            text-align: center;
        }

        .title {
            height: 18px;
        }

        #manuals-table tr.border td {
            /*border: 2px solid black;*/
        }

        .question {
            margin-top: 20px;
            background-color: silver;
        }

        .question td {
            padding: 5px;
            font-weight: bold;
        }

        .submit input {
            float: right;
            width: 100px !important;
        }

        .question p {
            margin: 0px;
            margin-left: 40px;
            text-indent: -40px;
            text-align: justify;
        }
    </style>
</head>
<body>
<?php include 'includes/main_menu.php'; ?>
<script>
    $(document).ready(function () {
        $("form").submit(function() {
            $('input[name=action]').val('Submitting...').attr('disabled', 'disabled');
        });

        /*
         * Prevent multiple radio buttons being highlighted, this would be as simple as setting the same name for
         * each radio button of the same questions. However that will screw up the radio code.
         */
        $('input[type="radio"]').click(function () {
            var rel = $(this).attr('rel');
            var eventResponder = $(this);

            //get all of the questions without the same questions number
            $(this).parent().parent().parent().find('input[rel=' + rel + ']').each(function (index, element) {
                var currentElement = $(element);

                //only turn off the inputs that are not the clicked one
                if (!eventResponder.is(currentElement)) {
                    $(element).attr('checked', false);
                    console.log(this);
                }
            });

        });
    });
</script>
<form action="save_test.php" method="get" name="form">
<?php
$form_fields = array('a1', 'a3');
//echo "<form method='get' name='form' action='save_test.php'>";
echo "<input type='hidden' name='test_id' value='{$test['id']}'>";
echo "<input type='hidden' name='lang' value='{$lang}'>";
?>
<table id="manuals-table">
    <tr>
        <td colspan="<?php echo sizeof($form_fields) + $can_edit; ?>" class="no-borders">&nbsp;</td>
    </tr>
    <tr>
        <th colspan="<?php echo sizeof($form_fields) + $can_edit; ?>" class="header-title"><?php echo $manual['title_' . $lang]; // . ' - ' . $test['title_' . $lang]; ?></th>
    </tr>
    <td colspan="<?php echo sizeof($form_fields) + $can_edit; ?>" class="no-borders">&nbsp;
        <?php
        if (array_key_exists('preview', $_GET)) {
            echo "<a href='test.php?id={$manual['id']}&lang=$lang'>Edit</a>";
        };
        ?>
    </td>
    </tr>
    <?php
    $tabstop = 1;
    foreach ($questions as $question) {
        echo "<tr class='separator'><td colspan='2'>&nbsp;</td></tr>";
        echo "<tr class='question'>";
        echo "<td colspan='2'>";
        echo "<p>{$question['question']}</p>";
        echo "</td>";
        echo "</tr>";
        $filename = 'img/tests/' . $question['test_id'] . '/' . $question['id'] . '.jpg';
        if (is_file($filename)) {
            echo "<tr>";
            echo "<td colspan='2' align='center'>";
            echo "<img src='$filename'>";
            echo "</td>";
            echo "</tr>";
        };
        $answers = array();
        for ($i = 1; $i <= 4; $i++) {
            $answers['a' . $i] = $question['a' . $i];
            switch ($question['type']) {
                case 1:
                    $answers['a' . $i] = "<input type='radio' rel='{$question['id']}' name='question[{$question['id']}][a{$i}]' tabindex='$tabstop'> " . $answers['a' . $i];
                    break;
                case 2:
                    $answers['a' . $i] = "<input type='checkbox' name='question[{$question['id']}][a{$i}]' tabindex='$tabstop'> " . $answers['a' . $i];
                    break;
                case 3:
                    $answers['a' . $i] = "$i) <input type=text name='question[{$question['id']}][a{$i}]' tabindex='$tabstop'>";
                    break;
                default:
                    $answers['a' . $i] = 'Wrong question type';
            };
            $tabstop++;
        };
        echo "\n<tr class='border' rel='q{$question['id']}'>\n";
        echo "<td class='answer'>\n";
        echo $answers['a1'] . "\n";
        echo "</td>\n";
        echo "<td class='answer'>\n";
        echo $answers['a3'] . "\n";
        echo "</td>\n";
        echo "</tr>\n";
        echo "<tr class='border' rel='q{$question['id']}'>\n";
        echo "<td class='answer'>\n";
        echo $answers['a2'] . "\n";
        echo "</td>";
        echo "<td class='answer'>\n";
        echo $answers['a4'] . "\n";
        echo "</td>\n";
        echo "</tr>\n";
        //echo "</form>\n";
    };
    ?>
    <tr>
        <td colspan='2' class='submit'><input type='submit' value='Submit' name='action'></td>
    </tr>
</table>
</form>
<br/>
<br/>

<?php include("ticker.php"); ?>
</body>
</html>
