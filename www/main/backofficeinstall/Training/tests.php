<?php
	include '../includes/application_top.php';
// be sure we works with main DB
	mysql_query("SET NAMES 'utf8';");
	$can_edit = false;
	if ($user->hasRole(['General Manager', 'SysAdmin'])) {
		$can_edit = true;
	};

    if($user->getUserName() == "Hiroshi") $can_edit = true;

	$langs = ['en', 'ja'];

    //why???
	$admin = $can_edit;

	//why???
	//$can_edit = false;

	if ($can_edit) {
	$action = $_POST['action'];
	switch (TRUE) {
		case ($action == 'Add Test'):
			foreach ($langs as $lang) {
				$data = [
					'manual_id'	=> $_POST['manual_id'],
					'title'		=> $_POST['i']['t' . $lang],
					'lang'		=> $lang
				];
				print_r($data);
				db_perform('training_tests', $data);
			};
			header("Location: /Training/tests.php");
			exit();
			break;
		case ($action == 'add'):
		case ($action == 'Add'):
			$data = $_POST['i'];
			unset($data['action']);
			db_perform('training_manuals', $data);
			header("Location: /Training/manuals.php");
			exit();
			break;
		case ($action == 'Update'):
			header("Location: /Training/?tid=" . $_SESSION['tid']);
			exit();
			break;
		case ($action == 'Delete'):
			$sql = "delete from training where id = '{$_GET['id']}'";
			mysql_query($sql) or die(mysql_error());
			header("Location: /Training/?tid=" . $_SESSION['tid']);
			exit();
			break;
		case ($action == 'CSV'):
			$current_month = date("Y-m", $current_time);
			header('Content-Type: text/csv; name="training'.$current_month.'.csv"');
			header('Content-Disposition: attachment; filename="training'.$current_month.'.csv"');
			echo '"' . implode('","', $headers) . '"' . "\r\n";
			//for ($i = 1; $i <= 12; $i++) {
    			//$d = $current_year . '-' . sprintf("%02d-", $i);
    			$sql = "select * from training WHERE `date` like '$current_month%' order by `date` ASC;";
    			$res = mysql_query($sql);
				$balance = 0;
    			while ($row = mysql_fetch_assoc($res)) {
					foreach ($fields as $field) {
						echo '"' . $row[$field] . '"' . ($field == $fields[sizeof($fields)-1] ? '' : ',');
					};
					echo "\r\n";
				};
			//};
			exit();
			break;
	};
	};
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Theory Tests</title>
<?php include "../includes/head_scripts.php"; ?>
	<script src="tests_titles.js"></script>
<style>
h1 {
	margin: 30px;
	text-align: center;
	width: 100%;
	color: black;
}
#manuals-table {
	margin-left: auto;
	margin-right: auto;
	width: 800px;
}
#manuals-table th {
	background-color: silver;
	font-size: 30px;
	padding: 10px;
}
#manuals-table input {
	width: 98%;
}
#manuals-table td.actions input {
    width: 49%;
}
.actions {
	width: 150px;
}
.new-title {
	font-weight: bold;
	text-align: center;
}
.cten, .ctja {
	width: 80% !important;
	float: right;
}
.new-test .actions {
	text-align: center;
}
.new-test .actions input {
	width: 100px !important;
}
.value.test a, .value.test input {
	margin-left: 20%;
	width: 80% !important;
}
</style>
</head>
<body>
<?php include 'includes/main_menu.php'; ?>
<script>
$(document).ready(function () {
	$(".delete").click(row_delete);
	$('.change').unbind('click').click(row_change);
});
</script>
<?php
	$form_fields = array('title_en', 'title_ja'); 
?>
<table id="manuals-table">
<tr>
    <td colspan="<?php echo sizeof($form_fields) + $can_edit+$admin; ?>" class="no-borders">&nbsp;</td>
<tr>
<tr>
    <th colspan="<?php echo sizeof($form_fields) + $can_edit + $admin; ?>" class="header-title">Theory Tests</th>
<tr>
<tr>
    <td colspan="<?php echo sizeof($form_fields) + $can_edit + $admin; ?>" class="no-borders">&nbsp;</td>
<tr>
<?php
	// check available tests
	$sql = "SELECT tt.manual_id, count(tt.id) as total FROM training_tests tt GROUP BY tt.manual_id";
	$res = mysql_query($sql) or die(mysql_error());
	$tests = array();
	while ($row = mysql_fetch_assoc($res)) {
		$tests[$row['manual_id']] = $row['total'];
	};
	$sql = "SELECT * FROM training_manuals WHERE site_id = '".CURRENT_SITE_ID."';";
	$res = mysql_query($sql) or die(mysql_error());
	while ($row = mysql_fetch_assoc($res)) {
		echo "<tr rel='{$row['id']}'>";
		$text = $row['title_en'];
		echo "<td class='value' rel='title_en'><a href='test.php?manual_id={$row['id']}&lang=en'>" . $text . '</a></td>';
		$text = $row['title_ja'];
		echo "<td class='value' rel='title_ja'><a href='test.php?manual_id={$row['id']}&lang=ja'>" . $text . '</a></td>';
		if ($can_edit) {
			echo "<td class='actions'>";
			echo "<input class='change' type='button' value='Change'>";
			echo "<input class='delete' type='button' value='Delete'>";
			echo "</td>";
		};
		echo "</tr>";
		// if there is any tests created
		if (array_key_exists($row['id'], $tests) && $tests[$row['id']] > 0) {
/*
			$sql = "SELECT tt.*, count(ttr.id) as taken FROM training_tests tt LEFT JOIN training_tests_results ttr on (tt.id = ttr.test_id and ttr.user_id = {$_SESSION['uid']} and ttr.take_date like '".date("Y-m")."-%') WHERE tt.manual_id = '{$row['id']}' GROUP BY tt.id;";
			$tres = mysql_query($sql) or die(mysql_error());
			while ($trow = mysql_fetch_assoc($tres)) {
				if ($trow['taken'] > 0 && (!$admin)) continue;
				echo "<tr rel='{$trow['id']}'>";
				echo "<td class='value test' rel='title_en'><a href='test.php?id={$trow['id']}&lang=en'>" . $trow['title_en'] . '</a></td>';
				echo "<td class='value test' rel='title_ja'><a href='test.php?id={$trow['id']}&lang=ja'>" . $trow['title_ja'] . '</a></td>';
				if ($admin) {
					echo "<td class='actions'>";
					echo "<input class='change' type='button' value='Change'>";
					echo "<input class='delete' type='button' value='Delete'>";
					echo "</td>";
				};
				echo "</tr>";
			};
*/
		} else {
			$data = array(
				'title_en'	=> 'Level 1',
				'title_ja'	=> 'Level 1',
				'manual_id'	=> $row['id']
			);
			db_perform('training_tests', $data);
/*
			$data['id']	= mysql_insert_id();
			$trow = $data;
			echo "<tr rel='{$trow['id']}'>";
			echo "<td class='value test' rel='title_en'><a href='test.php?id={$trow['id']}&lang=en'>" . $trow['title_en'] . '</a></td>';
			echo "<td class='value test' rel='title_ja'><a href='test.php?id={$trow['id']}&lang=ja'>" . $trow['title_ja'] . '</a></td>';
			echo "<td class='actions'>";
			echo "<input class='change' type='button' value='Change'>";
			echo "<input class='delete' type='button' value='Delete'>";
			echo "</td>";
			echo "</tr>";
*/
		};
/*
		if ($admin) {
			echo '<form method="post">';
			echo "<tr class='new-test'>";
			echo "<td class='value'>" . draw_input_field('ten', 'iten', array(), 'class="cten"', false) . '</td>';
			echo "<td class='value'>" . draw_input_field('tja', 'itja', array(), 'class="ctja"', false) . '</td>';
			echo "<td class='actions'><input name='manual_id' value='{$row['id']}' type='hidden'><input type='submit' name='action' value='Add Test'></td>";
			echo "</tr>";
			echo '</form>';
		};
*/
	};
?>
</tr>
<?php if ($can_edit) { ?>
<form method="post">
<tr>
	<td class="new-title">Title English</td>
	<td class="new-title">Title Japanese</td>
	<td class="new-title">&nbsp;</td>
</tr>
<tr>
<?php
			echo "<td>";
			echo draw_input_field('title_en', 'title_en', array('title_en'=>''));
			echo "</td>";
			echo "<td>";
			echo draw_input_field('title_ja', 'title_ja', array('title_ja'=>''));
			echo "</td>";
			echo "<td>";
			echo "<button name='action' class='add' type='submit' value='add'>Add</button>";
			echo "</td>";
?>
</tr>
</form>
<?php }; ?>
<tr>
	<td colspan=3><button id='back' onClick="document.location = '/Training/';">Back</button></td>
</tr>
</table>
<br />
<br />

</form>
<?php include ("ticker.php"); ?>
</body>
</html>
