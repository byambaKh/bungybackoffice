<?php
	include '../includes/application_top.php';
	if($_SERVER['REQUEST_METHOD'] == "GET")
	{
		if(empty($_GET['question']))
		{
			 echo '<script type="text/javascript">alert("You must answer all questions before proceeding."); window.history.back(); </script>';
			//echo "Nothing to save";		
		}else
		{
			function calculateTestMaxScore($testId, $lang){
		        $sql = "SELECT * FROM training_questions WHERE test_id = '$testId' AND lang = '$lang' ORDER BY question ASC";
		        $res = mysql_query($sql) or die(mysql_error());
		        while ($row = mysql_fetch_assoc($res)) {
		            $questions[$row['id']] = $row;
		        }

		        $total_points = 0;

		        foreach ($questions as $qid => $d) {
		            $total_points += $questions[$qid]['a1_points'] + $questions[$qid]['a2_points'] + $questions[$qid]['a3_points'] + $questions[$qid]['a4_points'];
		        }

		        return $total_points;
		    }
		    $testMaxScore = calculateTestMaxScore($_GET['test_id'], $_GET['lang']);
		    $questions = array();
		    $sql = "SELECT * FROM training_questions WHERE test_id = '{$_GET['test_id']}' AND lang = '{$_GET['lang']}' ORDER BY question ASC";
		    $res = mysql_query($sql) or die(mysql_error());
		    while ($row = mysql_fetch_assoc($res)) {
		        $questions[$row['id']] = $row;
		    };
			$data = array(
				'test_id'		=> $_GET['test_id'],
				'user_id'		=> $_SESSION['uid'],
				'take_date'		=> 'now()',
				'lang'			=> $_GET['lang']
			);
			db_perform('training_tests_results', $data);
			$result_id = mysql_insert_id();
			$total_points = 0;
			$points_received = 0;	

			foreach ($questions as $qid => $d) {
				if (!array_key_exists($qid, $_GET['question'])) 
				{
					$_GET['question'][$qid] = array();	
				}
				$data = $_GET['question'][$qid];
				for ($i = 1; $i <= 4; $i++) {
					$aname = 'a' . $i;
					if (!array_key_exists($aname, $data)) {
						$data[$aname] = '';
					};
					if ($data[$aname] != '') 
						$data[$aname] = 1;
					else 
						$data[$aname] = 0;
				};
				$total_points += $questions[$qid]['a1_points'] + $questions[$qid]['a2_points'] + $questions[$qid]['a3_points'] + $questions[$qid]['a4_points'];				
				$points_received += $questions[$qid]['a1_points'] * $data['a1'] 
								+ $questions[$qid]['a2_points'] * $data['a2']
								+ $questions[$qid]['a3_points'] * $data['a3']
								+ $questions[$qid]['a4_points'] * $data['a4'];
				//$points_received += (-1+$questions[$qid]['a1_points']) * $data['a1']
								//+ (-1+$questions[$qid]['a2_points']) * $data['a2']
								//+ (-1+$questions[$qid]['a3_points']) * $data['a3']
								//+ (-1+$questions[$qid]['a4_points']) * $data['a4'];
				$data['result_id'] = $result_id;
				db_perform('training_result_answers', $data);
			}; 
			$data = array(
				'test_score'	    => ($total_points > 0) ? sprintf("%.0d", $points_received / $total_points * 100) : 0
			);

			
			
			db_perform('training_tests_results', $data, 'update', 'id=' . $result_id);
			Header("Location: /Training/index.php");
			
		}	
	}
?>
