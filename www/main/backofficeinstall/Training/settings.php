<?php
	include '../includes/application_top.php';
	include '../Operations/reports_save.php';
	include 'training_fields.php';
// be sure we works with main DB
	$record_type = 'training_settings';
	mysql_set_charset('utf8');
	mysql_query("SET NAMES 'utf8'");
	$can_edit = false;
	if ($user->hasRole(array('Site Manager', 'General Manager', 'SysAdmin'))) {
		$can_edit = true;
	} else {
		Header("Location: index.php");
	};
	// get staff list
	// get current level for user
	// eof month
	$form_fields = array();
	foreach ($headers as $key => $sub_fields) {
		$field = $to_field($key);
		if (in_array($field, array('month', 'comments'))) {
			$form_fields[] = $field;
			continue;
		};
		foreach ($sub_fields as $sub_field) {
			$form_fields[] = $field . '_' . strtolower($sub_field);
		};
	};
	$values = array();
	// check if needed table exists
	
	$action = $_POST['action'];
	if (empty($action)) {
		$action = $_GET['action'];
	};
	if ($can_edit) {
	switch (TRUE) {
		case ($action == 'Update'):
			$record = $_POST['i'];
			print_r($_POST);
			file_put_contents('pass_settings.txt', serialize($record));
			Header("Location: /Training/settings.php");
			die();
			break;
	};
	};
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Training - Settings</title>
<?php include "../includes/head_scripts.php"; ?>
<style>
h1 {
	margin: 30px;
	text-align: center;
	width: 100%;
	color: black;
}
#training-table {
	border-collapse: collapse;
	width: 98%;
	margin-right: 1%;
	margin-left: 1%;
	font-family: Arial;
	font-size: 10pt;
}
#training-table input, #training-table select {
	width: 90%;
	border: none;
}
#training-table th {
	border: 2px solid black;
	white-space:nowrap;
}
#training-table td {
	border: 1px solid black;
	text-align: center;
	color: black;
}
.no-borders {
	border: none !important;
}
.no-border-bottom {
	border-bottom: none !important;
}
.small-borders-top-right {
	border-top: 1px solid black !important;
	border-right: 1px solid black !important;
}
.small-borders-top-left {
	border-top: 1px solid black !important;
	border-left: 1px solid black !important;
}
.small-border-right {
	border-right: 1px solid black !important;
}
.small-border-left {
	border-left: 1px solid black !important;
}
.double-border-right {
	border-right: 2px solid black !important;
}
.double-border-left {
	border-left: 2px solid black !important;
}
.edit {
	width: 70px !important;
}
.actions {
	background-color: yellow;
}
.actions button, .actions input {
	background-color: yellow;
	border: none;
	width: 70px !important;
}
td.month.value {
	text-align: right !important;
	white-space:nowrap;
	background-color: silver;
	color: black !important;
}
td.month.value option, #month-new option {
	text-align: right !important;
}
td input, th input  {
	text-align: center !important;
}
.current-level {
	text-align: left !important;
	color: black;
	font-size: 16px;
	font-weight: bold;
	padding-bottom: 10px;
}
div#current-level-title {
	font-family: arial;
	float: left;
	font-size: 16px;
	color: black;
}
#current-level {
	font-family: arial;
	display: block;
	width: 120px !important;
	margin-left: 20px;
	border: 1px solid black !important;
	float: left;
	height: 20px;
	font-size: 16px;
	text-align: center;
}
select#current-level {
	font-size: 14px !important;
}
#update-level {
	font-family: arial;
	height: 20px;
	width: 50px;
	float: left;
	background-color: yellow;
	color: black;
	border: 1px solid black;
	margin-left: 10px;
}
.header-title {
	background-color: red;
	text-align: left;
	font-family: arial;
	font-size: 20px;
	color: black;
	vertical-align: middle;
}
.header-title select {
	width: 150px !important;
	border: 1px solid black !important;
	font-size: 22px;
	font-family: arial;
	margin-left: 20px;
}
.sub-header th {
	font-family: arial;
	font-size: 9px;
	font-weight: normal;
}
.total-header {
	background-color: silver !important;
}
.totals-row th {
<?php if (!$can_edit) { ?>
	color: red !important;
<?php }; ?>
}
</style>
</head>
<body>
<?php include 'includes/main_menu.php'; ?>
<script>
<?php if ($can_edit) { ?>
	function row_update() {
		var id = $(this).attr('rel');
		var post_data = {};
		post_data.id = <?php echo CURRENT_SITE_ID; ?>;
		$(this).parent().parent().children("th.value").each(function () {
			var field = $(this).attr('rel');
			post_data[field] = $(this).children('[name='+field+']').prop('value').replace('%', '');
			if (field == 'shop_name' && $(this).children('[name='+field+'_new]').prop('value') != '') {
				post_data[field+'_new'] = $(this).children('[name='+field+'_new]').prop('value');
			};
			if (field == 'company' && $(this).children('[name='+field+'_new]').prop('value') != '') {
				post_data[field+'_new'] = $(this).children('[name='+field+'_new]').prop('value');
			};
		});
		$.ajax({
			url: 'ajax_handler.php?action=update_row&type=<?php echo $record_type; ?>',
			method: 'POST', 
			data: post_data,
			context: $(this)
		}).done(function () {
			$(this).parent().parent().children("th.value").each(function () {
				var field = $(this).attr('rel');
				if ((field == 'shop_name' || field == 'company') && $(this).children('[name='+field+'_new]').prop('value') != '') {
					$(this).html($(this).children('[name='+field+'_new]').prop('value'));
					fvalues[field].push({
						'id': $(this).html(), 
						'text' :$(this).html()
					});
					fvalues[field] = fvalues[field].sort(function (item1, item2) {
						return (item1.text > item2.text);
					});
				} else {
					$(this).html($(this).children('[name='+field+']').prop('value'));
					if (field == 'month') {
						var val = $(this).html();
						for (var i in fvalues[field]) {
							if (!fvalues[field].hasOwnProperty(i)) continue;
							if (fvalues[field][i].id == val) {
								$(this).html(fvalues[field][i].text);
							};
						};
					};
				};
			});
			$(this).prop('value', 'Edit').removeClass('update').addClass('edit').unbind('click').click(row_edit);
			$(this).parent().children(".cancel").prop('value', 'Delete').removeClass('cancel').addClass('delete').unbind('click').click(row_delete);
			//update_balance();
		});
	}
	function row_delete() {
		var id = $(this).attr('rel');
		if (window.confirm("Are you sure to delete this record?")) {
			//document.location = "?action=Delete&id=" + id;
			var post_data = {"id":id};
			$.ajax({
				url: 'ajax_handler.php?action=delete_row&type=<?php echo $record_type; ?>',
				method: 'POST', 
				data: post_data,
				context: $(this)
			}).done(function () {
				$(this).parent().parent().remove();
				//update_balance();
			}).fail(function () {
				alert('Record delete failed.');
			});
		};
	}
	function row_cancel() {
		var id = $(this).attr('rel');
		$(this).parent().parent().children("th.value").each(function () {
			var field = $(this).attr('rel');
			$(this).html($(this).children('input[type=hidden]').prop('value'));
		});
		$(this).parent().children(".update").prop('value', 'Edit').removeClass('update').addClass('edit').unbind('click').click(row_edit);
		$(this).prop('value', 'Delete').removeClass('camcel').addClass('delete').unbind('click').click(row_delete);
	}
	function row_edit() {
		var id = $(this).attr('rel');
		$(this).parent().parent().children("th.value").each(function () {
			var input_field = '';
			var field = $(this).attr('rel');
			var value = $(this).html();
			if (typeof field != 'undefined' && fvalues[field].length > 0) {
				input_field = $('<select>').prop('name', field);
				var text = value;
				for (fv in fvalues[field]) {
					if (fvalues[field].hasOwnProperty(fv)) {
						if (fvalues[field][fv].text == text) {
							value = fvalues[field][fv].id;
						};
						input_field.append(
							$('<option>').prop('value', fvalues[field][fv].id).text(fvalues[field][fv].text)
						);
					};	
				};
				input_field.val(value);
			} else {
				input_field = $('<input>').prop('name', field).prop('value', $(this).html());
			};
			$(this).html(input_field);
			$(this).append(
				$('<input>').prop('type', 'hidden').prop('name', field + '_old').prop('value', value)
			);
			if (field == 'shop_name' || field == 'company') {
				$(this).append($('<br />')).append(
					$('<input>').prop('name', field + '_new')
				);
			};
		});
		$(this).prop('value', 'Update').removeClass('edit').addClass('update').unbind('click').click(row_update);
		$(this).parent().children(".delete").prop('value', 'Cancel').removeClass('delete').addClass('cancel').unbind('click').click(row_cancel);
    	$('.date, .date input, input[name=date]').datepicker({
                showAnim: 'fade',
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd',
				defaultDate: '<?php echo date("Y-m-01", $current_time); ?>',
                currentText: 'Today',
                maxDate: new Date(<?php echo $maxDate?>)
    	});

	};
<?php
	$total_fields = $form_fields;
	array_pop($total_fields);
	array_shift($total_fields);
	$total_fields = array_filter($total_fields, function ($item) {
		return !preg_match("/(theory)$/", $item);
	});
	$total_fields = array_map(function ($item) {return '.totals-row .' . $item;}, $total_fields);
?>
	function update_balance() {
		$("<?php echo implode(', ', $total_fields); ?>").each(function () {
			var balance = 0;
			var rel = $(this).attr('rel');
			$("tr.records td."+rel).each(function () {
				var value = 0;
				value = $(this).html().replace(new RegExp("/([^\d]+)/"), '');
				if (value == '') value = 0;
				try {
					value = parseInt(value);
				} catch (e) {
					value = 0;
				} finally {
					balance += value;
				};
			});
			$(this).html(balance);
		});
	};
$(document).ready(function () {
	$('.date, .date input').datepicker({
                showAnim: 'fade',
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd',
				defaultDate: '<?php echo date("Y-m-01", $current_time); ?>',
                currentText: 'Today',
                maxDate: new Date(<?php echo $maxDate?>)
	});
	$(".delete").click(row_delete);
	$('.edit').unbind('click').click(row_edit);
	//update_balance();
});
	var fvalues = {};
<?php
	foreach ($form_fields as $field) {
		echo "\tfvalues['$field'] = " . json_encode(array_key_exists($field, $values) ? $values[$field] : array()) . ";\n";
	};
?>
function show_sales_calendar() {
	var d = $('#notes').datepicker({
                showAnim: 'fade',
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd',
                defaultDate: '<?php echo date("Y-m-01", $current_time); ?>',
                currentText: 'Today',
                maxDate: new Date(<?php echo $maxDate?>),
				onSelect: function (dstring) {
					$("#company-new select").val("BUNGY JAPAN");
					$("#who-new select").val("Dave");
					$("#description-new select").val("SALES DEPOSIT");
					$("#analysis-new select").val("Sales Deposit - Daily");
					$(this).val('Sales ' + /-([\d]{2})-/.exec(dstring)[1] + '/' + /-([\d]{2})$/.exec(dstring)[1]);
					$(this).datepicker('destroy');
					$(this).focus();
				}
    });
	d.datepicker('show');
}
	function change_uid() {
		document.location = '?tid=' + $("#tid").val();
	}
<?php }; ?>
</script>
<table id="training-table">
<tr>
    <td colspan="<?php echo sizeof($form_fields) + 1; ?>" class="no-borders" style="text-align: left;">
		<br />
	</td>
<tr>
<tr>
    <th colspan="<?php echo sizeof($form_fields) + 1; ?>" class="header-title">Training Settings
</th>
<tr>
<tr>
    <td colspan="<?php echo sizeof($form_fields) + 1; ?>" class="no-borders">&nbsp;</td>
<tr>
<tr>
	<td colspan="<?php echo sizeof($form_fields) + 1; ?>" class="no-borders current-level">
This system sets the totals required before staff training levels go green.<br />
Please set pass percentage for Theory, set the total required hours and set the total required jumps.
	</td>
</tr>
<?php
	foreach ($headers as $header => $fields) {
		$span = '';
		if (in_array($header, array('Month', 'Comments'))) {
			$span = ' rowspan="2"';
			$class = "";
		} else {
			$span = ' colspan="'.sizeof($fields).'"';
			$class = " class=\"no-border-bottom\"";
		};
		$field = $to_field($header);
		echo "\t<th id=\"$field-header\"$span$class>$header</th>\n";
	};
	if ($can_edit) {
		echo "\t<th id=\"actions-header\" rowspan=\"2\" class=\"no-borders\"></th>\n";
	};
?>
</tr>
<tr class='sub-header'>
<?php
	foreach ($headers as $header => $fields) {
		if (in_array($header, array('Month', 'Comments'))) {
			continue;
		};
		$field = $to_field($header);
		foreach ($fields as $sub_field) {
			echo "\t<th id=\"$field-" . strtolower($sub_field) . "-header\" class=\"small-borders-top-right\">$sub_field</th>\n";
		};
	};
?>
</tr>
<tr class="totals-row">
<?php
	$sql = "SELECT * FROM training_settings WHERE id = '".CURRENT_SITE_ID."'";
	$res = mysql_query($sql) or die(mysql_error());
	$row = mysql_fetch_assoc($res);
	foreach ($form_fields as $key => $field) {
		if (!is_array($row) || !array_key_exists($field, $row)) {
			$row[$field] = 0;
		};
		$text = $row[$field];
		if (in_array($field, array('month', 'comments'))) {
			$text = 'TOTAL';
			$class = ' total-header';
		} else {
			$class = ' value';
		};
		if (preg_match("/(theory|hours)/", $field)) {
			$class .= ' small-border-right';
		};
		if (preg_match("/(jumps|practical|hours)/", $field)) {
			$class .= ' small-border-left';
		};
		if (preg_match("/(theory)/", $field)) {
			$text .= '%';
		};
		echo "\t<th id=\"$field-total\" class=\"$field$class\" rel=\"$field\">{$text}</th>\n";
	};
?>
	<th class='no-borders'><input type="button" value="Edit" rel="<?php echo $row['id']; ?>" class="edit"></th>
</tr>
<tr>
	<th class='no-borders'><br /><button id="back" onClick="javascript: document.location='/Training/'">Back</button></th>
</tr>
</table>
</form>
<?php include ("ticker.php"); ?>
</body>
</html>
