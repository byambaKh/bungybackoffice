<?php
include "../includes/application_top.php";

//Check if the user is logged in before giving out data
$user = new BJUser();
if (!$user->isUser()) die();

//mysql_set_charset('utf8');
mysql_query("SET NAMES 'utf8'");
$action = $_GET['action'];
$type = $_GET['type'];
if (empty($type)) {
    $type = 'expenses';
};
if ($type == 'payroll') {
    $_POST['date'] = $_GET['date'] . '-' . $_POST['day'];
    unset($_POST['day']);
};
switch (TRUE) {
    case ($action == 'publishToggle') :
        $sql = "UPDATE training_manuals SET published = !published WHERE id = {$_POST['id']}";
        mysql_query($sql);
        $result = null;//we could check to see if it was set
        //get the value
        break;
    case ($action == 'update_row') :
        $action = 'insert';
        $where = '';
        if (isset($_POST['id'])) {
            if ($type == 'training_settings') {
                $res = mysql_query("SELECT * FROM $type WHERE id = '{$_POST['id']}';");
                if (mysql_num_rows($res) < 1) {
                    db_perform($type, array('id' => $_POST['id']));
                };
            };
            $action = 'update';
            $where = 'id=' . $_POST['id'];
            unset($_POST['id']);
        };
        if ($type == 'training') {
            $_POST['date'] = $_POST['month'] . '-01';
            foreach ($_POST as $field => $value) {
                mysql_query("ALTER TABLE $type ADD $field INT(11) NOT NULL DEFAULT '0';");
            };
        };
        db_perform($type, $_POST, $action, $where) or die(mysql_error());
        $result = array('result' => 'success');
        break;
    case ($action == 'delete_row'):
        $sql = "DELETE FROM $type WHERE id = '{$_POST['id']}';";
        mysql_query($sql) or die(mysql_error());
        break;
};
$result['request'] = array(
    'action' => $action,
    'type' => $type,
    'post' => $_POST
);
echo json_encode($result);
?>
