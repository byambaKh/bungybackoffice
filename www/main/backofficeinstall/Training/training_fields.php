<?php
	$headers = array(
		"Month" 			=> array(),
		"Recovery"			=> array('Theory', 'Hours', 'Jumps'),
		"Reception"			=> array('Theory', 'Hours', 'Jumps'),
		"CS / Harnesser"	=> array('Theory', 'Hours', 'Jumps'),
		"Jump Operator"		=> array('Theory', 'Hours', 'Jumps'),
		"Jump Master"		=> array('Theory', 'Hours', 'Jumps'),
		"Site Manager"		=> array('Theory', 'Hours', 'Jumps'),
		"Photographer"		=> array('Theory', 'Hours', 'Jumps'),
		"First Aid"			=> array('Theory', 'Practical'),
		"Abseil"			=> array('Theory', 'Practical'),
		"Swift Water"		=> array('Theory', 'Practical'),
		"Comments"			=> array(),
	);
	$to_field = function ($item) {
		return strtolower(
            preg_replace(
                "/(_)$/",
                "",
                preg_replace("([^\w]+)", "_", $item)
            )
        );
	};
?>
