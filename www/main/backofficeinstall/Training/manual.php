<?php
include '../includes/application_top.php';
mysql_query("SET NAMES 'utf8';");
// be sure we works with main DB

function getLastChapterNumber($manualId)
{
    //Get the last chapter that was inserted for this manual
    $findLastChapterQuery = "SELECT * FROM training_manuals_chapters WHERE manual_id = $manualId ORDER BY `order` DESC LIMIT 1";
    $findLastChapterResults = mysql_query($findLastChapterQuery);

    if (mysql_num_rows($findLastChapterResults)) {
        $lastChapter = mysql_fetch_assoc($findLastChapterResults);

        return $lastChapter['order'] + 1;
    } else return 0;//no last chapter
}

$can_edit = false;
if ($user->hasRole(array('General Manager', 'SysAdmin', 'ManualEdit'))) {
    $can_edit = true;
}

if($user->getUserName() == "RomainD") $can_edit = true;
if($user->getUserName() == "Hiroshi") $can_edit = true;
if($user->getUserName() == "Phil") $can_edit = true;
if ($user->getUserName() == "Training") $can_edit = false;  //byamba - add 20191129 add read only access to special user

if ($_GET['preview'] == 1) {
    $can_edit = false;
}
if ($can_edit) {
    //make action lowercase and remove \r and \n from it (the insert frame above has these)
    $action = strtolower(str_replace(array("\r", "\n"), '', $_POST['action']));
    $db_action = false;
    $where = '';
    switch (TRUE) {
        case ($action == 'update'):
            $db_action = 'update';

            $chapters = $_POST['i'];
            foreach ($chapters as $chapterUpdate) {
                $manual_id = $chapterUpdate['manual_id'];
                $chapter_id = $chapterUpdate['id'];

                $where = 'id=' . $chapterUpdate['id'];
                unset($chapterUpdate['id']);//we don't want to set the id again as this can cause an error
                db_perform('training_manuals_chapters', $chapterUpdate, 'update', $where);

                //we have an uploaded image
                if (count($_FILES['image' . $chapter_id]['name']) > 0) {
                  
                  	if (!is_dir('img/' . $manual_id )) {
 					   mkdir('img/' . $manual_id , 0755, true);
					}
                    $savedImage = move_uploaded_file($_FILES['image' . $chapter_id]["tmp_name"], 'img/' . $manual_id . '/' . $chapter_id . '.jpg');
                    //TODO should we perform some kind of test or just assume it worked because it always does
                    //Just assume for now, but add late we could send a notification to me if this fails.
                }
                /*
                if (!empty($_FILES['image'.$chapter_id]) && !move_uploaded_file($_FILES['image'.$chapter_id]["tmp_name"], 'img/' . $manual_id . '/' . $chapter_id . '.jpg')) {
                    //print_r($_FILES);
                    //print_r($_POST);
                    //print_r($data);
                    //print_r($chapter_id);
                    die();
                }
                */
            }
            break;
        case ($action == 'add'):
            $db_action = $db_action ? $db_action : 'insert';

            $data = $_POST['i'];
            $chapter_id = $data['id'];
            $manual_id = $data['manual_id'];

            $order = getLastChapterNumber($manual_id) + 1;
            $data['order'] = $order;

            unset($data['id']);
            db_perform('training_manuals_chapters', $data, $db_action, $where);
            if ($db_action == 'insert') {
                $chapter_id = mysql_insert_id();
            }
            if (!is_dir('img/' . $manual_id)) {
                mkdir('img/' . $manual_id);
            }
            if (!empty($_FILES['chapter_image']['name']) && !move_uploaded_file($_FILES["chapter_image"]["tmp_name"], 'img/' . $manual_id . '/' . $chapter_id . '.jpg')) {
                print_r($_FILES);
                print_r($_POST);
                print_r($data);
                print_r($chapter_id);
                die();
            }
            Header("Location: /Training/manual.php?id=" . $manual_id);
            die();
            break;
        case ($_GET['action'] == 'delete'):
            $sql = "delete from training_manuals_chapters where id = '{$_GET['chapter_id']}'";
            mysql_query($sql) or die(mysql_error());
            Header("Location: /Training/manual.php?id=" . $_GET['manual_id']);
            die();
            break;
        case ($action == 'csv'):
            $current_month = date("Y-m", $current_time);
            Header('Content-Type: text/csv; name="training' . $current_month . '.csv"');
            Header('Content-Disposition: attachment; filename="training' . $current_month . '.csv"');
            echo '"' . implode('","', $headers) . '"' . "\r\n";
            //for ($i = 1; $i <= 12; $i++) {
            //$d = $current_year . '-' . sprintf("%02d-", $i);
            $sql = "select * from training WHERE `date` like '$current_month%' order by `date` ASC;";
            $res = mysql_query($sql);
            $balance = 0;
            while ($row = mysql_fetch_assoc($res)) {
                foreach ($fields as $field) {
                    echo '"' . $row[$field] . '"' . ($field == $fields[sizeof($fields) - 1] ? '' : ',');
                }
                echo "\r\n";
            }
            //}
            die();
            break;
        case ($action == "insertframeabove"):
            //Move everything that is after the new chapter down by one in the order of chapters
            $manualId = $_GET['id'];
            $currentChapterOrder = $_GET['order'];

            $shiftChapterDownByOne = "UPDATE training_manuals_chapters tmc SET `order` = `order` + 1 WHERE manual_id = $manualId AND `order` >= $currentChapterOrder;";
            $insertNewChapter = "INSERT INTO training_manuals_chapters (`manual_id`, `order`) VALUES ($manualId, $currentChapterOrder);";

            //echo $shiftChapterDownByOne."<br><br>";
            //echo $insertNewChapter;

            mysql_query($shiftChapterDownByOne);
            mysql_query($insertNewChapter);
            break;
    }
}
$sql = "SELECT * FROM training_manuals WHERE id = '{$_GET['id']}'";
$res = mysql_query($sql) or die(mysql_error());
if ($row = mysql_fetch_assoc($res)) {
    $manual = $row;
} else {
    die('No manual id provided');
}
$chapters = array();
$sql = "SELECT * FROM training_manuals_chapters WHERE manual_id = '{$manual['id']}' ORDER BY `order` ASC";
$res = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_assoc($res)) {
    $chapters[] = $row;
}
$chaptersCount = count($chapters);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Manuals</title>
    <?php include "../includes/head_scripts.php"; ?>
    <script src="manuals_titles.js"></script>
    <script src="http://js.nicedit.com/nicEdit-latest.js" type="text/javascript"></script>
    <script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>

    <style>
        h1 {
            margin: 30px;
            text-align: center;
            width: 100%;
            color: black;
        }

        #manuals-table {
            margin-left: auto;
            margin-right: auto;
            width: 1000px;
            border-collapse: collapse;
        }

        #manuals-table th {
            font-size: 30px;
            padding: 10px;
            text-align: left;
            text-decoration: underline;
        }

        #manuals-table td {
            vertical-align: top;
            width: 300px;
            max-width: 300px;
        }

        #manuals-table td.text {
            min-width: 300px;
        }

        #manuals-table input, #manuals-table textarea {
            width: 98%;
        }

        textarea {
            height: 200px;
        }

        #manuals-table td.actions input {
            width: 98%;
        }

        #manuals-table td.actions {
            min-width: 0px !important;
            width: 70px;
        }

        .new-title {
            font-weight: bold;
            text-align: center;
        }

        .image {
            width: 300px !important;
        }

        .title {
            height: 18px;
            width: 350px;
        }

        #manuals-table tr.border td {
            border: 2px solid black;
        }

        .take {
            float: right;
        }
    </style>
</head>
<body>
<?php include 'includes/main_menu.php'; ?>
<script>
    $(document).ready(function () {
        $('.back').click(function () {
            document.location = 'manuals.php';
        });

        $('.delete').click(function () {
            var shouldDelete = confirm('Are you sure to delete this Chapter?');
            if (shouldDelete) {
                var chapterId = $(this).parent().parent().find('.chapterId').val();
                var manualId = $(this).parent().parent().find('.manualId').val();
                document.chaptersForm.action = 'manual.php?action=delete&chapter_id=' + chapterId + '&manual_id=' + manualId;
            }
        });

        $('.change').unbind('click').click(row_change);

        //sets the value of the input value to be the same as what is in nicEdit-main box so that the entire form is updated
        $('.update').click(function () {
            //var rel = $(this).parent().parent().attr('rel');
            $("#manuals-table").find('textarea').each(function (index, element) {
                $(element).val($(element).parent().find('.nicEdit-main').html());
            });
        });

        $('.add').click(function () {
            var rel = $(this).parent().parent().attr('rel');
            $("tr[rel=" + rel + "]").find('textarea').each(function () {
                $(this).val($(this).parent().find('.nicEdit-main').html());
            });//sets the value of the text area to be the same as what is in the text box
        });

        $('.insertFrameAbove').click(function () {
            var manualId = $(this).parent().parent().find('.manualId').val();
            var order = $(this).parent().parent().find('.order').val();

            document.chaptersForm.action = 'manual.php?action=insertframeabove&order=' + order + '&id=' + manualId;
        });

    });
</script>
<?php
$form_fields = array('image', 'title_en', 'title_ja');
?>
<table style='border: 1px;' id="manuals-table">
    <tr>
        <td colspan="<?php echo sizeof($form_fields) + $can_edit; ?>" class="no-borders">&nbsp;</td>
    </tr>
    <tr>
        <th colspan="<?php echo sizeof($form_fields) + $can_edit; ?>"
            class="header-title"><?php echo $manual['title_en'] . '<br>' . $manual['title_ja']; ?></th>
    </tr>
    <tr>
        <td colspan="<?php echo sizeof($form_fields) + $can_edit; ?>" class="no-borders">&nbsp;
            <?php
            if ($can_edit) {
                echo "<a href='manual.php?id={$manual['id']}&preview=1'>Preview</a>";
            }
            if (array_key_exists('preview', $_GET)) {
                echo "<a href='manual.php?id={$manual['id']}'>Edit</a>";
            }
            ?>
        </td>
    </tr>
    <?php
    echo "<tr><td colspan='" . (3 + $can_edit) . "'><button class='back' id='back'>Back</button></td></tr>";

    echo "\n<form name='chaptersForm' enctype='multipart/form-data' method='POST' action='manual.php?id={$_GET['id']}#order{$chapter['order']}'><!-- Start Form -->\n";
    foreach ($chapters as $chapter) {
        echo "<tr>";
        echo "<td colspan='" . (3 + $can_edit) . "'>&nbsp;</td>";
        echo "</tr>";
        //echo "<form enctype='multipart/form-data' method='POST' action='manual.php?id={$_GET['id']}'>";
        //echo "<form enctype='multipart/form-data' method='POST' action='manual.php#chap{$chapter['id']}'>";
        echo "<tr class='border' rel='chap{$chapter['id']}' id='order{$chapter['order']}'>";
        echo "<td rowspan='2' class='image' valign='top'>";
        $filename = 'img/' . $chapter['manual_id'] . '/' . $chapter['id'] . '.jpg';
        if (is_file($filename)) {
            echo "<img src='$filename' width='300px'>";
        }
        if ($can_edit) {
            echo "\t<input type='hidden' name='i[{$chapter['id']}][id]' class='chapterId' value='{$chapter['id']}'>\n";
            echo "\t<input type='hidden' name='i[{$chapter['id']}][manual_id]' class='manualId' value='{$chapter['manual_id']}'>\n";
            echo "\t<input type='hidden' name='i[{$chapter['id']}][order]' class='order' value='{$chapter['order']}'>\n";
            echo "\t<br>";
            echo "\t<input name='image{$chapter['id']}' type='file'>\n";
        }
        echo "<td class='title'>";
        if ($can_edit) {
            echo "<input name='i[{$chapter['id']}][title_en]' id='ititle_en[{$chapter['id']}][title_ja]' value='{$chapter['title_en']}'>";
            //echo draw_input_field("title_en", 'ititle_en' . $chapter['id'], $chapter);
        } else {
            echo $chapter['title_en'];
        }
        echo "</td>";
        echo "<td class='title'>";
        if ($can_edit) {
            echo "<input name='i[{$chapter['id']}][title_ja]' id='ititle_ja[{$chapter['id']}][title_ja]' value='{$chapter['title_ja']}'>";
            //echo draw_input_field("title_ja", 'ititle_ja' . $chapter['id'], $chapter);
        } else {
            echo $chapter['title_ja'];
        }
        echo "</td>";
        if ($can_edit) {
            echo "<td class='actions' rowspan='2' valign='top'>";
            echo "<input class='update' type='submit' value='Update' name='action'><br>";
            echo "<input class='delete' type='submit' value='Delete' name='action'>";
            echo "<input class='insertFrameAbove' type='submit' value='Insert\nFrame\nAbove' name='action'>";
            echo "</td>";
        }
        echo "</tr>";
        echo "<tr class='border' rel='chap{$chapter['id']}'>";
        echo "<td>";
        if ($can_edit) {
            //echo draw_textarea('text_en', 'itext_en' . $chapter['id'], $chapter);
            echo "<textarea name='i[{$chapter['id']}][text_en]' id='itext_jp{$chapter['id']}'>{$chapter['text_en']}</textarea>";
        } else {
            echo $chapter['text_en'];
        }
        echo "</td>";
        echo "<td>";
        if ($can_edit) {
            //echo draw_textarea('text_ja', 'itext_ja' . $chapter['id'], $chapter);
            echo "<textarea name='i[{$chapter['id']}][text_ja]' id='itext_en{$chapter['id']}'>{$chapter['text_ja']}</textarea>";
        } else {
            echo $chapter['text_ja'];
        }
        echo "</td>";
        echo "</tr>";
    }
    echo "\n</form><!-- End Form -->\n\n";
    ?>
    <?php if ($can_edit) { ?>
        <form method="post" enctype="multipart/form-data">
            <tr>
                <td colspan='4'>&nbsp;</td>
            </tr>
            <tr>
                <td class="new-title" colspan='4' style="background-color: silver;"><br>New Chapter<br><br></td>
            </tr>
            <tr>
                <td class="new-title">Image</td>
                <td class="new-title">English</td>
                <td class="new-title">Japanese</td>
                <td class="new-title">&nbsp;</td>
            </tr>
            <tr class="border" rel='new-chapter'>
                <?php
                $chapter = array();
                echo "<td rowspan='2'>";
                echo "<input name='i[manual_id]' type='hidden' value='{$manual['id']}'>";
                echo "<input name='chapter_image' type='file'>";
                echo "<td>";
                echo draw_input_field('title_en', 'ititle_en', $chapter);
                echo "</td>";
                echo "<td>";
                echo draw_input_field('title_ja', 'ititle_ja', $chapter);
                echo "</td>";
                echo "<td rowspan='2' valign='top'>";
                echo "<button name='action' class='add' type='submit' value='add'>Add</button>";
                echo "</td>";
                echo "</tr>";
                echo "<tr class='border' rel='new-chapter'>";
                echo "<td class='text'>";
                echo draw_textarea('text_en', 'itext_en', $chapter);
                echo "</td>";
                echo "<td class='text'>";
                echo draw_textarea('text_ja', 'itext_ja', $chapter);
                echo "</td>";
                ?>
            </tr>
        </form>
    <?php } ?>
    <tr>
        <td colspan="<?php echo sizeof($form_fields) + $can_edit; ?>" class="no-borders">&nbsp;</td>
    </tr>
    <tr>
        <td colspan='<?php echo sizeof($form_fields) + $can_edit; ?>' class='submit'>
            <?php if ($chaptersCount || $can_edit)://only show the second back button if there is a chapter there?>
                <button class='back' id='back'>Back</button>
            <?php endif ?>

            <?php
            $sql = "SELECT tt.* FROM training_tests tt LEFT JOIN training_tests_results ttr on (tt.id=ttr.test_id and ttr.user_id = '{$_SESSION['uid']}' and take_date like '" . date("Y-m-") . "%') WHERE tt.manual_id = '{$manual['id']}' and ttr.id IS NULL;";
            $res = mysql_query($sql) or die(mysql_error());
            if ($test = mysql_fetch_assoc($res)) {
                echo "<button class='take' onClick=\"document.location = 'take_test.php?id={$test['id']}&lang=ja'\">テストを受ける</button>";
                echo "<button class='take' onClick=\"document.location = 'take_test.php?id={$test['id']}&lang=en'\">Take Test</button>";
            }
            ?>
        </td>
    </tr>
</table>
<br/>
<br/>

<?php include("ticker.php"); ?>
</body>
</html>
