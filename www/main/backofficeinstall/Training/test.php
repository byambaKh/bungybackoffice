<?php
include '../includes/application_top.php';
// be sure we works with main DB
mysql_query("SET NAMES 'utf8';");

// by manual ID
if (isset($_GET['manual_id'])) {
    $sql = "SELECT * FROM training_tests WHERE manual_id = '{$_GET['manual_id']}' ORDER by id ASC LIMIT 1;";
    $res = mysql_query($sql) or die(mysql_error());
    if ($row = mysql_fetch_assoc($res)) {
        $_GET['id'] = $row['id'];
    } else {
        die('There is no test for this manual');
    };
}

$can_edit = false;


if ($user->hasRole(['General Manager', 'SysAdmin']) || ($user->getUserName() == "Hiroshi")) {
    $can_edit = true;
} else {
    header("Location: take_test.php?id=" . $_GET['id'] . "&lang=" . $_GET['lang']);
}

if ($_GET['preview'] == 1) {
    $can_edit = false;
}

if ($can_edit) {
    $action = $_POST['action'];
    $db_action = false;
    $where = '';
    $lang = $_GET['lang'];
    switch (TRUE) {
        case ($action == 'update'):
        case ($action == 'Update'):
            $where = 'id=' . $_POST['i']['id'];
            $db_action = 'update';
        case ($action == 'add'):
        case ($action == 'Add'):
            $db_action = $db_action ? $db_action : 'insert';
            $data = $_POST['i'];
            $test_id = $data['test_id'];
            $question_id = $data['id'];
            unset($data['id']);
            if ($data['type'] == 3) {
                $data['a1'] = '';
                $data['a2'] = '';
                $data['a3'] = '';
                $data['a4'] = '';
            };
            if ($data['type'] == 2) {
                for ($i = 1; $i < 5; $i++) {
                    $data['a' . $i . '_points'] = (array_key_exists('a' . $i . '_points', $_POST) && $_POST['a' . $i . '_points'] != '') ? 1 : 0;
                }
            } else {
                for ($i = 1; $i < 5; $i++) {
                    $data['a' . $i . '_points'] = ($i == $_POST['a_points']) ? 1 : 0;
                }
            };
            db_perform('training_questions', $data, $db_action, $where);
            if ($db_action == 'insert') {
                $question_id = mysql_insert_id();
            };
            if (!is_dir('img/tests/' . $test_id)) {
                mkdir('img/tests/' . $test_id);
            };
//echo $_FILES["question_image"]["tmp_name"] . ' ' . 'img/tests/' . $test_id . '/' . $question_id . '.jpg';
            if ($_POST['delete_image']) {
                unlink('img/tests/' . $test_id . '/' . $question_id . '.jpg');
            };
            if (!empty($_FILES['question_image']['name']) && !move_uploaded_file($_FILES["question_image"]["tmp_name"], 'img/tests/' . $test_id . '/' . $question_id . '.jpg')) {
                print_r($_FILES);
                print_r($_POST);
                print_r($data);
                print_r($question_id);
                die();
            };

            Header("Location: /Training/test.php?id=" . $test_id . '&lang=' . $lang);
            die();
            break;
        case ($action == 'Delete'):
        case ($action == 'delete'):
            unlink('img/tests/' . $_POST['i']['test_id'] . '/' . $_POST['i']['id'] . '.jpg');
            $sql = "delete from training_questions where id = '{$_POST['i']['id']}'";
            mysql_query($sql) or die(mysql_error());
            Header("Location: /Training/test.php?id=" . $_POST['i']['test_id'] . '&lang=' . $lang);
            die();
            break;
    };
};
$sql = "SELECT * FROM training_tests WHERE id = '{$_GET['id']}'";
$res = mysql_query($sql) or die(mysql_error());
if ($row = mysql_fetch_assoc($res)) {
    $test = $row;
} else {
    die('No test id provided');
};
$sql = "SELECT * FROM training_manuals WHERE id = '{$test['manual_id']}'";
$res = mysql_query($sql) or die(mysql_error());
if ($row = mysql_fetch_assoc($res)) {
    $manual = $row;
} else {
    die('No test id provided');
};
$questions = array();
$sql = "SELECT * FROM training_questions WHERE test_id = '{$test['id']}' AND lang = '$lang' ORDER BY question ASC";
$res = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_assoc($res)) {
    $questions[] = $row;
};
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Manuals</title>
    <?php include "../includes/head_scripts.php"; ?>
    <script src="http://js.nicedit.com/nicEdit-latest.js" type="text/javascript"></script>
    <script type="text/javascript">bkLib.onDomLoaded(nicEditors.allTextAreas);</script>
    <style>
        h1 {
            margin: 30px;
            text-align: center;
            width: 100%;
            color: black;
        }

        #manuals-table {
            margin-left: auto;
            margin-right: auto;
            width: 1000px;
            border-collapse: collapse;
        }

        #manuals-table th {
            font-size: 30px;
            padding: 10px;
            text-align: left;
            text-decoration: underline;
        }

        #manuals-table td {
            vertical-align: top;
        }

        #manuals-table input {
            width: 80%;
        }

        #manuals-table input[type='radio'] {
            width: 20px !important;
        }

        #manuals-table input[type='checkbox'] {
            width: 20px !important;
        }

        #manuals-table textarea {
            width: 98%;
        }

        textarea {
            height: 50px;
            width: 99%;
        }

        #manuals-table td.actions input, #manuals-table td.actions button {
            width: 98% !important;
        }

        .actions {
            width: 70px;
        }

        .new-title {
            font-weight: bold;
            text-align: center;
        }

        .title {
            height: 18px;
        }

        #manuals-table tr.border td {
            /*border: 2px solid black;*/
        }

        .question {
            background-color: silver;
        }

        .question td {
            padding: 5px;
            font-weight: bold;
        }
    </style>
</head>
<body>
<?php include 'includes/main_menu.php'; ?>
<script>
    $(document).ready(function () {
        $(".delete").click(function () {
            return confirm('Are you sure to delete this Item?');
        });
        $('.update, .add').click(function () {
            var rel = $(this).parent().parent().attr('rel');
            $("tr[rel=" + rel + "]").find('textarea').each(function () {
                $(this).val($(this).parent().find('.nicEdit-main').html());
            });
        });
        $('.type input').change(function () {
            var disabled = false;
            if ($(this).val() == 3) {
                disabled = 'disabled';
            }
            ;
            var q_id = $(this).parent().parent().attr('rel');
            $("tr[rel=" + q_id + "] td.answer input").attr('disabled', disabled);
            var question_type = 'radio';
            switch ($(this).val()) {
                case '2':
                    question_type = 'checkbox';
                    break;

            }
            for (var i = 1; i < 5; i++) {
                $("tr[rel=" + q_id + "] td.answer input.a" + i + "_points").prop('type', question_type)
                    .prop('name', question_type == 'radio' ? 'a_points' : "a" + i + "_points")
                    .prop('value', question_type == 'radio' ? i : 1);
                //$("tr[rel="+q_id+"] td.answer input[name=a"+i+"_points]").replaceWith(
                //$("<input name='a"+i+"_points' type='"+question_type+"' value='1'>")
                //);
            }
            ;
        });
    });
</script>
<?php
$form_fields = array('a1', 'a3');
?>
<table id="manuals-table">
    <tr>
        <td colspan="<?php echo sizeof($form_fields) + $can_edit; ?>" class="no-borders">&nbsp;</td>
    </tr>
    <tr>
        <th colspan="<?php echo sizeof($form_fields) + $can_edit; ?>" class="header-title"><?php echo $manual['title_' . $lang]; ?></th>
    </tr>
    <tr>
        <td colspan="<?php echo sizeof($form_fields) + $can_edit; ?>" class="no-borders">&nbsp;
            <?php
            if ($can_edit) {
                echo "<a href='take_test.php?id={$_GET['id']}&preview=1&lang=$lang'>Take test</a>";
            };
            ?>
        </td>
    </tr>
    <?php
    $i = 1;
    foreach ($questions as $question) {
        echo "<form method='post' enctype=\"multipart/form-data\">";
        echo "<tr class='question'><td colspan='3'>Question #" . ($i++) . "</td></tr>";
        echo "<tr rel='q{$question['id']}'>";
        echo "<td colspan='2' rel='q{$question['id']}'>";
        $filename = 'img/tests/' . $question['test_id'] . '/' . $question['id'] . '.jpg';
        if (is_file($filename)) {
            echo "<img src='$filename'>";
        };
        echo "<input name='question_image' type='file'>";
        if (is_file($filename)) {
            echo "<input name='delete_image' type='checkbox' id='delete-image'> Delete Image";
        };
        echo "<td rowspan='4' valign='top' class='actions'>";
        echo "<button name='action' type='submit' value='update' class='update'>Update</button>";
        echo "<button name='action' type='submit' value='delete' class='delete'>Delete</button>";
        echo "</td>";
        echo "</tr>";
        echo "<tr rel='q{$question['id']}'>";
        echo "<td colspan='2'>";
        echo "<input name='i[test_id]' type='hidden' value='{$test['id']}'>";
        echo "<input name='i[lang]' type='hidden' value='{$lang}'>";
        echo "<input name='i[id]' type='hidden' value='{$question['id']}'>";
        echo "<textarea name='i[question]' rows='3'>{$question['question']}</textarea>";
        echo "</td>";
        echo "</tr>";
        echo "<tr class='border' rel='q{$question['id']}'>";
        echo "<td colspan='2' class='type'>Question type: ";
        echo "<input name='i[type]' type='radio' value='1'" . (($question['type'] == 1) ? " checked='checked'" : "") . ">Radio";
        echo "<input name='i[type]' type='radio' value='2'" . (($question['type'] == 2) ? " checked='checked'" : "") . ">Check Boxes";
        //echo "<input name='i[type]' type='radio' value='3'" . (($question['type'] == 3) ? " checked='checked'" : "") . ">Input Fields";
        echo "</td>";
        echo "</tr>";
        echo "<tr class='border' rel='q{$question['id']}'>";
        echo "<td class='answer'>1: ";
        echo "<input type='" . ($question['type'] == 1 ? 'radio' : 'checkbox') . "' 
				name='a" . ($question['type'] == 1 ? '' : 1) . "_points' 
				value='" . ($question['type'] == 1 ? 1 : 1) . "' 
				class='a1_points'
				" . ($question['a1_points'] > 0 ? 'checked' : '') . ">";
        echo draw_input_field('a1', 'ia1', $question, ($question['type'] == 3) ? " disabled='disabled'" : "");
        echo "</td>";
        echo "<td class='answer'>3: ";
        echo "<input type='" . ($question['type'] == 1 ? 'radio' : 'checkbox') . "' 
				name='a" . ($question['type'] == 1 ? '' : 3) . "_points' 
				value='" . ($question['type'] == 1 ? 3 : 1) . "' 
				class='a3_points'
                " . ($question['a3_points'] > 0 ? 'checked' : '') . ">";
        echo draw_input_field('a3', 'ia3', $question, ($question['type'] == 3) ? " disabled='disabled'" : "");
        echo "</td>";
        echo "</tr>";
        echo "<tr class='border' rel='q{$question['id']}'>";
        echo "<td class='answer'>2: ";
        echo "<input type='" . ($question['type'] == 1 ? 'radio' : 'checkbox') . "' 
				name='a" . ($question['type'] == 1 ? '' : 2) . "_points' 
				value='" . ($question['type'] == 1 ? 2 : 1) . "' 
				class='a2_points'
                " . ($question['a2_points'] > 0 ? 'checked' : '') . ">";
        echo draw_input_field('a2', 'ia2', $question, ($question['type'] == 3) ? " disabled='disabled'" : "");
        echo "</td>";
        echo "<td class='answer'>4: ";
        echo "<input type='" . ($question['type'] == 1 ? 'radio' : 'checkbox') . "' 
				name='a" . ($question['type'] == 1 ? '' : 4) . "_points' 
				value='" . ($question['type'] == 1 ? 4 : 1) . "' 
				class='a4_points'
                " . ($question['a4_points'] > 0 ? 'checked' : '') . ">";
        echo draw_input_field('a4', 'ia4', $question, ($question['type'] == 3) ? " disabled='disabled'" : "");
        echo "</td>";
        echo "</tr>";
        echo "</form>";
    };
    ?>
    <?php if ($can_edit) { ?>
        <form method="post" enctype="multipart/form-data">
            <tr>
                <td colspan='4'>&nbsp;</td>
            </tr>
            <tr>
                <td class="new-title" colspan='4' style="background-color: silver;"><br>New Question<br><br></td>
            </tr>
            <tr class="border" rel='new'>
                <?php
                $question = array('type' => 1);
                echo "<td colspan='2'>";
                echo "<input name='question_image' type='file'>";
                echo "<td rowspan='4' valign='top' class='actions'>";
                echo "<button name='action' class='add' type='submit' value='add'>Add</button>";
                echo "</td>";
                echo "</tr>";
                echo "<tr rel='new'>";
                echo "<td colspan='2'>";
                echo "<input name='i[test_id]' type='hidden' value='{$test['id']}'>";
                echo "<input name='i[lang]' type='hidden' value='{$lang}'>";
                echo "<textarea name='i[question]' rows='3'></textarea>";
                echo "</td>";
                echo "</tr>";
                echo "<tr class='border' rel='new'>";
                echo "<td colspan='2' class='type'>Question type: ";
                echo "<input name='i[type]' type='radio' value='1' checked='checked'>Radio";
                echo "<input name='i[type]' type='radio' value='2'>Check Boxes";
                //echo "<input name='i[type]' type='radio' value='3'>Input Fields";
                echo "</td>";
                echo "</tr>";
                echo "<tr class='border' rel='new'>";
                echo "<td class='answer'>1: ";
                echo "<input type='" . ($question['type'] == 1 ? 'radio' : 'checkbox') . "' 
				name='a" . ($question['type'] == 1 ? '' : 1) . "_points' 
				value='" . ($question['type'] == 1 ? 1 : 1) . "' 
				class='a1_points'>";
                echo draw_input_field('a1', 'ia1', $question);
                echo "</td>";
                echo "<td class='answer'>3: ";
                echo "<input type='" . ($question['type'] == 1 ? 'radio' : 'checkbox') . "' 
				name='a" . ($question['type'] == 1 ? '' : 3) . "_points' 
				value='" . ($question['type'] == 1 ? 3 : 1) . "' 
				class='a3_points'>";
                echo draw_input_field('a3', 'ia3', $question);
                echo "</td>";
                echo "</tr>";
                echo "<tr class='border' rel='new'>";
                echo "<td class='answer'>2: ";
                echo "<input type='" . ($question['type'] == 1 ? 'radio' : 'checkbox') . "' 
				name='a" . ($question['type'] == 1 ? '' : 3) . "_points' 
				value='" . ($question['type'] == 1 ? 3 : 1) . "' 
				class='a3_points'>";
                echo draw_input_field('a2', 'ia2', $question);
                echo "</td>";
                echo "<td class='answer'>4: ";
                echo "<input type='" . ($question['type'] == 1 ? 'radio' : 'checkbox') . "' 
				name='a" . ($question['type'] == 1 ? '' : 4) . "_points' 
				value='" . ($question['type'] == 1 ? 4 : 1) . "' 
				class='a4_points'>";
                echo draw_input_field('a4', 'ia4', $question);
                echo "</td>";
                ?>
            </tr>
        </form>
    <?php }; ?>
    <tr>
        <td colspan=3>
            <button id='back' onClick="document.location = '/Training/tests.php';">Back</button>
        </td>
    </tr>
</table>
<br/>
<br/>

<?php include("ticker.php"); ?>
</body>
</html>
