<?php
include '../includes/application_top.php';
//Check if the user is logged in before going any further
$user = new BJUser();
if (!$user->isUser()){
    auth_user();
    die();
}
$sql = 'SET time_zone = "+9:00"';
$result = queryForRows($sql);//Set to japan time.
$sql = '
SELECT
  printjobs.id,
  waivers.lastname,
  waivers.firstname,
  waivers.weight_kg,
  printjobs.to_print,
  printjobs.site_id,
  printjobs.jump_code,
  customerregs1.BookingDate  
FROM sites
LEFT JOIN printjobs
  ON printjobs.site_id = sites.id
LEFT JOIN waivers
  ON printjobs.id = waivers.id
LEFT JOIN customerregs1
  ON waivers.bid = customerregs1.CustomerRegID 
WHERE sites.subdomain = "'. join('.', explode('.', $_SERVER['HTTP_HOST'], -2)) .'"
  AND BookingDate = LEFT(NOW(),10)
';
$result = queryForRows($sql);//Fetch the details of todays bookings.

function drawTable($result)//Creates a simple table from the results
{
    echo "<div class='tableWrapper'>";
    echo "<table>";
    echo "<tr>
            <th>ID</th>
            <th>Last Name</th>
            <th>First Name</th>
            <th>Weight</th>
            <th>Reprint</th>
            <th>Print status</th>
          </tr>";
    foreach ($result as $value) 
    {
        echo "<tr>";
        echo "<td>" . $value['id'] . "</td>";
        echo "<td>" . $value['lastname'] . "</td>";
        echo "<td>" . $value['firstname'] . "</td>";
        echo "<td>" . $value['weight_kg'] . "</td>";
        $clickevent = 'setPrint("'. $value['id'] . '","' . $value['site_id'] .'","' . $value['jump_code'] . '")';
        echo "<td><button type='button' onclick='" . $clickevent . "' id=button". $value['id'] . ">Print</button></td>";
        echo "<td id=print". $value['id'] . ">" . ($value['to_print'] == 0 ? "Not printing" : "Printing") . "</td>";
        echo "</tr>";
    }
    echo "</table>";
    echo "</div>";
}
?>

<!DOCTYPE html>
<html>
<head>

<link rel="stylesheet" type="text/css" href="css/printServer.css">
    <script>
    function setPrint(id, site, code)//Ajax to allow reprints without a reload
    {
        var xhttp = new XMLHttpRequest();
        xhttp.onreadystatechange = function() 
        {
            if (this.readyState == 4 && this.status == 200) 
            {
                document.getElementById("print"+id).innerHTML = "Printing";
            }
        }
      xhttp.open("GET", "setPrintStatus.php?id="+id+"&site="+site+"&code="+code, true);
      xhttp.send();
    }
    </script>
    <title>Print Server Controls</title>
    <meta charset="UTF-8">
</head>
<body>
    <?php drawTable($result)?>   
</body>
</html>
