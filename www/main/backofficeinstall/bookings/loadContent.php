<?php
function loadContent($path)
{
    //Get content based on language
    $filename =  $path;
    $json = file_get_contents($filename);
    $decoded = json_decode($json, true);
    if($_GET["lang"] == "en") $content = $decoded["en"];
    else $content = $decoded["jp"];
    return $content;
}
?>