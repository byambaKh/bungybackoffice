<?php
session_start();
$freeJumpTimes = $_SESSION['freeJumpTimes'];
$site_id = $_POST['site_id'];
$jumpers = $_POST['jumpers'];
$date = $_POST['date'];
$result = array();

foreach($freeJumpTimes as $key => $time) //Make an array of free jump timeslots.
{
    if($time["site_id"] == $site_id && $time["slots"] >= $jumpers && $time["BookingDate"] == $date)
    {
        $result[$key] = $time["BookingTime"];
    }
}
echo json_encode($result);
?>