<?php
header('Content-Type: text/html; charset=utf-8'); //need this to avoid encoding issues.
include("../includes/application_top_noAuth.php");
include_once("navbar/navbar.php");
include_once("loadContent.php");


//Get content based on language
$content = loadContent("regcomplete.json");
$mailContent = loadContent("mailcontent.json");
mysql_query('SET NAMES utf8;');
session_start();

$confirmCode = createConfirmationCode();
$emailOptOut = $_POST['email_opt_out'] == "on" ? 1 : 0;

$booking = [
    'site_id'           => mysql_real_escape_string ($_SESSION['place']),
    'ConfirmCode'       => $confirmCode,
    'BookingDate'       => mysql_real_escape_string ($_SESSION['date']),
    'BookingTime'       => mysql_real_escape_string ($_SESSION['time']),
    'NoOfJump'          => mysql_real_escape_string ($_SESSION['numPeople']),
    'RomajiName'        => mysql_real_escape_string ($_POST['lastname']) 
                           . ' ' . 
                           mysql_real_escape_string ($_POST['firstname']),
    'CustomerLastName'  => mysql_real_escape_string ($_POST['lastname']),
    'CustomerFirstName' => mysql_real_escape_string ($_POST['firstname']),
    'ContactNo'         => mysql_real_escape_string ($_POST['phone']),
    'CustomerEmail'     => mysql_real_escape_string ($_POST['email']),
    'TransportMode'     => mysql_real_escape_string ($_POST['transportation']),
    'place_id'          => mysql_real_escape_string ($_SESSION['place']), // Strictly necessary? Not sure whether place_id is ever used.
    'email_opt_out'     => $emailOptOut,
    'created'           => date('Y-m-d H:i:s'),
    'discount'          => ($_SESSION['discount'] == "karaoke" ? 1000 : 0),
    'discount_code'      =>($_SESSION['discount'] == "karaoke" ? "karaoke" : null)
];


    saveBooking($booking);
    sendEmail($booking, $mailContent);

function saveBooking($booking)
{
    if($_SESSION['booking_saved'] == false) $result = db_perform('customerregs_temp1', $booking);
    $_SESSION['booking_saved'] = true; // Prevent refreshing repeating db entries.
}
function sendEmail($booking, $mailContent)//Compiles an email. Uses string replacement to fill in customer booking details needed in the email.
{
    if($_SESSION['mail_sent'] == false)
    {
        $emailLink    = $mailContent['url_' . $booking['site_id']] . $booking['ConfirmCode'];
        $message_body = $mailContent['content_' . $booking['site_id']];
        $message_body = str_replace("{EMAIL_BOOK_DATE}",         $booking['BookingDate'], $message_body);
        $message_body = str_replace("{EMAIL_BOOK_TIME}",         $booking['BookingTime'], $message_body);
        $message_body = str_replace("{EMAIL_JUMP_NUMBER}",       $booking['NoOfJump'],    $message_body);
        $message_body = str_replace("{EMAIL_CUSTOMER_NAME}",     $booking['RomajiName'],  $message_body);
        $message_body = str_replace("{EMAIL_CONFIRMATION_LINK}", $emailLink,              $message_body);       
        $formatted_text = chunk_split(base64_encode($message_body));
        $boundary = "boundary-" . rand(1000, 9999) . '-' . rand(100000, 999999);
        $headers =  "Return-Path: confirm@bungyjapan.com\r\n";
        $headers .= "Reply-To: " . $mailContent['return_address_' . $booking['site_id']]. "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: multipart/alternative;\r\n";
        $headers .= "\tboundary=\"$boundary\"\r\n";
        $message_body = <<<EOT
--$boundary
Content-Type: text/html; charset="UTF-8"
Content-Transfer-Encoding: base64

$formatted_text

--$boundary--
EOT;
        mail($booking['CustomerEmail'], 
            '=?UTF-8?B?' . base64_encode($mailContent['subject']) . '?=', 
            $message_body, 
            $headers, 
            "-f confirm@bungyjapan.com");
    }
    $_SESSION['mail_sent'] = true; // Prevent refreshing repeating emails.
}
function createConfirmationCode()//Can these collide? Probably won't ever happen.
{
    $confirmCode = md5(uniqid(rand()));
    return $confirmCode;
}
?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="css/bookings.css">
<link rel="stylesheet" type="text/css" href="css/navmenu.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="jquery-dateformat.min.js"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M23LPW5');</script>
<!-- End Google Tag Manager -->
<!-- Byamba add 2020-12-02 Block refresh page -->
<script>
if ( window.history.replaceState ) {
  window.history.replaceState( null, null, window.location.href );
}
</script>
<!-- Byamba end -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M23LPW5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="content">
    <div class="row">    	
            <?php echo $content['explanation_1'] ?>
    </div>
    <div class="row">    	
            <?php echo $content['explanation_2'] ?>
    </div>
    <div class="row">    	
            <?php echo $content['explanation_3'] ?>
    </div>
    <div class="row">    	
            <?php echo $content['explanation_4'] ?>
    </div>
</div>
</body>
</html>

