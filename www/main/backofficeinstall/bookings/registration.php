<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
header('Content-Type: text/html; charset=utf-8'); //need this to avoid encoding issues.
include("redirect.php"); // must come before content load as they share a $content variable.
include("../includes/application_top_noAuth.php");
include_once("navbar/navbar.php");
include_once("discount.php");
include_once("loadContent.php");

//Get content based on language
$content = loadContent("registration.json");
$err = loadContent("validationErrors.json");
mysql_query('SET NAMES utf8;');
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="css/bookings.css">
<link rel="stylesheet" type="text/css" href="css/navmenu.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery-form-validator/2.3.26/jquery.form-validator.min.js"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M23LPW5');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M23LPW5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="content">
    <form action="<?php echo $content['next_link'] ?>" method = "post" class="form">
        <div class="row">    	
                <h1><?php echo $content['heading'] ?></h1>
        </div>
        <div class="row">    	
                <?php echo $content['detail_date'] ?><span><?php echo $_SESSION['date'] ?></span>
        </div>
        <div class="row">    	
                <?php echo $content['detail_time'] ?><span><?php echo $_SESSION['time'] ?></span>
        </div>
        <div class="row">    	
                <?php echo $content['detail_number'] ?><span><?php echo $_SESSION['numPeople'] ?></span>
        </div>
        <div class="row">    	
                <?php echo $content['first_name'] ?><input required type="text" name="firstname" class="picker" data-validation="custom" data-validation-regexp="^([a-zA-Z]+)$" data-validation-error-msg="<?php echo $err['textOnly'] ?>">
        </div>
        <div class="row">    	
                <?php echo $content['last_name'] ?><input required type="text" name="lastname" class="picker" data-validation="custom" data-validation-regexp="^([a-zA-Z]+)$" data-validation-error-msg="<?php echo $err['textOnly'] ?>">
        </div>
        <div class="row notice">    	
                <?php echo $content['mail_note_1'] ?>
        </div>
        <div class="row">    	
                <?php echo $content['email'] ?><input required type="text" name="email" class="picker" data-validation="email" data-validation-error-msg="<?php echo $err['email'] ?>">
        </div>
        <div class="row notice">    	
                <?php echo $content['mail_note_2'] ?>
        </div>
        <div class="row">    	
                <?php echo $content['phone'] ?><input required type="text" name="phone" class="picker" data-validation="number" data-validation-error-msg="<?php echo $err['phone'] ?>">
        </div>
        <div class="row nowrap">    	
                <input type ="checkbox" name="email_optin" id="opt"><label for="opt"><?php echo $content['promo_request'] ?></label>
        </div>
        <div class="row">    	
                <?php echo $content['transportation'] ?>
                <select required name = "transportation" class="picker">
                <option value="" selected disabled hidden>   <?php echo $content['please_select'] ?></option>
                <option value="<?php echo $content['shinkansen']?>"><?php echo $content['shinkansen']?></option>
                <option value="<?php echo $content['train']?>">     <?php echo $content['train']?></option>
                <option value="<?php echo $content['car']?>">       <?php echo $content['car']?></option>
                <option value="<?php echo $content['bus']?>">       <?php echo $content['bus']?></option>
                <option value="<?php echo $content['motorbike']?>"> <?php echo $content['motorbike']?></option>
            </select>
        </div>
        <div class="row">    	
                <input required type="submit" class="pretty_link forward" value = "<?php echo $content['next'] ?>">
        </div>
        <div class="row">    	
                <a class="pretty_link back" href="<?php echo $content['back_link'] ?>"><?php echo $content['back'] ?></a>
        </div>
    </form>
</div>
<script>
    $.validate({
    });
</script>
</body>
</html>

