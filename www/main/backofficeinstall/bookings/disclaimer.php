<?php
session_start();
header('Content-Type: text/html; charset=utf-8'); //need this to avoid encoding issues.
include("../includes/application_top_noAuth.php");
include("navbar/navbar.php");
include_once("discount.php");
include_once("loadContent.php");

//Get content based on language
$content = loadContent("disclaimer.json");
mysql_query('SET NAMES utf8;');

if(!empty($_POST['place']))     $_SESSION['place']     = $_POST['place'];
if(!empty($_POST['numPeople'])) $_SESSION['numPeople'] = $_POST['numPeople'];
if(!empty($_POST['date']))      $_SESSION['date']      = $_POST['date'];
if(!empty($_POST['time']))      $_SESSION['time']      = $_POST['time'];

function U20Link($content) // get the link to the under - 20 disclaimer for the booking site.
{
    $name = 'waiver_link_' . $_SESSION['place'];
    if(array_key_exists($name, $content)) return $content[$name];
    else return $content['waiver_link_default'];
}

?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="css/bookings.css">
<link rel="stylesheet" type="text/css" href="css/navmenu.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="jquery-dateformat.min.js"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M23LPW5');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M23LPW5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="content">
    <div class="row">    	
        <h1><?php echo $content['heading']?></h1><h1><?php echo '<a class="" href="' . U20Link($content) . '"><img class="img" alt="english" src="' . $content['waiver_img'] . '"></a>'?></h1>
    </div>   
    <div class="row">    	
        <h2><?php echo $content['subheading_1'] ?></h2>
    </div>
    <div class="row">    	
        <?php echo $content['row_1a'] ?>
    </div>
    <div class="row">    	
        <?php echo $content['row_1b'] ?>
    </div>
    <div class="row">    	
        <?php echo $content['row_1c'] ?>
    </div>
    <div class="row">    	
        <?php echo $content['row_1d'] ?>
    </div>
    <div class="row">
        <h2></h2>       
         <h1><?php echo '<a class="" href="' . $content['contact_link'] . '"><img class="img" alt="english" src="' . $content['contact_img'] . '"></a>'?></h1>
    </div>
    <div class="row">    	        
        <h2><?php echo $content['subheading_2'] ?>            
        </h2>
    </div>
    <div class="row">    	
        <?php echo $content['row_2a'] ?>
    </div>
    <div class="row">    	
        <h2><?php echo $content['subheading_3'] ?></h2>
    </div>
    <div class="row">    	
        <?php echo $content['row_3a'] ?>
    </div>
    <div class="row">    	
        <h2><?php echo $content['subheading_4'] ?></h2>
    </div>
    <div class="row">    	
        <?php echo $content['row_4a'] ?>
    </div>
    <div class="row">    	
        <h2><?php echo $content['subheading_5'] ?></h2>
    </div>
    <div class="row">    	
        <?php echo $content['row_5a'] ?>
    </div>
    <div class="row">    	
        <?php echo $content['row_5b'] ?>
    </div>
     <div class="row">      
        <h2><?php echo $content['subheading_6'] ?></h2>
    </div>
    <div class="row">       
        <?php echo $content['row_6a'] ?>
    </div>
    <div class="row">    	
        <h2><?php echo $content['subheading_7'] ?></h2>
    </div>    
    <div class="row">    	
        <?php echo $content['row_7a'] ?>
    </div>
    <div class="row">    	
        <a class="pretty_link forward" href="<?php echo $content['next_link'] ?>"><?php echo $content['next'] ?></a>
    </div>
    <div class="row">    	
        <a class="pretty_link back" href="<?php echo $content['back_link'] ?>"><?php echo $content['back'] ?></a>
    </div>
    
</div>
</body>
</html>

