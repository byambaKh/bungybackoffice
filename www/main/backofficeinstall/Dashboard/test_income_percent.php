<?php
require_once('../includes/application_top.php');

function incomeDeltaPercent($maxDate, $siteId)
{
    $maxDate = DateTime::createFromFormat('Y-m-d', $maxDate);
    $minDate = clone $maxDate;
    $minDate->modify("-56 days");

    $thisYearMaxDateString = $maxDate->format("Y-m-d");
    $thisYearMinDateString = $minDate->format("Y-m-d");

    $maxDate->modify("-1 year");
    $minDate->modify("-1 year");

    $lastYearMaxDateString = $maxDate->format("Y-m-d");
    $lastYearMinDateString = $minDate->format("Y-m-d");

    $sql = "
        SELECT 
              display_name,
              '$thisYearMinDateString' AS this_year_min_date,
              '$thisYearMaxDateString' AS this_year_max_date,
              '$lastYearMinDateString' AS last_year_min_date,
              '$lastYearMaxDateString' AS last_year_max_date,
              value_this_year, 
              value_last_year, 
              `status`,
              value_this_year, 
              value_last_year, 
              value_last_year - value_this_year AS difference, 
              ((value_this_year - value_last_year)/value_last_year) * 100 AS percentage 
              FROM sites INNER JOIN
              (
                    SELECT
                        IFNULL(SUM(IF(BookingDate BETWEEN '$thisYearMinDateString' AND '$thisYearMaxDateString', NoOfJump * Rate, 0)), 0) AS `value_this_year`,
                        IFNULL(SUM(IF(BookingDate BETWEEN '$lastYearMinDateString' AND '$lastYearMaxDateString', NoOfJump * Rate, 0)), 0) AS `value_last_year`,
                        site_id
                    FROM customerregs1
                    WHERE
                        DeleteStatus = 0
                        AND Checked = 1
                    GROUP BY site_id
            ) AS income_table ON (sites.id = income_table.site_id AND site_id = $siteId)
        WHERE sites.status = 1 AND hidden = 0
        ";

    $results = queryForRows($sql)[0];
    return $results;
}

function ()
{

}

$today = new DateTime;
$date = new DateTime;

$output = [];
for ($i = 0; $i <= 29; $i++) {
    $output[$i] = incomeDeltaPercent($date->format("Y-m-d"), 1);
    $date->modify("-1 day");
}

?>


<html>
<body>
<table>
    <tr>
        <td>display_name</td>
        <td>this_year_min_date</td>
        <td>this_year_max_date</td>
        <td>last_year_min_date</td>
        <td>last_year_max_date</td>
        <td>value_this_year</td>
        <td>value_last_year</td>
        <td>difference</td>
        <td>percentage_change</td>
    </tr>

    <?
    foreach ($output as $row) {
        echo "

            <tr>
                <td>{$row['display_name']}</td>
                <td>{$row['this_year_min_date']}</td>
                <td>{$row['this_year_max_date']}</td>
                <td>{$row['last_year_min_date']}</td>
                <td>{$row['last_year_max_date']}</td>
                <td>{$row['value_this_year']}</td>
                <td>{$row['value_last_year']}</td>
                <td>{$row['difference']}</td>
                <td>{$row['percentage']}</td>
            </tr> 
            
        ";
    }
    ?>
</table>
</body>
</html>
