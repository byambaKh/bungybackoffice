<?php require_once('../includes/application_top.php'); ?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,user-scalable=no">
	<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="HandheldFriendly" content="true">
	<title>Dashboard</title>
  	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="style/style.css?v=0009"> 
	<!-- <link rel="stylesheet" href="style/responsive.css?v=0009">  -->   
    <link rel="stylesheet" href="style/modalstyle.css?v=0009">
    <link rel="stylesheet" media="all and (orientation:portrait)" href="style/responsive.css?v=0009">
    <link rel="stylesheet" media="all and (orientation:landscape)" href="style/responsive_l.css?v=0009">    
</head>
<body>
	<div id="container">
		<div id="left">
			<img id="logo" src="image/bungyjapan-logo.png">
			<div id="overview" class="cell">
				<h1 id="overview_title">Company<br/>Overview</h1><br>
                <a href="/../Analysis/charts.php"><div id="chartlink">Charts</div></a> <!--<a href="#analyticModal">Check</a>--><br>
                <!-- <div id="analyticModal" class="modalDialog">
                    <div class="modalBody">
                        <a href="#close" title="Close" class="close">X</a>
                        <h2>Analytics</h2>
                        <p>Content</p>
                        <div id="embed-api-auth-container"></div>
                        <div id="chart-container"></div>
                        <div id="view-selector-container"></div>
                    </div>
                </div>               
               -->
			</div>
		</div>
		<div id="right"></div>
	</div>
	<script type="text/javascript" src="javascript/gauge.js"></script>
	<script type="text/javascript" src="javascript/dashboard.js?v=0001"></script>
    <script type="text/javascript" src="javascript/analytics.js"></script>
	<script>
	var dashboard = new Dashboard({
		url: 'ajax.php',
		period: 1800
	});
	</script>
</body>
</html>
