/**
 * @param {Object} config
 * @constructor
 */

var Dashboard = function(config){
	this.data = null;
	this.event = document.createEvent('Event');
	this.event.dashboard = this;
	this.event.initEvent('data:updated', true, true);
	this.overviewEl = document.getElementById('overview');
	this.sitesEl = document.getElementById('right');

	this.removeOverview = function(){
		var values = document.querySelectorAll('p.overview_value');
		for (var i = 0; i < values.length; i++) {
			this.overviewEl.removeChild(values[i]);
		}
		return true;
	}

	this.drawOverview = function(){
		this.removeOverview();
		for (var i = 0; i < this.data.companyOverview.length; i++) {
			var value = this.data.companyOverview[i];
			var key = Object.keys(value)[0]
			var valueEl = this.createElement('p', 'overview_value', key + ' ' + value[key]);
			this.overviewEl.appendChild(valueEl);
		}
		return true;
	}

	this.drawSites = function(){
		var emptySites = 0;
		for (var i = 0; i < this.data.sites.length; i++ ){
			if (this.data.sites[i].name !== ""){
				var site = this.data.sites[i];
				var siteEl = document.querySelector('[data-name="' + site.name + '"]');
				if (siteEl){
					siteEl.dataset.state = site.state;
					siteEl.querySelector('div.site_weather').className = 'site_weather ' + site.weather;
					siteEl.querySelector('div.site_temp').innerHTML = site.temperature + '&#8451';
					this.updateDial('wind' + site.name, site.dials['wind'].values, site.dials['wind'].value, site.dials['wind'].color);
					this.updateDial('jumps' + site.name, site.dials['jumps'].values, site.dials['jumps'].value, site.dials['jumps'].color);
					this.updateDial('staff' + site.name, site.dials['staff'].values, site.dials['staff'].value, site.dials['staff'].color);
				}
				else {
					var siteEl = this.createElement('div', 'site cell', null, {name: site.name, state: site.state});
					var closedEl = this.createElement('div', 'site_media', 'MEDIA');
					var mediaEl = this.createElement('div', 'site_closed', 'CLOSED TODAY');
					var titleEl = this.createElement('h2', 'site_title', site.name);
					var weatherPl = this.createElement('div', 'site_weather_placeholder');
					var weatherEl = this.createElement('div', 'site_weather ' + site.weather);
					var tempEl = this.createElement('div', 'site_temp', site.temperature + '&#8451');
					var gaugesPl = this.createElement('div', 'site_gauges_placeholder');
					var windEl = this.createCanvas('wind' + site.name, 100, 100, 'wind_dial');
					var jumpsEl = this.createCanvas('jumps' + site.name, 230, 230, 'jumps_dial');
					var staffEl = this.createCanvas('staff' + site.name, 100, 100, 'staff_dial');

                    siteEl.appendChild(closedEl);
					siteEl.appendChild(mediaEl);
					siteEl.appendChild(titleEl);
					weatherPl.appendChild(weatherEl);
					weatherPl.appendChild(tempEl);
					siteEl.appendChild(weatherPl);
					this.sitesEl.appendChild(siteEl);
					gaugesPl.appendChild(windEl);
					gaugesPl.appendChild(jumpsEl);
					gaugesPl.appendChild(staffEl);
					siteEl.appendChild(gaugesPl);
					var windDial = this.createDial(this.smallDial, windEl, 'Wind', site.dials['wind'].values, site.dials['wind'].value, site.dials['wind'].color);
					var jumpsDial = this.createDial(this.bigDial, jumpsEl, 'Jumps', site.dials['jumps'].values, site.dials['jumps'].value, site.dials['jumps'].color);
					var staffDial = this.createDial(this.smallDial, staffEl, 'Staff', site.dials['staff'].values, site.dials['staff'].value, site.dials['staff'].color);
					windDial.draw();
					jumpsDial.draw();
					staffDial.draw();
				}
			}
			else {
				emptySites++;
			}
		}
		var emptySitesEls = document.querySelectorAll('div.coming_soon');
		if (emptySitesEls.length != emptySites){
			if (emptySitesEls.length > emptySites){
				for (var i = 0; i < emptySitesEls.length - emptySites; i++ ){
					this.sitesEl.removeChild(emptySitesEls[i]);
				}
			}
			else {
				for (var i = 0; i < emptySites - emptySitesEls.length; i++ ){
					this.sitesEl.appendChild(this.createElement('div', 'coming_soon cell site', 'COMING SOON'));
				}
			}
		}
		return true;
	}

	this.createElement = function(tagName, className, innerHTML, data) {
		var el = document.createElement(tagName);
		if (className) el.className = className;
		if (innerHTML) el.innerHTML = innerHTML;
		if (data) {
			var keys = Object.keys(data);
			for (var i = 0; i < keys.length; i++ ){
				el.dataset[keys[i]] = data[keys[i]];
			}
		}
		return el;
	}

	this.createCanvas = function(id, width, height, className) {
		var el = document.createElement('canvas');
		el.id = id;
		el.width = width;
		el.height = height;
		if (className) el.className = className;
		return el;
	}

	this.createDial = function(config, el, text, values, value, color){
		config.renderTo = el;
		config.units = text;
		config.minValue = values[0];
		config.maxValue = values[values.length - 1];
		config.colors.circle = {innerEnd: color};
		config.majorTicks = values;
		var gauge = new Gauge(config);
		gauge.setValue(config.maxValue * parseFloat(value) / 100);
		return gauge;
	}

	this.updateDial = function(id, values, value, color) {
		var dial = Gauge.Collection.get(id);
		var maxValue = values[values.length - 1];
		dial.updateConfig({
			minValue: values[0],
			maxValue: maxValue,
			majorTicks: values,
			colors: {
				circle: {
					innerEnd: color
				}
			},
		});
		dial.setValue(maxValue * parseFloat(value) / 100);
		return true;
	}

	this.getData = function(){
		var _this = this;
		var req = new XMLHttpRequest();
		req.open('GET', config.url, true);
		req.onreadystatechange = function(evt){
			if (req.readyState === 4){
				if (req.status === 200){
					_this.data = JSON.parse(req.responseText);
					document.dispatchEvent(_this.event);
				}
				else {
					console.error('Could not load url with data ' + req.status);
				}
			}
		}
		req.send(null);
	}
	this.getData();
	document.addEventListener('data:updated', function(e){
		e.dashboard.drawSites();
		e.dashboard.drawOverview();
	}), false;
	this.updateInterval = window.setInterval(this.getData.bind(this), config.period * 1000);
	this.smallDial = {
		width: 100,
		height: 100,
		numbersFont: 'bold 12px Arial',
		title: false,
		valueBox: {visible: false},
		valueText: {visible: false},
		highlights: [],
		minorTicks: 0,
		strokeTicks: false,
		glow: false,
		circles: {
			outerVisible: false,
			middleVisible: false
		},
		colors: {
			units: '#040404',
			majorTicks: '#9E9E9E',
			needle: {
				start: '#007DF8',
				end: '#007DF8'
			}
		},
		needle: {
			type: 'line',
			circle: {
				size: 0
			}
		}
	},

	this.bigDial = {
		width: 230,
		height: 230,
		numbersFont: 'bold 15px Arial',
		title: false,
		valueBox: {visible: false},
		valueText: {visible: false},
		highlights: [
            { from: 0, to: 20, color: '#FD0E04' },
			{ from: 20, to: 40, color: '#FEFF00' },
			{ from: 40, to: 90, color: '#01FF06' },
			{ from: 90, to: 160, color: '#7E81FD' }
		],
		minorTicks: 0,
		strokeTicks: false,
		glow: false,
		circles: {
			outerVisible: false,
			middleVisible: false
		},
		colors: {
			units: '#040404',
			majorTicks: '#9E9E9E',
			needle: {
				start: '#007DF8',
				end: '#007DF8'
			}
		},
		needle: {
			type: 'line',
			circle: {
				size: 0
			}
		}
	}
}

window['Dashboard'] = Dashboard;
