<?php 
require_once("pageHelpers.php");
echo Html::head("Bungy Japan - Template Page");
?>

<div class="headerBar">
	<h1><a href="#">&lt;</a>  Ryujin - Timetable <small>(Today)</small></h1>
</div>
<div class="headerUnderline">
 
</div>

<br>

<?php echo Form::backLinkButton("http://google.com"); ?>

<?php
echo Form::beginButtonGroup();
echo Form::actionLinkButton("Make", "http://google.com");
echo Form::actionLinkButton("Change", "http://google.com");
echo Form::actionLinkButton("Cancel", "http://google.com");
echo Form::endButtonGroup();

echo "&nbsp;";

echo Form::beginButtonGroup();
echo Form::actionLinkButton("Daily View", "http://google.com");
echo Form::actionLinkButton("History", "http://google.com");
echo Form::endButtonGroup();
?>

<br>
<br>

<table class="pageTable" style="width:250px">
	<tr class="timeTable" >
        	<th width="20%">Time</th><th width="80%">Jumps Available</th>
	<tr>

	<tr class="odd" >  <td><a href="#">07:00</a></td><td><a href="#">3</a></td> </tr>
	<tr class="even"> <td><a href="#">07:00</a></td><td><a href="#">3</a></td> </tr>
	<tr class="odd" >  <td><a href="#">08:00</a></td><td><a href="#">3</a></td> </tr>
	<tr class="even"> <td><a href="#">09:00</a></td><td><a href="#">3</a></td> </tr>
	<tr class="odd" >  <td><a href="#">10:00</a></td><td><a href="#">3</a></td> </tr>
	<tr class="even"> <td><a href="#">11:00</a></td><td><a href="#">3</a></td> </tr>
	<tr class="odd" >  <td><a href="#">12:00</a></td><td><a href="#">3</a></td> </tr>
	<tr class="even rowDanger"> <td><a href="#">13:00</a></td><td><a href="#">-1</a></td> </tr>
	<tr class="odd rowDanger">  <td><a href="#">14:00</a></td><td><a href="#">-2</a></td> </tr>
	<tr class="even"> <td><a href="#">15:00</a></td><td><a href="#">3</a></td> </tr>
	<tr class="odd" >  <td><a href="#">16:00</a></td><td><a href="#">3</a></td> </tr>
	<tr class="even"> <td><a href="#">17:00</a></td><td><a href="#">3</a></td> </tr>
	<tr class="even"> <td class="blackCell" colspan = 2 >Booked: 61</td> </tr>

</table>

<br>
	<button type="button" class="btn btn-success">Submit</button>
	<button type="button" class="btn btn-danger">Delete </button>

<br>
<br>


<?php
$dropDownItems = array(
	0 => array('value' => '1', 'title' => 'John'),
	1 => array('value' => '2', 'title' => 'James'),
	2 => array('value' => '3', 'title' => 'Jack'),
	3 => array('value' => '4', 'title' => 'Jim'),
	4 => array('value' => '5', 'title' => 'Joe')
);

echo Form::dropDown($dropDownItems);
?>

<form action="">
	<input type="checkbox" name="vehicle" value="Bike">I have a bike<br>
	<input type="checkbox" name="vehicle" value="Car">I have a car 
</form>

<form action="">
<?php
	echo Form::radioButton("gender", "male", "Male");
	$radioButtonsArray = array(
        	0 => array('name' => 'car', 'value'=> '1', 'title'=> 'Ferrari'),
        	1 => array('name' => 'car', 'value'=> '2', 'title'=> 'Honda'),
        	2 => array('name' => 'car', 'value'=> '3', 'title'=> 'Lotus'),
        	3 => array('name' => 'car', 'value'=> '5', 'title'=> 'Lexus'),
        	4 => array('name' => 'car', 'value'=> '6', 'title'=> 'Mustang', 'checked'),
        	5 => array('name' => 'car', 'value'=> '7', 'title'=> 'McLaren')
	);
	echo Form::radioButtons($radioButtonsArray);

	$checkBoxesArray = array(
        	0 => array('name' => 'car', 'value'=> '1', 'title'=> 'Ferrari'),
        	1 => array('name' => 'car', 'value'=> '2', 'title'=> 'Honda'),
        	2 => array('name' => 'car', 'value'=> '3', 'title'=> 'Lotus'),
        	3 => array('name' => 'car', 'value'=> '5', 'title'=> 'Lexus'),
        	4 => array('name' => 'car', 'value'=> '6', 'title'=> 'Mustang', 'checked'),
        	5 => array('name' => 'car', 'value'=> '7', 'title'=> 'McLaren')
	);
	echo Form::checkBoxes($checkBoxesArray);

?>
</form>

<?php
echo Form::beginButtonGroup();
echo Form::actionLinkButton("Daily View" ,"http://www.google.com",array('target'=>'')); 
echo Form::warningLinkButton("Reports" ,"http://www.google.com",array('target'=>'')); 
echo Form::alertLinkButton("Something Else" ,"http://www.google.com",array('target'=>'')); 
echo Form::submitButton("Submit This Form OKAY!!!"); 
echo Form::endButtonGroup();
?>

<div class="footerBar"><p>Policies &amp; Contitions</p><p>2014 Bungy Japan all rights reserverd [datetime]&nbsp;&nbsp;</p></div>
<? echo Html::foot(); ?>
