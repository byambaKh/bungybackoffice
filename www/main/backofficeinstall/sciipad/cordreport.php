<?
require_once("../includes/application_top.php");
require_once("functions.php");
function cordUsageTable($jumpCount, $colour, $startWeight, $endWeight, $maxRows = 30 ) {

	$output = "<table class='cord-report'>\n";
    $output .= "<tr><td colspan='2' class='cord-report-details'>$startWeight $endWeight</td></tr>\n";
	$output .= "<tr><td colspan='2' class='cord-report-details'>$colour</td></tr>\n";
	for($i = 1; $i <= 30; $i++) {

		$output .= "<tr class='cord-report-row'>";
		if($i <= $jumpCount) $output .= "<td class='cord-report-cell cord-report-checked'>$i</td>";
		else $output .= "<td class='cord-report-cell cord-report-non-checked'>$i</td>";

		$j = $i + 30;
		if(($j) <= $jumpCount) $output .= "<td class='cord-report-cell cord-report-checked'>$j</td>";
		else $output .= "<td class='cord-report-cell cord-report-non-checked'>$j</td>";
		$output .= "</tr> \n";
	}
	$output .= "<tr><td colspan='2' class='cord-report-footer-total'>$jumpCount</td></tr>\n";
	$output .= "</table>\n";

    $output .= " \n";
	return $output;
}
if(!isset($_GET['date'])) $date = date('Y-m-d');
else{
    $date = $_GET['date'];
};

function dateAndNonJumpers($date, $nonJumpers) {

    return "
            <div class='cord-report-header'>
            <div class='cord-report-date'>Date: $date</div>
            <div class='cord-report-non-jumpers'>Non Jumpers:</div>
            <div class='cord-report-non-jumpers-count'>$nonJumpers</div>
            </div>";
}

function totalJumpsFooter($jumpTotal) {
    return "
            <div class='cord-report-footer'>
            <div class='cord-report-jump-total'>TODAY’S JUMP TOTAL</div>
            <div class='cord-report-jump-total-count'>$jumpTotal</div>
            </div>";
}

function jumpsCordColours($date, $site_id)
{
    //get the jumps for the site as rows
        //left join waivers and on customerregs1 where site_id and booking date ...
    //foreach row
    $query = "
        SELECT CustomerRegID, bid, ws.id waiver_id, NoOfJump, weight_kg, BookingDate, nj.waiver_id as non_jump_waiver_id, ws.`firstname`, ws.lastname FROM customerregs1 cr
        INNER JOIN waivers ws ON (ws.bid = cr.CustomerRegID)
        LEFT JOIN `non_jumpers` nj ON (nj.`waiver_id` = ws.id)
        WHERE cr.site_id = $site_id AND cr.BookingDate = '$date' AND cr.deleteStatus = 0 AND cr.NoOfJump > 0;
    ";

    $rows = queryForRows($query);
    $jumpsCords = array(
        'yellow' => 0,
        'red' => 0,
        'blue' => 0,
        'black' => 0,
        'underweight' => 0,
        'overweight' => 0,
        'total' => 0,
        'nonJump' => 0
    );

    foreach($rows as $row)
    {
        if($row['non_jump_waiver_id'])
        {
            $jumpsCords['nonJump']++;
            continue;
        }
        $colour =  getCordColorForWeightKg($row['weight_kg'], $date, $site_id);
        $jumpsCords[$colour]++;
        $jumpsCords['total']++;
    }

    return $jumpsCords;
}

//get
function cordReport($date, $site_id)
{
    //convert the $date into the format YYYY/MM/DD
    //grab the latest daily report from the database
    //calculate the number of jumps for each cord
    //calculate the cord bounds
    //Add up the totals

    $cb = cordBounds($date, $site_id); //cordBounds
    $jc = jumpsCordColours($date, $site_id); //jumpCounts

    $output = dateAndNonJumpers($date, $jc['nonJump']); //TODO non jumpers
    $output .= "<br>";
    $output .= cordUsageTable($jc['yellow'],"Yellow", $cb["yellowMin"],   $cb["yellowMax"]);
    $output .= cordUsageTable($jc['red'],   "Red",    $cb['redMin'],      $cb['redMax']);
    $output .= cordUsageTable($jc['blue'],  "Blue",   $cb['blueMin'],     $cb['blueMax']);
    $output .= cordUsageTable($jc['black'], "Black",  $cb['blackMin'],    $cb['blackMax']);
    $output .= "<br>";
    $output .= "<br>";
    $output .= totalJumpsFooter($jc['total']);

    return $output;
}
?>
<!DOCTYPE HTML>
<html>
	<head>
        <meta charset="UTF-8">
        <title>Cord Usage Report</title>
		<style>
            body{
                font-family: "Arial", "Sans-Serif", "Helvetica";
                font-size: 12px;
            }
			.cord-report{
                display: inline;
				padding-right: 10px;
                border-collapse: collapse;
			}

            .cord-report-cell{
                text-align: center;
            }

            .cord-report-row{
            }

            .cord-report-checked{
                background-image: url(cord_report_check.png);
                background-repeat: no-repeat;
                border: 1px solid black;
                border-bottom-width: 0px;
                border-top-width: 0px;
                width: 20px;
                height: 15px;

            }
            .cord-report-non-checked{
                border: 1px solid black;
                border-top-width: 0px;
                border-bottom-width: 0px;
                width: 20px;
                height: 15px;

            }

            .cord-report-header{
                display: block;
            }

            .cord-report-details{
                text-align: center;
                border: 1px solid black;
            }

            .cord-report-footer-total{
                text-align: center;
                border: 2px solid black;
            }

            .cord-report-date{
                display: inline;

            }

            .cord-report-non-jumpers{
                display: inline;

            }

            .cord-report-non-jumpers-count{
                display: inline;
                border: solid black 2px;
            }

            .cord-report-footer{
                display: block;
            }
            .cord-report-jump-total{
                display: inline;
                border: solid black 2px;
                clear: left;

            }

            .cord-report-jump-total-count{
                display: inline;
                border: solid black 2px;
                clear: left;
            }

		</style>
	</head>
	<body>
		<?
        echo "<h1> CORD LOG </h1>";
        echo cordReport($date, CURRENT_SITE_ID);
		?>
	<button type="button" onclick="printPage('Ticket')">Print</button>

	<a href="index.php">Back</a>
	</body>
<script>

function printPage(ticketPrinter){
    jsPrintSetup.setPrinter('Ticket');
    jsPrintSetup.setOption('marginTop', 1);
    jsPrintSetup.setOption('marginBottom', 1);
    jsPrintSetup.setOption('marginLeft', 1);
    jsPrintSetup.setOption('marginRight', 1);

    jsPrintSetup.setOption('headerStrLeft', '');
    jsPrintSetup.setOption('headerStrCenter', '');
    jsPrintSetup.setOption('headerStrRight', '');
    jsPrintSetup.setOption('footerStrLeft', '');
    jsPrintSetup.setOption('footerStrCenter', '');
    jsPrintSetup.setOption('footerStrRight', '');

    jsPrintSetup.setSilentPrint(true);
    jsPrintSetup.print();
    jsPrintSetup.setSilentPrint(false);
}
</script>
</html>
