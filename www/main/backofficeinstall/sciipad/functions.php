<?php
$count = 1;
function getCompletedWaiversForSiteAndDate($siteId, $date) {
    mysql_query("SET @a:=0");
    $query = "
        SELECT * FROM (
                SELECT
                    id, bid, lastname, `time`, booking_type, firstname, email, weight_kg, @a:=@a+1 AS jump_count
                FROM 
                (
                    SELECT
                        id, w.bid, lastname, cr.BookingTime AS TIME, cr.BookingType AS booking_type, firstname, email, weight_kg
                    FROM customerregs1 AS cr
                    INNER JOIN waivers AS w ON(cr.`CustomerRegID` = w.`bid`)
                    WHERE 
                        BookingDate = '$date' AND cr.site_id =  $siteId
                    ORDER BY id ASC
                ) AS t
                ORDER BY id ASC
        ) AS q ORDER BY jump_count DESC;
	";

    $waivers = queryForRows($query);
    return $waivers;
}

function cordBounds($date = NULL, $site_id = NULL)
{
    $query = "SELECT * FROM daily_reports WHERE site_id = $site_id AND date = '$date';";
    $results = queryForRows($query);

    if(count($results) == 1)
        $cordBounds = $results[0];
    else
        (die("No cord bounds for the day."));

    //get the weight from the daily report
    $minWeight			 = 40;

    $bounds = array();

    $bounds['yellowMin'] = (int)$minWeight;
    $bounds['yellowMax'] = (int)$cordBounds['eq_cord_tw_yellow'];

    $bounds['redMin']	 = (int)$bounds['yellowMax'] + 1;
    $bounds['redMax']	 = (int)$cordBounds['eq_cord_tw_red'];

    $bounds['blueMin']   = (int)$bounds['redMax'] + 1;
    $bounds['blueMax']   = (int)$cordBounds['eq_cord_tw_blue'];

    $bounds['blackMin']  = (int)$bounds['blueMax'] + 1;
    $bounds['blackMax']  = (int)$cordBounds['eq_cord_tw_black'];

    return $bounds;
}

/**
 * @param      $weight the weight of the jumper, must be in kg as a floating point string or number. No letters should be appended
 * @param null $date the date of the jump, will default to today if not set
 * @param null $site_id the site of the jump, will default to this site if not set
 *
 * @return string
 */

function getCordColorForWeightKg($weight, $date = NULL, $site_id = NULL)
{
    if (!is_numeric($weight)) die('Non Numeric Weight Encountered. Cannot supply a cord color.');
//the boundaries are stored as integers so weight must be rounded up
    $weight = round($weight);

    $cordBounds = cordBounds($date, $site_id);

    $yellowMin = $cordBounds['yellowMin'];
    $yellowMax = $cordBounds['yellowMax'];

    $redMin = $cordBounds['redMin'];
    $redMax = $cordBounds['redMax'];

    $blueMin = $cordBounds['blueMin'];
    $blueMax = $cordBounds['blueMax'];

    $blackMin = $cordBounds['blackMin'];
    $blackMax = $cordBounds['blackMax'];

    if ($weight < $yellowMin) {
        $output = "underweight";

    } else if (($weight >= $yellowMin) && ($weight <= $yellowMax)) {
        $output = "yellow";

    } else if (($weight >= $redMin) && ($weight <= $redMax)) {
        $output = "red";

    } else if (($weight >= $blueMin) && ($weight <= $blueMax)) {
        $output = "blue";

    } else if (($weight >= $blackMin) && ($weight <= $blackMax)) {
        $output = "black";

    } else {
        $output = "overweight";
    }

    return "$output";
}

function displayCustomerWaivers($waivers)
{
    if(count($waivers) == 0) {
        return "There are no waivers for today.";
    }

    $output = "<form id='all-waivers' method='POST'>
<table id='wavier-table'>
<thead>
<tr>
    <th>Last Name</th>
    <th>First Name</th>
    <th>Weight (Kg)</th>
    <th colspan='3'>Print Actions</th>
</tr>

</thead>
";
    foreach($waivers AS $index => $waiver) {
        $jumpCode = $waiver['firstname'][0].$waiver['lastname'][0].$waiver['id'].CURRENT_SITE_ID;

        $output .= "
             <tr>
                <td>
                    <input type='hidden' class='id' name='waivers[$index][id]' value='{$waiver['id']}'>
                    <input type='hidden' class='time' name='waivers[$index][time]' value='{$waiver['time']}'>
                    <input type='hidden' class='photo_number' name='waivers[$index][photo_number]' value='{$waiver['photo_number']}'>
                    <input type='hidden' class='no_of_people' name='waivers[$index][no_of_people]' value='{$waiver['no_of_people']}'>
                    <input type='hidden' class='jump_count' name='waivers[$index][jump_count]' value='{$waiver['jump_count']}'>
                    <input type='hidden' class='booking_type' name='waivers[$index][booking_type]' value='{$waiver['booking_type']}'>
                    <input type='hidden' class='jump_code' name='waivers[$index][jump_code]' value='{$jumpCode}'>
                    <input type='text' class='lastname' name='waivers[$index][lastname]' value='{$waiver['lastname']}'>
                </td>
                <td><input type='text' class='firstname' name='waivers[$index][firstname]' value='{$waiver['firstname']}'></td>
                <td><input type='text' class='weight_kg' name='waivers[$index][weight_kg]' value='{$waiver['weight_kg']}'></td>
                <td><button data-id='{$waiver['id']}' type='button' class='both-button'>Both</button></td>
                <td><button data-id='{$waiver['id']}' type='button' class='ticket-button'>Ticket</button></td>
                <td><button data-id='{$waiver['id']}' type='button' class='certificate-button'>Certificate</button></td>
			</tr>
		";
    }

    $output .= "</table></form>";
    return $output;
}

function updateWaivers($waivers)
{
    foreach($waivers AS $waiver) {
        $id = $waiver['id'];

        $info['weight_kg'] = $waiver['weight_kg'];
        $info['firstname'] = $waiver['firstname'];
        $info['lastname']  = $waiver['lastname'];

        db_perform('waivers', $info, 'update', "id = $id");
    }
}

?>
