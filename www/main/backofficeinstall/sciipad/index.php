<?
require_once('../includes/application_top.php');
require_once('functions.php');

/*
This file is to present a list of waivers that have been completed and allow the sci staff print their certificate and their ticket
*/
function handlePOST()
{
    if (isset($_POST['waivers'])) {
        updateWaivers($_POST['waivers']);
    }
}

handlePOST();
$date = date("Y-m-d");
//$date = '2016-08-15';
$waivers = getCompletedWaiversForSiteAndDate(CURRENT_SITE_ID, $date);
$content = displayCustomerWaivers($waivers);
$title = "Completed Waivers for $date";

$ticketFrameUrl = "../selfCheckIn/ticketLayout.php?time=$time&weight=$weight&bookingType=$bookingType&photoNumber=$photoNumber&jumpCount=$jumpCount&groupOfNumber=$groupOfNumber&firstName=$firstName&lastName=$lastName&cordColor=$cordColor&printer=$certificatePrinter&host=$host&paid=$paid";

?>
<!DOCTYPE html>
<html>
<head>
    <title><?= $title ?></title>
    <meta charset="UTF-8">
    <!--<script type="text/javascript" src="/js/jquery/2.1.4/jquery-2.1.4.js"></script>-->
    <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.4/jquery.js"></script>
    <script type="text/javascript" src="js/print.js"></script>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="/css/bootstrap/3.3.6/css/bootstrap.css">
    <script>
        var jumpDate = '<?=$date?>';

        function printCertificate(waiverRow) {
            var firstname = waiverRow.find('.firstname').val();
            var lastname = waiverRow.find('.lastname').val();
            var fullName = firstname + '%20' + lastname;
            var jumpCode = waiverRow.find('.jump_code').val();

            var certificateFrameUrl =
                "certificate.php?fullName=" + fullName
                + "&date=" + jumpDate
                + "&printer=Certificate&jumpCode=" + jumpCode;

            //console.log(certificateFrameUrl);
            var hiddenFrame = $('#hidden-frame');
            hiddenFrame.attr('src', certificateFrameUrl);
            console.log(hiddenFrame.attr('src'));
            return certificateFrameUrl;
        }

        function printTicket(waiverRow) {
            var firstname = waiverRow.find('.firstname').val();
            var lastname = waiverRow.find('.lastname').val();
            var weight_kg = waiverRow.find('.weight_kg').val();
            var bookingType = waiverRow.find('.booking_type').val();
            var time = waiverRow.find('.time').val();
            var groupOfNumber = waiverRow.find('.no_of_people').val();
            var photoNumber = waiverRow.find('.photo_number').val();

            var jumpCount = waiverRow.find('.jump_count').val();

            var ticketFrameUrl = "ticketLayout.php?time=" + time +
                "&weight=" + weight_kg + /**/
                "&bookingType=" + bookingType + /**/
                "&photoNumber=" + photoNumber + /**/
                "&jumpCount=" + jumpCount + /**/
                "&groupOfNumber=" + groupOfNumber + /**/
                "&firstName=" + firstname + /**/
                "&lastName=" + lastname + /**/
                "&weight=" + weight_kg + /**/
                "&printer=Ticket" + /**/
                "&host=host" +
                "&paid=paid";

            var hiddenFrame = $('#hidden-frame');
            hiddenFrame.attr('src', ticketFrameUrl);
            console.log(hiddenFrame.attr('src'));
            return ticketFrameUrl;
        }

        $('document').ready(function () {
            var form = $('input');
            //var wto;
            $('.certificate-button').click(function () {
                var waiverRow = $(this).parent().parent();

                var certificateFrameUrl = printCertificate(waiverRow);
                return certificateFrameUrl;
            });

            $('.ticket-button').click(function () {
                var waiverRow = $(this).parent().parent();

                var ticketFrameUrl = printTicket(waiverRow);
                return ticketFrameUrl;
            });

            $('.both-button').click(function () {
                console.log('print_both');
                var waiverRow = $(this).parent().parent();
                var ticketFrameUrl = printTicket(waiverRow);

                setTimeout(function () {
                    //do second print
                    //var waiverRow = $(this).parent().parent();
                    var certificateFrameUrl = printCertificate(waiverRow);

                }, 5000);
            });

            form.change(function () {
                var formSerialized = form.serialize();
                //clearTimeout(wto);
                //wto = setTimeout(function () {
                // do stuff when user has been idle for 1 second
                var inputs = $('button');
                //disable the buttons while saving
                inputs.each(function () {
                        $(this).prop('disabled', 'disabled')
                    }
                );

                $.ajax({
                        data: formSerialized,
                        dataType: 'json',
                        url: 'ajax.php?action=save-waivers',
                        type: 'POST',
                        //async: false,
                        //cache: false,
                        success: function (data) {
                            //re-enable buttons
                            inputs.each(function () {
                                    $(this).prop('disabled', '')
                                }
                            )
                        }
                    }
                );
                //}, 1000);
            });
        });

    </script>
</head>
<body>
<div id="content">
    <h1><img src="images/background.png" width="208" height="120"> <?= ucfirst($subdomain) ?> Waivers <?= $date ?></h1>
    <br>
    If the waiver you are looking for is not showing, please refresh the page. Newer waivers will appear at the top.
    <br>
    <a href="cordreport.php?date=<?=$date?>">View todays' Cord Log</a>
    <br>
    <br>
    <?= $content ?>
</div>
<iframe style="display:none" name="" id='hidden-frame' src="#"></iframe>
</body>
</html>
