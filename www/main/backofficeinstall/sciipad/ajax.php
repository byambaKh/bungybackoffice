<?
require_once('../includes/application_top.php');
require_once('functions.php');

if($_GET['action'] == 'save-waivers') {
    if(isset($_POST['waivers'])) updateWaivers($_POST['waivers']);
}
$result['success'] = true;

header("Content-type: application/json; encoding: utf-8");
echo json_encode($result);
