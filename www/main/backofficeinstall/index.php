<?php

include "includes/application_top.php";
include "includes/mobile.php";

if (SYSTEM_SUBDOMAIN_REAL == 'MAIN') {
    include 'index_main.php';
    die();
}
if (SYSTEM_SUBDOMAIN_REAL == 'ACCOUNTANT') {
    Header('Location: /Accountant/index.php');
    die();
}
?>
<!DOCTYPE html>
<html>
<head>
    <?php include 'includes/header_tags.php'; ?>
    <title><?php echo SYSTEM_SUBDOMAIN; ?> System </title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">

    <?php include 'includes/head_scripts.php'; ?>
    <script>
        function procLogout() {
            document.location = "?logout=true";
            return true;
        }
        function show_invoice() {
            if ($("#invoice-table").is(":hidden")) {
                $("#invoice-table").slideDown('slow');
            } else {
                $("#invoice-table").slideUp('slow');
            }
            ;
        }
        function procMonthInvoice(month) {
            var d = new Date();
            document.location = "/AgentInvoice/agent_invoice.php?date=" + d.getFullYear() + '-' + month;
        }
        function payroll_password() {
            $('#pass-dialog').html('Please enter password: ').append($('<input type=password name=password>'));
            $('#pass-dialog').dialog({
                buttons: [
                    {
                        text: "Cancel",
                        width: 200,
                        height: 40,
                        click: function () {
                            $(this).dialog("close");
                        }
                    },
                    {
                        text: "OK",
                        width: 100,
                        height: 40,
                        click: function () {
                            $.ajax({
                                url: 'includes/ajax_helper.php?action=check_password',
                                method: 'POST',
                                dataType: 'json',
                                data: $(this).children('input'),
                                context: $(this)
                            }).done(function (data) {
                                if (data['result'] == 'success') {
                                    document.location = '/Bookkeeping/payroll.php';
                                } else {
                                    alert('Wrong password entered');
                                };
                            });
                        }
                    }
                ],
                dialogClass: "info",
                draggable: false,
                modal: true,
                position: {my: "center", at: "center", of: window},
                resizable: false,
                minWidth: 400,
                maxWidth: 400,
                title: "Password required"
            });

        }
        function waiver_password() {
            $('#pass-dialog').html('Please enter password: ').append($('<input type=password name=password>'));
            $('#pass-dialog').dialog({
                buttons: [
                    {
                        text: "Cancel",
                        width: 200,
                        height: 40,
                        click: function () {
                            $(this).dialog("close");
                        }
                    },
                    {
                        text: "OK",
                        width: 100,
                        height: 40,
                        click: function () {
                            $.ajax({
                                url: 'includes/ajax_helper.php?action=check_password_waiver',
                                method: 'POST',
                                dataType: 'json',
                                data: $(this).children('input'),
                                context: $(this)
                            }).done(function (data) {
                                if (data['result'] == 'success') {
                                    document.location = '/Waiver/download.php';
                                } else {
                                    alert('Wrong password entered');
                                };
                            });
                        }
                    }
                ],
                dialogClass: "info",
                draggable: false,
                modal: true,
                position: {my: "center", at: "center", of: window},
                resizable: false,
                minWidth: 400,
                maxWidth: 400,
                title: "Password required"
            });

        }
    </script>
    <style>
        h1 {
            color: white;
        }

        button:disabled div {
            color: silver;
        }

        div {
            text-align: center;
        }

        button {
            background-image: url('img/blank-button.png');
            width: 417px;
            height: 63px;
            border: none;
            background-position: center;
            margin: 1px;
            overflow: hidden;
            padding: 0px;
            display: block;
            margin-left: auto;
            margin-right: auto;
            cursor: pointer;
        }

        button div {
            width: 100%;
            height: 100%;
            font-size: 40px;
            color: white;
            font-weight: normal;
            font-family: Verdana;
            margin: 0px;
            padding: 0px;
            background: firebrick;
        }

        #container-table {
            width: 100%;
        }

        #container-table td {
            width: 100%;
            text-align: center;
        }

        #invoice-container {
            font-family: helvetica;
            font-size: 10pt;
        }

        #invoice-table {
            width: 417px;
            margin-left: auto;
            margin-right: auto;
            background-color: black;
            padding-top: 10px;
            padding-bottom: 10px;
            display: none;
        }

        #invoice-table-header {
            background-color: black;
            color: white;
            padding: 3px;
            font-weight: bold;
        }

        #invoice-table button {
            width: 60px;
            height: 30px;
            background-image: none;
            background-color: yellow !important;
            border: 2px solid red;
            display: inline;
            padding: 5px;
            font-weight: bold;
            margin: 2px;
        }
    </style>

    <meta name="viewport" content="width=device-width">
</head>
<body bgcolor=black>
<div>
<img src="/img/index.jpg">

<h1><?php echo (SYSTEM_SUBDOMAIN_REAL != 'AGENT') ? SYSTEM_SUBDOMAIN : 'メインメニュー'; ?></h1>
<?php
$buttons = array();
$user = new BJUser();

$roles = $user->hasRole('Staff');

switch (TRUE) {
    case ($user->hasRole('SysAdmin')):
        $buttons = array(
            'Reception' => '/Reception/',
            'Calendar Edit Adv' => '/Calendar/',
            'Roster Edit' => '/Roster/',
            'Operations Adv Edit' => '/Operations/',            
            'CordLog Files' => '/cordLogFiles/',
            'Equipment' => '/Equipment/',
            'Salary' => '',            
            'Report Issue Adv' => '/Issues/',
            'Bookkeeping Adv' => '/Bookkeeping/',
            'Inventory Adv' => '/Inventory/',
            'Training Adv' => '/Training/',
            'System Admin Adv' => '/Manage/',
            'Waiver Data' => '/Waiver/download.php',
            'View Invoice' => array('javascript:show_invoice();'),
            'Project System' => 'http://standardmove.com/projects/',
            'Online Storage' => 'http://standardmove.com/storage/',
            'Change Password' => '/User/change_password.php',
            'Change Pass Adv' => '/Manage/manageUsers.php',
            'Log Out' => '/?logout=true',
            'Back' => 'http://main.' . SYSTEM_DOMAIN,
        );
        break;

    case ($user->hasRole('General Manager')):
        $buttons = array(
            'Calendar Edit' => '/Calendar/',
            'Roster Edit' => '/Roster/',
            'Operations Adv Edit' => '/Operations/',
            'Equipment' => '/Equipment/',
            'Salary' => '',
            'Report Issue Adv' => '/Issues/',
            'Bookkeeping Adv' => '/Bookkeeping/',
            'Inventory Adv' => '/Inventory/',
            'Training Adv' => '/Training/',
            'System Admin' => '/Manage/',
            'Project System' => 'http://standardmove.com/projects/',
            'Online Storage' => 'http://standardmove.com/storage/',
            'Change Password' => '/User/change_password.php',
            'Change Pass Adv' => '/Manage/manageUsers.php',
            'Log Out' => '/?logout=true',
            'Back' => 'http://main.' . SYSTEM_DOMAIN,
        );
        break;
    case ($user->getUserName() == 'Hiroshi'): //
        $buttons = array(
            'Calendar' => '/Calendar/',
            'Roster Edit' => '/Roster/',
            'Operations Edit' => '/Operations/', 
            'CordLog Files' => '/cordLogFiles/',                       
            'Equipment' => '/Equipment/',
            'Salary' => '',
            'Report Issue' => '/Issues/',
            'Free Jump' => '/FreeJump/',
            'Bookkeeping' => '/Bookkeeping/',
            'Inventory Adv' => '/Inventory/',
            'Training' => '/Training/',
            'Duties' => '/Help/?topic=SMDuties',
            'Change Password' => '/User/change_password.php',
            'Log Out' => '/?logout=true',
            'Back' => 'http://main.' . SYSTEM_DOMAIN,
        );
        break;

    case ($user->getUserName() == 'Sachiko'): //
        $buttons = array(
            'Calendar' => '/Calendar/',
            'Roster' => '/Roster/',
            'Inventory Adv' => '/Inventory/',
            'Operations Edit' => '/Operations/',
            'Payroll Edit' => array('javascript:payroll_password();'),
            'Waiver Data' => array('javascript:waiver_password();'),
            'Salary' => '',
            'Report Issue' => '/Issues/',
            'Free Jump' => '/FreeJump/',
            'Training' => '/Training/',
            '-' => '',
            '-' => '',
            'Change Password' => '/User/change_password.php',
            'Log Out' => '/?logout=true',
            'Back' => 'http://main.' . SYSTEM_DOMAIN,
        );
        break;

    case ($user->getUserName() == 'Yu'): //
        $buttons = array(
            'Inventory' => '/Inventory/',
            'Calendar' => '/Calendar/',
            'Roster' => '/Roster/',
            'Operations' => '/Operations/',
            'Salary' => '',
            'Report Issue' => '/Issues/',
            'Free Jump' => '/FreeJump/',
            'Training' => '/Training/',
            '-' => '',
            '-' => '',
            'Change Password' => '/User/change_password.php',
            'Log Out' => '/?logout=true',
            'Back' => 'http://main.' . SYSTEM_DOMAIN,
        );
        break;

    case ($user->getUserName() == 'noriko'): //
        $buttons = array(
            'Calendar' => '/Calendar/',
            'Roster' => '/Roster/',
            'Operations Edit' => '/Operations/',
            'Payroll Edit' => array('javascript:payroll_password();'),
            'Waiver Data' => array('javascript:waiver_password();'),
            'Salary' => '',
            'Inventory Adv' => '/Inventory/',
            'Report Issue' => '/Issues/',
            'Free Jump' => '/FreeJump/',
            'Training' => '/Training/',
            '-' => '',
            '-' => '',
            'Change Password' => '/User/change_password.php',
            'Log Out' => '/?logout=true',
            'Back' => 'http://main.' . SYSTEM_DOMAIN,
        );
        break;

	case ($user->getUserName() == 'Toshiko'): //
        $buttons = array(
            'Calendar' => '/Calendar/',
            'Roster' => '/Roster/',
            'Operations' => '/Operations/',
            'Salary' => '',
            'Report Issue' => '/Issues/',
            'Free Jump' => '/FreeJump/',
            'Training' => '/Training/',
            'Open/Close' => '/OpenSlots/',
            'Change Password' => '/User/change_password.php',
            'Log Out' => '/?logout=true',
            'Back' => 'http://main.' . SYSTEM_DOMAIN,
        );
        break;
		
    case ($user->hasRole('Site Manager')):
        $buttons = array(
            'Calendar' => '/Calendar/',
            'Roster Edit' => '/Roster/',
            'Operations Edit' => '/Operations/',            
            'Equipment' => '/Equipment/',
            'Salary' => '',
            'Report Issue' => '/Issues/',
            'Free Jump' => '/FreeJump/',
            'Bookkeeping' => '/Bookkeeping/',
            'Inventory' => '/Inventory/',
            'Training' => '/Training/',
            'Duties' => '/Help/?topic=SMDuties',
            'Change Password' => '/User/change_password.php',
            'Log Out' => '/?logout=true',
            'Back' => 'http://main.' . SYSTEM_DOMAIN,
        );
        break;

    case ($user->hasRole('Staff +')):
        $buttons = array(
            'Calendar' => '/Calendar/',
            'Roster' => '/Roster/',
            'Operations Edit' => '/Operations/',
            'Payroll Edit' => array('javascript:payroll_password();'),
            'Waiver Data' => array('javascript:waiver_password();'),
            'Salary' => '',
            'Report Issue' => '/Issues/',
            'Free Jump' => '/FreeJump/',
            'Training' => '/Training/',
            '-' => '',
            '-' => '',
            'Change Password' => '/User/change_password.php',
            'Log Out' => '/?logout=true',
            'Back' => 'http://main.' . SYSTEM_DOMAIN,
        );
        break;

    case ($user->hasRole('Staff')):
        $buttons = array(
            'Calendar' => '/Calendar/',
            'Roster' => '/Roster/',
            'Operations' => '/Operations/',
            'Salary' => '',
            'Report Issue' => '/Issues/',
            'Free Jump' => '/FreeJump/',
            'Training' => '/Training/',
            '-' => '',
            '-' => '',
            'Change Password' => '/User/change_password.php',
            'Log Out' => '/?logout=true',
            'Back' => 'http://main.' . SYSTEM_DOMAIN,
        );
        break;

    case ($user->hasRole('Reception')):
        $buttons = array(
            'Make Booking' => '/Reception/makeBookingIE.php',
            'Change Booking' => '/Reception/changeBookingMain.php',
            'Cancel Booking' => '/Reception/cancelBookingMain.php',
            'Daily View' => '/Reception/dailyViewIE.php',
            'Calendar' => '/Calendar/',
            'CMS' => 'http://www.' . SYSTEM_DOMAIN . '/cms/',
            '-' => '',
            '-' => '',
            '-' => '',
            '-' => '',
            'Change Password' => '/User/change_password.php',
            'Log Out' => '/?logout=true',
            'Back' => 'http://main.' . SYSTEM_DOMAIN
        );
        break;

    case ($user->hasRole('P&V') || $user->hasRole('P&V Admin')):
        $buttons = array(
            'Sales Spreadsheet' => '/PhotoVideo/photoVideoTableView.php',
            'Calendar' => '/Calendar/',
            'Roster' => '/Roster/',
            'Operations' => '/Operations/',
            'Free Jump' => '/FreeJump/',
            '-' => '',
            '-' => '',
            '-' => '',
            '-' => '',
            '-' => '',
            '-' => '',
            'Change Password' => '/User/change_password.php',
            'Log Out' => '/?logout=true',
            'Back' => 'http://main.' . SYSTEM_DOMAIN,
        );
        break;

    case ($user->hasRole('P&V +')):
        $buttons = array(
            'Sales Spreadsheet' => '/PhotoVideo/photoVideoTableView.php',
            'Calendar' => '/Calendar/',
            'Roster' => '/Roster/',
            'Reception' => '/Reception/',
            'Operations' => '/Operations/',
            'Free Jump' => '/FreeJump/',
            '-' => '',
            '-' => '',
            '-' => '',
            '-' => '',
            '-' => '',
            '-' => '',
            'Change Password' => '/User/change_password.php',
            'Log Out' => '/?logout=true',
            'Back' => 'http://main.' . SYSTEM_DOMAIN,
        );
        break;

    case ($user->hasRole('Agent') || SYSTEM_SUBDOMAIN_REAL == 'AGENT'):
        if ($lang == 'en') {
            $buttons = array(
                'Make Booking' => '/Agent/agentBooking.php',
                'Check Bookings' => '/Agent/agentBookings.php',
                'View Invoice' => array('javascript:show_invoice();'),
                'Change Password' => '/User/change_password.php',
                'Log Out' => '/?logout=true',
                'Help' => '/Agent/help.php',
            );
        } else {
            $buttons = array(
                '予約作成 ' => '/Agent/agentBooking.php',
                '予約確認' => '/Agent/agentBookings.php',
                '月間別請求明細' => array('javascript:show_invoice();'),
                'パスワード変更' => '/User/change_password.php',
                'ログアウト' => '/?logout=true',
                'ヘルプ' => '/Agent/help.php',
            );
        }
        break;
    case ($user->hasRole('Financial')):
        $buttons = array(
            'Bookkeeping Adv'	=> '/Bookkeeping/',
            'Calendar Edit Adv'	=> '/Calendar/',
            'Roster'=> '/Roster/',
            'Analysis' => '/Analysis/',
            'Reception' => '/Reception/',
            'Inventory' => '/Inventory/',
            'Charts'	=> '/Analysis/charts.php',
            'Agent Invoices'	=> '/Manage/viewAgentInvoice.php.php',
            'Project System'	=> 'http://standardmove.com/projects/',
            'Online Storage'	=> 'http://standardmove.com/storage/',
            'Change Password' => '/User/change_password.php',
            'Log Out'	=> '/menu.php?logout=true',
            'Back'	=> '/',
        );
        break;

    case ($user->hasRole('Price Waterhouse Cooper')):
        $buttons = array(
            /*'Bookkeeping'=> '/Bookkeeping/',*/
            'Inventory'	=> '/Inventory/',
            'Change Password' => '/User/change_password.php',
            'Log Out'	=> '/menu.php?logout=true',
            'Back'	=> '/',
        );
        break;

    case ($user->hasRole('Public Relations')):
        $buttons = array(
            'PR' => '/Pr/',
            'Training' => '/Training/',
            'Log Out'	=> '/menu.php?logout=true',
            'Back'	=> '/',
        );
        break;
    case ($user->hasRole('ISO Auditor')):
        $buttons = array(
            'Training' => '/Training/',
            'Log Out'	=> '/menu.php?logout=true',
            'Back'	=> '/',
        );
        break;

    case ($user->hasRole('Construction')):
        $buttons = array(
            'Roster'=> '/Roster/',
            'Project System'	=> 'http://standardmove.com/projects/',
            'OwnCloud'	=> 'http://standardmove.com/storage/',
            'Change Password' => '/User/change_password.php',
            'Log Out'	=> '/menu.php?logout=true',
            'Back'	=> '/',
        );
        break;
        
    case ($user->hasRole('JM')):
        $buttons = array(
            'Operations'=> '/Operations/',
            'Log Out'	=> '/menu.php?logout=true',
            'Back'	=> '/',
        );
        break;
        
    case ($user->hasRole('ManualEdit')):
    $buttons = array(
        'Manuals'=> '/Training/manuals.php',
        'Log Out'	=> '/menu.php?logout=true',
        'Back'	=> '/',
    );
        break;

    default:
        $buttons = array(
            'Log Out'	=> '/menu.php?logout=true',
            'Back'	=> '/',
        );
        break;
}

$i = 0;
while (list($text, $url) = each($buttons)) {
    if (empty($text)) continue;
    $i++;
    ?>
    <button onClick="<?php
    if (!is_array($url)) {//if
        if ($text == "Project System" || $text == "Online Storage") echo "window.open('$url','newWindow', 'height=600, width=900, menubars=yes, scrollbars=yes');";
        else echo "document.location = '$url';";
    } else {
        echo $url[0];
    } ?>"

        <?php echo (empty($url)) ? ' disabled' : ''; ?>>
        <div>

            <?php echo $text; ?>
        </div>
    </button>
    <?php
    if (is_array($url) && $url[0] == 'javascript:show_invoice();') {
        ?>
        <div id="invoice-container">
            <div id="invoice-table">
                <div id="invoice-table-header"><?php echo ($lang == 'en') ? 'CLICK ON RESPECTIVE MONTH TO GENERATE INVOICE' : '月を選択してください。'; ?></div>
                <br/>
                <?php
                for ($i = 1; $i <= 12; $i++) {
                    $t = mktime(0, 0, 0, $i, 15, date("Y"));
                    $m = strtoupper(date("M", $t));
                    if ($lang != 'en') {
                        $m = $i . ' 月';
                    }
                    ?>
                    <button name="<?=$m?>" id="<?=$m?>" value="<?=$m?>" onclick="procMonthInvoice('<?php printf("%02d", $i); ?>');"><?=$m?></button>
                    <?php
                    if ($i == 6) {
                        ?>
                        <br/>
                    <?php
                    }
                }
                ?>
            </div>
        </div>
    <?php
    }
}
?>
<br><br>
</div>
<div id="pass-dialog" style="display: none;"></div>
</body>
</html>
