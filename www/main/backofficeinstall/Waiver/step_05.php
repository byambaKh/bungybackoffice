<div id="step5" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">please enter the year you were born</span>
                <span class="lang_ja">生まれた年を西暦でご入力ください。</span>
            </h1>
        </div>
    </div>
    <div id="content-container">
        <div id="photo-number"><input name="born_year"></div>
    </div>
    <div id="photo-keyboard-container">
        <div class="photo-keyboard">
            <div class="keyboard-button">
                <button>1</button>
            </div>
            <div class="keyboard-button">
                <button>2</button>
            </div>
            <div class="keyboard-button">
                <button>3</button>
            </div>
            <div class="keyboard-button">
                <button>4</button>
            </div>
            <div class="keyboard-button">
                <button>5</button>
            </div>
        </div>
        <div class="photo-keyboard">
            <div class="keyboard-button">
                <button>6</button>
            </div>
            <div class="keyboard-button">
                <button>7</button>
            </div>
            <div class="keyboard-button">
                <button>8</button>
            </div>
            <div class="keyboard-button">
                <button>9</button>
            </div>
            <div class="keyboard-button">
                <button>0</button>
            </div>
        </div>
        <div id="enter-del-container">
            <div id="del">
                <button><span class="lang_en">DELETE</span><span class="lang_ja">訂正</span></button>
            </div>
            <div id="enter">
                <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
            </div>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
