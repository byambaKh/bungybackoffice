<?php
require_once('includes/application_top.php');

//get current number of jumps on this day
function getDayJumpNumber($site_id) {
    //if we get the number of completed waivers for the current day that is the total number of jumps completed
    //at present
    $sql = "
    SELECT IFNULL(COUNT(ws.id), 0) AS JumpCount FROM customerregs1 AS cr
        INNER JOIN waivers AS ws ON (cr.CustomerRegId = ws.bid)
        WHERE cr.BookingDate = LEFT(NOW(),10)
        AND cr.site_id = $site_id
        AND cr.DeleteStatus <> 1
        AND `CancelFeeQTY` = 0
    GROUP BY cr.BookingDate;";
    $jumpCount = queryForRows($sql)[0];
    //todo the plus 1 needs to be moved out of this function as it should return the current photo count
    return ($jumpCount['JumpCount'] ? $jumpCount['JumpCount'] : 0);
}

function getYearJumpNumber($site_id) {
    echo $site_id;
    $sql = "
    SELECT COUNT(ws.id) AS JumpCount FROM customerregs1 AS cr
        INNER JOIN waivers AS ws ON (cr.CustomerRegId = ws.bid)
        WHERE LEFT(cr.BookingDate,4) = LEFT(NOW(),4)
        AND cr.site_id = $site_id
        AND cr.DeleteStatus <> 1
        AND `CancelFeeQTY` = 0;";
    $jumpCount = queryForRows($sql)[0];
    var_dump ($jumpCount);
    return ($jumpCount['JumpCount'] ? $jumpCount['JumpCount']  : 0);
}

//get the jump code for the certificate
function getJumpCode($bookingData, $siteId) {
    //BookingInitials
    $initials = $bookingData['firstname'][0].$bookingData['lastname'][0];
    //Jump Number For The Day
    $dayJumpNumber = getDayJumpNumber($siteId);
    //Jump Number For The Year

    $yearJumpNumber = getYearJumpNumber($siteId);
    return "$initial$dayJumpNumber.$yearJumpNumber";
}
