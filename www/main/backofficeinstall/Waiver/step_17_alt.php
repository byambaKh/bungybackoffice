<div id="step17" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">Thank you. Registration complete!</span>
                <span class="lang_ja">登録が完了しました。</span>
            </h1>

            <h2>
                <span class="lang_en">Please give your name to the reception staff.</span>
                <span class="lang_ja">
                お客様の氏名をスタッフまで <br/> お申し付けください。
                </span>
            </h2>
        </div>
    </div>
    <div class="restart-container">
        <!--div>This form will restart in <span id="restart-in">5</span> seconds</div-->
        <div id="step17progress"></div>
    </div>
</div>
