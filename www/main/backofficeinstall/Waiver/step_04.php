<div id="step4" style="display: none;">
    <div class="header-container" style="margin-top: 10px; margin-bottom: 10px;">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">waiver of liability and indemnity agreement</span>
                <span class="lang_ja" style="font-size:45px;">バンジージャンプ確認書兼保険申込</span>
            </h1>
        </div>
    </div>
    <div id="content-container">
        <!--since I am going to rewrite this entire file I am leaving these styles here-->
        <style>
            .readable_text {
                font-family: "Helvetica";
                color: white;
            }

            #en_waiver_table tr td {
                padding-bottom: 10px;
                vertical-align: top;
                font-size: 20px;
                line-height: 17px;
                font-weight: 100;
            }

            .en_waiver_table_number {
                font-size: 13px;
                line-height: 4px;
            }

            #en_waiver_header p {
                font-size: 17px !important;
                line-height: 17px !important;
                font-weight: 900 !important;
            }

        </style>
        <span class="lang_en">
      <span id="en_waiver_header" class="lang_en en_waiver_header readable_text">
    <p>READ AND FULLY UNDERSTAND EACH PROVISION OF THIS AGREEMENT AND INDICATE YOUR ACKNOWLEDGEMENT BY TOUCHING "I AGREE".</p>
    <p>IN CONSIDERATION of the organizers allowing you (hereinafter referred to as "the participant") to utilize the facilities and equipment and to participate in bungy jumping and its associated activities, it is agreed that:</p>
</span>

<table id="en_waiver_table" class="readable_text">
    <tr>
        <td class="en_waiver_table_number">1.
        <td>I understand &amp; acknowledge that participating in bungy jumping has inherent risks, including but not limited to physical injury &amp; even death.</td>
    </tr>
    <tr>
        <td class="en_waiver_table_number">2.
        <td>
            I confirm that I understand and am in accordance with all applicable participation requirements. I assert that I have no injury or disease that would affect my ability to safely participate as such. I also agree to refrain from participating in bungy jumping when I am pregnant or under the influence of alcohol. I also understand and acknowledge that if I conceal an illness or injury, when participating in bungy jumping, the insurance policy as stipulated in article 3 shall not apply to me and that I shall be liable for my own acts (and omissions), accordingly.
            <br>
            <br>
            <b>*Customers who have low bone density, weak bones and/or osteoporosis will not be allowed to participate in bungy jump activities.
            There is a risk that you could break and/or injure your bones due to the strong forces involved in a bungy jump.
            If a claim is made that relates to any broken bones the customer must submit to a Bone Density or DXA (noninvasive) Test.</b>
        </td>
    </tr>
    <tr>
        <td class="en_waiver_table_number">3.
            <?php if ($site_id > 2): ?>
        <td> I understand the Organizers provides the following insurance cover for participating in bungy jumping: Death or residual disability: 10 million JPY.
            <br>I also understand and acknowledge that this insurance policy does not include the costs of medical care, transportation or property damage.
        </td>
        <?php else: ?>
            <td>I understand the Organizers provides the following insurance cover for participating in bungy jumping: 10 million JPY.
            <br>I also understand and acknowledge that this insurance policy does not include the costs of medical care, transportation or property damage.
        </td>
        <?php endif; ?>
    </tr>
    <tr>
        <td class="en_waiver_table_number">4.
        <td>I agree to observe and obey all posted rules, to follow any instructions or directions given by the Organizers through its staff, and to be fully aware of all safety matters. I also understand and acknowledge that if I do not observe and obey the said rules and instructions given by the Organizers, the Organizers has thBunge right to cancel my participation in bungy jumping, and that I shall be liable for my own actions (and omissions), accordingly.
        </td>
    </tr>
    <tr>
        <td class="en_waiver_table_number">5.
        <td>I intend my signature to be a complete and unconditional release of all liability for the Organizers and its staff due to act of god (including unpredicted natural phenomenon).</td>
    </tr>
    <tr>
        <td class="en_waiver_table_number">6.
        <td>I also understand and acknowledge that I shall be liable to pay the repair and/or replacement costs for any damage to the the Organizers’ property that is caused in whole or part by my intentional acts or omissions, or my failure to observe the rules and instructions given by the Organizers and its staff to any facilities or equipment operated and managed by the Organizers.</td>
    </tr>
    <tr>
        <td class="en_waiver_table_number">7.
        <td>I consent to medical care and transportation as the Organizers or medical professions may deem appropriate in order to obtain treatment in the event of my falling ill or becoming injured during my participation in the bungy jumping.</td>
    </tr>
    <tr>
        <td class="en_waiver_table_number">8.
        <td>I consent to the Organizers’ use of my image at no cost in photographs, motion pictures or recordings taken at any of the Organizers’ bungy jumping sites for use in the Organizers’ advertising, marketing or promotion throughout the world.</td>
    </tr>
    <tr>
        <td class="en_waiver_table_number">9.
        <td>I agree that the Organizers’ reserves the right to cancel bungy jumping in the event of inclement weather, natural or man-made disasters, acts of god or for any other reason that the Organizers deem, at its discretion, necessary for safety & security of participants or due to difficulty of operating bungy jumping.</td>
    </tr>
    <tr>
        <td class="en_waiver_table_number">10.
        <td>In the event that purchased photo packs cannot be provided, only a full refund of the photo pack will be given.</td>
    </tr>
    <tr>
        <td class="en_waiver_table_number">11.
        <td>If I am under 20 years old I agree that I have provided Bungy Japan staff with a LETTER OF CONSENT FOR WAIVER OF LIABILITY signed by my parent/guardian including their relationship to me.</td>
    </tr>
</table>
</span>

        <span class="lang_ja">
<?php if ($site_id > 2): ?>
    <p style="font-weight:bold; color: white; font-size: 20px;">
    私は、スタンダード・ムーブ株式会社（以下「主催者」といいます）が主催・催行するバンジージャンプを行うにあたり、この確認書に記載されている条項（１－１１）をよく読み、保険の内容やバンジージャンプの危険性、主催者の権利などを理解し、了承し、同意した上で、自筆の署名をいたします。
</p>
<?php else: ?>
    <p style="font-weight:bold; color: white; font-size: 20px;">
    私は、みなかみ町観光協会が主催し、スタンダード・ムーブ株式会社（以下「主催者」といいます）が主催・催行するバンジージャンプを行うにあたり、この確認書に記載されている条項（１－１１）をよく読み、保険の内容やバンジージャンプの危険性、主催者の権利などを理解し、了承し、同意した上で、自筆の署名をいたします。
</p>
<?php endif; ?>

            <style>
    #ja_waiver_table tr td {
        padding-bottom: 10px;
        vertical-align: top;
        font-size: 20px;
        line-height: 22px;
        font-weight: 100;
        color: white;
        font-weight: 100;
    }

    .ja_waiver_table_number {
        font-size: 17px;
        line-height: 8px;
        font-weight: 100;
    }
</style>

<table id="ja_waiver_table">
    <tr>
        <td class="ja_waiver_table_number">１.</td>
            <td>私が参加する<nobr>バンジージャンプ</nobr><nobr>は</nobr>、<nobr>その性質上、</nobr><nobr>死傷する危険</nobr>があることを<nobr>十分に</nobr><nobr>認識しています。</nobr>
        </td>
    </tr>
    <tr>
        <td class="ja_waiver_table_number">２.</td>
        <td>私は、主催者が定めた参加条件に<nobr>反しておりません。</nobr>私は、<nobr>バンジージャンプが</nobr><br><nobr>影響</nobr>を及ぼす可能性のある症状や怪我を<nobr>有しておりません</nobr>。私は、<nobr>飲酒、</nobr><nobr>酒気帯び、</nobr><nobr>妊娠もしておりません。</nobr>私は、私が<nobr>上記</nobr>の<nobr>症状や怪我を隠ぺいして</nobr><nobr>バンジージャンプ</nobr><nobr>を行った</nobr><nobr>場合には、</nobr><nobr>応分の責任</nobr>を負<nobr>わねばならず、</nobr><nobr>保険は適用されない</nobr>ことを<nobr>理解しています。</nobr>
        <br>
        <br>
            <b>-骨密度が低い方、骨が弱い・もろい方及び骨粗鬆症の方は参加できません。強い圧力が加わることで、骨折及び骨に関する怪我を負う可能性があります。
                骨折及び骨に関する怪我について保険請求をされる場合は骨密度検査もしくは DXA 検査 (非侵襲的) を受け、検査結果を提出しなければなりません。</b>
        </td>
    </tr>
    <tr>
        <td class="ja_waiver_table_number">３.</td>
        <?php if ($site_id > 2): ?>
            <td>私は、主催者が加入している保険が以下の内容（補償金額）であることを理解しています。
        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;死亡・後遺障害1,000万円<br>私は、保険には実際に支払った医療費や交通費等に
            <nobr>対する</nobr>
            <nobr>補償、</nobr>
            <nobr>また物損に対する</nobr>
            <nobr>補償がないことも</nobr>
            <nobr>理解しています。</nobr>
        </td>
        <?php else: ?>
            <td>私は、主催者が加入している保険が以下の内容（補償金額）であることを理解しています。
        <br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;死亡・後遺障害1,000万円<br>私は、保険には実際に支払った医療費や交通費等に
            <nobr>対する</nobr>
            <nobr>補償、</nobr>
            <nobr>また物損に対する</nobr>
            <nobr>補償がないことも</nobr>
            <nobr>理解しています。</nobr>
        </td>
        <?php endif; ?>

    </tr>
    <tr>
        <td class="ja_waiver_table_number">４.</td>
        <td>私は、主催者が設けた規則やそのスタッフの指示に従い、安全を心がけなければならないということを十分に理解しています。私は、主催者が設けた規則や指示を無視して行った行動に対しては、応分の責任を負わなければならず、主催者が、規則や指示に従わない参加者に対し、
            <nobr>バンジージャンプ</nobr>
            への
            <nobr>参加</nobr>
            を取り消す権利を有していることも、
            <nobr>理解しています。</nobr>
        </td>
    </tr>
    <tr>
        <td class="ja_waiver_table_number">５.</td>
        <td>私は、予期し得ぬ自然現象など、不可避的な事象によって引き起こされた事故については、主催者とそのスタッフに対して、
            <nobr>責任を問うことはしません。</nobr>
        </td>
    </tr>
    <tr>
        <td class="ja_waiver_table_number">６.</td>
        <td>私は、故意により、または主催者が設けた規則もしくは指示に反した行動により、主催者の管理する施設や備品などに損傷を与えた場合、その修理費を負担することを了承します。</td>
    </tr>
    <tr>
        <td class="ja_waiver_table_number">７.</td>
        <td>私は、バンジージャンプへ参加した結果、怪我や病気になった場合、主催者の判断により医療機関での治療を受け、
            <nobr>緊急</nobr>
            の
            <nobr>場合</nobr>
            には
            <nobr>主催者また</nobr>
            は
            <nobr>医師</nobr>
            が
            <nobr>応急処置</nobr>
            を施すことに同意いたします。
        </td>
    </tr>
    <tr>
        <td class="ja_waiver_table_number">８.</td>
        <td>私は、バンジージャンプへの参加にあたり撮影された私の写真や映像の肖像権を放棄し、それらを全世界において広告宣伝目的、
            <nobr>プロモーション</nobr>
            <nobr>活動</nobr>
            などで
            <nobr>使用する</nobr>
            <nobr>権利が主催者</nobr>
            に無償で
            <nobr>帰属する</nobr>
            <nobr>ことに</nobr>
            <nobr>
                <nobr>同意いたします。</nobr>
            </nobr>
        </td>
    </tr>
    <tr>
        <td class="ja_waiver_table_number">９.</td>
        <td>私は、天災、
            <nobr>天候不良等不可抗力の理由</nobr>
            <nobr>により、</nobr>
            <nobr>主催者</nobr>
            が
            <nobr>バンジージャンプ</nobr>
            の
            <nobr>実施もしく</nobr>
            は
            <nobr>運営に支障があり、</nobr>
            又は
            <nobr>安全</nobr>
            <nobr>確保に</nobr>
            <nobr>支障がある</nobr>
            と
            <nobr>判断した場合、</nobr>
            <nobr>バンジージャンプ</nobr>
            の
            <nobr>実施が中止となる</nobr>
            <nobr>ことに</nobr>
            <nobr>同意いたします。</nobr>
        </td>
    </tr>
    <tr>
        <td class="ja_waiver_table_number"><nobr>１０.</nobr></td>
        <td>
            <nobr>バンジージャパン</nobr>を<nobr>ご利用</nobr> に<nobr>なった</nobr> <nobr>当日</nobr>に<nobr>報告・並び</nobr>に<nobr>弊社内</nobr>の<nobr>事故報告書</nobr>の<nobr>作成が無い</nobr> <nobr>怪我</nobr> <nobr>について</nobr> <nobr>は、</nobr> <nobr>本保険</nobr> <nobr>による</nobr> <nobr>保険事故</nobr> <nobr>として</nobr>の<nobr>取り扱いが</nobr> <nobr>が</nobr> <nobr>出来ません。</nobr>
        </td>
    </tr>
   <tr>
        <td class="ja_waiver_table_number"><nobr>１１.</nobr></td>
        <td>
            もし20歳未満の場合は、ご両親／保護者 (貴方との関係を含む)の方に必ず「バンジージャンプ確認書兼保険申込 」に署名をしてもらい、バンジージャパンのスタッフへ提出することに同意します
        </td>
    </tr>
</table>
</span>
    </div>
    <div id="agree-exit-container">
        <div id="agree" style="padding-top: 10px;">
            <button>
                <span class="lang_en">I AGREE</span>
                <span class="lang_ja">同意する</span>
            </button>
        </div>
        <div id="exit" style="padding-top: 10px;">
            <button>
                <span class="lang_en">EXIT</span>
                <span class="lang_ja">同意しない</span>
            </button>
        </div>
    </div>
    <div class="back" style="top:1275px">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>