<div id="step12" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en"><span style="font-size: 80px;">please enter your</span> phone number</span>
                <span class="lang_ja">電話番号をご入力ください。</span>
            </h1>
        </div>
    </div>
    <div id="content-container">
        <div id="phone-number"><input name="phone_number"></div>
    </div>
    <div id="keyboard-container">
        <?php
        $keyboard = array(
            array('1', '2', '3', '4', '5'),
            array('6', '7', '8', '9', '0')
        );
        foreach ($keyboard as $line) {
            echo '<div class="keyboard-line">';
            foreach ($line as $letter) {
                echo "<div class=\"keyboard-button\"><button>" . strtoupper($letter) . "</button></div>";
            };
            echo "</div>";
        };
        ?>
        <div id="enter-del-container">
            <div id="del">
                <button><span class="lang_en">DELETE</span><span class="lang_ja">訂正</span></button>
            </div>
            <div id="enter">
                <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
            </div>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>