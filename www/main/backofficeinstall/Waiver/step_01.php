<div id="step1" style="display: none;">
    <div id="waiver-container">
        <div class="header-container"><img alt="Step 1: Language selection" src="img/wheader.png"></div>
        <div id="language-container">
            <div id="english-container" class="half">
                <button><img src="img/wenglish.png"></button>
            </div>
            <div id="japanese-container" class="half">
                <button><img src="img/wjapanese.png"></button>
            </div>
        </div>
    </div>
    <div id="step1-add-info">
        文字入力や各ボタンの選択が認識されるまで、<br>
        多少時間がかかります。ひとつずつ、ゆっくりめにご入力ください。
    </div>
</div>
