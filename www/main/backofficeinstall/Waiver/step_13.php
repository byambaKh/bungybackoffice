<div id="step13" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">Please enter your email address</span><span
                    class="lang_ja">メールアドレスをご入力ください。</span>
            </h1></div>
        <div id="email-note">
            <span class="lang_en">A customer survey will be sent to the email address provided <br/>and those that complete it will be entered in a monthly draw for 2 free jump tickets!</span>
            <span class="lang_ja">アンケートをメールにて送信しますので、ご協力をお願いいたします。<br/>
お答えいただいた方に毎月抽選で無料ジャンプ券2枚プレゼント！</span>
        </div>
    </div>
    <!--div id="rather-not">
      <button><span class="lang_en">No, thank you</span><span class="lang_ja">入力不要</span></button>
    </div-->
    <div id="email-container">
        <div id="email-input-container"><input name="email"></div>
    </div>
    <div id="keyboard-container">
        <?php
        $keyboard = array(
            array('1', '2', '3', '4', '5', '6', '7', '8', '9', '0'),
            array('q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'),
            array('a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', '-'),
            array('z', 'x', 'c', 'v', 'b', 'n', 'm', '.', '@', '_')
        );
        $i = 0;
        foreach ($keyboard as $line) {
            echo '<div class="keyboard-line' . $i . '">';
            foreach ($line as $letter) {
                echo "<div class=\"keyboard-button\"><button>" . strtoupper($letter) . "</button></div>";
            };
            echo "</div>";
            $i = '';
        };
        ?>
    </div>
    <div id="domains-container">
        <div class="domains-row">
            <div class="domains-cell first-cell">
                <button>.com</button>
            </div>
            <div class="domains-cell second-cell">
                <button>@i.softbank.jp</button>
            </div>
            <div class="domains-cell third-cell">
                <button>@yahoo.co.jp</button>
            </div>
        </div>
        <div class="domains-row">
            <div class="domains-cell first-cell">
                <button>.ne.jp</button>
            </div>
            <div class="domains-cell second-cell">
                <button>@gmail.com</button>
            </div>
            <div class="domains-cell third-cell">
                <button>@hotmail.ne.jp</button>
            </div>
        </div>
        <div class="domains-row">
            <div class="domains-cell first-cell">
                <button>.co.jp</button>
            </div>
            <div class="domains-cell second-cell">
                <button>@ezweb.ne.jp</button>
            </div>
            <div class="domains-cell third-cell">
                <button>@docomo.ne.jp</button>
            </div>
        </div>
    </div>
    <div id="enter-del-container">
        <div id="enter">
            <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
        </div>
        <div id="del">
            <button><span class="lang_en">DELETE</span><span class="lang_ja">訂正</span></button>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>