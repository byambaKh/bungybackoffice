<div id="step3" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">please enter your photo number</span>
                <span class="lang_ja">お写真又はビデオをご購入された方は左手首の番号をご入力ください。</span>
            </h1>

            <h2>
                <span class="lang_en">if you purchased them and have a number on your left wrist</span>
                <span class="lang_ja"></span>
            </h2>
        </div>
    </div>
    <div id="content-container">
        <div id="photo-example"><img src="img/step3_hand.png" alt="Hand example"/></div>
        <div id="photo-number"><input name="photo_number"></div>
        <div id="photo-dho">
            <button>
                <span class="lang_en">No photos purchased.</span>
                <span class="lang_ja">ご購入されない方はこちら</span>
            </button>
        </div>
    </div>
    <div id="photo-keyboard-container">
        <div class="photo-keyboard">
            <div class="keyboard-button">
                <button>1</button>
            </div>
            <div class="keyboard-button">
                <button>2</button>
            </div>
            <div class="keyboard-button">
                <button>3</button>
            </div>
            <div class="keyboard-button">
                <button>4</button>
            </div>
            <div class="keyboard-button">
                <button>5</button>
            </div>
        </div>
        <div class="photo-keyboard">
        <div class="keyboard-button">
                <button>6</button>
            </div>
            <div class="keyboard-button">
                <button>7</button>
            </div>
            <div class="keyboard-button">
                <button>8</button>
            </div>
            <div class="keyboard-button">
                <button>9</button>
            </div>
            <div class="keyboard-button">
                <button>0</button>
            </div>
        </div>
        <div id="enter-del-container">
            <div id="del">
                <button><span class="lang_en">DELETE</span><span class="lang_ja">訂正</span></button>
            </div>
            <div id="enter">
                <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
            </div>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>