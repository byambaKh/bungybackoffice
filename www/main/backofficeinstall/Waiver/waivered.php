<?php
  include "../includes/application_top.php";
	mysql_set_charset('utf8');

	$cDate = date("Y-m-d");

	$_SESSION['jumpDate'] = $_POST['jumpDate'];


function display_db_query($sql_select, $conn, $header_bool, $table_params) {
  $result_select = mysql_query($sql_select, $conn)
  or die("display_db_query:" . mysql_error());
  // find out the number of columns in result
  $column_count = mysql_num_fields($result_select)
  or die("display_db_query:" . mysql_error());  
  
  // Here the table attributes from the $table_params variable are added
  print("<TABLE $table_params align='center' width='400px'>");
  // optionally print a bold header at top of table
  if($header_bool) {
    
    print("<TR>");
    for($column_num = 0; $column_num < $column_count; $column_num++) {
      $field_name = mysql_field_name($result_select, $column_num);
      print("<TH bgcolor='#87CEFA'>$field_name</TH>");
    }
    print("</TR>");
  }
  //Check ROW Count
  $num_rows = mysql_num_rows($result_select);
  if($num_rows != 0){
     
                
	while($row = mysql_fetch_assoc($result_select)) {
		print("<TR ALIGN=CENTER VALIGN=TOP>");
			foreach ($row as $name => $value) {
				switch (TRUE) {
					case ($name == 'Signature'):
						echo "<TD valign='middle'><img src='$value' height='50'></TD>";
						break;
					default:
						echo "<TD valign='middle'><nobr>$value</nobr></TD>";
				};
			};
		print("</TR>");        
      }      
          
    }      
  print("</TABLE>");

}
function display_db_table($tablename, $conn, $header_bool, $table_params) {
  
  $chk = 1;
  if(is_null($_GET['jumpDate'])){
    $cDate = date("Y-m-d");
  } else {
	$cDate = $_GET['jumpDate'];
  };
    $sql_select_def = "SELECT 
				cr.RomajiName as 'Booking Name', 
				w.photo_number as 'Photo Number',
				w.birth_year as 'Birth Yr',
				w.birth_day as 'Birth Day',
				w.birth_month as 'Birth Mth',
				w.sex as 'SEX',
				w.lastname as 'Last Name',
				w.firstname as 'First Name',
				CONCAT(w.lastname, ' ', w.firstname) as 'Full Name',
				w.post_code as 'Post Code',
				w.phone_number as 'Contact Number',
				'' as 'Em Number',
				w.email as 'Email',
				w.prefecture as 'Address',
				cr.BookingDate as 'Booking Date', 
				cr.BookingTime as 'Jump Time', 
				cr.Rate as 'Rate', 
				cr.Agent as 'Agent', 
				cr.RateToPay as 'to Pay', 
				cr.noOfJump as 'Jumps', 
				cr.CollectPay as 'Collect', 
				cr.BookingType as 'Booking Type', 
				cr.ContactNo as 'Booking Phone', 
				cr.TransportMode as 'Transport Mode', 
				cr.Notes as 'Notes', 
				cr.CustomerRegID as 'Reg ID', 
				w.id as 'Waiver ID' " . (!isset($_GET['with_past'])?",
				w.sig_img as 'Signature'
				" : "").              
              " FROM customerregs1 cr " .
			  " JOIN waivers w on (cr.CustomerRegID = w.bid" . (!isset($_GET['with_past'])?" and w.ticketed = 0":'') . ") ".
              " WHERE 
					cr.site_id = '".CURRENT_SITE_ID."' 
				AND cr.bookingdate = '".$cDate."'".
              " and cr.Checked = '".$chk."'";

    //echo $sql_select_def;
    display_db_query($sql_select_def, $conn, $header_bool, $table_params);
  
}
?>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">

<?php include '../includes/head_scripts.php'; ?>
<script src="js/functions.js" type="text/javascript"></script>

<title>Waivers Data View</title>
<script type="text/javascript">

function OnSubmitForm(){
  if(document.getElementById('jumpDate')){
    document.dailyForm.submit();  
  }
}
function procBack(){
  if(document.getElementById('bck')){
    //document.dailyForm.action = "index.php";
    //document.dailyForm.submit();
    //return true;
  }
}

$(function() {
  $mydate1 = $('#jumpDate').datepicker({
    numberOfMonths: 1,    
    dateFormat: 'yy-mm-dd',
    altField: '#alternate',
    altFormat: 'yy-mm-dd',
    minDate: '0d'
  });
});

</script>
</head>
<body>
<?php //include '../includes/main_menu.php'; ?>
<br>
<form name="dailyForm" method="post">
  <table align="center"  width="400px"> 
    <tbody>    
      <tr>
        <td>
          <?php
            $table = "table1";
            display_db_table($table, $conn, TRUE, "border='1'");
          ?>
         
        </td>
      </tr>
      <!-- 
      <tr>
        <td colspan="4">
          <input type="button" name="bck" id="bck" value="BACK" style="background-color:#FFFF66;border-style:ridge;border-color:#FF0000;" onclick="procBack();">
        </td>
      </tr>
       -->
    </tbody>
  </table>
</form>  
</body>
</html>
