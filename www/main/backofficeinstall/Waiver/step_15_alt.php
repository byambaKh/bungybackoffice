<div id="step15" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">Thank You! Please return the iPad to a staff member.</span>
                <span class="lang_ja">画面に直接ご署名ください。</span>
            </h1>
        </div>
    </div>

    <div id="enter-container">
        <button id="next-button">
            <span class="lang_en">continue</span>
            <span class="lang_ja">次へ</span>
        </button>
    </div>

    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
