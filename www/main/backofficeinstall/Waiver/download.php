<?php
  include "../includes/application_top.php";

	$user = new BJUser();
	$user_allowed = false;
	if ($user->hasRole('SysAdmin') || ($user->hasRole('Staff +') && $_SESSION['waiver_password'] == $config['waiver_password'])) {
		$user_allowed = true;
	};
	if (!$user_allowed) {
		auth_user();
		die();
	};
	

  mysql_set_charset('utf8');
$startDate = $endDate = date("Y-m-d");

if (isset($_GET['jumpDate']) && preg_match("/^([\d]{4}-[\d]{2}-[\d]{2})$/", $_GET['jumpDate'])) {
    $startDate = $endDate = $_GET['jumpDate'];
}
if (isset($_GET['startDate']) && preg_match("/^([\d]{4}-[\d]{2}-[\d]{2})$/", $_GET['startDate'])) {
    $startDate = $_GET['startDate'];
}
if (isset($_GET['endDate']) && preg_match("/^([\d]{4}-[\d]{2}-[\d]{2})$/", $_GET['endDate'])) {
    $endDate = $_GET['endDate'];
}

	if (isset($_GET['action'])) {
		$only_email = false;
		switch ($_GET['action']) {
			case 'email': 
			case 'Enquete Report': 
				$only_email = true;
			case 'Insurance Report': 
			case 'csv': 
				if ($only_email) {
					$sql = "SELECT w.email, 1, 2, 3 from customerregs1 cr, waivers w
							LEFT JOIN non_jumpers nj on (w.id = nj.waiver_id) 
							WHERE 
								cr.site_id = '" . CURRENT_SITE_ID . "'
								and cr.bookingdate BETWEEN '$startDate' AND '$endDate' 
								and cr.CustomerRegID = w.bid
								and w.email != ''
								and nj.id IS NULL";
				} else {
					$sql = "SELECT cr.BookingDate,
							w.id,
							w.site_id,
							w.bid,
							w.photo_number,
							w.agree,
							w.birth_year,
							w.birth_month,
							w.birth_day,
							w.sex,
							w.lastname,
							w.firstname,
							w.prefecture,
							w.phone_number,
							w.email,
							w.post_code,
							w.ticketed  
							
							from customerregs1 cr, waivers w 
							LEFT JOIN non_jumpers nj on (w.id = nj.waiver_id)
							WHERE 
								cr.site_id = '" . CURRENT_SITE_ID . "'
								and cr.bookingdate BETWEEN '$startDate' AND '$endDate' 
								and cr.CustomerRegID = w.bid
								and nj.id IS NULL
								and (cr.Agent not like '__ Bungy' or CollectPay != 'Offsite')";
					$sql .= " ORDER BY BookingDate ASC, w.id ASC";
				};
				$res = mysql_query($sql) or die(mysql_error());
				$cDate = $startDate . '_' . $endDate;
				$domain = strtolower(SYSTEM_SUBDOMAIN);
				Header('Content-Type: text/csv; name="'.$domain.'-waivers-'.str_replace('-', '', $cDate).'.csv";charset=UTF-8');
				Header('Content-Disposition: attachment; filename="'.$domain.'-waivers-'.str_replace('-', '', $cDate).'.csv"');
				$delimiter = ",";
				$headers_sent = false || $only_email;
				if (!$headers_sent) {
					$num_fields = mysql_num_fields($res) - 3;
					$row = array();
					for ($i = 0; $i < $num_fields; $i++) {
						$r = mysql_fetch_field($res, $i);
						$row[] = $r->name;
						if ($r->name == 'sex') {
							$row[] = 'age';
						};
					};
					echo implode($delimiter, $row) . "\n";
					$headers_sent = true;
				};
				while ($row = mysql_fetch_assoc($res)) {
					// remove last 3 fields
					for ($i = 0; $i < 3; $i++) {
						array_pop($row);
					};
					if (!$headers_sent) {
						echo implode($delimiter, array_keys($row)) . "\n";
						$headers_sent = true;
					};
					if (!$only_email) {
						$bd = $row['birth_day'] . ' ' . $row['birth_month'] . ' ' . $row['birth_year'];
						$age = new DateTime($bd);
						$age = $age->diff(new DateTime($row['BookingDate']))->format('%y');;
						array_splice($row, array_search('sex', array_keys($row)), 1, array('sex' => $row['sex'], 'age' => $age));
						$row['prefecture'] = str_replace('IAMAVISITORFROM', '', $row['prefecture']);
						$row['prefecture'] = str_replace('ō', 'O', $row['prefecture']);
						$row['prefecture'] = str_replace('Ō', 'O', $row['prefecture']);
					};
					echo implode($delimiter, array_map(function ($item) {return '"' . $item . '"';}, $row)) . "\n";
				};
				die();
				break;
			case 'Delete Email': 
			case 'delete_email': 
				$sql = "Update waivers set email = '' WHERE email = '".db_input($_GET['email'])."';";
				$res = mysql_query($sql);
				Header("Location: /Waiver/download.php?startDate=$startDate&endDate=$endDate&result=".mysql_affected_rows());
				die();
				break;
		}; 
	};
?>
<!DOCTYPE html>
<html>
    <head>
<?php include '../includes/header_tags.php'; ?>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=8">
        <title>View All Data</title>

<?php include '../includes/head_scripts.php'; ?>
        <script type="text/javascript">
$(document).ready(function () {
	$('.datepicker').datepicker({
		showAnim: 'fade',
		numberOfMonths: 1,
		//dateFormat: 'dd/mm/yy',
		dateFormat: 'yy-mm-dd',
		currentText: 'Today'
	});
	//$('.datepicker').change(function () {$(this).parents('form').submit();});
	$('input[name=startDate]').change(function () {
		$('input[name=endDate]').val($('input[name=startDate]').val());
	});
	$('input[name=endDate]').change(function () {
		if ($('input[name=endDate]').val() < $('input[name=startDate]').val()) {
			$('input[name=startDate]').val($('input[name=endDate]').val());
		};
	});
	$('#back').click(function () {
		document.location = '/';
	});
<?php if (isset($_GET['result'])) { ?>
<?php if ($_GET['result'] > 0) { ?>
	alert("Email deleted from <?php echo $_GET['result']; ?> record(s)");
<?php } else { ?>
	alert("Email not found");
<?php }; ?>
<?php }; ?>
});

        </script>
<style>
body, table td {
	text-align: left;
}
table {
	margin-left: auto;
	margin-right: auto;
}
#back {
	float: left;
}
</style>
    </head>
    <body>
<form>
<table>
<tr><td colspan="2">
<br />
	<h1><?php echo SYSTEM_SUBDOMAIN; ?> Waiver Data Download</h1>
</td></tr>
<tr><td>
Start date: <input name="startDate" value="<?php echo $startDate; ?>" class="datepicker">
End date: <input name="endDate" value="<?php echo $endDate; ?>" class="datepicker">
</td><td>
	<button type="submit" name="action" value="csv">Insurance Report</button>
	<button type="submit" name="action" value="email">Enquete Report</button>
<td></tr>
<tr><td colspan="2">&nbsp;<td></tr>
<tr><td style="text-align: right;">
<button id="back" type="button">Back</button>
Email: <input name="email" value="">
</td><td>
<button type="submit" name="action" value="delete_email">Delete Email</button>
</td></tr>
<tr><td colspan="2">
</td></tr>
</table>
</form>

</body>
</html>
