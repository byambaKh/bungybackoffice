<div id="step6" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">please enter your birthday</span>
                <span class="lang_ja">誕生日をご入力ください。</h1>
        </div>
    </div>
    <div id="birthdate-container">
        <div id="birth-month-container">
            <div id="birth-month">
                <div id="birth-month-value"><input name="birth_month"></div>
            </div>
            <div class="keyboard-container">
                <div class="keyboard-button lang_en">
                    <button>JAN</button>
                </div>
                <div class="keyboard-button lang_en">
                    <button>FEB</button>
                </div>
                <div class="keyboard-button lang_en">
                    <button>MAR</button>
                </div>
                <div class="keyboard-button lang_en">
                    <button>APR</button>
                </div>
                <div class="keyboard-button lang_en">
                    <button>MAY</button>
                </div>
                <div class="keyboard-button lang_en">
                    <button>JUN</button>
                </div>
                <div class="keyboard-button lang_en">
                    <button>JUL</button>
                </div>
                <div class="keyboard-button lang_en">
                    <button>AUG</button>
                </div>
                <div class="keyboard-button lang_en">
                    <button>SEP</button>
                </div>
                <div class="keyboard-button lang_en">
                    <button>OCT</button>
                </div>
                <div class="keyboard-button lang_en">
                    <button>NOV</button>
                </div>
                <div class="keyboard-button lang_en">
                    <button>DEC</button>
                </div>
                <div class="keyboard-button lang_ja">
                    <button>1月</button>
                </div>
                <div class="keyboard-button lang_ja">
                    <button>2月</button>
                </div>
                <div class="keyboard-button lang_ja">
                    <button>3月</button>
                </div>
                <div class="keyboard-button lang_ja">
                    <button>4月</button>
                </div>
                <div class="keyboard-button lang_ja">
                    <button>5月</button>
                </div>
                <div class="keyboard-button lang_ja">
                    <button>6月</button>
                </div>
                <div class="keyboard-button lang_ja">
                    <button>7月</button>
                </div>
                <div class="keyboard-button lang_ja">
                    <button>8月</button>
                </div>
                <div class="keyboard-button lang_ja">
                    <button>9月</button>
                </div>
                <div class="keyboard-button lang_ja">
                    <button>10月</button>
                </div>
                <div class="keyboard-button lang_ja">
                    <button>11月</button>
                </div>
                <div class="keyboard-button lang_ja">
                    <button>12月</button>
                </div>
            </div>
        </div>
        <div id="birth-day-container">
            <div id="birth-day">
                <div id="bidth-day-value"><input name="birth_day"></div>
                <div id="birth-day-del-button">
                    <button><span class="lang_en">DEL</span><span class="lang_ja">訂正</span></button>
                </div>
            </div>
            <div class="keyboard-container">
                <div class="keyboard-button">
                    <button>1</button>
                </div>
                <div class="keyboard-button">
                    <button>2</button>
                </div>
                <div class="keyboard-button">
                    <button>3</button>
                </div>
                <div class="keyboard-button">
                    <button>4</button>
                </div>
                <div class="keyboard-button">
                    <button>5</button>
                </div>
                <div class="keyboard-button">
                    <button>6</button>
                </div>
                <div class="keyboard-button">
                    <button>7</button>
                </div>
                <div class="keyboard-button">
                    <button>8</button>
                </div>
                <div class="keyboard-button">
                    <button>9</button>
                </div>
                <div class="keyboard-button">
                    <button>0</button>
                </div>
            </div>
        </div>
    </div>
    <div id="enter-container">
        <div id="enter">
            <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>