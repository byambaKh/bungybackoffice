<div id="step8" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en"><span style="font-size: 80px;">please enter your</span> last name</span>
                <span class="lang_ja">ローマ字で姓字をご入力ください。</span>
            </h1>
        </div>
    </div>
    <div id="name-container">
        <div id="name-input-container"><input name="lastname"></div>
    </div>
    <div id="keyboard-container">
        <?php
        $keyboard = array(
            array('q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'),
            array('a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l'),
            array('z', 'x', 'c', 'v', 'b', 'n', 'm')
        );
        foreach ($keyboard as $line) {
            echo '<div class="keyboard-line">';
            foreach ($line as $letter) {
                echo "<div class=\"keyboard-button\"><button>" . strtoupper($letter) . "</button></div>";
            };
            echo "</div>";
        };
        ?>
        <div class="keyboard-line">
            <div class="keyboard-button lang_en" id="space">
                <button>SPACE</button>
            </div>
            <div class="keyboard-button lang_ja" id="space">
                <button>スペース</button>
            </div>
        </div>
    </div>
    <div id="name-container">  
        <div id="del-button">
            <button><span class="lang_en">DEL</span><span class="lang_ja">訂正</span></button>
        </div>
        <div id="enter-button">
            <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>