<div id="step14" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">Please sign your name using your finger</span>
                <span class="lang_ja">画面に直接ご署名ください。</span>
            </h1>
        </div>
    </div>
    <div id="sig-container">
        <canvas id="waiver-signature" height="600" width="1000"/>
    </div>
    <div id="actions-container">
        <div id="del-button">
            <button><span class="lang_en">DELETE</span><span class="lang_ja">訂正</span></button>
        </div>
        <div id="enter-button">
            <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ<//span></button>
        </div>
    </div>

    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>