<div id="step2" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">NAME OF BOOKING</span>
                <span class="lang_ja">代表者様のお名前</span>
            </h1>

            <h2>
                <span class="lang_en">Please select the name in which your booking was made and then press [Continue].</span>
                <span class="lang_ja">をタッチして、「次へ」お押ししてください。</span>
            </h2>
        </div>
        <div class="refresh">
            <button>
                <span class="lang_en">refresh</span>
                <span class="lang_ja">更新</span>
            </button>
        </div>
    </div>
    <div id="names-container">
        <div id="names-container-child" class="scrollable">
            <?php if ($pc_version == 0) { ?>
                <ul id="booking-list">
                    <?php foreach ($bookingNames as $bookingIndex => $booking) {
                        echo "<li id='{$booking['id']}'>{$booking['name']}</li>";
                    } ?>
                </ul>
            <?php } else { ?>
                <select id="booking-list" multiple="multiple">
                    <?php foreach ($bookingNames as $bookingIndex => $booking) {
                        echo "<option value='{$booking['id']}'>{$booking['name']}</option>";
                    } ?>
                </select>
            <?php }; ?>
        </div>
        <!--select name="bid" size="6" id="booking-id" style="width: 850px; height: 530px;">
          <option>test1</option>
          <option>test 2</option>
        </select-->
    </div>
    <div id="bottom-container">
        <button>
            <span class="lang_en">continue</span>
            <span class="lang_ja">次へ</span>
        </button>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
