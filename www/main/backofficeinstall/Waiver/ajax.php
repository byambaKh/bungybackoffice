<?php
include '../includes/application_top.php';
include 'jumpcode.php';
$site_id = (int)CURRENT_SITE_ID;
date_default_timezone_set('Asia/Tokyo');
mysql_query("SET NAMES UTF8;");

function writeSignatureFile($waiverId, $data)
{
    $path = "../uploads/signatures/$waiverId.jpg";
    $waiverImageFileHandle = fopen($path, "w");
    $image = str_replace("data:image/jpeg;base64,", "", $data);//get rid of mime data as this invalidates the file
    $image = base64_decode($image);
    fputs($waiverImageFileHandle, $image);
    fclose($waiverImageFileHandle);
    unset($waiverImageFileHandle);
}

$result = array();
switch (TRUE) {
    case (isset($_GET['action']) && $_GET['action'] == 'save-waiver'):
        #$fp = fopen('ajax.log', "a+");
        #fputs($fp, date('Y-m-d H-i-s : ') . print_r($_POST, true));
        #fclose($fp);
        // if multiply select used
        if (is_array($_POST['booking_id'])) {
            $_POST['booking_id'] = $_POST['booking_id'][0];
        };
        $data = array(
            'bid' => $_POST['booking_id'],
            'photo_number' => $_POST['photo_number'],
            'agree' => $_POST['agree'],
            'birth_year' => $_POST['birth_year'],
            'birth_month' => date("M", mktime(0, 0, 0, preg_replace("/([^\d]+)/", "", $_POST['birth_month']), 1, 2000)),
            'birth_day' => $_POST['birth_day'],
            'sex' => $_POST['sex'],
            'lastname' => strtoupper($_POST['last_name']),
            'firstname' => strtoupper($_POST['first_name']),
            'prefecture' => mb_strtoupper(preg_replace("/([\P{Latin}]+)/u", "", $_POST['prefecture'])),
            'phone_number' => $_POST['phone_number'],
            'email' => strtoupper($_POST['email']),
            'sig_img' => '',//sig_img is now blank and the images are stored in a folder
            'weight_kg' => $_POST['weight'],
            'site_id' => $site_id
        );


        db_perform('waivers', $data);
        $waiverId = mysql_insert_id();
        writeSignatureFile($waiverId, $_POST['sig_img']);
        
        $jumpCode = getJumpCode($data, $site_id);

        $printData = array(
            'id' => $waiverId,
            'site_id' => $_POST['site_id'],
            'to_print' => 1,
            'jump_code' => $jumpCode,
        );

        db_perform('printjobs', $printData);

        $result['success'] = true;
        break;
    case (isset($_GET['action']) && $_GET['action'] == 'get-names'):
        $i = 0;

        //Generate Fake bookings for the TEST subdomain
        while ($i++ < 10) {
            $result['data'][] = array(
                'id' => 1,
                'name' => 'TEST CUSTOMER',
            );
        };

        if (SYSTEM_SUBDOMAIN_REAL != 'TEST') {
            $result = array();
        };
        $bDate = date('Y-m-d');
        $sql = "SELECT cr.CustomerRegID, cr.CustomerLastName, cr.CustomerFirstName, cr.NoOfJump, count(w.id) as total_w
					FROM customerregs1 cr 
					LEFT JOIN waivers w on (cr.customerregid = w.bid)
					WHERE 
						cr.site_id = '" . CURRENT_SITE_ID . "'
						AND cr.BookingDate = '" . $bDate . "'
						and cr.Checked = 1
					GROUP BY cr.CustomerRegID;";
        $res = mysql_query($sql) or die(json_encode(array('error' => 'some issues with DB')));
        while ($row = mysql_fetch_assoc($res)) {
            if ($row['total_w'] < $row['NoOfJump']) {
                $result['data'][] = array(
                    'id' => $row['CustomerRegID'],
                    'name' => strtoupper(htmlentities(mb_convert_encoding($row['CustomerLastName'] . ' ' . $row['CustomerFirstName'], 'UTF-8', 'SJIS')))
                );
            };
        };
        $result['success'] = true;
        break;
};
Header("Content-type: application/json; encoding: utf-8");
echo json_encode($result);
