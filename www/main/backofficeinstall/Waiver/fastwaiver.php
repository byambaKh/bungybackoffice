<?php
include "../includes/application_top.php";
//error_reporting(E_ALL);
require_once('functions.php');
$max_steps = 15;
if (!isset($_GET['step'])) {
    $_GET['step'] = 1;
};
if ($_GET['step'] < 1 || $_GET['step'] > $max_steps) {
    $_GET['step'] = 1;
};
$lang = 'en';
if (array_key_exists('lang', $_GET) && in_array($_GET['lang'], array('en', 'ja'))) {
    $lang = $_GET['lang'];
};
$pc_version = true;
if (preg_match("/iPad/i", $_SERVER['HTTP_USER_AGENT'])) {
    $pc_version = false;
};

$bookingNames = getNames();

?>
<!DOCTYPE html>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<title>Bungy Japan Waiver</title>
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="viewport" content="initial-scale=0.8,user-scalable=no,maximum-scale=1,height=device-height">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<?php include "../includes/head_scripts.php"; ?>
<script src="js/jquery.event.move.js"></script>
<link rel="stylesheet"
      href="/Waiver/css/stylesheet.css"
      type="text/css" media="all"/>
<script>
var pc_version = <?php echo (int)$pc_version; ?>;
function scrollend(e) {
    document.lastScroll = null;
}
function scrollmove(e) {
    var targetEvent = e.originalEvent;
    if (typeof document.lastScroll == 'undefined' || document.lastScroll === null) {
        document.lastScroll = targetEvent.pageY;
    }
    ;
    var el = $(this).children();
    var old_scroll = $(el).prop('scroll');
    if (typeof old_scroll == 'undefined') {
        old_scroll = 0;
    }
    ;
    var scroll = old_scroll - targetEvent.pageY + document.lastScroll;
    if (scroll < 0) {
        scroll = 0;
    }
//alert($(el).height() + ' ' + $(el).parent().height() + ' ' + scroll);
    if (scroll > ($(el).height() - $(el).parent().height())) {
        scroll = $(el).height() - $(el).parent().height();
    }
    ;
    $(el).css('webkitTransform', 'translate(0px,-' + scroll + 'px)');
    $(el).prop('scroll', scroll);
    document.lastScroll = targetEvent.pageY;
}
// scrolling disable
document.addEventListener('touchmove', function (e) {
    e.preventDefault();
});
function select_language(lang) {
    document.bungywaiver_lang = lang;
    $('.lang_en, .lang_ja').css('display', 'none');
    $('.lang_' + lang).css('display', 'block');
}

$(document).ready(function () {
    if (pc_version == 0) {
        $('.scrollable').bind('touchmove', scrollmove);
        $('.scrollable').bind('touchend', scrollend);
    }
    ;
    $('input').prop('disabled', 'disabled');
    goto_step(<?php echo (int)$_GET['step']; ?>);
    select_language('<?php echo $lang; ?>');
    $(".back button").bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        document.current_step--;
        if (document.current_step == 11) {
            document.current_step--;
        }
        ;
        goto_step(document.current_step);
    });
    $('#english-container button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        select_language('en');
        goto_step(2);
    });
    $('#japanese-container button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        select_language('ja');
        goto_step(2);
    });
    //make the li list for the iPad booking name selection, selectable
    $('#step2 li').bind('click touchend MSPointerUp pointerup', function (e){
        e.preventDefault();
        $('#step2 li').removeClass('selected');
        $(this).addClass('selected');
    });

    $('#step2 .refresh button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        //return //fill_names();
    });
    $('#step2 #bottom-container button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var selected = false;
        if (pc_version == 0) {
            $('#step2 li').each(function () {
                if ($(this).hasClass('selected')) selected = $(this).prop('id');
            });
        } else {
            selected = $("#step2 select").val();
            if (typeof selected == 'undefined') {
                selected = false;
            }
        }
        ;
        if (selected) {
            save_var('booking_id', selected);
            goto_step(3);
        } else {
            show_info("bookingname");
            save_var('booking_id', 'null');
        }
        ;
    });
    $('#step3 .keyboard-button button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        if ($('#step3 #photo-number input').val().length == 3) return;
        $('#step3 #photo-number input').val('' + $('#step3 #photo-number input').val() + $(this).html());
    });
    $('#step3 #del  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step3 #photo-number input').val();
        if (val.length > 0) {
            $('#step3 #photo-number input').val(val.substr(0, val.length - 1));
        }
        ;
    });
    $('#step3 #photo-dho  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        save_var('photo_number', null);
        goto_step(4);
    });
    $('#step3 #enter  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step3 #photo-number input').val();
        if (val.length > 0) {
            save_var('photo_number', val);
            goto_step(4);
        } else {
            show_info("validphoto");
        }
        ;
    });
    $('#step4 #agree  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        save_var('agree', 1);
        goto_step(5);
    });
    $('#step4 #exit  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        document.bj_waiver = null;
        goto_step(1);
    });
    $('#step5 .keyboard-button button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        if ($('#step5 #photo-number input').val().length == 4) return;
        $('#step5 #photo-number input').val('' + $('#step5 #photo-number input').val() + $(this).html());
    });
    $('#step5 #del  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step5 #photo-number input').val();
        if (val.length > 0) {
            $('#step5 #photo-number input').val(val.substr(0, val.length - 1));
        }
        ;
    });
    $('#step5 #enter  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step5 #photo-number input').val();
        if (val.length > 0) {
            if (val > 1900) {
                save_var('birth_year', val);
                goto_step(6);
            } else {
                show_info("yeargreater");
            }
        } else {
            show_info("validyear");
        }
        ;
    });
    $('#step6 #birth-month-container button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        $('#step6 #birth-month input').val($(this).html());
    });
    $('#step6 #birth-day-container .keyboard-container button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        if ($('#step6 #birth-day input').val().length == 2) return;
        $('#step6 #birth-day input').val('' + $('#step6 #birth-day input').val() + $(this).html());
    });
    $('#step6 #birth-day-del-button button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step6 #birth-day input').val();
        if (val.length > 0) {
            $('#step6 #birth-day input').val(val.substr(0, val.length - 1));
        }
        ;
    });
    $('#step6 #enter button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var bmonth = $('#step6 #birth-month input').val();
        var bday = $('#step6 #birth-day input').val();
        var error = false;
        if (bmonth.length == 0) {
            show_info("validmonth");
            error = true;
        }
        ;
        if (!error && bday.length == 0) {
            show_info("validday");
            error = true;
        }
        ;
        if (!error && bday > 31) {
            show_info("validday31");
            error = true;
        }
        ;
        if (!error) {
            save_var('birth_month', bmonth);
            save_var('birth_day', bday);
            goto_step(7);
        }
        ;
    });
    $('#step7 #male-button button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        save_var('sex', 'male');
        goto_step(8);
    });
    $('#step7 #female-button button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        save_var('sex', 'female');
        goto_step(8);
    });
    $('#step8 .keyboard-button button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        $('#step8 #name-input-container input').val('' + $('#step8 #name-input-container input').val() + $(this).html().replace('SPACE', ' ').replace('スペース', ' '));
        var element = $('#step8 #name-input-container input')[0];
        if (element.setSelectionRange) {
            element.setSelectionRange(element.value.length, element.value.length); // scrolls to end
            element.focus(); // makes caret visible
        }
        ;
    });
    $('#step8 #del-button  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step8 #name-input-container input').val();
        if (val.length > 0) {
            $('#step8 #name-input-container input').val(val.substr(0, val.length - 1));
        }
        ;
        var element = $('#step8 #name-input-container input')[0];
        if (element.setSelectionRange) {
            element.setSelectionRange(element.value.length, element.value.length); // scrolls to end
            element.focus(); // makes caret visible
        }
        ;
    });
    $('#step8 #enter-button  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step8 #name-input-container input').val();
        if (val.length > 0) {
            save_var('last_name', val);
            goto_step(9);
        } else {
            show_info("validlast");
        }
        ;
    });
    $('#step9 .keyboard-button button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        $('#step9 #name-input-container input').val('' + $('#step9 #name-input-container input').val() + $(this).html().replace('SPACE', ' ').replace('スペース', ' '));
        var element = $('#step9 #name-input-container input')[0];
        if (element.setSelectionRange) {
            element.setSelectionRange(element.value.length, element.value.length); // scrolls to end
            element.focus(); // makes caret visible
        }
        ;
    });
    $('#step9 #del-button  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step9 #name-input-container input').val();
        if (val.length > 0) {
            $('#step9 #name-input-container input').val(val.substr(0, val.length - 1));
        }
        ;
        var element = $('#step9 #name-input-container input')[0];
        if (element.setSelectionRange) {
            element.setSelectionRange(element.value.length, element.value.length); // scrolls to end
            element.focus(); // makes caret visible
        }
        ;
    });
    $('#step9 #enter-button  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step9 #name-input-container input').val();
        if (val.length > 0) {
            save_var('first_name', val);
            goto_step(10);
        } else {
            show_info("validfirst");
        }
        ;
    });
    $('#step10 #place-container button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        save_var('prefecture', $(this).html());
        goto_step(11);
    });
    $('#step11 .keyboard-button button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        $('#step11 #street1-input-container input').val('' + $('#step11 #street1-input-container input').val() + $(this).html().replace('SPACE', ' ').replace('スペース', ' '));
        var element = $('#step11 #street1-input-container input')[0];
        if (element.setSelectionRange) {
            element.setSelectionRange(element.value.length, element.value.length); // scrolls to end
            element.focus(); // makes caret visible
        }
        ;
    });
    $('#step11 #del  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step11 #street1-input-container input').val();
        if (val.length > 0) {
            $('#step11 #street1-input-container input').val(val.substr(0, val.length - 1));
        }
        ;
        var element = $('#step11 #street1-input-container input')[0];
        if (element.setSelectionRange) {
            element.setSelectionRange(element.value.length, element.value.length); // scrolls to end
            element.focus(); // makes caret visible
        }
        ;
    });

    $('#step12 .keyboard-button button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        if ($('#step12 #phone-number input').val().length == 11) return;
        $('#step12 #phone-number input').val('' + $('#step12 #phone-number input').val() + $(this).html());
    });
    $('#step12 #del  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step12 #phone-number input').val();
        if (val.length > 0) {
            $('#step12 #phone-number input').val(val.substr(0, val.length - 1));
        }
        ;
    });
    $('#step12 #enter  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step12 #phone-number input').val();
        if (val.length > 8 && val.length < 12) {
            save_var('phone_number', val);
// ignore medical form screen
            save_var('med_form', 0);
            goto_step(13);
        } else {
            show_info("validphone");
        }
        ;
    });
    /*
     $('#step13 #rather-not button').click(function () {
     $('#step13 input').val('');
     $('#step13 #enter button').trigger('click');
     });
     */
    $('#step13 .keyboard-button button, #step13 #domains-container button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        $('#step13 input').val('' + $('#step13 input').val() + $(this).html().toLowerCase());
    });
    $('#step13 #del button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step13 input').val();
        if (val.length > 0) {
            $('#step13 input').val(val.substr(0, val.length - 1));
        }
        ;
    });
    $('#step13 #enter button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        if (!/(.+)@(.+\..+)/.test($("#step13 input").val())) {
            show_info('validemail');
            return;
        }
        ;
        save_var('email', $("#step13 input").val());
        goto_step(14);
    });

    $('#waiver-signature').bind('movestart MSPointerDown pointerdown', bjmovestart);
    $('#waiver-signature').bind('move MSPointerMove pointermove', bjmove);
    $('#waiver-signature').bind('moveend MSPointerUp pointerup', bjmoveend);
    //$('#waiver-signature').bind('click', bjmovestart);
    $('#step14 #del-button button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var c = $('#waiver-signature')[0];
        var ctx = c.getContext('2d');
        var rect = document.getElementById('waiver-signature').getBoundingClientRect();
        ctx.fillStyle = 'white';
        ctx.beginPath();
        ctx.fillRect(0, 0, rect.width, rect.height);
        ctx.stroke();
        document.bj_signature = null;
    });
    $('#step14 #enter-button button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        if (typeof document.bj_signature == 'undefined' || document.bj_signature === null) {
            alert('Please draw your signature');
            return false;
        }
        var c = $('#waiver-signature')[0];
        var img = c.toDataURL('image/jpeg', 0.8);
        save_var('sig_img', img);
        goto_step(15);
    });

});
function bjmovestart(e) {
    document.bj_last_coords = e;
    bjmove(e);
}
function bjmove(e) {
    if (typeof document.bj_last_coords == 'undefined' || document.bj_last_coords === null) {
        document.bj_last_coords = e;
    }
    var c = $('#waiver-signature')[0];
    var ctx = c.getContext('2d');
    var rect = document.getElementById('waiver-signature').getBoundingClientRect();
    ctx.beginPath();
    ctx.moveTo(document.bj_last_coords.pageX - rect.left - window.scrollX, document.bj_last_coords.pageY - rect.top - window.scrollY);
    ctx.lineWidth = 10;
    ctx.lineCap = 'round';
    ctx.strokeStyle = 'blue';
    ctx.lineTo(e.pageX - rect.left - window.scrollX, e.pageY - rect.top - window.scrollY);
    ctx.stroke();
    document.bj_last_coords = e;
    document.bj_signature = true;
}
function bjmoveend(e) {
    var c = $('#waiver-signature')[0];
    var ctx = c.getContext('2d');
    document.bj_last_coords = null;
}
function save_var(name, value) {
    if (typeof document.bj_waiver == 'undefined' || document.bj_waiver == null) {
        document.bj_waiver = new Object();
    }
    ;
    document.bj_waiver[name] = value;
}
function goto_step(step) {
    if (step == 2) {
        //fill_names();
    }
    ;
    if (step == 11) {
        step = 12;
    }
    ;
    document.current_step = step;
    for (var i = 1; i <= <?php echo $max_steps; ?>; i++) {
        if ($('#step' + i).length) {
            $('#step' + i).css('display', 'none');
            if (i == step) {
                $('#step' + i).css('display', 'block');
            }
            ;
        }
        ;
    }
    ;
    if (step == 14) {
        $('#step14 #del-button button').trigger('touchend');
    }
    ;
    // save data, restart form
    if (step == 15) {
        $("#step15progress").css({
            width: 800,
            height: 30,
            "background-color": 'black',
            border: "2px solid white",
            'margin-top': '50px',
            'margin-right': 'auto',
            'margin-left': 'auto'
        }).html("<div></div>");
        $("#step15progress div").css({
            width: 0,
            height: 26,
            "background-color": 'red'
        });
        var timer_sec = 5;
        document.reload_timer = setInterval(function () {
            timer_sec--;
            //$("#restart-in").html(timer_sec);
            var width = $("#step15progress div").css('width').replace('px', '');
            if (width == 0) width -= 2;
            if (width > 600) width -= 2;
            $("#step15progress div").css('width', parseInt(width) + 160);
            if (width > 600) {
                clearInterval(document.reload_timer);
                document.location = '/Waiver/';
            }
            ;
        }, 1000);
    }
    if (step == 15) {
        // save all data
        if (!save_data()) {
            show_info("errorsaving");
            goto_step(14);
            return;
        }
        ;
    }
    ;
}
function save_data() {
    return $.ajax(
        'ajax.php?action=save-waiver',
        {
            data: document.bj_waiver,
            dataType: 'json',
            type: 'POST',
            async: false,
            cache: false,
            success: function (data) {
                return true;
            }
        }
    );
}
function fill_names() {
    $.ajax(
        'ajax.php?action=get-names',
        {
            dataType: 'json',
            cache: false,
            success: function (data) {
                if (data['error']) {
                    show_info(data['error']);
                } else {
                    if (pc_version == 0) {
                        $('#step2 ul > li').remove();
                    } else {
                        $('#step2 select > option').remove();
                    }
                    ;
                    //$('#step2 select').prop('multiple', false);
                    if (data['data'] && data['data'].length) {
                        for (var i in data['data']) {
                            if (!data['data'].hasOwnProperty(i)) continue;
                            if (pc_version == 0) {
                                var o = $('<li></li>');
                                o.prop('id', data['data'][i].id);//set the #id as the database id of the booking
                                o.html(data['data'][i].name);//set the inner html to the name of the booking
                                o.bind('click touchend MSPointerUp pointerup', function (e) {
                                    e.preventDefault();
                                    $('#step2 li').removeClass('selected');
                                    $(this).addClass('selected');
                                });
                                $('#step2 ul').append(
                                    o
                                );
                            } else {
                                var o = $('<option></option>')
                                $('#step2 select').append(
                                    o.prop('value', data['data'][i].id).prop('text', data['data'][i].name)
                                );
                            }
                            ;
                        }
                    }
                    ;
                    //$('#step2 select').prop('multiple', true);
                }
            },
        }
    );
}
function show_info(msg) {
    var lang = {
        en: {
            "bookingname": "You must select the name under which your booking was made.",
            "validphoto": "You must enter a valid <br /><b>photo number</b><br /> or click<br /> the <b>\"No photos purchased\"</b> button.",
            "yeargreater": "The year must be greater than 1900.",
            "validyear": "The year must be greater than 1900.",
            "validmonth": "Please enter your month of birth.",
            "validday": "Please enter your day of birth.",
            "validday31": "Please enter your day of birth.",
            "validlast": "You must enter your last name to continue.",
            "validfirst": "You must enter your first name to continue.",
            "validphone": "Your phone number must contain from 9 to 11 digits.",
            "validemail": "Pleae enter valid email",
            "errorsaving": 'some issues saving you credentials'
        },
        ja: {
            "bookingname": "代表者様のお名前を<br />選択してください。",
            "validphoto": "お写真の番号を入力するか「ご購入されない方はこちら」のボタンを押してください。",
            "yeargreater": "１９００年以降でご入力ください。",
            "validyear": "１９００年以降でご入力ください。",
            "validmonth": "誕生月を入力してください。",
            "validday": "誕生日を入力してください。",
            "validday31": "誕生日を入力してください。",
            "validlast": "姓字を入力してください。",
            "validfirst": "名前を入力してください。",
            "validphone": "9文字～１１文字でご入力ください。",
            "validemail": 'Pleae enter valid email',
            "errorsaving": 'some issues saving you credentials'
        }
    }
    if (lang.hasOwnProperty(document.bungywaiver_lang) && lang[document.bungywaiver_lang].hasOwnProperty(msg)) {
        msg = lang[document.bungywaiver_lang][msg];
    }
    ;
    $('#info-dialog').html(msg);
    $('#info-dialog').dialog({
        buttons: [
            {
                text: (document.bungywaiver_lang == 'en') ? "Close" : "OK",
                width: 650,
                MSPointerUP: function () {
                    $('#info-dialog').dialog("close");
                },
                pointerup: function () {
                    $('#info-dialog').dialog("close");
                },
                click: function () {
                    $('#info-dialog').dialog("close");
                },
                touchend: function () {
                    $('#info-dialog').dialog("close");
                }
            }
        ],
        dialogClass: "alert",
        draggable: false,
        modal: true,
        position: {my: "center", at: "center", of: window},
        resizable: false,
        minWidth: 800,
        maxWidth: 1200,
        //html : msg,
        title: (document.bungywaiver_lang == 'en') ? "Information" : "注意"
    });
}
</script>
</head>
<body scroll="no">
<?php
if ($pc_version) {
    ?>
    <script>
        $(document).ready(function () {
            $("body").css('padding-top', '10px');
            $(".back").each(function () {
                $(this).css("top", 80 + parseInt($(this).css("top").replace('px', '')));
            });
            $("#domains-container").css("top", 55 + parseInt($("#domains-container").css("top").replace('px', '')));
            //$("#rather-not").css("top", 35 + parseInt($("#rather-not").css("top").replace('px', '')));
            $("#email-note").css("top", 50);
        });
    </script>
<?php
};
?>
<div>
<div id="step1" style="display: none;">
    <div id="waiver-container">
        <div class="header-container"><img alt="Step 1: Language selection" src="img/wheader.png"></div>
        <div id="language-container">
            <div id="english-container" class="half">
                <button><img src="img/wenglish.png"></button>
            </div>
            <div id="japanese-container" class="half">
                <button><img src="img/wjapanese.png"></button>
            </div>
        </div>
    </div>
    <div id="step1-add-info">
        <nobr>文字入力や各ボタンの選択が認識されるまで、</nobr>
        <br/>
        多少時間がかかります。ひとつずつ、ゆっくりめにご入力ください。
    </div>
</div>
<div id="step2" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">NAME OF BOOKING</span>
                <span class="lang_ja">代表者様のお名前</span>
            </h1>

            <h2>
                <span class="lang_en">Please select the name in which your booking was made and then press [Continue].</span>
                <span class="lang_ja">をタッチして、「次へ」お押ししてください。</span>
            </h2>
        </div>
        <div class="refresh">
            <button>
                <span class="lang_en">refresh</span>
                <span class="lang_ja">更新</span>
            </button>
        </div>
    </div>
    <div id="names-container">
        <div id="names-container-child" class="scrollable">
            <?php if ($pc_version == 0) { ?>
                <ul id="booking-list">
                    <?php foreach($bookingNames as $bookingIndex => $booking){
                        echo "<li id='{$booking['id']}'>{$booking['name']}</li>";
                    }?>
                </ul>
            <?php } else { ?>
                <select id="booking-list" multiple="multiple">
                    <?php foreach($bookingNames as $bookingIndex => $booking){
                        echo "<option value='{$booking['id']}'>{$booking['name']}</option>";
                    }?>
                </select>
            <?php }; ?>
        </div>
        <!--select name="bid" size="6" id="booking-id" style="width: 850px; height: 530px;">
          <option>test1</option>
          <option>test 2</option>
        </select-->
    </div>
    <div id="bottom-container">
        <button>
            <span class="lang_en">continue</span>
            <span class="lang_ja">次へ</span>
        </button>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step3" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">please enter your photo number</span>
                <span class="lang_ja">お写真又はビデオをご購入された方は左手首の番号をご入力ください。</span>
            </h1>

            <h2>
                <span class="lang_en">if you purchased them and have a number on your left wrist</span>
                <span class="lang_ja"></span>
            </h2>
        </div>
    </div>
    <div id="content-container">
        <div id="photo-example"><img src="img/step3_hand.png" alt="Hand example"/></div>
        <div id="photo-number"><input name="photo_number"></div>
        <div id="photo-dho">
            <button>
                <span class="lang_en">No photos purchased.</span>
                <span class="lang_ja">ご購入されない方はこちら</span>
            </button>
        </div>
    </div>
    <div id="photo-keyboard-container">
        <div class="photo-keyboard">
            <div class="keyboard-button">
                <button>1</button>
            </div>
            <div class="keyboard-button">
                <button>2</button>
            </div>
            <div class="keyboard-button">
                <button>3</button>
            </div>
            <div class="keyboard-button">
                <button>4</button>
            </div>
            <div class="keyboard-button">
                <button>5</button>
            </div>
            <div class="keyboard-button">
                <button>6</button>
            </div>
            <div class="keyboard-button">
                <button>7</button>
            </div>
            <div class="keyboard-button">
                <button>8</button>
            </div>
            <div class="keyboard-button">
                <button>9</button>
            </div>
            <div class="keyboard-button">
                <button>0</button>
            </div>
        </div>
        <div id="enter-del-container">
            <div id="del">
                <button><span class="lang_en">DELETE</span><span class="lang_ja">訂正</span></button>
            </div>
            <div id="enter">
                <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
            </div>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step4" style="display: none;">
    <div class="header-container" style="margin-top: 10px; margin-bottom: 10px;">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">waiver of liability and indemnity agreement</span>
                <span class="lang_ja" style="font-size:45px;">バンジージャンプ確認書兼保険申込</span>
            </h1>
        </div>
    </div>
    <div id="content-container">
        <!--since I am going to rewrite this entire file I am leaving these styles here-->
        <style>
            .readable_text {
                font-family: "Helvetica";
                color: white;
            }

            #en_waiver_table tr td {
                padding-bottom: 10px;
                vertical-align: top;
                font-size: 20px;
                line-height: 19px;
                font-weight: 100;
            }

            .en_waiver_table_number {
                font-size: 15px;
                line-height: 8px;
            }

            #en_waiver_header p {
                font-size: 17px !important;
                line-height: 17px !important;
                font-weight: 900 !important;
            }

        </style>
<span class="lang_en">
      <span id="en_waiver_header" class="lang_en en_waiver_header readable_text">
	<p>READ AND FULLY UNDERSTAND EACH PROVISION OF THIS AGREEMENT AND INDICATE YOUR ACKNOWLEDGEMENT BY TOUCHING "I AGREE".</p>
	<p>IN CONSIDERATION of the organizers allowing you (hereinafter referred to as "the participant") to utilize the facilities and equipment and to participate in bungy jumping and its associated activities, it is agreed that:</p>
</span>

<table id="en_waiver_table" class="readable_text">
    <tr>
        <td class="en_waiver_table_number">1.
        <td>I understand &amp; acknowledge that participating in bungy jumping has inherent risks, including but not limited to physical injury &amp; even death.</td>
    </tr>
    <tr>
        <td class="en_waiver_table_number">2.
        <td>I confirm that I understand and am in accordance with all applicable participation requirements. I assert that I have no injury or disease that would affect my ability to safely participate as such. I also agree to refrain from participating in bungy jumping when I am pregnant or under the influence of alcohol. I also understand and acknowledge that if I conceal an illness or injury, when participating in bungy jumping, the insurance policy as stipulated in article 3 shall not apply to me and that I shall be liable for my own acts (and omissions), accordingly.</td>
    </tr>
    <tr>
        <td class="en_waiver_table_number">3.
        <td>I understand that SM provides the following insurance cover for participating in bungy jumping: Death or residual disability: 10 million JPY Hospital confinement: 4,000 JPY per day (up to 180 days) Hospital visits: 2,000 JPY per day (up to 90 days).
            <br>I also understand and acknowledge that this insurance policy does not include the costs of medical care, transportation or property damage.
        </td>
    </tr>
    <tr>
        <td class="en_waiver_table_number">4.
        <td>I agree to observe and obey all posted rules, to follow any instructions or directions given by SM through its staff, and to be fully aware of all safety matters. I also understand and acknowledge that if I do not observe and obey the said rules and instructions given by SM, SM has the right to cancel my participation in bungy jumping, and that I shall be liable for my own actions (and omissions), accordingly.</td>
    </tr>
    <tr>
        <td class="en_waiver_table_number">5.
        <td>I grant a complete and unconditional release of all liability for SM and its staff due to act of god (including unpredicted natural phenomenon).</td>
    </tr>
    <tr>
        <td class="en_waiver_table_number">6.
        <td>I also understand and acknowledge that I shall be liable to pay the repair and/or replacement costs for any damage that is caused to SM property, in whole or part, by my intentional acts or omissions, or my failure to observe the rules and instructions given by SM and its staff to any facilities or equipment operated and managed by SM.</td>
    </tr>
    <tr>
        <td class="en_waiver_table_number">7.
        <td>I consent to medical care and transportation as SM or medical professions may deem appropriate in order to obtain treatment in the event of my falling ill or become injured during my participation in the bungy jumping.</td>
    </tr>
    <tr>
        <td class="en_waiver_table_number">8.
        <td>I consent to the SM’s use of my image at no cost in photographs, motion pictures or recordings taken at any SM’s bungy jumping sites for use in SM’s advertising, marketing or promotion throughout the world.</td>
    </tr>
    <tr>
        <td class="en_waiver_table_number">9.
        <td>I agree that SM reserves the right to cancel bungy jumping in the event of inclement weather, natural or man-made disasters, acts of god or for any other reason that SM deems, at its discretion, necessary for safety and security of participants or for difficulty of operating and managing bungy jumping.</td>
    </tr>
</table>
</span>

        <!--<p class="numerable">1. PARTIES INCLUDED: The participant understands that this agreement includes the organizers and their partners, employees, instructors, agents, the owners of the land, bridges utilized for bungy jumping, or its employees, designers and engineers of equipment or systems used.</p>

        <p class="numerable">2. ASSUMPTION OF RISK: The participant is fully aware that bungy jumping and all associated activity contain inherent risks and dangers. The participant understands the scope, nature, and extent of the risks involved and voluntarily and freely chooses to incur any and all such risks and dangers.</p>

        <p class="numerable">3. EXEMPTION FROM LIABILITY: The participant hereby fully and forever discharges and releases the organizers from any and all liability, claims, demands, actions, and causes of action whatsoever arising out of any damages sustained by the participant. Exemption from liability includes loss, damage, or injury resulting from the negligence of the organizers or from any other cause or causes.</p>

        <p class="numerable">4. COVENANT NOT TO SUE: The participant agrees, for him/herself and his/her heirs, executors or administrators, or assigns to indemnify and hold harmless the organizers nor to initiate or assist the prosecution of any claim for damages.</p>

        <p class="numerable">5. INDEMNITY AGREEMENT: The participant agrees for him/herself and his/her heirs, executors, assigns or administrators to hold harmless the organizers from any and all losses, claims, actions or proceedings of any kind which may be initiated by the participant and/or any other person or organization. This includes reimbursement of all legal costs and reasonable counsel fees incurred by the organizers. The agreement shall be effective not only for the participant.s first bungy jump or related activity but for any subsequent bungy jumps.</p>

        <p class="numerable">6. INSURANCE DISCLAIMER: I understand that the organizers provide medical and liability insurance for incidents or accidents which may arise as a result of my participation in any phase of Bungy jumping to cover medical treatment / hospitalization and death.</p>

        <p class="numerable">7. MEDICAL DISCLAIMER: I certify that I do not suffer from any conditions which may endanger the safety of myself or anyone else, by participating in bungy jumping. I further certify that I do not suffer from any of the following conditions: extreme asthma, epilepsy, a cardio/respiratory disorder, hypertension, any skeletal or joint or ligament problem or conditions. I further certify that I am neither pregnant nor in an intoxicated state.</p>

        <p>I hereby expressly recognize that this agreement is a contract pursuant to which I have released any and all claims against the organizers resulting from my participation in bungy jumping including any claims caused by the negligence of the organizers. I have read this agreement carefully and fully understand its contents and sign it of my own free will. I further certify that I am eighteen (18) years of age or older or I am 13 years or older and I am obtaining parental acceptance.</p>
              </span>
          -->
<?php if ($site_id > 2): ?>  
<p style="font-weight:bold; color: white; font-size: 20px;">
    私は、スタンダード・ムーブ株式会社（以下「主催者」といいます）が主催・催行するバンジージャンプを行うにあたり、この確認書に記載されている条項（１－１１）をよく読み、保険の内容やバンジージャンプの危険性、主催者の権利などを理解し、了承し、同意した上で、自筆の署名をいたします。
</p>
<?php else: ?>
<p style="font-weight:bold; color: white; font-size: 20px;">
    私は、みなかみ町観光協会が主催し、スタンダード・ムーブ株式会社（以下「主催者」といいます）が主催・催行するバンジージャンプを行うにあたり、この確認書に記載されている条項（１－１１）をよく読み、保険の内容やバンジージャンプの危険性、主催者の権利などを理解し、了承し、同意した上で、自筆の署名をいたします。
</p>  
<?php endif; ?>    


<style>
    #ja_waiver_table tr td {
        padding-bottom: 10px;
        vertical-align: top;
        font-size: 20px;
        line-height: 22px;
        font-weight: 100;
        color: white;
        font-weight: 100;
    }

    .ja_waiver_table_number {
        font-size: 20px;
        line-height: 8px;
        font-weight: 100;
    }
</style>

<table id="ja_waiver_table">
    <tr>
        <td class="ja_waiver_table_number">１.
        <td>私が参加する
            <nobr>バンジージャンプ</nobr>
            <nobr>は</nobr>
            、
            <nobr>その性質上、</nobr>
            <nobr>死傷する危険</nobr>
            があることを
            <nobr>十分に</nobr>
            <nobr>認識しています</nobr>
            。
        </td>
    </tr>
    <tr>
        <td class="ja_waiver_table_number">２.</td>
        <td>私は、主催者が定めた参加条件に
            <nobr>反しておりません</nobr>
            。私は、
            <nobr>バンジージャンプが</nobr>
            <br>
            <nobr>影響</nobr>
            を及ぼす可能性のある症状や怪我を
            <nobr>有しておりません</nobr>
            。私は、
            <nobr>飲酒、</nobr>
            <nobr>酒気帯び、</nobr>
            <nobr>妊娠もしておりません。</nobr>
            私は、私が
            <nobr>上記</nobr>
            の
            <nobr>症状や怪我を隠ぺいして</nobr>
            <nobr>バンジージャンプ</nobr>
            <nobr>を行った</nobr>
            <nobr>場合には、</nobr>
            <nobr>応分の責任</nobr>
            を負
            <nobr>わねばならず、</nobr>
            <nobr>保険は適用されない</nobr>
            ことを
            <nobr>理解しています。</nobr>
        </td>
    </tr>
    <tr>
        <td class="ja_waiver_table_number">３.</td>
        <?php if ($site_id > 2): ?>
        <td>私は、主催者が加入している保険が以下の内容（補償金額）であることを理解しています。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;死亡・後遺障害1,000万円<br>私は、保険には実際に支払った医療費や交通費等に
            <nobr>対する</nobr>
            <nobr>補償、</nobr>
            <nobr>また物損に対する</nobr>
            <nobr>補償がないことも</nobr>
            <nobr>理解しています。</nobr>
        </td>
        <?php else: ?>
        <td>私は、主催者が加入している保険が以下の内容（補償金額）であることを理解しています。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;死亡・後遺障害1,000万円　入院4,000円（180日限度）通院2,000円（90日限度）<br>私は、保険には実際に支払った医療費や交通費等に
            <nobr>対する</nobr>
            <nobr>補償、</nobr>
            <nobr>また物損に対する</nobr>
            <nobr>補償がないことも</nobr>
            <nobr>理解しています。</nobr>
        </td>
        <?php endif; ?>
    </tr>
    <tr>
        <td class="ja_waiver_table_number">４.</td>
        <td>私は、主催者が設けた規則やそのスタッフの指示に従い、安全を心がけなければならないということを十分に理解しています。私は、主催者が設けた規則や指示を無視して行った行動に対しては、応分の責任を負わなければならず、主催者が、規則や指示に従わない参加者に対し、
            <nobr>バンジージャンプ</nobr>
            への
            <nobr>参加</nobr>
            を取り消す権利を有していることも、
            <nobr>理解しています。</nobr>
        </td>
    </tr>
    <tr>
        <td class="ja_waiver_table_number">５.</td>
        <td>私は、予期し得ぬ自然現象など、不可避的な事象によって引き起こされた事故については、主催者とそのスタッフに対して、
            <nobr>責任を問うことはしません。</nobr>
        </td>
    </tr>
    <tr>
        <td class="ja_waiver_table_number">６.</td>
        <td>私は、故意により、または主催者が設けた規則もしくは指示に反した行動により、主催者の管理する施設や備品などに損傷を与えた場合、その修理費を負担することを了承します。</td>
    </tr>
    <tr>
        <td class="ja_waiver_table_number">７.</td>
        <td>私は、バンジージャンプへ参加した結果、怪我や病気になった場合、主催者の判断により医療機関での治療を受け、
            <nobr>緊急</nobr>
            の
            <nobr>場合</nobr>
            には
            <nobr>主催者また</nobr>
            は
            <nobr>医師</nobr>
            が
            <nobr>応急処置</nobr>
            を施すことに同意いたします。
        </td>
    </tr>
    <tr>
        <td class="ja_waiver_table_number">８.</td>
        <td>私は、バンジージャンプへの参加にあたり撮影された私の写真や映像の肖像権を放棄し、それらを全世界において広告宣伝目的、
            <nobr>プロモーション</nobr>
            <nobr>活動</nobr>
            などで
            <nobr>使用する</nobr>
            <nobr>権利が主催者</nobr>
            に無償で
            <nobr>帰属する</nobr>
            <nobr>ことに</nobr>
            <nobr>
                <nobr>同意いたします。</nobr>
            </nobr>
        </td>
    </tr>
    <tr>
        <td class="ja_waiver_table_number">９.</td>
        <td>私は、天災、
            <nobr>天候不良等不可抗力の理由</nobr>
            <nobr>により、</nobr>
            <nobr>主催者</nobr>
            が
            <nobr>バンジージャンプ</nobr>
            の
            <nobr>実施もしく</nobr>
            は
            <nobr>運営に支障があり、</nobr>
            又は
            <nobr>安全</nobr>
            <nobr>確保に</nobr>
            <nobr>支障がある</nobr>
            と
            <nobr>判断した場合、</nobr>
            <nobr>バンジージャンプ</nobr>
            の
            <nobr>実施が中止となる</nobr>
            <nobr>ことに</nobr>
            <nobr>同意いたします。</nobr>
        </td>
    </tr>
</table>
</span>
    </div>
    <div id="agree-exit-container">
        <div id="agree" style="padding-top: 10px;">
            <button>
                <span class="lang_en">I AGREE</span>
                <span class="lang_ja">同意する</span>
            </button>
        </div>
        <div id="exit" style="padding-top: 10px;">
            <button>
                <span class="lang_en">EXIT</span>
                <span class="lang_ja">同意しない</span>
            </button>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step5" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">please enter the year you were born</span>
                <span class="lang_ja">生まれた年を西暦でご入力ください。</span>
            </h1>
        </div>
    </div>
    <div id="content-container">
        <div id="photo-number" style="margin-left:320px;"><input name="born_year"></div>
    </div>
    <div id="photo-keyboard-container">
        <div class="photo-keyboard">
            <div class="keyboard-button">
                <button>1</button>
            </div>
            <div class="keyboard-button">
                <button>2</button>
            </div>
            <div class="keyboard-button">
                <button>3</button>
            </div>
            <div class="keyboard-button">
                <button>4</button>
            </div>
            <div class="keyboard-button">
                <button>5</button>
            </div>
            <div class="keyboard-button">
                <button>6</button>
            </div>
            <div class="keyboard-button">
                <button>7</button>
            </div>
            <div class="keyboard-button">
                <button>8</button>
            </div>
            <div class="keyboard-button">
                <button>9</button>
            </div>
            <div class="keyboard-button">
                <button>0</button>
            </div>
        </div>
        <div id="enter-del-container">
            <div id="del">
                <button><span class="lang_en">DELETE</span><span class="lang_ja">訂正</span></button>
            </div>
            <div id="enter">
                <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
            </div>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step6" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">please enter your birthday</span>
                <span class="lang_ja">誕生日をご入力ください。</h1>
        </div>
    </div>
    <div id="birth-month-container">
        <div id="birth-month">
            <div id="birth-month-value"><input name="birth_month"></div>
        </div>
        <div class="keyboard-container">
            <div class="keyboard-button lang_en">
                <button>JAN</button>
            </div>
            <div class="keyboard-button lang_en">
                <button>FEB</button>
            </div>
            <div class="keyboard-button lang_en">
                <button>MAR</button>
            </div>
            <div class="keyboard-button lang_en">
                <button>APR</button>
            </div>
            <div class="keyboard-button lang_en">
                <button>MAY</button>
            </div>
            <div class="keyboard-button lang_en">
                <button>JUN</button>
            </div>
            <div class="keyboard-button lang_en">
                <button>JUL</button>
            </div>
            <div class="keyboard-button lang_en">
                <button>AUG</button>
            </div>
            <div class="keyboard-button lang_en">
                <button>SEP</button>
            </div>
            <div class="keyboard-button lang_en">
                <button>OCT</button>
            </div>
            <div class="keyboard-button lang_en">
                <button>NOV</button>
            </div>
            <div class="keyboard-button lang_en">
                <button>DEC</button>
            </div>
            <div class="keyboard-button lang_ja">
                <button>1月</button>
            </div>
            <div class="keyboard-button lang_ja">
                <button>2月</button>
            </div>
            <div class="keyboard-button lang_ja">
                <button>3月</button>
            </div>
            <div class="keyboard-button lang_ja">
                <button>4月</button>
            </div>
            <div class="keyboard-button lang_ja">
                <button>5月</button>
            </div>
            <div class="keyboard-button lang_ja">
                <button>6月</button>
            </div>
            <div class="keyboard-button lang_ja">
                <button>7月</button>
            </div>
            <div class="keyboard-button lang_ja">
                <button>8月</button>
            </div>
            <div class="keyboard-button lang_ja">
                <button>9月</button>
            </div>
            <div class="keyboard-button lang_ja">
                <button>10月</button>
            </div>
            <div class="keyboard-button lang_ja">
                <button>11月</button>
            </div>
            <div class="keyboard-button lang_ja">
                <button>12月</button>
            </div>
        </div>
    </div>
    <div id="birth-day-container">
        <div id="birth-day">
            <div id="bidth-day-value"><input name="birth_day"></div>
            <div id="birth-day-del-button">
                <button><span class="lang_en">DEL</span><span class="lang_ja">訂正</span></button>
            </div>
        </div>
        <div class="keyboard-container">
            <div class="keyboard-button">
                <button>1</button>
            </div>
            <div class="keyboard-button">
                <button>2</button>
            </div>
            <div class="keyboard-button">
                <button>3</button>
            </div>
            <div class="keyboard-button">
                <button>4</button>
            </div>
            <div class="keyboard-button">
                <button>5</button>
            </div>
            <div class="keyboard-button">
                <button>6</button>
            </div>
            <div class="keyboard-button">
                <button>7</button>
            </div>
            <div class="keyboard-button">
                <button>8</button>
            </div>
            <div class="keyboard-button">
                <button>9</button>
            </div>
            <div class="keyboard-button">
                <button>0</button>
            </div>
        </div>
    </div>
    <div id="enter-container">
        <div id="enter">
            <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step7" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">please select your sex</span>
                <span class="lang_ja">性別をお選びください。</span>
            </h1>
        </div>
    </div>
    <div id="male-female-container">
        <div id="male-button">
            <button>
                <span class="lang_en">MALE</span>
                <span class="lang_ja">男性</span>
            </button>
        </div>
        <div id="female-button">
            <button>
                <span class="lang_en">FEMALE</span>
                <span class="lang_ja">女性</span>
            </button>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step8" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en"><span style="font-size: 80px;">please enter your</span> last name</span>
                <span class="lang_ja">ローマ字で姓字をご入力ください。</span>
            </h1>
        </div>
    </div>
    <div id="name-container">
        <div id="name-input-container"><input name="lastname"></div>
        <div id="del-button">
            <button><span class="lang_en">DEL</span><span class="lang_ja">訂正</span></button>
        </div>
        <div id="enter-button">
            <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
        </div>
    </div>
    <div id="keyboard-container">
        <?php
        $keyboard = array(
            array('q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'),
            array('a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l'),
            array('z', 'x', 'c', 'v', 'b', 'n', 'm')
        );
        foreach ($keyboard as $line) {
            echo '<div class="keyboard-line">';
            foreach ($line as $letter) {
                echo "<div class=\"keyboard-button\"><button>" . strtoupper($letter) . "</button></div>";
            };
            echo "</div>";
        };
        ?>
        <div class="keyboard-line">
            <div class="keyboard-button lang_en" id="space">
                <button>SPACE</button>
            </div>
            <div class="keyboard-button lang_ja" id="space">
                <button>スペース</button>
            </div>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step9" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en"><span style="font-size: 80px;">please enter your</span> first name</span>
                <span class="lang_ja">ローマ字で下の名前をご入力ください。</span>
            </h1>
        </div>
    </div>
    <div id="name-container">
        <div id="name-input-container"><input name="firstname"></div>
        <div id="del-button">
            <button><span class="lang_en">DEL</span><span class="lang_ja">訂正</span></button>
        </div>
        <div id="enter-button">
            <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
        </div>
    </div>
    <div id="keyboard-container">
        <?php
        $keyboard = array(
            array('q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'),
            array('a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l'),
            array('z', 'x', 'c', 'v', 'b', 'n', 'm')
        );
        foreach ($keyboard as $line) {
            echo '<div class="keyboard-line">';
            foreach ($line as $letter) {
                echo "<div class=\"keyboard-button\"><button>" . strtoupper($letter) . "</button></div>";
            };
            echo "</div>";
        };
        ?>
        <div class="keyboard-line">
            <div class="keyboard-button lang_en" id="space">
                <button>SPACE</button>
            </div>
            <div class="keyboard-button lang_ja" id="space">
                <button>スペース</button>
            </div>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step10" style="display: none;">
    <div class="header-container">
        <div class="head-image-container"><h1>
                <span class="lang_en">Where do you live?</span>
                <span class="lang_ja">お住まいの都道府県を選択してください。</span></h1></div>
    </div>
    <div id="place-container">
        <?php
        $perfs = array(
            array(
                "北海道・Hokkaido",
                "",
                array(
                    "type" => 'title',
                    "text" => "東北・Tohoku",
                ),
                "青森県・Aomori",
                "岩手県・Iwate",
                "宮城県・Miyagi",
                "秋田県・Akita",
                "山形県・Yamagata",
                "福島県・Fukushima"
            ),
            array(
                array(
                    "type" => 'title',
                    "text" => "関東・Kanto",
                ),
                "茨城県・Ibaraki",
                "栃木県・Tochigi",
                "群馬県・Gunma",
                "埼玉県・Saitama",
                "千葉県・Chiba",
                "東京都・Tōkyō",
                "神奈川県・
Kanagawa",
                "",
                "",
                "",
                array(
                    "cols" => 3,
                    "text" => "外国・I am a visitor from overseas",
                )
            ),
            array(
                array(
                    "type" => 'title',
                    "text" => "中部・Chūbu",
                ),
                "新潟県・Niigata",
                "富山県・Toyama",
                "石川県・Ishikawa",
                "福井県・Fukui",
                "山梨県・Yamanashi",
                "長野県・Nagano",
                "岐阜県・Gifu",
                "静岡県・Shizuoka",
                "愛知県・Aichi"
            ),
            array(
                array(
                    "type" => 'title',
                    "text" => "関西・Kansai",
                ),
                "三重県・Mie",
                "滋賀県・Shiga",
                "京都府・Kyōto",
                "大阪府・Ōsaka",
                "兵庫県・Hyōgo",
                "奈良県・Nara",
                "和歌山県・
Wakayama"
            ),
            array(
                array(
                    "type" => 'title',
                    "text" => "中国・Chūgoku",
                ),
                "鳥取県・Tottori",
                "島根県・Shimane",
                "岡山県・Okayama",
                "広島県・Hiroshima",
                "山口県・
Yamaguchi",
                "",
                array(
                    "type" => 'title',
                    "text" => "四国・Shikoku",
                ),
                "徳島県・
Tokushima",
                "香川県・Kagawa",
                "愛媛県・Ehime",
                "高知県・Kōchi"
            ),
            array(
                array(
                    "type" => 'title',
                    "text" => "九州・Kyushu",
                ),
                "福岡県・Fukuoka",
                "佐賀県・Saga",
                "長崎県・Nagasaki",
                "熊本県・
Kumamoto",
                "大分県・Ōita",
                "宮崎県・Miyazaki",
                "鹿児島県・
Kagoshima",
                "",
                "沖縄県・Okinawa"
            )
        );
        foreach ($perfs as $col) {
            echo '<div class="perfs-col">';
            foreach ($col as $perf) {
                switch (TRUE) {
                    case(is_array($perf) && array_key_exists('type', $perf)):
                        echo "<div class=\"perfs-cell\"><div class=\"perfs-title\">" . $perf['text'] . "</div></div>";
                        break;
                    case(is_array($perf) && array_key_exists('cols', $perf)):
                        $margin = 34;
                        $col_width = 170;
                        $cell_width = $perf['cols'] * $col_width + ($perf['cols'] - 1) * $margin;
                        echo "<div class=\"perfs-cell\" style=\"width: {$cell_width}px;\"><button>" . $perf['text'] . "</button></div>";
                        break;
                    case (empty($perf)):
                        echo "<div class=\"perfs-cell\">" . $perf . "</div>";
                        break;
                    default:
                        echo "<div class=\"perfs-cell\"><button>" . $perf . "</button></div>";
                        break;
                };
            };
            echo "</div>";
        };
        ?>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step11" style="display: none;">
    <div class="header-container">
        <div class="head-image-container"><img src="img/step11_header.jpg"/></div>
    </div>
    <div id="street-container">
        <div id="street1-input-container"><input name="street1"></div>
        <div id="street2-input-container"><input name="street2"></div>
        <div id="street3-input-container"><input name="street3"></div>
        <div id="street4-input-container"><input name="street4"></div>
    </div>
    <div id="keyboard-container">
        <?php
        $keyboard = array(
            array('1', '2', '3', '4', '5', '6', '7', '8', '9', '0'),
            array('q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'),
            array('a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', '-'),
            array('z', 'x', 'c', 'v', 'b', 'n', 'm', '.')
        );
        $i = 0;
        foreach ($keyboard as $line) {
            echo '<div class="keyboard-line' . $i . '">';
            foreach ($line as $letter) {
                echo "<div class=\"keyboard-button\"><button>" . strtoupper($letter) . "</button></div>";
            };
            echo "</div>";
            $i++;
        };
        ?>
        <div class="keyboard-line">
            <div class="keyboard-button lang_en" id="space">
                <button>SPACE</button>
            </div>
            <div class="keyboard-button lang_ja" id="space">
                <button>スペース</button>
            </div>
        </div>
    </div>
    <div id="enter-del-container">
        <div id="enter">
            <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
        </div>
        <div id="del">
            <button><span class="lang_en">DELETE</span><span class="lang_ja">訂正</span></button>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step12" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en"><span style="font-size: 80px;">please enter your</span> phone number</span>
                <span class="lang_ja">電話番号をご入力ください。</span>
            </h1>
        </div>
    </div>
    <div id="content-container">
        <div id="phone-number"><input name="phone_number"></div>
    </div>
    <div id="keyboard-container">
        <?php
        $keyboard = array(
            array('1', '2', '3', '4', '5'),
            array('6', '7', '8', '9', '0')
        );
        foreach ($keyboard as $line) {
            echo '<div class="keyboard-line">';
            foreach ($line as $letter) {
                echo "<div class=\"keyboard-button\"><button>" . strtoupper($letter) . "</button></div>";
            };
            echo "</div>";
        };
        ?>
        <div id="enter-del-container">
            <div id="del">
                <button><span class="lang_en">DELETE</span><span class="lang_ja">訂正</span></button>
            </div>
            <div id="enter">
                <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
            </div>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step13" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">Please enter your email address</span><span class="lang_ja">メールアドレスをご入力ください。</span>
            </h1></div>
        <div id="email-note">
            <span class="lang_en">A customer survey will be sent to the email address provided <br/>and those that complete it will be entered in a monthly draw for 2 free jump tickets!</span>
				<span class="lang_ja">アンケートをメールにて送信しますので、ご協力をお願いいたします。<br/>
お答えいただいた方に毎月抽選で無料ジャンプ券2枚プレゼント！</span>
        </div>
    </div>
    <!--div id="rather-not">
      <button><span class="lang_en">No, thank you</span><span class="lang_ja">入力不要</span></button>
    </div-->
    <div id="email-container">
        <div id="email-input-container"><input name="email"></div>
    </div>
    <div id="keyboard-container">
        <?php
        $keyboard = array(
            array('1', '2', '3', '4', '5', '6', '7', '8', '9', '0'),
            array('q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'),
            array('a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', '-'),
            array('z', 'x', 'c', 'v', 'b', 'n', 'm', '.', '@', '_')
        );
        $i = 0;
        foreach ($keyboard as $line) {
            echo '<div class="keyboard-line' . $i . '">';
            foreach ($line as $letter) {
                echo "<div class=\"keyboard-button\"><button>" . strtoupper($letter) . "</button></div>";
            };
            echo "</div>";
            $i = '';
        };
        ?>
    </div>
    <div id="enter-del-container">
        <div id="enter">
            <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
        </div>
        <div id="del">
            <button><span class="lang_en">DELETE</span><span class="lang_ja">訂正</span></button>
        </div>
    </div>
    <div id="domains-container">
        <div class="domains-row">
            <div class="domains-cell first-cell">
                <button>.com</button>
            </div>
            <div class="domains-cell second-cell">
                <button>@i.softbank.jp</button>
            </div>
            <div class="domains-cell third-cell">
                <button>@yahoo.co.jp</button>
            </div>
        </div>
        <div class="domains-row">
            <div class="domains-cell first-cell">
                <button>.ne.jp</button>
            </div>
            <div class="domains-cell second-cell">
                <button>@gmail.com</button>
            </div>
            <div class="domains-cell third-cell">
                <button>@hotmail.ne.jp</button>
            </div>
        </div>
        <div class="domains-row">
            <div class="domains-cell first-cell">
                <button>.co.jp</button>
            </div>
            <div class="domains-cell second-cell">
                <button>@ezweb.ne.jp</button>
            </div>
            <div class="domains-cell third-cell">
                <button>@docomo.ne.jp</button>
            </div>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step14" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">Please sign your name using your finger</span>
                <span class="lang_ja">画面に直接ご署名ください。</span>
            </h1>
        </div>
    </div>
    <div id="sig-container">
        <canvas id="waiver-signature" height="600" width="1000"/>
    </div>
    <div id="actions-container">
        <div id="del-button">
            <button><span class="lang_en">DELETE</span><span class="lang_ja">訂正</span></button>
        </div>
        <div id="enter-button">
            <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ<//span></button>
        </div>
    </div>

    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step15" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">Thank you. Registration complete!</span>
                <span class="lang_ja">登録が完了しました。</span>
            </h1>

            <h2>
                <span class="lang_en">Please give your name to the reception staff.</span>
          <span class="lang_ja">お客様の氏名をスタッフまで<br/>
お申し付けください。</span>
            </h2>
        </div>
    </div>
    <div class="restart-container">
        <!--div>This form will restart in <span id="restart-in">5</span> seconds</div-->
        <div id="step15progress"></div>
    </div>
</div>
</div>
<div id="info-dialog" style="display: none;"></div>
</body>
</html>
