<div id="step7" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">please select your sex</span>
                <span class="lang_ja">性別をお選びください。</span>
            </h1>
        </div>
    </div>
    <div id="male-female-container">
        <div id="male-button">
            <button>
                <span class="lang_en">MALE</span>
                <span class="lang_ja">男性</span>
            </button>
        </div>
        <div id="female-button">
            <button>
                <span class="lang_en">FEMALE</span>
                <span class="lang_ja">女性</span>
            </button>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
