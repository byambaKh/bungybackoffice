<div id="step11" style="display: none;">
    <div class="header-container">
        <div class="head-image-container"><img src="img/step11_header.jpg"/></div>
    </div>
    <div id="street-container">
        <div id="street1-input-container"><input name="street1"></div>
        <div id="street2-input-container"><input name="street2"></div>
        <div id="street3-input-container"><input name="street3"></div>
        <div id="street4-input-container"><input name="street4"></div>
    </div>
    <div id="keyboard-container">
        <?php
        $keyboard = array(
            array('1', '2', '3', '4', '5', '6', '7', '8', '9', '0'),
            array('q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'),
            array('a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', '-'),
            array('z', 'x', 'c', 'v', 'b', 'n', 'm', '.')
        );
        $i = 0;
        foreach ($keyboard as $line) {
            echo '<div class="keyboard-line' . $i . '">';
            foreach ($line as $letter) {
                echo "<div class=\"keyboard-button\"><button>" . strtoupper($letter) . "</button></div>";
            };
            echo "</div>";
            $i++;
        };
        ?>
        <div class="keyboard-line">
            <div class="keyboard-button lang_en" id="space">
                <button>SPACE</button>
            </div>
            <div class="keyboard-button lang_ja" id="space">
                <button>スペース</button>
            </div>
        </div>
    </div>
    <div id="enter-del-container">
        <div id="enter">
            <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
        </div>
        <div id="del">
            <button><span class="lang_en">DELETE</span><span class="lang_ja">訂正</span></button>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>