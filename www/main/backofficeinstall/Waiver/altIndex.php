<?php
include "../includes/application_top.php";
//error_reporting(E_ALL);
/*
This file contains the entire waiver sign in process. Eugh. Here is how this monster works.
- We bind every button to a function. Some of these functions save data. Almost all of them trigger a goto statement.
- The goto statements hide or reveal elements on the page based on their parent div "stepX". 
In this way at each step the relevant elements are visible and everything else is hidden.
-
*/
require_once('functions.php');
$max_steps = 17;
$site_id = (int)CURRENT_SITE_ID;

if (!isset($_GET['step'])) {
    $_GET['step'] = 1;
};
if ($_GET['step'] < 1 || $_GET['step'] > $max_steps) {
    $_GET['step'] = 1;
};
$lang = 'en';
if (array_key_exists('lang', $_GET) && in_array($_GET['lang'], array('en', 'ja'))) {
    $lang = $_GET['lang'];
};
$pc_version = true;
if (preg_match("/iPad/i", $_SERVER['HTTP_USER_AGENT'])) {
    $pc_version = false;
};

$bookingNames = getNames();

?>
<!DOCTYPE html>
<html style="zoom:90%;">
<head>
    <?php include 'includes/header_tags.php'; ?>
    <title>Bungy Japan Waiver</title>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="viewport" content="initial-scale=0.8,user-scalable=no,maximum-scale=1,height=device-height">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <?php include "../includes/head_scripts.php"; ?>
    <script src="js/jquery.event.move.js"></script>
    <link rel="stylesheet"
          href="/Waiver/css/stylesheet.css?v=3"
          type="text/css" media="all"/>
    <script>
        var pc_version = <?php echo (int)$pc_version; ?>;
        var site_id = <?php echo $site_id; ?>;

        function scrollend(e) {
            document.lastScroll = null;
        }

        function scrollmove(e) {
            var targetEvent = e.originalEvent;
            if (typeof document.lastScroll == 'undefined' || document.lastScroll === null) {
                document.lastScroll = targetEvent.pageY;
            }
            ;
            var el = $(this).children();
            var old_scroll = $(el).prop('scroll');
            if (typeof old_scroll == 'undefined') {
                old_scroll = 0;
            }
            ;
            var scroll = old_scroll - targetEvent.pageY + document.lastScroll;
            if (scroll < 0) {
                scroll = 0;
            }
//alert($(el).height() + ' ' + $(el).parent().height() + ' ' + scroll);
            if (scroll > ($(el).height() - $(el).parent().height())) {
                scroll = $(el).height() - $(el).parent().height();
            }
            ;
            $(el).css('webkitTransform', 'translate(0px,-' + scroll + 'px)');
            $(el).prop('scroll', scroll);
            document.lastScroll = targetEvent.pageY;
        }

        // scrolling disable
        document.addEventListener('touchmove', function (e) {
            e.preventDefault();
        });

        function select_language(lang) {
            document.bungywaiver_lang = lang;
            $('.lang_en, .lang_ja').css('display', 'none');
            $('.lang_' + lang).css('display', 'block');
        }

        $(document).ready(function () {
            if (pc_version == 0) {
                $('.scrollable').bind('touchmove', scrollmove);
                $('.scrollable').bind('touchend', scrollend);
            }
            ;
            save_var('site_id', site_id);
            $('input').prop('disabled', 'disabled');
            goto_step(<?php echo (int)$_GET['step']; ?>);
            select_language('<?php echo $lang; ?>');
            $(".back button").bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                document.current_step--;
                if (document.current_step == 11) {
                    document.current_step--;
                }
                ;
                goto_step(document.current_step);
            });
            $('#english-container button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                select_language('en');
                goto_step(2);
            });
            $('#japanese-container button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                select_language('ja');
                goto_step(2);
            });
            //make the li list for the iPad booking name selection, selectable
            $('#step2 li').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                $('#step2 li').removeClass('selected');
                $(this).addClass('selected');
            });

            $('#step2 .refresh button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                return fill_names();
            });
            $('#step2 #bottom-container button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                var selected = false;
                if (pc_version == 0) {
                    $('#step2 li').each(function () {
                        if ($(this).hasClass('selected')) selected = $(this).prop('id');
                    });
                } else {
                    selected = $("#step2 select").val();
                    if (typeof selected == 'undefined') {
                        selected = false;
                    }
                }
                ;
                if (selected) {
                    save_var('booking_id', selected);
                    goto_step(3);
                } else {
                    show_info("bookingname");
                    save_var('booking_id', 'null');
                }
                ;
            });
            $('#step3 .keyboard-button button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                if ($('#step3 #photo-number input').val().length == 3) return;
                $('#step3 #photo-number input').val('' + $('#step3 #photo-number input').val() + $(this).html());
            });
            $('#step3 #del  button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                var val = $('#step3 #photo-number input').val();
                if (val.length > 0) {
                    $('#step3 #photo-number input').val(val.substr(0, val.length - 1));
                }
                ;
            });
            $('#step3 #photo-dho  button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                save_var('photo_number', null);
                goto_step(4);
            });
            $('#step3 #enter  button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                var val = $('#step3 #photo-number input').val();
                if (val.length > 0) {
                    save_var('photo_number', val);
                    goto_step(4);
                } else {
                    show_info("validphoto");
                }
                ;
            });
            $('#step4 #agree  button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                save_var('agree', 1);
                goto_step(5);
            });
            $('#step4 #exit  button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                document.bj_waiver = null;
                goto_step(1);
            });
            $('#step5 .keyboard-button button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                if ($('#step5 #photo-number input').val().length == 4) return;
                $('#step5 #photo-number input').val('' + $('#step5 #photo-number input').val() + $(this).html());
            });
            $('#step5 #del  button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                var val = $('#step5 #photo-number input').val();
                if (val.length > 0) {
                    $('#step5 #photo-number input').val(val.substr(0, val.length - 1));
                }
                ;
            });
            $('#step5 #enter  button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                var val = $('#step5 #photo-number input').val();
                if (val.length > 0) {
                    if (val > 1900) {
                        save_var('birth_year', val);
                        goto_step(6);
                    } else {
                        show_info("yeargreater");
                    }
                } else {
                    show_info("validyear");
                }
                ;
            });
            $('#step6 #birth-month-container button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                $('#step6 #birth-month input').val($(this).html());
            });
            $('#step6 #birth-day-container .keyboard-container button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                if ($('#step6 #birth-day input').val().length == 2) return;
                $('#step6 #birth-day input').val('' + $('#step6 #birth-day input').val() + $(this).html());
            });
            $('#step6 #birth-day-del-button button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                var val = $('#step6 #birth-day input').val();
                if (val.length > 0) {
                    $('#step6 #birth-day input').val(val.substr(0, val.length - 1));
                }
                ;
            });
            $('#step6 #enter button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                var bmonth = $('#step6 #birth-month input').val();
                var bday = $('#step6 #birth-day input').val();
                var error = false;
                if (bmonth.length == 0) {
                    show_info("validmonth");
                    error = true;
                }
                ;
                if (!error && bday.length == 0) {
                    show_info("validday");
                    error = true;
                }
                ;
                if (!error && bday > 31) {
                    show_info("validday31");
                    error = true;
                }
                ;
                if (!error) {
                    save_var('birth_month', bmonth);
                    save_var('birth_day', bday);
                    goto_step(7);
                }
                ;
            });
            $('#step7 #male-button button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                save_var('sex', 'male');
                goto_step(8);
            });
            $('#step7 #female-button button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                save_var('sex', 'female');
                goto_step(8);
            });
            $('#step8 .keyboard-button button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                $('#step8 #name-input-container input').val('' + $('#step8 #name-input-container input').val() + $(this).html().replace('SPACE', ' ').replace('スペース', ' '));
                var element = $('#step8 #name-input-container input')[0];
                if (element.setSelectionRange) {
                    element.setSelectionRange(element.value.length, element.value.length); // scrolls to end
                    element.focus(); // makes caret visible
                }
                ;
            });
            $('#step8 #del-button  button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                var val = $('#step8 #name-input-container input').val();
                if (val.length > 0) {
                    $('#step8 #name-input-container input').val(val.substr(0, val.length - 1));
                }
                ;
                var element = $('#step8 #name-input-container input')[0];
                if (element.setSelectionRange) {
                    element.setSelectionRange(element.value.length, element.value.length); // scrolls to end
                    element.focus(); // makes caret visible
                }
                ;
            });
            $('#step8 #enter-button  button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                var val = $('#step8 #name-input-container input').val();
                if (val.length > 0) {
                    save_var('last_name', val);
                    goto_step(9);
                } else {
                    show_info("validlast");
                }
                ;
            });
            $('#step9 .keyboard-button button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                $('#step9 #name-input-container input').val('' + $('#step9 #name-input-container input').val() + $(this).html().replace('SPACE', ' ').replace('スペース', ' '));
                var element = $('#step9 #name-input-container input')[0];
                if (element.setSelectionRange) {
                    element.setSelectionRange(element.value.length, element.value.length); // scrolls to end
                    element.focus(); // makes caret visible
                }
                ;
            });
            $('#step9 #del-button  button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                var val = $('#step9 #name-input-container input').val();
                if (val.length > 0) {
                    $('#step9 #name-input-container input').val(val.substr(0, val.length - 1));
                }
                ;
                var element = $('#step9 #name-input-container input')[0];
                if (element.setSelectionRange) {
                    element.setSelectionRange(element.value.length, element.value.length); // scrolls to end
                    element.focus(); // makes caret visible
                }
                ;
            });
            $('#step9 #enter-button  button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                var val = $('#step9 #name-input-container input').val();
                if (val.length > 0) {
                    save_var('first_name', val);
                    goto_step(10);
                } else {
                    show_info("validfirst");
                }
                ;
            });
            $('#step10 #place-container button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                save_var('prefecture', $(this).html());
                goto_step(11);
            });
            $('#step11 .keyboard-button button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                $('#step11 #street1-input-container input').val('' + $('#step11 #street1-input-container input').val() + $(this).html().replace('SPACE', ' ').replace('スペース', ' '));
                var element = $('#step11 #street1-input-container input')[0];
                if (element.setSelectionRange) {
                    element.setSelectionRange(element.value.length, element.value.length); // scrolls to end
                    element.focus(); // makes caret visible
                }
                ;
            });
            $('#step11 #del  button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                var val = $('#step11 #street1-input-container input').val();
                if (val.length > 0) {
                    $('#step11 #street1-input-container input').val(val.substr(0, val.length - 1));
                }
                ;
                var element = $('#step11 #street1-input-container input')[0];
                if (element.setSelectionRange) {
                    element.setSelectionRange(element.value.length, element.value.length); // scrolls to end
                    element.focus(); // makes caret visible
                }
                ;
            });

            $('#step12 .keyboard-button button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                if ($('#step12 #phone-number input').val().length == 11) return;
                $('#step12 #phone-number input').val('' + $('#step12 #phone-number input').val() + $(this).html());
            });
            $('#step12 #del  button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                var val = $('#step12 #phone-number input').val();
                if (val.length > 0) {
                    $('#step12 #phone-number input').val(val.substr(0, val.length - 1));
                }
                ;
            });
            $('#step12 #enter  button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                var val = $('#step12 #phone-number input').val();
                if (val.length > 8 && val.length < 12) {
                    save_var('phone_number', val);
// ignore medical form screen
                    save_var('med_form', 0);
                    goto_step(13);
                } else {
                    show_info("validphone");
                }
                ;
            });
            /*
             $('#step13 #rather-not button').click(function () {
             $('#step13 input').val('');
             $('#step13 #enter button').trigger('click');
             });
             */
            $('#step13 .keyboard-button button, #step13 #domains-container button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                $('#step13 input').val('' + $('#step13 input').val() + $(this).html().toLowerCase());
            });
            $('#step13 #del button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                var val = $('#step13 input').val();
                if (val.length > 0) {
                    $('#step13 input').val(val.substr(0, val.length - 1));
                }
                ;
            });
            $('#step13 #enter button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                if (!/(.+)@(.+\..+)/.test($("#step13 input").val())) {
                    show_info('validemail');
                    return;
                }
                ;
                save_var('email', $("#step13 input").val());
                goto_step(14);
            });
            $('#step15 #next-button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                goto_step(16);
            });


            $('#waiver-signature').bind('movestart MSPointerDown pointerdown', bjmovestart);
            $('#waiver-signature').bind('move MSPointerMove pointermove', bjmove);
            $('#waiver-signature').bind('moveend MSPointerUp pointerup', bjmoveend);
            //$('#waiver-signature').bind('click', bjmovestart);
            $('#step14 #del-button button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                var c = $('#waiver-signature')[0];
                var ctx = c.getContext('2d');
                var rect = document.getElementById('waiver-signature').getBoundingClientRect();
                ctx.fillStyle = 'white';
                ctx.beginPath();
                ctx.fillRect(0, 0, rect.width, rect.height);
                ctx.stroke();
                document.bj_signature = null;
            });
            $('#step14 #enter-button button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                if (typeof document.bj_signature == 'undefined' || document.bj_signature === null) {
                    alert('Please draw your signature');
                    return false;
                }

                $('#step14 #enter-button button').prop('disabled', true);

                var c = $('#waiver-signature')[0];
                var img = c.toDataURL('image/jpeg', 0.8);
                save_var('sig_img', img);
                goto_step(15);
            });
            $('#step16 .keyboard-button button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                if ($('#step16 #photo-number input').val().length == 3) return;
                $('#step16 #photo-number input').val('' + $('#step16 #photo-number input').val() + $(this).html());
            });
            $('#step16 #del  button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                var val = $('#step16 #photo-number input').val();
                if (val.length > 0) {
                    $('#step16 #photo-number input').val(val.substr(0, val.length - 1));
                }
                ;
            });
            $('#step16 #enter  button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                var val = $('#step16 #photo-number input').val();
                if (val.length > 0) {
                    save_var('weight', val);
                    goto_step(17);
                } else {
                    show_info("weight");
                }
                ;
            });
        });

        function bjmovestart(e) {
            document.bj_last_coords = e;
            bjmove(e);
        }

        function bjmove(e) {
            if (typeof document.bj_last_coords == 'undefined' || document.bj_last_coords === null) {
                document.bj_last_coords = e;
            }
            var c = $('#waiver-signature')[0];
            var ctx = c.getContext('2d');
            var rect = document.getElementById('waiver-signature').getBoundingClientRect();
            ctx.beginPath();
            ctx.moveTo(document.bj_last_coords.pageX - rect.left - window.scrollX, document.bj_last_coords.pageY - rect.top - window.scrollY);
            ctx.lineWidth = 10;
            ctx.lineCap = 'round';
            ctx.strokeStyle = 'blue';
            ctx.lineTo(e.pageX - rect.left - window.scrollX, e.pageY - rect.top - window.scrollY);
            ctx.stroke();
            document.bj_last_coords = e;
            document.bj_signature = true;
        }

        function bjmoveend(e) {
            var c = $('#waiver-signature')[0];
            var ctx = c.getContext('2d');
            document.bj_last_coords = null;
        }

        function save_var(name, value) {
            if (typeof document.bj_waiver == 'undefined' || document.bj_waiver == null) {
                document.bj_waiver = new Object();
            }
            ;
            document.bj_waiver[name] = value;
        }

        function goto_step(step) {
            if (step == 2) {
                fill_names();
                $saved = false;
            }
            ;
            if (step == 11) {
                step = 12;
            }
            ;
            document.current_step = step;
            for (var i = 1; i <= <?php echo $max_steps; ?>; i++) {
                if ($('#step' + i).length) {
                    $('#step' + i).css('display', 'none');
                    if (i == step) {
                        $('#step' + i).css('display', 'block');
                    }
                    ;
                }
                ;
            }
            ;
            if (step == 14) {
                $('#step14 #del-button button').trigger('touchend');
            }
            ;
            // save data, restart form
            if (step == 17) {
                $("#step17progress").css({
                    width: 800,
                    height: 30,
                    "background-color": 'black',
                    border: "2px solid white",
                    'margin-top': '50px',
                    'margin-right': 'auto',
                    'margin-left': 'auto'
                }).html("<div></div>");
                $("#step17progress div").css({
                    width: 0,
                    height: 26,
                    "background-color": 'red'
                });
                var timer_sec = 5;
                document.reload_timer = setInterval(function () {
                    timer_sec--;
                    //$("#restart-in").html(timer_sec);
                    var width = $("#step17progress div").css('width').replace('px', '');
                    if (width == 0) width -= 2;
                    if (width > 600) width -= 2;
                    $("#step17progress div").css('width', parseInt(width) + 160);
                    if (width > 600) {
                        clearInterval(document.reload_timer);
                        document.location = '/Waiver/altIndex.php';
                    }
                    ;
                }, 1000);
            }
            if (step == 17) {
                if ($saved != true) {
                    if (!save_data())//save all data
                    {
                        show_info("errorsaving");
                        goto_step(14);
                        return;
                    }
                    ;
                    $saved = true;
                }
            }
            ;
        }

        function save_data() {
            return $.ajax(
                'ajax.php?action=save-waiver',
                {
                    data: document.bj_waiver,
                    dataType: 'json',
                    type: 'POST',
                    async: false,
                    cache: false,
                    success: function (data) {
                        return true;
                    }
                }
            );
        }

        function fill_names() {
            $.ajax(
                'ajax.php?action=get-names',
                {
                    dataType: 'json',
                    cache: false,
                    success: function (data) {
                        if (data['error']) {
                            show_info(data['error']);
                        } else {
                            if (pc_version == 0) {
                                $('#step2 ul > li').remove();
                            } else {
                                $('#step2 select > option').remove();
                            }
                            ;
                            //$('#step2 select').prop('multiple', false);
                            if (data['data'] && data['data'].length) {
                                for (var i in data['data']) {
                                    if (!data['data'].hasOwnProperty(i)) continue;
                                    if (pc_version == 0) {
                                        var o = $('<li></li>');
                                        o.prop('id', data['data'][i].id);//set the #id as the database id of the booking
                                        o.html(data['data'][i].name);//set the inner html to the name of the booking
                                        o.bind('click touchend MSPointerUp pointerup', function (e) {
                                            e.preventDefault();
                                            $('#step2 li').removeClass('selected');
                                            $(this).addClass('selected');
                                        });
                                        $('#step2 ul').append(
                                            o
                                        );
                                    } else {
                                        var o = $('<option></option>')
                                        $('#step2 select').append(
                                            o.prop('value', data['data'][i].id).prop('text', data['data'][i].name)
                                        );
                                    }
                                    ;
                                }
                            }
                            ;
                            //$('#step2 select').prop('multiple', true);
                        }
                    },
                }
            );
        }

        function show_info(msg) {
            var lang = {
                en: {
                    "bookingname": "You must select the name under which your booking was made.",
                    "validphoto": "You must enter a valid <br /><b>photo number</b><br /> or click<br /> the <b>\"No photos purchased\"</b> button.",
                    "yeargreater": "The year must be greater than 1900.",
                    "validyear": "The year must be greater than 1900.",
                    "validmonth": "Please enter your month of birth.",
                    "validday": "Please enter your day of birth.",
                    "validday31": "Please enter your day of birth.",
                    "validlast": "You must enter your last name to continue.",
                    "validfirst": "You must enter your first name to continue.",
                    "validphone": "Your phone number must contain from 9 to 11 digits.",
                    "validemail": "Pleae enter valid email",
                    "errorsaving": 'some issues saving you credentials',
                    "weight": "Please enter a value for weight"
                },
                ja: {
                    "bookingname": "代表者様のお名前を<br />選択してください。",
                    "validphoto": "お写真の番号を入力するか「ご購入されない方はこちら」のボタンを押してください。",
                    "yeargreater": "１９００年以降でご入力ください。",
                    "validyear": "１９００年以降でご入力ください。",
                    "validmonth": "誕生月を入力してください。",
                    "validday": "誕生日を入力してください。",
                    "validday31": "誕生日を入力してください。",
                    "validlast": "姓字を入力してください。",
                    "validfirst": "名前を入力してください。",
                    "validphone": "9文字～１１文字でご入力ください。",
                    "validemail": 'Pleae enter valid email',
                    "errorsaving": 'some issues saving you credentials',
                    "weight": "Please enter a value for weight"
                }
            }
            if (lang.hasOwnProperty(document.bungywaiver_lang) && lang[document.bungywaiver_lang].hasOwnProperty(msg)) {
                msg = lang[document.bungywaiver_lang][msg];
            }
            ;
            $('#info-dialog').html(msg);
            $('#info-dialog').dialog({
                buttons: [
                    {
                        text: (document.bungywaiver_lang == 'en') ? "Close" : "OK",
                        width: 650,
                        MSPointerUP: function () {
                            $('#info-dialog').dialog("close");
                        },
                        pointerup: function () {
                            $('#info-dialog').dialog("close");
                        },
                        click: function () {
                            $('#info-dialog').dialog("close");
                        },
                        touchend: function () {
                            $('#info-dialog').dialog("close");
                        }
                    }
                ],
                dialogClass: "alert",
                draggable: false,
                modal: true,
                position: {my: "center", at: "center", of: window},
                resizable: false,
                minWidth: 800,
                maxWidth: 1200,
                //html : msg,
                title: (document.bungywaiver_lang == 'en') ? "Information" : "注意"
            });
        }
    </script>
</head>
<body scroll="no">
<?php
if ($pc_version) {
    ?>
    <script>
        $(document).ready(function () {
            $("body").css('padding-top', '10px');
            $(".back").each(function () {
                $(this).css("top", 80 + parseInt($(this).css("top").replace('px', '')));
            });
            $("#domains-container").css("top", 55 + parseInt($("#domains-container").css("top").replace('px', '')));
            //$("#rather-not").css("top", 35 + parseInt($("#rather-not").css("top").replace('px', '')));
            $("#email-note").css("top", 50);
        });
    </script>
    <?php
};
?>
<div>
    <?php
    require("./step_01.php");// Language Selection
    require("./step_02.php");// Select Booking Name
    require("./step_03.php");// Enter Photo Number
    require("./step_04.php");// Waiver
    require("./step_05.php");// Year of Birth
    require("./step_06.php");// Birthday
    require("./step_07.php");// Sex
    require("./step_08.php");// Last Name
    require("./step_09.php");// First Name
    require("./step_10.php");// Where do you Live?
    require("./step_11.php");// Address (not used)
    require("./step_12.php");// Phone Number (not used)
    require("./step_13.php");// Email
    require("./step_14.php");// Signature

    //Alternative Pages for Waiver with ticket printers and manual weight input
    require("./step_15_alt.php");// Return iPad to staff
    require("./step_16_alt.php");// Manually Input Weight
    require("./step_17_alt.php");// Registration Complete
    ?>
    <div id="info-dialog" style="display: none;"></div>
</div>
</body>
</html>

