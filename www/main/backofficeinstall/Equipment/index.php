<?php
	include '../includes/application_top.php';
	include '../Operations/reports_save.php';
// be sure we works with main DB
	if (SYSTEM_SUBDOMAIN != 'TEST') {
		//include '../includes/main_connect.php';//no need to reconnect to the database one was defined in application_top.php
	};
	$can_edit = false;
	if (in_array($_SESSION['access_type'], array('Site Manager', 'General Manager', 'SysAdmin'))) {
		$can_edit = true;
	} else {
		//die(print_r($_SESSION, true));
	};
	if (!defined('CURRENT_SITE_ID') || is_null(CURRENT_SITE_ID)) {
		die('Current SITE ID not defined');
	};
	// process _POST
	$action = '';
	if (array_key_exists('action', $_POST)) {
		$action = $_POST['action'];
	};
	switch ($action) {
		case 'Add':
		case 'add':
			$data = array(
				'type_id'	=> $_POST['item_type'],
				'item_name'	=> $_POST['item_name']
			);
			db_perform('equipment_items', $data);
			$data = array(
				'site_id'	=> CURRENT_SITE_ID,
				'item_id'	=> mysql_insert_id()
			);
			db_perform('equipment_items_sites', $data);
			Header('Location: /Equipment/');
			die();
			break;
	};

	include '../includes/lists_functions.php';

	$eq_types = get_list_values('equipment_types');

    //sort the eq_types in natural order to match the order that list is edited in the list manager
    usort($eq_types, function($a, $b){
        return strnatcasecmp($a['text'], $b['text']);
    });
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo SYSTEM_SUBDOMAIN; ?> Equipment List</title>
<?php include "../includes/head_scripts.php"; ?>
<style>
h1 {
	margin: 30px;
	text-align: center;
	width: 100%;
	color: black;
}
#equipment-table {
	border-collapse: collapse;
	width: 350px;
	margin-right: auto;
	margin-left: auto;
	font-family: Arial;
	font-size: 10pt;
}
#equipment-table input, #equipment-table select {
	width: 90%;
	border: none;
}
#equipment-table th {
	border: 2px solid black;
}
#equipment-table td {
	border: 2px solid black;
	text-align: center;
	color: black;
}
.no-borders {
	border: none !important;
}
.actions {
	background-color: yellow;
	width: 80px;
}
.actions button, .actions input {
	background-color: yellow;
	border: none;
}
td.month.value {
	text-align: right !important;
	white-space:nowrap;
	background-color: silver;
	color: black !important;
}
td.month.value option, #month-new option {
	text-align: right !important;
}
td input {
	text-align: center !important;
}
.current-level {
	text-align: left !important;
	color: black;
	font-size: 12px;
	padding-bottom: 10px;
}
div#current-level-title {
	font-family: arial;
	float: left;
	font-size: 16px;
	color: black;
}
#current-level {
	font-family: arial;
	display: block;
	width: 120px !important;
	margin-left: 20px;
	border: 1px solid black !important;
	float: left;
	height: 20px;
	font-size: 16px;
	text-align: center;
}
select#current-level {
	font-size: 14px !important;
}
#update-level {
	font-family: arial;
	height: 20px;
	width: 50px;
	float: left;
	background-color: yellow;
	color: black;
	border: 1px solid black;
	margin-left: 10px;
}
.header-title {
	background-color: red;
	text-align: center;
	font-family: arial;
	font-size: 20px;
	color: black;
	vertical-align: middle;
	padding: 10px;
}
.header-title select {
	width: 150px !important;
	border: 1px solid black !important;
	font-size: 22px;
	font-family: arial;
	margin-left: 20px;
}
.total-header {
	background-color: silver !important;
}
#equipment-table tr.equipment-type td {
	text-align: left;
	padding-left: 10px;
	font-weight: bold;
	background-color: silver;
}
th.item-type {
	max-width: 100px;
	width: 100px;
}
.item-name {
	text-align: left !important;
	padding-left: 10px;
}
</style>
</head>
<body>
<?php include 'includes/main_menu.php'; ?>
<script>
$(document).ready(function () {
	$(".select").click(function () {
		document.location = 'equipment_log.php?item_id=' + $(this).attr('custom-id');
	});
});
</script>
<table id="equipment-table">
<tr>
    <td colspan="2" class="no-borders">&nbsp;</td>
<tr>
<tr>
    <th colspan="3" class="header-title"><?php echo SYSTEM_SUBDOMAIN; ?> Equipment List</th>
</tr>
<?php
	foreach ($eq_types as $key => $type) {
		echo "<tr class='equipment-type'><td colspan='3'>{$type['text']}</td></tr>\n";
		$sql = "SELECT ei.* FROM equipment_items ei, equipment_items_sites eis WHERE eis.site_id = '".CURRENT_SITE_ID."' and eis.item_id = ei.id and ei.type_id = '{$type['id']}' ORDER BY item_name ASC;";
		$res = mysql_query($sql) or die(mysql_error());
		if (mysql_num_rows($res) > 0) {
			while ($row = mysql_fetch_assoc($res)) {
				echo "<tr class='equipment-item'><td class='item-name' colspan='2'>{$row['item_name']}</td><td class='actions'><button class='select' custom-id='{$row['id']}'>Select</button></td></tr>\n";
			};
		} else {
			echo "<tr class='equipment-item'><td colspan='3' class='no-items-exists'>No items exists</td></tr>";
		};
	};
?>
<tr>
	<td class="no-borders" colspan="2">&nbsp;</td>
</tr>
<form method="POST">
<tr id="add-new-record">
	<th class='item-name'><input type="text" name="item_name" value=""></th>
	<th class='item-type'><?php echo draw_pull_down_menu('item_type', $eq_types); ?></th>
	<th class="actions" id="action-add"><input type="submit" name="action" value="Add"></th>
</tr>
</form>
<tr>
	<th class='no-borders'><br /><button id="back" onClick="javascript: document.location='/'">Back</button></th>
</tr>
</table>
</form>
<?php include ("ticker.php"); ?>
</body>
</html>
