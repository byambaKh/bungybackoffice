<?php
include "../includes/application_top.php";

//Check if the user is logged in before giving out data
$user = new BJUser();
if (!$user->isUser()) die();

$action = $_GET['action'];
$type = $_GET['type'];
if (empty($type)) {
    $type = 'equipment_log';
};
switch (TRUE) {
    case ($action == 'update_row') :
        $data = $_POST;
        unset($data['id']);
        db_perform('equipment_log', $data, 'update', 'id=' . $_POST['id']);
        $result = array('result' => 'success');
        break;
    case ($action == 'delete_row') :
        $sql = "delete from $type WHERE id = {$_POST['id']};";
        mysql_query($sql);
        $result = array('result' => 'success');
        break;
};
$result['request'] = array(
    'action' => $action,
    'type' => $type,
    'post' => $_POST
);
echo json_encode($result);
?>
