<?php
	include '../includes/application_top.php';
	include '../Operations/reports_save.php';
// be sure we works with main DB
	if (!defined('CURRENT_SITE_ID')) {
		die('Site ID not defined');
	};
	if (is_null(CURRENT_SITE_ID)) {
		die('Site ID is null');
	};
	$can_edit = true;
	$item_id = null;
	if (isset($_GET['item_id'])) {
		$item_id = $_GET['item_id'];
	};
	$record_type = 'equipment_log';
    $current_time = time();
    $d = date('Y-m', $current_time);
    if (isset($_GET['date'])) {
        $current_time = mktime(0, 0, 0, substr($_GET['date'], 5, 2), 15, substr($_GET['date'], 0, 4));
    } else {
        $current_time = mktime(0, 0, 0, substr($d, 5, 2), 15, substr($d, 0, 4));
    };
	if ($can_edit && isset($_GET['uid'])) {
		$_SESSION['uid'] = $_GET['uid'];
	};
	$headers = array(
		"I.D.",
		"Start Date",
		"Retire Date",
		"Notes",
	);
	$fields = array_map(function ($item) {
		return strtolower(
			preg_replace(
				"/(_)$/",
				"",
				preg_replace("([^\w]+)", "_", $item)
			)
		);
	}, $headers);
	$fields[0] = 'client_id';
	// get staff list
	foreach ($fields as $field) {
		$values[$field] = array();
	}
	$sql = "SELECT * FROM sites;";
	$res = mysql_query($sql) or die(mysql_error());
	$sites = array();
	$all_sites = array();
	while ($row = mysql_fetch_assoc($res)) {
		$all_sites[$row['id']] = $row;
		if ($row['id'] == CURRENT_SITE_ID) continue;
		$sites[] = array(
			'id'	=> $row['id'],
			'text'	=> $row['name'],
		);
	};
	
	$action = $_POST['action'];
	if (empty($action)) {
		$action = $_GET['action'];
	};
	if ($can_edit) {
	switch (TRUE) {
		case ($action == 'add'):
		case ($action == 'Add'):
            /*
			$sql = "SELECT max(client_id) as max_value FROM equipment_log WHERE site_id = '".CURRENT_SITE_ID."' and item_id = '{$_GET['item_id']}'";
			$res = mysql_query($sql) or die(mysql_error());
			$last_id = 1;
			if ($row = mysql_fetch_assoc($res)) {
				$last_id = (int)$row['max_value'] + 1;
			};*/
            $last_id = '';//don't increment the last id just set it to ''
			$data = array(
				'item_id'		=> (int)$_GET['item_id'],
				'site_id'		=> CURRENT_SITE_ID,
				'client_id'		=> $last_id,
				'start_date'	=> '',
				'retire_date'	=> '',
				'notes'			=> ''
			);
			db_perform('equipment_log', $data);
			Header("Location: /Equipment/equipment_log.php?item_id=" . $_GET['item_id']);
			die();
			break;
		case ($action == 'transfer_item'):
			$id = $_GET['id'];
			$site_id = $_GET['site_id'];
			$item_id = $_GET['item_id'];
			$data = array(
				'retire_date'	=> date("Y-m-d"),
				'notes'			=> 'Transferred to ' . $all_sites[$site_id]['name']
			);
			db_perform('equipment_log', $data, 'update', 'id=' . $id);
			// select new client ID for transferred item
			$sql = "SELECT MAX(client_id) max_id FROM equipment_log WHERE site_id = $site_id and item_id = $item_id";
			$res = mysql_query($sql) or die(mysql_error());
			$client_id = 1;
			if ($row = mysql_fetch_assoc($res)) {
				$client_id = (int)$row['max_id'] + 1;
			};
			$data = array(
				'site_id'	=> $site_id,
				'item_id'	=> $item_id,
				'client_id'	=> $client_id,
				'start_date'=> date("Y-m-d"),
				'notes'		=> 'Transferred from ' . $all_sites[CURRENT_SITE_ID]['name'] . ' on ' . date('Y-m-d')
			);
			db_perform('equipment_log', $data);
			$sql = "SELECT * FROM equipment_items_sites WHERE site_id = '$site_id' and item_id = '$item_id';";
			$res = mysql_query($sql) or die(mysql_error());
			if (mysql_num_rows($res) == 0) {
				$data = array(
					'site_id'	=> $site_id,
					'item_id'	=> $item_id
				);
				db_perform('equipment_items_sites', $data);
			};
			Header("Location: /Equipment/equipment_log.php?item_id=" . $_GET['item_id']);
			die();
			break;
		case ($action == 'purge'):
			$sql = "DELETE FROM equipment_log WHERE site_id = '".CURRENT_SITE_ID."' and item_id = '$item_id' AND retire_date < '".date('Y-m-d')."';";
			mysql_query($sql) or die(mysql_error());
			Header("Location: /Equipment/equipment_log.php?item_id=" . $_GET['item_id']);
			die();
			break;
	};
	};
	// get current item info
	if (!is_null($item_id)) {
		$sql = "SELECT ei.* FROM equipment_items ei, equipment_items_sites eis WHERE eis.site_id = '".CURRENT_SITE_ID."' and eis.item_id = ei.id and id = '$item_id';";
		$res = mysql_query($sql) or die(mysql_error());
		if ($row = mysql_fetch_assoc($res)) {
			$item_name = $row['item_name'];
		} else {
			$item_name = 'No item found';
			$item_id = null;
		};
	};
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Equipment Log</title>
<?php include "../includes/head_scripts.php"; ?>
<style>
h1 {
	margin: 30px;
	text-align: center;
	width: 100%;
	color: black;
}
#training-table {
	border-collapse: collapse;
	width: 650px;
	margin-right: auto;
	margin-left: auto;
	font-family: Arial;
	font-size: 10pt;
}
#training-table input, #training-table select {
	width: 90%;
	border: 1px solid silver;
}
#training-table th {
	border: 2px solid black;
	padding: 3px;
}
#training-table td {
	border: 2px solid black;
	text-align: center;
	color: black;
	padding: 3px;
}
.no-borders {
	border: none !important;
	padding: 0px;
}
.no-border-bottom {
	border-bottom: none !important;
}
.small-borders-top-right {
	border-top: 1px solid black !important;
	border-right: 1px solid black !important;
}
.small-borders-top-left {
	border-top: 1px solid black !important;
	border-left: 1px solid black !important;
}
.small-border-right {
	border-right: 1px solid black !important;
}
.small-border-left {
	border-left: 1px solid black !important;
}
.double-border-right {
	border-right: 2px solid black !important;
}
.double-border-left {
	border-left: 2px solid black !important;
}
.edit {
	width: 70px !important;
}
.actions {
border: none !important;	
}
.actions button, .actions input {
	background-color: yellow;
	border: 1px solid black !important;
	width: 70px !important;
}
td.month.value {
	text-align: right !important;
	white-space:nowrap;
	background-color: silver;
	color: black !important;
}
td.month.value option, #month-new option {
	text-align: right !important;
}
td input {
	text-align: center !important;
}
.current-level {
	text-align: left !important;
	color: black;
	font-size: 12px;
	padding-bottom: 10px;
}
div#current-level-title {
	font-family: arial;
	float: left;
	font-size: 16px;
	color: black;
}
#current-level {
	font-family: arial;
	display: block;
	width: 120px !important;
	margin-left: 20px;
	border: 1px solid black !important;
	float: left;
	height: 20px;
	font-size: 16px;
	text-align: center;
}
select#current-level {
	font-size: 14px !important;
}
#update-level {
	font-family: arial;
	height: 20px;
	width: 50px;
	float: left;
	background-color: yellow;
	color: black;
	border: 1px solid black;
	margin-left: 10px;
}
.header-title {
	background-color: red;
	text-align: left;
	font-family: arial;
	font-size: 20px;
	color: black;
	vertical-align: middle;
}
.header-title select {
	width: 150px !important;
	border: 1px solid black !important;
	font-size: 22px;
	font-family: arial;
	margin-left: 20px;
}
.total-header {
	background-color: silver !important;
}
.action-edit, .action-delete, .action-transfer {
	max-width: 70px;
	width: 70px;
}
.notes {
	min-width: 200px;
	width: 200px;
}
.totals-row th {
<?php if (!$can_edit) { ?>
	color: red !important;
<?php }; ?>
}
.action-back {
	text-align: left !important;
}
.action-purge {
	text-align: center !important;
}
.action-add {
	text-align: right !important;
}
.header {
	white-space: nowrap;
}
.start_date, .retire_date {
	min-width: 100px;
	width: 100px !important;
}
#transfer-dialog {
	display: none;
}
.ui-button {
    width: auto !important;
    height: auto !important;
}
.ui-dialog-titlebar-close {
    display: none;
}
.ui-dialog {
    width: 400px !important;
}
.ui-dialog-buttonset {
    width : 100%;
}
.ui-dialog .ui-dialog-buttonpane {
    padding: 0.3em !important;
}
.ui-dialog-title {
    width: 100% !important;
}

</style>
</head>
<body>
<?php include 'includes/main_menu.php'; ?>
<script>
<?php if ($can_edit) { ?>
<?php 
	for ($i = 1; $i < 13; $i++) {
	};
?>
	var month_names = <?php echo json_encode($month_names); ?>;
	function row_update() {
		var id = $(this).attr('rel');
		var post_data = {};
		post_data.id = id;
		$(this).parent().parent().parent().children("td.value").each(function () {
			var field = $(this).attr('rel');
			post_data[field] = $(this).children('[name='+field+']').prop('value');
			if (field == 'shop_name' && $(this).children('[name='+field+'_new]').prop('value') != '') {
				post_data[field+'_new'] = $(this).children('[name='+field+'_new]').prop('value');
			};
			if (field == 'company' && $(this).children('[name='+field+'_new]').prop('value') != '') {
				post_data[field+'_new'] = $(this).children('[name='+field+'_new]').prop('value');
			};
			post_data['item_id'] = <?php echo $item_id; ?>;
		});
		$.ajax({
			url: 'ajax_handler.php?action=update_row&type=<?php echo $record_type; ?>',
			method: 'POST', 
			data: post_data,
			context: $(this)
		}).done(function () {
			$(this).parent().parent().parent().children("td.value").each(function () {
				var field = $(this).attr('rel');
				if ((field == 'shop_name' || field == 'company') && $(this).children('[name='+field+'_new]').prop('value') != '') {
					$(this).html($(this).children('[name='+field+'_new]').prop('value'));
					fvalues[field].push({
						'id': $(this).html(), 
						'text' :$(this).html()
					});
					fvalues[field] = fvalues[field].sort(function (item1, item2) {
						return (item1.text > item2.text);
					});
				} else {
					$(this).html($(this).children('[name='+field+']').prop('value'));
					if (field == 'month') {
						var val = $(this).html();
						for (var i in fvalues[field]) {
							if (!fvalues[field].hasOwnProperty(i)) continue;
							if (fvalues[field][i].id == val) {
								$(this).html(fvalues[field][i].text);
							};
						};
					};
				};
			});
			$(this).prop('value', 'Edit').removeClass('update').addClass('edit').unbind('click').click(row_edit);
			$("#delete"+id).prop('value', 'Delete').removeClass('cancel').addClass('delete').unbind('click').click(row_delete);
			update_balance();
		});
	}
	function row_delete() {
		var id = $(this).attr('rel');
		if (window.confirm("Are you sure to delete this record?")) {
			//document.location = "?action=Delete&id=" + id;
			var post_data = {"id":id};
			$.ajax({
				url: 'ajax_handler.php?action=delete_row&type=<?php echo $record_type; ?>',
				method: 'POST', 
				data: post_data,
				context: $(this)
			}).done(function () {
				$(this).parent().parent().parent().remove();
				update_balance();
			}).fail(function () {
				alert('Record delete failed.');
			});
		};
	}
	function row_cancel() {
		var id = $(this).attr('rel');
		$(this).parent().parent().parent().children("td.value").each(function () {
			var field = $(this).attr('rel');
			$(this).html($(this).children('input[type=hidden]').prop('value'));
		});
		$("#edit"+id).prop('value', 'Edit').removeClass('update').addClass('edit').unbind('click').click(row_edit);
		$(this).prop('value', 'Delete').removeClass('camcel').addClass('delete').unbind('click').click(row_delete);
	}
	function row_edit() {
		var id = $(this).attr('rel');
		$(this).parent().parent().parent().children("td.value").each(function () {
			var input_field = '';
			var field = $(this).attr('rel');
			var value = $(this).html();
			if (typeof field != 'undefined' && fvalues[field].length > 0) {
				input_field = $('<select>').prop('name', field);
				var text = value;
				for (fv in fvalues[field]) {
					if (fvalues[field].hasOwnProperty(fv)) {
						if (fvalues[field][fv].text == text) {
							value = fvalues[field][fv].id;
						};
						input_field.append(
							$('<option>').prop('value', fvalues[field][fv].id).text(fvalues[field][fv].text)
						);
					};	
				};
				input_field.val(value);
			} else {
				input_field = $('<input>').prop('name', field).prop('value', $(this).html());
			};
			$(this).html(input_field);
			$(this).append(
				$('<input>').prop('type', 'hidden').prop('name', field + '_old').prop('value', value)
			);
			if (field == 'shop_name' || field == 'company') {
				$(this).append($('<br />')).append(
					$('<input>').prop('name', field + '_new')
				);
			};
		});
		$(this).prop('value', 'Update').removeClass('edit').addClass('update').unbind('click').click(row_update);
		$("#delete"+id).prop('value', 'Cancel').removeClass('delete').addClass('cancel').unbind('click').click(row_cancel);
    	$('.date, .date input, input[name=date]').datepicker({
                showAnim: 'fade',
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd',
				defaultDate: '<?php echo date("Y-m-01", $current_time); ?>',
                currentText: 'Today',
                maxDate: new Date(<?php echo $maxDate?>)
    	});

	};
	function update_balance() {
		var balance = 0;
		$("#training-table tr").each(function () {
			var r = {};
			r['in'] = $(this).children('[rel=in]').html();
			r.out = $(this).children('[rel=out]').html();
			if (typeof r['in'] != 'undefined' && r['in'] != '') {
				balance += parseInt(r['in']);
			};
			if (typeof r.out != 'undefined' && r.out != '') {
				balance -= parseInt(r.out);
			};
			$(this).children('[rel=balance]').html(balance);
		});
	};
$(document).ready(function () {
	$('.date, .date input, .datepicker').datepicker({
                showAnim: 'fade',
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd',
				defaultDate: '<?php echo date("Y-m-01", $current_time); ?>',
                currentText: 'Today',
                maxDate: new Date(<?php echo $maxDate?>)
	});
	$(".delete").click(row_delete);
	$('.edit').unbind('click').click(row_edit);
	update_balance();
	$('#add').click(function () {
		document.location='equipment_log.php?action=add&item_id=<?php echo $item_id; ?>';
	});
	$('#purge').click(function () {
		document.location='equipment_log.php?action=purge&item_id=<?php echo $item_id; ?>';
	});
	$(".transfer").click(function () {
		var id = $(this).attr('rel');
        $('#equipment-log-id').val(id);
        $('#transfer-dialog').dialog({
            buttons: [
                {
                    text: "Close",
                    click: function() { $( this ).dialog( "close" ); },
                    style: 'float: left;'
                },
                {
                    text: "Transfer",
                    style: 'float: right; margin-right: 0px;',
                    click: function() {
                        $('#site-selection').submit();
                    }
                }
            ],
            modal: true,
            title: "Please select target site"
        });
	});
});
	var fvalues = {};
<?php
	foreach ($fields as $field) {
		echo "\tfvalues['$field'] = " . json_encode(array_key_exists($field, $values) ? $values[$field] : array()) . ";\n";
	};
?>
function show_sales_calendar() {
	var d = $('#notes').datepicker({
                showAnim: 'fade',
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd',
                defaultDate: '<?php echo date("Y-m-01", $current_time); ?>',
                currentText: 'Today',
                maxDate: new Date(<?php echo $maxDate?>),
				onSelect: function (dstring) {
					$("#company-new select").val("BUNGY JAPAN");
					$("#who-new select").val("Dave");
					$("#description-new select").val("SALES DEPOSIT");
					$("#analysis-new select").val("Sales Deposit - Daily");
					$(this).val('Sales ' + /-([\d]{2})-/.exec(dstring)[1] + '/' + /-([\d]{2})$/.exec(dstring)[1]);
					$(this).datepicker('destroy');
					$(this).focus();
				}
    });
	d.datepicker('show');
}
	function change_uid() {
		document.location = '?uid=' + $("#uid").val();
	}
<?php }; ?>
</script>
<table id="training-table">
<tr>
    <td colspan="<?php echo sizeof($fields) + 4; ?>" class="no-borders">&nbsp;</td>
</tr>
<tr>
    <th colspan="<?php echo sizeof($fields) + 0; ?>" class="header-title"><?php echo SYSTEM_SUBDOMAIN . ' - ' . $item_name; ?> </th>
</tr>
<tr>
    <td colspan="<?php echo sizeof($fields) + 4; ?>" class="no-borders">&nbsp;</td>
</tr>
<tr>
<?php
	foreach ($fields as $key => $field) {
		$span = '';
		$class = ' class="header"';
		echo "\t<th id=\"$field-header\"$span$class>{$headers[$key]}</th>\n";
	};
	if ($can_edit) {
		echo "\t<th id=\"actions-header\" class=\"no-borders\"></th>\n";
	};
?>
</tr>
<?php
	$d = date('Y-', $current_time);
	$sql = "select * from equipment_log WHERE site_id = '".CURRENT_SITE_ID."' AND item_id = '$item_id' order by client_id ASC;";
	if($res = mysql_query($sql)) {
	$totals = array();
	while ($row = mysql_fetch_assoc($res)) {
?>
<tr>
<?php
		foreach ($fields as $key => $field) {
			$class = '';
			$text = $row[$field];
			if (preg_match("/(date)/is", $field)) {
				$class = ' date';
			};
			if ($text == '0000-00-00') {
				$text = '';
			};
			echo "\t<td id=\"$field-{$row['id']}\" class=\"value $field$class\" rel=\"$field\">{$text}</td>\n";
		};
	if ($can_edit) {
?>
	<th class="actions no-borders">&nbsp;</th>
	<th class="actions no-borders action-edit"><nobr><input type="button" value="Edit" rel="<?php echo $row['id']; ?>" class="edit" id="edit<?php echo $row['id']; ?>"></nobr></th>
	<th class="actions no-borders action-transfer"><nobr><input type="button" value="Transfer" rel="<?php echo $row['id']; ?>" class="transfer" id="transfer<?php echo $row['id']; ?>"></nobr></th>
	<th class="actions no-borders action-delete"><nobr><input type="button" value="Delete" rel="<?php echo $row['id']; ?>" class="delete" id="delete<?php echo $row['id']; ?>"></nobr></th>
<?php }; ?>
</tr>
<?php
	};
	};
?>
<tr>
	<th class='no-borders' colspan="<?php echo sizeof($fields) + 1; ?>">
	<table class="no-borders" style="width: 100%;">
	<tr>
		<td class="actions action-back"><button id="back" onClick="javascript: document.location='/Equipment/'">Back</button></td>
		<td class="actions action-purge"><button id="purge">Purge</button></td>
		<td class="actions action-add"><button id="add">Add</button></td>
	</tr>
	</table>
	</th>
</tr>
</table>
</form>
<div id="transfer-dialog" title="Please select site">
<form id="site-selection">
<label for="year">Target Site: </label>
<?php
	echo draw_pull_down_menu("site_id", $sites, '', 'id="site-id"');
?>
<input type="hidden" name="id" value="" id="equipment-log-id">
<input type="hidden" name="item_id" value="<?php echo $item_id; ?>">
<input type="hidden" name="action" value="transfer_item">
</form>
</div>
<?php include ("ticker.php"); ?>
</body>
</html>
