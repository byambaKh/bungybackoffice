<?php
require_once("../includes/application_top.php");
require_once("bookingcontroller.php");

//require_once("languageStrings.php");
//require_once("PageSequences.php");

//Nextals... Multilingual
//controller based
//simple clean code
//responsive

//copy the template from the booking page
//http://webdesignerwall.com/tutorials/responsive-design-with-css3-media-queries
//http://www.smashingmagazine.com/2011/01/guidelines-for-responsive-web-design/
//http://www.hongkiat.com/blog/responsive-web-tutorials/

function drawTimesTable($timesAndOpen, $lang)
{
    $returnString = '
				<table>
				<tr>
					<td></td> 
					<td>Time:</td> 
					<td>Maxiumum Number Of People</td>
				</tr>';

    foreach ($timesAndOpen as $timeAndOpen) {
        $returnString .= "
		<tr>
			<td><input type='radio' name='time' value='{$timeAndOpen['time']}'></td> 
			<td>{$timeAndOpen['time']}</td>
			<td>{$timeAndOpen['slot']}</td>
		</tr>";
    }

    return $returnString . "\n</table>";
}

//Prioritise setting the language through the url
if (isset($_GET['language'])) $language =
    $_GET['language'];
else
    if (isset($_SESSION['language'])) $language = $_SESSION['language'];
else
    $language = 'jpn';//default to Japanese

$_SESSION['language'] = $language;

$urlParts = explode("/", $_SERVER['REQUEST_URI']);
$page = preg_replace('/\\?.*/', '', end($urlParts));//get rid of the GET parameters


if (!isset($_SESSION['booking']))
    $_SESSION['booking'] = new OnlineBooking();

$bookingController = new BookingController();

$bookingController->page = $page;//set it so that the before action can check the page for certain actions

$bookingController->$page();//The previous code makes sure $page is only equal to a page from page sequence
extract($bookingController->variables);//make the variables accessible before page render

//General language strings
$languageStrings = [
    'eng' => [
        'next' => 'next',
        'back' => 'back',
        'help' => 'help',
    ],
    'jpn' => [
        'next' => '次へ',
        'back' => '戻る',
        'help' => 'help',
    ]
];

extract($languageStrings[$language]);
unset($languageStrings);

require_once("pages/template.php");
