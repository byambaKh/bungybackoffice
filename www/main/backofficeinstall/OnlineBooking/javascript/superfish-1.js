// =============================================================================
// JS/VENDOR/SUPERFISH-1.5.1.MIN.JS
// -----------------------------------------------------------------------------
// Copyright (c) 2008 Joel Birch
//
// Dual licensed under the MIT and GPL licenses:
// http://www.opensource.org/licenses/mit-license.php
// http://www.gnu.org/licenses/gpl.html
// =============================================================================

(function(e){e.fn.superfish=function(t){var n=e.fn.superfish,r=n.c,i=e('<span class="'+r.arrowClass+'"></span>'),s=function(t){var n=e(this),r=u(n);clearTimeout(r.sfTimer);n.showSuperfishUl().siblings().hideSuperfishUl()},o=function(){var t=e(this),r=u(t),i=n.op;clearTimeout(r.sfTimer);r.sfTimer=setTimeout(function(){i.retainPath=e.inArray(t[0],i.$path)>-1;t.hideSuperfishUl();if(i.$path.length&&t.parents("li."+i.hoverClass).length<1){i.onIdle.call(this);s.call(i.$path)}},i.delay)},u=function(t){t.hasClass(r.menuClass)&&e.error("Superfish requires you to update to a version of hoverIntent that supports event-delegation, such as this one: https://github.com/joeldbirch/onHoverIntent");var i=t.closest("."+r.menuClass)[0];n.op=n.o[i.serial];return i},a=function(t){var r="li:has(ul)";if(e.fn.hoverIntent&&!n.op.disableHI)t.hoverIntent(s,o,r);else{t.on("mouseenter",r,s);t.on("mouseleave",r,o)}t.on("focusin",r,s);t.on("focusout",r,o)},f=function(e){e.addClass(r.anchorClass).append(i.clone())};return this.addClass(r.menuClass).each(function(){var i=this.serial=n.o.length,s=e.extend({},n.defaults,t),o=e(this);s.$path=o.find("li."+s.pathClass).slice(0,s.pathLevels).each(function(){e(this).addClass(s.hoverClass+" "+r.bcClass).filter("li:has(ul)").removeClass(s.pathClass)});n.o[i]=n.op=s;a(o);o.find("li:has(ul)").each(function(){s.autoArrows&&f(e(">a:first-child",this))}).not("."+r.bcClass).hideSuperfishUl();s.onInit.call(this)})};var t=e.fn.superfish;t.o=[];t.op={};t.c={bcClass:"sf-breadcrumb",menuClass:"sf-js-enabled",anchorClass:"sf-with-ul",arrowClass:"sf-sub-indicator"};t.defaults={hoverClass:"sfHover",pathClass:"overideThisToUse",pathLevels:1,delay:800,animation:{opacity:"show"},speed:"normal",autoArrows:!0,disableHI:!1,onInit:function(){},onBeforeShow:function(){},onShow:function(){},onHide:function(){},onIdle:function(){}};e.fn.extend({hideSuperfishUl:function(){var n=t.op,r=n.retainPath===!0?n.$path:"";n.retainPath=!1;var i=e("li."+n.hoverClass,this).add(this).not(r).removeClass(n.hoverClass).find(">ul").hide().css("visibility","hidden");n.onHide.call(i);return this},showSuperfishUl:function(){var e=t.op,n=this.addClass(e.hoverClass).find(">ul:hidden").css("visibility","visible");e.onBeforeShow.call(n);n.animate(e.animation,e.speed,function(){e.onShow.call(n)});return this}})})(jQuery);

jQuery(document).ready(function() {
  jQuery('.sf-menu').superfish({
    delay : 650,
    speed : 'fast',
  });
});