<?php
require_once("../includes/application_top.php");

function testCreateConfirmedBooking() 
{
	$booking = new OnlineBooking();

	$booking->siteId		= 1;
	$booking->bookingDate   = "2015-11-17";
	$booking->bookingTime   = "13:30";
	$booking->jumpNumber    = 1;
	$booking->firstName     = "Last Name";
	$booking->lastName      = "First Name";
	$booking->email         = "developer@standardmove.com";
	$booking->phoneNumber   = "0123456789";
	$booking->transportation= "Car";

	$booking->saveConfirmed();//It should be written to the database customerregs1 table
}

function testCreateUnconfirmedBooking() 
{
	$booking = new OnlineBooking();

	$booking->siteId		= 1;
	$booking->bookingDate   = "2015-11-17";
	$booking->bookingTime   = "13:30";
	$booking->jumpNumber    = 1;
	$booking->firstName     = "Last Name";
	$booking->lastName      = "First Name";
	$booking->email         = "developer@standardmove.com";
	$booking->phoneNumber   = "0123456789";
	$booking->transportation= "Car";

	$booking->saveUnconfirmed();//It should be written to the database customerregstemp_1 table
}

function testLoadByConfirmationCode() 
{
	$string = "7c977348fc4a348ba7979a638e1e1ffc";
	OnlineBooking::confirmByLink($string);

}

testLoadByConfirmationCode();
