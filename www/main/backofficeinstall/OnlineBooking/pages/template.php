<!DOCTYPE html>
<!--[if IE 8]>
<html class="no-js ie8" lang="en-US"><![endif]-->
<!--[if IE 9]>
<html class="no-js ie9" lang="en-US"><![endif]-->
<!--[if gt IE 9]><!-->
<html style="" class=" js no-touch" lang="en-US"><!--<![endif]-->
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Our bridges | Bungy Japan</title>

    <link rel="stylesheet" href="style/language-selector.css" type="text/css" media="all">
    <link rel="profile" href="http://gmpg.org/xfn/11">
    <link rel="pingback" href="http://newfrontoffice.bungyjapan.jp/xmlrpc.php">
    <link href="style/css_002.css" rel="stylesheet" type="text/css">
    <meta name="robots" content="noindex,follow">
    <link rel="alternate" type="application/rss+xml" title="Bungy Japan » Feed" href="http://newfrontoffice.bungyjapan.jp/feed/">
    <link rel="alternate" type="application/rss+xml" title="Bungy Japan » Comments Feed" href="http://newfrontoffice.bungyjapan.jp/comments/feed/">
    <link rel="alternate" type="application/rss+xml" title="Bungy Japan » Our bridges Comments Feed" href="http://newfrontoffice.bungyjapan.jp/our-bridges/feed/">
    <link rel="stylesheet" id="responsive-lightbox-nivo-front-css" href="style/nivo-lightbox.css" type="text/css" media="all">
    <link rel="stylesheet" id="responsive-lightbox-nivo-front-template-css" href="style/default.css" type="text/css" media="all">
    <link rel="stylesheet" id="rs-plugin-settings-css" href="style/settings.css" type="text/css" media="all">
    <style type="text/css">
        .tp-caption a {
            color: #ff7302;
            text-shadow: none;
            -webkit-transition: all 0.2s ease-out;
            -moz-transition: all 0.2s ease-out;
            -o-transition: all 0.2s ease-out;
            -ms-transition: all 0.2s ease-out;
        }

        .tp-caption a:hover {
            color: #ffa902;
        }
    </style>
    <link rel="stylesheet" id="rs-captions-css" href="style/dynamic-captions.css" type="text/css" media="all">
    <link rel="stylesheet" id="x-shortcodes-integrity-light-css" href="style/integrity-light_002.css" type="text/css" media="all">
    <link rel="stylesheet" id="x-integrity-light-css" href="style/integrity-light.css" type="text/css" media="all">
    <link rel="stylesheet" id="x-font-standard-css" href="style/css.css" type="text/css" media="all">
    <script type="text/javascript" src="javascript/jquery.js"></script>
    <script type="text/javascript" src="javascript/jquery-migrate.js"></script>
    <script type="text/javascript" src="javascript/nivo-lightbox.js"></script>
    <script type="text/javascript">
        /* <![CDATA[ */
        var rlArgs = {
            "script": "nivo",
            "selector": "lightbox",
            "custom_events": "",
            "activeGalleries": "1",
            "effect": "fade",
            "keyboardNav": "1",
            "errorMessage": "The requested content cannot be loaded. Please try again later."
        };
        /* ]]> */
    </script>
    <script type="text/javascript" src="javascript/front.js"></script>
    <script type="text/javascript" src="javascript/jquery_002.js"></script>
    <script type="text/javascript" src="javascript/jquery_004.js"></script>
    <script type="text/javascript" src="javascript/backstretch-2.js"></script>
    <script type="text/javascript" src="javascript/modernizr-2.js"></script>
    <script type="text/javascript" src="javascript/x.js"></script>
    <script type="text/javascript" src="javascript/jplayer-2.js"></script>
    <script type="text/javascript" src="javascript/jquery-ui-1.js"></script>
    <script type="text/javascript" src="javascript/imagesloaded-3.js"></script>
    <script type="text/javascript" src="javascript/video-4.js"></script>
    <script type="text/javascript" src="javascript/bigvideo-1.js"></script>
    <script type="text/javascript" src="javascript/comment-reply.js"></script>
    <link rel="shortlink" href="http://newfrontoffice.bungyjapan.jp/?p=2">
    <meta name="generator" content="WPML ver:3.1.4 stt:1,28;0">
    <link rel="alternate" hreflang="en-US" href="http://newfrontoffice.bungyjapan.jp/our-bridges/">
    <link rel="alternate" hreflang="ja" href="http://newfrontoffice.bungyjapan.jp/our-bridges/?lang=ja">

    <style type="text/css">
    </style>
    <link rel="shortcut icon" href="images/fav.png">
    <link rel="apple-touch-icon-precomposed" href="images/touch-icon.png">
    <meta name="msapplication-TileColor" content="#ce000c">
    <meta name="msapplication-TileImage" content="images/tile-icon1.png">
    <meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress.">
    <style type="text/css">body {
        background: #f3f3f3 url(images/pat.png) center top repeat;
    }

    a, h1 a:hover, h2 a:hover, h3 a:hover, h4 a:hover, h5 a:hover, h6 a:hover, .x-topbar .p-info a:hover, .x-breadcrumb-wrap a:hover, .widget ul li a:hover, .widget ol li a:hover, .widget.widget_text ul li a, .widget.widget_text ol li a, .widget_nav_menu .current-menu-item > a, .x-widgetbar .widget ul li a:hover, .x-twitter-widget ul li a, .x-accordion-heading .x-accordion-toggle:hover, .x-comment-author a:hover, .x-comment-time:hover, .x-close-content-dock:hover i {
        color: #ff2a13;
    }

    a:hover, .widget.widget_text ul li a:hover, .widget.widget_text ol li a:hover, .x-twitter-widget ul li a:hover, .x-recent-posts a:hover .h-recent-posts {
        color: #d80f0f;
    }

    a.x-img-thumbnail:hover, .x-slider-revolution-container.below, .page-template-template-blank-3-php .x-slider-revolution-container.above, .page-template-template-blank-6-php .x-slider-revolution-container.above {
        border-color: #ff2a13;
    }

    .entry-thumb:before, .pagination span.current, .flex-direction-nav a, .flex-control-nav a:hover, .flex-control-nav a.flex-active, .jp-play-bar, .jp-volume-bar-value, .x-dropcap, .x-skill-bar .bar, .x-pricing-column.featured h2, .h-comments-title small, .x-entry-share .x-share:hover, .x-highlight, .x-recent-posts .x-recent-posts-img, .x-recent-posts .x-recent-posts-img:before, .tp-bullets.simplebullets.round .bullet:hover, .tp-bullets.simplebullets.round .bullet.selected, .tp-bullets.simplebullets.round-old .bullet:hover, .tp-bullets.simplebullets.round-old .bullet.selected, .tp-bullets.simplebullets.square-old .bullet:hover, .tp-bullets.simplebullets.square-old .bullet.selected, .tp-bullets.simplebullets.navbar .bullet:hover, .tp-bullets.simplebullets.navbar .bullet.selected, .tp-bullets.simplebullets.navbar-old .bullet:hover, .tp-bullets.simplebullets.navbar-old .bullet.selected, .tp-leftarrow.default, .tp-rightarrow.default {
        background-color: #ff2a13;
    }

    .x-nav-tabs > .active > a, .x-nav-tabs > .active > a:hover {
        -webkit-box-shadow: inset 0 3px 0 0 #ff2a13;
        box-shadow: inset 0 3px 0 0 #ff2a13;
    }

    .x-recent-posts a:hover .x-recent-posts-img, .tp-leftarrow.default:hover, .tp-rightarrow.default:hover {
        background-color: #d80f0f;
    }

    .x-main {
        width: 69.536945%;
    }

    .x-sidebar {
        width: 25.536945%;
    }

    .x-topbar {
        background-color: transparent;
    }

    .x-navbar .x-nav > li > a:hover, .x-navbar .x-nav > .current-menu-item > a {
        -webkit-box-shadow: inset 0 4px 0 0 #ff2a13;
        box-shadow: inset 0 4px 0 0 #ff2a13;
    }

    body.x-navbar-fixed-left-active .x-navbar .x-nav > li > a:hover, body.x-navbar-fixed-left-active .x-navbar .x-nav > .current-menu-item > a {
        -webkit-box-shadow: inset 8px 0 0 0 #ff2a13;
        box-shadow: inset 8px 0 0 0 #ff2a13;
    }

    body.x-navbar-fixed-right-active .x-navbar .x-nav > li > a:hover, body.x-navbar-fixed-right-active .x-navbar .x-nav > .current-menu-item > a {
        -webkit-box-shadow: inset -8px 0 0 0 #ff2a13;
        box-shadow: inset -8px 0 0 0 #ff2a13;
    }

    .x-topbar .p-info, .x-topbar .p-info a, .x-navbar .x-nav > li > a, .x-nav-collapse .sub-menu a, .x-breadcrumb-wrap a, .x-breadcrumbs .delimiter {
        color: #b7b7b7;
    }

    .x-navbar .x-nav > li > a:hover, .x-navbar .x-nav > .current-menu-item > a, .x-navbar .x-navbar-inner .x-nav-collapse .x-nav > li > a:hover, .x-navbar .x-navbar-inner .x-nav-collapse .x-nav > .current-menu-item > a, .x-navbar .x-navbar-inner .x-nav-collapse .sub-menu a:hover {
        color: #272727;
    }

    .rev_slider_wrapper {
        border-bottom-color: #ff2a13;
    }

    .x-navbar-static-active .x-navbar .x-nav > li > a, .x-navbar-fixed-top-active .x-navbar .x-nav > li > a {
        height: 90px;
        padding-top: 34px;
    }

    .x-navbar-fixed-left-active .x-navbar .x-nav > li > a, .x-navbar-fixed-right-active .x-navbar .x-nav > li > a {
        padding-top: 19px;
        padding-bottom: 19px;
        padding-left: 7%;
        padding-right: 7%;
    }

    .sf-menu li:hover ul, .sf-menu li.sfHover ul {
        top: 75px;;
    }

    .sf-menu li li:hover ul, .sf-menu li li.sfHover ul {
        top: -0.75em;
    }

    .x-navbar-fixed-left-active .x-widgetbar {
        left: 235px;
    }

    .x-navbar-fixed-right-active .x-widgetbar {
        right: 235px;
    }

    .x-container-fluid.width {
        width: 88%;
    }

    .x-container-fluid.max {
        max-width: 1200px;
    }

    .x-comment-author, .x-comment-time, .comment-form-author label, .comment-form-email label, .comment-form-url label, .comment-form-rating label, .comment-form-comment label, .widget_calendar #wp-calendar caption, .widget_calendar #wp-calendar th, .widget_calendar #wp-calendar #prev, .widget_calendar #wp-calendar #next, .widget.widget_recent_entries li a, .widget_recent_comments a:last-child, .widget.widget_rss li .rsswidget {
        font-weight: 400;
    }

    @media (max-width: 979px) {
        .x-navbar-fixed-left .x-container-fluid.width, .x-navbar-fixed-right .x-container-fluid.width {
            width: 88%;
        }

        .x-nav-collapse .x-nav > li > a:hover, .x-nav-collapse .sub-menu a:hover {
            -webkit-box-shadow: none;
            box-shadow: none;
        }

        .x-navbar-fixed-left-active .x-widgetbar {
            left: 0;
        }

        .x-navbar-fixed-right-active .x-widgetbar {
            right: 0;
        }
    }

    body {
        font-size: 14px;
        font-weight: 400;
    }

    a:focus, select:focus, input[type="file"]:focus, input[type="radio"]:focus, input[type="checkbox"]:focus {
        outline: thin dotted #333;
        outline: 5px auto #ff2a13;
        outline-offset: -1px;
    }

    h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
        font-weight: 400;
        letter-spacing: -1px;
    }

    .entry-header, .entry-content {
        font-size: 14px;
    }

    .x-brand {
        font-weight: 400;
        letter-spacing: -3px;
    }

    .x-brand img {
        width: 125px;
    }

    .x-main.full {
        float: none;
        display: block;
        width: auto;
    }

    @media (max-width: 979px) {
        .x-main.left, .x-main.right, .x-sidebar.left, .x-sidebar.right {
            float: none;
            display: block;
            width: auto;
        }
    }

    .x-btn-widgetbar {
        border-top-color: #000000;
        border-right-color: #000000;
    }

    .x-btn-widgetbar:hover {
        border-top-color: #444444;
        border-right-color: #444444;
    }

    body.x-navbar-fixed-left-active {
        padding-left: 235px;
    }

    body.x-navbar-fixed-right-active {
        padding-right: 235px;
    }

    .x-navbar {
        font-size: 12px;
    }

    .x-navbar .x-nav > li > a {
        font-weight: 400;
        font-style: normal;
    }

    .x-navbar-fixed-left, .x-navbar-fixed-right {
        width: 235px;
    }

    .x-navbar-fixed-top-active .x-navbar-wrap {
        height: 90px;
    }

    .x-navbar-inner {
        min-height: 90px;
    }

    .x-btn-navbar {
        margin-top: 20px;;
    }

    .x-btn-navbar, .x-btn-navbar.collapsed {
        font-size: 24px;
    }

    .x-brand {
        font-size: 54px;
        font-size: 5.4rem;
        margin-top: 5px;
    }

    body.x-navbar-fixed-left-active .x-brand, body.x-navbar-fixed-right-active .x-brand {
        margin-top: 30px;
    }

    @media (max-width: 979px) {
        body.x-navbar-fixed-left-active, body.x-navbar-fixed-right-active {
            padding: 0;
        }

        body.x-navbar-fixed-left-active .x-brand, body.x-navbar-fixed-right-active .x-brand {
            margin-top: 5px;
        }

        .x-navbar-fixed-top-active .x-navbar-wrap {
            height: auto;
        }

        .x-navbar-fixed-left, .x-navbar-fixed-right {
            width: auto;
        }
    }

    .x-btn, .button, [type="submit"] {
        color: #ffffff;
        border-color: #ac1100;
        background-color: #cd0007;
    }

    .x-btn:hover, .button:hover, [type="submit"]:hover {
        color: #ffffff;
        border-color: #600900;
        background-color: #cd0007;
    }

    .x-btn.x-btn-real, .x-btn.x-btn-real:hover {
        margin-bottom: 0.25em;
        text-shadow: 0 0.075em 0.075em rgba(0, 0, 0, 0.65);
    }

    .x-btn.x-btn-real {
        -webkit-box-shadow: 0 0.25em 0 0 #a71000, 0 4px 9px rgba(0, 0, 0, 0.75);
        box-shadow: 0 0.25em 0 0 #a71000, 0 4px 9px rgba(0, 0, 0, 0.75);
    }

    .x-btn.x-btn-real:hover {
        -webkit-box-shadow: 0 0.25em 0 0 #ce000c, 0 4px 9px rgba(0, 0, 0, 0.75);
        box-shadow: 0 0.25em 0 0 #ce000c, 0 4px 9px rgba(0, 0, 0, 0.75);
    }

    .x-btn.x-btn-flat, .x-btn.x-btn-flat:hover {
        margin-bottom: 0;
        text-shadow: 0 0.075em 0.075em rgba(0, 0, 0, 0.65);
        -webkit-box-shadow: none;
        box-shadow: none;
    }

    .x-btn.x-btn-transparent, .x-btn.x-btn-transparent:hover {
        margin-bottom: 0;
        border-width: 3px;
        text-shadow: none;
        text-transform: uppercase;
        background-color: transparent;
        -webkit-box-shadow: none;
        box-shadow: none;
    }

    .x-btn-circle-wrap:before {
        width: 172px;
        height: 43px;
        background: url(images/btn-circle-top-small.png) center center no-repeat;
        -webkit-background-size: 172px 43px;
        background-size: 172px 43px;
    }

    .x-btn-circle-wrap:after {
        width: 190px;
        height: 43px;
        background: url(images/btn-circle-bottom-small.png) center center no-repeat;
        -webkit-background-size: 190px 43px;
        background-size: 190px 43px;
    }

    .x-btn, .x-btn:hover, .button, .button:hover, [type="submit"], [type="submit"]:hover {
        text-shadow: 0 0.075em 0.075em rgba(0, 0, 0, 0.5);
    }

    .x-btn, .button, [type="submit"] {
        border-radius: 0.25em;
    }</style>
    <style type="text/css">.portfolio .post_meta {
        display: none !important;
    }

    .x-recent-posts a {
        border: 0px;
    }

    .x-recent-posts .x-recent-posts-date {
        display: none;
    }</style>

</head>
<body class="page page-id-2 page-template page-template-template-blank-1-php x-integrity x-integrity-light x-navbar-static-active x-full-width-layout-active x-full-width-active x-post-meta-disabled x-portfolio-meta-disabled wpb-js-composer js-comp-ver-3.7.4 vc_responsive">

<!--
BEGIN #top.site
-->

<div id="top" class="site">


    <header class="masthead" role="banner">


        <div class="x-navbar-wrap">
            <div class="x-navbar">
                <div class="x-navbar-inner x-container-fluid max width">
                    <a href="http://newfrontoffice.bungyjapan.jp/" class="x-brand img" title="">
                        <img src="images/logo-test22.png" alt=""> </a>
                    <a href="#" class="x-btn-navbar collapsed" data-toggle="collapse" data-target=".x-nav-collapse">
                        <i class="x-icon-bars"></i>
                        <span class="visually-hidden">Menu</span>
                    </a>
                    <nav class="x-nav-collapse collapse" role="navigation">


                        <div id="lang_sel">
                            <ul>
                                <li><a href="#" class="lang_sel_sel icl-en"> English</a>
                                    <ul>
                                        <li class="icl-ja">
                                            <?
                                            if($language == 'jpn')
                                                echo '<a href="?language=eng">English</a>';
                                            else
                                                echo '<a href="?language=jpn">日本語</a>'
                                            ?>

                                        </li>
                                    </ul>
                                </li>
                            </ul>
                        </div>
                        <ul id="menu-primary" class="x-nav sf-menu sf-js-enabled">
                            <li id="menu-item-7784" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-7784">
                                <a class="sf-with-ul" href="http://newfrontoffice.bungyjapan.jp/about/">About<span class="sf-sub-indicator"></span></a>
                                <ul style="display: none; visibility: hidden;" class="sub-menu">
                                    <li id="menu-item-7786" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7786">
                                        <a href="http://newfrontoffice.bungyjapan.jp/gallery/">Gallery</a></li>
                                    <li id="menu-item-7787" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7787">
                                        <a href="http://newfrontoffice.bungyjapan.jp/what-is-bungy-jump/">What is Bungy Jumping?</a>
                                    </li>
                                    <li id="menu-item-7785" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7785">
                                        <a href="http://newfrontoffice.bungyjapan.jp/all-links/">All links</a></li>
                                </ul>
                            </li>
                            <li id="menu-item-6686" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-2 current_page_item menu-item-has-children menu-item-6686">
                                <a class="sf-with-ul" href="http://newfrontoffice.bungyjapan.jp/our-bridges/">Our bridges<span class="sf-sub-indicator"></span></a>
                                <ul style="visibility: hidden; display: none;" class="sub-menu">
                                    <li id="menu-item-7472" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7472">
                                        <a href="http://newfrontoffice.bungyjapan.jp/bungy-spots/minakami/">Minakami Bungy (42m)</a>
                                    </li>
                                    <li id="menu-item-7473" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7473">
                                        <a href="http://newfrontoffice.bungyjapan.jp/bungy-spots/sarugakyo/">Sarugakyo Bungy (62m)</a>
                                    </li>
                                    <li id="menu-item-7474" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7474">
                                        <a href="http://newfrontoffice.bungyjapan.jp/bungy-spots/ryujin/">Ryujin Bungy (100m)</a>
                                    </li>
                                    <li id="menu-item-7475" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7475">
                                        <a href="http://newfrontoffice.bungyjapan.jp/bungy-spots/itsukimura/">Itsukimura Bungy (77m)</a>
                                    </li>
                                    <li id="menu-item-7480" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7480">
                                        <a href="http://newfrontoffice.bungyjapan.jp/bungy-spots/kaiun/">Kaiun Bungy (30m)</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="menu-item-7443" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-7443">
                                <a class="sf-with-ul" href="http://bungyjapan.jp/booking/">Booking<span class="sf-sub-indicator"></span></a>
                                <ul style="display: none; visibility: hidden;" class="sub-menu">
                                    <li id="menu-item-7476" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7476">
                                        <a href="http://www.bungyjapan.jp/en/index.php/booking/sections/C40/">Minakami reservations</a>
                                    </li>
                                    <li id="menu-item-7477" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7477">
                                        <a href="http://www.bungyjapan.jp/en/index.php/booking/sections/C39/">Sarugakyo reservations</a>
                                    </li>
                                    <li id="menu-item-7478" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7478">
                                        <a href="http://www.bungyjapan.jp/en/index.php/sites/sections/C47/">Ryujin reservations</a>
                                    </li>
                                    <li id="menu-item-7479" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7479">
                                        <a href="http://www.bungyjapan.jp/en/index.php/booking/sections/C37/">Itsukimura reservations</a>
                                    </li>
                                </ul>
                            </li>
                            <li id="menu-item-6684" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6684">
                                <a href="http://newfrontoffice.bungyjapan.jp/faq/">FAQ</a></li>
                            <li id="menu-item-6697" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6697">
                                <a href="http://newfrontoffice.bungyjapan.jp/contact/">Contact</a></li>
                        </ul>
                    </nav> <!-- end .x-nav-collapse.collapse -->
                </div> <!-- end .x-navbar-inner -->
            </div> <!-- end .x-navbar -->
        </div> <!-- end .x-navbar-wrap -->
    </header>


    <div class="x-main x-container-fluid max width offset" role="main">
        <article id="post-2" class="post-2 page type-page status-publish hentry no-post-thumbnail">
            <div class="entry-wrap entry-content">
                <div id="x-content-band-1" class="x-content-band vc man" style="background-color: #fff;">
                    <div class="x-container-fluid max width">
                        <div class="x-column vc whole" style="">
                            <h2 class="h-feature-headline h2">
                                <span><i class="x-icon-location-arrow"></i>Where?</span>
                            </h2>
                        </div>
                    </div>
                    <div class="x-container-fluid max width">
                        <div class="x-column vc whole" style="">
                            <h2 class="h-feature-headline h2">
                            <span><i class="x-icon-arrows"></i>Where?</span>
                            </h2>
                        </div>
                    </div>
                    <div class="x-container-fluid max width">
                        <div class="x-column vc whole" style="">
                            <h2 class="h-feature-headline h2">
                                <span><i class="x-icon-calendar"></i>Which Day?</span>
                            </h2>
                        </div>
                    </div>
                    <div class="x-container-fluid max width">
                        <div class="x-column vc whole" style="">
                            <h2 class="h-feature-headline h2">
                                <span><i class="x-icon-clock-o"></i>What Time?</span>
                            </h2>
                        </div>
                    </div>
                    <div class="x-container-fluid max width">
                        <div class="x-column vc whole" style="">
                            <h2 class="h-feature-headline h2">
                                <span><i class="x-icon-group"></i>How Many?</span>
                            </h2>
                        </div>
                    </div>
                    <div class="x-container-fluid max width">
                        <div class="x-column vc whole" style="">
                            <h2 class="h-feature-headline h2">
                                <span><i class="x-icon-check"></i>Conditions &amp; Policies</span>
                            </h2>
                        </div>
                    </div>
                    <div class="x-container-fluid max width">
                        <div class="x-column vc whole" style="">
                            <h2 class="h-feature-headline h2">
                                <span><i class="x-icon-file-text"></i>Information &amp; Payment Method</span>
                            </h2>
                        </div>
                    </div>
                    <div class="x-container-fluid max width">
                        <div class="x-column vc whole" style="">
                            <h2 class="h-feature-headline h2">
                                <span><i class="x-icon-thumbs-up"></i>Confirmation</span>
                            </h2>
                        </div>
                    </div>
                    <div class="x-container-fluid max width">
                        <div class="x-column vc whole" style="">
                            <h2 class="h-feature-headline h2">
                                <span><i class="x-icon-star"></i>Finalization</span>
                            </h2>
                        </div>
                    </div>
                </div>

                <div id="x-content-band-2" class="x-content-band vc man" style="background-color: transparent;">
                    <div class="x-container-fluid max width">
                        <form id="booking-form" action="" method="POST">
                            <? require_once( __DIR__ . "/$page.php" ) ?>
                        </form>

                    </div>
                </div>

            </div> <!-- end .entry-wrap.entry-content -->
            <span class="visually-hidden"><span class="author vcard"><span class="fn">bungyjapan</span></span><span class="entry-title">Our bridges</span><time class="entry-date updated" datetime="2014-03-24T11:18:29+00:00">03.24.2014</time></span>
        </article> <!-- end .hentry -->
    </div> <!-- end .x-main.x-container-fluid.max.width.offset -->


    <a class="x-scroll-top right fade" href="#top" title="Back to Top">
        <i class="x-icon-angle-up"></i>
    </a>

    <script>

        jQuery(document).ready(function ($) {

            //
            // Scroll top anchor.
            //
            // 1. Get the number of pixels to the bottom of the page.
            // 2. Get the difference from the body height and the bottom offset.
            // 3. Output the adjusted height for the page for acurate percentage parameter.
            //

            $.fn.scrollBottom = function () {
                return $(document).height() - this.scrollTop() - this.height();
            };

            var windowObj = $(window);
            var body = $('body');
            var bodyOffsetBottom = windowObj.scrollBottom(); // 1
            var bodyHeightAdjustment = body.height() - bodyOffsetBottom; // 2
            var bodyHeightAdjusted = body.height() - bodyHeightAdjustment; // 3
            var scrollTopAnchor = $('.x-scroll-top');

            function sizingUpdate() {
                var bodyOffsetTop = windowObj.scrollTop();
                if (bodyOffsetTop > ( bodyHeightAdjusted * 0.75 )) {
                    scrollTopAnchor.addClass('in');
                } else {
                    scrollTopAnchor.removeClass('in');
                }
            }

            windowObj.bind('scroll', sizingUpdate).resize(sizingUpdate);
            sizingUpdate();

            scrollTopAnchor.click(function () {
                $('html,body').animate({scrollTop: 0}, 850, 'easeInOutExpo');
                return false;
            });

        });

    </script>


    <footer class="x-colophon top" role="contentinfo">
        <div class="x-container-fluid max width">
            <div class="x-row-fluid">


                <div class="x-span3">
                    <div id="text-3" class="widget widget_text logo-footer">
                        <img src="images/logo-footer.jpg">
                    </div>
                </div>

                <div class="x-span3" style="display:none">
                    <div id="text-3" class="widget widget_text">
                        <img src="images/book-footer.jpg">
                    </div>
                </div>

                <div class="x-span3 footerBook">
                    <div id="text-4" class="widget widget_text">
                        <div class="textwidget">
                            <a href="http://www.bungyjapan.com/en/index.php/booking/" target="_blank">
                                <button class="btn-bungy-grey">　Online Booking</button>
                            </a><br>
                            or call us at<br>
                            0278-72-8133
                        </div>
                    </div>
                </div>
                <div class="x-span3">
                    <div id="text-2" class="widget widget_text"><h4 class="h-widget">Head Office</h4>
                        <div class="textwidget">〒379-1612<br>
                            200 Obinata<br>
                            Minakami-machi, Tone-gun<br>
                            Gunma-ken, 379-1612 <br>
                            TEL: 0278-25-4638<br>
                            FAX: 0278-25-4639<br></div>
                    </div>
                </div>
                <div class="x-span3">
                    <div id="text-3" class="widget widget_text"><h4 class="h-widget">Call us</h4>
                        <div class="textwidget">Call Center 0278-72-8133<br>
                        </div>
                    </div>
                </div>
            </div> <!-- end .x-row-fluid -->
        </div> <!-- end .x-container-fluid.max.width -->
    </footer> <!-- end .x-colophon.top -->


    <footer class="x-colophon bottom" role="contentinfo">
        <div class="x-container-fluid max width">


            <div class="x-social-global">
                <a data-original-title="Facebook" data-toggle="tooltip" data-placement="top" data-trigger="hover" href="https://ja-jp.facebook.com/bungyjapan" class="facebook" title="" target="_blank"><i class="x-social-facebook"></i></a><a data-original-title="Twitter" data-toggle="tooltip" data-placement="top" data-trigger="hover" href="https://twitter.com/bungyjapan/status/354042666062860288" class="twitter" title="" target="_blank"><i class="x-social-twitter"></i></a>
            </div>
            <div class="x-colophon-content">
                <p style="letter-spacing: 2px; text-transform: uppercase; opacity: 0.5; filter: alpha(opacity=50);">2016 Copyright by Bungy Japan. All rights reserved.</p>
            </div>

        </div> <!-- end .x-container-fluid.max.width -->
    </footer> <!-- end .x-colophon.bottom -->


</div>

<!--
END #top.site
-->


<script type="text/javascript" src="javascript/jquery_003.js"></script>
<script type="text/javascript">
    /* <![CDATA[ */
    var _wpcf7 = {"loaderUrl": "images\/ajax-loader.gif", "sending": "Sending ..."};
    /* ]]> */
</script>
<script type="text/javascript" src="javascript/scripts.js"></script>
<script type="text/javascript" src="javascript/x-shortcodes.js"></script>
<script type="text/javascript" src="javascript/easing-1.js"></script>
<script type="text/javascript" src="javascript/flexslider-2.js"></script>
<script type="text/javascript" src="javascript/collapse-2.js"></script>
<script type="text/javascript" src="javascript/alert-2.js"></script>
<script type="text/javascript" src="javascript/tab-2.js"></script>
<script type="text/javascript" src="javascript/transition-2.js"></script>
<script type="text/javascript" src="javascript/tooltip-2.js"></script>
<script type="text/javascript" src="javascript/popover-2.js"></script>
<script type="text/javascript" src="javascript/waypoints-2.js"></script>
<script type="text/javascript" src="javascript/hoverintent-7.js"></script>
<script type="text/javascript" src="javascript/superfish-1.js"></script>
<script type="text/javascript">
    /* <![CDATA[ */
    var icl_vars = {"current_language": "en", "icl_home": "http:\/\/newfrontoffice.bungyjapan.jp"};
    /* ]]> */
</script>
<script type="text/javascript" src="javascript/sitepress.js"></script>
<!--[if lt IE 9]>
<script type="text/javascript" src="http://newfrontoffice.bungyjapan.jp/wp-content/themes/x/framework/js/vendor/selectivizr-1.0.2.min.js"></script><![endif]-->


</body>
</html>
