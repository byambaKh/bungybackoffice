<?
$languageStrings = [
    'eng' => [
        'when' => 'When ?',
        'pleaseEnter' => 'Please Chose The Time You Would Like to Jump.',
    ],
    'jpn' => [
        'when' => '何時？',
        'pleaseEnter' => 'Japanese Loreum Ipsum',
    ]
];

extract($languageStrings[$language]);//extract the variables for the appropriate language
?>


<h1><?=$when?></h1>
<h2>Ryujin Ohashi, Hitachi Ota</h2>
<?php echo drawTimesTable($timesAndOpen, $language);?>

<button class="wide-button" id="next"><?=$next?></button>
<button class="wide-button" id="back"><?=$back?></button>
<button class="wide-button" id="help"><?=$help?></button>
