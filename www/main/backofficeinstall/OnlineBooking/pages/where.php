<?
$languageStrings = [
    'eng' => [
        'where' => 'Where ?',
    ],
    'jpn' => [
        'where' => 'どこで？',
    ]
];

extract($languageStrings[$language]);
?>
<h1><?=$where?></h1>
<!--<img src="img/site_3_booking_cover.jpg" id="" class="site-image">-->

<select class="" name="siteId">
    <option value="0">Minakami</option>
    <option value="1">Sarugakyo</option>
    <option value="2">Ryujin</option>
    <option value="3">Itsuki</option>
</select>

<p>
    Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum
</p>

<button class="wide-button" id="next"><?=$next?></button>
<br>
<br>
<br>
<button class="wide-button" id="help"><?=$help?></button>
