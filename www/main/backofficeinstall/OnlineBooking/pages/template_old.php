<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta name="viewport" content="width=device-width; initial-scale=1.0">
    <meta charset="utf-8">
    <link rel="stylesheet" href="css/threshold_booking.css" type="text/css" media="all" />
    <script src="/js/jquery-1.11.1.js" type="text/javascript"></script>

    <script>
        $(document).ready(function(){
            $('#back').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
            });

            /*//does not work
            $('#back').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                document.form.submit();
            });
            */
            $('#next').click(function(){
                //back in document history
                document.form.submit();
            });

            $('#help').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();

                //do //show some help text
            });

            $('#conditions-agree').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                $('#conditionsAgreed').val(1);
                document.forms['booking-form'].submit();
            });
        });
    </script>
</head>
<body>
<div id="pagewrap">
    <header id="header"><img src="img/header_<?=$language?>.jpg"></header>
    <div id="content-wrap">
        <?
        if($language == 'jpn')
            echo '<a href="?language=eng">English</a>';
        else
            echo '<a href="?language=jpn">日本語</a>'
        ?>

        <form id="booking-form" action="" method="POST">
        <? require_once( __DIR__ . "/$page.php" ) ?>
        </form>

    </div>
    <div id="footer">
        <!--
        <img src="img/footer_<?=$language?>.gif">
        -->
    </div>
</div>
</body>
</html>
