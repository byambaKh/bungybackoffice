<?php

class BookingController
{
    private static $pageSequence = [
        'where',
        'conditions',
        'which_day',
        'what_time',
        'people',
        'how_much',//Pay Later or Pay Now
        'finalize',
        'confirmation',
    ];

    public $booking;
    public $variables = [];

    private function getNextPage($page)
    {
        $pageSequence = self::getPageSequence();
        $index = array_search($page, $pageSequence);

        if($index === false) {
            $index = 0;//return first element
        } else if ($index + 1 <= count($pageSequence)) {
            $index += 1;
        }

        return $pageSequence[$index];
    }

    public static function getPageSequence()
    {
        return self::$pageSequence;
    }

    private function set($name, $variable)
    {
        $this->variables[$name] = $variable;
    }


    public function __construct()
    {
        if(isset($_SESSION['booking']))
            $this->booking = &$_SESSION['booking'];
        else {
            header("location: where");//start again
            exit;
        }

    }

    public function __destruct()
    {

    }

    private function checkConditionsAgreedTo()
    {
        //if the user tries to skip the agreeing to conditions
        //redirect back to the conditions page
        if( !$this->booking->conditionsAgreedTo ) {
            header("location: conditions");
            exit;
        }

    }

    private function beforeBookingSequenceAction()
    {
        //if a non existent page has been specified, start again
        if (!in_array($this->page, BookingController::getPageSequence())) {
            header("location: ".$this->getNextPage($this->page));
            exit();//prevent any other statements being executed
        }
    }

    public function where()
    {
        $this->beforeBookingSequenceAction();
        //set session booking object Location
        if(count($_POST)) {
            $this->booking->siteId = $_POST['siteId'];
            header("location: ".$this->getNextPage($this->page));//start again
            exit;

        } else {
            //load up the view data
            //get a list of all of the open active sites for
        }
    }

    public function which_day()
    {
        $this->beforeBookingSequenceAction();
        if(count($_POST)) {
            //perform the save...
            //set session booking object time
            $this->booking->bookingTime = $_POST['time'];
            header("location: ".$this->getNextPage($this->page));//start again
            exit;

        } else {
            //TODO:getBookTimes will return the correct data
            $timesAndOpen = [
                ['time' => '09:00', 'slot' => '1'],
                ['time' => '09:30', 'slot' => '3'],
                ['time' => '10:00', 'slot' => '4'],
                ['time' => '10:30', 'slot' => '1'],
                ['time' => '11:00', 'slot' => '6'],
                ['time' => '11:30', 'slot' => '3'],
                ['time' => '12:00', 'slot' => '3'],
                ['time' => '12:30', 'slot' => '3'],
                ['time' => '13:00', 'slot' => '2'],
                ['time' => '13:30', 'slot' => '0'],
                ['time' => '14:00', 'slot' => '2'],
                ['time' => '14:30', 'slot' => '4'],
                ['time' => '15:00', 'slot' => '5'],
                ['time' => '15:30', 'slot' => '1'],
                ['time' => '16:00', 'slot' => '2'],
                ['time' => '16:30', 'slot' => '0'],
                ['time' => '17:00', 'slot' => '0'],
            ];

            //load up the view data
            $this->set('timesAndOpen', $timesAndOpen);
        }

    }

    public function what_time()
    {
        $this->beforeBookingSequenceAction();
        if(count($_POST)) {
            //perform the save...
            //set session booking object time
            $this->booking->bookingTime = $_POST['time'];
            header("location: ".$this->getNextPage($this->page));//start again
            exit;

        } else {
            //TODO:getBookTimes will return the correct data
            $timesAndOpen = [
                ['time' => '09:00', 'slot' => '1'],
                ['time' => '09:30', 'slot' => '3'],
                ['time' => '10:00', 'slot' => '4'],
                ['time' => '10:30', 'slot' => '1'],
                ['time' => '11:00', 'slot' => '6'],
                ['time' => '11:30', 'slot' => '3'],
                ['time' => '12:00', 'slot' => '3'],
                ['time' => '12:30', 'slot' => '3'],
                ['time' => '13:00', 'slot' => '2'],
                ['time' => '13:30', 'slot' => '0'],
                ['time' => '14:00', 'slot' => '2'],
                ['time' => '14:30', 'slot' => '4'],
                ['time' => '15:00', 'slot' => '5'],
                ['time' => '15:30', 'slot' => '1'],
                ['time' => '16:00', 'slot' => '2'],
                ['time' => '16:30', 'slot' => '0'],
                ['time' => '17:00', 'slot' => '0'],
            ];

            //load up the view data
            $this->set('timesAndOpen', $timesAndOpen);
        }

    }

    public function people()
    {
        $this->beforeBookingSequenceAction();
        if(count($_POST)) {
            //set something and go to the next step
            $this->booking->jumpNumber= $_POST['time'];
            header("location: ".$this->getNextPage($this->page));//start again
        } else {
            //load up the view data
        }
    }

    public function conditions()
    {
        $this->beforeBookingSequenceAction();
        if(count($_POST)) {
            //set something and go to the next step
            if($_POST['conditionsAgreed']) {
                $this->booking->conditionsAgreedTo = 1;
                header("location: finalize");//start again
                exit;
            } else {
                header("location: people");//start again
                exit;
            }

        } else {
            //load up the view data
        }
    }

    public function finalize()
    {
        $this->beforeBookingSequenceAction();
        $this->checkConditionsAgreedTo();
        if(count($_POST)) {
            //set something and go to the next step
            $this->booking->firstName      = $_POST['first_name'];
            $this->booking->lastName       = $_POST['last_name'];
            $this->booking->phoneNumber    = $_POST['telephone_number'];
            $this->booking->email          = $_POST['email'];
            $this->booking->transportation = $_POST['transport'];
            header("location: confirmation");//start again
            exit;
        } else {
            //load up the view data
        }
    }

    public function confirmation()
    {
        $this->beforeBookingSequenceAction();
        $this->checkConditionsAgreedTo();
        if(count($_POST)) {
            //send confirmation email
        } else {
            //load up the view data
        }
    }

    public function confirm()
    {

    }

    public function goToNext($currentPage)
    {
        $this->beforeBookingSequenceAction();
        $nextPage = '';
        header("location: $nextPage");
    }

    public function goBack($currentPage)
    {
        $nextPage = '';
        header("location: $nextPage");
    }
}


