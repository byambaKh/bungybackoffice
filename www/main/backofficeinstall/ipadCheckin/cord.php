<?php
require_once("../includes/application_top_noAuth.php");
function cordBounds($date = NULL, $site_id = NULL, $weight)
{
    $query = "SELECT * FROM daily_reports WHERE site_id = $site_id AND date = '$date';";
    $results = queryForRows($query);

    if(count($results) == 1)
        $cordBounds = $results[0];
    else
        (die("No cord bounds for the day"));

    //get the weight from the daily report
    $minWeight			 = 40;

    $bounds = array();

    $bounds['yellowMin'] = (int)$minWeight;
    $bounds['yellowMax'] = (int)$cordBounds['eq_cord_tw_yellow'];

    $bounds['redMin']	 = (int)$bounds['yellowMax'] + 1;
    $bounds['redMax']	 = (int)$cordBounds['eq_cord_tw_red'];

    $bounds['blueMin']   = (int)$bounds['redMax'] + 1;
    $bounds['blueMax']   = (int)$cordBounds['eq_cord_tw_blue'];

    $bounds['blackMin']  = (int)$bounds['blueMax'] + 1;
    $bounds['blackMax']  = (int)$cordBounds['eq_cord_tw_black'];
    
    if      (weight < $bounds['yellowMin']) return "none";
    else if (weight < $bounds['yellowMax']) return "yellow";
    else if (weight < $bounds['redMax']) return "red";
    else if (weight < $bounds['blueMax']) return "blue";
    return "black";
}
?>
