<?php
require_once('cord.php');

$time = urldecode($_GET['time']);
$weight = urldecode($_GET['weight']);
$bookingType = urldecode($_GET['bookingType']);
$jumpCount = urldecode($_GET['jumpCount']);
$photoNumber = urldecode($_GET['photoNumber']);
$site_id = urldecode($_GET['site_id']);
//we could run a check to see if cord color matches the weight
$groupOfNumber = urldecode($_GET['groupOfNumber']);
$firstName = urldecode($_GET['firstName']);
$lastName = urldecode($_GET['lastName']);
$host = urldecode($_GET['host']);
$paid = urldecode($_GET['paid']);

$cordColor = cordBounds(date('Y-m-d'), $site_id, $weight);


switch($cordColor){
    case ("yellow"):
        $yellowHl = "highlight-cord";
        $redHl = "cord";
        $blueHl = "cord";
        $blackHl = "cord";
    break;
    case ("red"):
        $redHl = "highlight-cord";
        $yellowHl = "cord";
        $blueHl = "cord";
        $blackHl = "cord";
    break;
    case ("blue"):
        $blueHl = "highlight-cord";
        $yellowHl = "cord";
        $redHl = "cord";
        $blackHl = "cord";
    break;
    case ("black"):
        $blackHl = "highlight-cord";
        $yellowHl = "cord";
        $redHl = "cord";
        $blueHl = "cord";
    break;
    default:
        $blackHl = "cord";
        $yellowHl = "cord";
        $redHl = "cord";
        $blueHl = "cord";
    break;
}
/*
?time=8:00&weight = 66&bookingType= Walkin&jumpCount=5&photoNumber=5&cordColor=yellow&groupOfNumber=3&firstName=TEST&lastName=TEST&site_id=1
$time = "8:00";
$weight = "66";
$bookingType = "Walkin";
$jumpNumber = "5";
$photoNumber = "5";
$cordColor = "Yellow";
//we could run a check to see if cord color matches the weight
$groupOfNumber = "3";
$firstName = "TEST";
$lastName = "TEST";
$printer = urldecode($_GET['printer']);
*/
?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Ticket</title>
    <link rel="stylesheet" type="text/css" href="./css/ticket.css" media="all">
</head>
<body>

<div class="cellBody">
    <div class="blackOnWhite centerText bigText">Bungy Japan</div>
    <div class="subCell">
        <span class="sitLeft">
            Time:
        </span>
        <span class="sitRight">
            <?php echo "$bookingType";?>
        </span>
        <div class="bigText">
            <?php echo "$time";?>
        </div>
    </div>
    <div class="subCell clearfix">
    <div class="box">
        Photo:
        <div class="bigText biggerText">
            <?=$photoNumber?>
        </div>
    </div>
        Jump:
        <div class="bigText biggerText">
            <?=$jumpCount?>
        </div>
    </div>
    <div class="subCell">
        Weight:
        <div class="bigText biggerText biggestText">
            <?php echo $weight; ?>
        </div>
    </div>
    <div class="subCell">
        <div class="bigText">
            <span class="<?=$yellowHl?>">Y</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="<?=$redHl?>">R</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="<?=$blueHl?>">B</span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<span class="<?=$blackHl?>">BK</span>
        </div>
    </div>
    <div class="subCell">
        Name: 
        <div class="bigText nameText">
            <?php echo $firstName; ?>
        </div>
        <div class="bigText">
            <?php echo $lastName; ?>
        </div>
    </div>
    <div class="blackOnWhite smallText">Japan’s Only Bungy Professional</div>
    <div class="blackOnWhite"><?="{$weight}#{$host}#".date('ymd')."#$paid" ?></div>
</div>

</body>
</html>
