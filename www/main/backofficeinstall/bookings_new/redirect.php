<?php
include_once("loadContent.php");

//Get content based on language
$content = loadContent("redirect.json");

if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
if(empty($_SESSION['place'])) redirect($content);
if(empty($_SESSION['numPeople'])) redirect($content);
if(empty($_SESSION['date'])) redirect($content);
if(empty($_SESSION['time'])) redirect($content);

function redirect($content) // escapes back to form start if data is missing
{
    header("Location: " . $content['url']);
    exit;
}
?>