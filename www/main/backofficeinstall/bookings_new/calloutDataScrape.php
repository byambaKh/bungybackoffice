<?php
session_start();
$freeJumpTimes = $_SESSION['freeJumpTimes'];
/* testing code
$test = [];
array_push($test, $freeJumpTimes[0]);
array_push($test, $freeJumpTimes[600]);
array_push($test, $freeJumpTimes[400]);
array_push($test, $freeJumpTimes[900]);
var_dump($test);
prioritize($test, [1,2,3,4,5]);
var_dump($test);
*/
$site_id = $_POST['site_id'];
$jumpers = $_POST['jumpers'];
$date = $_POST['date'];
$result = array();
$lunchtimes = array("12:00 PM", "12:30 PM", "13:00 PM", "13:30 PM"); 

foreach($freeJumpTimes as $time)
{
    if($time["site_id"] != $site_id && $time["slots"] >= $jumpers && $time['BookingDate'] == $date && in_array($time["BookingTime"],$lunchtimes,TRUE)) // Check for lunchtime available slots
    {
        array_push($result,$time); //Compile all of them together into an array
    }
}
switch ($site_id)
{
    case 1://MK
        prioritize($result, [9,2]);
        break;
    case 2://SR
        prioritize($result, [9,1]);
        break;
    case 3://IB
        prioritize($result, [9,2]);
        break;
    case 9://YB
        prioritize($result, [3,2]);
        break;
    case 10://KU
        prioritize($result, [12]);
        break;
    case 11://FJ
        prioritize($result, [9,3]);
        break;
    default:
        echo json_encode($result[0]); //Just take whatever the first element happens to be
}

function prioritize($input, $priorities) //Sorts based on priority list then echoes the first element in json encoding
{
    usort($input, function($a, $b) use ($priorities) 
    {
        /*
        {"site_id":"4","BookingDate":"2019-08-17","BookingTime":"13:30 PM","slots":"3"} example $a
        {"site_id":"12","BookingDate":"2019-08-17","BookingTime":"12:00 PM","slots":"6"} example $b
        [12,4,3] example $priorities 
        */
        $aPosition = array_search($a["site_id"], $priorities);
        $bPosition = array_search($b["site_id"], $priorities);
        if($aPosition === FALSE && $bPosition === FALSE) return 0;          // neither in the list
        else if ($aPosition === FALSE && $bPosition !== FALSE) return 1;    // a not in list                
        else if ($aPosition !== FALSE && $bPosition === FALSE) return -1;   // b not in list               
        else if ($aPosition < $bPosition) return -1;                        // a higher up list               
        else if ($aPosition > $bPosition) return 1;                         // b higher up list               
        else return 0;
    });
    echo json_encode($input[0]);
}
?>