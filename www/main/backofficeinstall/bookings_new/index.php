<?php
header('Content-Type: text/html; charset=utf-8'); //need this to avoid encoding issues.
session_start();
$_SESSION['mail_sent'] = false;
include("../includes/application_top_noAuth.php");
include("navbar/navbar.php");
include_once("discount.php");
include_once("loadContent.php");

//Get content based on language
$content = loadContent("index.json");
mysql_query('SET NAMES utf8;');
if($_SESSION['booking_saved'] == true) $_SESSION = array();
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="css/bookings.css">
<link rel="stylesheet" type="text/css" href="css/callout.css">
<link rel="stylesheet" type="text/css" href="css/navmenu.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="jquery-dateformat.min.js"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M23LPW5');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M23LPW5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="content">
    <div class="row">    	
        <span class="errors"><?php echo $_GET['message'];?></span>
    </div>
    <form action="<?php echo $content['confirm'] ?>" method = "post" class="form">
        <div class="row">    	
            <h1><?php echo $content['heading'] ?></h1>
        </div>        
        <div class="row">    	
            <h2><?php echo $content['subheading'] ?></h2>
        </div>
        <div class="row">       
                <?php echo $content['email'] ?>
                <input required type="email" name="email" class="picker" data-validation="email" data-validation-error-msg="<?php echo $err['email'] ?>">
                <!-- <input type="email" class="form-control picker" name="email" id="exampleInputEmail1" data-validation="email"  aria-describedby="emailHelp" placeholder="Enter email" > -->
        </div>                        
        <div class="row" style="margin-top: 5vh;">
            <input type="submit" class="pretty_link forward" name="submit" value = "<?php echo $content['next'] ?>">
        </div>        
    </form>
</div>
</body>
</html>

