<?php
session_start();
include_once("loadContent.php");
function makeNavMenu()
{
    //Get content based on language
    $content = loadContent(__DIR__ . "/content_navbar.json");
    $menuItems = $content["menu"];
    //Construct navmenu  
    echo '<nav class=topnav>';
    echo'<a class="item logocontainer" href="http://bungyjapan.com"><img class="logo" src="//www.bungyjapan.com/wp-content/uploads/2016/10/logo-mast.png"
    alt="bungylogo"></a>';
    echo '<ul class="topMenu">';
    echo '<li class="doubleup"><a class="item img" href="' . strtok($_SERVER["REQUEST_URI"],'?') . '"><img class="flag" alt="japanese" src="./navbar/japanese.png"></a>'; // Create url without changing lang 
    echo '<a class="item img" href="' . strtok($_SERVER["REQUEST_URI"],'?') . '?lang=en"><img class="flag" alt="english" src="./navbar/english.png"></a></li>'; // set lang to english
    echo '<a href="javascript:void(0);" class="item icon" onclick="expand()">Menu</a>';
    foreach($menuItems as $menuItem)
    {
        if($menuItem["sub"] == null)//No submenus
        {
            echo '<li><a class="hideable item" href="' . $menuItem["link"] . '">'. $menuItem["heading"] .'</a></li>
            ';
        }
        else // Create a dropdown if submenus are present
        {
            echo '<li class="dropdown">
            <span onclick="dropdown('. "'" . $menuItem["heading"] . "'" .')" class="hideable item" value="' . $menuItem["link"] . '">'. $menuItem["heading"] .'</span>
            ';
            echo '<ul class="subMenu" id="'. $menuItem["heading"] .'">
            ';
            foreach($menuItem["sub"] as $subItem)
            {
                echo '<li><a class="hideable item" href="' . $subItem["link"] . '">'. $subItem["heading"] .'</a></li>
                ';
            }
            echo '
            </ul>
            </li>';
        }
    }
    echo "</ul>";
    echo '</nav>';
    echo '<div class="bottomDivider"></div>';
}
?>

<head>
    <link rel="stylesheet" href="./navbar/navmenu.css" />
    <link rel="stylesheet" type="text/css" href="css/navmenu.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
    <script src="jquery-dateformat.min.js"></script>
</head>

<div class="" id="">      
    <?php
        makeNavMenu();
    ?>
    
</div>

<script>
    var hideMenu = true;
    function dropdown(callerChild)
    {
       var subMenu = document.getElementById(callerChild);
       if ($(subMenu).is(':visible'))
       {
           $(subMenu).hide();
       } 
       else
       {
           $(subMenu).show();
       }
    }
    function expand()
    {
        if(hideMenu)
        {
            $(".hideable").css({"display": "block"});
        }
        else
        {
            $(".hideable").css({"display": "none"});
        }
        hideMenu = !hideMenu;
    }
</script>