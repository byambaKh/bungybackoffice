<?php
session_start();
$freeJumpTimes = $_SESSION['freeJumpTimes'];
$site_id = $_POST['site_id'];
$jumpers = $_POST['jumpers'];
$result = array();

foreach($freeJumpTimes as $time) //Makes an array of $results[time]-> true to pass to the jquery calendar for free days
{
    if($time["site_id"] == $site_id && $time["slots"] >= $jumpers)
    {
        $result[$time["BookingDate"]] = true;
    }
}

echo json_encode($result);
?>