<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
header('Content-Type: text/html; charset=utf-8'); //need this to avoid encoding issues.
include("../includes/application_top_noAuth.php");
include_once("navbar/navbar.php");
include_once("loadContent.php");

$passkey = mysql_real_escape_string($_GET['passkey']);
$thanksCont = loadContent("thankyou.json");
mysql_query('SET NAMES utf8;');

if(!$passkey)
{
    redirect($thanksCont['url_home'], "'Booking not found. Please check email.");
}else
{
    $sql = '
        SELECT * 
            FROM emailregs 
                WHERE ConfirmCode = "' . $passkey . '"';// Get customer's booking
    $booking = reset(queryForRows($sql)); // reset to get first element of "array" (should only be one element long anyway)

    if(!$booking)
    {
        $booking_status = 'Booking not found';
        redirect($thanksCont['url_home'], $booking_status);
    }

    $sql = "SELECT * FROM emailregs 
                WHERE ConfirmCode = '" . $passkey . "' and Expired = '1' ";   // Check expired booking

    $res = mysql_query($sql) or die(mysql_error());
    if(mysql_num_rows($res) > 0)
    {
       //$expiredMsg = 'Booking is expired';      
       redirect($thanksCont['url_home'], $thanksCont['expiredMsg']);   
    } 

    $_SESSION['cemail'] = $booking['CustomerEmail'];
    $_SESSION['code'] = $booking['ConfirmCode'];
}


function populateJumpPlaces()
{
    $sql = '
        SELECT 
        id, name, name_jp 
        FROM sites 
            WHERE hidden = 0 and id = 1;
    ';
    $jumpPlaces = queryForRows($sql);
    foreach($jumpPlaces as $row)
    {
        echo '<option value="' . $row['id'] . '">';
        if($_GET["lang"] == "en") echo $row['name'];
        else echo $row['name_jp'];
        echo '</option>';
    }
}

function redirect($content, $message) // escapes back to form start if data is missing
{
    echo '<script type="text/javascript">';   
    echo 'window.location.href="'. $content. "&message=" . $message .'";';
    echo '</script>';
    exit;
}

//Get content based on language
$content = loadContent("index.json");
mysql_query('SET NAMES utf8;');
if($_SESSION['booking_saved'] == true) $_SESSION = array();
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="css/bookings.css">
<link rel="stylesheet" type="text/css" href="css/callout.css">
<link rel="stylesheet" type="text/css" href="css/navmenu.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="jquery-dateformat.min.js"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M23LPW5');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M23LPW5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="content">
    <div class="row">    	
        <span class="errors"><?php echo $_GET['message'];?></span>
    </div>
    <form action="<?php echo $content['disclaimer'] ?>" method = "post" class="form">
        <div class="row">    	
            <h1><?php echo $content['heading'] ?></h1>
        </div>
        <div class="row" style="color:red">       
            <?php echo $content['PriceChange'] ?>
        </div>
        <div class="row">    	
            <h2><?php echo $content['subheading'] ?></h2>
        </div>        
        <div class="row">
            <?php echo $content['jump_place'] ?>	
            <select required name = "place" id = "jump_place" class="picker">
                <option value="" selected disabled hidden><?php echo $content['please_select'] ?></option>
                <?php populateJumpPlaces() ?>
            </select>
        </div>
        <div class="row">
            <?php echo $content['jump_number'] ?>
            <select required name = "numPeople" id = "jumpers" class="picker">
                <option value="" selected disabled hidden><?php echo $content['please_select'] ?></option>
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
            </select>
        </div>
        <div class="row">    	
            <?php echo $content['jump_date'] ?> <input required type="text" name = "date" id="datepicker" class="picker" disabled readOnly>
        </div>
        <div class="row notice">
            <?php echo $content['notice1'] ?>
        </div>
        <div class="row notice">
            <?php echo $content['notice2'] ?>
        </div>
        <div class="row">
            <?php echo $content['checkin_time'] ?>	
            <select required name = "time" id = "timepicker" class="picker">
                <option value="" selected disabled hidden><?php echo $content['please_select'] ?></option>
            </select>
        </div>
        <div class = "callout" id = "callout" onclick = "clickCallout()">
            <div class = "innerPadding">
                <div class="calloutRow">
                    <span class = "center"><?php echo $content['calloutTip'] ?></span>
                </div>
                <div class="calloutRow">
                    <span class = "half"><?php echo $content['calloutSite'] ?></span><span class = "half" id="calloutSite">SiteHere</span>
                </div>
                <div class="calloutRow">
                    <span class = "half"><?php echo $content['calloutTime'] ?></span><span class = "half" id="calloutTime">TimeHere</span>
                </div>
                <div class="calloutRow">
                    <span class = "center"><?php echo $content['calloutActNow'] ?></span>
                </div>
            </div>
        </div>
        <div class="row">
            <input type="submit" class="pretty_link forward" value = "<?php echo $content['next'] ?>">
        </div>
        <div class="row notice">
            <?php echo $content['notice3'] ?>
        </div>
    </form>
</div>
<script>
    var jumpPlacesReady = false;
    var openDayArray = {};
    $( function() 
        {
            $( "#datepicker" ).datepicker
            ({
                dateFormat: 'yy-mm-dd',
                beforeShowDay: function(date)
                {
                    if(openDayArray[$.format.date(date,"yyyy-MM-dd")] == true)
                    {
                        return [true,"",""]; // Open day
                    }
                    else return [false,"",""]; // Closed day
                }
            });
        } 
    );
    $(document).ready
    (
        function ()
        {
            $.ajax
            ({
                url: 'pollJumpPlaces.php',
                success: function()
                {
                    jumpPlacesReady = true;
                    updateJumpDate();
                }
            });
        }
    );
    
    var places = document.getElementById("jump_place");
    var jumpers = document.getElementById("jumpers");
    var date = document.getElementById("datepicker");
    var checkIn = document.getElementById("timepicker");
    var callout = document.getElementById("callout");
    var calloutSite = document.getElementById("calloutSite");
    var calloutTime = document.getElementById("calloutTime");
    var calloutData = new Object;
    
    places.onchange = function()
    {
        hideCallout();
        updateJumpDate();
    }
    jumpers.onchange = function()
    {
        hideCallout();
        updateJumpDate();
    }
    date.onchange = function()
    {
        hideCallout();
        updateTimes();
    }
    function updateJumpDate()//updates the calendar with available dates
    {
        timepicker.value = "default";
        timepicker.setAttribute("disabled", true);
        date.value = ""
        if(places.value != "default" && jumpers.value != "default" && jumpPlacesReady)
        {
            $.ajax
            ({
                url: 'makeDatePickerArray.php',
                type: 'POST',
                data: { site_id : places.value, jumpers : jumpers.value },
                dataType: "json",
                success: function(data)
                {
                    openDayArray = data;
                    date.removeAttribute("disabled"); //Make selectable
                }
            });
        }
    }
    function updateTimes(callback)//updates the dropdown with available times
    {
        $(checkIn).empty();
        checkIn.removeAttribute("disabled"); //Make selectable
        $.ajax
        ({
            url: 'makeTimeArray.php',
            type: 'POST',
            data: { site_id : places.value, jumpers : jumpers.value, date : date.value },
            dataType: "json",
            success: function(data)
            {
                jQuery.each(data, function(index, value)
                {
                    var o = new Option(value, value);
                    $(o).html(value);
                    checkIn.append(o);
                });
                updateCallout();//Get the callout ready now
                if (callback) callback();
            }
        });
    }
    function updateCallout()//updates the callout for lunchtime bookings
    {
        var placeList = new Object();//Construct an object with the site ids associated with names to display later (from the 'places' dropdown on the page)
        for (i = 0; i < places.options.length; i++)
        {
           var val = places.options[i].value;
           placeList[val] = places.options[i].text;
        }
        var lunchBookingAvailable;
        for (i = 0; i < checkIn.options.length; i++)// Check if a lunchtime slot is already available.
        {
           var test = checkIn.options[i].text;
           if (test == "12:00 PM" ||  test == "12:30 PM" || test == "13:00 PM" || test == "13:30 PM") return; // No need to continue if a slot exists.
        }
        lunchtimes = new Array();
        $.ajax
        ({
            url: 'calloutDataScrape.php',
            type: 'POST',
            data: { site_id : places.value, jumpers : jumpers.value, date : date.value },
            dataType: "json",
            success: function(data)// returns e.g. {"site_id":"9","BookingDate":"2019-08-17","BookingTime":"12:30 PM","slots":"6"}
            {
                calloutSite.innerText = placeList[data.site_id];
                calloutTime.innerText = data.BookingTime;
                calloutData = data;
            }
        });
        callout.classList.add("revealed");
    }
    function clickCallout()//Called if the customer wants the callout booking
    {
        hideCallout();
        for (i = 0; i < places.options.length; i++)//Set to the new place
        {
           if(calloutData.site_id == places.options[i].value) places.selectedIndex = i;
        }
        updateTimes(function()
        {
            for (i = 0; i < checkIn.options.length; i++)//Set to the available time
            {
               if(calloutData.BookingTime == checkIn.options[i].value) checkIn.selectedIndex = i;
            }
        });

    }
    function hideCallout()//Called to reset the callout if booking is changed
    {
        callout.classList.remove("revealed");
    }
</script>
</body>
</html>

