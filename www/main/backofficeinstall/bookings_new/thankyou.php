<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
header('Content-Type: text/html; charset=utf-8'); //need this to avoid encoding issues.
include("../includes/application_top_noAuth.php");
include_once("navbar/navbar.php");
include_once("loadContent.php");

//Get content based on language
$content = loadContent("thankyou.json");
mysql_query('SET NAMES utf8;');

$passkey = mysql_real_escape_string($_GET['passkey']);
if($_SESSION[$passkey] == true) $bookingStatus = "success";
else $bookingStatus = completeBooking($passkey);

if($bookingStatus != "success") redirect($content, $bookingStatus);

function completeBooking($passkey)
{
    if(!$passkey) // just redirect if there's no booking
    {
        $booking_status = '';
        return $booking_status;
    }
    $sql = '
        SELECT * 
            FROM customerregs_temp1 
                WHERE ConfirmCode = "' . $passkey . '"';// Get customer's booking
    $booking = reset(queryForRows($sql)); // reset to get first element of "array" (should only be one element long anyway)
    if(!$booking)
    {
        $booking_status = 'Booking not found';
        return $booking_status;
    }

    //add byamba - 20191213 check expired booking data ↓    
    $sql = "SELECT * FROM customerregs_temp1 
                WHERE ConfirmCode = '" . $passkey . "' and Expired = '1' ";   // Check expired booking

    $res = mysql_query($sql) or die(mysql_error());
    if(mysql_num_rows($res) > 0)
    {
       $content = loadContent("thankyou.json");
       //$expiredMsg = 'Booking is expired';      
       return $content['expiredMsg'];     
    } 
    //end ↑

    $sql = '
        SELECT maxJumpers - IFNULL(takenSlots, 0) as freeSlots
        FROM
            (SELECT value as maxJumpers, site_id from configuration 
                WHERE `key` = "online_slots" AND  site_id = "' . $booking['site_id'] . '") a
        LEFT JOIN 
            (SELECT site_id, SUM(NoOfJump) AS takenSlots
            FROM customerregs1 
                WHERE BookingDate = "' . $booking['BookingDate'] . '"
                AND BookingTime   = "' . $booking['BookingTime'] . '"
                AND  site_id      = "' . $booking['site_id']     . '") b
        ON a.site_id = b.site_id
    ';// Check if how many free slots are open at that time (in case it has filled up)
    $openSlots = reset(queryForRows($sql)); // reset to get first element of "array" (should only be one element long anyway)
    if(!$openSlots)
    {
        $booking_status = 'Booking Search Error';
        return $booking_status;
    }    

    if($booking['NoOfJump'] <= $openSlots['freeSlots'])
    {
        $code = $booking['ConfirmCode'];
        unset($booking['CustomerRegID']);
        unset($booking['ConfirmCode']);
        
        unset($booking['Expired']); //byamba -add 20191213 set null value to Expired column

        // fill default values
        $booking['OtherNo'] = '';
        $booking['BookingType'] = 'Website';
        $booking['Rate'] = getFirstJumpRate($booking['site_id'], $booking['BookingDate']);
        $booking['RateToPay'] = 0;
        $booking['BookingReceived'] = date("Y-m-d H:i:s");

        //if this is a not a credit card booking, use these defaults
        if ($booking['Agent'] != "Credit Card") {
            $booking['CollectPay'] = 'Onsite';
            $booking['Agent'] = 'NULL';
            $booking['Notes'] = 'Internet (' . date("Y/m/d") . ')';
        }
        
        if($booking['discount']) $booking['Rate'] -= $booking['discount'];
        if($booking['discount']) $booking['Notes'] .= $booking['discount_code'] . "Discount" . "(" . $booking['discount'] . yen .")";
        unset($booking['discount']);                

        if ($r = db_perform('customerregs1', $booking)) //Check if making a booking worked then make an event
        {
            $booking_status = 'success';
            $sql = 'DELETE FROM customerregs_temp1 WHERE ConfirmCode = "' . $code .'"';
            mysql_query($sql);
            $_SESSION[$code] = true;
            echo "
                    <script>
                        window.dataLayer = window.dataLayer || [];
                        window.dataLayer.push({
                        'event': 'Booking',
                        'bookingRate': ". $booking['Rate'] * $booking['NoOfJump'] . "
                        });
                    </script>
                ";
            if($booking['discount_code'] == "karaoke") //Disabled karaoke booking event trigger
            {
                echo "
                    <script>
                        window.dataLayer = window.dataLayer || [];
                        window.dataLayer.push({
                        'event': 'Karaoke',
                        'bookingRate': ". $booking['Rate'] * $booking['NoOfJump'] . "
                        });
                    </script>
                ";
            }
        } 
        else 
        {
            $booking_status = 'Booking Error: Could not save.';
        }
        return $booking_status;
    }
    else
    {
        $booking_status = 'Sorry, but the slot you had chosen has filled up!';
        return $booking_status;
    }
}
function redirect($content, $message) // escapes back to form start if data is missing
{
    echo '<script type="text/javascript">';
    echo 'window.location.href="' . $content['url_home'] . "&message=" . $message .'";';
    echo '</script>';
    exit;
}
?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="css/bookings.css">
<link rel="stylesheet" type="text/css" href="css/navmenu.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="jquery-dateformat.min.js"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M23LPW5');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M23LPW5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="content">
    <div class="row">    	
            <?php echo $content['heading'] ?>
    </div>
    <div class="row">    	
            <?php echo $content['subheading'] ?>
    </div>
    <div class="row">    	
            <?php echo $content['explanation_1'] ?>
    </div>
    <div class="row">    	
        <ol>
            <li><?php echo $content['conditions_1'] ?></li>
            <li><?php echo $content['conditions_2'] ?></li>
            <li><?php echo $content['conditions_3'] ?></li>
            <li><?php echo $content['conditions_4'] ?></li>
            <li><?php echo $content['conditions_5'] ?> <a href="<?php echo $content['health_link'] ?>"><?php echo $content['link_text'] ?></a></li>
        </ol>
    </div>
    <div class="row">    	
            <span><?php echo $content['explanation_2'] ?> <a href="<?php echo $content['faq_link'] ?>"><?php echo $content['link_text'] ?></a></span>
    </div>
</div>
</body>
</html>



