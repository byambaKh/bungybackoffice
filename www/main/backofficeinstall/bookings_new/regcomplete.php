<?php
header('Content-Type: text/html; charset=utf-8'); //need this to avoid encoding issues.
include("../includes/application_top_noAuth.php");
include_once("navbar/navbar.php");
include_once("loadContent.php");

//Get content based on language
$content = loadContent("confirmPage.json");
$thanksCont = loadContent("thankyou.json");

$mailContent = loadContent("mailcontent.json");
mysql_query('SET NAMES utf8;');
session_start();

$sql = '
        SELECT * 
            FROM emailregs 
                WHERE ConfirmCode = "' . mysql_real_escape_string($_SESSION['code']) . '"';// Get customer's booking
$customer = reset(queryForRows($sql)); 

//$confirmCode = createConfirmationCode();
$emailOptOut = $_POST['email_opt_out'] == "on" ? 1 : 0;

$booking = [
    'site_id'           => mysql_real_escape_string ($_SESSION['place']),    
    'BookingDate'       => mysql_real_escape_string ($_SESSION['date']),
    'BookingTime'       => mysql_real_escape_string ($_SESSION['time']),
    'NoOfJump'          => mysql_real_escape_string ($_SESSION['numPeople']),
    'RomajiName'        => mysql_real_escape_string ($_POST['lastname']) 
                           . ' ' . 
                           mysql_real_escape_string ($_POST['firstname']),
    'CustomerLastName'  => mysql_real_escape_string ($_POST['lastname']),
    'CustomerFirstName' => mysql_real_escape_string ($_POST['firstname']),
    'ContactNo'         => mysql_real_escape_string ($_POST['phone']),
    'CustomerEmail'     => mysql_real_escape_string ($_SESSION['cemail']),
    'TransportMode'     => mysql_real_escape_string ($_POST['transportation']),
    'place_id'          => mysql_real_escape_string ($_SESSION['place']), // Strictly necessary? Not sure whether place_id is ever used.
    'email_opt_out'     => $emailOptOut,
    'BookingReceived'           => date('Y-m-d H:i:s'),
    'discount'          => ($_SESSION['discount'] == "karaoke" ? 1000 : 0),
    'discount_code'      =>($_SESSION['discount'] == "karaoke" ? "karaoke" : null),

    
    'CollectPay'        => 'Onsite',
    'Agent'             => 'NULL',
    'created'           => $customer['Created'],
    'Notes'             => 'Internet (' . date("Y/m/d") . ')'
];


    $sql = '
        SELECT maxJumpers - IFNULL(takenSlots, 0) as freeSlots
        FROM
            (SELECT value as maxJumpers, site_id from configuration 
                WHERE `key` = "online_slots" AND  site_id = "' . $booking['site_id'] . '") a
        LEFT JOIN 
            (SELECT site_id, SUM(NoOfJump) AS takenSlots
            FROM customerregs1 
                WHERE BookingDate = "' . $booking['BookingDate'] . '"
                AND BookingTime   = "' . $booking['BookingTime'] . '"
                AND  site_id      = "' . $booking['site_id']     . '") b
        ON a.site_id = b.site_id
    ';// Check if how many free slots are open at that time (in case it has filled up)
    
    $openSlots = reset(queryForRows($sql)); // reset to get first element of "array" (should only be one element long anyway)
    
    //print_r($booking);

    if($booking['NoOfJump'] <= $openSlots['freeSlots'])
    {
        $code = mysql_real_escape_string ($_SESSION['code']);       
        //unset($booking['ConfirmCode']);       
       
        // fill default values
        $booking['OtherNo'] = '';
        $booking['BookingType'] = 'Website';
        $booking['Rate'] = getFirstJumpRate($booking['site_id'], $booking['BookingDate']);
        $booking['RateToPay'] = 0;   

        if($booking['discount']) $booking['Rate'] -= $booking['discount'];
        if($booking['discount']) $booking['Notes'] .= $booking['discount_code'] . "Discount" . "(" . $booking['discount'] . yen .")";
        unset($booking['discount']);    

        sendEmail($booking, $mailContent);   

        //print_r($booking);
        if ($_SESSION['booking_saved'] == false) //Check if making a booking worked then make an event
        {
            if($r = db_perform('customerregs1', $booking))
            {
                $booking_status = 'success';
                $sql = 'DELETE FROM emailregs WHERE ConfirmCode = "' . $code .'"';
                mysql_query($sql);
                //$_SESSION[$code] = true;
                echo "
                        <script>
                            window.dataLayer = window.dataLayer || [];
                            window.dataLayer.push({
                            'event': 'Booking',
                            'bookingRate': ". $booking['Rate'] * $booking['NoOfJump'] . "
                            });
                        </script>
                    ";
                if($booking['discount_code'] == "karaoke") //Disabled karaoke booking event trigger
                {
                    echo "
                        <script>
                            window.dataLayer = window.dataLayer || [];
                            window.dataLayer.push({
                            'event': 'Karaoke',
                            'bookingRate': ". $booking['Rate'] * $booking['NoOfJump'] . "
                            });
                        </script>
                    ";
                }
            }else 
            {
                $booking_status = 'Booking Error: Could not save.';
                redirect($content['url'].mysql_real_escape_string ($_SESSION['code']), $booking_status);  
            } 
            
            $_SESSION['booking_saved'] = true;
        }    

    }else
    {
        $booking_status = 'Sorry, but the slot you had chosen has filled up!';
        redirect($content['url'].mysql_real_escape_string ($_SESSION['code']), $booking_status);  
    }
    //echo  $booking_status;

//saveBooking($booking);
//sendEmail($booking, $mailContent);

function saveBooking($booking)
{
    if($_SESSION['booking_saved'] == false) $result = db_perform('customerregs_temp1', $booking);
    $_SESSION['booking_saved'] = true; // Prevent refreshing repeating db entries.
}
function sendEmail($booking, $mailContent)//Compiles an email. Uses string replacement to fill in customer booking details needed in the email.
{
    if($_SESSION['mail_sent'] == false)
    {
        $emailLink    = $mailContent['url_' . $booking['site_id']] . $booking['ConfirmCode'];
        $message_body = $mailContent['content_' . $booking['site_id']];
        $message_body = str_replace("{EMAIL_BOOK_DATE}",         $booking['BookingDate'], $message_body);
        $message_body = str_replace("{EMAIL_BOOK_TIME}",         $booking['BookingTime'], $message_body);
        $message_body = str_replace("{EMAIL_JUMP_NUMBER}",       $booking['NoOfJump'],    $message_body);
        $message_body = str_replace("{EMAIL_CUSTOMER_NAME}",     $booking['RomajiName'],  $message_body);
        $message_body = str_replace("{EMAIL_CONFIRMATION_LINK}", $emailLink,              $message_body);       
        $formatted_text = chunk_split(base64_encode($message_body));
        $boundary = "boundary-" . rand(1000, 9999) . '-' . rand(100000, 999999);
        $headers =  "Return-Path: confirm@bungyjapan.com\r\n";
        $headers .= "Reply-To: " . $mailContent['return_address_' . $booking['site_id']]. "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";
        $headers .= "Content-Type: multipart/alternative;\r\n";
        $headers .= "\tboundary=\"$boundary\"\r\n";
        $message_body = <<<EOT
--$boundary
Content-Type: text/html; charset="UTF-8"
Content-Transfer-Encoding: base64

$formatted_text

--$boundary--
EOT;
        mail($booking['CustomerEmail'], 
            '=?UTF-8?B?' . base64_encode($mailContent['subject']) . '?=', 
            $message_body, 
            $headers, 
            "-f confirm@bungyjapan.com");
    }
    $_SESSION['mail_sent'] = true; // Prevent refreshing repeating emails.
}
function redirect($content, $message) // escapes back to form start if data is missing
{
    echo '<script type="text/javascript">';
    echo 'window.location.href="' . $thanksCont['url_home'] . "&message=" . $message .'";';
    echo '</script>';
    exit;
}

?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="css/bookings.css">
<link rel="stylesheet" type="text/css" href="css/navmenu.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="jquery-dateformat.min.js"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M23LPW5');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M23LPW5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="content">
    <div class="row">       
            <?php echo $thanksCont['heading'] ?>
    </div>
    <div class="row">       
            <?php echo $thanksCont['subheading'] ?>
    </div>
    <div class="row">       
            <?php echo $thanksCont['explanation_1'] ?>
    </div>
    <div class="row">       
        <ol>
            <li><?php echo $thanksCont['conditions_1'] ?></li>
            <li><?php echo $thanksCont['conditions_2'] ?></li>
            <li><?php echo $thanksCont['conditions_3'] ?></li>
            <li><?php echo $thanksCont['conditions_4'] ?></li>
            <li><?php echo $thanksCont['conditions_5'] ?></li>
            <li><?php echo $thanksCont['conditions_6'] ?><a href="<?php echo $thanksCont['health_link'] ?>"><?php echo $thanksCont['link_text'] ?></a></li>
        </ol>
    </div>
    <div class="row">       
            <span><?php echo $thanksCont['explanation_2'] ?> <a href="<?php echo $thanksCont['faq_link'] ?>"><?php echo $thanksCont['link_text'] ?></a></span>
    </div>
</div>
</body>
</html>

