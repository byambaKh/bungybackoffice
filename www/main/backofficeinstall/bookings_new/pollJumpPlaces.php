<?php
include("../includes/application_top_noAuth.php");
$sql = '
SELECT firstresult.site_id, firstresult.BookingDate, firstresult.BookingTime, freeslots - fullslots AS slots
    FROM
    (
    SELECT a.site_id, a.BookingDate, a.BookingTime, IFNULL(b.slots, 0) AS fullslots
        FROM 
            (SELECT site_id, BOOK_DATE AS BookingDate, BOOK_TIME AS BookingTime 
            FROM Time_State 
                WHERE OPERATION_DESC = "Time ON" 
                AND BOOK_DATE >= NOW()) a
        LEFT JOIN 
            (SELECT site_id, BookingDate,  BookingTime, SUM(NoOfJump)AS slots 
            FROM bungyjapan_book_main.customerregs1 
            GROUP BY site_id, BookingDate,  BookingTime) b
        ON a.site_id = b.site_id AND a.BookingDate = b.BookingDate AND a.BookingTime = b.BookingTime
    ) firstresult
    LEFT JOIN
        (SELECT value as freeslots, site_id
        FROM configuration
            WHERE `key` = "online_slots") freeslottable
    ON firstresult.site_id = freeslottable.site_id
INNER JOIN 
    calendar_state AS cal
ON cal.site_id = firstresult.site_id AND cal.BOOK_DATE = firstresult.BookingDate AND cal.OPERATION_DESC = "Set LOCK OFF"
';//Slightly complex SQL - combines four tables to get open times, booked times, maximum jumper slots and whether the day is locked in order to retrieve the free times for future bookings. This query can be run as is to check its results.
$result = queryForRows($sql);
session_start();
$_SESSION['freeJumpTimes'] = $result;
?>