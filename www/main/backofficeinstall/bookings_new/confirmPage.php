<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
}
header('Content-Type: text/html; charset=utf-8'); //need this to avoid encoding issues.
include("../includes/application_top_noAuth.php");
include_once("navbar/navbar.php");
include_once("loadContent.php");

$mailContent = loadContent("firstmail.json");
$content = loadContent("confirmPage.json");

mysql_query('SET NAMES utf8;');

	if(isset($_POST['submit']))
	{
		// Fetching variables of the form which travels in URL	
		$email = $_POST['email'];

		if (filter_var($email, FILTER_VALIDATE_EMAIL)) 
		{
			function createConfirmationCode()//Can these collide? Probably won't ever happen.
			{
			    $confirmCode = md5(uniqid(rand()));
			    return $confirmCode;
			}



function sendEmail($booking, $mailContent)//Compiles an email. Uses string replacement to fill in customer booking details needed in the email.
{	
    if($_SESSION['mail_sent'] == false)
	{ 
		$emailLink    = $mailContent['url'] .$booking['ConfirmCode'];       
		$message_body1 = $mailContent['content_1'];	
		$message_body2.= str_replace("{EMAIL_CONFIRMATION_LINK}", $emailLink,  $message_body1);       
		$formatted_text = chunk_split(base64_encode($message_body2));
		$boundary = "boundary-" .rand(1000, 9999). '-' . rand(100000, 999999);
		$headers =  "Return-Path: confirm@bungyjapan.com\r\n";
		$headers .= "Reply-To: " .$mailContent['return_address_1']. "\r\n";
		$headers .= "MIME-Version: 1.0\r\n";
		$headers .= "Content-Type: multipart/alternative;\r\n";
		$headers .= "\tboundary=\"$boundary\"\r\n";
		
		$message_body = <<<EOT
--$boundary
Content-Type: text/html; charset="UTF-8"
Content-Transfer-Encoding: base64

$formatted_text

--$boundary--
EOT;
	
	mail($booking['CustomerEmail'], 
		    '=?UTF-8?B?' . base64_encode($mailContent['subject']) . '?=', 
			$message_body, 
			$headers, 
			"-f confirm@bungyjapan.com");		     
    }    
	$_SESSION['mail_sent'] = true; // Prevent refreshing repeating emails.
}	

		function saveBooking($booking)
		{
		    if($_SESSION['booking_saved'] == false) $result = db_perform('emailregs', $booking);
		    $_SESSION['booking_saved'] = true; // Prevent refreshing repeating db entries.
		}	
			
			$confirmCode = createConfirmationCode();
			$booking = [		    
			    'ConfirmCode'       => $confirmCode,
			    'CustomerEmail'		=> mysql_real_escape_string ($email),
			    'Created'			=> date('Y-m-d H:i:s')
			];

			saveBooking($booking);
			sendEmail($booking, $mailContent);	

			unset($_POST['email']);

		} else {	
			echo "<script>history.go(-1);</script>";		
		    //header("Location: " . $_SERVER['HTTP_REFERER']);
			//exit;			
		}
		
	}else{
		echo '<script type="text/javascript">';
    	echo 'window.location.href="' . $mailContent['url_home'] .'";';
   		echo '</script>';
    	exit;
	}	



?>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="stylesheet" type="text/css" href="css/bookings.css">
<link rel="stylesheet" type="text/css" href="css/navmenu.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="jquery-dateformat.min.js"></script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-M23LPW5');</script>
<!-- End Google Tag Manager -->
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M23LPW5"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div class="content">
    <div class="row">    	
           <?php echo $content['explanation_1'] ?>
    </div>
    <div class="row">    	
           <?php echo $content['explanation_2'] ?>
    </div>
    <div class="row">    	
           <BR><?php echo $content['explanation_3'] ?>
    </div>
    <div class="row">    	
           <?php echo $content['explanation_4'] ?>
    </div>
    <div class="row">    	
           <BR><?php echo $content['explanation_5'] ?>
    </div>
</div>
</body>
</html>