<ul id="main-menu" class="">
<?php show_menu($main_menu_items); ?>
</ul>
<script>
$.ui.menu.prototype._open = function( submenu ) {
		var position = null;
		if (submenu.parents('ul').length > 1) {
			position = { my: "left top", at: "right top" };
		} else {
			position = { my: "left top", at: "left bottom" };
		}
		position = $.extend({
			of: this.active
		}, position );

		clearTimeout( this.timer );
		this.element.find( ".ui-menu" ).not( submenu.parents( ".ui-menu" ) )
			.hide()
			.attr( "aria-hidden", "true" );

		submenu
			.show()
			.removeAttr( "aria-hidden" )
			.attr( "aria-expanded", "true" )
			.position( position );
};
$( "#main-menu" ).menu({ 
	position: { my: "left top", at: "left bottom" } 
});
</script>
<?php 
  function show_menu($menu_items, $level = 0) {
	foreach ($menu_items as $menu_item) {
		if ( array_key_exists('perm', $menu_item) 
				&& (!array_key_exists('user_access', $_SESSION) || !in_array($menu_item['perm'], $_SESSION['user_access']))
			) {
			// do not show this link and all sub-menu
			continue;
		};
		$menu_link = '#';
		$menu_title = '';
		if (array_key_exists('title', $menu_item)) $menu_title = $menu_item['title'];
		if (array_key_exists('link', $menu_item)) $menu_link = $menu_item['link'];
		echo str_repeat("\t", $level);
		echo '<li><a href="' . $menu_link . '">' . $menu_title . '</a>';
		if (!empty($menu_item['sub'])) {
			echo "\n";
			echo str_repeat("\t", $level+1);
			echo "<ul>\n";
			show_menu($menu_item['sub'], $level+2);
			echo str_repeat("\t", $level+1);
			echo "</ul>\n";
			echo str_repeat("\t", $level);
		};
		echo "</li>\n";
	};
  }
?>
