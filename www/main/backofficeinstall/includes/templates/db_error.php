<?php
?>
<!DOCTYPE html>
<html>
  <head>
<?php include 'includes/header_tags.php'; ?>
	<title><?php echo SYSTEM_SUBDOMAIN; ?> System </title>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<META HTTP-EQUIV="REFRESH" CONTENT="30">
<?php include 'includes/head_scripts.php'; ?>
<style>
h1, h2, h3 {
    color: white;
}
button:disabled div {
    color: silver;
}
div {
    text-align: center;
}
</style>
</head>
<body bgcolor=black>
<div>
<img src="/img/index.jpg">
<h1>System Busy</h1>
<h2>We`re sorry, system is temporarily unavailable.</h2>
<h3>This page will be reloaded in 30 sec.</h3>
<br><br>
</div>
</body></html>
