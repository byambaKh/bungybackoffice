みなかみバンジージャンプご予約確認内容自動送信メール
注意：　まだ予約は完了しておりません。

この度は、群馬県みなかみバンジージャンプへお申し込みいただきありがとうございます。
以下の内容をご確認ください。

ご予約内容が正しい場合は、内容の後にあるリンクをクリックすると、ご予約が確定されます。

【場所】 　群馬県みなかみ町　（４２ｍ）

【予約日】 {EMAIL_BOOK_DATE} (金)

【集合時間】 {EMAIL_BOOK_TIME}

【予約人数】  {EMAIL_JUMP_NUMBER}人

【ご予約代表者名】 {EMAIL_CUSTOMER_NAME} 様

ご予約内容が正しければ下のリンクをクリックしてください。

{EMAIL_CONFIRMATION_LINK}

間違いがある場合はお手数ですが www.bungyjapan.com に戻って始めからやり直してください。

【集合場所】
〒379-1612
群馬県利根郡みなかみ町小日向143

※お車の場合は「みなかみの森」バンジー専用駐車場をご利用ください。受付から徒歩2分程度です（無料）。

バンジージャパン
０２７８－７２－８１３３