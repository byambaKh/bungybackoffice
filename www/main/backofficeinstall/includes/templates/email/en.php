Bungy Japan Registration Confirmation Email.
Thank you very much for registering with Bungy Japan.

Please check the details of your booking as they appear below.

If correct, please click on the link provided below to complete your booking.

If there is an error or you would like to make changes to your booking details, please DO NOT click on the link provided below. Simply delete this email and return to http://www.bungyjapan.com to make a new booking.

Booking Site: Minakami, Gunma (42 m)

Booking Date :  {EMAIL_BOOK_DATE}

Booking Time : {EMAIL_BOOK_TIME}

No. of Jumpers : {EMAIL_JUMP_NUMBER}

Name : {EMAIL_CUSTOMER_NAME}

Phone No : {EMAIL_CUSTOMER_PHONE}

If the above information is correct, please click on the link below to complete your booking.

{EMAIL_CONFIRMATION_LINK}

Our Location:
Bungy Japan
143 Obinata, Minakami-machi, Tone-gun, Gunma-ken, 379-1612

〒３７９－１６１２
群馬県利根郡みなかみ町小日向143
バンジージャパン

Our Phone Number: 0278 - 72 - 8133

If you are coming by car, simply exit at the Minakami IC, turn left at the lights and follow the road to the big, white bridge. Free parking is available around the corner from our office at Minakami no Mori ( みなかみの森 ).