<!DOCTYPE html>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta name="viewport" content="width=device-width">
    <meta http-equiv="X-UA-Compatible" content="IE=8">
    <link rel="stylesheet" href="/Booking/css/style.css" type="text/css"/>
    <title><?php echo ($lang == 'en') ? 'Bungy Japan Login' : 'バンジージャパン　エージェントシステム '; ?></title>
</head>
<body>
<style>
    .error {
        color: red;
        width: 100%;
        text-align: center;
    }

    div {
        text-align: center;
    }
</style>
<form method="post" name="aform">
    <?php
    if (isset($_SESSION['login_message'])) {
        echo "<div id='login_error' class='error'>" . $_SESSION['login_message'] . '</div>';
        unset($_SESSION['login_message']);
    };
    ?>
    <div>
        <center>
            <table align="center" cellpadding="0" cellspacing="0" width="310px">
                <tbody>
                <tr>
                    <td align="center" bgcolor="#000000" colspan="6">
                        <h3>
                            <font color="#FFFFFF"><?php echo ($lang == 'en') ? 'Bungy Japan User Login' : 'バンジージャパン　エージェントシステム '; ?></font>
                        </h3>
                    </td>
                </tr>

                <tr>
                    <td align="right" bgcolor="#B22222" valign="middle">
                        <font color="#FFFFFF"><?php echo ($lang == 'en') ? 'User Name' : 'ユーザーネーム '; ?>:</font>
                    </td>
                    <td align="center" bgcolor="#B22222" valign="middle">
                        <input type="text" name="auth_user" size="19" autofocus>
                    </td>
                </tr>
                <tr>
                    <td align="right" bgcolor="#B22222" valign="middle">
                        <font color="#FFFFFF"><?php echo ($lang == 'en') ? 'Password' : 'パスワード '; ?>:</font>
                    </td>
                    <td align="center" bgcolor="#B22222" valign="middle">
                        <input type="password" size="19" name="auth_pass">
                    </td>
                </tr>                
                <tr>
                    <td align="center" bgcolor="#000000" colspan="6" valign="middle" style="padding-bottom: 5px; padding-top: 5px">
                        <input type="submit" style="width:76;background-color:#FFFF66" name="login" value="<?php echo ($lang == 'en') ? 'Login' : 'ログイン '; ?>">
                    </td>
                </tr>
                </tbody>
            </table>
        </center>
    </div>
</form>
</body>
</html>
