<?php
require_once("../includes/application_top.php");


$passkey = $_GET['passkey'];

// try to fetch temp record first
$sql = "SELECT * FROM customerregs_temp1 WHERE confirmcode = '" . $passkey . "'";
$result = mysql_query($sql);
$error_code = "success";
$booking = array();
if ($result && ($booking = mysql_fetch_assoc($result))) {
    // check total of registered jumps for this date/time
    $total = 0;
    $sql = "SELECT SUM(NoOfJump) as total
			from customerregs1 
			where 
				site_id = '" . CURRENT_SITE_ID . "' and
				DeleteStatus = 0 and
				BookingDate = '{$booking['BookingDate']}' and 
				BookingTime = '{$booking['BookingTime']}'";
    $res = mysql_query($sql);
    if ($row = mysql_fetch_assoc($res)) {
        $total = $row['total'];
    };
    // check if this tel number already have jump registered with status not deleted and not checked-out
    $total_registered = 0;
    $sql = "SELECT count(*) AS total FROM customerregs1
			WHERE
				ContactNo = '" . db_input($booking['ContactNo']) . "'
				AND DeleteStatus = 0
				AND Checked = 0
				AND bookingDate >= '" . date("Y-m-d") . "'";
    $res = mysql_query($sql);

    if ($row = mysql_fetch_assoc($res)) {
        $total_registered += $row['total'];
    };

    if($booking['Agent'] === "Credit Card") $total_registered = 0;//allow credit card to make multiple bookings in a day

    if ($total + $booking['NoOfJump'] > $config['online_slots']) {
        // if currently booked plus this booking > 6 then deny booking
        $error_code = "denied";

    } else if ($total_registered > 0) {
        // if registered no deleted and no checkedout records exists - deny booking
        $error_code = "denied_phone";
    } else {
        // normal booking process
        // delete temp record
        $sql = "DELETE FROM customerregs_temp1 WHERE CustomerRegID = '" . $booking['CustomerRegID'] . "'";
        mysql_query($sql);
        // unset temp ID and ConfirmCode
        unset($booking['CustomerRegID']);
        unset($booking['ConfirmCode']);
        // fill other default values
        $booking['OtherNo'] = '';
        $booking['BookingType'] = 'Website';
        $booking['Rate'] = $config['first_jump_rate'];
        $booking['RateToPay'] = 0;
        $booking['BookingReceived'] = date("Y-m-d h:i:s");


        //if this is a not a credit card booking, use these defaults
        //else these things have already been defined for us in the
        //paymentreturn.php page
        if ($booking['Agent'] != "Credit Card") {
            $booking['CollectPay'] = 'Onsite';
            $booking['Agent'] = 'NULL';
            $booking['Notes'] = 'Internet (' . date("Y/m/d") . ")";
        }

        if ($r = db_perform('customerregs1', $booking)) {
            $error_code = 'success';
        } else {
            $error_code = 'notsaved';
        }
        $booking['status_code'] = $error_code;
        send_backup_email($booking);
    }
} else {
    $error_code = "incorrect";
}

// logging
//$fp = fopen("../registration.log", "a+");
//fputs($fp, date("Y-m-d H:i:s - ") . $_SERVER['REMOTE_IP'] . " - " . $error_code. "\n");
//fputs($fp, print_r($_REQUEST, true));
//fputs($fp, print_r($booking, true));
//fputs($fp, "==============================================================================================================\n");
//fclose($fp);
