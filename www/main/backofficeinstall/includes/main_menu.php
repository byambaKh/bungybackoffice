<?php
	$main_menu_items = array(
		array(
			'title'	=> t('Reception'),
			'perm'	=> 'Reception',
			'sub'	=> array(
				array(
					'title'	=> t('Make Booking'),
					'link'	=> '/Reception/makeBookingIE.php'
				),
				array(
					'title'	=> t('Change Booking'),
					'link'	=> '/Reception/changeBookingMain.php'
				),
				array(
					'title'	=> t('Cancel Booking'),
					'link'	=> '/Reception/cancelBookingMain.php'
				),
				array(
					'title'	=> t('Daily View'),
					'link'	=> '/Reception/dailyViewIE.php'
				),
			),
		),
		array(
			'title'	=> t('Management'),
			'perm'	=> 'Manage',
			'sub'	=> array(
				array(
					'title'	=> t('Management Console'),
					'link'	=> '/Manage/managementConsole.php'
				),
				array(
					'title'	=> t('Days ON/OFF'),
					'link'	=> '/Manage/processCalDays.php'
				),
				array(
					'title'	=> t('Time ON/OFF'),
					'link'	=> '/Manage/processDate.php'
				),
				array(
					'title'	=> t('OnLine Booking'),
					'sub'	=> array(
						array(
							'title'	=> t('Turn System ON'),
							'link'	=> '/Manage/processSiteOn.php'
						),
						array(
							'title'	=> t('Turn System OFF'),
							'link'	=> '/Manage/processSiteOff.php'
						),
					)
				),
				array(
					'title'	=> t('User Accounts'),
					'link'	=> '/Manage/manageUserAccount.php'
				),
			)
		),
		array(
			'title'	=> t('Operations'),
			'perm'	=> 'Operations',
			'link'	=> '/Operations/operationActivities.php',
			'sub'	=> array (
				array(
					'title'	=> t('Dam Information'),
					'link'	=> '/Operations/operationActivities.php?dam_info=true'
				),
				array(
					'title'	=> t('Bookings Received'),
					'link'	=> '/Operations/operationActivities.php?last_bookings=true'
				),
				array(
					'title'	=> t('Daily View'),
					'link'	=> '/Operations/operationActivities.php'
				),
			)
		),
		array(
			'title'	=> t('Waiver'),
			'perm'	=> 'Waiver',
			'link'	=> '/Waiver/checkIN.php'
		),
		array(
			'title'	=> t('OnLine Bookings'),
			'sub'	=> array(
				array(
					'title'	=> t('English'),
					'link'	=> '/BookJPTest/searchIndex.php'
				),
				array(
					'title'	=> t('Japanese'),
					'link'	=> '/BookJPTest/searchIndex.php'
				),
			),
		),
	);
	if (array_key_exists('user_access', $_SESSION)) {
		$main_menu_items[] = array(
			'title'	=> t('Log out'),
			'link'	=> '/?logout=true'
		);
	};
	//include dirname(__FILE__) . '/templates/main_menu.php';
	function t($string) {
		return $string;
	};
?>
<div style="background-color: white;">
<?php
	//include '../includes/application_top.php';
    //This second include of application_top.php causes issues and should not happen.
    $user = new BJUser();
    if ($section != 'Calendar' && $user->hasRole(array("Staff +", "General Manager", "SysAdmin", "Site Manager", "Staff", "ManualEdit"))) {
        include "../includes/switch_site.php";
    };
    //enables site switching for accountants
    $user = new BJUser();//the previous script overwrites $user with an array
    if($section == 'Bookkeeping' && $user->hasRole(array("Accountant"))) include "../includes/switch_site.php";
?>
<div style="width: 20px; margin-left: auto; margin-right: auto;"><a href="/"><img src="/img/toolbar_home.png"></a></div>
</div>
<br clear="both">
<?php 
if (in_array(SYSTEM_SUBDOMAIN_REAL, array('TEST'))) {
	$places = BJHelper::getPlaces();
	$query_string = array_filter(array_map(function ($key, $value) {
		if ($key == 'site') return false;
		if (is_scalar($value)) {
			return "&$key=".urlencode($value);
		};
		return false;
	}, array_keys($_GET), $_GET));
	echo "<div style='width: 800px;margin-left: auto; margin-right: auto; text-align: center;'>";
	foreach ($places as $place) {
		echo ($place['id'] == CURRENT_SITE_ID) ? '<b>' : "<a href=?site={$place['subdomain']}".implode('', $query_string).">";
		echo $place['name'];
		echo ($place['id'] == CURRENT_SITE_ID) ? '</b>' : "</a> ";
		echo " ";
	};
	echo "</div>";
};
?>
