<?php
function draw_agent_drop_down($field_name, $field_value = 'NULL', $parameters = '')
{
    global $config;
    $agents = getAgents();
    $parameters .= " id='$field_name'";
    echo draw_pull_down_menu($field_name, $agents, $field_value, $parameters);
}

function draw_pull_down_menu($name, $values, $default = '', $parameters = '', $sjis = true) {
    global $_GET, $_POST;

    $field = '<select name="' . $name . '"';

    if (!empty($parameters)) $field .= ' ' . $parameters;

    $field .= '>';

    if (empty($default) && ((isset($_GET[$name]) && is_string($_GET[$name])) || (isset($_POST[$name]) && is_string($_POST[$name])))) {
        if (isset($_GET[$name]) && is_string($_GET[$name])) {
            $default = stripslashes($_GET[$name]);
        } elseif (isset($_POST[$name]) && is_string($_POST[$name])) {
            $default = stripslashes($_POST[$name]);
        }
    }

    $selected_found = false;
    for ($i = 0, $n = sizeof($values); $i < $n; $i++) {
        $field .= '<option value="' . htmlspecialchars($values[$i]['id']) . '"';
        if ($default == $values[$i]['id'] && !$selected_found) {
            $field .= ' selected="selected"';
            $selected_found = true;
        }
        if (array_key_exists('status', $values[$i]) && $values[$i]['status'] == 0) {
            $field .= ' disabled="disabled"';
        };

        if (array_key_exists('subdomain', $values[$i]) && $values[$i]['subdomain'] != '') {
            $field .= ' subdomain="' . $values[$i]['subdomain'] . '"';
        };
        if (array_key_exists('price', $values[$i]) && $values[$i]['price'] != '') {
            $field .= ' price="' . $values[$i]['price'] . '"';
        };
        if (array_key_exists('background', $values[$i]) && $values[$i]['background'] != '') {
            $field .= ' style="background-color: #' . $values[$i]['background'] . ' !important;"';
        };

        //$field .= '>' . htmlspecialchars($values[$i]['text']) . '</option>';
        //this causes issues on the server or remote
        if($sjis)
            $field .= '>' . htmlspecialchars($values[$i]['text'], ENT_HTML5, "Shift_JIS", true) . '</option>';
        else
            $field .= '>' . $values[$i]['text'] . '</option>';

    }
    if (!$selected_found && !empty($default)) {
        $field .= '<option value="' . htmlspecialchars($default) . '"';
        $field .= ' selected="selected"';
        $field .= '>' . htmlspecialchars($default) . '</option>';
    };
    $field .= '</select>';

    return $field;
}

function draw_input_field($name, $id, $report, $params = '', $directly = true)
{
    if (!empty($params)) $params = ' ' . $params;
    $value = '';
    if (array_key_exists($name, $report)) {
        $value = $report[$name];
    };
    $str = "<input name=\"i[$name]\" id=\"$id\" value=\"$value\"$params>";
    if ($directly) {
        echo $str;
    } else {
        return $str;
    }
}

function draw_textarea($name, $id, $report, $params = '')
{
    if (!empty($params)) $params = ' ' . $params;
    $value = '';
    if (array_key_exists($name, $report)) {
        $value = $report[$name];
    };
    echo "<textarea name=\"i[$name]\" id=\"$id\"$params>$value</textarea>";

}

function draw_select($name, $values, $id, $report, $params = '', $directly = true)
{
    if (!empty($params)) $params = ' ' . $params;
    if (!empty($id)) $params .= ' id="' . $id . '"';
    $value = '';
    $value_exists = true;
    if (array_key_exists($name, $report)) {
        $value = $report[$name];
        $value_exists = false;
    };
    $result = "<select name=\"i[$name]\"$params>";
    foreach ($values as $v) {
        if (!is_array($v)) {
            $v = array(
                'id' => $v,
                'value' => $v
            );
        };
        if (!array_key_exists('value', $v) && array_key_exists('text', $v)) {
            $v['value'] = $v['text'];
        };
        $selected = ($v['id'] == $value) ? ' selected="selected"' : '';
        if (!empty($selected)) {
            $value_exists = true;
        };
        $result .= "<option value=\"{$v['id']}\"$selected>{$v['value']}</option>";
    };
    if (!$value_exists) {
        $result .= "<option value=\"{$value}\" selected=\"selected\">{$value}</option>";
    };
    $result .= "</select>";
    if ($directly) {
        echo $result;
    } else {
        return $result;
    };
}

?>
