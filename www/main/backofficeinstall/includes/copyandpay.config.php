<?php
                    $url = "https://inpendium.net/frontend/GenerateToken";
                    $sender = "8a8394c1477d5dd001478f5cc518387f";
                    $userLogin = "8a8394c1477d5dd001478f5cc5193883";
                    $userPwd = "GkS4R2bj";
                    $secret = "Hm35nkwxBMWWEddt";
                    $currency = "JPY";
                    $chanel = $config['copyandpay_channel'];

                    $data = array(
			"SECURITY.SENDER" => $sender,
                        "TRANSACTION.CHANNEL" => $chanel,
			"IDENTIFICATION.TRANSACTIONID" => time(),
                        "USER.LOGIN" => $userLogin,
                        "USER.PWD" => $userPwd,
                        //"USER.SECRET" => $secret,
                        "PAYMENT.TYPE" => "DB",
                        "PRESENTATION.AMOUNT" => $_SESSION['total_price'],
                        "PRESENTATION.CURRENCY" => $currency,
                        "NAME.GIVEN" => mb_convert_encoding($_SESSION['address_last'], "UTF-8", $_SESSION['address_encoding']),
                        "NAME.FAMILY" => mb_convert_encoding($_SESSION['address_first'], "UTF-8", $_SESSION['address_encoding']),
                        "ADDRESS.STREET" => trim(
				mb_convert_encoding($_SESSION['address_street'], "UTF-8", $_SESSION['address_encoding'])
				. " " .
				mb_convert_encoding($_SESSION['address_street2'], "UTF-8", $_SESSION['address_encoding'])
			),
                        "ADDRESS.ZIP" => mb_convert_encoding($_SESSION['address_zip'], "UTF-8", $_SESSION['address_encoding']),
                        "ADDRESS.CITY" => mb_convert_encoding($_SESSION['address_city'], "UTF-8", $_SESSION['address_encoding']),
                        "ADDRESS.STATE" => mb_convert_encoding($_SESSION['address_state'], "UTF-8", $_SESSION['address_encoding']),
                        "CONTACT.PHONE" => mb_convert_encoding($_SESSION['address_phone'], "UTF-8", $_SESSION['address_encoding']),
                        "CONTACT.MOBILE" => $_SESSION['address_mobile'] ? 
				mb_convert_encoding($_SESSION['address_mobile'], "UTF-8", $_SESSION['address_encoding'])
				:
				mb_convert_encoding($_SESSION['address_phone'], "UTF-8", $_SESSION['address_encoding']),
                        "ADDRESS.COUNTRY" => "JP",
                        "CONTACT.EMAIL" => $_SESSION['regemail'],
		    );

                    if ($config['copyandpay_mode'] == 'TEST') {
                        $data['TRANSACTION.MODE'] = "INTEGRATOR_TEST";
                    } else {
                        $data['TRANSACTION.MODE'] = "LIVE";
                    };

		    $_SESSION['copyandpay_token_request'] = $data;

                    $params = array('http' => array(
                        'method' => 'POST',
                        'content' => http_build_query($data),
                    ));
                    $ctx = stream_context_create($params);
                    $fp = @fopen($url, 'rb', false, $ctx);
                    if (!$fp) {
                        throw new Exception("Problem with $url, $php_errormsg");
                    }
                    $response = @stream_get_contents($fp);
                    if ($response === false) {
                        throw new Exception("Problem reading data from $url, $php_errormsg");
                    }

                    $responseJson = json_decode($response, true);
                    $token = $responseJson['transaction']['token'];
                    if ($config['copyandpay_mode'] == 'TEST') {
                        echo "<div class='alert'>Please note, Credit card processing in TEST mode, <br />no payment will be processed</div>";
                    };

?>
