<?php
// places selection
require_once('../includes/classes/domain.php');

$places = BJHelper::getPlaces();
//move headoffice to the bottom of the menu
$mainMenuSite = $places[0];
array_splice($places, 0, 1);
array_push($places, $mainMenuSite);

$user = new BJUser();

$allowed_sites = array();
$addedMain = false;
foreach ($places as $p) {
    //if ($p['subdomain'] != 'main' && $p['hidden'] == 1) continue;
    //currently the hidden column will decide if the domain appears in the site switch AND in the booking menu. We dont want this
    //showInSiteSwitch allows you to show subdomains in the site switchmenu only
    if ($p['subdomain'] != 'main' && $p['showInSiteSwitchMenu'] == 0) continue;
    //if ($p['active'] == 0) continue;
    if ($user->hasPermission($section, $p['id'])) {//if the user has permission to be on that part of the site in the for that site_id
        //if subdomain is main and user is not a SysAdmin, Site Manager, GM, ignore
        $isMainDomain = ($p['subdomain'] == 'main');
        $isInventorySection = ($section == 'Inventory');
        $isSiteManager = ($user->hasRole(array('Site Manager')));
        $isSysAdminOrGeneralManager = ($user->hasRole(array('SysAdmin', 'General Manager')));
        $isHiroshi = (!$user->getUserName() == 'Hiroshi');

        //prevent SM's from seeing 'Head Office' if they are not Hiroshi on the Inventory System
        //if ($isMainDomain && !$isSysAdminOrGeneralManager && !$isHiroshi) continue;
        if ($isMainDomain && !$isSysAdminOrGeneralManager && !$isHiroshi && $isInventorySection && $isSiteManager) continue;

        if($p['subdomain'] == 'main') $addedMain = true;
        $allowed_sites[] = array('id' => $p['subdomain'], 'text' => $p['display_name'], 'subdomain' => $p['subdomain']);
    }
}

if(!$addedMain && $section == 'Roster') {
    $allowed_sites[] = array('id' => 0, 'text' => 'Head Office', 'subdomain' => 'main');
}

if (count($allowed_sites) > 1) {
    $sql = "SELECT * from users WHERE UserID = '{$_SESSION['uid']}';";
    $res = mysql_query($sql) or die(mysql_error());
    if ($user = mysql_fetch_array($res)) {
        $query_string = array();
        foreach ($_GET as $k => $v) {
            $query_string[] = $k . '=' . rawurlencode($v);
        };
        if (!empty($query_string)) {
            $query_string = '?' . implode("&", $query_string);
        } else {
            $query_string = '';
        };
        ?>
        <div id="switch-site" style="float: right;">
            <form style="display: inline-block;" method="post">
                Switch Site: <?php echo draw_pull_down_menu('target_site', $allowed_sites, strtolower(SYSTEM_SUBDOMAIN), 'id="target_site" style="width: 150px; border: 1px solid; height: auto !important;"'); ?>
            </form>
        </div>
        <script>
            <?php
            $domainObject = new Domain;
            $domain = $domainObject->getDomain();
            $location = $_SERVER['PHP_SELF'] . $query_string;//append the get parameters to the current location of the script
            ?>

            var bjuser = '<?php echo $user['UserName']; ?>';
            var bjpass = '<?php echo $user['UserPwd']; ?>';
            $(document).ready(function () {
                $('#target_site').change(function () {
                    var form = $('#switch-site > form');
                    form.append($('<input type=hidden name=auth_user>').val(bjuser));
                    form.append($('<input type=hidden name=auth_pass>').val(bjpass));
                    form.prop('action', 'http://' + $(this).children(':selected').attr('subdomain') + '.<?php echo $domain; ?>' + '<?php echo $location; ?>');
                    form.submit();
                });
            });
        </script>
    <?php
    };
};
?>
