<?php
	function get_list_values($list_alias, $id_column = 'id') {
		if (empty($list_alias)) return false;
		$sql = "SELECT li.* 
			FROM lists l 
			LEFT JOIN lists_items li on (l.id = li.list_id)
			WHERE l.alias = '$list_alias'";
		$res = mysql_query($sql) or die(mysql_error());
		$values = array();
		while ($row = mysql_fetch_assoc($res)) {
			$values[] = array(
				'id'	=> $row[$id_column],
				'text'	=> $row['value']
			);
		};
		mysql_free_result($res);
		return $values;
	}
?>
