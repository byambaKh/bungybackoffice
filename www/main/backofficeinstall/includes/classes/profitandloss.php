<?php
//namespace Finance;
require_once("income.php");

class ProfitAndLoss
{
    static $sql;
    private $tableData;
    private $config;
    private $siteId;
    private $year;
    private $shortSiteName;
    private $shouldCalculateTotal;
    private $currentSiteDisplayName;
    private $sites;

    public function __construct($siteId, $year, $total = false)
    {
        $this->siteId = $siteId;
        $this->sites = $this->getAllSites();
        $this->year = $year;
        $this->tableData = $this->initTableData();
        //$this->config = $this->getConfiguration($siteId);
        if ($total)
            $this->currentSiteDisplayName = "All Sites";
        else
            $this->currentSiteDisplayName = $this->sites[$siteId]['display_name'];

        $this->shouldCalculateTotal = false;

        if ($total) $this->shouldCalculateTotal = true;

        //if we are on a site without a shortcode column in the expenses or banking table let shortname be null
        //this is used in calculating partial expenses from the banking and expenses tables
        $shortNames = [1 => 'mk', 2 => 'sg', 3 => 'ib', 4 => 'ky'];

        $this->shortSiteName = null;
        if (array_key_exists($siteId, $shortNames)) {
            $this->shortSiteName = $shortNames[$siteId];//site_id's start from 1
        }

        $this->calculateIncomeAndExpense();
    }

    private function getAllSites()
    {
        $places = queryForRows("SELECT * FROM sites ORDER BY id");

        $allPlaces = [];
        foreach ($places as $key => $place) {
            $allPlaces[$place['id']] = $place;
        }

        return $places;
    }

    protected function initTableData()
    {
        $tableData = [
            'income'   => [
                'headers'     => [
                    0 => ["INCOME", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "TOTALS"],
                ],
                'bungy'       => [
                    'onsite'  => [
                        'jumps'         => [],
                        'cancellations' => [],
                        'total'         => [],
                    ],
                    'offsite' => [
                        'jumps'         => [],
                        'cancellations' => [],
                        'total'         => [],
                    ],
                    'total'   => [],
                ],
                'merchandise' => [
                    'sales' => [
                        'tShirt' => [],
                        'photo'  => [],
                        'other'  => [],
                    ],
                    'total' => [],
                ],
                'total'       => [],
            ],
            'expenses' => [
                'headers'                         => [0 => ["EXPENSES", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "TOTALS"],],
                'subHeaders'                      => [0 => ["", "amount", "amount", "amount", "amount", "amount", "amount", "amount", "amount", "amount", "amount", "amount", "amount"]],
                'tourismBoardPayments'            => [],
                'incomeMinusTourismBoardPayments' => [],
                'staffSalary'                     => [],
                'siteSalary'                      => [
                    'sites' => [
                        0 => [
                            'total'  => '',
                            'people' => [
                                'name'   => 'xyz',
                                'salary' => '12356'
                            ],
                        ], //sites indexed by id or name
                    ], //by site and by person for head office
                ],
                'staffSalaryTotal'                => [],
                'expenseItems'                    => [],
                'expenseTotal'                    => [
                    'TOTAL EXPENSE', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'
                ],
            ],
            'total'    => [],
        ];

        return $tableData;
    }

    protected function calculateIncomeAndExpense()
    {
        $rates = $this->allRatesChargedThisYear();

        $onsiteJumps = $this->calculateOnsiteJumpsForAllRates($rates);
        $onsiteCancellations = $this->calculateOnsiteCancellations();

        $allOnsiteRows = array_merge($onsiteCancellations, $onsiteJumps);

        $onsiteSubtotal = $this->sumPaxIncomeRows($allOnsiteRows, "Onsite Subtotal");

        $offsiteJumps = $this->calculateOffsiteJumpsForAllRates($rates);
        $offsiteCancellations = $this->calculateOffsiteCancellations();

        $allOffsiteRows = array_merge($offsiteCancellations, $offsiteJumps);
        $offsiteSubtotal = $this->sumPaxIncomeRows($allOffsiteRows, "Offsite Subtotal");

        $allBungyTotals = array_merge($offsiteSubtotal, $onsiteSubtotal);
        $bungyTotal = $this->sumPaxIncomeRows($allBungyTotals, "Bungy Total");

        $merchandise = $this->calculateMerchandise();
        $merchandiseTotal = $this->sumPaxIncomeRows($merchandise, 'Merchandise Total');
        $allIncomeRows = array_merge($bungyTotal, $merchandiseTotal);
        $incomeTotal = $this->sumPaxIncomeRows($allIncomeRows, "$this->currentSiteDisplayName Income Total");

        $tourismBoardPayments = $this->calculateTourismBoardPaymentFromBanking();
        $tourismBoardPaymentTotal = $this->sumExpenseRows($tourismBoardPayments, "$this->currentSiteDisplayName Tourism Board Payments");
        $incomeMinusTourismBoard = $this->subtractExpenseRows($incomeTotal, $tourismBoardPayments, "Income Minus Tourism Board Payments");

        $expensesInternationalRemit = $this->calculateInternationalRemit();
        $expensesItems = $this->calculateExpenseItems();
        array_push($expensesItems, $expensesInternationalRemit[0]);

        $expensesItemsTotal = $this->sumExpenseRows($expensesItems, "Expense Items Total");
        $expensesStaffSalary = $this->calculateStaffSalary();
        $expensesLoans = $this->calculateExpenseLoans();//

        $expensesStaffSalaryTotal = $this->sumExpenseRows($expensesStaffSalary, "Wages For Staff");

        $expensesTotal = $this->addExpenseRows($expensesItemsTotal, $expensesStaffSalaryTotal);
        $expensesTotal = $this->addExpenseRows($expensesTotal, $expensesLoans);
        $expensesTotal = $this->addExpenseRows($expensesTotal, $tourismBoardPaymentTotal, "$this->currentSiteDisplayName Expenses Total");

        $incomeMinusExpenses = $this->subtractExpenseRows($incomeTotal, $expensesTotal, "$this->currentSiteDisplayName Income - Expenses");

        $totalProfitAndLoss = $incomeTotal[0][13]['amount'] - $tourismBoardPayments[0][13]['amount'] - $expensesTotal[0][13]['amount'];

        /*********************************************************************************************************************************/

        $this->tableData['income']['bungy']['onsite']['jumps'] = $onsiteJumps;
        $this->tableData['income']['bungy']['onsite']['cancellations'] = $onsiteCancellations;
        $this->tableData['income']['bungy']['onsite']['total'] = $onsiteSubtotal;

        $this->tableData['income']['bungy']['offsite']['jumps'] = $offsiteJumps;
        $this->tableData['income']['bungy']['offsite']['cancellations'] = $offsiteCancellations;
        $this->tableData['income']['bungy']['offsite']['total'] = $offsiteSubtotal;

        $this->tableData['income']['bungy']['total'] = $bungyTotal;

        $this->tableData['income']['merchandise']['sales'] = $merchandise;
        $this->tableData['income']['merchandise']['total'] = $merchandiseTotal;
        $this->tableData['income']['total'] = $incomeTotal;

        $this->tableData['expenses']['tourismBoardPayments'] = $tourismBoardPayments;
        $this->tableData['expenses']['incomeMinusTourismBoardPayments'] = $incomeMinusTourismBoard;
        $this->tableData['expenses']['staffSalary'] = $expensesStaffSalary;
        $this->tableData['expenses']['staffSalaryTotal'] = $expensesStaffSalaryTotal;
        $this->tableData['expenses']['itemised']['items'] = $expensesItems;
        $this->tableData['expenses']['itemised']['total'] = $expensesItemsTotal;
        $this->tableData['expenses']['loans'] = $expensesLoans;
        $this->tableData['expenses']['internationalRemit'] = $expensesInternationalRemit;

        $this->tableData['expenses']['total'] = $expensesTotal;
        $this->tableData['total'][] = $totalProfitAndLoss;

        $this->tableData['incomeMinusExpenses']['total'] = $incomeMinusExpenses;

        return;
    }

    /**********************************************************************************************************************/
    /*******************************************INCOME FUNCTIONS***********************************************************/
    /**********************************************************************************************************************/

    protected function allRatesChargedThisYear()
    {
        global $config;

        $sql = "SELECT DISTINCT Rate
		FROM customerregs1
		WHERE
			DeleteStatus = 0
            #{$this->siteFilterSql('AND')}
			AND Checked = 1
			AND BookingDate like '$this->year-%'
			AND NoOfJump > 0
			AND Rate > 0
			AND CollectPay = 'Onsite'
		ORDER BY Rate DESC;";
        $rates = queryForRows($sql);//Put this in to the loop;

        $output = [];
        foreach ($rates as $rate) {
            $output[] = (int)$rate['Rate'];
        }
        $siteRates = [7500, 8000, 10000, 15000, 12000, 1000, 12000, 15000, 7000];

        $output = array_merge($siteRates, $output);
        //At least have the first jump rate so that projected jumps can be calculated
        if (count($output) == 0) $output[] = $config['first_jump_rate'];
        $output = array_unique($output);
        rsort($output);

        return $output;
    }

    private function siteFilterSql($prepend = '', $append = '', $tablePrefix = '')
    {
        if ($tablePrefix)
            $tablePrefix .= '.';//add dot to the prefix

        if ($this->shouldCalculateTotal) {
            return '';//don't filter by any site
        } else {
            return "$prepend {$tablePrefix}site_id = $this->siteId $append";//filter by the specified site
        }
    }

    protected function calculateOnsiteJumpsForAllRates($rates)
    {
        $startDate = date("Y-m-d");//today
        $endDate = "{$this->year}-12-31";//end of the year
        /*
        //$rate = $this->getFirstJumpRateForSite();
        //note if start date exceeds end date, nothing is returned ie, it will only return values for this year
        $sqlProjectedJumps = "
            SELECT
            DATE_FORMAT(roster_date, '%m') as yearmonth,
            sum(jumps) as pax,
            sum(jumps * $rate) as amount
            FROM roster_target
            WHERE
            roster_date > '$startDate'
            AND site_id = $site
            AND roster_date <= '$endDate'
            group by yearmonth; ";
        */

        $allJumpsSql = "
        SELECT
         	SUM(pax) AS pax,
         	SUM(IF(source='roster', pax * `value`, amount)) AS amount,
         	IF(source='roster', `value`, Rate) AS Rate,
         	yearmonth
        FROM  (
        (
        SELECT
            DATE_FORMAT(BookingDate, '%m') as yearmonth,
            NoOfJump as pax,
            NoOfJump * Rate as amount,
            Rate,
            site_id,
            'bookings' AS source
        FROM customerregs1
        WHERE
            DeleteStatus = 0
             {$this->siteFilterSql('AND')}
            AND Checked = 1
            AND BookingDate like '$this->year%'
            AND NoOfJump > 0
            AND Rate > 0
            AND CollectPay = 'Onsite'
        )

        UNION ALL

        (SELECT
            DATE_FORMAT(BookingDate, '%m') as yearmonth,
            2ndj_qty as pax,
            2ndj as amount,
            ROUND(2ndj/2ndj_qty) AS Rate,
            site_id,
            'bookings' AS source
        FROM customerregs1
        WHERE
            DeleteStatus = 0
             {$this->siteFilterSql('AND')}
            AND Checked = 1
            AND BookingDate LIKE '$this->year%'
            AND CollectPay = 'Onsite'
            And 2ndj_qty > 0
            And 2ndj > 0

        )

        UNION ALL

        (SELECT  DATE_FORMAT(sale_time, '%m') as yearmonth,
            sale_total_qty_2nd_jump as pax,
            sale_total_2nd_jump as amount,
            ROUND(sale_total_2nd_jump/sale_total_qty_2nd_jump) AS Rate,
            site_id,
            'merchandise' AS source
        FROM merchandise_sales
        WHERE
            sale_time like '$this->year%'
             {$this->siteFilterSql('AND')}
            AND sale_total_2nd_jump > 0
            AND sale_total_qty_2nd_jump > 0
        )
        UNION ALL
 		#projections
 		(
        SELECT
            DATE_FORMAT(roster_date, '%m') as yearmonth,
            jumps as pax,
            0 as amount,
            0 AS Rate,
            site_id,
            'roster' AS source
        FROM roster_target
        WHERE
            roster_date > '$startDate'
             {$this->siteFilterSql('AND')}
            AND site_id <> 0 #Ignore projections on main
            AND roster_date <= '$endDate'

        )
        )
   AS all_jumps
     LEFT JOIN
	configuration ON (configuration.site_id = all_jumps.site_id AND configuration.key = 'first_jump_rate')
	GROUP BY Rate, yearmonth
	ORDER BY yearmonth;
        ";

        //create an array of rates
        $blankPaxRow = $this->fillMissingIncomeMonths([]);
        $data = [];
        foreach ($rates as $rate) {
            $data[(int)$rate] = $blankPaxRow;
            $data[(int)$rate][0] = $rate;//set the title column to the rate
        }
        unset($rate);

        //write the data from the database over it
        $allJumpsResult = queryForRows($allJumpsSql);
        foreach ($allJumpsResult as $jumpsAndRates) {
            $month = $jumpsAndRates['yearmonth'];
            $rate = $jumpsAndRates['Rate'];
            $data[(int)$rate][(int)$month]['pax'] = $jumpsAndRates['pax'];
            $data[(int)$rate][(int)$month]['amount'] = $jumpsAndRates['amount'];
        }
        unset($rate);

        foreach ($data as $rate => $pricesAndMonths) {
            $data[(int)$rate] = $this->fillMissingIncomeMonths($pricesAndMonths);
            $data[(int)$rate][0] = $rate;//set the title column to the rate
            $data[(int)$rate][13] = $this->totalPaxIncomeHorizontal($data[(int)$rate]);
        }

        krsort($data);//key reverse sort

        return $data;
    }

    protected function fillMissingIncomeMonths($row = [], $additionalParameters = [])
    {
        $blankPax = [
            0  => "unset",
            1  => ['pax' => 0, 'yearmonth' => 1, 'amount' => 0],
            2  => ['pax' => 0, 'yearmonth' => 2, 'amount' => 0],
            3  => ['pax' => 0, 'yearmonth' => 3, 'amount' => 0],
            4  => ['pax' => 0, 'yearmonth' => 4, 'amount' => 0],
            5  => ['pax' => 0, 'yearmonth' => 5, 'amount' => 0],
            6  => ['pax' => 0, 'yearmonth' => 6, 'amount' => 0],
            7  => ['pax' => 0, 'yearmonth' => 7, 'amount' => 0],
            8  => ['pax' => 0, 'yearmonth' => 8, 'amount' => 0],
            9  => ['pax' => 0, 'yearmonth' => 9, 'amount' => 0],
            10 => ['pax' => 0, 'yearmonth' => 10, 'amount' => 0],
            11 => ['pax' => 0, 'yearmonth' => 11, 'amount' => 0],
            12 => ['pax' => 0, 'yearmonth' => 12, 'amount' => 0],
            13 => ['pax' => 0, 'yearmonth' => 13, 'amount' => 0],
        ];

        //Add additional entries to the blank array
        //This is so that the blank array rows can have the form:
        // 1  => ['pax' => 0, 'yearmonth' => 1,  'amount' => 0, 'extra1' => '', 'extra2' => ''],
        foreach ($blankPax as $key => &$entry) {
            if ($key == 0) continue;

            $entry = array_merge($entry, $additionalParameters);
        }

        $data = array_replace($blankPax, $row);

        return $data;
    }

    protected function totalPaxIncomeHorizontal($row)
    {
        $horizontalSum = [
            'pax'    => 0,
            'amount' => 0
        ];

        foreach ($row as $index => $cell) {
            if ($index == 0) continue; //ignore the row title

            $horizontalSum['pax'] += $cell['pax'];
            $horizontalSum['amount'] += $cell['amount'];
        }

        return $horizontalSum;
    }

    protected function calculateOnsiteCancellations()
    {
        $data = [];
        $sqlOnsiteCancellations = "
            SELECT
                DATE_FORMAT(BookingDate, '%m') AS yearmonth,
                SUM(CancelFeeQTY) AS pax,
                SUM(CancelFee * CancelFeeQTY) AS amount
            FROM customerregs1
            WHERE
                {$this->siteFilterSql('', 'AND')}
                BookingDate LIKE '$this->year-%'
                AND Checked = 1
                AND CancelFeeCollect = 'Onsite'
                AND NoOfJump > 0
            GROUP BY yearmonth
            ORDER BY yearmonth ASC;
		";

        $onsiteCancellations = queryForRows($sqlOnsiteCancellations);
        foreach ($onsiteCancellations as $onsiteCancellation) {
            $data[(int)$onsiteCancellation['yearmonth']] = $onsiteCancellation;
        }

        $data[13] = $this->totalPaxIncomeHorizontal($data);
        $data = $this->fillMissingIncomeMonths($data);
        $data [0] = "Onsite Cancellations";

        return [$data];//return an array of rows (one row in this case)
    }

    protected function sumPaxIncomeRows($rows, $title = "sumPaxIncomeRows() output")
    {
        $output = [];

        $output[0] = $title;
        foreach ($rows as $row) {
            foreach ($row as $cellIndex => $cell) {
                if ($cellIndex == 0) continue;//ignore the title cell
                $output[$cellIndex]['pax'] += $cell['pax'];
                $output[$cellIndex]['amount'] += $cell['amount'];
            }
        }

        return [$output];
    }

    //Takes an array of rows, sums them and returns the result

    protected function calculateOffsiteJumpsForAllRates($rates)
    {
        $allJumpsSql = "
        SELECT SUM(pax) AS pax, SUM(amount) AS amount, Rate, yearmonth FROM	(
        (SELECT
            DATE_FORMAT(BookingDate, '%m') as yearmonth,
            NoOfJump as pax,
            NoOfJump * Rate as amount,
            Rate
        FROM customerregs1
        WHERE
            DeleteStatus = 0
            {$this->siteFilterSql('AND')}
            AND Checked = 1
            AND BookingDate like '$this->year%'
            AND NoOfJump > 0
            AND Rate > 0
            AND CollectPay = 'Offsite'
        )

        UNION ALL

        (SELECT
            DATE_FORMAT(BookingDate, '%m') as yearmonth,
            2ndj_qty as pax,
            2ndj as amount,
            ROUND(2ndj/2ndj_qty) AS Rate
        FROM customerregs1
        WHERE
            DeleteStatus = 0
            {$this->siteFilterSql('AND')}
            AND Checked = 1
            AND BookingDate LIKE '$this->year-%'
            AND CollectPay = 'Offsite'
            And 2ndj_qty > 0
            And 2ndj > 0

        )
    ) t_union
    GROUP BY Rate, yearmonth
    ORDER By yearmonth;

        ";

        //create an array of rates
        $blankPaxRow = $this->fillMissingIncomeMonths([]);
        $data = [];
        foreach ($rates as $rate) {
            $data[(int)$rate] = $blankPaxRow;
            $data[(int)$rate][0] = $rate;//set the title column to the rate
        }
        unset($rate);

        //write the data from the database over it
        $allJumpsResult = queryForRows($allJumpsSql);
        foreach ($allJumpsResult as $jumpsAndRates) {
            //Make sure month keys are integers or else bad things happen.
            $month = (int)$jumpsAndRates['yearmonth'];
            $rate = (int)$jumpsAndRates['Rate'];
            $data[$rate][$month]['pax'] = $jumpsAndRates['pax'];
            $data[$rate][$month]['amount'] = $jumpsAndRates['amount'];
        }
        unset($rate);

        foreach ($data as $rate => $pricesAndMonths) {
            $rate = (int)$rate;
            $data[$rate] = $this->fillMissingIncomeMonths($pricesAndMonths);
            $data[$rate][0] = $rate;//set the title column to the rate
            $data[$rate][13] = $this->totalPaxIncomeHorizontal($data[$rate]);
        }

        krsort($data);//key reverse sort

        return $data;
    }

    protected function calculateOffsiteCancellations()
    {
        $sqlOffsiteCancellations = "SELECT
			DATE_FORMAT(BookingDate, '%m') AS yearmonth,
			SUM(CancelFeeQTY) AS pax,
			SUM(CancelFee * CancelFeeQTY) AS amount
		FROM customerregs1
		WHERE
			BookingDate LIKE '$this->year-%'
		    {$this->siteFilterSql('AND')}
			AND CancelFeeCollect = 'Offsite'
		GROUP BY yearmonth
		ORDER BY yearmonth ASC;";

        $offsitePaxAndAmounts = queryForRows($sqlOffsiteCancellations);
        foreach ($offsitePaxAndAmounts as $offsitePaxAndAmount) {
            $data[(int)$offsitePaxAndAmount['yearmonth']] = $offsitePaxAndAmount;
        }

        $data[0] = 'Offsite Cancellations';
        $data = $this->fillMissingIncomeMonths($data);
        $data[13] = $this->totalPaxIncomeHorizontal($data);

        return [$data];//return an array of rows (one row in this case)
    }

    //Adds two rows, each wrapped in an array, together and returns the result

    protected function calculateMerchandise()
    {
        $sqlMerchandise = "
            SELECT
                #SUM(pax + pax_photo + pax_other)  AS total_pax,
                #SUM(amount + amount_photo + amount_other)  AS total_amount,
                yearmonth,
                SUM(pax_tshirt) AS pax_tshirt,
                SUM(amount_tshirt) AS amount_tshirt,
                SUM(pax_photo) AS pax_photo,
                SUM(amount_photo) AS amount_photo,
                ROUND(SUM(pax_other)) AS pax_other,
                SUM(amount_other) AS amount_other,
                site_id
                FROM (
                    (SELECT
                        DATE_FORMAT(BookingDate, '%m') as yearmonth,
                        tshirt_qty as pax_tshirt,
                        tshirt as amount_tshirt,
                        ROUND(photos/3500) as pax_photo,
                        #photos_qty as pax_photo,
                        #photos as amount_photo,
                        photos_qty*500 as amount_photo,
                        /*other / 100 + video / 1 + gopro / 1 as pax_other,*/
                        /*other + video + gopro as amount_other,*/
                        other/1000 AS pax_other,
                        other AS amount_other,
                        site_id
                    FROM customerregs1
                    WHERE
                        BookingDate LIKE '$this->year%'
                        AND Checked = 1
                        AND NoOfJump > 0
                        AND DeleteStatus = 0
                    )

                    UNION ALL

                    (SELECT  DATE_FORMAT(sale_time, '%m') as yearmonth,
                            sale_total_qty_tshirt as pax_tshirt,
                            sale_total_tshirt as amount_tshirt,
                            sale_total_qty_photo as pax_photo,
                            sale_total_qty_photo * 500 as amount_photo,#company collects 500 yen for every sale
                            sale_total_qty - sale_total_qty_photo - sale_total_qty_tshirt - sale_total_qty_2nd_jump as pax_other,
                            sale_total - sale_total_photo - sale_total_tshirt - sale_total_2nd_jump as amount_other,
                            site_id
                    FROM merchandise_sales
                    WHERE
                        sale_time like '$this->year-%'
                    )

            ) t_union
                {$this->siteFilterSql('WHERE')}
            GROUP BY yearmonth
            ORDER BY yearmonth;
        ";

        $merchandiseResults = queryForRows($sqlMerchandise);

        $tShirts = $this->fillMissingIncomeMonths();
        $photos = $this->fillMissingIncomeMonths();
        $other = $this->fillMissingIncomeMonths();

        foreach ($merchandiseResults as $month) {
            $tShirts[(int)$month['yearmonth']]['pax'] = $month['pax_tshirt'];
            $tShirts[(int)$month['yearmonth']]['amount'] = $month['amount_tshirt'];

            $photos[(int)$month['yearmonth']]['pax'] = $month['pax_photo'];
            $photos[(int)$month['yearmonth']]['amount'] = $month['amount_photo'];

            $other[(int)$month['yearmonth']]['pax'] = $month['pax_other'];
            $other[(int)$month['yearmonth']]['amount'] = $month['amount_other'];
        }

        $tShirts[13] = $this->totalPaxIncomeHorizontal($tShirts);
        $photos[13] = $this->totalPaxIncomeHorizontal($photos);
        $other[13] = $this->totalPaxIncomeHorizontal($other);

        $tShirts[0] = "T-Shirts";
        $photos[0] = "Photos";
        $other[0] = "Other";

        return [
            'tShirts' => $tShirts,
            'photos'  => $photos,
            'other'   => $other
        ];
    }

    protected function calculateTourismBoardPaymentFromBanking()
    {

        $sql = "
            SELECT
               SUM(`out` -`in`) AS `amount`,
               DATE_FORMAT(`date`, '%m') AS `date`
            FROM
                banking
                WHERE analysis = 'Tourism Board Payments'
                AND `date` LIKE '$this->year-%'
                {$this->siteFilterSql('AND', '')}
            GROUP BY DATE_FORMAT(`date`, '%m')
            ORDER BY `date`;
        ";
        $results = queryForRows($sql);

        $output = [];
        foreach ($results as $resultsIndex => $result) {
            $date = (int)$result['date'];
            $output[$date]['amount'] = $result['amount'];
        }

        $output = $this->fillMissingExpenseMonths($output);
        $output[0] = "$this->currentSiteDisplayName Tourism Board Payments:";
        $output[13] = $this->totalExpenseHorizontal($output);

        return [$output];
    }

    /**********************************************************************************************************************/
    /*********************************************EXPENSE FUNCTIONS********************************************************/
    /**********************************************************************************************************************/

    protected function fillMissingExpenseMonths($row)
    {
        $blankExpense = [
            0  => "unset",
            1  => ['amount' => 0],
            2  => ['amount' => 0],
            3  => ['amount' => 0],
            4  => ['amount' => 0],
            5  => ['amount' => 0],
            6  => ['amount' => 0],
            7  => ['amount' => 0],
            8  => ['amount' => 0],
            9  => ['amount' => 0],
            10 => ['amount' => 0],
            11 => ['amount' => 0],
            12 => ['amount' => 0],
            13 => ['amount' => 0],
        ];

        $row = array_replace($blankExpense, $row);

        return $row;
    }

    protected function totalExpenseHorizontal($row)
    {
        $horizontalSum = [
            'amount' => 0
        ];

        foreach ($row as $index => $cell) {
            if ($index == 0) continue; //ignore the row title

            $horizontalSum['amount'] += $cell['amount'];
        }

        return $horizontalSum;
    }

    //Sum the total horizontally
    protected function sumExpenseRows($rows, $title = "sumExpenseRows() output")
    {
        $output = [];

        $output[0] = $title;
        foreach ($rows as $row) {
            foreach ($row as $cellIndex => $cell) {
                if ($cellIndex == 0) continue;//ignore the title cell

                if (array_key_exists('pax', $cell))
                    $output[$cellIndex]['pax'] += $cell['pax'];
                $output[$cellIndex]['amount'] += $cell['amount'];
            }
        }

        return [$output];
    }

    protected function subtractExpenseRows($rowsA, $rowsB, $title = "subTractRows() output")
    {
        $rowA = $rowsA[0];//since these are arrays of rows, take the first entry in each
        $rowB = $rowsB[0];

        $output = [];
        $output[0] = $title;
        foreach ($rowA as $index => $cell) {
            if ($index == 0) continue;
            $output[$index]['amount'] = $rowA[$index]['amount'] - $rowB[$index]['amount'];
        }

        return [$output];
    }

    protected function calculateExpenseItems()
    {
        if ($this->shortSiteName == null) {
            $sqlCombinedExpenses = "
            SELECT SUM(allExpenses.amount) AS amount,
              yearmonth,
              description,
              origin_table
              FROM (
            (SELECT
                    DATE_FORMAT(`date`, '%m') AS yearmonth,
                    description,
                    SUM(`out` - `in`) AS amount,
                    'banking' AS origin_table,
                    analysis
                FROM banking
                WHERE
                    {$this->siteFilterSql('', 'AND')}
                    `date` LIKE '$this->year-%'
                    AND (description NOT LIKE '%Wages%' AND notes <> 'Contract Services')
                GROUP BY yearmonth, description, analysis
            )
            UNION
            (SELECT
                    DATE_FORMAT(`date`, '%m') AS yearmonth,
                    description,
                    SUM(`cost`) AS amount,
                    'expenses' AS origin_table,
                    analysis
                FROM expenses
                WHERE
                    {$this->siteFilterSql('', 'AND')}
                    `date` LIKE '$this->year-%'
                    AND description NOT LIKE '%Wages%'
                GROUP BY yearmonth, description, analysis
            )
        ) as allExpenses
        WHERE
            (description NOT LIKE '%Wages%')
        	AND description NOT LIKE 'BANK TRANSFER'
        	AND description NOT LIKE 'SALES DEPOSIT%'
        	AND description NOT LIKE 'PETTY CASH REIMBURSEMENT'
        	AND description NOT LIKE 'SALES DEPOSIT FROM Rafting Company, Pension ETC For Bungy Sales'
			AND analysis NOT LIKE 'Tourism Board Payments'
        GROUP BY allExpenses.yearmonth, allExpenses.description
        ORDER BY allExpenses.description;
        ";
        } else {

            $sqlCombinedExpenses = "
        SELECT SUM(allExpenses.amount) AS amount,
          `yearmonth`,
          `description`,
          `origin_table`
          FROM (
            (SELECT
                    DATE_FORMAT(`date`, '%m') AS yearmonth,
                    description,
                    SUM(`out` - `in`) AS amount,
                    'banking' AS origin_table,
                    analysis
                FROM banking
                WHERE
                    {$this->siteFilterSql('', 'AND')}
                    `date` LIKE '$this->year-%'
                    AND (description NOT LIKE '%Wages%' AND notes <> 'Contract Services')
                    AND ($this->shortSiteName = NULL OR $this->shortSiteName = 0)
                GROUP BY yearmonth, description, analysis
            )
            UNION
            (SELECT
                    DATE_FORMAT(`date`, '%m') AS yearmonth,
                    description,
                    SUM(`cost`) AS amount,
                    'expenses' AS origin_table,
                    analysis
                FROM expenses
                WHERE
                    {$this->siteFilterSql('', 'AND')}
                    `date` LIKE '$this->year-%'
                    AND description NOT LIKE '%Wages%'
                    AND ($this->shortSiteName = NULL OR $this->shortSiteName = 0)
                GROUP BY yearmonth, description, analysis
                )

            UNION
            #partial (Percentage Expenses from the mk, sg, ib, ky columns)
            (SELECT
	                DATE_FORMAT(`date`, '%m') AS `yearmonth`,
	                description,
	                SUM($this->shortSiteName/100 * (`out` - `in`)) AS `amount`,
	                'banking' AS origin_table,
	                analysis
	          FROM banking
              WHERE`date` LIKE '$this->year-%'
                    AND ($this->shortSiteName <> NULL OR $this->shortSiteName <> 0)
                    AND (description NOT LIKE '%Wages%' AND notes <> 'Contract Services')
              GROUP BY yearmonth, description, analysis
              )
            UNION
            (SELECT
	                DATE_FORMAT(`date`, '%m') AS `yearmonth`,
	                description,
	                SUM($this->shortSiteName/100 * (`cost`)) AS `amount`,
	                'expenses' AS origin_table,
	                analysis
	          FROM expenses
              WHERE`date` LIKE '$this->year-%'
                    AND ($this->shortSiteName <> NULL OR $this->shortSiteName <> 0)
                    AND description NOT LIKE '%Wages%'
              GROUP BY yearmonth, description, analysis
            )
        ) as allExpenses
        WHERE
            (description NOT LIKE '%Wages%')
        	AND description NOT LIKE 'BANK TRANSFER'
        	AND description NOT LIKE 'SALES DEPOSIT%'
        	AND description NOT LIKE 'PETTY CASH REIMBURSEMENT'
        	AND description NOT LIKE 'SALES DEPOSIT FROM Rafting Company, Pension ETC For Bungy Sales'
			AND analysis NOT LIKE 'Tourism Board Payments'
        GROUP BY allExpenses.yearmonth, allExpenses.description
        ORDER BY allExpenses.description;
        ";
        }

        //if we are on a site without a shortName code in the expenses query, ignore the partial expenses part


        $expenses = queryForRows($sqlCombinedExpenses);

        $monthsGroupedByExpense = [];

        foreach ($expenses as $expense) {
            //For some reason Packing & Shipping and Sales & Marketing can have extra spaces leading to duplicate keys
            $description = trim($expense['description']);
            $month = (int)$expense['yearmonth'];
            $amount = (int)$expense['amount'];

            //if descriptions exists in monthsGroupedByExpense array
            if (array_key_exists($description, $monthsGroupedByExpense)) {
                //check if the month key exists in output array['description']['month']
                if (array_key_exists($month, $monthsGroupedByExpense)) {
                    //if there is already an entry for the month, add the expense to it
                    //this should never run as the query does this part already
                    $monthsGroupedByExpense[$description]['amount'] += $amount;
                } else {
                    //if it does, sum it's amount with the existing amount
                    $monthsGroupedByExpense[$description][(int)$month]['month'] = (int)$month;
                    $monthsGroupedByExpense[$description][(int)$month]['amount'] = (int)$amount;
                }

            } else {
                //if the key does not exist add it and its amount to the to the monthsGroupedByExpense array
                $monthsGroupedByExpense[$description][(int)$month]['month'] = (int)$month;
                $monthsGroupedByExpense[$description][(int)$month]['amount'] = (int)$amount;
            }

        }

        $output = [];
        foreach ($monthsGroupedByExpense as $key => $expenseType) {
            $row = $this->fillMissingExpenseMonths($expenseType);
            $row[0] = $key;//make the first value be the title. This is okay row[0] is the unset title
            $row[13] = $this->totalExpenseHorizontal($row);
            $output[] = $row;//append the row onto the list of rows

        }

        return $output;
    }

    protected function calculateStaffSalary()
    {
        $sqlSalariesNoPaymentsWithPercentages = "
        SELECT DATE_FORMAT(salary.date, '%m') as `date`, ROUND(SUM(`out`), 0) as `out`, who FROM (
            /*Payments that are not percentages*/
            (SELECT
                `date`,
                  `out` - `in` AS `out`,
                /*if(site_id = 0, ROUND((`out` - `in`)*(100-(mk+sg+ib+ky))/100) ,`out` - `in`) AS `out`,*/
                analysis,
                company AS who,
                'banking' AS origin_table
                FROM banking
                WHERE
                    (description LIKE '%wages%' OR notes = 'Contract Services')
                    {$this->siteFilterSql('AND')}
                    AND NOT
			        (
				         (mk IS NOT NULL AND mk <> 0 and mk <> '')
				      OR (sg IS NOT NULL AND sg <> 0 AND sg <> '')
				      OR (ky IS NOT NULL AND ky <> 0 AND ky <> '')
				      OR (ib IS NOT NULL AND ib <> 0 AND ib <> '')
			        )
            )

            UNION ALL

            (SELECT
                `date`,
                cost AS `out`,
                analysis,
                shop_name AS who,
                'expenses' AS origin_table
                FROM expenses
                WHERE description LIKE '%wages%'
                    {$this->siteFilterSql('AND')}
                    AND NOT
			        (
				         (mk IS NOT NULL AND mk <> 0 and mk <> '')
				      OR (sg IS NOT NULL AND sg <> 0 AND sg <> '')
				      OR (ky IS NOT NULL AND ky <> 0 AND ky <> '')
				      OR (ib IS NOT NULL AND ib <> 0 AND ib <> '')
			        )
            )
            ";

        $sqlSalariesAllPayments = "
        SELECT DATE_FORMAT(salary.date, '%m') as `date`, ROUND(SUM(`out`), 0) as `out`, who FROM (
            /*Payments that are not percentages*/
            (SELECT
                `date`,
                  `out` - `in` AS `out`,
                /*if(site_id = 0, ROUND((`out` - `in`)*(100-(mk+sg+ib+ky))/100) ,`out` - `in`) AS `out`,*/
                analysis,
                company AS who,
                'banking' AS origin_table
                FROM banking
                WHERE
                    (description LIKE '%wages%'  OR notes = 'Contract Services')
                    {$this->siteFilterSql('AND')}
            )

            UNION ALL

            (SELECT
                `date`,
                cost AS `out`,
                analysis,
                shop_name AS who,
                'expenses' AS origin_table
                FROM expenses
                WHERE
                    description LIKE '%wages%'
                    {$this->siteFilterSql('AND')}
            )
            ";
        // Payments that are percentages
        // In the case that shortSiteName is not in the array, zero is returned so the percentage calculation is
        // 0/100 not. Not an invalid column name.

        $shortSiteName = in_array($this->shortSiteName, ['mk', 'sg', 'ib', 'ky']) ? $this->shortSiteName : 0;

        $sqlPercentageSalaries = "
            UNION ALL

            (SELECT
                    `date`,
                    IF( $this->siteId = 0,
                      (`out` - `in`)*(100-(mk+sg+ib+ky))/100,
                      ($shortSiteName/100 * (`out` - `in`))
                    ) AS `out`,
	                analysis,
	                company AS `who`,
	                'banking-percentage' AS origin_table
	          FROM banking
              WHERE`date` LIKE '$this->year-%'
                    AND
                    (
                        description LIKE '%wages%'
                        OR notes = 'Contract Services'
                    )
                    AND
			        (
				      (mk IS NOT NULL AND mk <> 0 and mk <> '')
				      OR (sg IS NOT NULL AND sg <> 0 AND sg <> '')
				      OR (ky IS NOT NULL AND ky <> 0 AND ky <> '')
				      OR (ib IS NOT NULL AND ib <> 0 AND ib <> '')
			        )
            )

            UNION ALL

            (SELECT
                    `date`,
                    IF( $this->siteId = 0,
                      `cost`*(100-(mk+sg+ib+ky))/100,
                      (`cost` * $shortSiteName/100)
                    ) AS `out`,
	                analysis,
	                shop_name AS `who`,
	                'expenses-percentage' AS origin_table
	          FROM expenses
              WHERE`date` LIKE '$this->year-%'
                    AND description LIKE '%wages%'
                    AND
			        (
				      (mk IS NOT NULL AND mk <> 0 and mk <> '')
				      OR (sg IS NOT NULL AND sg <> 0 AND sg <> '')
				      OR (ky IS NOT NULL AND ky <> 0 AND ky <> '')
				      OR (ib IS NOT NULL AND ib <> 0 AND ib <> '')
			        )
            )
            ";

        $sqlGroupOrder = "
            ) AS salary
            WHERE salary.date LIKE '$this->year-%'
            group by MONTH(salary.date), `who`
            ORDER BY salary.date;

        ";

        $emptyExpenseRow = [
            0  => ' - ',
            1  => ['amount' => 0],
            2  => ['amount' => 0],
            3  => ['amount' => 0],
            4  => ['amount' => 0],
            5  => ['amount' => 0],
            6  => ['amount' => 0],
            7  => ['amount' => 0],
            8  => ['amount' => 0],
            9  => ['amount' => 0],
            10 => ['amount' => 0],
            11 => ['amount' => 0],
            12 => ['amount' => 0],
            13 => ['amount' => 0]
        ];
        //cases
        //site id is 0 (main)
        if (($this->siteId == 0) && !$this->shouldCalculateTotal) {
            $sqlSalaries = $sqlSalariesNoPaymentsWithPercentages . $sqlPercentageSalaries . $sqlGroupOrder;
            //partial payments to main
            //full payments
        } else if ($this->shortSiteName != null) {//ie is site with a column (mk/sg/ib/ky)
            $sqlSalaries = $sqlSalariesNoPaymentsWithPercentages . $sqlPercentageSalaries . $sqlGroupOrder;
            //site_id is a regular site with a column
            //full payments
            //partial payments

        } else if (($this->shortSiteName == null) && (!$this->shouldCalculateTotal)) {
            $sqlSalaries = $sqlSalariesNoPaymentsWithPercentages . $sqlGroupOrder;
            //site_id is a regular site without a column
            //full payments

        } else if ($this->shouldCalculateTotal) {

            $sqlSalaries = $sqlSalariesAllPayments . $sqlGroupOrder;
            //we are calculating the totals
            //full payments, ignore the percentages and take the whole number

        } else {
            exit("Unexpected state.");
        }

        self::$sql .= $sqlSalaries;

        $staffSalariesResults = queryForRows($sqlSalaries);

        $monthsGroupedByPerson = [];
        foreach ($staffSalariesResults as $staffSalary) {
            $who = $staffSalary['who'];
            $amount = (int)$staffSalary['out'];
            $month = (int)$staffSalary['date'];

            $monthsGroupedByPerson[$who][$month]['amount'] = $amount;
        }

        $output = [];
        foreach ($monthsGroupedByPerson as $personName => $row) {
            $outputRow = $this->fillMissingExpenseMonths($row);
            $outputRow[0] = $personName;//make the first value be the title.
            $outputRow[13] = $this->totalExpenseHorizontal($outputRow);
            $output[] = $outputRow;//append the outputRow onto the list of outputRows
        }

        if (!count($output)) $output = [$emptyExpenseRow];

        return $output;
    }

    protected function calculateExpenseLoans()
    {
        $query = "
            SELECT 
                DATE_FORMAT(`date`, '%m') AS `date`,
                `out`-`in` AS amount
                FROM 
                banking 
            WHERE 
                notes LIKE \"%loan%\" 
                {$this->siteFilterSql('AND')} 
                AND `date` LIKE '{$this->year}-%' 
            GROUP BY DATE_FORMAT(`date`, '%m') 
            ORDER BY `date` ;
        ";


        $results = queryForRows($query);

        $output = [];
        foreach ($results as $resultsIndex => $result) {
            $date = (int)$result['date'];
            $output[$date]['amount'] = $result['amount'];
        }

        $output = $this->fillMissingExpenseMonths($output);
        $output[0] = "$this->currentSiteDisplayName Loans";
        $output[13] = $this->totalExpenseHorizontal($output);

        return [$output];
    }

    protected function calculateInternationalRemit() {
        $query = "
            SELECT 
              SUM(international_remit) AS amount,
              DATE_FORMAT(`date`, '%m') AS date 
            FROM
              banking
            WHERE `date` LIKE '{$this->year}-%' 
            GROUP BY DATE_FORMAT(`date`, '%m') 
            ORDER BY `date` ;
        ";
        $results = queryForRows($query);

        $output = [];
        foreach ($results as $resultsIndex => $result) {
            $date = (int)$result['date'];
            $output[$date]['amount'] = $result['amount'];
        }

        $output = $this->fillMissingExpenseMonths($output);
        $output[0] = "$this->currentSiteDisplayName International Remit";
        $output[13] = $this->totalExpenseHorizontal($output);

        return [$output];
    }

    protected function addExpenseRows($rowsA, $rowsB, $title = "addExpenseRows() output")
    {
        $rowA = $rowsA[0];//since these are arrays of rows, take the first entry in each
        $rowB = $rowsB[0];

        $output = [];
        $output[0] = $title;
        foreach ($rowA as $index => $cell) {
            if ($index == 0) continue;
            $output[$index]['amount'] = $rowA[$index]['amount'] + $rowB[$index]['amount'];
        }

        return [$output];
    }

    public function getTableData()
    {
        return $this->tableData;
    }

    public function setTableData($tableData)
    {
        return $this->tableData = $tableData;
    }

    public function getSiteName()
    {
        return $this->currentSiteDisplayName;
    }

    /**********************************************************************************************************************/
    /*********************************************DRAW FUNCTIONS***********************************************************/
    /**********************************************************************************************************************/

    public function draw()
    {
        $currentYear = $this->year;
        $previousYear = $this->year - 1;
        $nextYear = $this->year + 1;
        $siteName = $this->currentSiteDisplayName;
        if ($this->shouldCalculateTotal == "") $siteName = "All Sites";

        //Income
        $htmlIncomeMonthHeaders = $this->drawRows($this->tableData['income']['headers'], "month-headers", "", "th", 2);
        $htmlIncomeColumnHeaders = $this->drawPaxAmountIncomeHeader();
        $htmlOnsiteJumps = $this->drawPaxAmountIncomeRows($this->tableData['income']['bungy']['onsite']['jumps']);
        $htmlOnsiteCancellations = $this->drawPaxAmountIncomeRows($this->tableData['income']['bungy']['onsite']['cancellations']);
        $htmlOnsiteSubtotal = $this->drawIncomeSubtotalRows($this->tableData['income']['bungy']['onsite']['total']);

        $htmlOffsiteJumps = $this->drawPaxAmountIncomeRows($this->tableData['income']['bungy']['offsite']['jumps']);
        $htmlOffsiteCancellations = $this->drawPaxAmountIncomeRows($this->tableData['income']['bungy']['offsite']['cancellations']);
        $htmlOffsiteSubtotal = $this->drawIncomeSubtotalRows($this->tableData['income']['bungy']['offsite']['total']);
        $htmlBungyTotal = $this->drawIncomeSubtotalRows($this->tableData['income']['bungy']['total']);

        $htmlMerchandise = $this->drawPaxAmountIncomeRows($this->tableData['income']['merchandise']['sales']);
        $htmlMerchandiseTotal = $this->drawIncomeSubtotalRows($this->tableData['income']['merchandise']['total']);

        $htmlIncomeTotal = $this->drawIncomeSubtotalRows($this->tableData['income']['total']);

        //Expenses
        $htmlExpensesMonthHeaders = $this->drawRows($this->tableData['expenses']['headers'], "expenses-month-headers", "", "th", 2);
        $htmlExpensesColumnHeaders = $this->drawAmountExpensesHeader();
        $htmlTourismBoardPayments = $this->drawExpensesRows($this->tableData['expenses']['tourismBoardPayments'], "pink", "amount", "td", 2);
        $htmlIncomeTotalMinusTourismBoardTotal = $this->drawExpensesRows($this->tableData['expenses']['incomeMinusTourismBoardPayments'], "income-total-tboard pink", "amount", "th", 2);
        $htmlStaffSalary = $this->drawExpensesRows($this->tableData['expenses']['staffSalary'], "", "amount", "td", 2);
        $htmlStaffSalaryTotal = $this->drawExpensesRows($this->tableData['expenses']['staffSalaryTotal'], "income-total-tboard pink", "amount", "td", 2);
        $htmlExpenseItems = $this->drawExpensesRows($this->tableData['expenses']['itemised']['items'], "", "amount", "td", 2);
        $htmlExpenseItemsTotal = $this->drawExpensesRows($this->tableData['expenses']['itemised']['total'], "expenses-subtotal-header", "amount", "td", 2);
        $htmlExpenseLoans = $this->drawExpensesRows($this->tableData['expenses']['loans'], "expenses-subtotal-header light-blue", "amount", "td", 2);
        $htmlExpensesTotal = $this->drawExpensesRows($this->tableData['expenses']['total'], "income-total-tboard pink", "amount", "td", 2);
        $htmlIncomeMinusExpenses = $this->drawExpensesRowsColor($this->tableData['incomeMinusExpenses']['total'], "income-total-tboard pink", "amount", "td", 2);
        $htmlProfitAndLossTotal = $this->drawProfitAndLossTotal($this->tableData['total']);

        $output = $this->drawForwardAndBackLinks($previousYear, $currentYear, $nextYear);

        $output .= "</th>
        </tr>
        $htmlIncomeMonthHeaders
        <!--<tr class='column-headers'>-->
        $htmlIncomeColumnHeaders
        <!--</tr>-->
        $htmlOnsiteJumps
        $htmlOnsiteCancellations
        $htmlOnsiteSubtotal
        $htmlOffsiteJumps
        $htmlOffsiteCancellations
        $htmlOffsiteSubtotal
        $htmlBungyTotal
        $htmlMerchandise
        $htmlMerchandiseTotal
        $htmlIncomeTotal
        <tr>
            <th class='divider' colspan='27'>&nbsp</th>
        </tr>
        $htmlExpensesMonthHeaders
        $htmlExpensesColumnHeaders
        $htmlTourismBoardPayments
        <!--$htmlIncomeTotalMinusTourismBoardTotal-->
        $htmlStaffSalary
        $htmlStaffSalaryTotal
        $htmlExpenseItems
        $htmlExpenseItemsTotal
        $htmlExpenseLoans
        $htmlExpensesTotal
        $htmlIncomeMinusExpenses
        $htmlProfitAndLossTotal
        </table>
        ";

        return $output;
    }

    public function drawRows($rows, $rowClass = '', $cellClass = '', $cellType = 'td', $colspan = 1)
    {
        $output = '';
        foreach ($rows as $indexRows => $row) {//Iterate through each of the rows

            $output .= "<tr class='$rowClass'>\n";
            foreach ($row as $indexRow => $cell) {//Iterate through each of the current row's cells
                if ($indexRow == 0) {
                    $output .= "<$cellType>$cell</$cellType>\n";

                } else {
                    $output .= "<$cellType colspan='$colspan' class='$cellClass'>$cell</$cellType>\n";

                }
            }
            $output .= "</tr>\n";
        }

        return $output;
    }

    public function drawPaxAmountIncomeHeader()
    {
        $output = "<tr class='column-headers'>\n";
        foreach ($this->tableData['income']['headers'] as $index => $cell) {
            if ($index == 0) {
                $output .= "<th class='first-col'></th>\n";

            } else {
                $output .= "<th class='pax'>pax</th>";
                $output .= "<th class='amount'>amount</th>\n";

            }
        }

        return $output . "</tr>\n";
    }

    public function drawPaxAmountIncomeRows($rows)
    {
        $output = '';
        foreach ($rows as $index => $row) {
            $output .= "<tr>\n";

            foreach ($row as $indexRow => $cell) {
                if ($indexRow == 0) {
                    $output .= "<td class='first-col'>$cell</td>\n";

                } else {
                    if ($cell['pax'] == 0) $cell['pax'] = '-';
                    if ($cell['amount'] == 0) $cell['amount'] = '-';

                    $output .= "<td class='pax'>{$cell['pax']}</td>";
                    $output .= "<td class='amount'>{$this->makeReadable($cell['amount'])}</td>\n";
                }
            }

            $output .= "</tr>\n";
        }

        return $output;
    }

    protected function makeReadable($number)
    {
        return number_format((double)$number);
    }

    public function drawIncomeSubtotalRows($rows)
    {
        $output = '';
        foreach ($rows as $rowsIndex => $row) {
            $output = "<tr class='bungy-total'>\n";
            foreach ($row as $index => $cell) {
                if ($index == 0) {
                    $output .= "<th class='subtotal-header'>$cell</th>\n";

                } else {
                    $output .= "<th class='pax'>{$cell['pax']}</th>";
                    $output .= "<th class='amount'>{$this->makeReadable($cell['amount'])}</th>\n";
                }
            }
        }

        return $output . "</tr>\n";
    }

    public function drawAmountExpensesHeader()
    {
        $output = "<tr class='column-headers'>\n";
        foreach ($this->tableData['expenses']['subHeaders'][0] as $index => $cell) {
            if ($index == 0) {
                $output .= "<th class='first-col'></th>\n";

            } else {
                $output .= "<th class='amount' colspan='2'>amount</th>\n";

            }
        }

        return $output . "</tr>\n";
    }

    public function drawExpensesRows($rows, $rowClass = '', $cellClass = '', $cellType = 'td', $colspan = 1)
    {
        $output = '';
        foreach ($rows as $indexRows => $row) {

            $output .= "<tr class='$rowClass'>\n";
            foreach ($row as $indexRow => $cell) {
                if ($indexRow == 0) {
                    $output .= "<$cellType class='first-col'>$cell</$cellType>\n";

                } else {
                    $output .= "<$cellType colspan='$colspan' class='$cellClass'>{$this->makeReadable($cell['amount'])}</$cellType>\n";

                }
            }

            $output .= "</tr>\n";
        }

        return $output;
    }

    public function drawExpensesRowsColor($rows, $rowClass = '', $cellClass = '', $cellType = 'td', $colspan = 1)
    {
        $output = '';
        foreach ($rows as $indexRows => $row) {

            $output .= "<tr class='$rowClass'>\n";
            foreach ($row as $indexRow => $cell) {
                if ($indexRow == 0) {
                    $output .= "<$cellType class='first-col'>$cell</$cellType>\n";

                } else {
                    $colorClass = '';
                    if ($cell['amount'] > 0) {
                        $colorClass = 'positive';
                    } else if ($cell['amount'] < 0) {
                        $colorClass = 'negative';
                    }

                    $output .= "<$cellType colspan='$colspan' class='$cellClass $colorClass'>{$this->makeReadable($cell['amount'])}</$cellType>\n";

                }
            }

            $output .= "</tr>\n";
        }

        return $output;
    }

    protected function drawProfitAndLossTotal($value)
    {
        return "
        <tr class='profit-total'>
            <th class='subtotal-header'>Total Profit Or Loss</th>
            <th colspan='2' class='amount'>$value[0]</th>
            <th colspan='24' class='amount'>&nbsp;</th>
        </tr>";
    }

    public function drawForwardAndBackLinks($previousYear, $currentYear, $nextYear)
    {
        $urlString = '';
        if (isset($_GET['total']) && ($_GET['total'] == 'all')) $urlString .= '&total=all';
        if (isset($_GET['subdomain'])) $urlString .= "&subdomain={$_GET['subdomain']}";

        $output = "<table id='main-table'>
        <tr>
            <th class='divider' colspan='27'>
                <a href='analysis.php?year={$previousYear}{$urlString}' class='floatleft'>&lt;&lt;&lt; $previousYear</a>";

        if ($currentYear < date('Y')) {
            $output .= "<a href='analysis.php?year={$nextYear}{$urlString}' class='floatright'>$nextYear&gt;&gt;&gt; </a>";

            return $output;
        }

        return $output;
    }

    /**********************************************************************************************************************/
    /*********************************************OTHER FUNCTIONS***********************************************************/
    /**********************************************************************************************************************/

    protected function addPaxIncomeRows($rowA, $rowB)
    {
        foreach ($rowA as $index => $rowAcell) {
            if ($index == 0) continue; //ignore the row title
            if ($index == 13) break; //ignore the unused 13th column

            $rowA[$index]['pax'] += $rowB[$index]['pax'];
            $rowA[$index]['amount'] += $rowB[$index]['amount'];

        }

        return $rowA;
    }

    protected function addPaxIncomeRowsMonth($rowA, $rowB, $month)
    {
        foreach ($rowA as $index => $rowAcell) {
            if ($index == 0) continue; //ignore the row title
            if ($index <= $month) continue;//ignore month with actual data in favor of projected
            if ($index == 13) break; //ignore the unused 13th column

            $rowA[$index]['pax'] += $rowB[$index]['pax'];
            $rowA[$index]['amount'] += $rowB[$index]['amount'];

        }

        return $rowA;
    }

    protected function getSecondJumpRateForSite($siteId)
    {
        return $this->getConfigurationValue($siteId, 'second_jump_rate');
    }

    protected function getConfigurationValue($siteId, $key)
    {
        $query = "
        SELECT `value` FROM configuration
        WHERE `site_id` = $siteId
            AND `key` = '$key';
        ";

        $result = queryForRows($query);

        return $result[0]['value'];
    }

    protected function calculateOnsiteProjectedJumps($rate, $site)
    {

        $startDate = date("Y-m-d");//today
        $endDate = "{$this->year}-12-31";//end of the year

        //note if start date exceeds end date, nothing is returned ie, it will only return values for this year
        $sqlProjectedJumps = "
            SELECT
            DATE_FORMAT(roster_date, '%m') as yearmonth,
            sum(jumps) as pax,
            sum(jumps * $rate) as amount
            FROM roster_target
            WHERE
            roster_date > '$startDate'
            AND site_id = $site
            AND roster_date <= '$endDate'
            group by yearmonth; ";

        $data = [];

        $paxAndAmounts = queryForRows($sqlProjectedJumps);
        foreach ($paxAndAmounts as $paxAndAmount) {
            $paxAndAmount['type'] = 'projected';

            $paxAndAmount['yearmonth'] = (int)$paxAndAmount['yearmonth'];
            $paxAndAmount['pax'] = (int)$paxAndAmount['pax'];
            $paxAndAmount['amount'] = (int)$paxAndAmount['amount'];
            $data[(int)$paxAndAmount['yearmonth']] = $paxAndAmount;
        }
        $data = $this->fillMissingIncomeMonths($data);
        $data[13] = $this->totalPaxIncomeHorizontal($data);
        $data[0] = $rate;

        return $data;
    }

    protected function calculateTourismBoardPaymentsFast()
    {
        $startDate = date("Y-m-d");//today
        $endDate = "{$this->year}-12-31";//end of the year
        /*******************************************************************************************************************
         * Note: tb_paid jumps is not the same as insurance number for Ryujin. Ryujin only pays for jumps where the rate is
         * 15000, 14000, 13000
         *******************************************************************************************************************/
        $sql = "
        SELECT
            DATE_FORMAT(`date`, '%m') AS `date`,
            jump_data.site_id,
            conf_tb_rate.value AS tb_rate, #tourism_board rate
            SUM(tb_paid_jumps) AS tb_paid_jumps, #paid jumps
            SUM(conf_tb_rate.value * tb_paid_jumps) - ((promo + media) * 1500 + bungy * 1000) AS tb_payment, #tb
            SUM(bungy) AS bungy,
            SUM(promo) AS promo,
            SUM(media) AS media,
            SUM(cancel) AS cancel

            FROM
            (
                (
                    #free and promo jumps
                    SELECT
                        DATE_FORMAT(BookingDate, '%Y-%m-01') AS `date`,
                        site_id,
                        0 AS tb_paid_jumps,
                        SUM(IF(foc LIKE 'Bungy%', NoOfJump, 0))   AS bungy,
                        SUM(IF(foc LIKE 'Promo%', NoOfJump, 0))   AS promo,
                        SUM(IF(foc LIKE 'Media%', NoOfJump, 0))   AS media,
                        SUM(IF(foc LIKE 'Cancel%', NoOfJump, 0))  AS cancel,
                        SUM(IF(foc LIKE 'NJ%', NoOfJump, 0))  AS non_jump
                    FROM customerregs1

                    WHERE BookingDate LIKE '{$this->year}-%'
                        AND site_id NOT IN (3, 4, 10)#Does not apply to Ryujin, Itsuki or Nara
                        {$this->siteFilterSql('AND')}
                        AND Rate = 0
                        AND Checked = 1
                        AND DeleteStatus = 0
                        AND NoOfJump > 0
                    GROUP BY `date`, `site_id`
                    ORDER BY `date`
                )

                UNION ALL

                (
                    #tb_paid_jumps number MK, SG etc. They are insurance number * a flat rate from the configuration table
                    SELECT
                        DATE_FORMAT(custreg.BookingDate, '%Y-%m-01') as `date`,
                        custreg.site_id AS site_id,
                        CAST((custreg.NoOfJump /*+ IFNULL(ms.total_2nd, 0)*/) AS SIGNED)
                        - CAST(IF(custreg.Agent like '___Bungy' AND custreg.CollectPay = 'Offsite', custreg.NoOfJump, 0) AS SIGNED)/*j_test_jumps*/
                        - CAST(IF(custreg.Rate = conf.value, custreg.NoOfJump, 0) AS SIGNED)/*j_2nd_jumps conf.value = second jump rate from configuration*/
                        - CAST(IFNULL(nj.non_jump, 0) AS SIGNED) /*j_non_jumps*/

                        AS tb_paid_jumps,
                        0 AS bungy,
                        0 AS promo,
                        0 AS media,
                        0 AS cancel,
                        0 AS non_jump

                    FROM customerregs1 AS custreg
                        LEFT JOIN configuration AS conf ON (conf.site_id = custreg.site_id AND conf.key = 'second_jump_rate')

                    LEFT JOIN (
                    SELECT DISTINCT w.id, cr.BookingDate, COUNT(w.id) as non_jump, cr.CustomerRegID as nonJumpRegID
                        FROM customerregs1 cr, waivers w, non_jumpers
                        WHERE
                            cr.BookingDate LIKE '{$this->year}-%'
                            AND cr.Checked = 1
                            AND cr.DeleteStatus = 0
                            AND cr.CustomerRegID = w.bid
                            AND w.id = non_jumpers.waiver_id
                    ) nj ON (nj.BookingDate = custreg.BookingDate AND custreg.CustomerRegID = nonJumpRegID)
                    WHERE
                        custreg.BookingDate LIKE '$this->year-%'
                        AND custreg.site_id <> 3 #do not calculate for ryujin as the rules are different, we calculate in the next section
                        AND Checked = 1
                        AND DeleteStatus = 0
                        AND NoOfJump > 0
                        {$this->siteFilterSql('AND', '', 'custreg')}
                )

                UNION ALL

                (
                    #tb_paid_jumps number for ryujin
                    SELECT
                        DATE_FORMAT(BookingDate, '%Y-%m-01') AS `date` ,
                        site_id,
                        NoOfJump AS tb_paid_jumps,
                        0 AS bungy,
                        0 AS promo,
                        0 AS media,
                        0 AS cancel,
                        0 AS non_jump
                    FROM
                        customerregs1
                    WHERE
                        site_id = 3 #Only for Ryujin
                        {$this->siteFilterSql('AND')}
                        /*
                        If site_id is anything but 3, this part of the query returns zero as the first site_id is 3.
                        site_id = 3 AND site_id = 2 returns nothing
                        */
                        AND Rate IN (15000, 14000, 13000)
                        AND CollectPay = 'Onsite'#Should this be there? Shouldn't payments be for both onsite and offsite???
                        AND BookingDate LIKE '{$this->year}-%'
                        AND Checked = 1
                        AND DeleteStatus = 0
                )

                UNION ALL

                (
                    SELECT
                        DATE_FORMAT(roster_date, '%Y-%m-01') AS `date` ,
                        rt.site_id,
                        rt.jumps AS tb_paid_jumps,
                        0 AS bungy,
                        0 AS promo,
                        0 AS media,
                        0 AS cancel,
                        0 AS non_jump
                    FROM roster_target  AS rt
                    LEFT JOIN configuration AS conf_rate
                        ON (rt.site_id = conf_rate.site_id AND conf_rate.key='first_jump_rate')
                    WHERE
                        roster_date > '$startDate'
                        {$this->siteFilterSql('AND', '', 'rt')}
                        AND rt.site_id <> 0 #Ignore projections on main
                        AND roster_date <= '$endDate'
                )

            ) AS jump_data

            LEFT JOIN configuration AS conf_tb_rate

            ON (
                conf_tb_rate.site_id = jump_data.site_id
                AND conf_tb_rate.key = 'tb_payment'
            )

        GROUP BY `date`
        ORDER BY `date`;
        ";

        $results = queryForRows($sql);

        $output = [];
        foreach ($results as $resultsIndex => $result) {
            $date = (int)$result['date'];
            $output[$date]['amount'] = $result['tb_payment'];
            $output[$date]['pax'] = $result['tb_paid_jumps'];
        }

        $output = $this->fillMissingExpenseMonths($output);
        $output[0] = "$this->currentSiteDisplayName Tourism Board Payments:";

        return [$output];
    }

    protected function calculateTourismBoardPayments()
    {

        //The central function is getInsuranceTotal in Income
        //It requires the insurance number (number of people on that day/ in that month), the tourismBoard payment
        //and the freeOfCharge deduction for the day/month
        //freeOfChargeDeduction comes from

        //Find the number of different free jump types in total for the month

        //Problem definition
        /*
        The calculation for tourism board payments is complicated.

        It cannot be completed in just a single sql query. The way it is calculated differs for each site
        For a single site, it is a straight calculation
            //call the function for site_id
        For multiple sites...
            call the function for each site

         */
        $freeOfChargeDeductionDataMonthly = [];
        $insuranceNumbersMonthly = [];
        $tourismBoardPayment = null;

        if (!$this->shouldCalculateTotal) {

            $deduction = [];
            $freeOfChargeDeductionDataMonthly = Income::getFreeOfChargeDeductionDataForMonth($this->siteId, $this->year);
            foreach ($freeOfChargeDeductionDataMonthly as $yearMonth => $deductionData) {
                $deduction[$yearMonth]['amount'] = Income::freeOfChargeDeduction($this->siteId, $freeOfChargeDeductionDataMonthly[$yearMonth]);
            }

            //find the number of people to be insurance (not every jump needs to be insured)
            $insurance = [];
            $insuranceNumbersMonthly = Income::getInsuranceNoMonthly($this->siteId, $this->getFirstJumpRateForSite($this->siteId), $this->year);
            foreach ($insuranceNumbersMonthly as $yearMonth => $insuranceNumber) {
                $insurance[$yearMonth]['amount'] = $insuranceNumber['amount'];//
            }

            $tourismBoardPayment = $this->getTourismBoardRate($this->siteId);

        }

        $data = [];
        $months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];
        foreach ($months as $month) {
            $yearMonth = $this->year . $month;

            $data[(int)$month]['amount'] = Income::getInsuranceTotal(
                $this->siteId,
                $freeOfChargeDeductionDataMonthly[$yearMonth],
                $insuranceNumbersMonthly[$yearMonth]['amount'],
                $tourismBoardPayment,
                $this->year . "-" . $month//,<--need a way of supplying onsite and offsite jumps
            );
        }

        $data = $this->fillMissingExpenseMonths($data);
        $data[0] = "Tourism Board Payments";
        $data[13] = $this->totalPaxIncomeHorizontal($data);

        return [$data];
    }

    protected function getFirstJumpRateForSite($siteId)
    {
        return $this->getConfigurationValue($siteId, 'first_jump_rate');
    }

    protected function getTourismBoardRate($siteId)
    {
        return $this->getConfigurationValue($siteId, 'tb_payment');
    }

    private function getConfiguration($siteId)
    {
        $configuration = [];
        $sqlConfiguration = "select * from configuration where site_id = {$siteId};";
        $configurationResults = queryForRows($sqlConfiguration);
        foreach ($configurationResults as $configurationResult) {
            $configuration[$configurationResult['key']] = $configurationResult['value'];
        }

        return $configuration;
    }
}
