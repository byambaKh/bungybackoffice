<?php
/*
 * Class that connects using PHP 7+ mysqli instead of mysql functions
 * Note that this class uses a default encoding of utf8mb4 instead of sjis (in the older Connection class)
 */

class ConnectionMysqli{
    private $servername;
    private $username;
    private $password;
    private $database_name;
    public $configFilePath;
    private $_database_connection;
    private $_database_connection_select;

    public function __construct() {
        //$this->configFilePath = dirname(__DIR__)."/../../conf.ini";
        $this->configFilePath = CONFIGURATION_PATH;
    }

    public function getConfigVariables() {
        return parse_ini_file($this->configFilePath, TRUE);
    }

    public function dbCon($sname, $uname, $pwd) {
        $this->servername = $sname;
        $this->username = $uname;
        $this->password = $pwd;

        $this->_database_connection = mysqli_connect($sname, $uname, $pwd) or $this->_show_error();
        //or trigger_error(mysql_error(),E_USER_ERROR);
        if ($this->_database_connection) {

            return $this->_database_connection;
        } else {
            $this->_show_error();
            return null;
        }
    }

    public function _show_error() {
        include(dirname(__FILE__) . '/templates/db_error.php');
        die();
    }

    public function databaseConnectionSelect($dbname) {
        $this->database_name = $dbname;

        $this->_database_connection_select = mysqli_select_db($this->_database_connection, $this->database_name);
        return $this->_database_connection_select;
    }

    public function databaseConnectionProcess() {
        //PHP evaluates file paths relative to the including file not the file
        //included so saying ../ will give different results depending on the file
        //it is included on. You must specify the absolute path
        //dirname gets the parent directory of the current file/directory pointed to
        //this goes two levels up to the
        $configVars = $this->getConfigVariables();

        $systemDatabase = 'database';

        $hVal = $configVars['database']['host'];
        $unmVal = $configVars['database']['username'];
        $pwdVal = $configVars['database']['passwd'];


        $cons = $this->dbCon($hVal, $unmVal, $pwdVal);
        $this->databaseConnectionSelect($configVars['database']['dbname']);
        mysqli_set_charset($this->_database_connection, 'sjis');//TODO: this needs to be UTF-8
        //mysqli_set_charset('utf8mb4', $this->_database_connection);//TODO: this needs to be UTF-8
        return $cons;
    }

    public function selectQuery($sql_select, $cons) {
        $configVars = $this->getConfigVariables();

        $schVal = $configVars[SYSTEM_DATABASE]['dbname'];
        if (!is_null($cons)) {
            mysqli_select_db($schVal, $cons);
            $this->result = mysqli_query($sql_select, $cons);
        }

        return $this->result;
    }
}