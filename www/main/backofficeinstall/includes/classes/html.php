<?php
//abstract this all out of the file so that when I make changes I only edit the class
//or the file refered to by the class
//how does cakephp include run file x and place it's contents inside of the template

//TODO this should be moved to the include file
define('AppRootDirectory', dirname(dirname(dirname(__FILE__))));
//define(WEB_ROOT, );

abstract class Html{
	static $indentCount = 0;

	private static function indent(){
		$indentString = '';
		//echo self::$indentCount;
        	for($i = 0; $i < self::$indentCount; $i++) {
			$indentString .= "\t";
		}
		return $indentString;
	}

	public static function increaseIndent(){
        	self::$indentCount++;
	}

	public static function decreaseIndent(){
        	self::$indentCount--;
	}

	private static function keysAndValues($keyValueArray){
		$returnString = '';

		foreach($keyValueArray as $key => $value) {
			$value = self::webString($value); //encode any special chars as html entities
                	$returnString .= "$key = '$value' ";	
		}
		return $returnString;
	}

	public static function docType ($docType) {
		$docTypes = array (//from cakephp
			'html4-strict' 	=> '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN" "http://www.w3.org/TR/html4/strict.dtd">',
			'html4-trans' 	=> '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">',
			'html4-frame' 	=> '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Frameset//EN" "http://www.w3.org/TR/html4/frameset.dtd">',
			'html5' 	=> '<!DOCTYPE html>',
			'xhtml-strict' 	=> '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">',
			'xhtml-trans' 	=> '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">',
			'xhtml-frame' 	=> '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Frameset//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-frameset.dtd">',
			'xhtml11' 	=> '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">'
		);

		//could return a default doctype if nothing is chosen or if the desired doctype is not chosen.
		$returnString = $docTypes[$docType];
		if($returnString == null) return 'Doctype Not Found';
		else return $returnString."\n";
	}


	public static function webString($string) {
		return htmlspecialchars($string, ENT_HTML5, "UTF-8");
	}

	public static function scaleToMobile(){
        	return self::indent().'<meta name="viewport" content="width=device-width">'."\n";
	}

	public static function startPage($values = null){//echos out an opening html tag with attributes passed with values
		$returnString = self::indent()."<html>\n";
		self::increaseIndent();
		return $returnString; 
	}
	
	public static function endpage ($values = null){//echos out the closing htmltag
		self::decreaseIndent();
		return "</html>\n";
	}

	//accepts an array of keys and values
	//opens the head tag 
	public static function startHeader(){
		$returnString = self::indent()."<head>\n";
		self::increaseIndent();
		return $returnString;
	}

	public static function endHeader(){
		self::decreaseIndent();
		return self::indent()."</head>\n";
	}

	public static function meta($attributes){//accepts an array of keys and values
		return self::indent()."<meta ".self::keysAndValues($attributes).">\n";
	}

	public static function title($title){
		return self::indent()."<title>".self::webString($title)."</title>\n";
	}


	public static function css($cssScript, $embedBoolean = false){
		$indent = self::indent();//we need support for media types! and text/css
        //<!--<link rel="stylesheet" href="/css/main_menu.css" type="text/css" media="all" />-->


        	if($embedBoolean){
			return $indent."<style>\n".$cssScript."\n".$indent."</style>"."\n";
		} else {
			if($cssScript[0]=='/')//if the string is a full path to a folder 
				return $indent.'<link href="'.$cssScript.'" rel="stylesheet">'."\n";
			else 
				return $indent.'<link href="'.'/css/'.$cssScript.'" rel="stylesheet">'."\n";

		}
	}

	public static function js($jsScript, $embedBoolean = false){
		$indent = self::indent();

        	if($embedBoolean){
			return $indent."<script>\n".$indent.$jsScript."\n".$indent."</script>"."\n";
		} else {
			if($jsScript[0]=='/')//if the string is a full path to a folder 
				return $indent.'<script type="text/javascript" src="'.$jsScript.'" ></script>'."\n";
			else 
				return $indent.'<script type="text/javascript" src="'.'/js/'.$jsScript.'" ></script>'."\n";
		}
	}

	public static function vbs($vbsScript, $embedBoolean = false){
		$indent = self::indent();
                //TODO should I include this in the head parameters list for consistency? or use the extension of the script to determine the 
		//include type
        	if($embedBoolean){
			return $indent."<script language='VBScript'>\n".$indent.$vbsScript."\n".$indent."</script>"."\n";
		} else {
			if($vbsScript[0]=='/')//if the string is a full path to a folder 
				return $indent.'<script type="text/vbscript src="'.$vbsScript.'"> </script>'."\n";
			else 
			//<script type="text/vbscript" src="VBScript_file_URL">
				return $indent.'<script type="text/vbscript src="'.'/vbs/'.$vbsScript.'"> </script>'."\n";
		}
	}

	
	public static function startBody(){
		$returnString = self::indent()."<body>\n";
		self::increaseIndent();
	        return $returnString;
        }

	public static function endBody(){
		self::decreaseIndent();
		return "\n".self::indent()."</body>\n";
	}

	public static function head($title = 'Bungy Japan', $cssScripts = array(), $jsScripts = array(), $options = array()){
		$header = '';
		$header .= self::docType('html5');
		$header .= self::startPage();
		$header .= self::startHeader();
		$header .= self::meta(array('charset'=>'UTF-8'));
		$header .= self::meta(array('name'=>'author', 'content'=>'bungyjapan'));
		$header .= self::meta(array('name'=>'viewport', 'content'=>'width=device-width, initial-scale=1'));
		$header .= self::meta(array('http-equiv'=>'X-UA-Compatible', 'content'=>'IE=edge'));
		$header .= self::css('bootstrap.min.css');

		$header .= "<link rel=\"shortcut icon\" href=\"/favicon.ico\" />
		<link rel=\"apple-touch-icon\" href=\"/apple-touch-icon.png\" />";

		$header .= self::css('bjstyle.css');

		foreach($cssScripts as $key => $cssScript){
                	$header .= self::css($cssScript)."\n";
		}

		foreach($jsScripts as $key => $jsScript){
                	$header .= self::js($jsScript)."\n";
		}

		$header .= self::title($title);
		$header .= self::endHeader();
		$header .= self::startBody();

		return $header;
	}

	public static function foot(){
		$footer = self::endBody();
		$footer.= self::endPage();
		return $footer;
	}

	public static function menuPageLink($text, $url, $options = array()){
		return "<button onClick=\"document.location = '$url';\" >
			<div>$text</div>
		</button>";
	}

	public static function menuPageAction($text, $action = "", $options = array()){

	}

	public static function inputPageButton($title, $action, $width = null, $height = null, $classes = "", $id = "", $options = array()){
		if($height) $propertyHeight = "height:".$height;
		if($width) $propertyWidth = "width:".$width;
		if(count($action)) $propertyAction =  "onclick=\"$action\"";

		return "<input type=\"button\" class=\"button $classes\" id=\"$id\" value=\"$title\" $propertyAction style=\"$propertyHeight; $propertyWidth;\">";
	}

	public static function pageButton(){
		//type submit, reset, button        	
		//action
		//size
		//title
		//classes
		//id
		//options
	}

	public static function beginForm(){

	}

	public static function endForm(){

	}
}


?>
