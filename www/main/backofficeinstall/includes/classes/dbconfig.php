<?php
	class BJDBConfig extends BJObject {
		private $_config = null;
		public function __construct() {
			$config_filename = APPLICATION_DIR . 'includes/config.ini';
			if (!file_exists($config_filename)) {
				self::showException('No DB configuration file found ' . $config_filename);
			}
			$this->_config = parse_ini_file(dirname(__FILE__) . '/config.ini', TRUE);
		}
		public function getConfig($name = null) {
			if (is_null($name)) return $this->_config;
			if (!array_key_exists($name, $this->_config)) 
				self::showException("No config found for DataBase " . $name);
			return $this->_config($name);
		}
	}
?>
