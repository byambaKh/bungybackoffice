<?php

class profitAndLossCollection
{
    public static $rowTemplate = ['', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
        private $collection = [];//enable the pax columns in the income table
private $enablePaxColumn;

    public function __construct($sites, $year)
    {
        $this->enablePaxColumn = false;//shows a pax (amount) column in the income sheet
        foreach ($sites as $siteId) {
            $this->collection["sites"][$siteId] = new ProfitAndLoss($siteId, $year);
        }
        $this->collection["total"] = new ProfitAndLoss(0, $year, true);

    }

    public function drawExpensesRows($collection)
    {

    }

    public function draw()
    {
        $sites = $this->collection['sites'];
        $total = $this->collection['total'];
        $totalData = $total->getTableData();

        $headerColspan = $this->enablePaxColumn ? 2 : 1;

        $output = '';
        $output .= $this->drawTopSection("INCOME ", 'dark-green', $headerColspan);

        $onsiteSection = [
            'superSection' => [
                'headerTitle' => "All Sites Onsite Bungy:",
                'headerRows'  => $totalData['income']['bungy']['onsite']['total'][0],
                'footerTitle' => "All Sites Onsite Bungy:",
                'footerRows'  => $totalData['income']['bungy']['onsite']['total'][0],
            ],

            //All section statements must be passed through eval in the for loop
            'section'      => [
                'headerTitle' => '"$siteName Onsite:"',
                "headerRows"  => '$tableData["income"]["bungy"]["onsite"]["total"][0]',

                'sectionRows' => '$this->arrayAppend($tableData["income"]["bungy"]["onsite"]["jumps"], $tableData["income"]["bungy"]["onsite"]["cancellations"][0])',

                'footerTitle' => '"$siteName Onsite Totals:"',
                "footerRows"  => '$tableData["income"]["bungy"]["onsite"]["total"][0]'
            ],

            'colors' => [
                'darker'  => 'medium-green',
                'lighter' => 'light-green',
            ]
        ];
        $offsiteSection = [
            'superSection' => [
                'headerTitle' => "All Sites Offsite Bungy:",
                'headerRows'  => $totalData['income']['bungy']['offsite']['total'][0],
                'footerTitle' => "All Sites Offsite Bungy:",
                'footerRows'  => $totalData['income']['bungy']['offsite']['total'][0],
            ],

            //All section statements must be passed through eval in the for loop
            'section'      => [
                'headerTitle' => '"$siteName Offsite:"',
                "headerRows"  => '$tableData["income"]["bungy"]["offsite"]["total"][0]',

                'sectionRows' => '$this->arrayAppend($tableData["income"]["bungy"]["offsite"]["jumps"], $tableData["income"]["bungy"]["offsite"]["cancellations"][0])',

                'footerTitle' => '"$siteName Offsite Totals:"',
                "footerRows"  => '$tableData["income"]["bungy"]["offsite"]["total"][0]'
            ],

            'colors' => [
                'darker'  => 'medium-green',
                'lighter' => 'light-green',
            ]
        ];
        $merchandiseSection = [
            'superSection' => [
                'headerTitle' => "All Merchandise Total:",
                'headerRows'  => $totalData['income']['merchandise']['total'][0],
                'footerTitle' => "All Merchandise Total:",
                'footerRows'  => $totalData['income']['merchandise']['total'][0],
            ],

            //All section statements must be passed through eval in the for loop
            'section'      => [
                'headerTitle' => '"$siteName Merchandise:"',
                "headerRows"  => '$tableData["income"]["merchandise"]["total"][0]',

                'sectionRows' => '$tableData["income"]["merchandise"]["sales"]',

                'footerTitle' => '"$siteName Merchandise:"',
                "footerRows"  => '$tableData["income"]["merchandise"]["total"][0]'
            ],

            'colors' => [
                'darker'  => 'medium-green',
                'lighter' => 'light-green',
            ]
        ];

        $staffSalarySection = [
            'superSection' => [
                'headerTitle' => "Staff Salary Total:",
                'headerRows'  => $totalData['expenses']['staffSalaryTotal'][0],
                'footerTitle' => "Staff Salary Total:",
                'footerRows'  => $totalData['expenses']['staffSalaryTotal'][0],
            ],

            //All section statements must be passed through eval in the for loop
            'section'      => [
                'headerTitle' => '"$siteName Staff Salary:"',
                "headerRows"  => '$tableData["expenses"]["staffSalaryTotal"][0]',

                'sectionRows' => '$tableData["expenses"]["staffSalary"]',

                'footerTitle' => '"$siteName Staff Salary:"',
                "footerRows"  => '$tableData["expenses"]["staffSalaryTotal"][0]'
            ],

            'colors' => [
                'darker'  => 'medium-red',
                'lighter' => 'light-red',
            ]
        ];

        $expenseItemsSection = [
            'superSection' => [
                'headerTitle' => "Expense Items:",
                'headerRows'  => $totalData['expenses']['itemised']['total'][0],
                'footerTitle' => "Expense Items:",
                'footerRows'  => $totalData['expenses']['itemised']['total'][0],
            ],

            //All section statements must be passed through eval in the for loop
            'section'      => [
                'headerTitle' => '"$siteName Total"',
                "headerRows"  => '$tableData["expenses"]["itemised"]["total"][0]',

                'sectionRows' => '$tableData["expenses"]["itemised"]["items"]',

                'footerTitle' => '"$siteName Total:"',
                "footerRows"  => '$tableData["expenses"]["itemised"]["total"][0]'
            ],

            'colors' => [
                'darker'  => 'medium-red',
                'lighter' => 'light-red',
            ]
        ];

        $tourismBoardSection = [
            'superSection' => [
                'headerTitle' => "Tourism Board Payments Total:",
                'headerRows'  => $totalData['expenses']['incomeMinusTourismBoardPayments'][0],
                'footerTitle' => "Tourism Board Payments Total:",
                'footerRows'  => $totalData['expenses']['incomeMinusTourismBoardPayments'][0],
            ],

            'section'      => [
                'headerTitle' => '"$siteName Income Minus Tourism Board:"',
                "headerRows"  => '$tableData["expenses"]["incomeMinusTourismBoardPayments"][0]',

                'sectionRows' => '$tableData["expenses"]["tourismBoardPayments"]',

                'footerTitle' => '"$siteName Income Minus Tourism Board:"',
                "footerRows"  => '$tableData["expenses"]["incomeMinusTourismBoardPayments"][0]'
            ],
            'colors'       => [
                'darker'  => 'medium-red',
                'lighter' => 'light-red',
            ]
        ];

        $expenseLoans = [
            'superSection' => [
                'headerTitle' => "Expense Loans:",
                'headerRows'  => $totalData['expenses']['loans'][0],
                'footerTitle' => "Expense Loans:",
                'footerRows'  => $totalData['expenses']['loans'][0],
            ],

            'section'      => [
                'headerTitle' => '"$siteName Total"',
                "headerRows"  => '$tableData["expenses"]["loans"][0]',

                'sectionRows' => '$tableData["expenses"]["loans"]',

                'footerTitle' => '"$siteName Total:"',
                "footerRows"  => '$tableData["expenses"]["loans"]["0"]'
            ],

            'colors' => [
                'darker'  => 'medium-blue',
                'lighter' => 'light-blue',
            ]
        ];

        $output .= $this->drawSection($onsiteSection);
        $output .= $this->drawSection($offsiteSection);
        $output .= $this->drawSection($merchandiseSection);
        $output .= $this->drawIncomeTotalRows($this->collection);

        $output .= "</tbody>\n";
        $output .= "</td>\n";
        $output .= "</tr>\n";
        $output .= "</table>";

        $this->enablePaxColumn = false;

        $output .= $this->drawTopSection("EXPENSES ", 'dark-red', $headerColspan);
        //$output .= $this->drawSection($tourismBoardSection); Only draw tourism board totals not rows
        $output .= $this->drawTourismBoardTotalRows($this->collection);
        $output .= $this->drawSection($staffSalarySection);
        $output .= $this->drawSection($expenseItemsSection);
        $output .= $this->drawSection($expenseLoans);
        $output .= $this->drawExpensesTotalRows($this->collection);
        $output .= $this->drawIncomeMinusExpensesTotalRows($this->collection);

        $colspan = $this->enablePaxColumn ? 25 : 12;

        foreach ($this->collection['sites'] as $site) {
            $tableData = $site->getTableData();
            $siteName = $site->getSiteName();
            $output .= "<tr class='final-total'><td>$siteName Profit and Loss</td><td>{$this->makeReadable($tableData['total'][0])}</td><td colspan='$colspan'></td></tr>\n";
        }
        $output .= "<tr class='final-total'><td>All Sites Profit and Loss</td><td>{$this->makeReadable($totalData['total'][0])}</td><td colspan='$colspan'></td></tr>\n";

        $output .= "</td>\n";
        $output .= "</tr>\n";
        $output .= "</tbody>";
        $output .= "</table>";

        return $output;
    }

    public function drawTopSection($title, $color, $colspan)
    {
        $tableWidth = (13 * $colspan) + 1;
        $output = "
        <table class='profit-and-loss-combined'>
            <thead>
            <tr class='$color no-hover'>
                <th class='first-col'>$title</th>
                <th colspan='$colspan'>JAN</th>
                <th colspan='$colspan'>FEB</th>
                <th colspan='$colspan'>MAR</th>
                <th colspan='$colspan'>APR</th>
                <th colspan='$colspan'>MAY</th>
                <th colspan='$colspan'>JUN</th>
                <th colspan='$colspan'>JUL</th>
                <th colspan='$colspan'>AUG</th>
                <th colspan='$colspan'>SEP</th>
                <th colspan='$colspan'>OCT</th>
                <th colspan='$colspan'>NOV</th>
                <th colspan='$colspan'>DEC</th>
                <th colspan='$colspan'>TOTAL</th>
            </tr>
            </thead>
            <tbody>
            <tr class='no-hover'>
            <td colspan='$tableWidth'>\n";

        return $output;
    }

    function drawSection($section)
    {
        $superSectionHeaderTitle = $section['superSection']['headerTitle'];
        $superSectionHeaderRows = $section['superSection']['headerRows'];
        $superSectionFooterTitle = $section['superSection']['footerTitle'];
        $superSectionFooterRows = $section['superSection']['footerRows'];

        $sectionHeaderRowsEval = $section['section']['headerRows'];
        $sectionHeaderTitleEval = $section['section']['headerTitle'];
        $sectionRowsEval = $section['section']['sectionRows'];
        $sectionFooterTitleEval = $section['section']['footerTitle'];
        $sectionFooterRowsEval = $section['section']['footerRows'];
        $colorLight = $section['colors']['lighter'];
        $colorDark = $section['colors']['darker'];

        $sites = $this->collection['sites'];

        $output = '';
        $superSectionHeader = $this->replaceRowTitle($superSectionHeaderRows, $superSectionHeaderTitle);
        $output .= $this->drawFoldableHeader($superSectionHeader, $colorDark);


        $colspan = 14;
        if ($this->enablePaxColumn) $colspan = 27;

        $output .= "<tr class='no-hover'>\n<td colspan='$colspan'>\n";
        foreach ($sites as $siteId => $site) {
            $tableData = $site->getTableData();
            $siteName = $site->getSiteName();

            $sectionHeaderRows = [];
            $sectionHeaderTitle = [];
            $sectionRows = [];
            $sectionFooterRows = [];
            $sectionFooterTitle = [];

            eval('$sectionHeaderRows = ' . $sectionHeaderRowsEval . ";");
            eval('$sectionHeaderTitle = ' . $sectionHeaderTitleEval . ";");
            eval('$sectionRows = ' . $sectionRowsEval . ";");
            eval('$sectionFooterTitle =' . $sectionFooterTitleEval . ";");
            eval('$sectionFooterRows =' . $sectionFooterRowsEval . ";");

            $header = $this->replaceRowTitle($sectionHeaderRows, $sectionHeaderTitle);

            $output .= $this->drawFoldableHeader($header, $colorLight);
            $output .= $this->drawFoldedRows($sectionRows);//this must be evaluated

            $footer = $this->replaceRowTitle($sectionFooterRows, $sectionFooterTitle);
            $output .= $this->drawFoldableFooter($footer);
        }

        $output .= "</td>\n</tr>\n";
        $superSectionFooter = $this->replaceRowTitle($superSectionFooterRows, $superSectionFooterTitle);
        $output .= $this->drawFoldableFooter($superSectionFooter);

        return $output;
    }

    private function replaceRowTitle($row, $title)
    {
        $row[0] = $title;

        return $row;
    }

    public function drawFoldableHeader($data, $classes = '')
    {
        $output = "<table class='$classes'>\n";
        $output .= "<thead>\n<tr class='foldable'><!--drawFoldableHeader-->\n";

        foreach ($data as $index => $cell) {
            if ($index == 0) {
                $output .= "<th class='first-col'><span class='expander'>[+]</span> $cell </th>\n";
            } else {
                if ($this->enablePaxColumn)
                    $output .= "<th><span class='header-totals'>{$this->makeReadable($cell['pax'])}</span></th>\n";
                $output .= "<th><span class='header-totals'>{$this->makeReadable($cell['amount'])}</span></th>\n";
            }
        }

        return $output . "</tr>\n</thead>\n<tbody class='folded'>\n";
    }

    protected function makeReadable($number)
    {
        return number_format((double)$number);
    }

    public function drawFoldedRows($data)
    {
        $output = "<!--drawFoldedRows-->\n";
        foreach ($data as $rowIndex => $row) {
            $output .= "<tr>\n";
            foreach ($row as $cellIndex => $cell) {
                if (!is_array($cell)) {//first cell is just a value
                    $output .= "<td class='first-col'>$cell</td>\n";
                } else {
                    if ($this->enablePaxColumn)
                        $output .= "<td>{$this->makeReadable($cell['pax'])}</td>\n";

                    $output .= "<td>{$this->makeReadable($cell['amount'])}</td>\n";
                }
            }
            $output .= "</tr>\n";
        }

        return $output;
    }

    public function drawFoldableFooter($data)
    {
        $output = "</tbody>\n<tfoot class='folded'>\n<tr><!--drawFoldableFooter-->\n";

        foreach ($data as $index => $cell) {
            if ($index == 0) {
                $output .= "<td>$cell</td>\n";
            } else {
                if ($this->enablePaxColumn)
                    $output .= "<td>{$this->makeReadable($cell['pax'])}</td>\n";
                $output .= "<td>{$this->makeReadable($cell['amount'])}</td>\n";
            }
        }

        return $output . "</tr>\n</tfoot>\n</table>\n";
    }

    public function drawIncomeTotalRows($collection)
    {
        //draw totals for each site total
        $sites = $collection['sites'];
        $total = $collection['total'];
        $output = '';

        $totalTableData = $total->getTableData();
        $output .= $this->drawFoldableHeader($totalTableData['income']['total'][0]);
        foreach ($sites as $siteId => $site) {
            $siteTableData = $site->getTableData();
            $output .= $this->drawFoldedRows($siteTableData['income']['total']);
        }
        $output .= $this->drawFoldableFooter($totalTableData['income']['total'][0]);

        //$output .= $this->drawFoldedRows($totalTableData['income']['total']);
        return $output;
    }

    public function drawTourismBoardTotalRows($collection)
    {
        //draw totals for each site total
        $sites = $collection['sites'];
        $total = $collection['total'];
        $output = '';

        $totalTableData = $total->getTableData();
        $output .= $this->drawFoldableHeader($totalTableData['expenses']['tourismBoardPayments'][0], 'medium-red');
        foreach ($sites as $siteId => $site) {
            $siteTableData = $site->getTableData();
            $output .= $this->drawFoldedRows($siteTableData["expenses"]["tourismBoardPayments"], 'light-red');
        }
        $output .= $this->drawFoldableFooter($totalTableData['expenses']['tourismBoardPayments'][0]);

        return $output;
    }

    public function drawExpensesTotalRows($collection)
    {
        //draw totals for each site total
        $sites = $collection['sites'];
        $total = $collection['total'];
        $output = '';

        $totalTableData = $total->getTableData();
        $output .= $this->drawFoldableHeader($totalTableData['expenses']['total'][0]);
        foreach ($sites as $siteId => $site) {
            $siteTableData = $site->getTableData();
            $output .= $this->drawFoldedRows($siteTableData['expenses']['total']);
        }
        $output .= $this->drawFoldableFooter($totalTableData['expenses']['total'][0]);

        //$output .= $this->drawFoldedRows($totalTableData['income']['total']);
        return $output;
    }

    public function drawIncomeMinusExpensesTotalRows($collection)
    {
        //draw totals for each site total
        $sites = $collection['sites'];
        $total = $collection['total'];
        $output = '';

        $totalTableData = $total->getTableData();
        $output .= $this->drawFoldableHeader($totalTableData['incomeMinusExpenses']['total'][0], "final-total");
        foreach ($sites as $siteId => $site) {
            $siteTableData = $site->getTableData();
            $output .= $this->drawFoldedRows($siteTableData['incomeMinusExpenses']['total'], "final-total");
        }
        $output .= $this->drawFoldableFooter($totalTableData['incomeMinusExpenses']['total'][0], "final-total");

        //$output .= $this->drawFoldedRows($totalTableData['income']['total']);
        return $output;
    }

    public function arrayAppend($array1, $array2)
    {
        array_push($array1, $array2);

        return $array1;
    }
}


