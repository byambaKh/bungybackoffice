<?php
class OnlineBooking //it should extend from a booking super class
{
    private $firstName;
    private $lastName;
    private $bookingDateTime;
    private $bookingTime;
    private $bookingDate;
    private $email;
    private $jumpNumber;
    private $rate;
    private $notes;
    private $siteId;
    private $transportation;
    private $phoneNumber;
    private $confirmationCode;
    private $id;
    private $conditionsAgreedTo;

    //save the booking to the database

    //getter code inspired by http://stackoverflow.com/a/6185525
    public function __get ($property)
    {
        //check if there is a defined getter method for the function and if so return that o
        $getterMethod = "get".ucfirst($property);
        if(method_exists($this, $getterMethod))
            return $this->$getterMethod();
        else if (property_exists($this, $property)) {
            return $this->$property;
        } else {
            user_error("Undefined property $property");
        }
    }

    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
            return $this;
        } else {
            user_error("Undefined property $property");
        }

    }

    public function setFromArray($post){
       foreach($post as $property => $value) {
           //set the property if it exists
           $this->__set($property, $value);
       }
    }


    public function __construct()
    {
    }

    public function createConfirmationCode()
    {
        //this is how is it is created in registration.php
        $code = md5(uniqid(rand()));
        return $code;
    }

    public function saveUnconfirmed()
    {
        $firstName  = strtoupper($this->firstName) ;
        $lastName   = strtoupper($this->lastName);

        $booking = [
            'site_id'           => $this->siteId,
            'ConfirmCode'       => $this->createConfirmationCode(),
            'BookingDate'       => $this->bookingDate,//$this->bookingDateTime->format("Y-m-d"),
            'BookingTime'       => $this->bookingTime,//$this->bookingDateTime->format("H:i A"),
            'NoOfJump'          => $this->jumpNumber,
            'RomajiName'        => "$lastName $firstName",
            'CustomerLastName'  => $this->lastName,
            'CustomerFirstName' => $this->firstName,
            'CustomerEmail'     => $this->email,
            'ContactNo'         => $this->phoneNumber,
            'TransportMode'     => $this->transportation,
            'place_id'          => $this->siteId,
        ];

        db_perform("customerregs_temp1", $booking);
        $this->id = mysqli_insert_id();
    }

    public function saveConfirmed()//save confirmed if it is a CC booking
    {
        global $config;

        $firstName  = strtoupper($this->firstName) ;
        $lastName   = strtoupper($this->lastName);
        $romajiName = "$lastName $firstName";

        $booking = [
            'site_id'           => $this->siteId,
            'BookingDate'       => $this->bookingDate,//$this->bookingDateTime->format("Y-m-d"),
            'BookingTime'       => $this->bookingTime,//$this->bookingDateTime->format("H:i A"),
            'NoOfJump'          => $this->jumpNumber,
            'RomajiName'        => $romajiName,
            'CustomerLastName'  => $lastName,
            'CustomerFirstName' => $firstName,
            'CustomerEmail'     => $this->email,
            'ContactNo'         => $this->phoneNumber,
            'TransportMode'     => $this->transportation,
            'Rate'              => $config['first_jump_rate'],//TODO what is the rate for a ryujin
            'RateToPay'         => '',
            'RateToPayQTY'      => '',
            'SplitName1'        => $romajiName,
            'SplitName2'        => $romajiName,
            'SplitName3'        => $romajiName,
            'SplitTime1'        => $this->bookingTime,//$this->bookingDateTime->format("H:i A"),
            'SplitTime2'        => $this->bookingTime,//$this->bookingDateTime->format("H:i A"),
            'SplitTime3'        => $this->bookingTime,//$this->bookingDateTime->format("H:i A"),
            'Notes'             => "Internet (".date("Y/m/d").")",
            'Agent'             => 'NULL',
            'UserName'          => 'Bungy',
            'BookingReceived'   => date('Y-m-d H:i:s'),
            'place_id'          => $this->siteId,
        ];
        db_perform("customerregs1", $booking);
        $this->id = mysqli_insert_id();
    }

    //update the booking in the database
    public function update()
    {
    }

    public function deleteTempBookingById($id)
    {
        $query = "DELETE from customerregs_temp1 WHERE CustomerRegID = '$id';";
        mysql_query($query);
    }

    //confirm the booking
    public function confirm()
    {
        $this->saveConfirmed();
		//todo check if the booking is confirmed
        $this->deleteTempBookingById($this->id);
    }

    //confirm the booking by the unique booking ID
    public static function confirmByLink($confirmationId)
    {
        $booking = static::loadByConfirmationCode($confirmationId);

        if($booking != NULL) {
            $booking->confirm();
            return true;
        }
        else return false;
    }

    public static function assignValuesFromTempBookingDatabaseResult($onlineBooking, $bookingArray)
    {
        $onlineBooking->id               = $bookingArray['CustomerRegID'];
        $onlineBooking->siteId           = $bookingArray['site_id'];
        $onlineBooking->confirmationCode = $bookingArray['ConfirmCode'];
        $onlineBooking->bookingDate      = $bookingArray['BookingDate'];
        $onlineBooking->bookingTime      = $bookingArray['BookingTime'];
        $onlineBooking->jumpNumber       = $bookingArray['NoOfJump'];
        $onlineBooking->lastName         = $bookingArray['CustomerLastName'];
        $onlineBooking->firstName        = $bookingArray['CustomerFirstName'];
        $onlineBooking->email            = $bookingArray['CustomerEmail'];
        $onlineBooking->phoneNumber      = $bookingArray['ContactNo'];
        $onlineBooking->transportation   = $bookingArray['TransportMode'];
        $onlineBooking->siteId           = $bookingArray['site_id'];

        return $onlineBooking;
    }

    public static function assignValuesFromBookingDatabaseResult($onlineBooking, $bookingArray)
    {
        $onlineBooking->id               = $bookingArray['CustomerRegID'];
        $onlineBooking->siteId           = $bookingArray['site_id'];
        $onlineBooking->confirmationCode = $bookingArray['ConfirmCode'];
        $onlineBooking->bookingDate      = $bookingArray['BookingDate'];
        $onlineBooking->bookingTime      = $bookingArray['BookingTime'];
        $onlineBooking->jumpNumber       = $bookingArray['NoOfJump'];
        $onlineBooking->lastName         = $bookingArray['CustomerLastName'];
        $onlineBooking->firstName        = $bookingArray['CustomerFirstName'];
        $onlineBooking->email            = $bookingArray['CustomerEmail'];
        $onlineBooking->phoneNumber      = $bookingArray['ContactNo'];
        $onlineBooking->transportation   = $bookingArray['TransportMode'];
        $onlineBooking->siteId           = $bookingArray['site_id'];

        return $onlineBooking;
    }

    public static function loadById($id)
    {
    }

    public static function loadByConfirmationCode($confirmationCode)
    {
        //load from the database and set class variables
        //check if it exists in the database
        //cant use mysqli until we have a proper mysqli connection set up in application startup
        $confirmationId = db_input($confirmationCode);
        $query = ("SELECT * FROM customerregs_temp1 WHERE ConfirmCode = '$confirmationId'");
        $bookingFound = queryForRows($query);

        if(count($bookingFound)) {
            $onlineBooking = new OnlineBooking();
            $booking = static::assignValuesFromTempBookingDatabaseResult($onlineBooking, $bookingFound[0]);
            return $booking;

        } else return NULL;
        //if it does, return an instantiated object
        //if not dont return null
    }

    public static function loadByDateTimeSiteId($bookingDate, $bookingTime, $siteId)
    {
        $query = "SELECT * FROM customerregs1 WHERE BookingDate ='$bookingDate' AND BookingTime = '$bookingTime' AND siteId = '$siteId';";
        $bookingFound = queryForRows($query);

        if(count($bookingDate)) {
            $onlineBooking = new OnlineBooking();
            return static::assignValuesFromBookingDatabaseResult($onlineBooking, $bookingFound[0]);
            //return $onlineBooking;
        }
    }

    //
    public function sendConfirmationEmail($language, $recipients)
    {

    }

    //
    public function sendCancelledBookingEmail()
    {
    }

    public function checkBookingIsPossible(OnlineBooking $booking)
    {
        //get the booking time, date and site, check find the booking

        //check if is greater than
    }

    public static function checkDuplicateBookings()
    {
    }

    public static function checkBookingSlot($date, $time, $siteId)
    {
        //return the number of open slots for that booking
    }

    //checks if the bookings for the day have met the threshold
    public function countBookingsForSiteDateBetweenTimes($siteId, $date, $timeStart, $timeEnd)
    {
        $siteId    = db_input($siteId);
        $date      = db_input($date);
        $timeStart = db_input($timeStart);
        $timeEnd   = db_input($timeEnd);

        $query = "SELECT * FROM customerregs1 WHERE BookingDate = '$date'
            AND BookingTime >= '$timeStart'
            AND BookingTime <= '$timeEnd'
            AND site_id = $siteId;";
        return queryForRows($query);

    }

    //if the booking page is left open for ~30 minutes, expire the booking and ask
    //the user to restart the booking.
    public function expireBooking()
    {
    }

    public function getFreeSlotsAndJumps()
    {
    }
}
