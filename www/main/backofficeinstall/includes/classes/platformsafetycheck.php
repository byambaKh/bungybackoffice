<?php

/**
 * Created by PhpStorm.
 * User: bungyjapan
 * Date: 8/13/15
 * Time: 2:59 PM
 */
class PlatformSafetyCheck
{
    public $site_id;
    public $details = [];
    public $questions;
    public $answers;
    public $shouldSendEmail = false;
    public $emailBody;

    //public $date;
    public static function reportAlreadyExists($site_id, $date)
    {
        return count(queryForRows("SELECT id FROM psc_report_details WHERE site_id = $site_id AND `date` = '$date'"));
    }

    public static function reportExistsAfter($site_id, $date)
    {
        return count(queryForRows("SELECT id FROM psc_report_details WHERE site_id = $site_id AND `date` > '$date'"));
    }

    public function __construct($site_id, $dateOrReportId)
    {
        if (!isset($site_id)) {
            throw new Exception('Invalid Site Id ' . $site_id);
        }
        $this->site_id = $site_id;

        if (is_numeric($dateOrReportId)) {
            $this->initById($dateOrReportId);

        } else if (preg_match('/....-..-../i', $dateOrReportId)) {
            $this->initByDate($dateOrReportId);

        } else $this->initByDate(date('Y-m-d'));
    }

    private function initById($id)
    {
        $this->loadDetailsById($id);
        $this->initByDate($this->details['date']);
    }

    private function initByDate($date)
    {
        if (self::reportAlreadyExists($this->site_id, $date)) {
            $this->loadDetailsByDate($date);
            $this->loadQuestions();
            $this->loadAnswers();

        } else {//no report exists
            $this->details['id'] = '';
            $this->details['date'] = $date;
            //$this->details['staffInspector'] = '';
            $this->details['siteManagers'] = '';
            $this->details['site_id'] = CURRENT_SITE_ID;

            $this->loadQuestions();
            $this->loadAnswers();//this should create an array of blank answers

        }
    }


    private function loadDetailsByDate($date)
    {
        //We will only ever retrieve one report detail for a date since there will only ever be one. So we grab the 0th
        $this->details = queryForRows("SELECT * FROM psc_report_details WHERE `date` = '$date' AND site_id = $this->site_id")[0];
    }

    private function loadDetailsById($id)
    {
        $this->details = queryForRows("SELECT * FROM psc_report_details WHERE id='" . (int)$id . "'")[0];
    }

    private function loadDetails()
    {
        $detailsSql = "SELECT * FROM psc_report_details WHERE site_id = $this->site_id AND date < '{$this->details['date']}' ORDER BY `date` DESC LIMIT 1 ";//get the latest report details
        $this->details = queryForRows($detailsSql)[0];
    }

    private function getAnswerTemplate($questionId)
    {
        return [
            'id'                           => '',
            'reportId'                     => '',
            'priority'                     => '',
            'acknowledgedBySiteManager'    => '',
            'acknowledgedByGeneralManager' => '',
            'acknowledgedByOwner'          => '',
            'questionId'                   => $questionId,
            'commentsManagement'           => '',
            'commentsJumpMaster'           => '',
            'dateClosed'                   => '0000-00-00',
            'date'                         => $this->details['date'],
            'goodCondition'                => '',
            'staffInspector'               => '',          
        ];

    }

    private function loadAnswers()
    {//load the lastest answers
        //grab the latest report before the current date
        $answersSql = "SELECT * FROM psc_report_answers WHERE `reportId` = '{$this->details['id']}';";
        $answers = queryForRows($answersSql);

        //if there is no prior report
        if (count($answers) == 0) {
            //get the most previous report answers
            $priorDetailsSql = "SELECT * FROM psc_report_details WHERE `date` < '{$this->details['date']}' AND site_id = $this->site_id ORDER BY `date` DESC LIMIT 1;";
            $priorDetails = queryForRows($priorDetailsSql);
            if (count($priorDetails)) {
                $answersSql = "SELECT * FROM psc_report_answers WHERE `reportId` = '{$priorDetails[0]['id']}';";
                $answers = queryForRows($answersSql);
            } else {
                //generate a 1 indexed array of blank answers
                foreach ($this->questions as $questionIndex => $question) {
                    $answers[$questionIndex + 1] = $this->getAnswerTemplate($question['id']);
                }
            }            
        }

        $isSavedReport = $this->reportAlreadyExists($this->details['site_id'], $this->details['date']);
        $reportExistsAfter = $this->reportExistsAfter($this->details['site_id'], $this->details['date']);

        foreach ($answers as $answerIndex => $answer) {
            //check if the answer the previous issue has been closed and if the date matches the report date
            //hide issues that were not closed on the report date

            $wasResolvedInThisReport = $answer['dateResolved'] == $this->details['date'];
            $isInGoodCondition = $answer['goodCondition'] == 'y';
            $isOpenIssue = !($answer['goodCondition'] == 'y');//is an open issue for the current report.

            $isResolvedIssue = $answer['dateClosed'] != '0000-00-00';

            //if an issue was raised or the issue is still open, then issueRaised is true
            $issueRaised = $isResolvedIssue || !$isInGoodCondition;

            $blankIssue = $this->getAnswerTemplate($answer['questionId']);

            if ($isSavedReport) {
                //don't do anything to the data just grab everything as loaded
            } else {
                if ($reportExistsAfter) {
                    //if a report exists after this one, we grab the previous report and modify the dates of the good
                    //condition items to be for this date
                    //The bad condition items are not modified but are allowed to propagate
                    //if it is in good condition, set the date to be today
                    if ($isInGoodCondition) $answers[$answerIndex]['date'] = $this->details['date'];
                } else {
                    //If it is a resolved issue for this report
                    if ($isResolvedIssue && $wasResolvedInThisReport) {
                        //Don't blank issues resolved in this report, only ones resolved after
                    } else if ($isResolvedIssue) {
                        //if it is a resolved issue
                        $answers[$answerIndex] = $blankIssue;
                    } else if ($isOpenIssue) {
                        //don't do anything allow it to propagate forwards
                    } else {//it is a non issue ie: in good condition
                        $answers[$answerIndex] = $blankIssue;
                    }
                }
                //report for a day that does not exist
                //set the data of answers to be the data of the report for non issues
            }
        }
        unset($answerIndex);

        foreach ($answers as $answerIndex => $answer) {
            $this->answers[$answer['questionId']] = $answer;
        }
    }

    private function loadQuestions()
    {
        $questionsSql = "SELECT * FROM psc_report_questions WHERE active = 1";
        $questions = queryForRows($questionsSql);

        //index the questions by the question id
        foreach ($questions as $questionIndex => $question) {
            $this->questions[$question['id']] = $question;
        }
    }

    public function save($report)
    {
        //include 'reports_save.php';
        if (self::reportAlreadyExists($report['details']['site_id'], $report['details']['date'])) {            
            $action = 'update';
            $parameters = "`id` = {$this->details['id']}";
        } else {            
            $action = 'insert';
            $parameters = "";
        }

        db_perform('psc_report_details', $report['details'], $action, $parameters);

        if ($action == "insert")
            $reportId = mysql_insert_id();
        else
            $reportId = $this->details['id'];
        
        foreach ($report['answers'] as $questionIndex => &$answer) {
            //if ($action == 'update') {
            //}
            
            if ($answer['dateClosed'] == '') {
                $answer['dateClosed'] = '0000-00-00'; //trying to save '' as a date results in 0000-00-00
            }
            $answer['reportId'] = $reportId;

            //if there is a bad condition item that was created on the same date as the report
            //if ($answer['goodCondition'] == 'n' && ($this->details['date'] == $answer['date'])) {

            $notClosed = ($answer['dateClosed'] != '0000-00-00');
            $isNewIssue = $report['details']['date'] == $answer['date'];
            $isOpenIssue = $answer['goodCondition'] == 'n';
            $emailWasSent = isset($answer['emailSent']) ? $answer['emailSent']: 0;

            if ($isNewIssue && $isOpenIssue && !$emailWasSent) {//if answer date == report date, it is a new issue added today

                //also check if there is a new issue
                $this->emailBody .= "
                    $questionIndex )
                    <h2>{$this->questions[$questionIndex]['en']}<br>{$this->questions[$questionIndex]['jp']}</h2>
                    <h3>Jump Master Comments</h3>
                    <br>{$answer['commentsJumpMaster']}
                    <h3>Management Comments</h3>
                    <br>{$answer['commentsManagement']}<br><br>\n";

                    $answer['emailSent'] = 1;//Assume the mail is sent if the email has been added to the email body
                    $this->shouldSendEmail = true;
            }


            $parameters = "reportId = '$reportId' AND questionId = {$answer['questionId']}";
            //save the question
            db_perform('psc_report_answers', $answer, $action, $parameters);
        }

        $this->loadDetailsByDate($this->details['date']);
        $this->loadAnswers();
        //if the history is saved enable email send flag
        $this->shouldSendEmail = $this->saveHistory($report);
    }

    private function historyExists($date, $questionId)
    {
        //For an open issue, the $answer will always have the same date as the date it was initially opened
        //for that reason we can check if there is an open issue using the answers date against the history
        //table
        $historyQuery = "SELECT id FROM psc_report_histories WHERE `date` = '$date' AND questionId = $questionId";

        return count(queryForRows($historyQuery));
    }

    public function saveHistory($report)
    {//this should only save for completed history items
        //
        $newHistorySaved = false;

        foreach ($report['answers'] as $answerIndex => $answer) {
            //foreach answer
            $reportId = $answer['reportId'];
            $questionId = $answer['questionId'];
            $issueOpenedDate = $answer['date'];

            $history = [
                'date'        => $answer['date'],
                'description' => $this->questions[$questionId]['description'],
                'reportId'    => $reportId,
                'questionId'  => $questionId,
            ];


            if ($answer['dateClosed'] != '0000-00-00') {
                if (!$this->historyExists($issueOpenedDate, $questionId)) {
                    db_perform('psc_report_histories', $history, "insert", "");
                    $newHistorySaved = true;
                }
            }
        }

        return $newHistorySaved;
    }

    public function sendReportEmailTo($recipients)
    {
        $issueUrl = "http://{$_SERVER['HTTP_HOST']}{$_SERVER['SCRIPT_NAME']}?date={$this->details['date']}";

        $mail = new PHPMailer;
        $mail->From = 'noreply@bungyjapan.com';
        $mail->FromName = ucfirst(CURRENT_SITE_DISPLAY_NAME) . ' Operations Daily Checks';

        foreach ($recipients as $recipient) {
            $mail->addAddress($recipient);
        }

        $mail->addReplyTo('noreply@bungyjapan.com', 'Operations - Daily Checks');
        $mail->isHTML(true);
        $mail->CharSet = 'UTF-8';

        $mail->Subject = "Report {$this->details['date']} on " . ucfirst(CURRENT_SITE_DISPLAY_NAME) . " has an Open Issue";
        $mail->Body = "A daily platform safety report has been submitted with one or more open issues on <a href='$issueUrl'>" . ucfirst(CURRENT_SITE_DISPLAY_NAME) . "</a>. <br>$this->emailBody";

        $mail->send();
    }

    private function createReportLine($reportId, $question, $questionNumber, $answer = null)
    {
        //filter out zero dates from the database as the db_perform puts quotes around 'NULL' this results in 0000-00-00
        //in the database we dont want to show that
        if ($answer['date'] == '0000-00-00') $answer['date'] = '';

        $goodConditionDropDown = $this->createGoodConditionDropDown($reportId, $question['id'], $answer);
        $answerLine = $this->createAnswerLine($reportId, $question['id'], $answer);
        
        $inspectorNames =  $this->createInspectorDDL($reportId, $question['id'], $answer);    //add 2020-02-20 -                     
               
        $reportLine = "\n
                <div class='report-line'>
                    <div class='item-number'>" . ($questionNumber) . ")</div>
                    <div class='item-question-container'>
                    <div class='item-question-jp'>{$question['en']}</div>
                    <div class='item-question-en'>{$question['jp']}</div>
                </div>
                \n"
            . $goodConditionDropDown . "\n" . $answerLine . "\n" .$inspectorNames . "\n" . "</div>\n";

        return $reportLine;
    }

    private function createAnswerLine($reportId, $questionId, $answer)
    {
        //TODO extract values for data and place them in to the
        //TODO set hidden/shown
        echo '';

        $priority0Checked = $priority1Checked = $priority2Checked = '';

        if ($answer['priority'] === 0) $priority0Checked = "checked";//use strict comparison as '' == 0
        if ($answer['priority'] == 1) $priority1Checked = "checked";
        if ($answer['priority'] == 2) $priority2Checked = "checked";

        $acknowledged0Checked = $acknowledged1Checked = $acknowledged2Checked = '';

        if ($answer['acknowledgedBySiteManager'] == 'y') $acknowledged0Checked = "checked";
        if ($answer['acknowledgedByGeneralManager'] == 'y') $acknowledged1Checked = "checked";
        if ($answer['acknowledgedByOwner'] == 'y') $acknowledged2Checked = "checked";

        if ($answer['dateClosed'] == '0000-00-00') $answer['dateClosed'] = '';

        $hideClass = '';
        if ($answer['goodCondition'] != 'n') $hideClass = 'hide';

        echo '';

        return
            "\n
        <div class='answer-line $hideClass' id='answer-line-$questionId'>
            <h2>JM Comments:</h2>
            <textarea name='report[answers][$questionId][commentsJumpMaster]' rows='8' cols='50'>{$answer['commentsJumpMaster']}</textarea><br>
            Priority:
            <input type='radio' group='report[answers][$questionId][priority]' name='report[answers][$questionId][priority]' $priority0Checked value='0'>Stop Operations
            <input type='radio' group='report[answers][$questionId][priority]' name='report[answers][$questionId][priority]' $priority1Checked value='1'>Proceed after confirmation by SM
            <input type='radio' group='report[answers][$questionId][priority]' name='report[answers][$questionId][priority]' $priority2Checked value='2'>Proceed - Fix When Possible
            <input type='hidden' name='report[answers][$questionId][reportId]' value='$reportId'>
            <input type='hidden' name='report[answers][$questionId][questionId]' value='$questionId'>
            <input type='hidden' name='report[answers][$questionId][date]' value='{$answer['date']}'>
            <br>
            <br>
            Acknowledged By:
            <input type='checkbox' name='report[answers][$questionId][acknowledgedBySiteManager]' value='y' $acknowledged0Checked>Site Manager
            <input type='checkbox' name='report[answers][$questionId][acknowledgedByGeneralManager]' value='y' $acknowledged1Checked>General Manager
            <input type='checkbox' name='report[answers][$questionId][acknowledgedByOwner]' value='y' $acknowledged2Checked>Owner
            <br>
            <br>
            <h2>Management Comments:</h2>
            <textarea name='report[answers][$questionId][commentsManagement]' rows='8' cols='50'>{$answer['commentsManagement']}</textarea><br>
            <br>
            Date Resolved: <input class='answer-date-value' type='text' name='report[answers][$questionId][dateClosed]' value='{$answer['dateClosed']}'>&nbsp;&nbsp;
            <button class='resolve-issue' rel='$questionId' type='button' value='save'>Resolve Issue</button>
        </div>
        \n";
    }

    private function createGoodConditionDropDown($reportId, $questionId, $answer)
    {
        $goodConditionYesSelected = $goodConditionNoSelected = $goodConditionUnselected = '';

        if ($answer['goodCondition'] == 'y') $goodConditionYesSelected = 'selected';
        if ($answer['goodCondition'] == 'n') $goodConditionNoSelected = 'selected';

        //if neither of the two are selected, select the empty value
        if (strlen($goodConditionYesSelected . $goodConditionNoSelected) == 0) $goodConditionUnselected = 'selected';

        return
            "\n<div class='item-value' id='good-condition-$questionId'>
                <select class='yesNo' rel='$questionId' id='good-condition-select-$questionId' name='report[answers][$questionId][goodCondition]'>
                    <option value='' $goodConditionUnselected></option>
                    <option value='y' $goodConditionYesSelected>良</option>
                    <option value='n' $goodConditionNoSelected>否</option>
                </select>
            </div>\n";
    }

    //add 2020-02-16 create inspector dropdownlist in daily checks ↓
    private function createInspectorDDL($reportId, $questionId, $answer)
    {        
        //BJHelper::getStaffListByRolesAndSite('StaffListName', array("Staff +", "Site Manager"), CURRENT_SITE_ID, true);                               
        $inspector =  BJHelper::getStaffList('StaffListName', true);  
        return draw_pull_down_menu("report[answers][$questionId][staffInspector]", $inspector, $answer['staffInspector'], 'id="inspectorDDL"') . "\n";
    }

    //end ↑

    public function renderIssue($questionId)
    {
        //get the question
        $question =
        $issueHtml = $this->createReportLine($this->details['id'], $this->questions[$questionId], $questionId, $this->answers[$questionId]);

        return $issueHtml;
    }

    public function render()
    {
        //Generate report html
        $reportHtml = '';
        foreach ($this->questions as $questionIndex => $question) {
            $questionId = $question['id'];

            if (isset($this->answers[$questionId]))
                $answer = $this->answers[$questionId];
            else $answer = [];

            $emptyAnswer = [
                'id'                           => '',
                'reportId'                     => '',
                'questionId'                   => '',
                'priority'                     => '',
                'acknowledgedBySiteManager'    => '',
                'acknowledgedByGeneralManager' => '',
                'acknowledgedByOwner'          => '',
                'commentsManagement'           => '',
                'commentsJumpMaster'           => '',
                'dateClosed'                   => '',
                'date'                         => date('Y-m-d'),
                'goodCondition'                => '',
                'siteManagers'               => ''        //2020-02-19 add byamba - inspector name insert into table
            ];

            //fill the array in if there are missing values or there is not report for the day
            $answer = array_merge($emptyAnswer, $answer);

            $reportHtml .= $this->createReportLine($this->details['id'], $question, $questionIndex, $answer);
        }

        return $reportHtml;
    }

}