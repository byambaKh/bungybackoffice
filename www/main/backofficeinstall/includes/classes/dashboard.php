<?php

class Dashboard
{
    private $dataStructure;

    private $activeSites;
    private $bookingsMadeToday;
    private $jumpsToday;
    private $staffOnHand;
    private $merchandiseSales;
    private $openOrClosed;

    private $totalJumpsCompletedToday;
    private $totalJumpsBookedToday;
    private $totalStaffWorking;
    private $totalCancellations;
    private $totalBookings;
    private $totalJumpsYear;    //add 20190926    

    private $numberOfModals = 0;

    private $locations = [
        1  => ['latitude' => '36.759516', 'longitude' => '138.97336'],//Minakami
        2  => ['latitude' => '36.684046', 'longitude' => '140.465559'],//Sarugakyo
        3  => ['latitude' => '36.730078', 'longitude' => '138.894546'],//Ryujin
        4  => ['latitude' => '32.39779', 'longitude' => '130.823171'],//Itsuki
        9  => ['latitude' => '36.5524', 'longitude' => '138.7012'],//Yamba
        10 => ['latitude' => '34.607003', 'longitude' => '135.670673'], //Nara
        12 => ['latitude' => '35.114680', 'longitude' => '138.461394'], //Fuji
        13 => ['latitude' => '35.114680', 'longitude' => '138.461394'] //Gifu
    ];

    private $relativeIncome = [];//month
    private $yearRelativeIncome = [];//year   

    private $newbookingData = []; //byamba

    private $forecastIoAPIKey = "0aaba90753892e073dc956b123c58685";

    function __construct()
    {
        $date = date('Y-m-d');

        $this->activeSites = $this->activeSitesByNameAndId();
        $this->bookingsMadeToday = $this->bookingsMadeToday($date);
        $this->jumpsToday = $this->jumpsToday($date);
        $this->staffOnHand = $this->staffOnHand($date);
        $this->merchandiseSales = $this->merchandiseSales($date);
        $this->openOrClosed = $this->openOrClosed($date);

        $this->totalJumpsBookedToday = $this->totalJumpsBookedToday($date);
        $this->totalJumpsCompletedToday = $this->totalJumpsCompletedToday($date);
        $this->totalStaffWorking = $this->totalStaffWorking($date);
        $this->totalCancellations = $this->totalCancellations($date);
        $this->totalBookings = $this->totalBookings($date);

        $this->buildArray();
        echo $this->outputJson();

    }

    /******************************************************************************************************************/
    //##############################################MULTI-VALUE QUERIES#################################################
    /******************************************************************************************************************/

    function activeSitesByNameAndId()
    {
        $query = "
            SELECT
              id AS site_id,
              display_name AS `value`,
              short_name AS shortName
            FROM sites
            WHERE
              id IN (1, 2, 3, 4, 9, 10, 12, 13)
        ";

        return $this->getResultsAndConvert($query);
    }

    private function getResultsAndConvert($query)
    {
        $data = queryForRows($query);
        $output = [];
        foreach ($data as $row) {
            $siteId = $row['site_id'];
            $value = $row['value'];
            $output[$siteId] = $value;
        }

        return $output;
    }

    function bookingsMadeToday($date)
    {
        $query = "
            SELECT
                COUNT(`BookingReceived`) AS `value`,
                site_id
            FROM customerregs1
            WHERE
                DATE(`BookingReceived`) = '$date'
            GROUP BY site_id
        ";

        $query = $this->leftJoinWithActiveSites($query);

        return $this->getResultsAndConvert($query);
    }

    private function leftJoinWithActiveSites($query)
    {
        $fullQuery = "
            SELECT
                p.id AS site_id,
                IFNULL(q.value, 0) AS `value`
                FROM sites p
            LEFT JOIN

            (

            {$query}

            ) q

            ON (q.`site_id` = p.id)
            WHERE
                p.id IN (1, 2, 3, 4, 9, 10, 12, 13)
                #`status` = 1
                #AND hidden = 0;
        ";

        return $fullQuery;
    }

    function jumpsToday($date)
    {
        $query = "
            SELECT
                SUM(IFNULL(`NoOfJump`, 0))  AS `value`,
                site_id
                FROM
                    customerregs1 AS c
                WHERE
                    BookingDate = '$date'
                    #AND c.checked = 1
                    AND c.DeleteStatus = 0
            GROUP BY site_id";


        $query = $this->leftJoinWithActiveSites($query);

        return $this->getResultsAndConvert($query);
    }

    function staffOnHand($date)
    {
        //date is in a different format for the roster table
        //2016-02-01 is 201602 and a roster_day column value of 1
        list($year, $month, $day) = explode('-', $date);
        $date = $year . $month . (int)$day;

        $query = "
            SELECT
                 site_id,
                 IFNULL(COUNT(rp.id), 0) AS `value`
            FROM roster_position AS rp
            LEFT JOIN roster_position_all rpa ON (
                rpa.id = position_id AND
                `name` NOT IN ('OFC', 'TRN', 'TYO', 'MK', 'SG', 'IB', 'KY', 'NR', 'YB', 'JPN', 'INTL', 'P&V1', 'P&V2', 'BM', 'CRD', 'OFC', 'CC') #Remove Non-Operations Positions
                )
            WHERE
                CONCAT(roster_month, roster_day) = '$date'
            GROUP BY site_id
        ";

        $query = $this->leftJoinWithActiveSites($query);

        return $this->getResultsAndConvert($query);
    }

    function merchandiseSales($date)
    {
        $query = "
            SELECT
                SUM(sale_total_2nd_jump) AS `value`,
                site_id
            FROM merchandise_sales
                WHERE
                    DATE(sale_time) = '$date'
            GROUP BY site_id
        ";

        $query = $this->leftJoinWithActiveSites($query);

        return $this->getResultsAndConvert($query);
    }

    function openOrClosed($date)
    {
        $query = "
            SELECT
                IF(OPERATION_DESC = 'Set LOCK OFF', 'Open', 'Closed') AS `value`,
                site_id
            FROM
                calendar_state
            WHERE BOOK_DATE = '$date'
        ";

        $query = $this->leftJoinWithActiveSites($query);

        return $this->getResultsAndConvert($query);
    }

    /************************************************************************************************/
    //########################################SINGLE-VALUE QUERIES####################################
    /************************************************************************************************/

    function totalJumpsBookedToday($date)
    {
        $query = "
        SELECT SUM(`NoOfJump`) AS `value`
        FROM customerregs1
            WHERE
            DeleteStatus = 0
            AND BookingDate = '$date';
        ";

        return $this->queryForSingleValue($query);
    }

    function totalJumpsCompletedToday($date)
    {
        $query = "
        SELECT SUM(`NoOfJump`) AS `value`
        FROM customerregs1
            WHERE checked = 1
            AND DeleteStatus = 0
            AND BookingDate = '$date';
        ";

        return $this->queryForSingleValue($query);
    }

    function totalStaffWorking($date)
    {
        //date is in a different format for the roster table
        //2016-02-01 is 201602 and a roster_day column value of 1
        list($year, $month, $day) = explode('-', $date);
        $siteId = 0;
        $date = $year . $month . (int)$day;
        $query = "
            SELECT
                 site_id,
                 IFNULL(COUNT(rp.id), 0) AS `value`
            FROM roster_position AS rp
            INNER JOIN roster_position_all rpa ON (
                rpa.id = position_id AND
                `name` IN (
                  'JC', 'JM1', 'JM2', 'JO1', 'JO2', 'REC1', 'REC2','RECC','CSB','CS','CSR1','CSR2','CSR3','H','TRN','B/U','CRD1','CRD2','CRD3'
                ) #Remove Non-Operations Positions
            )
            WHERE
                CONCAT(roster_month, roster_day) = '$date'
        ";
        return $this->queryForSingleValue($query);
    }

    function totalCancellations($date)
    {        
        /* byamba - comment out ↓ 2020-06-26

        $query = "SELECT SUM(`CancelFeeQTY`) AS `value`
        FROM customerregs1
        WHERE BookingDate = '$date'";

        return $this->queryForSingleValue($query);
        */

        $siteId = 0;      
        $query = "SELECT SUM(`CancelFeeQTY`) AS `value`
        FROM customerregs1
        WHERE BookingDate = '$date'";

        $canceledBooking = $this->queryForSingleValue($query);               
        
        $query2 = "
            SELECT
                COUNT(`CustomerRegID`) AS `value`
            FROM customerregs1
            WHERE
                BookingDate = '$date'
                AND NoOfJump > 0
                AND DeleteStatus = 0
                AND CancelFeeQTY = 0                                
        ";

        $tbook = $this->queryForSingleValue($query2);

        $cbooking = $this->queryForSingleValue($query2);      
        $breakdown  = '<h2> Cancellation detail</h2>';
        $breakdown .= '<p><div>';                        
        $breakdown .= "Number of today’s bookings canceled: ".$canceledBooking."<br><br>";              
        $breakdown .= 'Today’s total bookings: '.$tbook.'<br>';      
        $breakdown .= '</div></p>';
        
        return $this->wrapInModal($breakdown, $canceledBooking, $siteId);

    }

    function totalBookings($date)
    {
        $query = "
            SELECT
                COUNT(`BookingReceived`) AS `value`
            FROM customerregs1
            WHERE
                DATE(`BookingReceived`) = '$date'
                AND DeleteStatus = 0
                AND CancelFeeQTY = 0
        ";

        return $this->queryForSingleValue($query);
    }

    function buildArray()
    {
        $outputArray = [];

        foreach ($this->activeSites as $siteId => $site) {

            //weather toggle for development
            $weatherEnabled = true;

            if ($weatherEnabled) {
                $weatherData = $this->getForecastIoWeatherForSiteId($siteId);
            } else {
                $weatherData['icon'] = '';
                $weatherData['windSpeed'] = '';
                $weatherData['temperature'] = '';
            }

            $outputArray['sites'][$siteId]['temperature'] = $weatherData["temperature"];
            $outputArray['sites'][$siteId]['weather'] = $weatherData["icon"];
            $outputArray['sites'][$siteId]['mediaDay'] = false;
            /*
            $outputArray['sites'][$siteId]['temperature'] = 0;
            $outputArray['sites'][$siteId]['weather'] = ;
            */

            $outputArray['sites'][$siteId]['name'] = $site;
            $outputArray['sites'][$siteId]['state'] = strtolower($this->openOrClosed[$siteId]);
            //if($siteId == '3')
            //$outputArray['sites'][$siteId]['state'] = 'media';

            $outputArray['sites'][$siteId]['dials']['jumps']['values'] = [0, 20, 40, 60, 80, 100, 120, 140, 160];
            $outputArray['sites'][$siteId]['dials']['jumps']['value'] = ($this->jumpsToday[$siteId] / 160) * 100 . "%";
            $outputArray['sites'][$siteId]['dials']['jumps']['color'] = $this->colorForJumpsToday($this->jumpsToday[$siteId]);

            $outputArray['sites'][$siteId]['dials']['wind']['values'] = [0, "", "", "", 50, "", "", "", 100];
            $outputArray['sites'][$siteId]['dials']['wind']['value'] = $weatherData["windSpeed"];//TODO placeholder value
            $outputArray['sites'][$siteId]['dials']['wind']['color'] = "grey";

            $outputArray['sites'][$siteId]['dials']['staff']['values'] = [0, 2, 4, 6, 8, 10, 12, 14, 16];
            $outputArray['sites'][$siteId]['dials']['staff']['value'] = ((int)$this->staffOnHand[$siteId]) / 16 * 100 . "%";//TODO placeholder value
            $outputArray['sites'][$siteId]['dials']['staff']['color'] = "grey";

            $this->yearRelativeIncome[$siteId] = $this->siteStats(date("Y-m-d"), $siteId, "incomeForLastYear");

        }

        $this->yearRelativeIncome[0] = $this->siteStats(date("Y-m-d"), 0, "incomeForLastYear");//income for the group
        
        $outputArray['sites'] = array_values($outputArray['sites']);

        //Fill in the remaining blank sites
        $emptySites = 9 - count($outputArray['sites']);//9 tiles on the dashboard

        for ($i = 0; $i < $emptySites; $i++) {
            $outputArray['sites'][] = ["name" => ""];
        }       

        //$relativeIncome = $this->buildRelativeIncomeString($this->relativeIncome);
        $yearRelativeIncome = $this->buildRelativeIncomeString($this->yearRelativeIncome);
        $resultKaraokeBookings = $this->karaokeBookings();
        $resultKaraokeHits = $this->karaokeHits();

        //Add Byamba - 20190926 total jumps of this year
        $totalJumpsYear = $this->thisYearJumps(null);
        $RelativeJumps = $this->thisRelativeJumps(null);
        $LastYearJumps = $this->LastYearJumps();            //add 20200105 - byamba
        $PreYearJumps = $this->PreYearJumps();              //add 20200105 - byamba
                
        $newbookingNum = $this->newBookingPopup();
        $jumpsCompleted = $this->jumpsCompletedPopup();
        $totalCancellation = $this->totalCancelBookings();
        $totalWorkingStaff = $this->totalStaffW();

        $totalJumps = "{$this->totalJumpsCompletedToday}/{$this->totalBookings}";
        $outputArray['companyOverview'] = [
            ["Today's new bookings:" => $newbookingNum], //"$this->totalBookings"
            ["Jumps Completed:" => $jumpsCompleted], //$totalJumps
            ["Number of today’s bookings canceled:" => $totalCancellation],  //"$this->totalCancellations"
            ["Today’s operations staff:" => $totalWorkingStaff], //"$this->totalStaffWorking"
            //["Month%: " => $relativeIncome],
            ["Yearly Income Change % : " => $yearRelativeIncome],
            //["Karaoke Bookings: " => $resultKaraokeBookings],
            //["Karaoke Hits: " => $resultKaraokeHits],
            ["Total jumps to date this calendar year: " =>  number_format($totalJumpsYear)],
            ["Total jumps to date previous calendar year: " =>   number_format($RelativeJumps)],
            ["Total jumps in the last 365 days: " =>   number_format($LastYearJumps)],
            ["Total jumps to date for the previous 365 days: " =>   number_format($PreYearJumps)],
            //["Second Jumps:" => "0"],
            //["T-Shirts Sold:" => "0"],
            //["Photos Sold:" => "0"],
        ];

        $this->dataStructure = $outputArray;
    }

    function getForecastIoWeatherForSiteId($siteId)
    {
        if (!array_key_exists($siteId, $this->locations)) return [];
        $longitude = $this->locations[$siteId]['longitude'];
        $latitude = $this->locations[$siteId]['latitude'];

        $request = "https://api.forecast.io/forecast/{$this->forecastIoAPIKey}/$latitude,$longitude";

        $weatherData = json_decode(file_get_contents($request), true);
        $temperature = round(($weatherData['currently']['temperature'] - 32) * 5 / 9);
        $windSpeed = round($weatherData['currently']['windSpeed']);
        $forecastIoIcon = $weatherData['currently']['icon'];

        //https://iconstore.co/icons/weather-vector-icons/ these look like good icons
        //https://iconstore.co/icons/rns-weather-icons/
        $weatherIcon = [
            "clear-day"           => "sunny",
            "clear-night"         => "sunny",
            "rain"                => "rain",
            "snow"                => "snow",
            "sleet"               => "rain_snow",
            "wind"                => "rain",
            "fog"                 => "cloudy",
            "cloudy"              => "cloudy",
            "cloud"               => "cloudy",
            "partly-cloudy-day"   => "cloudy",
            "partly-cloudy-night" => "cloudy"
        ];

        $icon = $weatherIcon[$forecastIoIcon];
        $weather = [
            "temperature" => $temperature,
            "windSpeed"   => $windSpeed,
            "icon"        => $icon
        ];

        return $weather;
    }

    function colorForJumpsToday($jumps)
    {
        if (!$jumps) $jumps = 0;//if jumps is null or ''

        $color = '';

        if ($jumps <= 20) {
            $color = '#FD0E04';//red

        } elseif ($jumps <= 40) {
            $color = '#FEFF00';//yellow

        } elseif ($jumps <= 90) {
            $color = '#01FF06';//green

        } elseif ($jumps <= 160) {
            $color = '#7E81FD';//purple
        }

        return $color;
    }

    function siteStats($dateString, $siteId, $methodName)
    {
        set_time_limit (60);
        $dateThisYearObject = DateTime::createFromFormat("Y-m-d", $dateString);
        $dateLastYearObject = clone($dateThisYearObject);
        $dateLastYearObject->modify("-1 year");

        $incomeThisYear = $this->$methodName($dateThisYearObject, $siteId);

        //byamba - add 20200430 make changes for Yamba income calculation, please delete from 2021.04.20 after
        if($siteId == '9')
        {
            $incomeLastYear = '0';
        }else
        {
            $incomeLastYear = $this->$methodName($dateLastYearObject, $siteId);
        }
        //end 
        
        $incomeThisMonth       = $this->incomeForLast30Days($dateThisYearObject, $siteId);
        $incomeThisMonthLastYr = $this->incomeForLast30Days($dateLastYearObject, $siteId);

        $return = $this->calcPercChange($incomeThisYear, $incomeLastYear);
        $return2 = $this->calcPercChange($incomeThisMonth, $incomeThisMonthLastYr);

        //get jump numbers of each sides, If equal to zero it's all sites
        if($siteId != "0")
        {
            $jumps = $this->thisYearJumps($siteId);
            $lastYJumps = $this->thisRelativeJumps($siteId);
            $siteName =  $this->getSiteName($siteId);
        }else
        {
            $jumps = $this->thisYearJumps(null);
            $lastYJumps = $this->thisRelativeJumps(null);
            $siteName =  "Total";
        }

        $difference = $incomeThisYear - $incomeLastYear;         
        $breakdown  = '<h2>' . $siteName . '</h2>';
        
        if($siteId != "0")
        {
            $breakdown .= '<div class="one">';
        }else
        {
            $breakdown .= '<div>';
        }
        
        $breakdown .= 'The income over the last year= ' . number_format($incomeThisYear) . ' Yen<br>';
        $breakdown .= 'The income over the previous year= ' . number_format($incomeLastYear) . ' Yen<br>';
        $breakdown .= 'Income difference &nbsp; = '  . number_format($difference) . ' Yen<br><br>';
        $breakdown .= $difference . ' / ' . $incomeLastYear . ' * 100 = ' . $return . '%<br><br>';


        $difference = $incomeThisMonth - $incomeThisMonthLastYr;
        $breakdown .=  'Income for the last 30 days = ' . number_format($incomeThisMonth) . ' Yen<br>';
        $breakdown .= 'Last year`s income for the same period = ' . number_format($incomeThisMonthLastYr) . ' Yen<br>';
        $breakdown .= 'Income difference &nbsp; = '  . number_format($difference) . ' Yen<br><br>';
        $breakdown .= $difference . ' / ' . $incomeThisMonthLastYr . ' * 100 = ' . $return2 . '% <br><br>';

        $breakdown .= 'Total jumps last year: ' . number_format($jumps) . ' <br>';
        $breakdown .= 'Jumps difference percentage: ' . $this->calcPercChange($jumps, $lastYJumps) . '% <br><br></div>';
        
        if($siteId != "0")
        {
            $breakdown .= "<div class='two'> Name of staff at site: <br><br>";
            list($year, $month, $day) = explode('-', $dateString);
            $date = $year . $month . (int)$day;
            
            $workingStaff = $this->staffList($date, $siteId);
            
            foreach ($workingStaff as $member) {
                $breakdown .= $member['value'] .'<br>';
            }
            $breakdown .= "</div>";
        }
                
        $return = $this->wrapInModal($breakdown, $return, $siteId);
        return $return;
    }

    function calcPercChange($incomeThisYear, $incomeLastYear)
    {
        if ($incomeLastYear)//If Non-Zero $lastYearIncome
        {
            $return = (($incomeThisYear - $incomeLastYear) / $incomeLastYear) * 100;
            $return = round($return, 1);
        }
        else
        {
            $return = '∞';
        }

        //prepend + if positive
        if ($return > 0)
        {
            $return = "<span style='color: green'>+" . $return . '</span>';
        }
        else if ($return < 0)
        {
            $return = "<span style='color: red'>" . $return . '</span>';
        } else
        {
        }

        return $return;
    }
    function buildRelativeIncomeString($relativeIncomeArray)
    {        
        $shortNames = [
            0  => 'ALL',
            1  => 'MK',
            2  => 'SG',
            3  => 'IB',
            4  => 'IK',
            9  => 'YB',
            10  => 'NR',
            12 => 'FJ',
            13 => 'GF'
        ];

        $return = '';
        foreach ($relativeIncomeArray as $siteId => $income) {
            $return .= $shortNames[$siteId] . ":" . $income . "  ";
        }

        return $return;
    }

    //byamba - add 2020/06/26 ↓        
    function newBookingPopup()
    {          
        $date = date('Y-m-d');
        $num = 18;
        $query1 = "
            SELECT
                COUNT(`BookingReceived`) AS `value`
            FROM customerregs1
            WHERE
                DATE(`BookingReceived`) = '$date'
                AND DeleteStatus = 0
                AND CancelFeeQTY = 0
        "; 
        
        $nbooking = $this->queryForSingleValue($query1);      
        $query2 = "
            SELECT
                SUM(CancelFeeQTY ) AS `value` 
            FROM customerregs1
            WHERE
                BookingDate = '$date'                
        "; 
        $cbooking = $this->queryForSingleValue($query2);      

        $query3 = "
            SELECT
                SUM(`NoOfJump`) AS `value`
            FROM customerregs1
            WHERE
                DATE(`BookingReceived`) = '$date'
                AND DeleteStatus = 0
                AND CancelFeeQTY = 0
                AND NoOfJump > 0
        "; 
        $jumpers = $this->queryForSingleValue($query3); 
        if($jumpers == "")
        {
            $jumpers = 0;
        }

        $returnData = $jumpers ." jumpers from ". $nbooking . " bookings";

        $breakdown  = '<h2> Booking details </h2>';
        $breakdown .= '<p><div>';                        
        $breakdown .= "Today’s new bookings : ".$jumpers. " jumpers from " .$nbooking." bookings <br>";      
        $breakdown .= '(This is number of jumper bookings. This is calculated by counting the “BookingReceived” column data from Booking Data. This info is counted from 12:00 am to 11:59 pm today.) <br><br>';
        $breakdown .= 'Number of today’s bookings canceled: '.$cbooking.'<br>';      
        $breakdown .= '(This is sum of CancelFeeQTY  column data of only today’s booking data.
      Cancellations are done manually by staff.) <br>';
        $breakdown .= '</div></p>';
        
        return $this->wrapInModal($breakdown, $returnData, $num);
    }


    function jumpsCompletedPopup()
    {          
        $date = date('Y-m-d');
        $siteId = 19;      
        $query = "
        SELECT SUM(`NoOfJump`) AS `value`
        FROM customerregs1
            WHERE checked = 1
            AND CancelFeeQTY = 0
            AND DeleteStatus = 0
            AND BookingDate = '$date';
        ";

        $jCompleted = $this->queryForSingleValue($query);  
        if($jCompleted == "")
        {
            $jCompleted = 0;
        }

        $query1 = "
        SELECT SUM(`NoOfJump`) AS `value`
        FROM customerregs1
            WHERE DeleteStatus = 0
            AND CancelFeeQTY = 0
            AND NoOfJump > 0
            AND BookingDate = '$date';
        ";

        $allJumps = $this->queryForSingleValue($query1);               
        $jumpInfo = $jCompleted."/".$allJumps;
        
        $breakdown  = '<h2> Jumps details </h2>';
        $breakdown .= '<p><div>';                        
        $breakdown .= 'Today’s completed jumps: '.$jCompleted.'<br>';      
        $breakdown .= '(This is sum of Jump numbers, it’s only checked bookings without cancellation booking from Booking Data)<br><br>';      
        $breakdown .= 'Today’s total jumps: '.$allJumps.'<br>';         
        $breakdown .= '(This is sum of jump numbers of today’s booking data. not included cancelled booking.) <br>';
        $breakdown .= '</div></p>';
        return $this->wrapInModal($breakdown, $jumpInfo, $siteId);
    }

    //end ↑

    function wrapInModal($modalContent, $wrappedString, $siteid)//Don't forget to include the css to style this modal.
    { 
        $return = '<a href="#openModal'.$siteid.'">' . $wrappedString . '</a>';
        $return .= 
            '<div id="openModal'.$siteid.'" class="modalDialog">
             <div>
                <a href="#close" title="Close" class="close">X</a>
                <div class="modalBody"> 
                  '. $modalContent.'                  
                </div>   
            </div>
            </div>';
        return $return;
    }

    function outputJson()
    {
        return json_encode($this->dataStructure);
    }


    /************************************************************************************************/
    //#######################################GENERAL FUNCTIONS#######################################
    /************************************************************************************************/

    function incomeForLast30Days($dateObject, $siteId)
    {
        $endDateString = $dateObject->format('Y-m-d');

        $dateObject->modify("-30 Days"); //30 Days ago  
        $startDateString = $dateObject->format('Y-m-d');

        if ($siteId != 0)
            $siteFilter = "AND site_id = $siteId";
        else
            $siteFilter = '';

        $query = "
            SELECT
                IFNULL(SUM(NoOfJump * Rate), 0) AS `value`
            FROM customerregs1
            WHERE
                DeleteStatus = 0
                $siteFilter
                AND Checked = 1
                AND BookingDate BETWEEN '$startDateString' AND '$endDateString';
        ";

        return $this->queryForSingleValue($query);
    }

    function queryForSingleValue($query)
    {
        $results = queryForRows($query);
        if (count($results)) {
            return $results[0]['value'];
        } else return NULL;
    }

    function incomeForLastYear($dateObject, $siteId)
    {
        $endDateString = $dateObject->format('Y-m-d');

        $dateObject->modify('-1 year'); //First of day of the year
        $startDateString = $dateObject->format('Y-m-d');

        if ($siteId != 0)
        {
            $siteFilter = "AND site_id = $siteId";
        }
        else
        {
            $siteFilter = '';
        }

        $query = "
            SELECT
                IFNULL(SUM(NoOfJump * Rate), 0) AS `value`
            FROM customerregs1
            WHERE
                DeleteStatus = 0
                $siteFilter
                AND Checked = 1
                AND BookingDate BETWEEN '$startDateString' AND '$endDateString';
        "; 

        return $this->queryForSingleValue($query);
    }

    function incomeSinceYearStart($dateObject, $siteId)
    {
        $endDateString = $dateObject->format('Y-m-d');

        $dateObject->setDate($dateObject->format("Y"), 1, 1); //First of day of the year
        $startDateString = $dateObject->format('Y-m-d');


        if ($siteId != 0)
            $siteFilter = "AND site_id = $siteId";
        else
            $siteFilter = '';

        $query = "
            SELECT
                IFNULL(SUM(NoOfJump * Rate), 0) AS `value`
            FROM customerregs1
            WHERE
                DeleteStatus = 0
                $siteFilter
                AND Checked = 1
                AND BookingDate BETWEEN '$startDateString' AND '$endDateString';
        ";

        return $this->queryForSingleValue($query);
    }

    function incomeForThisMonth($dateObject, $siteId)
    {
        $endDateString = $dateObject->format('Y-m-d');
        $startDateString  = clone($dateThisYearObject);/*
        $startDateString->modify("-28 Days");
        /*$startDateString = $dateObject->format('Y-m-d');
        /*

        if ($siteId != 0)
            $siteFilter = "AND site_id = $siteId";
        else
            $siteFilter = '';

        $query = "
            SELECT
                IFNULL(SUM(NoOfJump * Rate), 0) AS `value`
            FROM customerregs1
            WHERE
                DeleteStatus = 0
                $siteFilter
                AND Checked = 1
                AND BookingDate BETWEEN '$startDateString' AND '$endDateString';
        ";

        return $this->queryForSingleValue($query);*/
        return $endDateString;
    }

    function staffList($date, $siteid)
    {       
        $query =
        "
           SELECT rs.site_id , u.StaffListName as `value`
            FROM roster_position rp, roster_staff rs, users u, roster_position_all rpa
            WHERE
                CONCAT(rp.roster_month, rp.roster_day) = $date
                and rs.site_id = $siteid
                and rp.roster_month = rs.roster_month
                and rp.position_id = rpa.id
                and rp.place = rs.place
                and rs.staff_id = u.UserID
                and rp.site_id = rs.site_id;
        ";
        
        return queryForRows($query); //$this->getResultsAndConvert($query);
    }

    function karaokeBookings()
    {
        $query = "
            SELECT COUNT(*) AS `value`
            FROM customerregs1
            WHERE
                discount_code = 'karaoke';
        ";

        return $this->queryForSingleValue($query);
    }

    function karaokeHits()
{
    // Add correct path to your countlog.txt file.
    $path = '../bookings/karaokeCount.txt';

    // Opens countlog.txt to read the number of hits.
    $file  = fopen( $path, 'r' );
    $count = fgets( $file, 1000 );
    $count = abs( intval( $count ) );
    fclose( $file );

    return $count;
}

    function totalTShirtsSold()
    {

    }

    function secondJumps()
    {

    }

    //Add Byamba 20190922 - Calculate to total jumps in this year
    function thisYearJumps($site_id)
    {
        $firstDate = date("Y-01-01");
        $lastDate = date("Y-m-d");

        $query = "
            select SUM(`NoOfJump`)  as `value` from customerregs1
                 where checked = 1 and DeleteStatus = 0
                 and BookingDate BETWEEN '$firstDate' AND '$lastDate'
                 ";

        if(strlen($site_id) > 0)
        {
            $query .= " and site_id = '$site_id' ";
        }

        return $this->queryForSingleValue($query);
    }

    //Total jumps compared to last year
    function thisRelativeJumps($site_id)
    {
        //$firstDate = date("Y-01-01");
        //$lastDate = date("Y-m-d");

        $PYearStart = date("Y-01-01",strtotime("-1 year"));
        $PYearEnd = date("Y-m-d",strtotime("-1 year"));

        $PrevYearQuery = "
            select SUM(`NoOfJump`)  as `value` from customerregs1
                 where checked = 1 and DeleteStatus = 0
                 and BookingDate BETWEEN '$PYearStart' AND '$PYearEnd'";

        if(strlen($site_id) > 0)
        {
            $PrevYearQuery .= " and site_id = '$site_id' ";
        }

        $PrevYearNumber =  $this->queryForSingleValue($PrevYearQuery);

        //$query = "
        //    select SUM(`NoOfJump`)  as `value` from customerregs1
        //         where checked = 1 and DeleteStatus = 0
        //         and BookingDate BETWEEN '$firstDate' AND '$lastDate';
        //         ";

        //$ThisYearNumber =  $this->queryForSingleValue($query);

        /*if ($PrevYearNumber)//If Non-Zero $PrevYearNumber
        {
            $return = (($ThisYearNumber - $PrevYearNumber) / $PrevYearNumber) * 100;
            $return = round($return, 1);
        }
        else
        {
            $return = '∞';
        }

        //prepend + if positive
        if ($return > 0)
        {
            $return = "<span style='color: green'>+" . $return . '</span>';
        }
        else if ($return < 0)
        {
            $return = "<span style='color: red'>" . $return . '</span>';
        } else
        {
        }*/


        return $PrevYearNumber;
    }

    //byamba - add 20200105 Add additional jump numbers　↓
    function LastYearJumps()
    {
        $firstDate = date("Y-m-d",strtotime("-1 year"));
        $lastDate = date("Y-m-d");

        $query = "
            select SUM(`NoOfJump`)  as `value` from customerregs1
                 where checked = 1 and DeleteStatus = 0
                 and BookingDate BETWEEN '$firstDate' AND '$lastDate'
                 ";

        return $this->queryForSingleValue($query);
    }

    function PreYearJumps()
    {
        $firstDate = date("Y-m-d",strtotime("-2 year"));
        $lastDate = date("Y-m-d",strtotime("-1 year"));

        $query = "
            select SUM(`NoOfJump`)  as `value` from customerregs1
                 where checked = 1 and DeleteStatus = 0
                 and BookingDate BETWEEN '$firstDate' AND '$lastDate'
                 ";

        if(strlen($site_id) > 0)
        {
            $query .= " and site_id = '$site_id' ";
        }

        return $this->queryForSingleValue($query);
    }

    //byamba - end 20200105↑   

    function getSiteName($site_id)
    {

        $query = "
            SELECT
              display_name as `value`
            FROM sites ";

        if(strlen($site_id) > 0)
        {
            $query .= " where id = $site_id ";
        }

        return $this->queryForSingleValue($query);
    }

    //byamba add 2020-06-26 ↓

     function totalStaffW()
    {
        $date = date("Y-m-d");
        //date is in a different format for the roster table
        //2016-02-01 is 201602 and a roster_day column value of 1
        list($year, $month, $day) = explode('-', $date);
        $siteId = 21;
        $date = $year . $month . (int)$day;
        $query = "
            SELECT
                 site_id,
                 IFNULL(COUNT(rp.id), 0) AS `value`
            FROM roster_position AS rp
            INNER JOIN roster_position_all rpa ON (
                rpa.id = position_id AND
                `name` IN (
                  'JC', 'JM1', 'JM2', 'JO1', 'JO2', 'REC1', 'REC2','RECC','CSB','CS','CSR1','CSR2','CSR3','H','TRN','B/U','CRD1','CRD2','CRD3'
                ) #Remove Non-Operations Positions
            )
            WHERE
                CONCAT(roster_month, roster_day) = '$date' and site_id != '0'
        ";        

        //add byamba - 20200626　↓         

        $numOfStaff =$this->queryForSingleValue($query);      
        $breakdown  = '<h2> Operation staffs </h2>';
        $breakdown .= '<p><div>';                        
        $breakdown .= 'Today’s operations staff: '.$numOfStaff.'<br><br>';              
        $breakdown .= "(This is counted by 2 table data. It’s roster_position and roster_position_all tables. But only name in  'JC', 'JM1', 'JM2', 'JO1', 'JO2', 'REC1', 'REC2','RECC','CSB','CS','CSR1','CSR2','CSR3','H','TRN','B/U','CRD1','CRD2','CRD3' It means working staffs on site.) <br>";
        $breakdown .= '</div></p>';
        
        return $this->wrapInModal($breakdown, $numOfStaff, $siteId);
        //　end ↑
    }

    function totalCancelBookings()
    {        
        $date = date("Y-m-d");
        $siteId = 20;      
        $query = "SELECT SUM(`CancelFeeQTY`) AS `value`
        FROM customerregs1
        WHERE BookingDate = '$date'";

        $canceledBooking = $this->queryForSingleValue($query);               
        
        $query2 = "
            SELECT
                COUNT(`CustomerRegID`) AS `value`
            FROM customerregs1
            WHERE
                BookingDate = '$date'
                AND NoOfJump > 0
                AND DeleteStatus = 0
                AND CancelFeeQTY = 0                                
        ";

        $tbook = $this->queryForSingleValue($query2);

        $cbooking = $this->queryForSingleValue($query2);      
        $breakdown  = '<h2> Cancellation detail</h2>';
        $breakdown .= '<p><div>';                        
        $breakdown .= 'Number of today’s bookings canceled: '.$canceledBooking.'<br>';              
        $breakdown .= '( This is sum of CancelFeeQTY  column data of only today’s booking data.
      Cancellations are done manually by staff. ) <br><br>';
        $breakdown .= 'Today’s total bookings: '.$tbook.'<br>';      
        $breakdown .= '(This is sum of jump numbers of today’s booking data. not included cancelled booking.)';
        $breakdown .= '</div></p>';
        
        return $this->wrapInModal($breakdown, $canceledBooking, $siteId);

    }

    //end ↑




}
