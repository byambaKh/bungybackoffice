<?
require_once("../includes/application_top.php");

class oneTimePin
{

    /**
     * Create a zero padded 4 digit string
     *
     * @return string
     */
    static private function newPin()
    {
        return rand(1000, 9999);
    }

    /**
     * Inserts a new sites in to the database if one does not exists already
     *
     * @param $siteId
     */
    static private function insertNewSite($siteId)
    {
        $pin = static::newPin();
        mysql_query("INSERT INTO one_time_pin (site_id, pin) VALUES ($siteId, $pin);");
    }

    /**
     * Returns the one time pin if it already exists or inserts a new site if it does not
     *
     * @param $siteId
     *
     * @return mixed
     */
    static public function getOneTimePin($siteId)
    {
        $query = "SELECT pin FROM one_time_pin WHERE site_id = $siteId";
        $result = queryForRows($query);//need to figure out what happens if there is no one time pin there

        if (count($result) == 0) {
            static::insertNewSite($siteId);
            $result = queryForRows($query);//need to figure out what happens if there is no one time pin there
        }

        return $result[0]['pin'];
    }

    /**
     * Regenerates a new one time pin and updates teh database
     *
     * @param $siteId
     */
    static public function setNewOneTimePinForSite($siteId)
    {
        //generate a 4 digit pin
        $pin = static::newPin();

        //saves it to the database
        mysql_query("UPDATE one_time_pin SET pin = $pin WHERE site_id = $siteId;");
    }

    /**
     * Checks if the submitted pin matches the pin in the database, if it does, it will update the pin and return a 1 (matched)
     * or a zero (did not match)
     *
     * @param $inputPin
     * @param $siteId
     *
     * @return int
     */
    static public function useOneTimePin($inputPin, $siteId)
    {
        if ($inputPin == static::getOneTimePin($siteId)) { //if pin matches what is in the database
            static::setNewOneTimePinForSite($siteId); //generate and save a new one time pin
            return 1; //give authentication

        } else {
            return 0; //if not return a failure
        }
    }
}
