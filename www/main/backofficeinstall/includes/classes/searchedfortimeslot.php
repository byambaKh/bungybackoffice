<?php

class searchedForTimeSlot
{
    function __construct(){

    }

    static function insertSearchedForTimeSlot($date, $time, $siteId) {
        $created = date('Y:m:d H:i:s', time());
        $data = [
            'date' => $date,
            'time' => $time,
            'site_id' => $siteId,
            'created' => $created
        ];

        db_perform('searched_for_slots', $data);
    }
}