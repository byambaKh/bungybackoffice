<?php
//
class TableData{
	private $rowCount;
	private $inputData;

	private $outputData;
    public $columnCount;
    public $data;

    //WARNING! Having duplicate rows causes issues in the create parseHeaderToColumnIndex code. Columns go missing.
    public function parseHeaderToColumnIndex($header){//takes the header and parses it in to an array that maps names to columns
        $nameToColumnIndex = array();
        $nameToColumnValueUnsorted= array();
        $nameToColumnValue = array();
        $columnIndex = 0;

        $maxHeaderHeight = count($header);
        $colSpanAndRowSpanData = array();

        /*
         * This algorithm goes through the header structure and calculates the index of a column based on adding
         * the colspans on preceeding rows.
         *
         * In the case where is is a single row as a header, we just add up the colspans
         *
         * If it is a more complex structure where there are rows spanning in to other rows, things get more complicated
         *
         * ______________________________________________________________
         * |   |   |    |                     |
         * |   |   |    |                     |
         * |   |   |    |                     |
         * |   |   |    |                     |
         */

        foreach($header as $headerHeight => $row){//iterate through each row of the header
            //the current height represents how far away the header is from where the data starts
            //for simple one row headers, this will always be 1 (row and colspans are 1 indexed)
            //for more complicated headers, we may be 3 rows up, but the cell could have a rowspan of 3.
            //that could make it a cell that sits on top of a column.
            $currentHeight = $maxHeaderHeight - $headerHeight;
            $columnIndex = 0;

            //this copy is used only for the present row
            //once we have used a spanned cell, we can remove it
            $colSpanAndRowSpanDataPriorRows = $colSpanAndRowSpanData;

            foreach($row as &$cell){//go through each cell for that row
                //set some defaults if these are not set
                //NOTE: colspan = 0 has a special meaning in firefox that we are not accounting for, ignore it for now
                if(!isset($cell['colspan'])) $cell['colspan'] = 1;
                if(!isset($cell['rowspan'])) $cell['rowspan'] = 1;

                //add some extra information for calculation purposes
                $cell['currentHeight'] = $currentHeight;
                $cell['currentRow'] = $headerHeight;

                if($cell['rowspan'] == $currentHeight){//Means that the cell is on top of column data

                    //we need to check if there is a cell above that spans in to this row
                    //cells that span both row and columns are a problem
                    foreach($colSpanAndRowSpanDataPriorRows as $key => $cellRowSpan){
                        /*
                         * for the current cell
                         *  is there a cell above ie: index < current index
                         *   that spans in to it's row ie: the row span is greater than the difference between this row and the preceeding row
                         *    and is directly before it ie: has the same index? or it's index + colspan is the same
                         *      if so add its colspan to the column index
                         */
                        //does it span in to this row???
                        if($cell['currentHeight'] > ($cellRowSpan['currentHeight'] - $cellRowSpan['rowspan'])){
                            //is it directly before this cell?
                            if($cellRowSpan['index'] == $columnIndex){//if where that previous above cell starts is where we are now on the row below
                                //add it to the column index;
                                $columnIndex += $cellRowSpan['colspan']; //add it's colspan to out index
                                //unset($colSpanAndRowSpanDataPriorRows[$key]);
                                //remove it from the array because we have used it already and we don't want to count it again in
                                //this row
                            }
                        }
                    }
                    $nameToColumnIndex[$cell['name']]  = $columnIndex;
                    //we need a way to map column names
                    $nameToColumnValueUnsorted[$cell['name']] = $cell['value'];
                }

                $cell['index'] = $columnIndex;
                $columnIndex += $cell['colspan'];

                if($cell['rowspan'] > 1){
                    //store the column index of the cell in an array that is not deleted until the end
                    //store the column and rowspan of the cell

                    $colSpanAndRowSpanData[] = $cell;//stash cells that span rows in an array to search
                }
            }
        }
        asort($nameToColumnIndex);
        //sort that to
        foreach ($nameToColumnIndex as $name =>$index){
           $nameToColumnValue[] = $nameToColumnValueUnsorted[$name];
        }

        foreach ($nameToColumnIndex as $name => $index){
            $indexToColumnName[$index] = $name;
        }
        $indexes = array($nameToColumnIndex, $indexToColumnName, $nameToColumnValue);//assign these to properties in the object
        return $indexes;
    }

    /*
	function __construct($data, $headerData, $footerData){
		
		$this->columnCount	= count($data[0]);	
		$this->rowCount		= count($data);	

		$this->inputData = $data;
		$this->processData();

	}
    */
}
?>
