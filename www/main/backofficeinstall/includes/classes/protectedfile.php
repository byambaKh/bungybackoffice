<?
class protectedFile
{
    public $path;

    function acceptableType($type)
    {
        $array = array("image/jpeg", "image/jpg", "image/png", "image/png");
        if (in_array($type, $array))
            return true;

        return false;
    }

    function getFileType($file)
    {
        //Deprecated, but still works if defined...
        /*if (function_exists("mime_content_type"))
            return mime_content_type($file);
        //New way to get file type, but not supported by all yet.
        else
        if (function_exists("finfo_open")) {
            $finfo = finfo_open(FILEINFO_MIME_TYPE);
            $type = finfo_file($finfo, $file);
            finfo_close($finfo);

            return $type;
        } else {//Otherwise...just use the file extension
        */
            $types = array(
                'jpg' => 'image/jpeg',
                'jpeg' => 'image/jpeg',
                'png' => 'image/png',
                'gif' => 'image/gif',
                'bmp' => 'image/bmp'
            );
            $ext = strtolower(substr($file, strrpos($file, '.') + 1));
            if (array_key_exists($ext, $types)) return $types[$ext];

            return "unknown";
        //}
    }

    function viewImage($file, $path)
    {
        $type = $this->getFileType($path.$file);
        if ($this->acceptableType($type))
        {
            header("Content-type: $type");
            //echo file_get_contents($path.$file);
            readfile($path.$file);
            exit;
        }

        header('HTTP/1.1 403 Forbidden');
        exit;

    }

    function downloadFile($file, $path)
    {

    }
}
