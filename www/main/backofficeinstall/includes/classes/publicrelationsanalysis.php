<?php

class PublicRelationsAnalysis
{
    private $siteName;
    private $percentageChangeIncomeOver30Days;
    private $report;
    private $siteId;
    private $dateTime;//refactor to startDateTime
    private $endDateTime;

    function __construct($date, $siteId)
    {
        //load reports up for the current month
        $this->dateTime = DateTime::createFromFormat("Y-m-d", $date . "-01");
        $this->endDateTime = clone $this->dateTime;
        $this->endDateTime->modify('last day of this month');

        $sites = [
            1  => "MINAKAMI",
            2  => "SARUGAKYO",
            3  => "RYUJIN",
            4  => "ITSUKI",
            10 => "NARA",
            12 => "FUJI"
        ];

        $this->siteId = $siteId;
        $this->siteName = $sites[$siteId];
        $this->report = $this->load();
        $this->percentageChangeIncomeOver30Days = $this->percentageChangeOnLastYear(date("Y-m-d"), $siteId);
    }

    /************Code from dashboard.php****************/
    //TODO refactor to share with dashboard.php
    function queryForSingleValue($query)
    {
        $results = queryForRows($query);
        if (count($results)) {
            return $results[0]['value'];
        } else return NULL;
    }

    function incomeForLast30Days($dateObject, $siteId)
    {
        $endDateString = $dateObject->format('Y-m-d');

        $dateObject->modify("-30 Days"); //30 Days ago
        $startDateString = $dateObject->format('Y-m-d');

        $query = "
            SELECT
                IFNULL(SUM(NoOfJump * Rate), 0) AS `value`
            FROM customerregs1
            WHERE
                site_id = $siteId
            AND BookingDate BETWEEN '$startDateString' AND '$endDateString';
        ";

        return $this->queryForSingleValue($query);
    }

    function percentageChangeOnLastYear($dateString, $siteId)
    {
        $dateThisYearObject = DateTime::createFromFormat("Y-m-d", $dateString);
        $dateLastYearObject = clone($dateThisYearObject);
        $dateLastYearObject->modify("-1 year");

        $thisYearIncome = $this->incomeForLast30Days($dateThisYearObject, $siteId);
        $lastYearIncome = $this->incomeForLast30Days($dateLastYearObject, $siteId);

        if ($lastYearIncome)//If Non-Zero $lastYearIncome
        {
            $return = (($thisYearIncome - $lastYearIncome) / $lastYearIncome) * 100;

        } else {
            $return = '∞';
        }

        $return = round($return, 1)."%";

        //prepend + if positive
        if ($return > 0) {
            $return = "<span style='color: green'>+". $return.'</span>';
        } else if ($return < 0) {
            $return = "<span style='color: red'>". $return.'</span>';
        } else {
            //do nothing
        }

        return $return;
    }
    /************End Code from dashboard.php****************/

    public static function save($reports)
    {
        foreach ($reports['sites'] as $siteId => $site) {
            foreach ($site as $ordinal => $day) {
                $day['date'] = date("Y-m-".$ordinal);
                $day['site_id'] = $siteId;
                $day['updated'] = date("Y-m-d H:i:s");

                if(array_key_exists('id', $day)){//only existing entries have id
                    $id = $day['id'];
                    db_perform('public_relations_analysis', $day, 'update', "id = '$id' ");
                } else {
                    $day['created'] = date("Y-m-d H:i:s");
                    db_perform('public_relations_analysis', $day, 'insert');
                }
            }
        }
    }

    private function addBookingDataToTarget($report){
        $projectedResults = $this->loadRosterProjectedJumps();
        $receivedResults  = $this->loadReceivedBookingsAndJumps();
        $actualResults = $this->loadActualBookingsAndJumps();
        $walkInResults = $this->loadWalkInJumps();

        foreach($report['days'] as $dayOrdinal => &$day) {
            if (array_key_exists($dayOrdinal, $projectedResults))
                $day['projected'] = $projectedResults[$dayOrdinal]['projected'];

            if (array_key_exists($dayOrdinal, $receivedResults)) {
                $day['received_jumps'] = $receivedResults[$dayOrdinal]['received_jumps'];
                $day['received_bookings'] = $receivedResults[$dayOrdinal]['received_bookings'];
            }

            if (array_key_exists($dayOrdinal, $actualResults)) {
                $day['actual_jumps'] = $actualResults[$dayOrdinal]['actual_jumps'];
                $day['actual_bookings'] = $actualResults[$dayOrdinal]['actual_bookings'];
            }

            if (array_key_exists($dayOrdinal, $walkInResults)) {
                $day['walk-in_jumps'] = $walkInResults[$dayOrdinal]['walk-in_jumps'];
            }

            if($day['actual_jumps'] < $day['projected']) {
               $day['achievedProjected'] = false;
            } else {
                $day['achievedProjected'] = true;

            }
        }

        return $report;
    }

    public function load()
    {
        /*
        $report = [
            'siteId'   => '1',
            'siteName' => 'Minakami',
            'days'     => [
                1 => [
                    'Date'      => '1',
                    'name'      => 'Monday',
                    'projected' => '',
                    'actual'    => '',
                    'received'  => ''
                ],

                2 => [
                    'name'      => 'Monday',
                    'projected' => '',
                    'actual'    => '',
                    'received'  => ''
                ],

                3 => [
                    'name'      => 'Monday',
                    'projected' => '',
                    'actual'    => '',
                    'received'  => ''
                ],
            ],
        ];

        $dateTime = clone $this->dateTime;
        $dayOrdinal = $dateTime->format('j');
        $report['days'][(int)$dayOrdinal] = [
            'name'      => $dateTime->format('l')[0],//first character of the name
            'date'      => (int)$dateTime->format("j"),//the ordinal of the date, no leading zeros
            'projected' => 0,
            'actual'    => 0,
            'received'  => 0
        ];
        */

        $date = $this->dateTime->format('Y-m');
        //this table now is completely unnecessary as we are not saving anything
        //everything comes from projections and from the roster
        $reportSql = "SELECT * FROM public_relations_analysis WHERE `site_id` = $this->siteId AND `date` LIKE '$date%'";

        $results = queryForRows($reportSql);
        if (count($results)) {


            $report = [];
            foreach($results as $result){
                $id         = $result['id'];

                $received   = $result['received'];
                $ordinal    = (int)substr($result['date'], 8, 9);//get the last parts of the string as the day number

                $dateTime = DateTime::createFromFormat("Y-m-d", "$date-$ordinal");
                $firstLetterOfDayName = $dateTime->format('l')[0];

                $report['siteName'] = $this->siteName;
                $report['siteId'] = $this->siteId;
                $report['days'][$ordinal] = [
                    'id'        => $id,
                    'name'      => $firstLetterOfDayName,
                    'date'      => (int)$ordinal,
                    'projected' => 0,
                    'actual_jumps'    => 0,
                    'actual_bookings'    => 0,
                    'received_jumps'    => 0,
                    'received_bookings'    => 0,
                    'received'  => 0,
                    'walk-in_jumps'  => 0
                ];
            }

            echo '';
        } else {
            $report = $this->generateReport();
        }

        return $this->addBookingDataToTarget($report);
    }

    private function loadRosterProjectedJumps() {
        $monthStart = $this->dateTime->format("Y-m-d");
        $monthEnd = $this->endDateTime->format("Y-m-d");

        $sql = "
            SELECT
                DATE_FORMAT(roster_date, '%d') AS `ordinal`,
                jumps as jumps,
                site_id
            FROM roster_target
            WHERE
                roster_date >= '$monthStart'
                AND roster_date <= '$monthEnd'
                AND site_id = $this->siteId
                AND site_id <> 0 #Ignore projections on main
        ";

        $results = queryForRows($sql);
        $output = [];

        foreach ($results as $result) {
            $dayOrdinal = (int)$result['ordinal'];
            $output[$dayOrdinal]['projected']    = (int)$result['jumps'];
        }

        return $output;
    }

    private function loadWalkInJumps()
    {
        $monthStart = $this->dateTime->format("Y-m-d");
        $monthEnd = $this->endDateTime->format("Y-m-d");

        $sql = "
            SELECT
                DATE_FORMAT(DATE(`BookingDate`), '%d') AS `date`,
                sum(NoOfJump) AS total_jumps
                FROM `customerregs1`
            WHERE
                site_id = $this->siteId
                AND `BookingDate` BETWEEN '$monthStart' AND '$monthEnd'
                AND NoOfJump > 0
                AND DeleteStatus = 0
                AND (notes LIKE '%WALK-IN%' OR BookingType = 'Walk-in')
            GROUP BY `date`
            ORDER BY `date`
        ";

        $results = queryForRows($sql);
        $output = [];
        foreach ($results as $result) {
            $dayOrdinal = (int)$result['date'];
            $output[$dayOrdinal]['walk-in_jumps']    = (int)$result['total_jumps'];
        }

        return $output;

    }

    private function loadActualBookingsAndJumps()
    {
        $monthStart = $this->dateTime->format("Y-m-d");
        $monthEnd = $this->endDateTime->format("Y-m-d");

        $sql = "
            SELECT
                DATE_FORMAT(`BookingDate`, '%d') AS `date`,
                count(*) AS total_records,
                sum(NoOfJump) AS total_jumps
                FROM `customerregs1`
            WHERE
                site_id = $this->siteId
                AND `BookingDate` BETWEEN '$monthStart' AND '$monthEnd'
                AND NoOfJump > 0
                AND DeleteStatus = 0
            GROUP BY `date`
            ORDER BY `date`
        ";

        $results = queryForRows($sql);
        $output = [];
        foreach ($results as $result) {
            $dayOrdinal = (int)$result['date'];
            $output[$dayOrdinal]['actual_jumps']    = (int)$result['total_jumps'];
            $output[$dayOrdinal]['actual_bookings'] = (int)$result['total_records'];
        }

        return $output;
    }

    private function loadReceivedBookingsAndJumps()
    {
        $monthStart = $this->dateTime->format("Y-m-d");
        $monthEnd = $this->endDateTime->format("Y-m-d");

        $sql = "
            SELECT
                DATE_FORMAT(DATE(`BookingReceived`), '%d') AS `date`,
                count(*) AS total_records,
                sum(NoOfJump) AS total_jumps
                FROM `customerregs1`
            WHERE
                site_id = $this->siteId
                AND `BookingReceived` BETWEEN '$monthStart 00:00:00' AND '$monthEnd 00:00:00'
                AND NoOfJump > 0
                AND DeleteStatus = 0
            GROUP BY `date`
            ORDER BY `date`
        ";

        $results = queryForRows($sql);
        $output = [];
        foreach ($results as $result) {
            $dayOrdinal = (int)$result['date'];
            $output[$dayOrdinal]['received_jumps']    = (int)$result['total_jumps'];
            $output[$dayOrdinal]['received_bookings'] = (int)$result['total_records'];
        }

        return $output;
    }

    private function generateReport()
    {
        $dateTime = clone $this->dateTime;//clone because it will be modified
        $endDateTime = &$this->endDateTime;//reference because it will not be modified

        $report = [
            'siteId'   => '',
            'siteName' => '',
            'days'     => []
        ];

        while ($dateTime <= $endDateTime) {
            $dayOrdinal = $dateTime->format('j');
            $report['days'][(int)$dayOrdinal] = [
                'name'      => $dateTime->format('l')[0],//first character of the name
                'date'      => (int)$dateTime->format("j"),//the ordinal of the date, no leading zeros
                'projected' => 0,//load from Roster
                'actual_jumps'       => 0,
                'actual_bookings'    => 0,
                'received_jumps'     => 0,
                'received_bookings'  => 0,
                'received'  => 0,
                'walk-in_jumps'  => 0
            ];

            $dateTime->modify("+1 day");
        }

        return $report;
    }

    /*******************************************************************************************************************/
    /*************************************************DRAW FUNCTION*****************************************************/
    /*******************************************************************************************************************/

    private function drawSiteNameBox()
    {
        return "
        <div class ='site-name-box site-$this->siteId'>
            <div class='site-name-text'>$this->siteName $this->percentageChangeIncomeOver30Days </div>
        </div>";
    }

    private function drawTable()
    {
        $date = $this->dateTime->format('Y-m-01');

        $output = "<input type='hidden' value='$date' name='report[date]'/>\n";
        $output .= "<table class='public-relations-table'>\n";

        $firstCellContents = $this->drawSiteNameBox();
        //draw the day ordinal
        $output .= $this->drawRow($this->report['days'], 'date', 'Dates', false, false, $firstCellContents);
        $output .= $this->drawRow($this->report['days'], 'name', 'Days', false);
        $output .= $this->drawRow($this->report['days'], 'site_hits', 'Site Hits', false);
        $output .= $this->drawRow($this->report['days'], 'projected','Projected Jumps', false, false);
        $output .= $this->drawRow($this->report['days'], 'actual_jumps', 'Actual Jumps', false, false);
        $output .= $this->drawRow($this->report['days'], 'actual_bookings', 'Actual Bookings', false, false);
        $output .= $this->drawRow($this->report['days'], 'received_jumps', 'RCV ON Jumps', false, false);
        $output .= $this->drawRow($this->report['days'], 'received_bookings', 'RCV ON Bookings', false, false);
        $output .= $this->drawRow($this->report['days'], 'walk-in_jumps', 'Walk In Jumps', false, false);
        //$output .= $this->drawRow($this->report['days'], 'percent_income_delta', '% Income Last Year Over 30 Days', false, false);

        return $output . "</table>\n";
    }

    private function drawRow($days, $dayValueKey, $title = '', $enableInput = true, $enableIdField = false, $firstCellContents = '')
    {
        if ($title == '') $title = ucfirst($dayValueKey);

        $output = '';
        $output .= "<tr>\n";

        if($firstCellContents != '')//if there is something to put in the first cell
            $output .= "<td rowspan='9' class='site-name-cell'>$firstCellContents</td>\n";
        else
            $output .= "";

        $output .= $this->drawTitleCell($title);

        if ($enableInput)
            $output .= $this->drawCellInput($days, $dayValueKey, $enableIdField);
        else
            $output .= $this->drawCell($days, $dayValueKey);

        $output .= $this->drawTitleCell($title);
        $output .= "</tr>\n";

        return $output;
    }

    private function drawCell($days, $dayValueKey)
    {
        $output = '';
        foreach ($days as $dayIndex => $day) {

            $colorClass = '';
            if($dayValueKey == 'actual_jumps') {
                if($day['achievedProjected'] == false){
                    $colorClass = 'negative';
                }
            }

            $output .= "<td class='date-cell $colorClass'>{$day[$dayValueKey]}</td>\n";
        }

        return $output;
    }

    private function drawCellInput($days, $dayValueKey, $enableIdField = false)
    {
        $output = '';
        $dayValueKey = strtolower($dayValueKey);
        foreach ($days as $dayIndex => $day) {
            $output .= "<td class='date-cell'>";
            $output .= "<input type='text' name='report[sites][$this->siteId][$dayIndex][$dayValueKey]' value='{$day[$dayValueKey]}'/>";

            if($enableIdField && array_key_exists('id', $day)) {
                $output .= "<input type='hidden' name='report[sites][$this->siteId][$dayIndex][id]' value='{$day['id']}'/>";
            }

            $output .= "</td>\n";
        }

        return $output;
    }

    private function drawTitleCell($title)
    {
        $title = strtoupper($title);

        return "<td>$title</td>\n";
    }

    public function draw($showDateAdvancer = false)
    {
        $output = '';

        if ($showDateAdvancer) {
            $output .= $this->drawBackForwardDate();
        }
        $output .= $this->drawTable();

        return $output;
    }

    public function drawBackForwardDate()
    {
        $nextMonth = clone $this->dateTime;
        $nextMonth->modify("+1 month");

        $lastMonth = clone $this->dateTime;
        $lastMonth->modify("-1 month");

        $nextMonthString = $nextMonth->format("Y-m");
        $lastMonthString = $lastMonth->format("Y-m");

        return "<a href='?date=$lastMonthString'>&lt;&lt;&lt;</a><a href='#'></a>&gt;&gt;&gt;<a href='?date=$nextMonthString'></a>\n";
    }
}