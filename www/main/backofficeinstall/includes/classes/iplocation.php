<?php
/**
 * Class IpLocation
 *
 * This class uses the database from http://www.phptutorial.info/iptocountry/the_script.html
 * to determine where the user is located.
 *
 * This class will determine the location of a user based on their IP address. It will return a country code by looking
 * up the iP address in a set of files that can be downloaded from http://www.phptutorial.info/iptocountry/the_script.html
 * Calling:
 *
 *  $test = new IpLocation();
 *
 * Will result in using the $_SERVER['REMOTE_ADDR']
 *
 *
 * @package main
 * @author Ayodeji Osokoya deji@standardmove.com
 * @version @Revision: 1.0
 * @access public
 */
class IpLocation {
    private $iPAddress;
    /**
     * @var The country code as a two letter representation, ie: JP
     * @access private
     */
    private $twoLetterCode;

    /**
     * @var The country code as a three letter representation, ie: JPN
     * @access private
     */
    private $threeLetterCode;

    /**
     * @var The country code as regular words ie: United States, Japan
     * @access private
     */
    private $countryName;

    /**
     * @param null $iPAddressUser The iPAddress of the user
     */
    function __construct($iPAddressUser = null) {
        if ($iPAddressUser == null) $iPAddressUser = $_SERVER['REMOTE_ADDR'];
        if ($iPAddressUser == null) $iPAddressUser = "127.0.0.1";
        $this->iPAddress = $iPAddressUser;
        $this->findLocation($iPAddressUser);
        //$this->getTwoLetterCode();
        //$this->getThreeLetterCode();
        //$this->getCountryName();

    }

    /**
     * @param $iPAddressUser The iPAddress of the user
     * @return A string with a two letter code of the country or return null
     */
    function findLocation($iPAddressUser = null) {
        if ($iPAddressUser == null) $iPAddressUser = $this->iPAddress;
        $iPParts = explode(".", $iPAddressUser);

        $twoLetterCode = null;
        $ranges = array();//this will be recorded over in by the file included from
        require(dirname(__FILE__) . "/../vendor/ip_files/" . $iPParts[0] . ".php");//Use the first part of the ip address to load the corresponding file

        $iPCodeUser = ($iPParts[0] * 16777216) + ($iPParts[1] * 65536) + ($iPParts[2] * 256) + ($iPParts[3]);//ipaddress is converted to number for easy comparison

        foreach ($ranges as $startIpRange => $endIpRangeArray) {//endIpRangeArray contains the end range and a 2 letter code signifying the country
            if ($startIpRange <= $iPCodeUser) {
                if ($ranges[$startIpRange][0] >= $iPCodeUser) {
                    $twoLetterCode = $ranges[$startIpRange][1];
                    break;
                }
            }
        }
        unset($ranges);
        $this->twoLetterCode = $twoLetterCode;
        return $twoLetterCode;
    }
    /**
     * returns the iPAddress of the user
     */
    function getIPAddress() {
        return $this->iPAddress;//already defined in the constructor
    }

    /**
     * returns the country code as a 3 letter representation
     */
    function getTwoLetterCode() {
        return $this->twoLetterCode;//already defined in the constructor
    }

    /**
     * returns the country code as a 2 letter representation
     */
    function getThreeLetterCode() {
        if (!isset($this->threeLetterCode)) {
            $countries = array();//this is recorded over by the included countries.php files

            require(dirname(__FILE__)."/../vendor/ip_files/countries.php");
            $twoLetterCode = (string)$this->getTwoLetterCode();
            $this->threeLetterCode = $countries[$twoLetterCode][0];
            unset($countries);
        }
        return $this->threeLetterCode;
    }

    /**
     * returns the country code as a regular word(s)
     */
    function getCountryName() {
        if (!isset($this->countryName)) {
            $countries = array();//this is recorded over by the included countries.php files

            require(dirname(__FILE__)."/../vendor/ip_files/countries.php");
            $twoLetterCode = (string)$this->getTwoLetterCode();//php storm complains "illegal key type..." if we do not cast
            $this->countryName = $countries[$twoLetterCode][1];
            unset($countries);
        }
        return $this->countryName;
    }
}

?>
