
<?php
//takes a tableData Object and renders a table view
/**
 * Class TableView
 */
class TableView{
    /**
     * @var
     */
    private $tableData;

    /**
     * @var an array of configuration options for the table. Things like the id etc
     */
    private $parameters;
    /**
     * @var
     */
    public $columnCount;

    /**
     * @param $tableData
     * @param array $parameters
     */
    public function __construct($tableData, $parameters = array()){
        $this->tableData    = $tableData->data;
        $this->columnCount  = $tableData->columnCount;
        $this->parameters = $parameters;

        unset($tableData);
    }

    /**
     * @return string
     */
    public function startTable(){
        $id     = '';
        $class  = '';
        if(isset($this->parameters['id'])) $id = $this->parameters['id'];
        if(isset($this->parameters['class'])) $class = $this->parameters['class'];

       return "<table id='$id' class='$class'>";
    }

    /**
     * @return string
     */
    public function endTable(){
        return "</table>".PHP_EOL;
    }

    /**
     * @return string
     */
	public function drawTable(){
        $returnString  = $this->startTable();

        $returnString .= $this->drawHeader();
        $returnString .= $this->drawRows();
        $returnString .= $this->drawFooter();

        $returnString .= $this->endTable();
        return $returnString;
	}


    /**
     * @param $cell
     * @return string
     */
	function drawCell($cell){
        echo '';
        if(is_array($cell)){//cell is an associative array of keys and values
            $type = null;

            $this->addClass($cell, 'cell');
            $attributes = $this->setAttributes($cell);

            if(isset($cell['type'])) $type = $cell['type'];

            if($type == 'input'){
               $returnString = "<td><input type='text' $attributes></td>\n";

            } else {
                if(isset($cell['link'])) {
                    $returnString = "\t\t<td $attributes><a href='{$cell['link']}'>{$cell['value']}</a></td>\n";
                } else $returnString = "\t\t<td $attributes>{$cell['value']}</td>\n";//use this once cells are arrays
            }

        } else {//cell is a single value
            $returnString = "\t\t<td class='cell'>$cell</td>\n";
        }

        return $returnString;

		//Need to include styles
        //TODO! I need a way to get info from the parent rows and columns
        //getParentColumnAttributes: we could pass the parent column row and column in to find this
        //or we could store the cells row and column in it too $cell = array(value, row, column)
        //The style of the row superceeds that of the column or the other way round
        //The style of the cell superceeds that of the column or cell
	}

    /**
     * @return string
     */
	private function drawRows(){
        $returnString = "";
        foreach($this->tableData['rows'] as $rowIndex => &$row){
            $rowStyle = ($rowIndex % 2) ? "evenRow" : "oddRow";
            //would it make for sense for the row name to just be another cell in the data?

            //append an odd or even row style to the row
            if(!isset($row['classes'])) $row['classes'] = $rowStyle;
            else $row['classes'] .= ' '.$rowStyle;

            $attributes = $this->setAttributes($row);//Todo Should this happen in the data formatter???
            $returnString .= "\t<tr $attributes>\n";//set the classes to
            //$returnString .= "\t\t<td class='rowNames'>{$row['name']}</td>\n";
            foreach($row['data'] as $cellIndex => &$cell) {
                $returnString .= $this->drawCell($cell);
            }
            $returnString .= "\t</tr>\n";
        }
        return $returnString;
	}

    /**
     * @return string
     */
	private function drawHeader(){
        $returnString = '';
        foreach($this->tableData['header']as $headerRowIndex => &$headerRow){

            //we are adding a class string to an indexed array. Then when we iterate through the array it, that class
            //string is being indexed like it is an array
            //$headerRow = $this->addClass($headerRow, "headers");//This line is causing the problems
            $attributes = $this->setAttributes($headerRow);
            $returnString .= "\t<tr $attributes>\n";

            foreach($headerRow as $index => &$column){
                //if($index == 0) continue;//skip the first row as we printed it out above as this first coloumn is blank
                $column = $this->addClass($column, "columnName");
                $attributes = $this->setAttributes($column);

                $returnString .= "\t\t<th $attributes>{$column['value']}</th>\n";
            }
            $returnString .= "\t</tr>\n";
        }
        return $returnString."\n";
	}

    /**
     * @return string
     */
	private function drawFooter(){
        //if the footer is empty just return nothing
        if(!is_array($this->tableData['footer'])||!count($this->tableData['footer'])) return '';

        $returnString = "";
        $attributes = $this->setAttributes($this->tableData['footer']);
        $returnString .= "\t<tr $attributes>\n";//set the classes to

        //if the footer is empty just return nothing
        if(!is_array($this->tableData['footer'])||!count($this->tableData['footer'])) return '';

        foreach($this->tableData['footer']['data'] as $cellIndex => &$cell) {
            $returnString .= $this->drawCell($cell);
        }
        $returnString .= "\t</tr>".PHP_EOL;

        return $returnString;
	}

    /**
     * @param $data
     * @param $className
     * @return mixed
     */
    private function addClass($data, $className){
        if(!isset($data['classes'])) {
       //if(!array_key_exists("classes", $data)) {
            $data['classes'] = $className;//WHY IS THIS CAUSING A WARNING!!!!!!!!!
                //somehow we are passing strings to this function
        } else $data['classes'] .= " $className";
        //else $data['classes'] = "$className {$data['classes']}";
        return $data;
    }

    /**
     * @param $data
     * @return string
     */
    function setAttributes($data){
        $columnSpan = $classes = $id = $rowId = $columnId = $rowspan = $name = $readonly = $value = $rel = '';

        if(isset($data['rowId']))       $rowId      =  "????='{$data['rowId']}'";//id of the row in the database
        if(isset($data['columnId']))    $columnId   =  "????='{$data['columnName']}'";//id of the column in the database
        if(isset($data['rowspan']))     $rowspan    =  "rowspan='{$data['rowspan']}'";
        if(isset($data['colspan']))     $columnSpan =  "colspan='{$data['colspan']}'";
        if(isset($data['classes']))     $classes    =  "class='{$data['classes']}'";//This could be an array of classes
        if(isset($data['id']))          $id         =  "id='{$data['id']}'";//css id
        if(isset($data['value']))       $value      =  "value='{$data['value']}'";
        if(isset($data['readonly']))    $readonly   =  "readonly='{$data['readonly']}'";
        if(isset($data['name']))        $name       =  "name='{$data['name']}'";
        if(isset($data['rel']))         $rel       =  "rel='{$data['rel']}'";

        return trim("$columnSpan $name $rel $value $readonly $rowspan $id $classes $rowId $columnId");
    }
}
?>
