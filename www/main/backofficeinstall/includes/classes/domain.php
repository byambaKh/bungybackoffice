<?php

/**
 *This class is used to find the current subdomain regardless of the domain
 *we are currently in. The old code used to use a hardcoded string to figure it out
 *but this determines it dynamically.
 *
 * We assume domains are of the format site.bungyjapan.com and not something else like sub.site.bungyjapan.com
 *
 */
require_once dirname(__DIR__) . '/functions.php';
require_once __DIR__ . '/connection.php';

class Domain
{
    private $presentUrl;
    private $urlParts;
    private $urlPartsCount;
    private $siteId;
    private $nameEnglish;
    private $nameJapanese;
    private $showInBooking;
    private $hidden;
    private $status;
    //private $subdomain;
    private $shortName;
    private $displayName;
    private $showInSiteSwitchMenu;
    private $existsInDatabase;

    //function __construct($url = "www.google.com"){
    function __construct($url = null)
    {
        if (is_null($url)) $url = $_SERVER['HTTP_HOST'];

        $replacements = array("/^http:\/\//i", "/^https:\/\//i", "/^www\./i");

        $url = preg_replace($replacements, "", $url);

        if ($removedSubDirectories = strstr($url, '/', true)) {
            $url = $removedSubDirectories;
        }

        $this->presentUrl = $url;
        $this->urlParts = explode('.', $url);
        $this->urlPartsCount = count($this->urlParts);
        $this->findSudomainInformation($this->getSubdomain());
    }

    function findSudomainInformation($subdomain)
    {
        $sql = "SELECT * FROM sites WHERE subdomain = '$subdomain'";
        $subdomainInformation = queryForRows($sql);

        if (count($subdomainInformation)) {
            $this->existsInDatabase = true;
            $this->siteId = $subdomainInformation[0]['id'];
            $this->nameEnglish = $subdomainInformation[0]['name'];
            $this->nameJapanese = $subdomainInformation[0]['status'];
            $this->hidden = $subdomainInformation[0]['name_jp'];
            //$this->subdomain            = $subdomainInformation[0]['subdomain'];
            $this->shortName = $subdomainInformation[0]['short_name'];
            $this->displayName = $subdomainInformation[0]['display_name'];
            $this->status = $subdomainInformation[0]['status'];
            $this->showInBooking = !$subdomainInformation[0]['hidden'];
            $this->showInSiteSwitchMenu = $subdomainInformation[0]['showInSiteSwitchMenu'];

            return true;
        } else {
            $this->existsInDatabase = false;

            return false;
        }
    }

    function existsInDatabase()
    {
        return $this->existsInDatabase;
    }

    function getSiteId()
    {
        return $this->siteId;
    }

    function getStatus()
    {
        return $this->status;
    }

    function getDisplayName()
    {
        return $this->displayName;
    }

    function getSubdomain()
    {//returns the subdomain of the url ie minakami.bungyjapan.com returns bungyjapan.com
        if (is_array($this->urlParts)) {
            return $this->urlParts[0]; //the first bit of the url is the subd
        } else return '';
    }

    function getDomain()
    {
        if (is_array($this->urlParts)) {
            //the last minus 2 is the domain
            //mail.google.com
            //mail   = 0 //google = 1 //com    = 2

            //count will be 3
            //goole is 1
            if ($this->urlPartsCount > 1) {
                //copy the url array
                $urlPartsCopy = $this->urlParts;
                unset($urlPartsCopy[0]);//remove the subdomain
                $domain = implode('.', $urlPartsCopy);

                return $domain;
            } else return $this->urlParts[0];
        } else return '';
    }

    function getParts()
    {
        return $this->urlParts;
    }

    function getTopLevelDomain()
    {
        //return the last element in urlParts?
    }


    /*
    We could be more complicated and make cases for if the domain is
    bungyjapan.com or bungyjapan.co.jp. But this is good enough
    We would need to remove the domain suffix
    */
}

/*
*/
/*
$urls = array(
    "http://ryujin.localhost/",
    "http://ryujin.bungyjapan.com",
    "ryujin.localhost/",
    "http://www.ryujin.bungyjapan.com",
    "https://www.ryujin.bungyjapan.com"
);

foreach($urls as $url){
    $domain = new Domain($url);
    echo $domain->getSubDomain()."\n";
}
*/

?>
