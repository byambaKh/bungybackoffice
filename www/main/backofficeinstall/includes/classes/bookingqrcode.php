<?php
require_once( __DIR__ . '/../phpqrcode/qrlib.php' );

class BookingQRCode
{
    /**
     * Get a QR code representation of string passed in to the function.
     *
     * There are two methods of calling the QRcode because by default the class will output a png header when we don't want
     * it to causing HTML pages to be interpreted as pngs. We only want the image data without the header.
     *
     * 1. $headerFix = true will output an html header to replace the png header
     * 2. $headerFix = false will write the file to disc and then read it back in to a variable
     *
     * @param string $string
     * @param bool $headerFix
     * @param int  $size The Size of the QRCode
     *
     * @return string base64 HTML representation of the string
     */
    static function base64ImgString($string, $headerFix = false, $size = 10)
    {
        ob_start();

        if ($headerFix) {
            QRcode::png($string, false, QR_ECLEVEL_L, $size);
            $data = ob_get_clean();
            ob_end_clean();
            header('Content-Type: text/html');//overwrite the png headers that were already sent by the lib

        } else {
            $tempFilePath = "../../uploads/temp.png";
            QRcode::png($string, $tempFilePath, QR_ECLEVEL_L, $size);
            ob_end_clean();

            $data = file_get_contents($tempFilePath);
            unlink($tempFilePath);

        }

        $base64Data = base64_encode($data);

        return "<img alt='Embedded QR Code' src='data:image/png;base64,$base64Data' style='width:100%; height: auto;'/>";
    }

}

