<?php

/**
 *
 */
class ipAccess
{
    //http://snipplr.com/view/40837/phpmysql-ip-ban-list/
    /**
     * Stores the IP address as a string
     */
    protected $ipAddress;
    protected $ipStatus;

    /**
     * @param string IP Address as a string
     */
    public function __construct($ipAddress)
    {
        $this->ipAddress = $ipAddress;
        $this->iPStatus();
    }

    /**
     * @return string The status of the iP address. 'banned' or 'allowed'
     */
    public function getStatus()
    {
        if ('test for banning') {
            return 'banned';
        } else return 'allowed';
    }

    /**
     * Get the number of failed accesses depending on the parameters
     *
     * Queries the database and calculates the number of failed accesses of a particular type
     *
     * @param int $duration The number of milliseconds since now to condsider
     * @param mixed[] $types The type(s) of access failure (remoteLogin|wrong|password...)
     * @return int Returns the number of elements
     */
    public function getFailedAccess($duration, $types)
    {

    }

    /**
     * @return bool
     */
    public function isAllowed()
    {

    }

    /**
     * @return bool
     */
    public function isBlocked()
    {

    }

    /**
     * blocks ips in the database
     */
    public function blockIp()
    {

    }

    /**
     *
     */
    public function unblockIp()
    {

    }
}
