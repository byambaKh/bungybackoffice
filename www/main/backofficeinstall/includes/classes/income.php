<?php

class Income
{
/***************************************INCOME SHEET ONLY FUNCTIONS*****************************************************/
    public static function getFreeOfChargeDeductionDataIncomeSheet($site_id, $d, $all_data)
    {
        //jumps have these types in the foc column
        //These are the reason why the jump is free
        //bungy = staff jump, promo = promo jump, Media is media jump
        //Cancel = a previously cancelled jump that got a free ticket as someone did not show
        //NJ is a non jump that got free ticket to go again another time
        $freeOfChargeTypesQuery = "
            SELECT
                SUM(IF(foc LIKE 'Bungy%', NoOfJump, 0)) AS bungy,
                SUM(IF(foc LIKE 'Promo%', NoOfJump, 0)) AS promo,
                SUM(IF(foc LIKE 'Media%', NoOfJump, 0)) AS media,
                SUM(IF(foc LIKE 'Cancel%', NoOfJump, 0)) AS cancel,
                SUM(IF(foc LIKE 'NJ%', NoOfJump, 0)) AS non_jump,
                BookingDate AS date

            FROM customerregs1

            WHERE BookingDate LIKE '$d%'
                AND site_id = $site_id
                AND Rate = 0
                AND Checked = 1
                AND DeleteStatus = 0
                AND NoOfJump > 0
            GROUP BY BookingDate
            ORDER BY BookingDate;
        ";

        $freeOfChargeTypesResults = queryForRows($freeOfChargeTypesQuery);

        //transform in to a date indexed array of free of charge jumps and their types
        $freeOfChargeTypes = [];
        foreach ($freeOfChargeTypesResults as $freeOfChargeTypesResult) {
            $freeOfChargeTypes[$freeOfChargeTypesResult['date']] = $freeOfChargeTypesResult;
        }

        //create an array with all of the promo jumps and non jumps
        $freeOfChargeTypesNonJumps = [];//an array of all free of charge types and non jumps
        foreach ($all_data as $dateKey => $dayData) {
            $promo = 0;
            $media = 0;
            $bungy = 0;
            $nonJumps = 0;
            $cancels = 0;


            if (array_key_exists($dateKey, $freeOfChargeTypes)) {
                $promo = $freeOfChargeTypes[$dateKey]['promo'];
                $media = $freeOfChargeTypes[$dateKey]['media'];
                $bungy = $freeOfChargeTypes[$dateKey]['bungy'];
                $cancels = $freeOfChargeTypes[$dateKey]['cancel'];
                $nonJumps = $freeOfChargeTypes[$dateKey]['non_jump'];
            }

            $freeOfChargeTypesNonJumps[$dateKey] = [
                "promo"   => $promo,
                "media"   => $media,
                "bungy"   => $bungy,
                "nonJump" => $nonJumps,
                "cancel"  => $cancels,
            ];
        }

        return $freeOfChargeTypesNonJumps;
    }
    /**
     * On some sites a free of charge jumps are deducted from the tourism board total payment. This function calculates
     * the type of free of charge jumps.
     * @param int $site_id The id of the site
     * @param string $d The date for the month ie: 2015-07
     * @param array $all_data An array of income sheet rows
     *
     * @return array An array of days for the month with their associated bungy, promo, media, cancel and non_jump values
     */

    public static function getInsuranceTotalIncomeSheet($site_id, $freeOfChargeAndNonJumps, $day, $tbPayment)
    {
        $insuranceTotal = 0;
        if ($site_id != 3) {//There is a special calculation for Ryujin
            $freeOfChargeDeduction = static::freeOfChargeDeduction($site_id, $freeOfChargeAndNonJumps, $day);
            $insuranceTotal = $day['tboard_day_total'] = $day['insurance_no'] * $tbPayment - $freeOfChargeDeduction;

        } else if ($site_id == 3) {
            //Commission is only paid for jumpers that pay 15000, 14000, 13000
            //iterate through each array key and check for a/b/c jumps
            //day is a row from the incomesheet :

            /*
            [a8000] => 0
            [a5000] => 0
            [a_photo] => 0
            [a_cancel] => 0
            [a_total] => 0
            [b_tshirt_total_qty] => 0
            [b_tshirt_total] => 0
            [b_other] => 0
            [b_other_total] => 0
            [b_total] => 0
            [c8000] => 0
            [c_cancel] => 0
            [c_total] => 0
            [ab_total] => 0
            [insurance_no] => 0
            [tboard_day_total] => 0
            [a_topay] => 0
            [b_topay] => 0
            [daily_banked] => 0
            [insurance_no_total] => 0
            */
            foreach ($day as $dayKey => $value) {
                if (preg_match("/^[ac]\d+/", $dayKey, $matches)) {//match numbers starting with a or c
                    $rateKey = $matches[0];
                    $rate = ltrim($rateKey, "ac");
                    //if the jump costs these prices only do we add to the insurance total
                    if (in_array($rate, [15000, 14000, 13000]))
                        $insuranceTotal += $tbPayment * $day[$dayKey];
                }
            }
        }

        return $insuranceTotal;
    }
/*********************************************P&L SHEET FUNCTIONS*****************************************************/
    //free of charge jumps...
    //It has been requested that we insurance cost of free of charge jumps based on a set of rules that is specific to each
    //jump site. This is done in the form of a deduction from the total Tourism board payment
    public static function freeOfChargeDeduction($site_id, $freeOfChargeAndNonJumps, $day = [])
    {
        //Minakami OR Sarugakyo
        $deduction = 0;
        if ($site_id == 1 || $site_id == 2) {
            $cancel = $freeOfChargeAndNonJumps['cancel'];
            $promo = $freeOfChargeAndNonJumps["promo"];
            $nonJumps = $freeOfChargeAndNonJumps["nonJump"];
            $media = $freeOfChargeAndNonJumps["media"];
            $bungy = $freeOfChargeAndNonJumps["bungy"];

            $deduction = $promo * 1500 + $media * 1500 + $bungy * 1000 + $cancel * 0 + $nonJumps * 0;
        }

        //the deduction is zero for other sites
        return $deduction;
    }

    public static function getFreeOfChargeDeductionDataForMonth($siteId, $year)
    {
        //jumps have these types in the foc column
        //These are the reason why the jump is free
        //bungy = staff jump, promo = promo jump, Media is media jump
        //Cancel = a previously cancelled jump that got a free ticket as someone did not show
        //NJ is a non jump that got free ticket to go again another time
        $monthTemplate = ["Bungy" => 0, "Promo" => 0, "Media" => 0, "Cancel" => 0, "NJ" => 0];
        $months[$year."01"] = $monthTemplate;
        $months[$year."02"] = $monthTemplate;
        $months[$year."03"] = $monthTemplate;
        $months[$year."04"] = $monthTemplate;
        $months[$year."05"] = $monthTemplate;
        $months[$year."06"] = $monthTemplate;
        $months[$year."07"] = $monthTemplate;
        $months[$year."08"] = $monthTemplate;
        $months[$year."09"] = $monthTemplate;
        $months[$year."10"] = $monthTemplate;
        $months[$year."11"] = $monthTemplate;
        $months[$year."12"] = $monthTemplate;

        $freeOfChargeTypesQuery = "
        	SELECT
                SUM(IF(foc LIKE 'Bungy%', NoOfJump, 0))   AS bungy,
                SUM(IF(foc LIKE 'Promo%', NoOfJump, 0))   AS promo,
                SUM(IF(foc LIKE 'Media%', NoOfJump, 0))   AS media,
                SUM(IF(foc LIKE 'Cancel%', NoOfJump, 0))  AS cancel,
                SUM(IF(foc LIKE 'NJ%', NoOfJump, 0))      AS non_jump,
                DATE_FORMAT(BookingDate, '%Y%m')          AS `date`

            FROM customerregs1

            WHERE BookingDate LIKE '$year%'
                AND site_id = $siteId
                AND Rate = 0
                AND Checked = 1
                AND DeleteStatus = 0
                AND NoOfJump > 0
            GROUP BY `date`
            ORDER BY `date`;
        ";

        $freeOfChargeTypesResults = queryForRows($freeOfChargeTypesQuery);

        //transform in to a date indexed array of free of charge jumps and their types
        $freeOfChargeTypes = [];
        foreach ($freeOfChargeTypesResults as $freeOfChargeTypesResult) {
            $freeOfChargeTypes[$freeOfChargeTypesResult['date']] = $freeOfChargeTypesResult;
        }

        //create an array with all of the promo jumps and non jumps
        $freeOfChargeTypesNonJumps = [];//an array of all free of charge types and non jumps

        foreach ($months as $dateKey => $monthData) {
            $promo = 0;
            $media = 0;
            $bungy = 0;
            $nonJumps = 0;
            $cancels = 0;

            if (array_key_exists($dateKey, $freeOfChargeTypes)) {
                $promo    = $freeOfChargeTypes[$dateKey]['promo'];
                $media    = $freeOfChargeTypes[$dateKey]['media'];
                $bungy    = $freeOfChargeTypes[$dateKey]['bungy'];
                $cancels  = $freeOfChargeTypes[$dateKey]['cancel'];
                $nonJumps = $freeOfChargeTypes[$dateKey]['non_jump'];
            }

            $freeOfChargeTypesNonJumps[$dateKey] = [
                "promo"   => $promo,
                "media"   => $media,
                "bungy"   => $bungy,
                "nonJump" => $nonJumps,
                "cancel"  => $cancels,
            ];
        }

        return $freeOfChargeTypesNonJumps;
    }

    public static function getInsuranceTotal($site_id, $freeOfChargeAndNonJumps, $insuranceNumber, $tbPayment, $date)
    {

        /*
         * This is what $day looks like
        [a8000] => 0
        [a5000] => 0
        [a_photo] => 0
        [a_cancel] => 0
        [a_total] => 0
        [b_tshirt_total_qty] => 0
        [b_tshirt_total] => 0
        [b_other] => 0
        [b_other_total] => 0
        [b_total] => 0
        [c8000] => 0
        [c_cancel] => 0
        [c_total] => 0
        [ab_total] => 0
        [insurance_no] => 0
        [tboard_day_total] => 0
        [a_topay] => 0
        [b_topay] => 0
        [daily_banked] => 0
        [insurance_no_total] => 0
        */
        $insuranceTotal = 0;
        if ($site_id != 3) {//There is a special calculation for Ryujin
            $freeOfChargeDeduction = static::freeOfChargeDeduction($site_id, $freeOfChargeAndNonJumps);
            $insuranceTotal =  $insuranceNumber * $tbPayment - $freeOfChargeDeduction;
            //$insuranceTotal = $day['tboard_day_total'] = $day['insurance_no'] * $tbPayment - $freeOfChargeDeduction;

        } else if ($site_id == 3) {
            //Comission is only paid for jumpers that pay 15000, 14000, 13000
            //iterate through each array key and check for a/b/c jumps
            //day is a row from the incomesheet :

            $sql = "
                    SELECT SUM(`NoOfJump`) AS jumpers  FROM
                        customerregs1
                    WHERE site_id = 3
                    AND Rate IN (15000,14000,13000)
                    AND checked = 1
                    AND BookingDate LIKE '$date%'
                    AND DeleteStatus = 0
                    GROUP BY site_id;
                    ";//onsite and offsite jumps are added but second jumps are ignored

            $result = queryForRows($sql);
            $jumpers = $result['0']['jumpers'];
            $insuranceTotal = $tbPayment * $jumpers;
        }

        return $insuranceTotal;

        /*
        //this calculates the insurance total, but needs some testing
                	SELECT SUM(total) AS tbPayment, SUM(NoOfJump) as jumps, `date`, site_id, Rate  FROM (
        	#
        	(
        	SELECT
                (IF(foc LIKE 'Bungy%', NoOfJump, 0) * 1000)	    +
                (IF(foc LIKE 'Promo%', NoOfJump, 0) * 1500) 	+
                (IF(foc LIKE 'Media%', NoOfJump, 0) * 1500) 	+
                (IF(foc LIKE 'Cancel%', NoOfJump, 0)* 0) 		+
                (IF(foc LIKE 'NJ%', NoOfJump, 0)    * 0) 	AS total,
                `NoOfJump`,
                #(SUM(IF(foc LIKE 'Bungy%', NoOfJump, 0)) * 1000) 	+
                #(SUM(IF(foc LIKE 'Promo%', NoOfJump, 0)) * 1500) 	+
                #(SUM(IF(foc LIKE 'Media%', NoOfJump, 0)) * 1500) 	+
                #(SUM(IF(foc LIKE 'Cancel%', NoOfJump, 0))* 0) 		+
                #(SUM(IF(foc LIKE 'NJ%', NoOfJump, 0))    * 0) 	AS total,
                DATE_FORMAT(BookingDate, '%Y-%m-%d')          	AS `date`,
                site_id,
                'unknown' AS `value`,
                Rate

            FROM customerregs1

            WHERE BookingDate LIKE '2015-01-%'
                #AND site_id = 1
                AND site_id <> 3
                AND Rate = 0
                AND Checked = 1
                AND DeleteStatus = 0
                AND NoOfJump > 0
            ORDER BY `date`
            )
            UNION ALL
            (
         #Ryujin
            SELECT
            	#SUM(`NoOfJump`) * configuration.value AS jumpers,
            	`NoOfJump` * 500 AS total,#note that for ryujin, the tbPayment value is 500 not what is in the database as the prices changed on 2015-04-01 this is defined at the top of income.php
            	            	`NoOfJump`,
            	DATE_FORMAT(BookingDate, '%Y-%m-%d') AS `date`,
            	customerregs1.site_id,
            	configuration.value AS `value`,
            	Rate
            FROM
                  customerregs1
            LEFT JOIN configuration
            	ON (
            		configuration.site_id = customerregs1.site_id
            		AND configuration.key = 'tb_payment'
            	)
            WHERE customerregs1.site_id = 3
               AND Rate IN (15000,14000,13000)
               AND checked = 1
               AND BookingDate LIKE '2015-01-%'
               AND DeleteStatus = 0
             #GROUP BY `date`, site_id
             )
          ) AS union_result
          WHERE site_id = 3
          GROUP BY `date`, Rate
          #ORDER BY site_id, `date`;
        */
    }

    //Insurance No is the number of jumpers that need to be insured
    public static function getInsuranceNoMonthly($siteId, $secondJumpRate, $year){
        $sqlInsurance = "
            SELECT
                DATE_FORMAT(custreg.BookingDate, '%Y%m') as `date`,
                sum(
                    CAST((custreg.NoOfJump /*+ IFNULL(ms.total_2nd, 0)*/) AS SIGNED)
                    #+ CAST((custreg.2ndj_qty/* + IFNULL(ms.total_2nd, 0)*/) AS SIGNED)  /*2ndj_qty commented out to prevent double counting of 2nd jumps*/
                    - CAST(IF(custreg.Agent like '___Bungy' AND custreg.CollectPay = 'Offsite', custreg.NoOfJump, 0) AS SIGNED)/*j_test_jumps*/
                    - CAST(IF(custreg.Rate = '{$secondJumpRate}', custreg.NoOfJump, 0) AS SIGNED)/*j_2nd_jumps */
                    - CAST(IFNULL(nj.non_jump, 0) AS SIGNED) /*j_non_jumps*/
                ) AS j_insurance_no

            FROM customerregs1 AS custreg

            LEFT JOIN (
                SELECT DISTINCT w.id, cr.BookingDate, COUNT(w.id) as non_jump, cr.CustomerRegID as nonJumpRegID
                FROM customerregs1 cr, waivers w, non_jumpers
                WHERE
                cr.site_id = $siteId
                AND cr.BookingDate LIKE '$year-%'
                AND cr.Checked = 1
                AND cr.DeleteStatus = 0
                AND cr.CustomerRegID = w.bid
                AND w.id = non_jumpers.waiver_id
                GROUP BY cr.BookingDate
            ) nj ON nj.BookingDate = custreg.BookingDate AND custreg.CustomerRegID = nonJumpRegID
          /*
          currently I am ignoring the sales from the merchandise tables as this gives the wrong answer in some cases
          I think because we end up subtracting the value again in the daily_report.php calculation
            LEFT JOIN (
                SELECT DATE(sale_time) AS sale_date, IFNULL(SUM(sale_total_qty_2nd_jump), 0) AS total_2nd
                FROM merchandise_sales
                WHERE site_id = 1 AND sale_time LIKE '$year-%'
                GROUP BY sale_date
            ) AS ms ON ms.sale_date =  custreg.BookingDate
          */
            WHERE site_id = $siteId
                AND custreg.BookingDate LIKE '$year-%'
                AND custreg.Checked = 1
                AND custreg.DeleteStatus = 0
            GROUP BY `date`; ";

        $blank[$year."01"] = ['amount' => 0];
        $blank[$year."02"] = ['amount' => 0];
        $blank[$year."03"] = ['amount' => 0];
        $blank[$year."04"] = ['amount' => 0];
        $blank[$year."05"] = ['amount' => 0];
        $blank[$year."06"] = ['amount' => 0];
        $blank[$year."07"] = ['amount' => 0];
        $blank[$year."08"] = ['amount' => 0];
        $blank[$year."09"] = ['amount' => 0];
        $blank[$year."10"] = ['amount' => 0];
        $blank[$year."11"] = ['amount' => 0];
        $blank[$year."12"] = ['amount' => 0];

        $insuranceTotals = queryForRows($sqlInsurance);
        $data = [];
        foreach($insuranceTotals as $insuranceTotal) {
            $date = $insuranceTotal['date'];
            $data[$date]['amount'] = $insuranceTotal['j_insurance_no'];
        }

        $merged = array_replace($blank, $data);
        return $merged;
    }

    public static function getInsuranceTotalForMonth($site_id, $freeOfChargeAndNonJumps, $day, $tbPayment){

        return null;
    }
}