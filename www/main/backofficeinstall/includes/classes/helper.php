<?php

class BJHelper extends BJObject
{
    static public function getConfiguration($site_id = CURRENT_SITE_ID)
    {
        $config = array();
        $sql = "SELECT * FROM configuration WHERE site_id = '" . $site_id . "';";
        $res = mysql_query($sql) or die(mysql_error());
        while ($row = mysql_fetch_assoc($res)) {
            $config[$row['key']] = $row['value'];
        };

        return $config;
    }

    static public function getPlaces()
    {
        $places = array();
        $sql = "SELECT * FROM sites ORDER BY id;";
        $res = mysql_query($sql) or die(mysql_error());
        while ($place = mysql_fetch_assoc($res)) {
            $places[] = $place;
        }
        //$mainMenu = array_pop($places);
        //$mainMenu = array_push($places);
        return $places;
    }

    static public function getPlacesDropDown()
    {
        $places = self::getPlaces();

        return array_map(function ($item) {
            return array(
                'id'   => $item['id'],
                'text' => $item['name']
            );
        }, $places);
    }

    static public function getPlaceName($pid)
    {
        $places = self::getPlaces();
        $name = 'UNKNOWN';
        foreach ($places as $p) {
            if ($pid == $p['id']) {
                $name = $p['name'];
            };
        };

        return $name;
    }

    static public function getShortPlaceName($pid)
    {
        $sql = "SELECT * FROM sites where id = '$pid';";
        $res = mysql_query($sql) or die(mysql_error());
        $place_name = '';
        if ($place = mysql_fetch_assoc($res)) {
            $place_name = $place['short_name'];
        }

        return $place_name;

    }

    static public function getAgentRatesList($site_id = CURRENT_SITE_ID)
    {
        // get gived site config
        $sql = "SELECT * FROM configuration WHERE site_id = $site_id;";
        $res = mysql_query($sql) or die(mysql_error());
        $config = array();
        while ($row = mysql_fetch_assoc($res)) {
            $config[$row['key']] = $row['value'];
        };
        mysql_free_result($res);
        $agent_at_id = BJHelper::getAccessTypeID('agent');
        $sql = "SELECT r.price
        FROM users u
        LEFT JOIN user_roles r on (u.UserID = r.user_id and r.site_id = '" . $site_id . "')
        WHERE u.UserName = '{$_SESSION['myusername']}';";
        $res = mysql_query($sql) or die(mysql_error());
        $rates = array(0);

        $rates[] = $config['second_jump_rate'];

        if(array_key_exists('agent_jump_rate_1', $config)) {
            $rates[] = $config['agent_jump_rate_1'];//an extra rate to show in the agent menu
        }

        if ($config['repeater_rate'] > 0) {
            $rates[] = $config['repeater_rate'];
        };
        //$rates[] = $config['first_jump_rate'];
        if (($row = mysql_fetch_assoc($res)) && ($row['price'] > 0)) {
            $rates[] = $row['price'];
        } else {
            $rates[] = $config['default_agent_price'];
        };

        //byamba - add 20191129 add agent new price name is agent_price_new
        if ($config['agent_price_new'] > 0) {
            $rates[] = $config['agent_price_new'];
        };
        //end
        mysql_free_result($res);
        $rates = array_unique($rates);
        sort($rates);

        return array_map(function ($item) {
            return array(
                'id'   => $item,
                'text' => $item
            );
        }, $rates);
    }
    //returns a list of possible rates for a jump
    //ie: on Reception/groupBooking.php
    static public function getRatesList($first_empty = false)
    {
        //This use to use the rates table to get all of the rates but now
        //we are using lists_items so that a user can edit the lists themselves
        //$sql = "SELECT Amount FROM rates order by Amount ASC";
        $sql = "SELECT `value` FROM lists_items WHERE list_id=22 ORDER BY CAST(`value` AS SIGNED) ASC";//list_id for To Pay Agent Rates = 22
        $res = mysql_query($sql) or die(mysql_error());
        $result = array();
        if ($first_empty) {
            $result[] = array(
                'id'     => '-1',
                'text'   => '- Select -',
                'status' => false
            );
        };

        while ($row = mysql_fetch_assoc($res)) {
            $result[] = array(
                'id'   => $row['value'],
                'text' => $row['value']
            );
        };

        mysql_free_result($res);

        return $result;
    }

    static public function getStaffList($id_field = 'UserID', $first_empty = false)
    {
        $sql = "SELECT * FROM users WHERE StaffListName <> '' ORDER BY StaffListName ASC;";
        $res = mysql_query($sql) or die(mysql_error());
        $_list = array();
        while ($row = mysql_fetch_assoc($res)) {
            $_list[] = $row;
        };
        mysql_free_result($res);

        $result = array();
        if ($first_empty) {
            $result[] = array(
                'id'   => '',
                'text' => '--'
            );
        };
        foreach ($_list as $row) {
            $result[] = array(
                'id'   => $row[$id_field],
                'text' => $row['StaffListName']
            );
        };

        return $result;
    }

    static public function getStaffListByRolesAndSite($id_field = 'UserID', $user_roles, $site_id, $first_empty = false)
    {
        if(!is_array($user_roles)) return array();
        //convert the array of roles to a string like this 'role1','role2','role3','role4'
        foreach($user_roles as $index => $user_role) {
            $user_roles[$index] = "'".$user_role."'";
        }
        $access_roles_string = implode(", ", $user_roles);

        $sql = "
            SELECT
                u.*,
                access_type_id,
                act.type_name,
                act.alias,
                u.StaffListName
            FROM users AS u
            LEFT JOIN user_roles AS ur ON (u.UserID = ur.user_id)
            LEFT JOIN access_types AS act ON (act.id = access_type_id)
            WHERE
                StaffListName <> ''
                AND type_name IN ($access_roles_string)
                AND ur.site_id = $site_id
            ORDER BY StaffListName ASC;
        ";

        $res = mysql_query($sql) or die(mysql_error());
        $_list = array();
        while ($row = mysql_fetch_assoc($res)) {
            $_list[] = $row;
        }

        mysql_free_result($res);

        $result = array();
        if ($first_empty) {
            $result[] = array(
                'id'   => '',
                'text' => '--'
            );
        }

        foreach ($_list as $row) {
            $result[] = array(
                'id'   => $row[$id_field],
                'text' => $row['StaffListName']
            );
        }

        return $result;
    }

    static public function getStaffListByRole($roles)
    {
        if (is_scalar($roles)) $roles = array($roles);

        $roles = "'" . implode("', '", $roles) . "'";

        $sql = "SELECT DISTINCT u.StaffListName
				FROM users u 
				LEFT JOIN user_roles ur on (u.UserID = ur.user_id)
				LEFT JOIN access_types at on (ur.access_type_id = at.id)
				WHERE 
					(ur.site_id = '" . CURRENT_SITE_ID . "' OR ur.site_id = 0)
					AND at.type_name IN ($roles)";
        $res = mysql_query($sql) or die(mysql_error());
        $result = array();
        while ($row = mysql_fetch_assoc($res)) {
            $result[] = array(
                'id'   => $row['StaffListName'],
                'text' => $row['StaffListName']
            );
        };

        return $result;
    }

    static public function getList($group_alias = null, $list_alias = null)
    {
        // get needed lists IDs
        $where = array();
        $result = array();
        if (!is_null($group_alias)) {
            $where[] = "g.alias = '$group_alias'";
        };
        if (!is_null($list_alias)) {
            $where[] = "l.alias = '$list_alias'";
        };
        if (!empty($where)) {
            $sql = "SELECT l.id AS list_id, l.alias AS list_alias, g.id AS group_id, g.alias AS group_alias, li.id, li.value
          FROM lists_groups g 
          INNER JOIN lists l ON (g.id = l.group_id)
          INNER JOIN lists_items li ON (l.id = li.list_id)
          WHERE " . implode(' AND ', $where) . "
          ORDER BY g.alias ASC, l.alias ASC, li.value DESC";

            $res = mysql_query($sql) or die(mysql_error());
            while ($row = mysql_fetch_assoc($res)) {
                if (!array_key_exists($row['group_alias'], $result)) {
                    $result[$row['group_alias']] = array();
                };
                if (!array_key_exists($row['list_alias'], $result[$row['group_alias']])) {
                    $result[$row['group_alias']][$row['list_alias']] = array();
                };
                $result[$row['group_alias']][$row['list_alias']][] = array(
                    'id'   => $row['value'],
                    'text' => $row['value']
                );
            };
        };

        return $result;
    }

    static public function getListWithAdditionalFields($group_alias, $list_alias)
    {
        $group_id = self::getListGroupID($group_alias);
        $result = array();
        if (!is_null($group_id)) {
            $list_id = self::getListID($group_id, $list_alias);
            $sql = "SELECT alias, id FROM lists_add_columns WHERE list_id = '$list_id'";
            $res = mysql_query($sql) or die(mysql_error());
            $what = array(' ');
            $from = array();
            while ($row = mysql_fetch_assoc($res)) {
                $what[] = "lai" . $row['id'] . ".value as {$row['alias']}";
                $from[] = "LEFT JOIN lists_add_items lai" . $row['id'] . " on (lai{$row['id']}.column_id = '{$row['id']}' and li.id = lai{$row['id']}.item_id)";
            };

            // get needed lists IDs
            $sql = "SELECT l.id as list_id, l.alias as list_alias, li.id, li.value " . implode(', ', $what) . "
          FROM lists l
          INNER JOIN lists_items li on (l.id = li.list_id)
          " . implode("\n", $from) . "
          WHERE l.id = '$list_id'
          ORDER BY l.alias ASC, li.id ASC";
            $res = mysql_query($sql) or die(mysql_error());
            while ($row = mysql_fetch_assoc($res)) {
                $result[] = $row;
            };
        };

        return $result;
    }

    static public function createList($group_alias, $list_alias, $values)
    {
        $group_id = self::getListGroupID($group_alias);
        if (!is_null($group_id)) {
            $list_id = self::getListID($group_id, $list_alias);
            if (is_null($list_id)) {
                // create list
                $data = array(
                    'group_id' => $group_id,
                    'alias'    => $list_alias
                );
                db_perform('lists', $data);
                $list_id = mysql_insert_id();
                // create values
                foreach ($values as $row) {
                    $data = array(
                        'list_id' => $list_id,
                        'value'   => $row['text']
                    );
                    db_perform('lists_items', $data);
                };
            };
        };
    }

    static public function getListGroupID($group_alias)
    {
        $sql = "SELECT id from lists_groups WHERE alias = '$group_alias';";
        $result = null;
        $res = mysql_query($sql) or die(mysql_error());
        if ($row = mysql_fetch_assoc($res)) {
            $result = $row['id'];
        };

        return $result;
    }

    static public function getListID($group_id, $list_alias)
    {
        $sql = "SELECT id from lists WHERE group_id = '$group_id' AND alias = '$list_alias';";
        $result = null;
        $res = mysql_query($sql) or die(mysql_error());
        if ($row = mysql_fetch_assoc($res)) {
            $result = $row['id'];
        };

        return $result;
    }

    static public function addCompanyName($name)
    {
        $group_id = self::getListGroupID('general');
        $list_id = self::getListID($group_id, 'company');
        $data = array(
            'list_id' => $list_id,
            'value'   => $name
        );

        //get company names
        //if company name is not in company names
        //add it
        db_perform('lists_items', $data);
    }

    //Takes a concatenated list of list items. It will only add a new company name if the name does not exist in the list
    static public function addCompanyNameUniquely($name, $allCompanies)
    {
        $allNames = array();

        //create a simple list of just the names of the companies
        foreach ($allCompanies as $key => $company) {//generate a list of agents for so that we can make sure we don't add in duplicate
            $allNames[] = strtolower($company['text']);
        }
        if (!in_array(strtolower($name), $allNames)) {
            self::addCompanyName($name);
        }
    }

    static public function getBookingTypeList()
    {
        $result = array();
        $result[] = array(
            'id'   => '',
            'text' => '',
        );
        $sql = "SELECT * FROM bookingType ORDER BY CompanyName ASC;";
        $res = mysql_query($sql) or die(mysql_error());
        while ($row = mysql_fetch_assoc($res)) {
            $result[] = array(
                'id'   => $row['CompanyName'],
                'text' => $row['CompanyName'],
            );
        };

        return $result;
    }

    static public function getCalendarEventTimes()
    {
        $result = array();
        for ($i = 7; $i < 19; $i++) {
            $val = sprintf("%02d:00", $i);
            $result[] = array(
                'id'   => $val,
                'text' => $val
            );
        };

        return $result;
    }

    static public function getAccessTypeID($at_alias)
    {
        $sql = "SELECT * FROM access_types WHERE `alias` = '$at_alias'";
        $res = mysql_query($sql) or die(mysql_error());
        $result = null;
        if ($row = mysql_fetch_assoc($res)) {
            $result = $row['id'];
        };

        return $result;
    }

    static public function getAgents($all = false)
    {
        global $config;
        $agent_access_type_id = self::getAccessTypeID('agent');
        $sql = "SELECT DISTINCT u.username, u.UserID, ur.price, u.sort_order 
        FROM user_roles ur, users u
        WHERE 
          ur.user_id = u.UserID AND
		";
        if (!$all) {
            $sql .= "(site_id = 0 or site_id = '" . CURRENT_SITE_ID . "') AND ";
        };
        $sql .= " access_type_id = '$agent_access_type_id'
        ORDER BY sort_order DESC, username ASC;";
        $res = mysql_query($sql) or die(mysql_error());
        $agents = array();
        $processed = array();
        while ($row = mysql_fetch_assoc($res)) {
            if (!in_array($row['UserID'], $processed)) {
                $agents[] = array(
                    'id'    => $row['username'],
                    'text'  => $row['username'],
                    'price' => ($row['price'] > 0) ? $row['price'] : ($row['username'] == 'NULL' ? $config['first_jump_rate'] : $config['default_agent_price'])
                );
                $processed[] = $row['UserID'];
            };
        };

        return $agents;
    }

    static public function getBookingTimes()
    {
        $sql = "SELECT * FROM TimeMaster ORDER BY bookingTime ASC;";
        $res = mysql_query($sql) or die(mysql_error());
        $times = array();
        while ($row = mysql_fetch_assoc($res)) {
            $times[] = array(
                'id'   => $row['bookingTime'],
                'text' => $row['bookingTime'],
            );
        };

        return $times;
    }

    static public function getTemplate($template, $lang)
    {
        $sql = "SELECT * FROM templates WHERE site_id in (0, '" . CURRENT_SITE_ID . "') AND template_type = '$template' AND lang = '$lang' ORDER BY site_id DESC;";
        $res = mysql_query($sql) or die(mysql_error());
        $result = false;
        if ($row = mysql_fetch_assoc($res)) {
            $result = $row['template_content'];
        };

        return $result;
    }

    static public function getConfirmationEmailTemplate($lang)
    {
        return self::getTemplate('confirmation_email', $lang);
    }

    static public function getPaidConfirmationEmailTemplate($lang)
    {
        return self::getTemplate('confirmation_email_paid', $lang);
    }

    static public function getBookingTypes()
    {
        $sql = "SELECT CompanyName FROM bookingType ORDER BY CompanyName ASC";
        $res = mysql_query($sql) or die(mysql_error());
        $result = array();
        while ($row = mysql_fetch_assoc($res)) {
            $result[] = $row['CompanyName'];
        };

        return $result;
    }

    static public function getSiteEventColors()
    {
        $sql = "SELECT site_id, `value` FROM configuration WHERE `key` = 'events_color';";
        $res = mysql_query($sql) or die(mysql_error());
        $result = array();
        while ($row = mysql_fetch_assoc($res)) {
            $result[$row['site_id']] = $row['value'];
        };

        return $result;
    }

    static public function getEventColors()
    {
        $sql = "SELECT * FROM calendar_event_colours ORDER BY title;";
        $res = mysql_query($sql) or die(mysql_error());
        $result = array();
        while ($row = mysql_fetch_assoc($res)) {
            $result[] = $row;
        };

        return $result;
    }

    static public function getEventColorsDropDown()
    {
        $colors = self::getEventColors();
        $result = array();
        $result[] = array(
            'id'         => '',
            'text'       => 'default',
            'background' => 'FFF'
        );
        foreach ($colors as $c) {
            $result[] = array(
                'id'         => $c['color'],
                'text'       => $c['title'],
                'background' => $c['color']
            );
        };

        return $result;
    }

    static public function getRosterPositions()
    {
        $sql = "SELECT * FROM roster_position_all ORDER BY sort_order ASC, id ASC;";
        $res = mysql_query($sql) or die(mysql_error());
        $result = array();
        while ($row = mysql_fetch_assoc($res)) {
            $result[] = $row;
        };

        return $result;
    }

    static public function getRosterPositionsDropDown()
    {
        $positions = self::getRosterPositions();
        $result = array();
        $result[] = array(
            'id'   => '',
            'text' => '',
        );
        foreach ($positions as $p) {
            $result[] = array(
                'id'   => $p['id'],
                'text' => $p['name'],
            );
        };

        return $result;
    }

    static public function getRosterFunctions()
    {
        $sql = "SELECT * FROM roster_function_all ORDER BY sort_order ASC, id ASC;";
        $res = mysql_query($sql) or die(mysql_error());
        $result = array();
        while ($row = mysql_fetch_assoc($res)) {
            $result[] = $row;
        };

        return $result;
    }

    static public function getRosterFunctionsDropDown()
    {
        $positions = self::getRosterFunctions();
        $result = array();
        $result[] = array(
            'id'   => '',
            'text' => '',
        );
        foreach ($positions as $p) {
            $result[] = array(
                'id'     => $p['id'],
                'text'   => $p['name'],
                'bcolor' => $p['bcolor'],
                'tcolor' => $p['tcolor'],
            );
        };

        return $result;
    }

    static public function getRosterTemplates($site_id = CURRENT_SITE_ID)
    {
        $sql = "select * FROM roster_daily_templates WHERE site_id = '$site_id' order by max_jumps DESC, id ASC;";
        $res = mysql_query($sql) or die(mysql_error());
        $result = array();
        while ($row = mysql_fetch_assoc($res)) {
            $result[] = $row;
        };

        return $result;
    }

    static public function getRosterTemplatesDropDown($site_id = CURRENT_SITE_ID)
    {
        $positions = self::getRosterTemplates($site_id);
        $result = array();
        $result[] = array(
            'id'   => '',
            'text' => '',
        );
        foreach ($positions as $p) {
            $result[] = array(
                'id'         => $p['id'],
                'text'       => $p['template_name'],
                'color'      => $p['template_color'],
                'max_jumps'  => $p['max_jumps'],
                'min_jumps'  => $p['min_jumps'],
                'start_time' => $p['start_time'],
                'end_time'   => $p['end_time'],
            );
        };

        return $result;
    }

    static public function getInventoryTypes()
    {
        // get all item types
        $sql = "SELECT * FROM inventory_items_types ORDER BY type_name ASC;";
        $res = mysql_query($sql) or die(mysql_error());
        $item_types = array();
        while ($row = mysql_fetch_assoc($res)) {
            $item_types[$row['id']] = $row['type_name'];
        };
        mysql_free_result($res);

        return $item_types;
    }

    static public function getInventoryTypesList()
    {
        $positions = self::getInventoryTypes();
        $result = array();
        foreach ($positions as $id => $text) {
            $result[] = array(
                'id'   => $id,
                'text' => $text,
            );
        };

        return $result;
    }

    static public function getWaiversDropDown($d)
    {
        $sql = "
        SELECT t.* FROM (
          SELECT w.id, w.firstname, w.lastname, w.jump_number as jump_number
          FROM customerregs1 cr, waivers w
          WHERE 
            cr.site_id = '" . CURRENT_SITE_ID . "'
            and cr.BookingDate = '$d'
            and cr.CustomerRegID = w.bid
          ORDER BY cr.BookingTime ASC, w.lastname ASC, w.firstname ASC) t LEFT JOIN non_jumpers nj on (t.id = nj.waiver_id) WHERE nj.id IS NULL;";
        $result = array();
        $res = mysql_query($sql) or die(mysql_error());
        while ($row = mysql_fetch_assoc($res)) {
            $result[] = array(
                'id'   => $row['id'],
                'text' => $row['jump_number'] . ' ' . $row['lastname'] . ' ' . $row['firstname']
            );
        };

        return $result;
    }

    //add byamba - 2020-02-20 ↓ getting site managers name with default value
    static public function getManagersByRole($roles, $first = false)
    {
        if (is_scalar($roles)) $roles = array($roles);

        $roles = "'" . implode("', '", $roles) . "'";

        $sql = "SELECT DISTINCT u.StaffListName
                FROM users u 
                LEFT JOIN user_roles ur on (u.UserID = ur.user_id)
                LEFT JOIN access_types at on (ur.access_type_id = at.id)
                WHERE 
                    (ur.site_id = '" . CURRENT_SITE_ID . "' OR ur.site_id = 0)
                    AND at.type_name IN ($roles)";
        $res = mysql_query($sql) or die(mysql_error());

        $list = array();
        while ($row = mysql_fetch_assoc($res)) {
            $list[] = $row;
            /*
            array(
                'id'   => $row['StaffListName'],
                'text' => $row['StaffListName']
            );*/
        };

        mysql_free_result($res);
        $result = array();

        if ($first) {
            $result[] = array(
                'id'   => '',
                'text' => '--'
            );
        }

        foreach ($list as $row) {
            $result[] = array(
                'id'   => $row['StaffListName'],
                'text' => $row['StaffListName']
            );
        }

        return $result;
        
    }

    static public function getDescriptionData($first_empty = false)
    {
        $ddlData = array();
        $sql = "SELECT * FROM incident_report_descriptionddl; ";
        $res = mysql_query($sql) or die(mysql_error());
        
        $_list = array();
        while ($row = mysql_fetch_assoc($res)) {
            $_list[] = $row;
        }

        mysql_free_result($res);

        $result = array();
        if ($first_empty) {
            $result[] = array(
                'id'   => '',
                'text' => '--'
            );
        }

        foreach ($_list as $row) {
            $result[] = array(
                'id'   => $row['title'],
                'text' => $row['title']
            );
        }

        return $result;
    }

    //end ↑

}

?>
