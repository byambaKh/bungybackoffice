<?php 
/**
 * An abstract class to manage super global variables subclass for specific variables like post etc
 *
 * Instead of $_POST['something'] call $post = new post();
 * accessing is handled like an object so if ($post->something) is set, we will get 
 * the value or NULL if it is not set
 * This is much more preferable to having to 
 */

abstract class SuperGlobal{
	/**
	 * A variable that stores the super global being wrapped
	 */
	protected $superGlobalArray = array();

	/**
	 *
	 */
	function __construct()
	{
		$className = get_class($this); //get the current class name
		$superGlobalName = strtoupper($className);//prepend _ to it and upper case

        //$this->superGlobalArray = ${$superGlobalName};//this won't work
        //since superglobals cannot be used as variable variables, lets do this the hard way
        switch($superGlobalName) {
            case ("GLOBALS"):
                $this->superGlobalArray = &$GLOBALS;
                break;
            case ("POST"):
                $this->superGlobalArray = &$_POST;
                break;
            case ("GET"):
                $this->superGlobalArray = &$_GET;
                break;
            case ("SERVER"):
                $this->superGlobalArray = &$_SERVER;
                break;
            case ("FILES"):
                $this->superGlobalArray = &$_FIlES;
                break;
            case ("COOKIE"):
                $this->superGlobalArray = &$_COOKIE;
                break;
            case ("SESSION"):
                $this->superGlobalArray = &$_SESSION;
                break;
            case ("REQUEST"):
                $this->superGlobalArray = &$_REQUEST;
                break;
            case ("ENV"):
                $this->superGlobalArray = &$_ENV;
                break;
        }
	}

	/**
	 *
	 *
	 */
	//TODO: How do we handle N-dimensional superGlobal array access
	//implement the ArrayObject interface?
	//I would like to beable to do
		//$post->time->date->hour instead of $_POST['time']['date']['hour']
		//or do this getPath("time/date/hour")
    /**
     *
     */
	function applyFunction()
	{
        //run array walk over the array

	}

    /**
     * @param $path
     * @param $array
     * @param string $delimiter
     * @return mixed
     */
	function getByPath($path, $array, $delimiter = '/')
	{
		//convert the path to an array
		$pathArray = explode($delimiter, $path);
		return pathRecurse($pathArray, $array);
	}

    /**
     * @param $pathArray
     * @param $array
     * @param bool $reference
     * @return null
     */
    //TODO can't return things by reference
	private function pathRecurse($pathArray, $array, $reference = true)
	{
		if(!is_array($array)) return null;
		//get the first chunk of the path and remove from path array
		$pathPart		= array_shift($pathArray);
		$pathPartCount  = count($pathArray);

		if(isset($array[$pathPart])) {
			if($pathPartCount ==  0){ //we are at the end of the path so return what is at the end

                if($reference == true) return $array[$pathPart];
                else return $array[$pathPart];//if it is not an array but is set, just return the value
		
			} else { //path part count > 0 and we have some paths to find
				if(array_key_exists($pathPart, $array) && is_array($array[$pathPart]))
					return $this->pathRecurse($pathArray, $array[$pathPart]);
				else return null;//the path does not exist
			}
				
		} else return null;
	}

    function camelCaseToCaps($string)
    {
        //Second pattern gets rid of underscores at the beginning of the string if the first letter is a capital
        return strtoupper(preg_replace(array('/([A-Z])/', '/^_/'), array('_$1', ''), $string));
    }

    /**
     * @param $string
     * @return string
     */
    function capsToCamelCase($string)
    {
        $string = strtolower($string);
        $parts = explode('_', $string);

        $camelCase = '';
        foreach($parts as $partIndex => $part){
            if($partIndex == 0){
                $camelCase .= $part;
            } else {
                $camelCase .= ucfirst($part);
            }
        }
        return $camelCase;
    }

    /**
     * @param $key
     * @return null
     */
	function __get($key)
	{
        $key = $this->camelCaseToCaps($key);
		if(isset($this->superGlobalArray[$key])) {
			return $this->superGlobalArray[$key]; 
		} else return null;
	}

    /**
     * @param $key
     * @param $value
     * @return confirmed
     */
	function __set($key, $value)
	{
        $key = $this->capsToCamelCase($key);
	   //if the key exists set it
	   if(array_key_exists($key, $this->superGlobalArray)) {
           $this->superGlobalArray[$key] = $value;
           return true;
       } else {
           return false;
       }
	}
}
?>
