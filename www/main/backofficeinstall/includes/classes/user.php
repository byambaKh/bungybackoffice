<?php

class BJUser extends BJObject
{
    private $uid = null;
    private $data = array();
    private $permissions = array();
    private $access_types = array();
    private $roles = array();
    private $shouldUpdatePassword = false;
    private $baitAccount = false;

    public function __construct($uid = null)
    {
        $this->loadAccessTypes();
        if (is_null($uid) && isset($_SESSION['uid'])) {
            $this->uid = $_SESSION['uid'];
        };

        if (!is_null($this->uid)) {
            $this->loadUser();
        };
    }

    public function autoLogOn($login)
    {
        $sql = "SELECT * FROM users WHERE username = '" . mysql_real_escape_string($login) . "'";
        $res = mysql_query($sql) or die(mysql_error());
        if ($row = mysql_fetch_assoc($res)) {
            $this->authUser($row['UserName'], $row['UserPwd']);
        } else {
            return false;
        };

        return true;
    }

    public function getShouldUpdatePassword()
    {
        //checks if the user should update their password upon long
        return $this->shouldUpdatePassword;
    }

    public function setShouldUpdatePassword($boolean)
    {
        $booleanString = ($boolean ? "TRUE" : "FALSE");

        return mysql_query("UPDATE users SET shouldUpdatePassword = $booleanString WHERE UserID = $this->uid");
    }


    public function authUser($login, $pass)
    {
        $sql = "SELECT * FROM users WHERE username = '" . mysql_real_escape_string($login) . "' AND UserPwd = '" . mysql_real_escape_string($pass) . "'";
        $res = mysql_query($sql) or die(mysql_error());
        if ($row = mysql_fetch_assoc($res)) {
            $this->uid = $row['UserID'];
            $_SESSION['uid'] = $row['UserID'];
            $this->loadUser();
        } else {
            return false;
        };

        return true;
    }

    public function getID()
    {
        return $this->uid;
    }

    public function getUserName()
    {
        return $this->data['UserName'];
    }

    public function getStaffListName()
    {
        return $this->data['StaffListName'];
    }

    public function getATPermissions($alias)
    {
        if (!array_key_exists($alias, $this->access_types['by_alias'])) {
            return false;
        };
        if (!array_key_exists($this->access_types['by_alias'][$alias], $this->at_permissions)) {
            return false;
        };

        return $this->at_permissions[$this->access_types['by_alias'][$alias]];
    }

    private function loadUser()
    {
        if (!is_null($this->uid)) {
            $sql = "SELECT * FROM users WHERE UserID = {$this->uid};";
            $res = mysql_query($sql) or die(mysql_error());
            if ($row = mysql_fetch_array($res)) {
                $this->data = $row;
                $this->shouldUpdatePassword = $row['shouldUpdatePassword'];
                $this->loadUserPermissions();
                //baitAccounts are accounts that are there to see if someone will log in with those credentials
                //if database accounts are leaked these might be logged in to
                //$this->baitAccount = $row['value'];
            } else {
                die("User not exists with ID: {$this->uid}");
            };
        } else {
            die('UID not set');
        };
    }

    private function loadUserPermissions()
    {
        $sql = "SELECT * FROM user_roles WHERE user_id = {$this->uid}";
        $res = mysql_query($sql) or die(mysql_error());
        while ($row = mysql_fetch_array($res)) {
            $this->roles[$row['site_id']][] = $row['access_type_id'];
        };
        mysql_free_result($res);
    }

    public function hasPermission($directory, $site_id = CURRENT_SITE_ID)
    {
        $sites = array();
        if ($site_id == '*') {
            $sql = "SELECT * FROM sites WHERE status = '1'";
            $res = mysql_query($sql) or die(mysql_error());
            while ($row = mysql_fetch_assoc($res)) {
                $sites[] = $row['id'];
            };
        } else {
            $sites = array($site_id);
        };
        $result = false;
        foreach ($sites as $site_id) {
            if (empty($this->permissions) || !array_key_exists($site_id, $this->permissions)) {
                $this->permissions[$site_id] = array();
                $r = array();
                if (array_key_exists(0, $this->roles)) {
                    $r = array_merge($this->roles[0], $r);
                };
                if (array_key_exists($site_id, $this->roles)) {
                    $r = array_merge($this->roles[$site_id], $r);
                };
                $r = array_unique($r);
                foreach ($r as $access_type_id) {
                    if (!array_key_exists($access_type_id, $this->at_permissions)) continue;
                    $this->permissions[$site_id] = array_merge($this->permissions[$site_id], $this->at_permissions[$access_type_id]);
                };
                $this->permissions[$site_id] = array_unique($this->permissions[$site_id]);
                sort($this->permissions[$site_id]);
            };
            //echo '<pre>';
            //print_r($this->permissions);
            //echo '</pre>';
            $result |= in_array($directory, $this->permissions[$site_id]);
        };

        //if we are on the Head Office (main) and on the Roster page, return true
        //this gives everyone access to see the roster
        if ((CURRENT_SITE_ID == 0) && ($directory == 'Roster')) $result = true;
        if(($directory == 'Inventory') && ($this->getUserName() == 'Yu')) $result = true;//let Yu access inventory even as a staff user
        if ($directory == 'Help') $result = true;//give everyone default access to the Help directory
        //see the notes in user_auth.php for why
        return $result;
    }

    private function loadAccessTypes()
    {
        $sql = "SELECT * FROM access_types ORDER BY type_name ASC;";
        $res = mysql_query($sql) or die(mysql_error());
        $this->access_types = array(
            'by_id'    => array(),
            'by_name'  => array(),
            'by_alias' => array()
        );
        $this->at_permissions = array();
        while ($row = mysql_fetch_assoc($res)) {
            $this->access_types['by_id'][$row['id']] = $row;
            $this->access_types['by_name'][$row['type_name']] = $row['id'];
            $this->access_types['by_alias'][$row['alias']] = $row['id'];
            $this->at_permissions[$row['id']] = array();
        };
        mysql_free_result($res);
        $sql = "SELECT * FROM access_type_permissions ORDER BY access_type_id ASC";
        $res = mysql_query($sql) or die(mysql_error());
        while ($row = mysql_fetch_assoc($res)) {
            if (!array_key_exists($row['access_type_id'], $this->at_permissions)) {
                $this->at_permissions[$row['access_type_id']] = array();
            };
            $this->at_permissions[$row['access_type_id']][] = $row['directory'];
        };
        //echo '<pre>';
        //print_r($this->permissions);
        //print_r($this->at_permissions);
        //echo '</pre>';
        mysql_free_result($res);
    }

    public function hasRole($role_name)
    {
        if (!is_array($role_name)) {
            $role_name = array($role_name);
        }
        $result = false;
        foreach ($role_name as $role) {
            if (!array_key_exists($role, $this->access_types['by_name'])) {
                return false;
                die('Non existing user-role ' . $role);
            };
            $at_id = $this->access_types['by_name'][$role];
            if (empty($this->roles)) {
                return false;
            };
            if (
                (array_key_exists(0, $this->roles) && in_array($at_id, $this->roles[0]))
                || (
                    defined('CURRENT_SITE_ID')
                    && array_key_exists(CURRENT_SITE_ID, $this->roles)
                    && in_array($at_id, $this->roles[CURRENT_SITE_ID]))
            ) {
                $result = true;
            };
        };

        return $result;
    }

    public function getAgentPlaces()
    {
        $places = BJHelper::getPlaces();
        $allowed = array();
        $aatid = $this->access_types['by_alias']['agent'];
        foreach ($places as $p) {
            // bypass not active places
            if ($p['status'] != 1 || $p['hidden'] != 0) continue;
            if (!array_key_exists($p['id'], $this->roles) || !in_array($aatid, $this->roles[$p['id']])) {
                if (!array_key_exists(0, $this->roles) || !in_array($aatid, $this->roles[0])) continue;
            };

            $allowed[] = $p;
        };

        return $allowed;
    }

    /**
     * Return true if this person is a user or else return false
     *
     * If the user does not have a UID return false as they are not logged in or are not a valid user
     *
     * @return bool
     */
    public function isUser()
    {
        return $this->uid ? true : false;
    }

    public function isBaitAccount()
    {
        return $this->baitAccount ? true : false;
    }
}

?>
