<?php
/**
 * Created by PhpStorm.
 * User: bungyjapan
 * Date: 2/22/15
 * Time: 12:34 PM
 */
require_once(dirname(__FILE__).'/../functions.php');

class accessLog {
    function __construct(IpLocation $iplocation) {
        //user username
        $iPAddress = '';//$iplocation->getIPAddress();
        $countryName = $iplocation->getCountryName();
        $countryCode = $iplocation->getTwoLetterCode();
        $username = isset($_SESSION['myusername']) ? $_SESSION['myusername'] : '' ;
        $dateTime = date("Y-m-d H:i:s");

        $this->saveLog($iPAddress, $countryName, $countryCode, $username, $dateTime);
    }

    function checkProxyLink(){//creates a link to check if an ip is a proxy

    }

    function infoSniperLocation(){
        //http://www.infosniper.net/index.php?ip_address=101.98.153.130&map_source=1&overview_map=1&lang=1&map_type=1&zoom_level=7
        //gives the city and ...
    }

    function saveLog($iPAddress, $countryName, $countryCode, $username, $dateTime){
        $serverName = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : '';
        $requestUri = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : '';

        $data = array(
            'iPAddress' => $iPAddress,
            'countryName' => $countryName,
            'countryCode' => $countryCode,
            'username' => $username,
            'dateTime' => $dateTime,
            'userAgent' => isset($_SERVER['HTTP_USER_AGENT']) ? $_SERVER['HTTP_USER_AGENT'] : '',
            'host' => isset($_SERVER['HTTP_HOST']) ? $_SERVER['HTTP_HOST'] : '',
            'serverName' => isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : '',
            'serverArray' => json_encode($_SERVER),
            'language' => isset($_SERVER['HTTP_ACCEPT_LANGUAGE']) ? $_SERVER['HTTP_ACCEPT_LANGUAGE'] : '',
            'requestMethod' => isset($_SERVER['REQUEST_METHOD']) ? $_SERVER['REQUEST_METHOD'] : '',
            'referer' => isset($_SERVER['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '',
            'relativeUrl' => isset($_SERVER['PHP_SELF']) ? $_SERVER['PHP_SELF'] : '',
            'url' => $serverName.$requestUri
        );
        db_perform('accesslogs', $data);
        //save the relevant information to the database
    }
} 