<?php
	class BJFactory {
		private static $db = array();
		private static $user = null;
		private static $dbconfig = null;

		public static function getUser() {
			if (is_null(self::$user)) {
				self::$user = new BJUser();
			};
			return self::$user;
		}
		public static function getDB($name) {
			// check if object exists
			if (!array_key_exists($name, self::$db)) {
				$config = self::getDBConfig($name);
				self::$db[$name] = new BJDB($config['host'], $config['username'], $config['passwd'], $config['dbname']);
			};
			return self::$db[$name];
		}
		private static function getDBConfig($name = null) {
			if (is_null(self::$dbconfig)) {
				self::$dbconfig = new BJDBConfig();
			};
			return self::$dbconfig->getConfig($name);
		}
	}
?>
