<?php

//namespace Finance;

class ProfitAndLossTemp
{
    private $tableData;
    private $config;
    private $siteId;
    private $year;
    private $shortSiteName;
    private $shouldCalculateTotal;
    private $currentSiteDisplayName;
    private $sites;

    public function __construct($site_id, $year, $total = false)
    {
        $this->siteId = $site_id;
        $this->sites = $this->getAllSites();
        $this->year = $year;
        $this->tableData = $this->initTableData();
        $this->getConfiguration();
        $this->currentSiteDisplayName = $this->sites[$site_id]['display_name'];

        $this->shouldCalculateTotal = false;

        if ($total) $this->shouldCalculateTotal = true;

        //if we are on a site without a shortcode column in the expenses or banking table let shortname be null
        $shortNames = ['mk', 'sg', 'ib', 'ky'];

        $this->sortSiteName = null;
        if (array_key_exists($site_id - 1, $shortNames))
            $this->shortSiteName = $shortNames[$site_id - 1];//site_id's start from 1

        $this->calculateIncomeAndExpense();
    }

    private function siteFilterSql($prepend = '', $append = '')
    {
        if ($this->shouldCalculateTotal) {
            return '';//don't filter by any site
        } else {
            return "$prepend site_id = $this->siteId $append";//filter by the specified site
        }
    }

    public function getTableData()
    {
        return $this->tableData;
    }

    public function setTableData($tableData)
    {
        return $this->tableData = $tableData;
    }

    public function getSiteName()
    {
        return $this->currentSiteDisplayName;
    }

    private function getAllSites()
    {
        $places = queryForRows("SELECT * FROM sites ORDER BY id");

        $allPlaces = [];
        foreach ($places as $key => $place) {
            $allPlaces[$place['id']] = $place;
        }

        return $places;
    }

    private function getConfiguration()
    {
        //TODO return a configuration for no site (ie totalling all sites together)

        $sqlConfiguration = "select * from configuration where site_id = {$this->siteId};";
        $configurationResults = queryForRows($sqlConfiguration);
        foreach ($configurationResults as $configurationResult) {
            $this->config[$configurationResult['key']] = $configurationResult['value'];
        }
    }

    protected function calculateIncomeAndExpense()
    {
        $onsiteJumps = $this->calculateOnsiteJumps();
        $onsiteCancellations = $this->calculateOnsiteCancellations();

        $allOnsiteRows = array_merge($onsiteCancellations, $onsiteJumps);

        $onsiteSubtotal = $this->sumPaxIncomeRows($allOnsiteRows, "Onsite Subtotal");

        $offsiteJumps = $this->calculateOffsiteJumps();
        $offsiteCancellations = $this->calculateOffsiteCancellations();

        $allOffsiteRows = array_merge($offsiteCancellations, $offsiteJumps);
        $offsiteSubtotal = $this->sumPaxIncomeRows($allOffsiteRows, "Offsite Subtotal");

        $allBungyTotals = array_merge($offsiteSubtotal, $onsiteSubtotal);
        $bungyTotal = $this->sumPaxIncomeRows($allBungyTotals, "Bungy Total");//wrong in tableData only one entry

        $merchandise = $this->calculateMerchandise();
        $merchandiseTotal = $this->sumPaxIncomeRows($merchandise, 'Merchandise Total');
        $allIncomeRows = array_merge($bungyTotal, $merchandiseTotal);
        $incomeTotal = $this->sumPaxIncomeRows($allIncomeRows, "IncomeTotal");

        $tourismBoardPayments = $this->calculateTourismBoardPayments();
        $incomeMinusTourismBoard = $this->subtractRow($incomeTotal, $tourismBoardPayments, "Income Minus Tourism Board Payments");


        $expensesItems = $this->calculateExpenseItems();
        $expensesItemsTotal = $this->sumExpenseRows($expensesItems, "Expense Items Total");
        $expensesStaffSalary = $this->calculateStaffSalary();
        $expensesStaffSalaryTotal = $this->sumExpenseRows($expensesStaffSalary, "Wages For Staff");

        $totalProfitAndLoss = $incomeTotal[0][13]['amount'] - $tourismBoardPayments[0][13]['amount'] - $expensesItemsTotal[0][13]['amount'];

        $this->tableData['income']['bungy']['onsite']['jumps'] = $onsiteJumps;
        $this->tableData['income']['bungy']['onsite']['cancellations'] = $onsiteCancellations;
        $this->tableData['income']['bungy']['onsite']['total'] = $onsiteSubtotal;

        $this->tableData['income']['bungy']['offsite']['jumps'] = $offsiteJumps;
        $this->tableData['income']['bungy']['offsite']['cancellations'] = $offsiteCancellations;
        $this->tableData['income']['bungy']['offsite']['total'] = $offsiteSubtotal;

        $this->tableData['income']['bungy']['total'] = $bungyTotal;

        $this->tableData['income']['merchandise']['sales'] = $merchandise;
        $this->tableData['income']['merchandise']['total'] = $merchandiseTotal;
        $this->tableData['income']['total'] = $incomeTotal;

        $this->tableData['expenses']['tourismBoardPayments'] = $tourismBoardPayments;
        $this->tableData['expenses']['incomeMinusTourismBoardPayments'] = $incomeMinusTourismBoard;
        $this->tableData['expenses']['staffSalary'] = $expensesStaffSalary;
        $this->tableData['expenses']['staffSalaryTotal'] = $expensesStaffSalaryTotal;
        $this->tableData['expenses']['itemised']['items'] = $expensesItems;
        $this->tableData['expenses']['itemised']['total'] = $expensesItemsTotal;
        $this->tableData['total'][] = $totalProfitAndLoss;

        return;
    }

    protected function initTableData()
    {
        $tableData = [
            'income'   => [
                'headers'     => [
                    0 => ["INCOME", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "TOTALS"],
                ],
                'bungy'       => [
                    'onsite'  => [
                        'jumps'         => [],
                        'cancellations' => [],
                        'total'         => [],
                    ],
                    'offsite' => [
                        'jumps'         => [],
                        'cancellations' => [],
                        'total'         => [],
                    ],
                    'total'   => [],
                ],
                'merchandise' => [
                    'sales' => [
                        'tShirt' => [],
                        'photo'  => [],
                        'other'  => [],
                    ],
                    'total' => [],
                ],
                'total'       => [],
            ],
            'expenses' => [
                'headers'                         => [0 => ["EXPENSES", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC", "TOTALS"],],
                'subHeaders'                      => [0 => ["", "amount", "amount", "amount", "amount", "amount", "amount", "amount", "amount", "amount", "amount", "amount", "amount"]],
                'tourismBoardPayments'            => [],
                'incomeMinusTourismBoardPayments' => [],
                'staffSalary'                     => [],
                'siteSalary'                      => [
                    'sites' => [
                        0 => [
                            'total'  => '',
                            'people' => [
                                'name'   => 'xyz',
                                'salary' => '12356'
                            ],
                        ], //sites indexed by id or name
                    ], //by site and by person for head office
                ],
                'staffSalaryTotal'                => [],
                'expenseItems'                    => [],
                'expenseTotal'                    => [
                    'TOTAL EXPENSE', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0', '0'
                ],
            ],
            'total'    => [],
        ];

        return $tableData;
    }

    protected function sumPaxIncomeRows($rows, $title = "sumPaxIncomeRows() output")
    {
        $output = [];

        $output[0] = $title;
        foreach ($rows as $row) {
            foreach ($row as $cellIndex => $cell) {
                if ($cellIndex == 0) continue;//ignore the title cell
                $output[$cellIndex]['pax'] += $cell['pax'];
                $output[$cellIndex]['amount'] += $cell['amount'];
            }
        }

        return [$output];
    }

    protected function sumExpenseRows($rows, $title = "sumExpenseRows() output")
    {
        $output = [];

        $output[0] = $title;
        foreach ($rows as $row) {
            foreach ($row as $cellIndex => $cell) {
                if ($cellIndex == 0) continue;//ignore the title cell
                $output[$cellIndex]['pax'] += $cell['pax'];
                $output[$cellIndex]['amount'] += $cell['amount'];
            }
        }

        return [$output];
    }

    protected function subtractRow($rowsA, $rowsB, $title = "subTractRows() output")
    {
        $rowA = $rowsA[0];//since these are arrays of rows, take the first entry in each
        $rowB = $rowsB[0];

        $output = [];
        $output[0] = $title;
        foreach ($rowA as $index => $cell) {
            if ($index == 0) continue;
            $output[$index]['amount'] = $rowA[$index]['amount'] - $rowB[$index]['amount'];
        }

        return [$output];
    }

    protected function makeReadable($number)
    {
        return number_format((double)$number);
    }
    /**********************************************************************************************************************/
    /*******************************************INCOME FUNCTIONS***********************************************************/
    /**********************************************************************************************************************/

    protected function totalPaxIncomeHorizontal($row)
    {
        $horizontalSum = [
            'pax'    => 0,
            'amount' => 0
        ];

        foreach ($row as $index => $cell) {
            if ($index == 0) continue; //ignore the row title

            $horizontalSum['pax'] += $cell['pax'];
            $horizontalSum['amount'] += $cell['amount'];
        }

        return $horizontalSum;
    }

    protected function addPaxIncomeRows($rowA, $rowB)
    {
        foreach ($rowA as $index => $rowAcell) {
            if ($index == 0) continue; //ignore the row title
            if ($index == 13) break; //ignore the unused 13th column

            $rowA[$index]['pax'] += $rowB[$index]['pax'];
            $rowA[$index]['amount'] += $rowB[$index]['amount'];

        }

        return $rowA;
    }

    protected function addPaxIncomeRowsMonth($rowA, $rowB, $month)
    {
        foreach ($rowA as $index => $rowAcell) {
            if ($index == 0) continue; //ignore the row title
            if ($index <= $month) continue;//ignore month with actual data in favor of projected
            if ($index == 13) break; //ignore the unused 13th column

            $rowA[$index]['pax'] += $rowB[$index]['pax'];
            $rowA[$index]['amount'] += $rowB[$index]['amount'];

        }

        return $rowA;
    }

    protected function calculateMerchandise()
    {
        $photoPrice = configValueForKeyAndLocation("price_photo", strtolower($this->currentSiteDisplayName));
        if (is_null($photoPrice)) $photoPrice = 0;//no photoprice for main

        $sqlMerchandiseSecondJumps = "SELECT  DATE_FORMAT(sale_time, '%m') as yearmonth,
      			sum(sale_total_tshirt) as amount,
      			sum(sale_total_qty_tshirt) as pax,
      			sum(sale_total_qty_photo * 500) as amount_photo,#company collects 500 yen for every sale
      			sum(sale_total_qty_photo) as pax_photo,
      			sum(sale_total - sale_total_photo - sale_total_tshirt - sale_total_2nd_jump) as amount_other,
      			sum(sale_total_qty-sale_total_qty_photo-sale_total_qty_tshirt-sale_total_qty_2nd_jump) as pax_other
		FROM merchandise_sales
		WHERE
			sale_time like '$this->year-%'
		    {$this->siteFilterSql('AND')}
		GROUP BY yearmonth
		ORDER BY yearmonth ASC; ";

        $merchandise = queryForRows($sqlMerchandiseSecondJumps);

        $tShirts = $this->fillMissingIncomeMonths();
        $photos = $this->fillMissingIncomeMonths();
        $other = $this->fillMissingIncomeMonths();

        foreach ($merchandise as $month) {
            $tShirts[(int)$month['yearmonth']]['pax'] = $month['pax'];
            $tShirts[(int)$month['yearmonth']]['amount'] = $month['amount'];

            $photos[(int)$month['yearmonth']]['pax'] = $month['pax_photo'];
            $photos[(int)$month['yearmonth']]['amount'] = $month['amount_photo'];

            $other[(int)$month['yearmonth']]['pax'] = $month['pax_other'];
            $other[(int)$month['yearmonth']]['amount'] = $month['amount_other'];
        }

// tshirt from customerregs1
        $sqlBookingMerchandise = "SELECT
			DATE_FORMAT(BookingDate, '%m') as yearmonth,
			SUM(tshirt_qty) as pax,
			SUM(tshirt) as amount,
			SUM(photos / $photoPrice) as pax_photo,#
			SUM(photos*500/$photoPrice) as amount_photo,
			SUM(other / 100 + video / {$this->config['price_video']} + gopro / {$this->config['price_gopro']}) as pax_other,
			SUM(other + video + gopro) as amount_other
		FROM customerregs1
		WHERE
			BookingDate like '$this->year-%'
			{$this->siteFilterSql('AND')}
			AND Checked = 1
			AND NoOfJump > 0
			AND DeleteStatus = 0
		GROUP BY yearmonth
		ORDER BY yearmonth ASC;";

        $bookingMerchandise = queryForRows($sqlBookingMerchandise);

        foreach ($bookingMerchandise as $month) {
            $tShirts[(int)$month['yearmonth']]['pax'] += $month['pax'];
            $tShirts[(int)$month['yearmonth']]['amount'] += $month['amount'];

            $photos[(int)$month['yearmonth']]['pax'] += $month['pax_photo'];
            $photos[(int)$month['yearmonth']]['amount'] += $month['amount_photo'];

            $other[(int)$month['yearmonth']]['pax'] += $month['pax_other'];
            $other[(int)$month['yearmonth']]['amount'] += $month['amount_other'];
        }

        $tShirts[0] = 'T-Shirts';
        $photos[0] = 'Photos';
        $other [0] = 'Other';

        return [
            'tShirts' => $tShirts,
            'photos'  => $photos,
            'other'   => $other
        ];
    }

    protected function calculateOffsiteCancellations()
    {
        $sqlOffsiteCancellations = "SELECT
			DATE_FORMAT(BookingDate, '%m') AS yearmonth,
			SUM(CancelFeeQTY) AS pax,
			SUM(CancelFee * CancelFeeQTY) AS amount
		FROM customerregs1
		WHERE
			BookingDate LIKE '$this->year-%'
		    {$this->siteFilterSql('AND')}
			AND CancelFeeCollect = 'Offsite'
		GROUP BY yearmonth
		ORDER BY yearmonth ASC;";

        $offsitePaxAndAmounts = queryForRows($sqlOffsiteCancellations);
        foreach ($offsitePaxAndAmounts as $offsitePaxAndAmount) {
            $data[(int)$offsitePaxAndAmount['yearmonth']] = $offsitePaxAndAmount;
        }

        $data = [];
        $data[0] = 'Offsite Cancellations';
        $data = $this->fillMissingIncomeMonths($data);
        $data[13] = $this->totalPaxIncomeHorizontal($data);

        return [$data];//return an array of rows (one row in this case)
    }

    protected function calculateOffsiteJumps()
    {
        $ratesCharged = $this->allRatesChargedThisYear();

        $subtotals = [];
        $jumps = [];
        $secondJumps = [];
        foreach ($ratesCharged as $rate) {
            if ($rate == 0) continue;
            $jumps[$rate] = $this->fillMissingIncomeMonths($this->calculateOffsiteJumpsForRate($rate));
            $jumps[$rate][0] = (int)$rate;
        }

        $allJumps = [];
        foreach ($ratesCharged as $rateIndex => $rate) {
            if ($rate > 0) {
                $allJumps[$rateIndex] = $this->addPaxIncomeRows($jumps[$rate], $secondJumps[$rate]);
                $allJumps[$rateIndex][13] = $this->totalPaxIncomeHorizontal($allJumps[$rateIndex]);
            }
        }

        return $allJumps;

    }

    protected function calculateOffsiteJumpsForRate($rate)
    {

        $sqlOffsitePaxAndAmounts = "SELECT
				DATE_FORMAT(BookingDate, '%m') as yearmonth,
				sum(NoOfJump) as pax,
				sum(NoOfJump * Rate) as amount
			FROM customerregs1
			WHERE
				DeleteStatus = 0
			    {$this->siteFilterSql('AND')}
				AND Checked = 1
				AND Rate = $rate
				AND BookingDate like '$this->year-%'
				AND NoOfJump > 0
				AND CollectPay = 'Offsite'
			GROUP BY yearmonth
			ORDER BY yearmonth ASC;
		";
        $data = [];

        $offsitePaxAndAmounts = queryForRows($sqlOffsitePaxAndAmounts);
        foreach ($offsitePaxAndAmounts as $offsitePaxAndAmount) {
            $data[(int)$offsitePaxAndAmount['yearmonth']] = $offsitePaxAndAmount;
        }

        $data = $this->fillMissingIncomeMonths($data);
        $data[13] = $this->totalPaxIncomeHorizontal($data);
        $data[0] = 'Offsite Jumps';

        return $data;
    }

    protected function allRatesChargedThisYear()
    {
        global $config;

        $sql = "SELECT DISTINCT Rate
		FROM customerregs1
		WHERE
			DeleteStatus = 0
            #{$this->siteFilterSql('AND')}
			AND Checked = 1
			AND BookingDate like '$this->year-%'
			AND NoOfJump > 0
			AND Rate > 0
			AND CollectPay = 'Onsite'
		ORDER BY Rate DESC;";
        $rates = queryForRows($sql);//Put this in to the loop;

        $output = [];
        foreach ($rates as $rate) {
            $output[] = (int)$rate['Rate'];
        }
        $siteRates = [7500, 8000, 10000, 15000, 12000, 1000, 12000, 15000, 7000];

        $output = array_merge($siteRates, $output);
        //At least have the first jump rate so that projected jumps can be calculated
        if (count($output) == 0) $output[] = $config['first_jump_rate'];
        $output = array_unique($output);
        rsort($output);

        return $output;
    }

    protected function calculateOnsiteJumpsForRate($rate)
    {

        //Get all of number of jumps for each rate
        $sqlPaxAndAmount = "
            SELECT
				DATE_FORMAT(BookingDate, '%m') as yearmonth,
				sum(NoOfJump) as pax,
				sum(NoOfJump * Rate) as amount
			FROM customerregs1
			WHERE
				DeleteStatus = 0
                {$this->siteFilterSql('AND')}
				AND Checked = 1
				AND Rate = $rate
				AND BookingDate like '$this->year-%'
				AND NoOfJump > 0
				AND Rate > 0
				AND CollectPay = 'Onsite'
			GROUP BY yearmonth
			ORDER BY yearmonth ASC; ";

        $data = [];

        $paxAndAmounts = queryForRows($sqlPaxAndAmount);

        foreach ($paxAndAmounts as $paxAndAmount) {
            $data[(int)$paxAndAmount['yearmonth']] = $paxAndAmount;
        }

        //return nothing
        $data = $this->fillMissingIncomeMonths([]);

        return $data;
    }

    protected function calculateOnsiteProjectedJumps($rate, $site)
    {

        $startDate = date("2016-01-01");//today
        $endDate = "{$this->year}-12-31";//end of the year


        //note if start date exceeds end date, nothing is returned ie, it will only return values for this year
        $sqlPaxAndAmount = "SELECT
            DATE_FORMAT(roster_date, '%m') as yearmonth,
            sum(jumps) as pax,
            sum(jumps * $rate) as amount
            FROM roster_target
            WHERE
            roster_date > '$startDate'
            AND site_id = $site
            AND roster_date <= '$endDate'
            group by yearmonth; ";

        $data = [];

        $paxAndAmounts = queryForRows($sqlPaxAndAmount);
        foreach ($paxAndAmounts as $paxAndAmount) {
            $paxAndAmount['type'] = 'projected';

            $paxAndAmount['yearmonth'] = (int)$paxAndAmount['yearmonth'];
            $paxAndAmount['pax'] = (int)$paxAndAmount['pax'];
            $paxAndAmount['amount'] = (int)$paxAndAmount['amount'];
            $data[(int)$paxAndAmount['yearmonth']] = $paxAndAmount;
        }
        $data = $this->fillMissingIncomeMonths($data);
        $data[13] = $this->totalPaxIncomeHorizontal($data);
        $data[0] = $rate;

        return $data;
    }

    protected function calculateOnsiteSecondJumpsForRate($rate)
    {
        $data = [];
        if ($rate == $this->config['second_jump_rate']) {
            //add second jumps data
            $sqlSecondJumps = "SELECT
					DATE_FORMAT(BookingDate, '%m') as yearmonth,
					sum(2ndj / {$this->config['second_jump_rate']}) as pax,
					sum(2ndj) as amount
				FROM customerregs1
				WHERE
					DeleteStatus = 0
                    {$this->siteFilterSql('AND')}
					AND Checked = 1
					AND BookingDate LIKE '$this->year-%'
				/*	AND CollectPay = 'Onsite'*/
				GROUP BY yearmonth
				ORDER BY yearmonth ASC;
			";
            $secondJumps = queryForRows($sqlSecondJumps);

            foreach ($secondJumps as $secondJump) {
                $monthAsInt = (int)$secondJump['yearmonth'];
                if (!array_key_exists($monthAsInt, $data)) {
                    $data[(int)$secondJump['yearmonth']] = [
                        'pax'    => 0,
                        'amount' => 0
                    ];
                }
                $data[(int)$secondJump['yearmonth']]['pax'] += $secondJump['pax'];
                $data[(int)$secondJump['yearmonth']]['amount'] += $secondJump['amount'];
                unset($monthAsInt);
            }
            // add second jumps from merchandise
            $sqlMerchandiseJumps = "SELECT  DATE_FORMAT(sale_time, '%m') as yearmonth,
        			sum(sale_total_2nd_jump) as amount,
        			sum(sale_total_qty_2nd_jump) as pax
				FROM merchandise_sales
				WHERE
					sale_time like '$this->year-%'
                    {$this->siteFilterSql('AND')}
				GROUP BY yearmonth
				ORDER BY yearmonth ASC;
			";
            $merchandiseJumps = queryForRows($sqlMerchandiseJumps);
            foreach ($merchandiseJumps as $merchandiseJump) {
                $monthAsInt = (int)$merchandiseJump['yearmonth'];
                if (!array_key_exists($monthAsInt, $data)) {
                    $data[(int)$merchandiseJump['yearmonth']] = [
                        'pax'    => 0,
                        'amount' => 0
                    ];
                }
                $data[(int)$merchandiseJump['yearmonth']]['pax'] += $merchandiseJump['pax'];
                $data[(int)$merchandiseJump['yearmonth']]['amount'] += $merchandiseJump['amount'];
                unset($monthAsInt);
            }
        }
        $data = $this->fillMissingIncomeMonths([]);

        return $data;

    }

    protected function calculateOnsiteCancellations()
    {
        $data = [];
        $sqlOnsiteCancellations = "SELECT
			DATE_FORMAT(BookingDate, '%m') AS yearmonth,
			SUM(CancelFeeQTY) AS pax,
			SUM(CancelFee * CancelFeeQTY) AS amount
		FROM customerregs1
		WHERE
            {$this->siteFilterSql('', 'AND')}
			BookingDate LIKE '$this->year-%'
			AND Checked = 1
			AND CancelFeeCollect = 'Onsite'
			AND NoOfJump > 0
		GROUP BY yearmonth
		ORDER BY yearmonth ASC;";

        $onsiteCancellations = queryForRows($sqlOnsiteCancellations);
        foreach ($onsiteCancellations as $onsiteCancellation) {
            $data[(int)$onsiteCancellation['yearmonth']] = $onsiteCancellation;
        }

        $output = $this->fillMissingIncomeMonths($data);
        $output[0] = "Onsite Cancellations";

        return [$output];//return an array of rows (one row in this case)
    }

    protected function fillMissingIncomeMonths($row = [], $additionalParameters = [])
    {
        $blankPax = [
            0  => "unset",
            1  => ['pax' => 0, 'yearmonth' => 1, 'amount' => 0],
            2  => ['pax' => 0, 'yearmonth' => 2, 'amount' => 0],
            3  => ['pax' => 0, 'yearmonth' => 3, 'amount' => 0],
            4  => ['pax' => 0, 'yearmonth' => 4, 'amount' => 0],
            5  => ['pax' => 0, 'yearmonth' => 5, 'amount' => 0],
            6  => ['pax' => 0, 'yearmonth' => 6, 'amount' => 0],
            7  => ['pax' => 0, 'yearmonth' => 7, 'amount' => 0],
            8  => ['pax' => 0, 'yearmonth' => 8, 'amount' => 0],
            9  => ['pax' => 0, 'yearmonth' => 9, 'amount' => 0],
            10 => ['pax' => 0, 'yearmonth' => 10, 'amount' => 0],
            11 => ['pax' => 0, 'yearmonth' => 11, 'amount' => 0],
            12 => ['pax' => 0, 'yearmonth' => 12, 'amount' => 0],
            13 => ['pax' => 0, 'yearmonth' => 13, 'amount' => 0],
        ];

        //Add additional entries to the blank array
        //This is so that the blank array rows can have the form:
        // 1  => ['pax' => 0, 'yearmonth' => 1,  'amount' => 0, 'extra1' => '', 'extra2' => ''],
        foreach ($blankPax as $key => &$entry) {
            if ($key == 0) continue;

            $entry = array_merge($entry, $additionalParameters);
        }

        $data = array_replace($blankPax, $row);

        return $data;
    }

    protected function getFirstJumpRateForSite($siteId)
    {
        $query = "
        SELECT `value` FROM configuration
        WHERE `site_id` = $siteId
            AND `key` = 'first_jump_rate';
        ";

        $result = queryForRows($query);

        return $result[0]['value'];
    }

    protected function getSecondJumpRateForSite($siteId)
    {
        $query = "
        SELECT `value` FROM configuration
        WHERE `site_id` = $siteId
            AND `key` = 'second_jump_rate';
        ";

        $result = queryForRows($query);

        return $result[0]['value'];
    }

    /**
     * @return array
     * Onsite jumps = first jumps (for all prices) + second jumps + merchandise
     * if it is a future date, then the calculation is:
     * projected jumps *
     */
    protected function calculateOnsiteJumps()
    {
        global $config;

        //Onsite Jumps
        //get each of the prices charged for jumps during that month
        //then for each of those prices, perform a calculation


        $ratesCharged = $this->allRatesChargedThisYear();

        $jumps = [];
        $secondJumps = [];
        foreach ($ratesCharged as $rate) {
            if ($rate == 0) continue;
            $jumps[$rate] = $this->fillMissingIncomeMonths($this->calculateOnsiteJumpsForRate($rate));
            $jumps[$rate][0] = (int)$rate;// Set the title to the jump price
            $secondJumps[$rate] = $this->fillMissingIncomeMonths($this->calculateOnsiteSecondJumpsForRate($rate));
            $secondJumps[$rate][0] = (int)$rate;
            // process each online Rate
        }

        if ($this->shouldCalculateTotal) {
            /*
            $sitesRates = [
                1000,
                7000,
                7500,
                8000,
                10000,
                12000,
                12000,
                15000,
                15000,
            ];

            $ratesCharged = array_merge($sitesRates, $ratesCharged);
            $ratesCharged = array_unique($ratesCharged);
            rsort($ratesCharged);

            $projectedJumps = [];
            foreach ($sitesRates as $siteIndex => $siteRate) {
                $projectedJumps[$siteRate] = $this->calculateOnsiteProjectedJumps($siteRate, $siteIndex);
            }

            $projectedJumps = [];
            foreach ($ratesCharged as $siteIndex => $siteRate) {
                $rate = $this->getFirstJumpRateForSite($this->siteId);
                $projectedJumps[$siteRate] = $this->calculateOnsiteProjectedJumps($siteRate, $siteIndex);
            }
            */
            $siteRate = $this->getFirstJumpRateForSite($this->siteId);
            $projectedJumps[$siteRate] = $this->calculateOnsiteProjectedJumps($siteRate, $this->siteId);

            $allJumps = [];
            foreach ($ratesCharged as $rateIndex => $rate) {
                if ($rate > 0) {
                    if (array_key_exists($rate, $projectedJumps)) {
                        $jumps[$rate] = $this->addPaxIncomeRowsMonth($jumps[$rate], $projectedJumps[$rate], (int)date('m'));
                    }
                    $allJumps[$rateIndex] = $this->addPaxIncomeRows($jumps[$rate], $secondJumps[$rate]);
                    $allJumps[$rateIndex][13] = $this->totalPaxIncomeHorizontal($allJumps[$rateIndex]);
                }
            }

        } else {
            $projectedJumps[$config['first_jump_rate']] = $this->calculateOnsiteProjectedJumps($config['first_jump_rate'], $this->siteId);

            $allJumps = [];
            foreach ($ratesCharged as $rateIndex => $rate) {
                if ($rate > 0) {
                    //Disabled projections until the calculations are done
                    if ($rate == $config['first_jump_rate']) {
                        $jumps[$rate] = $this->addPaxIncomeRows($jumps[$rate], $projectedJumps[$rate]);
                    }
                    $allJumps[$rateIndex] = $this->addPaxIncomeRows($jumps[$rate], $secondJumps[$rate]);
                    $allJumps[$rateIndex][13] = $this->totalPaxIncomeHorizontal($allJumps[$rateIndex]);
                }
            }
        }


        return $allJumps;
    }
    /**********************************************************************************************************************/
    /*********************************************EXPENSE FUNCTIONS********************************************************/
    /**********************************************************************************************************************/
    protected function fillMissingExpenseMonths($row)
    {
        $blankExpense = [
            0  => "unset",
            1  => ['amount' => 0],
            2  => ['amount' => 0],
            3  => ['amount' => 0],
            4  => ['amount' => 0],
            5  => ['amount' => 0],
            6  => ['amount' => 0],
            7  => ['amount' => 0],
            8  => ['amount' => 0],
            9  => ['amount' => 0],
            10 => ['amount' => 0],
            11 => ['amount' => 0],
            12 => ['amount' => 0],
            13 => ['amount' => 0],
        ];

        $row = array_replace($blankExpense, $row);

        return $row;
    }

    protected function calculateTourismBoardPayments()
    {
        $sqlTourismBoardPayments = "SELECT
			DATE_FORMAT(`date`, '%m') AS yearmonth,
			SUM(tboard_day_total) AS amount
		FROM income
		WHERE
			site_id = $this->siteId
			AND `date` LIKE '$this->year-%'
		GROUP BY yearmonth
		ORDER BY yearmonth ASC;";

        $tourismBoardPayments = queryForRows($sqlTourismBoardPayments);
        $data = [];

        foreach ($tourismBoardPayments as $tourismBoardPayment) {
            $data[(int)$tourismBoardPayment['yearmonth']] = $tourismBoardPayment;
        }

        $data = $this->fillMissingExpenseMonths($data);
        $data[0] = "Tourism Board Payments";
        $data[13] = $this->totalPaxIncomeHorizontal($data);

        return [$data];
    }

    /*
    protected function calculateTourismBoardPayments($site_id, $freeOfChargeAndNonJumps, $day, $tbPayment)
    {
        $months = ["01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12"];

        foreach($months as $month) {
            $index = $this->year.$month;
            $data[$index]['amount'] = Income::getInsuranceTotalForEachMonth($site_id, $freeOfChargeAndNonJumps, $day, $tbPayment);
        }

        $data = $this->fillMissingExpenseMonths($data);
        $data[0] = "Tourism Board Payments";
        $data[13] = $this->totalPaxIncomeHorizontal($data);

        return [$data];
    }
    */

    protected function calculateStaffSalary()
    {
        //TODO conditional return based on siteId
        $sqlSalaries = "
        SELECT DATE_FORMAT(salary.date, '%m') as `date`, ROUND(SUM(`out`), 0) as `out`, who FROM (
            (SELECT
                `date`,
                `out` - `in` AS `out`,
                analysis,
                company AS who,
                'banking' AS origin_table
                FROM banking
                WHERE
                    description LIKE '%wages%'
                    {$this->siteFilterSql('AND')}
            )

            UNION

            (SELECT
                `date`,
                cost AS `out`,
                analysis,
                shop_name AS who,
                'expenses' AS origin_table
                FROM expenses
                WHERE description LIKE '%wages%'
                    {$this->siteFilterSql('AND')}
            )
            ";

        $sqlPercentageSalaries = "
            UNION
            (SELECT
                    `date`,
	                ($this->shortSiteName/100 * (`out` - `in`)) AS `out`,
	                'banking' AS origin_table,
	                company AS `who`,
	                analysis
	          FROM banking
              WHERE`date` LIKE '$this->year-%'
                    AND ($this->shortSiteName <> NULL OR $this->shortSiteName <> 0)
                    AND description LIKE '%wages%'
            )
            ";

        $sqlGroupOrder = "
            ) AS salary
            WHERE salary.date LIKE '$this->year-%'
            group by salary.date, `who`
            ORDER BY salary.date;

        ";

        //If we are on a site without a short name column, calculate percentage
        if ($this->shortSiteName == null)
            $sqlSalaries = $sqlSalaries . $sqlGroupOrder;
        else
            $sqlSalaries = $sqlSalaries . $sqlPercentageSalaries . $sqlGroupOrder;

        $staffSalariesResults = queryForRows($sqlSalaries);

        $monthsGroupedByPerson = [];
        foreach ($staffSalariesResults as $staffSalary) {
            $who = $staffSalary['who'];
            $amount = (int)$staffSalary['out'];
            $month = (int)$staffSalary['date'];

            $monthsGroupedByPerson[$who][$month]['amount'] = $amount;
        }

        $output = [];
        foreach ($monthsGroupedByPerson as $personName => $row) {
            $outputRow = $this->fillMissingExpenseMonths($row);
            $outputRow[0] = $personName;//make the first value be the title. This is okay outputRow[0] is the unset title
            $outputRow[13] = $this->totalExpenseHorizontal($outputRow);
            $output[] = $outputRow;//append the outputRow onto the list of outputRows
        }


        return $output;
    }

    protected function calculateExpenseItems()
    {
        if ($this->shortSiteName == null) {
            $sqlCombinedExpenses = "
            SELECT SUM(allExpenses.amount) AS amount,
              yearmonth,
              description,
              origin_table
              FROM (
            (SELECT
                    DATE_FORMAT(`date`, '%m') AS yearmonth,
                    description,
                    SUM(`out` - `in`) AS amount,
                    'banking' AS origin_table,
                    analysis
                FROM banking
                WHERE
                    {$this->siteFilterSql('', 'AND')}
                    `date` LIKE '$this->year-%'
                    AND description NOT LIKE'%Wages%'
                GROUP BY yearmonth, description, analysis
            )
            UNION
            (SELECT
                    DATE_FORMAT(`date`, '%m') AS yearmonth,
                    description,
                    SUM(`cost`) AS amount,
                    'expenses' AS origin_table,
                    analysis
                FROM expenses
                WHERE
                    {$this->siteFilterSql('', 'AND')}
                    `date` LIKE '$this->year-%'
                    AND description NOT LIKE '%Wages%'
                GROUP BY yearmonth, description, analysis
            )
        ) as allExpenses
        WHERE
        	description NOT LIKE '%Wages%'
        	AND description NOT LIKE 'BANK TRANSFER'
        	AND description NOT LIKE 'SALES DEPOSIT%'
        	AND description NOT LIKE 'PETTY CASH REIMBURSEMENT'
        	AND description NOT LIKE 'SALES DEPOSIT FROM Rafting Company, Pension ETC For Bungy Sales'
			AND analysis NOT LIKE 'Tourism Board'
        GROUP BY allExpenses.yearmonth, allExpenses.description
        ORDER BY allExpenses.description;
        ";
        } else {

            $sqlCombinedExpenses = "

        SELECT SUM(allExpenses.amount) AS amount,
          `yearmonth`,
          `description`,
          `origin_table`
          FROM (
            (SELECT
                    DATE_FORMAT(`date`, '%m') AS yearmonth,
                    description,
                    SUM(`out` - `in`) AS amount,
                    'banking' AS origin_table,
                    analysis
                FROM banking
                WHERE
                    {$this->siteFilterSql('', 'AND')}
                    `date` LIKE '$this->year-%'
                    AND description NOT LIKE'%Wages%'
                    AND ($this->shortSiteName = NULL OR $this->shortSiteName = 0)
                GROUP BY yearmonth, description, analysis
            )
            UNION
            (SELECT
                    DATE_FORMAT(`date`, '%m') AS yearmonth,
                    description,
                    SUM(`cost`) AS amount,
                    'expenses' AS origin_table,
                    analysis
                FROM expenses
                WHERE
                    {$this->siteFilterSql('', 'AND')}
                    `date` LIKE '$this->year-%'
                    AND description NOT LIKE '%Wages%'
                    AND ($this->shortSiteName = NULL OR $this->shortSiteName = 0)
                GROUP BY yearmonth, description, analysis
                )

            UNION
            #partial (Percentage Expenses from the mk, sg, ib, ky columns)
            (SELECT
	                DATE_FORMAT(`date`, '%m') AS `yearmonth`,
	                description,
	                SUM($this->shortSiteName/100 * (`out` - `in`)) AS `amount`,
	                'banking' AS origin_table,
	                analysis
	          FROM banking
              WHERE`date` LIKE '$this->year-%'
                    AND ($this->shortSiteName <> NULL OR $this->shortSiteName <> 0)
                    AND description NOT LIKE '%Wages%'
              GROUP BY yearmonth, description, analysis
              )
            UNION
            (SELECT
	                DATE_FORMAT(`date`, '%m') AS `yearmonth`,
	                description,
	                SUM($this->shortSiteName/100 * (`cost`)) AS `amount`,
	                'expenses' AS origin_table,
	                analysis
	          FROM expenses
              WHERE`date` LIKE '$this->year-%'
                    AND ($this->shortSiteName <> NULL OR $this->shortSiteName <> 0)
                    AND description NOT LIKE '%Wages%'
              GROUP BY yearmonth, description, analysis
            )
        ) as allExpenses
        WHERE
        	description NOT LIKE '%Wages%'
        	AND description NOT LIKE 'BANK TRANSFER'
        	AND description NOT LIKE 'SALES DEPOSIT%'
        	AND description NOT LIKE 'PETTY CASH REIMBURSEMENT'
        	AND description NOT LIKE 'SALES DEPOSIT FROM Rafting Company, Pension ETC For Bungy Sales'
			AND analysis NOT LIKE 'Tourism Board'
        GROUP BY allExpenses.yearmonth, allExpenses.description
        ORDER BY allExpenses.description;
        ";
        }

        //if we are on a site without a shortName code in the expenses query, ignore the partial expenses part


        $expenses = queryForRows($sqlCombinedExpenses);

        $monthsGroupedByExpense = [];

        foreach ($expenses as $expense) {
            //For some reason Packing & Shipping and Sales & Marketing can have extra spaces leading to duplicate keys
            $description = trim($expense['description']);
            $month = (int)$expense['yearmonth'];
            $amount = (int)$expense['amount'];

            //if descriptions exists in monthsGroupedByExpense array
            if (array_key_exists($description, $monthsGroupedByExpense)) {
                //check if the month key exists in output array['description']['month']
                if (array_key_exists($month, $monthsGroupedByExpense)) {
                    //if there is already an entry for the month, add the expense to it
                    //this should never run as the query does this part already
                    $monthsGroupedByExpense[$description]['amount'] += $amount;
                } else {
                    //if it does, sum it's amount with the existing amount
                    $monthsGroupedByExpense[$description][(int)$month]['month'] = (int)$month;
                    $monthsGroupedByExpense[$description][(int)$month]['amount'] = (int)$amount;
                }

            } else {
                //if the key does not exist add it and its amount to the to the monthsGroupedByExpense array
                $monthsGroupedByExpense[$description][(int)$month]['month'] = (int)$month;
                $monthsGroupedByExpense[$description][(int)$month]['amount'] = (int)$amount;
            }

        }

        $output = [];
        foreach ($monthsGroupedByExpense as $key => $expenseType) {
            $row = $this->fillMissingExpenseMonths($expenseType);
            $row[0] = $key;//make the first value be the title. This is okay row[0] is the unset title
            $row[13] = $this->totalExpenseHorizontal($row);
            $output[] = $row;//append the row onto the list of rows

        }

        return $output;
    }

    protected function totalExpenseHorizontal($row)
    {
        $horizontalSum = [
            'amount' => 0
        ];

        foreach ($row as $index => $cell) {
            if ($index == 0) continue; //ignore the row title

            $horizontalSum['amount'] += $cell['amount'];
        }

        return $horizontalSum;
    }
    /**********************************************************************************************************************/
    /*********************************************DRAW FUNCTIONS***********************************************************/
    /**********************************************************************************************************************/
    public function draw()
    {
        $currentYear = $this->year;
        $previousYear = $this->year - 1;
        $nextYear = $this->year + 1;
        $siteName = $this->currentSiteDisplayName;
        if ($this->shouldCalculateTotal == "") $siteName = "All Sites";

        //Income
        $htmlIncomeMonthHeaders = $this->drawRows($this->tableData['income']['headers'], "month-headers", "", "th", 2);
        $htmlIncomeColumnHeaders = $this->drawPaxAmountIncomeHeader();
        $htmlOnsiteJumps = $this->drawPaxAmountIncomeRows($this->tableData['income']['bungy']['onsite']['jumps']);
        $htmlOnsiteCancellations = $this->drawPaxAmountIncomeRows($this->tableData['income']['bungy']['onsite']['cancellations']);
        $htmlOnsiteSubtotal = $this->drawIncomeSubtotalRows($this->tableData['income']['bungy']['onsite']['total']);

        $htmlOffsiteJumps = $this->drawPaxAmountIncomeRows($this->tableData['income']['bungy']['offsite']['jumps']);
        $htmlOffsiteCancellations = $this->drawPaxAmountIncomeRows($this->tableData['income']['bungy']['offsite']['cancellations']);
        $htmlOffsiteSubtotal = $this->drawIncomeSubtotalRows($this->tableData['income']['bungy']['offsite']['total']);
        $htmlBungyTotal = $this->drawIncomeSubtotalRows($this->tableData['income']['bungy']['total']);

        $htmlMerchandise = $this->drawPaxAmountIncomeRows($this->tableData['income']['merchandise']['sales']);
        $htmlMerchandiseTotal = $this->drawIncomeSubtotalRows($this->tableData['income']['merchandise']['total']);

        $htmlIncomeTotal = $this->drawIncomeSubtotalRows($this->tableData['income']['total']);

        //Expenses
        $htmlExpensesMonthHeaders = $this->drawRows($this->tableData['expenses']['headers'], "expenses-month-headers", "", "th", 2);
        $htmlExpensesColumnHeaders = $this->drawAmountExpensesHeader();
        $htmlTourismBoardPayments = $this->drawExpensesRows($this->tableData['expenses']['tourismBoardPayments'], "pink", "amount", "td", 2);
        $htmlIncomeTotalMinusTourismBoardTotal = $this->drawExpensesRows($this->tableData['expenses']['incomeMinusTourismBoardPayments'], "income-total-tboard pink", "amount", "th", 2);
        $htmlStaffSalary = $this->drawExpensesRows($this->tableData['expenses']['staffSalary'], "", "amount", "td", 2);
        $htmlStaffSalaryTotal = $this->drawExpensesRows($this->tableData['expenses']['staffSalaryTotal'], "income-total-tboard pink", "amount", "td", 2);
        $htmlExpenseItems = $this->drawExpensesRows($this->tableData['expenses']['itemised']['items'], "", "amount", "td", 2);
        $htmlExpenseItemsTotal = $this->drawExpensesRows($this->tableData['expenses']['itemised']['total'], "expenses-subtotal-header", "amount", "td", 2);
        $htmlProfitAndLossTotal = $this->drawProfitAndLossTotal($this->tableData['total']);

        $output = "<table id='main-table'>
        <tr>
            <th class='main-header' colspan='27'>
                <h1>P&L Analysis $siteName - $currentYear</h1>
            </th>
        </tr>
        <tr>
            <th class='divider' colspan='27'>
                <a href='analysis_new.php?year=$previousYear' class='floatleft'>&lt;&lt;&lt; $previousYear</a>";

        if ($currentYear < date('Y')) {
            $output .= "<a href='analysis_new.php?year=$nextYear' class='floatright'>$nextYear&gt;&gt;&gt; </a>";
        }

        $output .= "</th>
        </tr>
        $htmlIncomeMonthHeaders
        <!--<tr class='column-headers'>-->
            $htmlIncomeColumnHeaders
        <!--</tr>-->
        $htmlOnsiteJumps
        $htmlOnsiteCancellations
        $htmlOnsiteSubtotal
        $htmlOffsiteJumps
        $htmlOffsiteCancellations
        $htmlOffsiteSubtotal
        $htmlBungyTotal
        $htmlMerchandise
        $htmlMerchandiseTotal
        $htmlIncomeTotal
        <tr>
            <th class='divider' colspan='27'>&nbsp</th>
        </tr>
        $htmlExpensesMonthHeaders
        $htmlExpensesColumnHeaders
        $htmlTourismBoardPayments
        $htmlIncomeTotalMinusTourismBoardTotal
        $htmlStaffSalary
        $htmlStaffSalaryTotal
        $htmlExpenseItems
        $htmlExpenseItemsTotal
        $htmlProfitAndLossTotal
        </table>
        ";

        return $output;
    }

    public function drawPaxAmountIncomeRows($rows)
    {
        $output = '';
        foreach ($rows as $index => $row) {
            $output .= "<tr>\n";

            foreach ($row as $indexRow => $cell) {
                if ($indexRow == 0) {
                    $output .= "<td class='first-col'>$cell</td>\n";

                } else {
                    if ($cell['pax'] == 0) $cell['pax'] = '-';
                    if ($cell['amount'] == 0) $cell['amount'] = '-';

                    $output .= "<td class='pax'>{$cell['pax']}</td>";
                    $output .= "<td class='amount'>{$this->makeReadable($cell['amount'])}</td>\n";
                }
            }

            $output .= "</tr>\n";
        }

        return $output;
    }

    public function drawExpensesRows($rows, $rowClass = '', $cellClass = '', $cellType = 'td', $colspan = 1)
    {
        $output = '';
        foreach ($rows as $indexRows => $row) {

            $output .= "<tr class='$rowClass'>\n";
            foreach ($row as $indexRow => $cell) {
                if ($indexRow == 0) {
                    $output .= "<$cellType class='first-col'>$cell</$cellType>\n";

                } else {
                    $output .= "<$cellType colspan='$colspan' class='$cellClass'>{$this->makeReadable($cell['amount'])}</$cellType>\n";

                }
            }

            $output .= "</tr>\n";
        }

        return $output;
    }

    public function drawRows($rows, $rowClass = '', $cellClass = '', $cellType = 'td', $colspan = 1)
    {
        $output = '';
        foreach ($rows as $indexRows => $row) {//Iterate through each of the rows

            $output .= "<tr class='$rowClass'>\n";
            foreach ($row as $indexRow => $cell) {//Iterate through each of the current row's cells
                if ($indexRow == 0) {
                    $output .= "<$cellType>$cell</$cellType>\n";

                } else {
                    $output .= "<$cellType colspan='$colspan' class='$cellClass'>$cell</$cellType>\n";

                }
            }
            $output .= "</tr>\n";
        }

        return $output;
    }

    public function drawPaxAmountIncomeHeader()
    {
        $output = "<tr class='column-headers'>\n";
        foreach ($this->tableData['income']['headers'] as $index => $cell) {
            if ($index == 0) {
                $output .= "<th class='first-col'></th>\n";

            } else {
                $output .= "<th class='pax'>pax</th>";
                $output .= "<th class='amount'>amount</th>\n";

            }
        }

        return $output . "</tr>\n";
    }

    public function drawAmountExpensesHeader()
    {
        $output = "<tr class='column-headers'>\n";
        foreach ($this->tableData['expenses']['subHeaders'][0] as $index => $cell) {
            if ($index == 0) {
                $output .= "<th class='first-col'></th>\n";

            } else {
                $output .= "<th class='amount' colspan='2'>amount</th>\n";

            }
        }

        return $output . "</tr>\n";
    }

    public function drawIncomeSubtotalRows($rows)
    {
        $output = '';
        foreach ($rows as $rowsIndex => $row) {
            $output = "<tr class='bungy-total'>\n";
            foreach ($row as $index => $cell) {
                if ($index == 0) {
                    $output .= "<th class='subtotal-header'>$cell</th>\n";

                } else {
                    $output .= "<th class='pax'>{$cell['pax']}</th>";
                    $output .= "<th class='amount'>{$this->makeReadable($cell['amount'])}</th>\n";
                }
            }
        }

        return $output . "</tr>\n";
    }

    protected function drawProfitAndLossTotal($value)
    {
        return "
        <tr class='profit-total'>
            <th class='subtotal-header'>Total Profit Or Loss</th>
            <th colspan='2' class='amount'>$value[0]</th>
            <th colspan='24' class='amount'>&nbsp;</th>
        </tr>";
    }

}



