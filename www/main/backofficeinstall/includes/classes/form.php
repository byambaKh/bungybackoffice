<?php 
//UI Elements

	//- SubmitButton
	//properties is an array with a title, custom color (not recommended) action, link? class
	//
	/*
	* functions can take a set of parameters such as title, url, class or they can take an array
	* such as array(title => 'title', url => 'url', class => 'class' attributes => 'sub array of html attributes');
        * the basic parameters are title, url, then $options specifies extra things
        *
	*/
require_once('html.php');

abstract class Form{
	//this array is merged with $options so that default values are set for all of the variables
	static $options = array( 
		'classes' => array(), 
		'target' => ''
	);

	static function beginForm($name, $action, $method){
        	return "<form name=\"$name\" action=\"$action\" method=\"$method\">";
	}

	static function endForm(){
		return "</form>";
	}

	static function actionButtonJs($title, $onClick, $options = null){
		//when pressed, calls some javascript and does something
		extract($options);//convert the array into variables
		//url
		//onClick
		//options
		//class specify extra classes to ass to the classes tag

  		$returnString = '<button type="button" class="btn btn-primary '.implode(' ', $classes).'">'.$title.'</button>';
		return $returnString;
	}

	static function statusClassForStatusType($status){
		$statusTypesClasses = array(
                	'warning' => 'danger',
                	'alert' => 'warning',
                	'action' => 'primary',
		);

		if(array_key_exists($status, $statusTypesClasses)){
                	return $statusTypesClasses[$status];
		}

		else return null;
	}

	static function beginButtonGroup($classes = ''){
        	return "<div class=\"btn-group $classes\">";
	}

	static function endButtonGroup(){
		return "</div>";

	}

	static function linkButton($title, $url, $options = array(), $status){//This could have a parent function that it calls with a class for each button type
		//ie warningLinkButton
		//When pressed, does goes to another page
		$options = array_merge(self::$options, $options);
		extract($options);//convert the array into variables

		if($statusClass = self::statusClassForStatusType($status)) $buttonStatusClass = 'btn-'.$statusClass;
		else $buttonStatusClass = '';


 		return '<a class="btn '.$buttonStatusClass.' '.implode(' ', $classes).'" href="'.$url.'" target="'.$target.'".>'.$title.'</a>'; 
	}                                                              

	static function backLinkButton($url, $options = array()){
		return self::linkButton('&lt;', $url, $options = array(), 'action');
	}
	
	static function actionLinkButton($title, $url, $options = array()){
		return self::linkButton($title, $url, $options = array(), 'action');
	}

	static function warningLinkButton($title, $url, $options = array()){
		return self::linkButton($title, $url, $options = array(), 'warning');
	}

	static function alertLinkButton($title, $url, $options = array()){
		return self::linkButton($title, $url, $options = array(), 'alert');
	}

	static function linkText($title, $url, $options = array()){
                $options = array_merge(self::$options, $options); 
		extract($options);//convert the array into variables
		//we have to define these in our own css file as bootstrap does not define colours for links

		if($statusClass = self::statusClassForStatusType($status)) $linkStatusClass = $statusClass;
		else $linkStatusClass = '';
		//get classes from the class value
		//TODO set append status classes


 		return '<a class="'.$linkStatusClass.' '.implode(' ', $classes).'" href="'.$url.'" target="'.$target.'".>'.$title.'</a>'; 
	}

	static function dropDown($elements, $options = array()){
		$dropDown = '<select>';
		foreach($elements as $key => $element){
			$selected = '';
			if(array_key_exists('selected', $element)) $selected == 'selected';
			$dropDown .= "<option value=\"{$element['value']}\" $selected> {$element['title']}</option>";
		}
		$dropDown .= '</select>';
		return $dropDown;

	}

	static function radioButton($name, $value, $title, $options = array()){
        //how about another option called "checkedForValue"
        //if value == checkedForValue then checked

		$checked = '';
		if(array_key_exists('checked', $options)) $checked = 'checked';
        if(array_key_exists('checkedForValue', $options)) {
            if($value == $options['checkedForValue']) $checked = 'checked';
        }

		return "<input type='radio' name='$name' $checked value='$value'>$title";
	}

	static function radioButtons($radioButtons, $options = array()){
        $radioButtonsString = '';
		foreach($radioButtons as $key => $radioButton){
			$name  = $radioButton['name'];
			$value = $radioButton['value'];
			$title = $radioButton['title'];

            $radioButtonsString .= self::radioButton($name, $value, $title, $options).'<br>';
		}

		return $radioButtonsString;
	}

    static function hidden($name, $value, $options = array()) {
        return "<input type='hidden' name='$name' value='$value'>\n";
    }

	static function checkBox($name, $value, $title, $options = array()){

		$checked = '';
		if(array_key_exists('checked', $options)) $checked = 'checked';

        //we declare a  hidden input so that if the field is not selected we at least get
        //something in POST set to zero. This is instead of the it just not being submitted
        //which happens by default.
        $returnString = "<input type='hidden' name='$name' value='0'>\n";
		$returnString.= "<input type='checkbox' name='$name' $checked value='$value'>$title";

        return $returnString;
	}

	static function checkBoxes($checkBoxes, $options = array()){
        	$checkBoxesString = '';
		foreach($checkBoxes as $key => $checkBox){
			$name  = $checkBox['name'];
			$value = $checkBox['value'];
			$title = $checkBox['title'];

                	$checkBoxesString .= self::checkBox($name, $value, $title, $options);
		}

		return $checkBoxesString;
	}

	static function submitButton($value, $options = array()){
		return "<input type=\"submit\" class=\"btn btn-success\" value=\"$value\">";
	}

	public static function upload($text, $action = '', $inputName = "file"){
		return"<input type='file' class='fileInput' name='$inputName'>";
	}

	public static function uploadMultiple($text, $action = '', $inputName = "file"){
		return"<input type='file' class='fileInput' multiple name='$inputName'>";
	}

	public static function menuPageUpload($text, $action, $inputName = "file"){
		return "<form class=\"inputBox\" action=\"$action\" method=\"post\" enctype=\"multipart/form-data\">
		<label for=\"$inputName\">Filename:</label>
		<input type=\"file\" class=\"fileInput\" name=\"$inputName\">
		<input type=\"submit\" class=\"button\" name=\"upload\" value=\"$text\">
		</form>";
	}
	//warningButton();
	//alertButton();
	//groupOfButtons();//an array of properties
	//text
	//textField

	//input();	
}



?>

