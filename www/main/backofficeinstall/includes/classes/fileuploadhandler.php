<?

class fileUploadHandler
{
	public $restrictedExtensions = [];
	public $forceFileName = '';//force the file to get a new name when uploaded
	public $uploadDirectory = '';//
	public $storeInUploadsFolder = true;//set this to false to upload outside of the uploads folder
	public $category = "";//The category column in hte uploads table
	public $arrayName = 'files';//the name of the array in $_['files']

	function __construct()
	{
		//
	}

	private function checkExtension($extension)
	{
		//if the extension is in the list of restricted extensions or there are is not list of
		//restricted extension allow the upload
		if((in_array(strtolower($extension), $this->restrictedExtensions)) || (count($this->restrictedExtensions) == 0))
			return true;
		else return false;
	}


	//from http://php.net/manual/en/features.file-upload.multiple.php
	private static function reArrayFiles(&$file_post)
	{
		$file_ary = array();
		$file_count = count($file_post['name']);
		$file_keys = array_keys($file_post);

		for ($i=0; $i<$file_count; $i++) {
			foreach ($file_keys as $key) {
				$file_ary[$i][$key] = $file_post[$key][$i];
			}
		}

		return $file_ary;
	}

	public function upload()
	{
		$relativeDirectory = "/" . $this->uploadDirectory;
		if($this->storeInUploadsFolder) $this->uploadDirectory = $_SERVER['DOCUMENT_ROOT'].'/uploads/'.$this->uploadDirectory;

		global $user;
		//for each file
		$_FILES[$this->arrayName] = self::reArrayFiles($_FILES[$this->arrayName]);

		foreach($_FILES['files'] as $filesIndex => $file)
		{
			$temp = explode(".", $file["name"]);
			$extension = strtolower(end($temp));//make the extension lowercase
			$originalName = $file["name"];
			$databaseFileName = $this->forceFileName ? $this->forceFileName : $originalName;

			$noErrors = ($file["file"]["error"] == 0);
			$allowedExtension = $this->checkExtension($extension);

            if($allowedExtension && $noErrors)
			{
				$storageName = $user->getID()."_".date('Ymd')."_".md5(rand(1,100000)).".".$extension;//A unique name for storing the file
				$destinationPath = $this->uploadDirectory . $storageName;
				if(move_uploaded_file($file["tmp_name"], $destinationPath)) {
				$data = [
					'originalName' => $originalName,
					'storageName' => $storageName,
					'directory' => $relativeDirectory,
					'filename' => $databaseFileName,
					'userId' => $user->getID(),
					'dateTime' => date('Y-m-d H:i:s'),
					'category' => $this->category,
					'site_id' => CURRENT_SITE_ID,
				];

                    db_perform('uploads', $data);
				} else {
					die('Upload Failed Please Try Again');
				}
			}
		}
	}
}
 
