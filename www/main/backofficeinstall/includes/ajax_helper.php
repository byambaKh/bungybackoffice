<?php
/* vim: set ts=4: */
require_once('../includes/application_top.php');

$action = $_GET['action'];

$user = new BJUser();
//check if the person is logged in or not before allowing them access to data
//if (!$user->isUser()) die();

$_GET['bDate'] = preg_replace("/([^-^0-9])/", "", $_GET['bDate']);
$result = array();
switch ($action) {
    case 'check_password_waiver':
        //check if the person is logged in or not before allowing them access to data
        if (!$user->isUser()) die();

        $result = array(
            'result' => 'failed'
        );
        if ($_POST['password'] == $config['waiver_password']) {
            $result['result'] = 'success';
            $_SESSION['waiver_password'] = $_POST['password'];
        };
        break;
    case 'check_password':
        //check if the person is logged in or not before allowing them access to data
        if (!$user->isUser()) die();

        $result = array(
            'result' => 'failed'
        );
        if ($_POST['password'] == $config['payroll_password']) {
            $result['result'] = 'success';
            $_SESSION['payroll_password'] = $_POST['password'];
        };
        break;
    case 'getAgentPrices':
        $place = $_GET['place'];
        $places = BJHelper::getPlaces();
        foreach ($places as $p) {
            if ($p['subdomain'] == $place) {
                $result['result'] = 'success';
                $result['options'] = BJHelper::getAgentRatesList($p['id']);
                $c = BJHelper::getConfiguration($p['id']);
                $result['repeater_rate'] = ($c['repeater_rate'] > 0) ? $c['repeater_rate'] : null;
                $result['second_jump_rate'] = ($c['second_jump_rate'] > 0) ? $c['second_jump_rate'] : null;
            };
        };
        break;
    case 'getBadDates':
        $place = $_GET['place'];
        $places = BJHelper::getPlaces();
        foreach ($places as $p) {
            if ($p['subdomain'] == $place) {
                $result['result'] = 'success';
                $result['options'] = getCalendarState($p['id']);
            };
        };
        break;
    case 'getBookTimes':
        $bDate = $_GET['bDate'];
        $place = $_GET['place'];

        $regid = (int)$_GET['regid'];
        $online = (boolean)$_GET['online'];
        $place_id = CURRENT_SITE_ID;
        if (isset($_GET['place'])) {
            $places = BJHelper::getPlaces();
            foreach ($places as $p) {
                if ($p['subdomain'] == $place) {
                    $place_id = $p['id'];
                };
            };
        };
    
    	if(isset($_GET['checkForPast'])) $checkForPast = $_GET['checkForPast'];
    	else $checkForPast = FALSE;
        $result = getBookTimes($bDate, $regid, $online, $place_id, $checkForPast);
        break;
    case 'getTimeState':
        //check if the person is logged in or not before allowing them access to data
        if (!$user->isUser()) die();

        $bDate = $_GET['bDate'];
        $result = getTimeState($bDate);
        break;
    case 'setTimeState':
        //check if the person is logged in or not before allowing them access to data
        if (!$user->isUser()) die();

        $bDate = $_POST['bDate'];
        $times = $_POST['times'];
        $result = setTimeState($bDate, $times);
        break;
    case 'getCordInfo':
        //check if the person is logged in or not before allowing them access to data
        if (!$user->isUser()) die();

        $cid2 = $_GET['cid2'];
        $cid3 = $_GET['cid3'];
        $result = getCordInfo($cid2, $cid3);
        break;
};


echo json_encode($result);
