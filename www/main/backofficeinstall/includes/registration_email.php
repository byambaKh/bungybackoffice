<?php
if(!isset($creditCardSimulation)) $creditCardSimulation = false;
if (!isset($lang)) $lang = 'jp';
if ($lang == 'jp') {
    setlocale(LC_ALL, "ja_JP.sjis");
    $booking_dir = 'Yoyaku';
} else {
    setlocale(LC_ALL, "en_GB.utf8");
    $booking_dir = 'Booking';
};
$booking_dir = "bookings";//ignore the old bookings
$to = $_POST['email'];
$jdate = DateTime::createFromFormat('Y-m-d', $jumpDate);
$replacement = array(
    'EMAIL_BOOK_DATE' => mb_convert_encoding(strftime("%Y-%m-%d (%a)", $jdate->getTimestamp()), 'UTF-8', 'SJIS'),
    'EMAIL_BOOK_TIME' => $chkInTime,
    'EMAIL_JUMP_NUMBER' => $noOfJump,
    'EMAIL_CUSTOMER_NAME' => strtoupper($romajiname),
    'EMAIL_CUSTOMER_PHONE' => $_POST['teleno'],
    'EMAIL_CONFIRMATION_LINK' => "http://" . $_SERVER['HTTP_HOST'] . "/$booking_dir/Thankyou.php?passkey=$confirmCode"
);
$headers = "Return-Path: {$config['confirmation_email']}\r\n";
//$headers .= 'MIME-Version: 1.0' . "\r\n";
$headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
mysql_query("SET NAMES UTF8;");

//if paid //figure out if the booking was paid or not was paid
//paidByCreditCard is defined in the calling script paymentreturn.php
if (!isset ($paidByCreditCard)) $paidByCreditCard = false;
$template = $paidByCreditCard ? BJHelper::getPaidConfirmationEmailTemplate($lang) : BJHelper::getConfirmationEmailTemplate($lang);

if ($template) {
    $file = explode("\n", $template);
    $subject = trim(array_shift($file));
    $body = implode("\n", $file);
} else {
    mail("{$config['confirmation_email']}", "Cannot locate customer confirmation template for $lang.", print_r($replacement, true), $headers);
};

$body = str_replace(
    array_map(function ($el) {
            return '{' . $el . '}';
        }, array_keys($replacement)),
    array_values($replacement),
    $body
);

$boundary = "next-part-" . rand(1000, 9999) . '-' . rand(100000, 999999);
//$headers .= 'Content-Transfer-Encoding: base64' . "\r\n";
$headers = "Return-Path: {$config['confirmation_email']}\r\n";
$headers .= "Reply-To: $subdomain@bungyjapan.com\r\n";
$headers .= "MIME-Version: 1.0\r\n";
$headers .= "Content-Type: multipart/alternative;\r\n";
$headers .= "\tboundary=\"$boundary\"\r\n";

$content = chunk_split(base64_encode($body));
//$text_content = chunk_split(base64_encode(strip_tags($body) . "\n" . $replacement['EMAIL_CONFIRMATION_LINK']));
$body = <<<EOT
--$boundary
Content-Type: text/plain; charset="UTF-8"
Content-Transfer-Encoding: base64

$content

--$boundary--
EOT;

mail($to, '=?UTF-8?B?' . base64_encode($subject) . '?=', $body, $headers, "-f {$config['confirmation_email']}");
?>
