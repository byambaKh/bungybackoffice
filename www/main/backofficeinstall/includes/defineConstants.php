<?php

$domainObject = new Domain();

//TODO: Use the refactor->rename tool in phpStorm to rename these constants to something sensible
define('CURRENT_SITE_ID', $domainObject->getSiteId());
define('CURRENT_SITE_DISPLAY_NAME', $domainObject->getDisplayName());
define('SYSTEM_DATABASE_INFO', $domainObject->getSubdomain());
define('APP_DOMAIN', $domainObject->getDomain());
define('APP_SUBDOMAIN', $domainObject->getSubdomain());
define('BOOKING_ROOT', '/');
define('SYSTEM_DOMAIN', $domainObject->getDomain());
define('SYSTEM_SUBDOMAIN', strtoupper($domainObject->getSubdomain()));
define('SYSTEM_SUBDOMAIN_REAL', strtoupper($domainObject->getSubdomain()));
define('EMAIL', 'info@bungyjapan.com');

$countryFromConfiguration = parse_ini_file(CONFIGURATION_PATH)['country'];
define('COUNTRY', $countryFromConfiguration ? $countryFromConfiguration : NULL );

$subdomain = $domainObject->getSubdomain();
$email = EMAIL;
$siteStatus = $domainObject->getStatus();

$currentSiteId = CURRENT_SITE_ID;

$lang = SYSTEM_SUBDOMAIN_REAL == 'AGENT' ? 'ja' : 'en';

//The maximum date that site calendars can go up to
$maxDateCustomer = "2021, 01, 28";//max date for customer calendar, month is zero indexed (january is 00 ;) )
$maxDate = "2021, 01, 28";//max date for the back office


//Booking Payments
$creditCardSimulation = false;

//Self Check In
$scaleSimulation = false;
$cashMachineSimulation = false;
