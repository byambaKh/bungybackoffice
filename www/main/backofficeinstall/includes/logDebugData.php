<?php
function logDebugData($data)
{
    $debugOutputFile = $_SERVER['DOCUMENT_ROOT'] . "/Reception/blankCheckinBug.txt";
    echo $debugOutputFile;

    $handle = fopen($debugOutputFile, 'a');

    if ($handle) {
        //echo "file found";
        if (is_array($data)) {
            //write print_r($data);
            $dataArrayAsString = print_r($data, true);
            $dataArrayAsString .= "\n\n";

            fwrite($handle, $dataArrayAsString);
        } else {
            fwrite($handle, $data . "\n\n");
        }
    }
}
