<?php
//if file exists
$iPFile = dirname(__FILE__) . '/../IPs.txt';
if (file_exists($iPFile)) {
    $ips = file($iPFile);
    reset($ips);

    while (list($key, $val) = each($ips)) {
        $ips[$key] = trim($val);
    }

    if (!isset($_POST['auth_user']) &&
        !isset($_GET['logout']) &&
        !isset($_SESSION['uid']) &&
        $_SERVER['SCRIPT_NAME'] == '/Waiver/index.php' &&
        (in_array($_SERVER['REMOTE_ADDR'], $ips))) {

        $user = new BJUser();
        $user->autoLogOn('Bungy');
    }
}