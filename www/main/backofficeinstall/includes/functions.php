<?php
require_once("classes/domain.php");
//TODO extract the email addresses to defined constants/variables

function getFOCTitles()
{
    global $config;
    $sql = "SELECT * FROM foc_values ORDER BY sort_order ASC;";
    $res = mysql_query($sql) or die(mysql_error());
    $titles = array();
    while ($row = mysql_fetch_assoc($res)) {
        $titles[] = array(
            'id' => $row['text'],
            'text' => $row['text']
        );
    };
    return $titles;
}

function getAgents()
{
    return BJHelper::getAgents();
}

function configValueForKeyAndLocation($key, $domain)
{
    $domain = strtolower($domain);
    $query = "SELECT value
                    FROM sites
                    LEFT JOIN
                    configuration ON (sites.id = configuration.site_id)
                WHERE
                    sites.subdomain = '{$domain}'
                AND configuration.key = '{$key}';";
    $result = queryForRows($query);
    return $result[0]['value'];
}

function getActiveCordCID3($cid2)
{
    $sql = "SELECT * FROM sites WHERE id = " . CURRENT_SITE_ID . ";";
    $res = mysql_query($sql) or die(mysql_error());
    if ($row = mysql_fetch_assoc($res)) {
        $cid1 = $row['short_name'];
    } else {
        die("Cannot locate cord CID1 for current site");
    };
    //select the active cord for this site
    $sql = "SELECT * FROM cord_logs WHERE site_id = '" . CURRENT_SITE_ID . "' AND cid1 = '$cid1' and cid2='$cid2' and status = 1;";
    $res = mysql_query($sql) or die(mysql_error());
    $cid3 = '';//return nothing if there are no active cords
    if (mysql_num_rows($res) == 1) {
        $row = mysql_fetch_assoc($res);
        $cid3 = $row['cid3'];//return the cord id if there is 1 active cord

    } else if (mysql_num_rows($res) > 1) {
        $colorName = array(
            "SL" => 'yellow',
            'L' => 'red',
            'H' => 'blue',
            'SH' => 'black'
        );

        $color = $colorName[$cid2];
        //If there is more than one active cord there is something wrong that needs to be fixed
        //Just terminate the operations
        die("<h1>Error: More that one $color cord is active. Only one cord of a particular color may be activated at a time. Please go to Cord Logs and retire a cord.</h1>");//fail if there are more than 1 active cords
    }
    return $cid3;
}

/**
 * Get the latest cord info from the daily_reports table
 *
 * This function is a duplicate getCordInfo but is designed to get the latest cord info in daily report that does has
 * all of the eq_cord_ct_* (jump count) and all of the eq_cord_uvt_* (UV hours) values filled in. This function is used
 * in the cord_logs.php file and is called once for each cord colour (yellow, blue, red, black). There was previously
 * a 'bug' where for many daily reports there was blank information for the cord counts and cord uv time. I was not able
 * to reproduce why.
 *
 * These were added to the sql query to check that the report is 'filled in':
 *    AND eq_cord_ct_yellow   <> ''
 *  AND eq_cord_ct_blue     <> ''
 *  AND eq_cord_ct_red      <> ''
 *  AND eq_cord_ct_black    <> ''
 *  AND eq_cord_uvt_yellow  <> ''
 *  AND eq_cord_uvt_blue    <> ''
 *  AND eq_cord_uvt_red     <> ''
 *  AND eq_cord_uvt_black   <> ''
 *
 * The design of the cord logs system is very odd. Why is cord data stored in the daily_reports table?
 * Why is the data stored as TEXT? instead of INT?
 * I have warned management via an email that the data should be verified against paper hard copies printed out each day
 * and the people using this system should be made aware that the values displayed may not be correct.
 *
 * @param $cid2 part 2 of the cord id
 * @param $cid3 part 3 of the cord id
 * @return array|resource information from the daily report
 */
function getLatestCordInfo($cid2, $cid3)
{
    $colors = array(
        'SL' => 'yellow',
        'L' => 'red',
        'H' => 'blue',
        'SH' => 'black'
    );

    $cord_id_field = "eq_cord_cc_%1\$s_num";
    $uvt_field = "eq_cord_uvt_%1\$s";
    $ct_field = "eq_cord_ct_%1\$s";
    $low_field = "eq_cord_tw_%1\$s_low";
    $high_field = "eq_cord_tw_%1\$s";

    $cord_info = array();
    $site_id = CURRENT_SITE_ID;
    foreach ($colors as $key => $c) {
        if ($key != $cid2) continue;
        $sql = sprintf(
            "SELECT $cord_id_field as cid, $uvt_field as uvt_total, $ct_field as total, `date`, $low_field as wr_low, $high_field as wr_high
				FROM daily_reports
				WHERE
				site_id = '$site_id'
				and $cord_id_field = '$cid3' and
				`date` <= '" . date("Y-m-d") . "'
				AND `date` >= '" . date("Y-01-01") . "'
				AND eq_cord_ct_yellow   <> ''
				AND eq_cord_ct_blue     <> ''
				AND eq_cord_ct_red      <> ''
				-- AND eq_cord_ct_black    <> ''  //changes for Gifu, They don't have blackCord
				AND eq_cord_uvt_yellow  <> ''
				AND eq_cord_uvt_blue    <> ''
				AND eq_cord_uvt_red     <> ''
				-- AND eq_cord_uvt_black   <> ''  //changes for Gifu, They don't have blackCord
				ORDER BY `date` DESC
				LIMIT 1;",
            $c);
        

        $res = mysql_query($sql) or die(mysql_error());
        if (mysql_num_rows($res) > 0) {
            $cord_info = mysql_fetch_assoc($res);
        };
    };

    $res = array(
        'result' => empty($cord_info) ? 'failed' : 'success',
        'data' => $cord_info
    );
    return $res;
}

/**
 * @param $cid2
 * @param $cid3
 * @return array|resource
 */
function getCordInfo($cid2, $cid3)
{
    $colors = array(
        'SL' => 'yellow',
        'L' => 'red',
        'H' => 'blue',
        'SH' => 'black'
    );
    $cord_id_field = "eq_cord_cc_%1\$s_num";
    $uvt_field = "eq_cord_uvt_%1\$s";
    $ct_field = "eq_cord_ct_%1\$s";
    $low_field = "eq_cord_tw_%1\$s_low";
    $high_field = "eq_cord_tw_%1\$s";
    $cord_info = array();
    $site_id = CURRENT_SITE_ID;
    foreach ($colors as $key => $c) {
        if ($key != $cid2) continue;
        $sql = sprintf(
            "SELECT $cord_id_field as cid, $uvt_field as uvt_total, $ct_field as total, `date`, $low_field as wr_low, $high_field as wr_high
				FROM daily_reports 
				WHERE 
				site_id = '$site_id'
				and $cord_id_field = '$cid3' and 
				`date` <= '" . date("Y-m-d") . "'
				and `date` >= '" . date("Y-01-01") . "'
				ORDER BY `date` DESC
				LIMIT 1;",
            $c);

        //echo $sql."<br><br><br>";
        $res = mysql_query($sql) or die(mysql_error());
        if (mysql_num_rows($res) > 0) {
            $cord_info = mysql_fetch_assoc($res);
        };
    };

    $res = array(
        'result' => empty($cord_info) ? 'failed' : 'success',
        'data' => $cord_info
    );
    return $res;
}

function send_backup_email($booking, $from_reception = false)
{
    $domain = new Domain();
    $subdomain = $domain->getSubdomain();

    if ($from_reception) {
        $booking['CustomerEmail'] = 'Reception';
        $subject = "Booking created by Reception";
        $body = "Booking Confirmed: Reception Booking Backup email\r\n";
        // check if it is new booking with agent = Other
        if ($booking['Agent'] == 'OTHER') {
            //OLD subdomain code
            //$subdomain = 'Unknown';
            //if (preg_match("/([^\.]*)\.bungyjapan\.com/", $_SERVER['HTTP_HOST'], $m)) {
            //$subdomain = $m[1];
            //};
            // send email to Dave
            mail(
                'dave@bungyjapan.com',
                'Notification of "Other" Agent',
                "Site: " . ucfirst($subdomain) . "\r\n" .
                "Date: " . $booking['BookingDate'] . "\r\n" .
                "Time: " . $booking['BookingTime'] . "\r\n" .
                "Notes: " . $booking['Notes'],
                "Return-Path: backup@bungyjapan.com\r\n",
                "-f backup@bungyjapan.com"
            );
        };
        // EOF check
    } else {
        $subject = "Booking confirmed by customer";
        $body = "Booking Confirmed: Online Booking Backup email\r\n";
    };
    $places = getPlaces();
    $place = 'Unknown';


    //OLD Subdomain code
    //if (preg_match("/([^\.]*)\.bungyjapan\.com/", $_SERVER['HTTP_HOST'], $m)) {

    if ($subdomain) {
        $domains = array(
            //TODO It would really be a good idea if we queried 'places' in the database to get this dynamically
            'minakami' => 'MK',
            'sarugakyo' => 'SG',
            'ibaraki' => 'IB',
            'itsuki' => 'IK',
            'naruto' => 'NT',
            'nakijin' => 'NK',
            'event' => 'EV',
            'test' => 'TEST'
        );
        if (array_key_exists($subdomain, $domains)) {
            $subject = $domains[$subdomain] . ' ' . $subject;//prepend the domain code to the email subject
            //$subject = mb_strtoupper($subdomain)."- $subject";//MINAKAMI - Subject
        };
        array_walk($places, function ($item, $index) use (&$place, $subdomain) {
            if ($item['subdomain'] == $subdomain) $place = $item['name'];
        });
    };


    $to = 'backup@bungyjapan.com';
    $body .= "----------------------------------------------------\r\n";
    $headers = "Return-Path: backup@bungyjapan.com\r\n";
    $body .= "Site: " . $place . "\r\n";
    $body .= "Booking Date: " . $booking['BookingDate'] . "\r\n";
    $body .= "Booking Time: " . $booking['BookingTime'] . "\r\n";
    $body .= "No. of Jumpers: " . $booking['NoOfJump'] . "\r\n";
    if ($from_reception && array_key_exists('SplitJump1', $booking) && $booking['SplitJump1'] > 0) {
        for ($i = 1; $i < 4; $i++) {
            $body .= "Split Time $i: " . $booking['SplitTime' . $i] . "\r\n";
            $body .= "Split Jump $i: " . $booking['SplitJump' . $i] . "\r\n";
        };
    };
    $body .= "Name: " . $booking['RomajiName'] . "\r\n";
    $body .= "Contact No.: " . $booking['ContactNo'] . "\r\n";
    if ($from_reception) {
        $body .= "Notes: " . $booking['Notes'] . "\r\n";
    } else {
        $body .= "Email: " . $booking['CustomerEmail'] . "\r\n";
    };
    $body .= "----------------------------------------------------\r\n";
    if (!$from_reception) {
        $body .= "Saved to DB: " . $booking['status_code'] . "\r\n";
    };

    if (mail($to, $subject, $body, $headers, "-f backup@bungyjapan.com")) {
    } else {
        return "<p>Message delivery failed...</p>";
    }
    return true;
}

function getPlaces()
{
    $sql = "SELECT * FROM sites WHERE hidden = 0;";
    $res = mysql_query($sql) or die("getPlaces:" . mysql_error());
    $result = array();
    while ($row = mysql_fetch_assoc($res)) {
        $result[] = $row;
    };
    return $result;
}

function updateBooking($booking)
{
    if (!is_array($booking[0])) {
        $booking = array($booking);
    };
    foreach ($booking as $key => $b) {
        $data = $b;
        $data['site_id'] = CURRENT_SITE_ID;
        if (isset($b['CustomerRegID'])) {
            // update_record
            unset($data['CustomerRegID']);
            if ($key > 0) {
                $data['GroupBooking'] = $groupBooking;//this variable is undefined on the 0th loop!
                //then assigned later once for subsequent loops
            } elseif (sizeof($booking) > 1) {
                $data['GroupBooking'] = 1;
            } else {
                $data['GroupBooking'] = 0;
            };
            db_perform('customerregs1', $data, 'update', 'CustomerRegID=' . $b['CustomerRegID']);
            // if it is first record, set groupBooking ID
            if ($key == 0 && sizeof($booking) > 1) {
                $groupBooking = $b['CustomerRegID'];
            };
        } else {
            // insert_record
            $data["BookingReceived"] = date("Y-m-d h:i:a");
            if ($key > 0) {
                $data['GroupBooking'] = $groupBooking;
            };
            db_perform('customerregs1', $data);
            // if groupBooking set chield groupBooking ID value
            if (sizeof($booking) > 1 && $key == 0) {
                $groupBooking = mysql_insert_id();
            };
        };
    }

}

function getBookingInfo($regid)
{
    if (!$regid) return null;//if regid = zero or '', the query returns all bookings!
    $sql = "SELECT * FROM customerregs1
		WHERE
		(CustomerRegID = '$regid'
				OR GroupBooking = '$regid')
		ORDER BY GroupBooking ASC, CustomerRegID ASC;";
    $res = mysql_query($sql) or die("getBookingInfo:" . mysql_error());

    if (mysql_num_rows($res) == 0) {
        return false;
    };
    $booking = array();
    while ($row = mysql_fetch_assoc($res)) {
        $booking[] = $row;
    };
    return $booking;
}

/*
//specifying site_id in the query is not necessary
function getBookingInfo($regid) {
    $sql = "SELECT * FROM customerregs1
		WHERE 
		site_id = " . CURRENT_SITE_ID . "
		AND (CustomerRegID = '$regid'
				OR GroupBooking = '$regid')
		ORDER BY GroupBooking ASC, CustomerRegID ASC;";
    $res = mysql_query($sql) or die("getBookingInfo:" . mysql_error());

    if (mysql_num_rows($res) == 0) {
        return false;
    };
    $booking = array();
    while ($row = mysql_fetch_assoc($res)) {
        $booking[] = $row;
    };
    return $booking;
}
*/

function getCalendarState($site_id = null)
{
    if (is_null($site_id)) {
        if (defined("CURRENT_SITE_ID") && !is_null(CURRENT_SITE_ID)) {
            $site_id = CURRENT_SITE_ID;
        };
    };
    $sql = "select BOOK_DATE from calendar_state where site_id = '$site_id' AND USER_VIEW_STATUS = '1' AND OP_VIEW_STATUS = '1' ORDER BY BOOK_DATE";
    $result = mysql_query($sql) or die(mysql_errno() . ":<b> " . mysql_error() . "</b>");

    $bDates = array();
    while ($row = mysql_fetch_array($result)) {
        $bDates[] = $row['BOOK_DATE'];
    }
    return $bDates;

}

function getFullDates()
{
    /*
     This will get each timeslot whether empty or not and the number of jumps in it
    What causes problems is that when we start filtering for anything on customerregs , the empty slots disappear
    so if we add cr.deleteStatus = 0, the 0 slots disappear...
    http://stackoverflow.com/questions/18750464/sql-left-join-losing-rows-after-filtering this has the answer
    I could also perfom another join on the time_state table
     SELECT count(cr.`CustomerRegID`) AS bookingCount, BOOK_DATE, BOOK_TIME FROM Time_State ts
	LEFT JOIN customerregs1 cr ON (ts.BOOK_DATE = cr.BookingDate AND ts.`BOOK_TIME` = cr.BookingTime)
	WHERE ts.site_id = 1 AND ts.BOOK_DATE > '2015-06-01'
	GROUP BY BOOK_DATE, BOOK_TIME
	ORDER BY BOOK_DATE, BOOK_TIME;

    fixed version... must see if this is the same as the slow original version by sergey. Moving conditions in to the ON
    prevents the rows from being deleted but moving them all there makes the query very slow
SELECT count(cr.`CustomerRegID`) AS bookingCount, BOOK_DATE, BOOK_TIME FROM Time_State ts
	LEFT JOIN customerregs1 cr ON (ts.BOOK_DATE = cr.BookingDate AND ts.`BOOK_TIME` = cr.BookingTime AND cr.deleteStatus = 0 AND cr.NoOfJump != 0)
	WHERE ts.site_id = 1 AND ts.BOOK_DATE > '2015-06-01' AND ts.BOOK_DATE < '2015-06-30'
	GROUP BY BOOK_DATE, BOOK_TIME
	ORDER BY BOOK_DATE, BOOK_TIME;

    This looks like a working query but the booking counts in the inner query look high, runs in under two seconds but we calculate teh number of free slots
    SELECT bookingCount, BOOK_DATE, BOOK_TIME, SUM(IF((6 - bookingCount) > 0, 1, 0)) AS free FROM (
	SELECT count(cr.`CustomerRegID`) AS bookingCount, BOOK_DATE, BOOK_TIME FROM Time_State ts
	LEFT JOIN customerregs1 cr ON (ts.BOOK_DATE = cr.BookingDate AND ts.`BOOK_TIME` = cr.BookingTime)
	WHERE ts.site_id = 1 AND ts.BOOK_DATE BETWEEN '2015-05-01' AND '2015-05-30' AND cr.deleteStatus = 0
	GROUP BY BOOK_DATE, BOOK_TIME
	ORDER BY BOOK_DATE, BOOK_TIME
	) AS slots

    SELECT bookingCount, BOOK_DATE, BOOK_TIME, SUM(IF((6 - bookingCount) > 0, 1, 0)) AS free FROM (
	SELECT count(cr.`CustomerRegID`) AS bookingCount, BOOK_DATE, BOOK_TIME FROM Time_State ts
	LEFT JOIN customerregs1 cr ON (ts.BOOK_DATE = cr.BookingDate AND ts.`BOOK_TIME` = cr.BookingTime AND cr.deleteStatus = 0 AND cr.NoOfJump > 0)
	WHERE ts.site_id = 1 AND ts.BOOK_DATE BETWEEN '2015-05-01' AND '2015-05-30'
	GROUP BY BOOK_DATE, BOOK_TIME
	ORDER BY BOOK_DATE, BOOK_TIME
	) AS slots

SELECT BOOK_DATE, BOOK_TIME, RomajiName, CustomerRegId FROM Time_State ts
	LEFT JOIN customerregs1 cr ON (ts.BOOK_DATE = cr.BookingDate AND ts.`BOOK_TIME` = cr.BookingTime)
	WHERE ts.site_id = 1 AND ts.BOOK_DATE > '2015-06-01'
	ORDER BY BOOK_DATE, BOOK_TIME;

    Working query?
This seems to work! but we get missing days as for some days the timestate is not set if the site is closed so there is no timestate at all
SELECT BOOK_DATE, SUM(IF((6 - bookingCount) > 0, 1, 0)) AS open_slots FROM (
	SELECT SUM(cr.NoOfJump) AS bookingCount, BOOK_DATE, BOOK_TIME FROM Time_State ts
	LEFT JOIN customerregs1 cr ON (ts.BOOK_DATE = cr.BookingDate AND ts.`BOOK_TIME` = cr.BookingTime AND cr.deleteStatus = 0 AND cr.site_id = 1 )
	WHERE ts.site_id = 1 AND ts.BOOK_DATE BETWEEN '2015-05-01' AND '2015-05-30' AND ts.`OP_VIEW_STATUS` = 0
	GROUP BY BOOK_DATE, BOOK_TIME
	ORDER BY BOOK_DATE, BOOK_TIME
	) AS slots
WHERE bookingCount > 0 GROUP BY BOOK_DATE;

SELECT * FROM Time_State WHERE BOOK_DATE = '2015-05-03' AND site_id = 1 ORDER BY `BOOK_TIME`;
SELECT * FROM customerregs1 WHERE BookingDate = '2015-05-03' AND site_id = 1 AND BookingTime = '08:30 AM';
    This day is meant to be full using sergeys query but my query shows the 8:30 slots as being not full is my query more accurate
     */

    global $config;
    $site_id = CURRENT_SITE_ID;
    $today = date('Y-m-d');
//The current query (not in comments runs too slowly)
// in comments is an attempt at optimizing it, I added a from date as we don't need the older dates and
// This has the query run at around 10 seconds locally, but runs slow remotely (Is memory the reason?)
    //The last left join is the slow part, the other parts run in milli seconds
//    $sql = "
//SELECT book_date FROM (
//	SELECT book_date, sum(IF(avail > 0, avail, 0) * open) AS slots_avail
//	FROM (
//		SELECT t1.`BOOK_DATE` AS book_date,
//		t1.bookingTime,
//		IFNULL({$config['online_slots']}-sum(cr1.NoOfJump), {$config['online_slots']}) AS avail,
//		IF(t1.open IS NULL, '1', '0') AS open
//		FROM (
//			SELECT t2.*, ts.TID AS open /**/
//			FROM (/*Gives all of the dates that are open and possible times  */
//				SELECT cs.BOOK_DATE, tm.bookingTime FROM calendar_state cs, TimeMaster tm
//				WHERE cs.site_id = '$site_id'
//				AND cs.BOOK_DATE >= '$today'
//				AND USER_VIEW_STATUS <> 1
//				AND OP_VIEW_STATUS <> 1
//			 ) AS t2
//			 LEFT JOIN Time_State ts
//			 ON (/*Gets the appends the ID of the open timeslot*/
//				ts.site_id = '$site_id'
//				AND t2.BOOK_DATE >= '$today'
//				AND t2.BOOK_DATE = ts.BOOK_DATE
//				AND t2.bookingTime = ts.BOOK_TIME
//				AND ts.OP_VIEW_STATUS = 1
//			  )
//		) AS t1
//		LEFT JOIN customerregs1 AS cr1
//			ON (
//				cr1.site_id = '$site_id'
//				AND cr1.BookingDate >= '$today'
//				AND cr1.deleteStatus = 0
//				AND t1.`BOOK_DATE` = cr1.`BookingDate`
//				AND t1.`bookingTime` = cr1.`BookingTime`
//			) GROUP BY book_date, bookingTime
//		) res_table GROUP BY book_date
//	) last_res_table
//WHERE slots_avail < 1;
//";
    /*
        $sql = "
            SELECT book_date FROM (
                    SELECT book_date, sum(IF(avail > 0, avail, 0) * open) as slots_avail FROM (
                        SELECT
                        t1.`BOOK_DATE` as book_date,
                        t1.bookingTime,
                        IFNULL({$config['online_slots']}-sum(cr1.NoOfJump), {$config['online_slots']}) as avail,
                        IF(t1.open IS NULL, '1', '0') as open
                        FROM (
                            SELECT t2.*, ts.TID as open
                            FROM (
                                SELECT cs.BOOK_DATE, tm.bookingTime FROM calendar_state cs, TimeMaster tm
                                WHERE
                                cs.site_id = '$site_id'
                                AND USER_VIEW_STATUS <> 1
                                and OP_VIEW_STATUS <> 1
                                 ) as t2
                            LEFT JOIN Time_State ts
                            on (
                                ts.site_id = '$site_id'
                                AND t2.BOOK_DATE = ts.BOOK_DATE
                                AND t2.bookingTime = ts.BOOK_TIME
                                AND ts.OP_VIEW_STATUS = 1
                               )
                             ) as t1
                        LEFT JOIN customerregs1 as cr1
                        on (
                                cr1.site_id = '$site_id' and
                                cr1.deleteStatus = 0 and
                                t1.`BOOK_DATE` = cr1.`BookingDate`
                                and t1.`bookingTime` = cr1.`BookingTime`

                           )
                        GROUP BY book_date, bookingTime
                        ) res_table GROUP BY book_date) last_res_table
                        WHERE slots_avail < 1;
        ";
        $res = mysql_query($sql) or die(mysql_error());
        $fDates = array();
        while ($row = mysql_fetch_assoc($res)) {
            $fDates[] = $row['book_date'];
        };
        */
    $fDates = array();
    return $fDates;
}

function getBookTimes($date, $regid = 0, $online_booking = false, $site_id = null, $checkIfPast = FALSE)
{
    global $config;
    if (is_null($site_id)) {
        $site_id = CURRENT_SITE_ID;
    };

    //we load config for the site as this function can be called for a site that is not the current site
    $configForSite = BJHelper::getConfiguration($site_id);


    $sql = "SELECT
		t1.`BOOK_DATE` as book_date, 
		t1.bookingTime, 
		IFNULL({$configForSite['online_slots']}-sum(cr1.NoOfJump), {$configForSite['online_slots']}) as avail,
		IF(t1.open IS NULL, '1', '0') as open
			FROM (
					SELECT t2.*, ts.TID as open 
					FROM (
						SELECT cs.BOOK_DATE, tm.bookingTime FROM calendar_state cs, TimeMaster tm
						WHERE 
						cs.site_id = '$site_id'
						AND `BOOK_DATE` = '$date'
						and USER_VIEW_STATUS <> 1
						and OP_VIEW_STATUS <> 1
					     ) as t2
					LEFT JOIN Time_State ts
					on (
						ts.site_id = '$site_id'
						AND t2.BOOK_DATE = ts.BOOK_DATE 
						AND t2.bookingTime = ts.BOOK_TIME 
						AND (ts.OP_VIEW_STATUS = 1
							" . (($online_booking) ? 'OR ts.USER_VIEW_STATUS = 1' : '') . "
						    )
					   )
					/*WHERE ts.TID IS NULL*/
			     ) as t1
			     LEFT JOIN customerregs1 as cr1
			     on (
					     cr1.site_id = '$site_id'
					     AND cr1.deleteStatus = 0 
					     AND t1.`BOOK_DATE` = cr1.`BookingDate` 
					     AND t1.`bookingTime` = cr1.`BookingTime`
					     " . (($regid > 0) ? "and not (cr1.CustomerRegID = $regid or cr1.GroupBooking = $regid)" : "") . "
				)
			     GROUP BY book_date, bookingTime;";

    $res = mysql_query($sql) or die(json_encode(array('result' => 'error:' . mysql_error())));
    if (mysql_num_rows($res) > 0) {
        $result['result'] = 'success';
        while ($row = mysql_fetch_assoc($res)) {
            $result['options'][] = array(
                'value' => $row['bookingTime'],
                'text' => $row['bookingTime'] . " ({$row['avail']})",
                'avail' => $row['avail'],
                'open' => checkIfBookingInPast($row, $checkIfPast)
            );
        }
    } else {
        $result = array(
            'result' => 'success',
            'options' => array(
                array(
                    'value' => '',
                    'text' => 'No Time'
                )
            )
        );
    };
    return $result;
}

function checkIfBookingInPast($row, $checkIfPast)
{
    $today = date('Y-m-d');
    $now = date('H:i');
    if ($checkIfPast == TRUE && $row['book_date'] == $today && substr($row['bookingTime'], 0, -3) <= $now) // substr trims the " AM/PM"
        return 0;
    else
        return $row['open'];
}


function isGroupBooking($bid)
{
    $gbid = false;
    $sql = "SELECT GroupBooking FROM customerregs1 WHERE CustomerRegID = " . (int)$bid . ";";
    $result = mysql_query($sql) or die(mysql_errno() . ":<b> " . mysql_error() . "</b>");
    if ($row = mysql_fetch_assoc($result)) {
        if ($row['GroupBooking'] > 0) {
            // if GroupBooking > 1 - this is child record and we need to edit parent one.
            if ($row['GroupBooking'] > 1) {
                $gbid = $row['GroupBooking'];
            } else {
                $gbid = $bid;
            };
        };
    }
    return $gbid;
}

function getTimeState($date)
{
    $site_id = CURRENT_SITE_ID;
    $sql = "
		SELECT t2.*, NOT ts.OP_VIEW_STATUS as open, NOT ts.USER_VIEW_STATUS as online_open
		FROM (
				SELECT cs.BOOK_DATE, tm.bookingTime FROM calendar_state cs, TimeMaster tm
				WHERE 
				cs.site_id = '$site_id'
				and `BOOK_DATE` = '$date'
				/*
				These two lines are disabled so that we may edit days that are off in the calendar
				*/
				/*
				and USER_VIEW_STATUS <> 1
				and OP_VIEW_STATUS <> 1
				*/
		     ) as t2
		LEFT JOIN Time_State ts
		on (
				ts.site_id = '$site_id'
				AND t2.BOOK_DATE = ts.BOOK_DATE
				AND t2.bookingTime = ts.BOOK_TIME
		   )
		ORDER BY bookingTime ASC";        

    $res = mysql_query($sql) or die(json_encode(array('result' => 'error:' . mysql_error())));
    if (mysql_num_rows($res) > 0) {
        $result['result'] = 'success';
        while ($row = mysql_fetch_assoc($res)) {
            $result['options'][] = array(
                'value' => $row['bookingTime'],
                'open' => $row['open'],
                'online_open' => $row['online_open']
            );
        }
    } else {
        $result = array(
            'result' => 'error',
            'options' => array(
                array(
                    'value' => '',
                    'text' => 'No Time'
                )
            )
        );
    };
    return $result;
}

function setTimeState($date, $times)
{
    $site_id = CURRENT_SITE_ID;

    // byamba - add 2020-09-20 
    $sql_del = " delete from Time_State
    WHERE site_id = '$site_id' AND BOOK_DATE = '" . $date . "'";
    mysql_query($sql_del) or die(mysql_error());
    //end

   
    $sql = "
		SELECT tm.bookingTime, ts.TID, IF(ts.TID IS NULL, 1, 0)  as open
		FROM TimeMaster tm 
		LEFT JOIN Time_State ts
		on (
				'$site_id' = ts.site_id
				AND '$date' = ts.BOOK_DATE
				AND tm.bookingTime = ts.BOOK_TIME
		   )
		ORDER BY bookingTime ASC";
    $res = mysql_query($sql) or die(json_encode(array('result' => 'error:' . mysql_error())));
    $times = json_decode(stripslashes($times));
    if (mysql_num_rows($res) > 0) {
        $result['result'] = 'success';
        while ($row = mysql_fetch_assoc($res)) {
            $current_times[$row['bookingTime']] = $row;
        }
        foreach ($times as $new_time) {
            if (!array_key_exists($new_time->value, $current_times)) {
                $current_times[$new_time->value] = array('TID' => null);
            };
            $current_row = $current_times[$new_time->value];
            $row = array(
                'site_id' => $site_id,
                'BOOK_DATE' => $date,
                'BOOK_TIME' => $new_time->value,
                'OP_VIEW_STATUS' => ($new_time->open) ? 0 : 1,
                'USER_VIEW_STATUS' => ($new_time->online_open) ? 0 : 1,
                'OPERATION_DESC' => ($new_time->open) ? 'Time ON' : 'Time OFF'
            );
            if ($current_row['TID'] != null && $current_row['TID'] != 'NULL') {
                db_perform('Time_State', $row, 'update', 'TID=' . $current_row['TID']);
            } else {
                db_perform('Time_State', $row);
            };

        };
    } else {
        $result = array(
            'result' => 'error',
            'options' => array(
                array(
                    'value' => '',
                    'text' => 'No Time'
                )
            )
        );
    };
    return $result;
}

function db_perform($table, $data, $action = 'insert', $parameters = '', $conn = null)
{
    reset($data);
    if ($action == 'insert') {
        $query = 'INSERT INTO ' . $table . ' (';
        while (list($columns,) = each($data)) {
            $query .= '`' . $columns . '`' . ', ';
        }
        $query = substr($query, 0, -2) . ') values (';
        reset($data);
        while (list(, $value) = each($data)) {
            switch ((string)$value) {
                case 'now()':
                    $query .= 'now(), ';
                    break;
                case 'null':
                    $query .= 'null, ';
                    break;
                default:
                    $query .= '\'' . db_input($value) . '\', ';
                    break;
            }
        }

        $query = substr($query, 0, -2) . ')';

    } elseif ($action == 'update') {
        $query = 'update ' . $table . ' set ';
        while (list($columns, $value) = each($data)) {
            switch ((string)$value) {
                case 'now()':
                    $query .= '`' . $columns . '` = now(), ';
                    break;
                case 'null':
                    $query .= '`' . $columns .= '` = null, ';
                    break;
                default:
                    $query .= '`' . $columns . '` = \'' . db_input($value) . '\', ';
                    break;
            }
        }
        $query = substr($query, 0, -2) . ' where ' . $parameters;
    }
    
    if (!is_null($conn)) {
        return mysql_query($query, $conn);
    } else {
        return mysql_query($query);
    }
}

function i_db_perform($table, $data, $action = 'insert', $parameters = '', $conn = null)
{
    global $conn_i;

    reset($data);
    if ($action == 'insert') {
        $query = 'INSERT INTO ' . $table . ' (';
        while (list($columns,) = each($data)) {
            $query .= '`' . $columns . '`' . ', ';
        }
        $query = substr($query, 0, -2) . ') values (';
        reset($data);
        while (list(, $value) = each($data)) {
            switch ((string)$value) {
                case 'now()':
                    $query .= 'now(), ';
                    break;
                case 'null':
                    $query .= 'null, ';
                    break;
                default:
                    $query .= '\'' . db_input($value) . '\', ';
                    break;
            }
        }

        $query = substr($query, 0, -2) . ')';

    } elseif ($action == 'update') {
        $query = 'update ' . $table . ' set ';
        while (list($columns, $value) = each($data)) {
            switch ((string)$value) {
                case 'now()':
                    $query .= '`' . $columns . '` = now(), ';
                    break;
                case 'null':
                    $query .= '`' . $columns .= '` = null, ';
                    break;
                default:
                    $query .= '`' . $columns . '` = \'' . db_input($value) . '\', ';
                    break;
            }
        }
        $query = substr($query, 0, -2) . ' where ' . $parameters;
    }

    if (!is_null($conn_i)) {
        return mysqli_query($conn_i, $query);
    } else {
        return mysqli_query($conn_i, $query);
    }
}

function i_fetch_assoc($result)
{
    $row = mysqli_fetch_assoc($result);
    return $row;
}

function i_free($result)
{
    mysqli_free_result($result);
}

function db_input($string)
{

    $string = stripslashes($string);
    if (function_exists('mysql_real_escape_string')) {
        return mysql_real_escape_string($string);
    } elseif (function_exists('mysql_escape_string')) {
        return mysql_escape_string($string);
    }

    return addslashes($string);
}

function i_db_input($string)
{

    global $conn_i;

    $string = stripslashes($string);
    if (function_exists('mysqli_real_escape_string')) {
        return mysqli_real_escape_string($conn_i, $string);
    } elseif (function_exists('mysqli_escape_string')) {
        return mysqli_escape_string($conn_i, $string);
    }

    return addslashes($string);
}

function i_query($query)
{
    global $conn_i;

    $result = mysqli_query($conn_i, $query);
    return $result;
}

function i_error()
{
    global $conn_i;
    return mysqli_error($conn_i);
}

function logDebugData($data)
{//used in debugging the Reception blank check in. To be deleted
    $debugOutputFile = $_SERVER['DOCUMENT_ROOT'] . "/Reception/blankCheckinBug.log";

    $handle = fopen($debugOutputFile, 'a');

    if ($handle) {
        if (is_array($data)) {
            //write print_r($data);
            $dataArrayAsString = print_r($data, true);
            $dataArrayAsString .= "\n\n";

            fwrite($handle, $dataArrayAsString);
        } else {
            fwrite($handle, $data . "\n\n");
        }
    }
}

function logDataToFile($data, $file)
{
    $debugOutputFile = $_SERVER['DOCUMENT_ROOT'] . $file;

    $handle = fopen($debugOutputFile, 'a');

    if ($handle) {
        if (is_array($data)) {
            //write print_r($data);
            $dataArrayAsString = print_r($data, true);
            $dataArrayAsString .= "\n\n";

            fwrite($handle, $dataArrayAsString);
        } else {
            fwrite($handle, $data . "\n\n");
        }
    }
}

//returns an array of query results so you don't have to mess with results and fetch assoc
function queryForRows($query)
{
    $results = mysql_query($query);
    if ($results == false) echo mysql_error();
    while (($rows[] = mysql_fetch_assoc($results)) || array_pop($rows)) ;//get all of the rows of the result
    mysql_free_result($results);
    return $rows;
}

function i_queryForRows($query)
{
    global $conn_i;
    $results = mysqli_query($conn_i, $query);
    if ($results == false) echo mysqli_error($conn_i);

    while (($rows[] = mysqli_fetch_assoc($results)) || array_pop($rows));//get all of the rows of the result

    mysqli_free_result($results);

    if(count($rows)) {
        return $rows;
    } else {
        return null; //return null for no results
    }
}

function i_queryForValue($query, $value)
{
    $rows = i_queryForRows($query);

    $results = null;

    if($rows !== null) {
        $results = $rows[0][$value];
    }

    return $results;
}

function i_inserted_id()
{
   global $conn_i;

   return mysqli_insert_id($conn_i);
}


function pr($printString)
{
    echo "<pre>";
    print_r($printString);
    echo "</pre>";
}

function isSecure()
{
    return (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off') || $_SERVER['SERVER_PORT'] == 443;
}

function getTransferProtocol()
{
    if (isSecure()) return "https://";
    else return "http://";
}

function auth_user()
{
    global $user, $lang, $section;
    // not logged in user
    //$siteId = (SYSTEM_SUBDOMAIN_REAL == 'AGENT') ? '*' : CURRENT_SITE_ID;
    //$hasPermission = $user->hasPermission($section, $siteId);

    if (!isset($_POST['auth_user'])) {
        include(dirname(__FILE__) . '/templates/user_auth.php');
        exit;
        /*} else if(!$hasPermission) {
            access_denied();*/
    } else {
        // check user login/pass
        if ($user->authUser($_POST['auth_user'], $_POST['auth_pass'])) {

            $_SESSION['myusername'] = $user->getUserName();
            $_SESSION['uid'] = $user->getID();
            unset($_GET['logout']);
            $query = array();

            foreach ($_GET as $k => $v) {
                $query[] = $k . '=' . $v;
            };

            if (SYSTEM_SUBDOMAIN_REAL != 'AGENT' && $user->hasRole('Agent')) {
                Header("Location: http://agent." . APP_DOMAIN);
                die();
            };

            Header('Location: ' . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH) . '?' . implode('&', $query));
        } else {
            $_SESSION['login_message'] = "Invalid username or password.";//Send out an email for failed logins

            $country = new IpLocation();

            $headers = "Return-Path: noreply@bungyjapan.com\r\n";
            $headers .= "Reply-To: noreply@bungyjapan.com\r\n";
            $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
            $headers .= "MIME-Version: 1.0\r\n";

            $username = isset($_POST['auth_user']) ? $_POST['auth_user'] : null;
            $password = isset($_POST['auth_pass']) ? $_POST['auth_pass'] : null;

            $loginDateTime = new DateTime();
            $message = "An unauthorised login attempt occurred at: {$loginDateTime->format('Y-m-d H:i:s')} from '{$country->getCountryName()}' with IP Address: {$country->getIPAddress()}. ";
            if ($username != null)
                $message .= "The user attempted to login with Username: '$username' and password: '$password' but was successfully blocked.";

            $message .= "<br><pre>" . print_r($_SERVER, true) . "</pre>";

            mail("developer@standardmove.com", "Failed Login Attempt from IP: {$country->getIPAddress()} Country: '{$country->getCountryName()}''", $message, $headers, '-f noreply@bungyjapan.com');
            //mail("developer@standardmove.com", "International Access Attempt from Country '{$country->getCountryName()}''", $message, $headers, '-f noreply@bungyjapan.com');

            //header("Location: /?logout=true");

            Header('Location: ' . $_SERVER['REQUEST_URI']);
        };
    }
}

/*
 * When index.php loads, an Ajax call then runs head if the page exists in the help table (see head_scripts.php)
 * and if so, appends a [?] help button to the page's back button. This file also calls Help/index.php to see alter
 * the 'back_exists' flag in the database. If the user does not have access to Help/index.php access_denied() is called.
 *
 * Now that we are checking for suspicious access, this would send out an email every time the page is loaded.
 * In user.php hasPermission() we have made accessing Help always return true so this does not happen.
 */

function access_denied()
{//send out an email attempting to access pages outside of what is permitted
    global $user, $lang;

    $country = new IpLocation();

    $headers = "Return-Path: noreply@bungyjapan.com\r\n";
    $headers .= "Reply-To: noreply@bungyjapan.com\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";

    $username = $_SESSION['myusername'];
    $page = $_SERVER['REQUEST_URI'];

    $loginDateTime = new DateTime();
    $message = "User: '$username' attempted to access $page at: {$loginDateTime->format('Y-m-d H:i:s')} from '{$country->getCountryName()}' with IP Address: {$country->getIPAddress()}. They were denied access due to insufficient permissions.";
    $message .= "<br><pre>" . print_r($_SERVER, true) . "</pre>";

    mail("developer@standardmove.com", "Failed Page Access Attempt From: '{$country->getCountryName()}''", $message, $headers, '-f noreply@bungyjapan.com');

    $_SESSION['login_message'] = "You are not allowed to access this page";

    include(dirname(__FILE__) . '/templates/user_auth.php');
    die();
}

//TODO: Move this to a page helper
//the current help button code comes from includes/head_scripts and uses javascript to append a help button to
//the back button (has id="back"). I really do not like this as it is difficult to debug and unnecessary complexity.
function helpButton()
{
    $uri = parse_url($_SERVER['REQUEST_URI']);
    $c = count(explode('/', $uri['path']));
    $uri['path'] = str_replace('index.php', '', $uri['path']);

    $helpSql = "SELECT * FROM help WHERE url = '{$uri['path']}' AND active = 1;";
    $helpResults = mysql_query($helpSql);
    while (($help[] = mysql_fetch_assoc($helpResults)) || array_pop($help)) ;

    $helpId = $help[0]['id'];
    return "<button type='button' rel='$helpId' class='user-help'>?</button>";
}

/**
 * Tests if a variable is set and if not, will either assign a value to it or return a false value of the type specified
 * in $type.
 *
 * In cases like $_GET['date'] we this can be used to check if it is set, then assign a date to it or set it to null
 * $date = ifIsSet($_GET['date'], date());//if $_GET['date'] is not set, return date()
 * $date = ifIsSet($_GET['date'], null, 'string');//if $_GET['date'] is not set, return '';
 * @param      $variable the variable being checked
 * @param null $value a value to assign to the variable if it is not set
 * @param null $type the type of non set value to return
 *
 * @return null|string
 */
function ifIsSet($variable, $value = null, $type = null)
{
    if (!isset($variable)) {
        if ($value == null) { //return an empty/false value of the type specified
            switch ($type) {
                case "string":
                    $variable = "";
                    break;
                case "boolean":
                    $variable = false;
                    break;
                default://if type is not set just return null
                    $variable = null;
                    break;
            }
        } else { //Assign the value specified to the return
            $variable = $value;
        }
    }

    return $variable;
}

function formatNumber($number)
{
    $numberParts = explode(".", $number);
    if (count($numberParts) == 1) {//The number does not have a decimal point
        return number_format($number);

    } else if (count($numberParts) == 2) {
        $afterDecimalPoint = $numberParts[0];
        $beforeDecimalPoint = $numberParts[1];
        return number_format($afterDecimalPoint) . "." . $beforeDecimalPoint;
    } else {
        throw new Exception ("Invalied Number Supplied to formatNumber()");
    }
}

function unformatNumber($number)
{
    return str_replace(",", "", $number);
}

function siteNameToId($name)
{
    $name = strtolower($name);
    $name = mysql_real_escape_string($name);

    $query = "SELECT * FROM sites WHERE subdomain = '$name'";
    $results = queryForRows($query);

    if (count($results)) {
        return $results[0]['id'];
    } else return null;
}

function idToSiteName($id)
{
    $id = (int)$id;

    $query = "SELECT * FROM sites WHERE id = '$id'";
    $results = queryForRows($query);

    if (count($results)) {
        return $results[0]['display_name'];
    } else return null;

}


/**
 * Get the price of the first jump site
 *
 * the price is normally just set in $config['first_jump_rate'] However, we need an automatic
 * change over to happen so that bookings made with a jumpdate after April 1st 2017 will have
 * a new price.
 *
 * In the database, the new price is set and the oldrates are applied from the array below until
 * April 1st
 *
 * @param $site_id
 * @param $date
 *
 * @return mixed
 */
function getFirstJumpRate($site_id, $date)
{
    global $config;

    $rate = $config['first_jump_rate'];//this will contain the new price
    $dateBooking = DateTime::createFromFormat('Y-m-d H:i:s', $date . " 00:00:00");
    $dateSwitchOver = DateTime::createFromFormat('Y-m-d H:i:s', "2017-04-01 00:00:00");

    //Apply Old Rates Until April 1st 2017
    if ($dateBooking < $dateSwitchOver) {

        $oldRates = [
            7500,//0
            8000,//1 MK
            10000,//2
            15000,//3
            6000,//4
            0,//5
            7500,//6
            1000,//7
            12000,//8
            9000,//9
            9000,//10
            8000,//11
            9000,//12
        ];

        $rate = $oldRates[$site_id];
    }

    return $rate;
}

function getScmData($site_id)
{
    global $config;
    $sql = "SELECT * FROM dashboardSCM WHERE site_id = '$site_id' ORDER BY id ASC;";
    $res = mysql_query($sql) or die(mysql_error());
    $titles = array();
    while ($row = mysql_fetch_assoc($res)) {
        $titles[] = array(
            'id' => $row['id'],
            'site_id' => $row['site_id'],
            'scm' => $row['scm'],
            'status' => $row['status'],
            'scm_reason' => $row['scm_reason'],
            'condition' => $row['condition'],
            'up_time' => $row['up_time'],
            'number_of_restarts' => $row['number_of_restarts'],
            'description' => $row['description']

        );
    };
    return $titles;
}

function updateScmData($scm, $site_id, $status, $reason, $condition, $desc)
{
    global $config;
    $sql = "UPDATE dashboardSCM SET `status`='$status', `scm_reason`='$reason', `condition`='$condition',  `description`='$desc' where `site_id`='$site_id' AND `scm` LIKE '$scm'";
    $res = mysql_query($sql) or die(mysql_error());
    return $res;
}

