<?php
date_default_timezone_set("Asia/Tokyo");
setlocale(LC_ALL, "ja_JP.sjis");

if (preg_match('/\.test/i', $_SERVER['HTTP_HOST'])) {//if we are on .dev, display all errors
    //error_reporting(0);
    error_reporting(E_ALL & ~E_DEPRECATED & ~E_NOTICE);
    error_reporting(E_ALL);
    //error_reporting(E_ALL);
    //ini_set('display_errors', 'On');
    ini_set('display_errors', 'Off');
    define("ENVIRONMENT", "development");

} else {//if we are on .com, hide all errors
    error_reporting(E_ALL);
    ini_set('display_errors', 'Off');
    ini_set('log_errors', 'On');
    define("ENVIRONMENT", "server");
}


//Set the PHP autoloader
spl_autoload_register(function ($class) {
    $filename = preg_replace('/^(bj)/i', '', strtolower($class));
    $filename = dirname(__FILE__) . '/classes/' . $filename . '.php';
    if (file_exists($filename) && is_readable($filename)) {
        require_once $filename;
    };
});
//start session after autoloading classes as deserialization will cause
//__PHP_Incomplete_Class_Name warning

session_set_cookie_params(0);
session_start();
if (isset($_GET['logout'])) {
    session_destroy();
    session_start();
    //Header("Location: " . parse_url($_SERVER['REQUEST_URI'], PHP_URL_PATH));
    //die();
};

//set the include path
$doc_root = dirname(__DIR__).'/';
set_include_path(get_include_path() . PATH_SEPARATOR . $doc_root);

$currentDirectory = dirname(dirname( __DIR__ ));
define('APPLICATION_DIRECTORY', $currentDirectory);//
define('CONFIGURATION_PATH', $currentDirectory . "/conf.ini");

//these objects are required by defineConstants.php
$objDatabaseConnection = new Connection();
$conn = $objDatabaseConnection->databaseConnectionProcess();

$objDatabaseConnection = new ConnectionMysqli();
$conn_i= $objDatabaseConnection->databaseConnectionProcess();

$domainObject = new Domain();

//I will have to add an exception for these in the domain object
//if(!$domainObject->existsInDatabase()) die('This subdomain is currently undefined in the database'); //the site does not exist so kill startup but this kills agents...
$user = new BJUser();
$country = new IpLocation();//required before calling user_auth.php
new accessLog($country);//save the users access

//include required files (some of these need a rename)

//parse the configuration file again
//define the country as a constant

require_once __DIR__ . '/defineConstants.php';
require_once __DIR__ . '/functions.php';
require_once __DIR__ . '/html_output.php';
require_once __DIR__ . '/autoIpLogin.php';
require_once __DIR__ . '/user_auth.php';

require_once __DIR__ . '/vendor/autoload.php';//Composer Autoloader

/*
//Not needed for now
require_once __DIR__ . '/generated-conf/config.php';//config for propel
*/

$config = BJHelper::getConfiguration(CURRENT_SITE_ID);

if (count($config) == 0 && !is_null(CURRENT_SITE_ID) && CURRENT_SITE_ID != 0) {
    echo("<font color=red>In order to use this site, we need to set configuration variables first. <br />");
    echo "Go to <a href='/Manage/editConfiguration.php'>Manage</a> section to set configuration</font>";
}

echo '';
