<script src="/js/jquery/1.10.2/jquery.min.js"></script>
<script src="/js/jquery/1.10.1/jquery-ui.min.js"></script>
<link rel="stylesheet" href="/js/jquery/1.10.1/jquery-ui.css" type="text/css" media="all"/>
<link rel="stylesheet" href="/css/main_menu.css" type="text/css" media="all"/>
<script>
$(document).ready(function () {
    $('.user-help').click(function () {
        var helpId = $(this).attr('rel');
        window.open('/Help/?id=' + helpId, 'bj_help', 'width=600,height=600,location=no,menubar=no,status=no,toolbar=no,scrollbars=1', false);
    })
});
</script>
<?php
$uri = parse_url($_SERVER['REQUEST_URI']);
$c = count(explode('/', $uri['path']));
$uri['path'] = str_replace('index.php', '', $uri['path']);

if ($c > 1) {
    $sql = "select * from help WHERE url = '{$uri['path']}';";
    if ($res = mysql_query($sql)) {
        if ($row = mysql_fetch_assoc($res)) {
            // if title is not empty, show help button
            if ($row['title'] != '' && $row['active'] == 1) {
                ?>
                <script>
                    $(document).ready(function () {
                        var b = $('<button>').prop('id', 'help').text('?').attr('topic', <?php echo $row['id']; ?>).addClass('button').attr('type', 'button');
                        $('#back').parent().append(b);
                        $('#help').on('click', function () {
                            var helpw = window.open('/Help/?id=' + $(this).attr('topic'), 'bj_help', 'width=600,height=600,location=no,menubar=no,status=no,toolbar=no,scrollbars=1', false);
                        });
                    });
                </script>
            <?php
            };
            // if no help page and back button exists - check it
            if (!$row['back_exists']) {
                ?>
                <script>
                    $(document).ready(function () {
                        var bexists = $('#back').length;
                        $.ajax('/Help/index.php?action=back_exists&back_exists=' + bexists + '&id=' + <?php echo $row['id']; ?>);
                    });
                </script>
            <?php
            };
        } else {
            // if no record in DB - just create this record
            $data = array(
                'url' => $uri['path']
            );
            db_perform('help', $data);
            $row = array();
            $row['id'] = mysql_insert_id();
            ?>
            <script>
                $(document).ready(function () {
                    var bexists = $('#back').length;
                    $.ajax('/Help/index.php?action=back_exists&back_exists=' + bexists + '&id=' + <?php echo $row['id']; ?>);
                });
            </script>
        <?php
        }
    };
};
?>
