<?php
// get Sys admin permissions
//require_once("define_constants.php");
//require_once __DIR__ . '/functions.php';
//$user = new BJUser();
$all_perms = $user->getATPermissions('admin');
// parse URLs
$myurl = parse_url($_SERVER['REQUEST_URI']);
$url_parts = explode('/', $myurl['path']);
$file = basename($myurl['path']);
$section = '';
if (count($url_parts) > 1) {
    $section = $url_parts[1];
};

// agent login to main server denied
// EOF Agent denied access to main.bungyjapan.com
// if user trying to access /booking/ folder or any other protected - check for login and permissions
if ((empty($section) && SYSTEM_SUBDOMAIN_REAL != 'MAIN') || in_array($section, $all_perms)) {
    if (array_key_exists('uid', $_SESSION) && !empty($_SESSION['uid']) && !isset($_POST['auth_user'])) {
        //if the site is AGENT
        $siteId = (SYSTEM_SUBDOMAIN_REAL == 'AGENT') ? '*' : CURRENT_SITE_ID;
        $hasPermission = $user->hasPermission($section, $siteId);
		$userName = $user->getUserName();

        if(($section == 'Inventory') && (in_array($userName, ['Hiroshi', 'Yu', 'noriko', 'sugai', 'Sachiko']))) $hasPermission = 1;//let Yu access inventory even as a staff user
        //if(($section == 'Analysis') && (in_array($userName, ['sugai']))) $hasPermission = 1;//let sugai access analysis even as a staff user
        //if(($section == 'Bookkeeping') && (in_array($userName, ['sugai']))) $hasPermission = 1;//let sugai access analysis even as a staff user
        if(($section == 'Signage') && (in_array($userName, ['Takumi']))) $hasPermission = 1;//let Takumi access signage even as a staff user
        if(($section == 'Training') && (in_array($userName, ['DavidScott']))) $hasPermission = 1;//let David access Training even as a staff user
        //if(($section == 'Training') && (in_array($userName, ['Phil']))) $hasPermission = 1;//let Phil access Training even as a staff user


      
        //we need to check if we can access a particular page on a particular site
        if (empty($section) || $hasPermission) {
            // all OK there
            // user is logged in and have access to this folder/file
            if (SYSTEM_SUBDOMAIN_REAL != 'AGENT' && $user->hasRole('Agent')) {//only let agents access the agents subdomain
                access_denied();
            };
        } else {
            access_denied();
        };
    } else {
        auth_user();
    };
} else {
    //echo "unknown Directory";
}

//functions moved to functions.php for now
if ($user->getShouldUpdatePassword() && !isset($preventPasswordChangeRedirect)) {
    $_SESSION['password_error']     = "WARNING. Your password is out of date. <br> You must update your password now or you will lose access to the system.";
    $_SESSION['shouldRedirectTo']   = $_SERVER['REQUEST_URI'];
    $url ="http://{$_SERVER['HTTP_HOST']}/User/change_password.php";
    header("Location: ".$url);
}

$allowedInternationalUserList = ['Beau', 'Dez', 'Bungy', 'Lou', 'Mike'];

$allowedInternationalUser = !(
		isset($_SESSION['myusername']) && 
		(
			!in_array($_SESSION['myusername'], $allowedInternationalUserList)
		)
	);

if(!$allowedInternationalUser) {
    $inJapan = $country->getThreeLetterCode() == 'JPN';
    $inSingapore = $country->getThreeLetterCode() == 'SGP';
    $inSingapore = false; //prevent Singapre login for now
    $isLocalhost = $_SERVER['REMOTE_ADDR'] == '127.0.0.1';
    $inAllowedCountry = ($inSingapore || $inJapan || $isLocalhost);

//if we are not in
//check if it is a booking / yoyaku
    $isABooking = preg_match("/^\/(Yoyaku|Booking)/i", $_SERVER['REQUEST_URI']);
    $isAllowedFile = preg_match("/includes\/ajax_helper.php/i", $_SERVER['REQUEST_URI']);//allow access to the ajax_helper file for ajax requests on bookings

    if (!$isAllowedFile && !$inAllowedCountry && !$isABooking && (ENVIRONMENT != "development")) {
        //if we are not in Japan, and not running locally log out and send emails
        //This code is repeated in user_auth.php to give emails of failed domestic logins
        $headers = "Return-Path: noreply@bungyjapan.com\r\n";
        $headers .= "Reply-To: noreply@bungyjapan.com\r\n";
        $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
        $headers .= "MIME-Version: 1.0\r\n";

        $username = isset($_POST['auth_user']) ? $_POST['auth_user'] : null;
        $password = isset($_POST['auth_pass']) ? $_POST['auth_pass'] : null;

        $loginDateTime = new DateTime();
        $message = "An unauthorised login attempt occurred at: {$loginDateTime->format('Y-m-d H:i:s')} from '{$country->getCountryName()}' with IP Address: {$country->getIPAddress()}. ";
        $message .= "The user attempted to login with Username: '$username' and password: '$password' but was blocked due to not being in Japan.";

        $message .= "<br><pre>" . print_r($_SERVER, true) . "</pre>";

        mail("developer@standardmove.com", "Failed Remote Access Attempt from IP {$country->getIPAddress()} the Country of: '{$country->getCountryName()}''", $message, $headers, '-f noreply@bungyjapan.com');

        header("Location: /?logout=true");
    }
}
