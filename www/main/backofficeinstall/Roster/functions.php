<?php

function isWeekend($date)
{
    return $date->format('N') > 5;//if the day is  than Monday it's a weekend
}

function isNationalHoliday($date)
{
    //TODO: Implement a feed parser or have a list of holidays in the database
    //Or add events to the calendar that are national holidays

    //Holidays until 2020 from http://www.timeanddate.com/holidays/japan/2015
    $holidays = [
        "2015-01-01", "2015-01-02", "2015-01-03", "2015-01-12", "2015-02-11", "2015-03-03", "2015-03-20", "2015-03-21",
        "2015-04-29", "2015-05-03", "2015-05-04", "2015-05-05", "2015-05-06", "2015-06-21", "2015-07-07", "2015-07-20",
        "2015-09-21", "2015-09-22", "2015-09-23", "2015-09-23", "2015-10-12", "2015-11-03", "2015-11-15", "2015-11-23",
        "2015-12-22", "2015-12-23", "2015-12-25", "2015-12-31", "2016-01-01", "2016-01-02", "2016-01-03", "2016-01-11",
        "2016-02-11", "2016-03-03", "2016-03-20", "2016-03-20", "2016-03-21", "2016-04-29", "2016-05-03", "2016-05-04",
        "2016-05-05", "2016-06-20", "2016-07-07", "2016-07-18", "2016-08-11", "2016-09-19", "2016-09-22", "2016-09-22",
        "2016-10-10", "2016-11-03", "2016-11-15", "2016-11-23", "2016-12-21", "2016-12-23", "2016-12-25", "2016-12-31",
        "2017-01-01", "2017-01-02", "2017-01-02", "2017-01-03", "2017-01-09", "2017-02-11", "2017-03-03", "2017-03-20",
        "2017-03-20", "2017-04-29", "2017-05-03", "2017-05-04", "2017-05-05", "2017-06-21", "2017-07-07", "2017-07-17",
        "2017-08-11", "2017-09-18", "2017-09-22", "2017-09-22", "2017-10-09", "2017-11-03", "2017-11-15", "2017-11-23",
        "2017-12-21", "2017-12-23", "2017-12-25", "2017-12-31", "2018-01-01", "2018-01-02", "2018-01-03", "2018-01-08",
        "2018-02-11", "2018-02-12", "2018-03-03", "2018-03-20", "2018-03-20", "2018-04-29", "2018-04-30", "2018-05-03",
        "2018-05-04", "2018-05-05", "2018-06-21", "2018-07-07", "2018-07-16", "2018-08-11", "2018-09-17", "2018-09-23",
        "2018-09-23", "2018-09-24", "2018-10-08", "2018-11-03", "2018-11-15", "2018-11-23", "2018-12-21", "2018-12-23",
        "2018-12-24", "2018-12-25", "2018-12-31", "2019-01-01", "2019-01-02", "2019-01-03", "2019-01-14", "2019-02-11",
        "2019-03-03", "2019-03-20", "2019-03-20", "2019-04-29", "2019-05-03", "2019-05-04", "2019-05-05", "2019-05-06",
        "2019-06-21", "2019-07-07", "2019-07-15", "2019-08-11", "2019-08-12", "2019-09-16", "2019-09-23", "2019-09-23",
        "2019-10-14", "2019-11-03", "2019-11-04", "2019-11-15", "2019-11-23", "2019-12-22", "2019-12-23", "2019-12-25",
        "2019-12-31", "2020-01-01", "2020-01-02", "2020-01-03", "2020-01-13", "2020-02-11", "2020-03-03", "2020-03-20",
        "2020-03-20", "2020-04-29", "2020-05-03", "2020-05-04", "2020-05-05", "2020-05-06", "2020-06-20", "2020-07-07",
        "2020-07-20", "2020-08-11", "2020-09-21", "2020-09-22", "2020-09-22", "2020-09-22", "2020-10-12", "2020-11-03",
        "2020-11-15", "2020-11-23", "2020-12-21", "2020-12-23", "2020-12-25", "2020-12-31"
    ];

    return in_array($date->format("Y-m-d"), $holidays);
}

function isOffDay($date, $offDays)
{
    if(!is_array($offDays)) return false;

    return in_array($date->format("Y-m-d"), $offDays);
}

function getOffDays($date, $site_id)
{
    //grab this from the calendars
    $d = $date->format('Y-m');
    $sql = "SELECT BOOK_DATE FROM calendar_state WHERE OP_VIEW_STATUS = 1 AND BOOK_DATE LIKE '$d%' AND site_id = $site_id;";
    $results = queryForRows($sql);

    $offDays = [];
    foreach ($results as $result)
    {
        $offDays[] = $result['BOOK_DATE'];
    }
    return $offDays;
}

function defaultProjectedValues($site_id, $d)
{
    //TODO grab these from the database when the user sets them
    $offDayProjected          = 0;
    $weekendProjected         = 40;
    $nationalHolidayProjected = 50;
    $regularDayProjected      = 20;

    $date = DateTime::createFromFormat('Y-m-d', $d."-01");
    $dateEnd   = clone $date;

    $dateEnd->modify("last day of this month");

    $defaultJumpNumbers = [];
    $offDays = getOffDays($date, $site_id);

    while($date->format('U') <= $dateEnd->format('U'))
    {
        if(isOffDay($date, $offDays)){
            $projectedJumps = $offDayProjected;

        } else if(isNationalHoliday($date)){//NOTE we are giving holiday projections precedence over weekends
            $projectedJumps = $nationalHolidayProjected;

        } else if(isWeekend($date)){
            $projectedJumps = $weekendProjected;

        } else {
            $projectedJumps = $regularDayProjected;

        }

        $defaultJumpNumbers[$date->format('d')] = $projectedJumps;

        $date->modify("+1 day");
    }

    return $defaultJumpNumbers;
}


function getProjected($site_id, $d)
{
    $projected = array();
    $sql = "SELECT * FROM roster_target 
			LEFT JOIN calendar_state 
			ON roster_target.site_id = calendar_state.site_id AND roster_target.roster_date = calendar_state.BOOK_DATE
			WHERE roster_target.site_id = '$site_id' 
			AND roster_date LIKE '$d-%' 
			ORDER BY roster_date ASC;";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        $projected[substr($row['roster_date'], 8, 2)] = 
			($row['USER_VIEW_STATUS'] == 1 ? 0 : $row['jumps']);// If the day is turned off set the projected jumps to 0.
    };
    mysql_free_result($res);

    return $projected;
}

function getRosterBookings($site_id, $d)
{
    $sql = "SELECT BookingDate, sum(NoOfJump) as bookings FROM customerregs1 WHERE site_id = '$site_id' AND BookingDate like '$d-%' and DeleteStatus = 0 GROUP BY BookingDate ORDER BY BookingDate ASC;";
    $res = mysql_query($sql) or die(mysql_error());
    $bookings = array();
    while ($row = mysql_fetch_assoc($res)) {
        $day = substr($row['BookingDate'], 8, 2);
        if (!array_key_exists($day, $bookings)) {
            $bookings[$day] = $row['bookings'];
        };
    };
    mysql_free_result($res);

    return $bookings;
}
?>
