<?php
include '../includes/application_top.php';
$mode = 'daily_view';
if (isset($_GET['mode'])) {
    $mode = $_GET['mode'];
};
// User rights
$read_only = true;
$user = new BJUser();
if ($user->hasRole('Site Manager') || $user->hasRole('General Manager') || $user->hasRole('SysAdmin' || $user->hasRole() == 'RosterManager')) {
    $read_only = false;
};
//EOF User rights
$current_time = time();
$d = date('Y-m-d', $current_time);
if (isset($_GET['date'])) {
    $current_time = mktime(0, 0, 0, substr($_GET['date'], 5, 2), substr($_GET['date'], 8, 2), substr($_GET['date'], 0, 4));
} else {
    $current_time = mktime(0, 0, 0, substr($d, 5, 2), substr($d, 8, 2), substr($d, 0, 4));
};
$d = date('Y-m-d', $current_time);
$current_date = $d;
$month_days = date("t", $current_time);
$cmonth = date('Ym', $current_time);
// positions
$positions = BJHelper::getRosterPositionsDropDown();
$positions_by_id = array();
foreach ($positions as $p) {
    $positions_by_id[$p['id']] = $p;
};
//functions
$functions = BJHelper::getRosterFunctionsDropDown();
$function_by_id = array();
foreach ($functions as $f) {
    $functions_by_id[$f['id']] = $f;
};
// templates
$templates = BJHelper::getRosterTemplatesDropDown();
$templates_by_id = array();
foreach ($templates as $t) {
    $templates_by_id[$t['id']] = $t;
};

$action = $_POST['action'];
switch ($action) {
    case 'update':
        $site_id = CURRENT_SITE_ID;
        $template_id = $_GET['template_id'];
        if ($mode == 'template') {
            $places = array();
            foreach ($_POST['position'] as $p) {
                $places[$p['place']] = $p['position_id'];
                $sql = "SELECT id from roster_daily_pos
						WHERE 
							site_id = '$site_id' 
							AND template_id = '$template_id'
							AND place = '{$p['place']}'";
                $res = mysql_query($sql);
                $action = 'insert';
                $where = '';
                if ($row = mysql_fetch_assoc($res)) {
                    $action = 'update';
                    $where = 'id=' . $row['id'];
                };
                $data = array(
                    'site_id'     => $site_id,
                    'template_id' => $template_id,
                    'place'       => $p['place'],
                    'position_id' => $p['position_id']
                );
                db_perform('roster_daily_pos', $data, $action, $where);
            };
        };
        if ($mode == 'template') {
            $roster_date = '0000-00-00';
        } else {
            $roster_date = $_GET['date'];
        };
        foreach ($_POST['time'] as $t) {
            // ignore records without position selected
            if ($mode == $template) {
                if (!array_key_exists($t['place'], $places)) {
                    continue;
                };
            };
            $sql = "SELECT id FROM roster_daily_funcs
						WHERE 
							site_id = '$site_id'
							AND template_id = '$template_id'
							AND place = '{$t['place']}'
							AND roster_date = '$roster_date'
							AND roster_time = '{$t['time']}';";
            $res = mysql_query($sql);
            $action = 'insert';
            $where = '';
            if ($row = mysql_fetch_assoc($res)) {
                $action = 'update';
                $where = 'id=' . $row['id'];
                if (empty($t['func_id']) || $t['func_id'] == 0) {
                    $sql = preg_replace("/^(.*?)WHERE/is", "DELETE FROM roster_daily_funcs WHERE ", $sql);
                    $res = mysql_query($sql) or die(mysql_error());
                    continue;
                };
            };
            if (empty($t['func_id']) || $t['func_id'] == 0) {
                continue;
            };
            $data = array(
                'site_id'        => $site_id,
                'template_id'    => $template_id,
                'place'          => $t['place'],
                'roster_date'    => $roster_date,
                'roster_time'    => $t['time'],
                'roster_func_id' => $t['func_id']
            );
            db_perform('roster_daily_funcs', $data, $action, $where);
        };
        $params = '';
        if ($mode == 'template') {
            $params = 'mode=' . $mode . '&template_id=' . $template_id;
        } else {
            $params = 'date=' . $current_date;
        };
        //Header("Location: /Roster/daily_roster.php?" . $params);
        die();
        break;
};
$current_template = null;
$current_template_id = null;
$page_title = $current_date;
$data_exists = false;
$needed_jumps = 0;
$func_date = '0000-00-00';
if ($mode == 'template') {
    if ($_GET['template_id']) {
        $current_template_id = $_GET['template_id'];
    };
    // check current template ID to be valid
} else {
    // if daily_view mode, just search for right template
    // if this page already have DB entries
    $site_id = CURRENT_SITE_ID;
    $sql = "SELECT * FROM roster_daily_funcs WHERE site_id = '$site_id' and roster_date = '$current_date' ORDER BY NULL;";
    $res = mysql_query($sql) or die(mysql_error());
    if ($row = mysql_fetch_assoc($res)) {
        $current_template_id = $row['template_id'];
        $data_exists = true;
        $func_date = $row['roster_date'];
    };
    // if there is no previous records, get default records
    if (is_null($current_template_id)) {
        // find current template
        $sql = "SELECT * FROM roster_target WHERE site_id = '$site_id' and roster_date = '$current_date';";
        $res = mysql_query($sql) or die(mysql_error());
        if ($row = mysql_fetch_assoc($res)) {
            $needed_jumps = $row['jumps'];
        };
    };
};
// select right template info
$template_found = false;
$first_template = null;
foreach ($templates as $t) {
    if (empty($t['id'])) continue;
    if (is_null($first_template) && !empty($t['id'])) {
        $first_template = $t;
    };
    if ( // search completed by template ID
        (!is_null($current_template_id) && $current_template_id == $t['id'])
        || // search completed by jumps needed
        ($needed_jumps > 0 && $needed_jumps <= $t['max_jumps'] && $needed_jumps >= $t['min_jumps'])
    ) {
        $template_found = true;
        $current_template = $t;
    };
};
if (!$template_found) {
    $current_template = $first_template;
}
// select template data
if (!is_null($current_template)) {
    $current_template_name = $current_template['text'];
    $current_template_jumpers = $current_template['min_jumps'] . ' - ' . $current_template['max_jumps'] . ' jumpers';
    $current_template_color = $current_template['color'];
// fetch current template
    $site_id = CURRENT_SITE_ID;
    $template_id = $current_template['id'];
// positions
    $sql = "SELECT * FROM roster_daily_pos WHERE site_id = '$site_id' and template_id = '$template_id' ORDER BY place ASC;";
    $res = mysql_query($sql) or die(mysql_error());
    $tpositions = array();
    while ($row = mysql_fetch_assoc($res)) {
        $tpositions[$row['place']] = $row;
    };
// functions
    $sql = "SELECT * FROM roster_daily_funcs WHERE site_id = '$site_id' and template_id = '$template_id' and roster_date = '$func_date' ORDER BY NULL;";
    $res = mysql_query($sql) or die(mysql_error());
    $tfuncs = array();
    while ($row = mysql_fetch_assoc($res)) {
        if (!array_key_exists($row['place'], $tfuncs)) {
            $tfuncs[$row['place']] = array();
        };
        $tfuncs[$row['place']][$row['roster_time']] = $row;
    };
};
if ($mode == 'template') {
    $params = 'mode=' . $mode;
    $page_title = ucfirst($mode) . ' - ' . $current_template['text'];
    $params .= '&template_id=' . $current_template['id'];
} else {
    $params = 'date=' . $current_date . "&template_id=" . $current_template['id'];
};
// refill some data if exists
$names = array();
// select staff names
if ($mode == 'daily_view') {
    $roster_month = date("Ym", $current_time);
    $roster_day = date("d", $current_time);
    $sql = "SELECT
					rp.position_id,
					rs.staff_id,
					u.StaffListName
				FROM 
					roster_position rp,
					roster_staff rs,
					users u
				WHERE
					rp.site_id = '$site_id'
					AND rp.roster_month = '$roster_month'
					AND rp.roster_day = '$roster_day'
					AND rp.place = rs.place
					AND rs.site_id = '$site_id'
					AND rs.roster_month = '$roster_month'
					AND rs.staff_id = u.UserID;";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        $names[$row['position_id']] = $row['StaffListName'];
    };
};
// for printout
$page_stitle = '';
if ($mode == 'daily_view') {
    $page_stitle = '<span id="location">Platform</span> ';
};
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo SYSTEM_SUBDOMAIN; ?> DAILY ROSTER - <?php echo $page_title; ?></title>
    <?php include "../includes/head_scripts.php"; ?>
    <script language="javascript" src="/js/tooltip.js"></script>
    <style>
        @media print {
            .not-printed, .not-printed * {
                display: none !important;
            }
        }

        /* TOOLTIP */
        #tt {
            position: absolute;
            display: block;
            background: url(/img/tt_left.gif) top left no-repeat
        }

        #tttop {
            display: block;
            height: 5px;
            margin-left: 5px;
            background: url(/img/tt_top.gif) top right no-repeat;
            overflow: hidden
        }

        #ttcont {
            display: block;
            padding: 2px 12px 3px 7px;
            margin-left: 5px;
            background: #666;
            color: #FFF
        }

        #ttbot {
            display: block;
            height: 5px;
            margin-left: 5px;
            background: url(/img/tt_bottom.gif) top right no-repeat;
            overflow: hidden
        }

        /* EOF TOOLTIP */
        #roster-table-header {
            margin-left: auto;
            margin-right: auto;
            width: 1060px;
        }

        #roster-table {
            border-collapse: collapse;
            border: 2px solid black;
            margin-left: auto;
            margin-right: auto;
        }

        .daily-header th {
            font-family: Arial;
            font-size: 10px;
            text-align: center;
            border: 1px solid black;
        }

        .daily-header th.time {
            width: 40px;
        }

        .daily-header th.position {
            width: 70px;
        }

        .daily-header th.name {
            width: 50px;
        }

        .divider td {
            border-top: 1px solid black;
            border-bottom: 1px solid black;
        }

        .daily-row td {
            border: 1px dashed silver;
        }

        .daily-row td.position, .daily-row td.name {
            font-family: Arial;
            font-size: 8px;
            text-align: center;
            vertical-align: middle;
        }

        .daily-row td.name {
            border-left: 1px solid black;
            border-right: 1px solid black;
        }

        .daily-row td.time {
            font-family: Arial;
            font-size: 8px;
            text-align: center;
            vertical-align: middle;
        }

        #funcs-drop, #pos-drop, #template-drop {
            display: block;
            background-color: white;
            color: black;
            width: 80px;
            height: auto;
            font: Arial 10px;
        }

        #funcs-drop ul, #pos-drop ul, #template-drop ul {
            margin: 0px;
            padding: 0px;
            border-collapse: collapse;
        }

        #funcs-drop ul li, #pos-drop ul li {
            display: block;
            list-style: none;
            margin: 0px;
            padding: 0px;
            border: 1px solid black;
            text-align: center;
            font-family: Arial;
            font-size: 10px;
            cursor: pointer;
        }

        #template-drop ul li {
            display: block;
            list-style: none;
            margin: 0px;
            padding: 5px;
            border: 1px solid black;
            text-align: center;
            font-family: Arial;
            font-size: 12px;
            cursor: pointer;

        }

        td.template, td.jumpers {
            border: 1px solid black;
            text-align: center;
        }

        .selected {
            background-color: #eee !important;
        }

        #buttons {
            width: 1000px;
            margin-left: auto;
            margin-right: auto;
            margin-top: 20px;
        }

        #buttons button {
            background-color: yellow;
            border: 1px solid black;
            border-radius: 5px;
            padding: 5px;
        }

        #update {
            float: right;
            width: 100px;
        }

        #print {
            float: right;
            width: 100px;
            margin-right: 400px;
        }

        h1 {
            overflow: visible;
            position: relative;
        }

        #location {
            position: absolute;
            display: block-inline;
            top: 0px;
            left: 0px;
            cursor: pointer;
        }

        #location-drop {
            display: block;
            background-color: white;
            color: black;
            width: 180px;
            height: auto;
        }

        #location-drop ul {
            margin: 0px;
            padding: 0px;
            border-collapse: collapse;
        }

        #location-drop ul li {
            display: block;
            list-style: none;
            margin: 0px;
            padding: 0px;
            border: 1px solid black;
            text-align: center;
            cursor: pointer;
        }

    </style>
    <script>
        var mode = '<?php echo $mode; ?>';
        var current_date = '<?php echo $current_date; ?>';
        var funcs = <?php echo json_encode($functions_by_id); ?>;
        <?php if ($mode == 'template') { ?>
        <?php if (!$read_only) { ?>
        var positions = <?php echo json_encode($positions_by_id); ?>;
        var templates = <?php echo json_encode($templates_by_id); ?>;
        <?php }; ?>
        <?php } else { ?>
        var locations = ["Platform", "Recovery", "Reception", "Office"];
        <?php }; ?>
        function color_table() {
            $('td.time').each(function () {
                var bcolor = 'white';
                var tcolor = 'black';
                if (typeof $(this).attr('custom-func-id') != 'undefined' && $(this).attr('custom-func-id') != 0) {
                    bcolor = funcs[$(this).attr('custom-func-id')].bcolor ? funcs[$(this).attr('custom-func-id')].bcolor : 'white';
                    tcolor = funcs[$(this).attr('custom-func-id')].bcolor ? funcs[$(this).attr('custom-func-id')].tcolor : 'black';
                }
                ;
                $(this).css('background-color', bcolor);
                $(this).css('color', tcolor);
            });
        }
        $(document).ready(function () {
            color_table();
            <?php if (!$read_only) { ?>
            $("td.time").click(show_func_drop_down);
            <?php }; ?>
            <?php if ($mode == 'template') { ?>
            <?php if (!$read_only) { ?>
            $("td.position").click(show_pos_drop_down);
            $("td.template").click(show_template_drop_down);
            <?php }; ?>
            <?php } else { ?>
            $("span#location").click(show_location_drop_down);
            $("span#location").hover(function () {
                $(this).css('background-color', '#eeeeee');
            }, function () {
                $(this).css('background-color', 'white');
            })
            $("#print").click(function () {
                window.print();
            });
            <?php }; ?>
            $('body').click(function (e) {
                if (!((e.target.className == 'time' || e.target.className == 'time selected') && e.target.nodeName == 'TD')) {
                    $('#funcs-drop').remove();
                    $('.selected').removeClass('selected');
                    color_table();
                }
                ;
                if (!(e.target.className == 'position' && e.target.nodeName == 'TD')) {
                    $('#pos-drop').remove();
                    $('.position').css('background-color', 'white');
                }
                ;
                if (!(e.target.className == 'template' && e.target.nodeName == 'TD')) {
                    $('#template-drop').remove();
                }
                ;
                if (!(e.target.nodeName == 'SPAN')) {
                    $('#location-drop').remove();
                }
                ;
            });
            <?php if (!$read_only) { ?>
            $('#update').click(save_roster);
            <?php }; ?>
        });
        <?php if (!$read_only) { ?>
        function save_roster() {
            $(this).html("Updating...");
            $(this).attr('disabled', 'disabled');
            var data = {'action': 'update'};
            <?php if ($mode == 'template') { ?>
            data.position = [];
            $('td.position').each(function () {
                data.position.push({
                    'place': $(this).attr('custom-place'),
                    'position_id': $(this).attr('custom-pos-id')
                });
            });
            <?php }; ?>
            data.time = [];
            $('td.time').each(function () {
                data.time.push({
                    'place': $(this).attr('custom-place'),
                    'time': $(this).attr('custom-time'),
                    'func_id': $(this).attr('custom-func-id')
                });
            });
            $.ajax({
                'method': 'POST',
                'url': '<?php echo $PHP_SELF; ?>?<?php echo $params; ?>',
                'data': data,
                'success': function () {
                    window.alert('Information updated.');
                    //document.location = '<?php echo $PHP_SELF; ?>?<?php echo $params; ?>';
                    $('#update').html('Update');
                    $('#update').attr('disabled', false);
                }
            });
            return false;
        }
        <?php if ($mode == 'template') { ?>
        function show_pos_drop_down(e) {
            $('#pos-drop').remove();
            if (e.target.nodeName != 'TD') return;
            var curr = $(this);
            var dd_div = $("<div id='pos-drop'>");
            dd_div.html("<ul><li custom-pos-id=''>&nbsp;</li></ul>");
            for (i in positions) {
                if (!positions.hasOwnProperty(i)) continue;
                if (!positions[i].id) continue;
                dd_div.children("ul").append("<li " +
                "custom-pos-id='" + positions[i].id + "'>" +
                (positions[i].text ? positions[i].text : '&nbsp;') + '</li>');
            }
            ;
            var ot = curr.offset();
            dd_div.css({
                'position': 'absolute',
                'left': ot.left + 5,
                'top': ot.top + 18,
            });
            curr.append(dd_div);
            $('#pos-drop li').click(on_select_pos);
            curr.css('background-color', '#eee');
        }
        function on_select_pos(e) {
            if (e.target.nodeName != 'LI') return;
            var parent_td = $(this).parent().parent().parent();
            var custom_place = parent_td.attr('custom-place');
            parent_td = $('td.position[custom-place=' + custom_place + ']');
            parent_td.attr('custom-pos-id', $(this).attr('custom-pos-id'));
            parent_td.html($(this).html());
            $('.position').css('background-color', 'white');
        }
        <?php }; ?>
        function show_func_drop_down(e) {
            $('#funcs-drop').remove();
            if (e.target.nodeName != 'TD') return;
            var curr = $(this);
            var dd_div = $("<div id='funcs-drop'>");
            dd_div.html("<ul><li custom-func-id=''>&nbsp;</li></ul>");
            for (i in funcs) {
                if (!funcs.hasOwnProperty(i)) continue;
                if (!funcs[i].id) continue;
                dd_div.children("ul").append("<li style='background-color: " +
                (funcs[i].bcolor ? funcs[i].bcolor : 'white') + "; " +
                "color: " + (funcs[i].tcolor ? funcs[i].tcolor : 'black') + ";' " +
                "custom-func-id='" + (funcs[i].id ? funcs[i].id : '0') + "'>" +
                (funcs[i].text ? funcs[i].text : '&nbsp;') + '</li>');
            }
            ;
            var ot = curr.offset();
            dd_div.css({
                'position': 'absolute',
                'left': ot.left - 20,
                'top': ot.top + 20,
            });
            curr.append(dd_div);
            $('#funcs-drop li').click(on_select_func);
            //curr.css('background-color', '#eee');
            if ($(this).hasClass('selected')) {
                $(this).removeClass('selected');
            } else {
                $(this).addClass('selected');
            }
            ;
        }
        function on_select_func(e) {
            if (e.target.nodeName != 'LI') return;
            var parent_td = $(this).parent().parent().parent();
            var parent_td = $("td.selected");
            parent_td.attr('custom-func-id', $(this).attr('custom-func-id'));
            var bcolor = funcs[$(this).attr('custom-func-id')].bcolor;
            var tcolor = funcs[$(this).attr('custom-func-id')].tcolor;
            parent_td.html($(this).html());
            parent_td.css({
                'background-color': bcolor ? bcolor : 'white',
                'color': tcolor ? tcolor : 'black'
            });
            parent_td.removeClass('selected');
        }
        <?php if ($mode == 'template') { ?>
        function show_template_drop_down(e) {
            $('#template-drop').remove();
            if (e.target.nodeName != 'TD') return;
            var curr = $(this);
            var dd_div = $("<div id='template-drop'>");
            dd_div.html("<ul></ul>");
            for (i in templates) {
                if (!templates.hasOwnProperty(i)) continue;
                if (!templates[i].id) continue;
                dd_div.children("ul").append("<li style='background-color: " +
                (templates[i].color ? templates[i].color : 'white') + ";'" +
                "custom-template-id='" + (templates[i].id ? templates[i].id : '0') + "'>" +
                (templates[i].text ? templates[i].text : '&nbsp;') + '</li>');
            }
            ;
            var ot = curr.offset();
            dd_div.css({
                'position': 'absolute',
                'left': ot.left,
                'top': ot.top + 22,
            });
            curr.append(dd_div);
            $('#template-drop li').click(on_select_template);
        }
        function on_select_template(e) {
            if (e.target.nodeName != 'LI') return;
            var parent_td = $(this).parent().parent().parent();
            var template_id = $(this).attr('custom-template-id');
            document.location = '<?php echo $PHP_SELF; ?>?' + '<?php echo $params; ?>'.replace('template_id=<?php echo $current_template['id']; ?>', 'template_id=' + template_id);
        }
        <?php }; ?>
        <?php }; ?>
        <?php if ($mode == 'daily_view') { ?>
        function show_location_drop_down(e) {
            $('#location-drop').remove();
            if (e.target.nodeName != 'SPAN') return;
            var curr = $(this);
            var dd_div = $("<div id='location-drop'>");
            dd_div.html("<ul></ul>");
            for (i in locations) {
                if (!locations.hasOwnProperty(i)) continue;
                dd_div.children("ul").append("<li>" + locations[i] + '</li>');
            }
            ;
            var ot = curr.offset();
            dd_div.css({
                'position': 'absolute',
                'left': ot.left - 50,
                'top': ot.top + 10,
            });
            curr.append(dd_div);
            $('#location-drop li').click(on_select_location);
        }
        function on_select_location(e) {
            if (e.target.nodeName != 'LI') return;
            var parent_td = $(this).parent().parent().parent();
            var location_text = $(this).html();
            parent_td.html(location_text);
            parent_td.trigger('mouseout');
        }
        <?php }; ?>
    </script>
</head>
<body>
<?php include 'includes/main_menu.php'; ?>
<table id="roster-table-header">
    <tr>
        <th colspan="24">
            <h1><?php echo $page_stitle; ?><?php echo SYSTEM_SUBDOMAIN; ?> DAILY ROSTER <br/><?php echo $page_title; ?>
            </h1>
        </th>
    </tr>
</table>
<table id="roster-table">
    <?php
    if (!is_null($current_template)) {
        ?>
        <tr class="template-selection">
            <td class="template" style="background-color: <?php echo $current_template_color; ?>;"><?php echo $current_template_name; ?></td>
            <td class="jumpers" colspan="3"><?php echo $current_template_jumpers; ?></td>
            <td colspan="20">&nbsp;</td>
        </tr>
        <tr class="divider-no-borders">
            <td colspan="24">&nbsp;</td>
        </tr>
        <?php
        for ($dp = 0; $dp < 2; $dp++) {
            ?>
            <tr class="daily-header">
                <th class="position">POSITION</th>
                <th class="name">NAME</th>
                <?php
                for ($i = 0; $i < 22; $i++) {
                    $mins = $i * 15 + $dp * 330;
                    $hour = 7 + ($mins - (($mins + 30) % 60) + 30) / 60;
                    $mins = ($mins + 30) % 60;
                    $time = sprintf("%02d:%02d", $hour, $mins);
                    echo "\t<th class='time'>$time</th>\n";
                };
                ?>
            </tr>
            <?php
            for ($place = 1; $place < 13; $place++) {
                $pos_id = '';
                $pos_name = '&nbsp;';
                if (isset($tpositions) && !empty($tpositions) && array_key_exists($place, $tpositions)) {
                    $pos_id = $tpositions[$place]['position_id'];
                    $pos_name = $positions_by_id[$tpositions[$place]['position_id']]['text'];
                };
                if (empty($pos_name)) {
                    $pos_name = '&nbsp;';
                }
                ?>
                <tr class="daily-row">
                    <td class="position" custom-place="<?php echo $place; ?>" custom-pos-id="<?php echo $pos_id; ?>"><?php echo $pos_name; ?></td>
                    <td class="name"><?php echo (isset($names[$pos_id])) ? $names[$pos_id] : ''; ?></td>
                    <?php
                    for ($i = 0; $i < 22; $i++) {
                        $mins = $i * 15 + $dp * 330;
                        $hour = 7 + ($mins - (($mins + 30) % 60) + 30) / 60;
                        $mins = ($mins + 30) % 60;
                        $time = sprintf("%02d:%02d", $hour, $mins);
                        $func_id = 0;
                        $func = '';
                        if (isset($tfuncs) && !empty($tfuncs) && array_key_exists($place, $tfuncs) && array_key_exists($time, $tfuncs[$place])) {
                            $func_id = $tfuncs[$place][$time]['roster_func_id'];
                            $func = $functions_by_id[$func_id]['text'];
                        };
                        echo "\t<td class='time' custom-func-id='$func_id' custom-place='$place' custom-time='$time'>$func</td>\n";
                    };
                    ?>
                </tr>
            <?php
            }
        ;
            if ($dp == 0) {
                ?>
                <tr class="divider">
                    <td colspan="24">&nbsp;</td>
                </tr>
            <?php
            };
        };
    } else {
        // current_template non exists
        ?>
        <tr>
            <td colspan="24">No templates exists</td>
        </tr>
    <?php
    };
    ?>
</table>
<div id="buttons" class="not-printed">
    <?php if (!$read_only) { ?>
        <button type="button" name='action' value='update' id="update">Update</button>
    <?php }; ?>
    <?php if ($mode == 'daily_view') { ?>
        <button type="button" name='action' value='print' id="print">Print</button>
    <?php }; ?>
    <button onClick="javascript: document.location='<?php echo ($mode == 'template') ? '/ManageRoster/templates.php' : '/Roster/?date=' . date('Y-m', $current_time); ?>'" id="back">Back</button>
</div>
<br/>
<?php include("ticker.php"); ?>
</body>
</html>
