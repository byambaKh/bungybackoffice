<?php
include '../includes/application_top.php';
require("functions.php");
// User rights
$read_only = true;
$user = new BJUser();
if ($user->hasRole('Site Manager') ||
    $user->hasRole('General Manager') ||
    $user->hasRole('SysAdmin') ||
    $user->hasRole('RosterManager') ||
    $user->hasRole('Construction')) {
    $read_only = false;
}

//EOF User rights
$current_time = time();
$d = date('Y-m', $current_time);
if (isset($_GET['date'])) {
    $current_time = mktime(0, 0, 0, substr($_GET['date'], 5, 2), 15, substr($_GET['date'], 0, 4));
} else {
    $current_time = mktime(0, 0, 0, substr($d, 5, 2), 15, substr($d, 0, 4));
}
$d = date('Y-m', $current_time);
$current_date = $d;
$params = 'date=' . $d;
$month_days = date("t", $current_time);
$cmonth = date('Ym', $current_time);
$site_id = CURRENT_SITE_ID;
if (!$read_only) {
    $action = $_POST['action'];
    switch ($action) {
        case 'update':
            $current_time = mktime(0, 0, 0, substr($_POST['date'], 5, 2), 15, substr($_POST['date'], 0, 4));
            $roster_month = str_replace("-", "", $_POST['date']);
            $current_date = $_POST['date'];
            foreach ($_POST['staff'] as $place => $sid) {
                // ignore not selected rows and delete position records
                if ($sid == 0) {
                    mysql_query("DELETE from roster_staff WHERE site_id = '$site_id' AND roster_month = '$roster_month' and place = '$place';") or die(mysql_error());
                    mysql_query("DELETE from roster_position WHERE site_id = '$site_id' AND roster_month = '$roster_month' and place = '$place';") or die(mysql_error());
                    continue;
                }
                $sql = "SELECT id from roster_staff WHERE site_id = '$site_id' AND roster_month = '$roster_month' AND place = '$place';";
                $res = mysql_query($sql) or die(mysql_error());
                // prepare staff record
                $data = array(
                    'staff_id' => $sid,
                );
                $action = 'insert';
                $where = '';
                if ($row = mysql_fetch_assoc($res)) {
                    db_perform('roster_staff', $data, 'update', 'id=' . $row['id']);
                } else {
                    $data['place'] = $place;
                    $data['roster_month'] = $roster_month;
                    $data['site_id'] = $site_id;
                    db_perform('roster_staff', $data) or die(mysql_error());
                }
                foreach ($_POST['position'][$place] as $day => $position_id) {
                    // fix for JS send array
                    if ($day == 0) continue;
                    // if empty, just try to delete
                    if (empty($position_id) || $position_id == 0) {
                        mysql_query(sprintf("DELETE FROM roster_position
							WHERE 
								site_id = '$site_id' 
								AND roster_month = '$roster_month' 
								and roster_day = '%d' 
								and place = '$place';", $day));
                    } else {
                        // check database record for this day
                        $sql = sprintf("SELECT id FROM roster_position
							WHERE 
								site_id = '$site_id'
								AND roster_month = '$roster_month' 
								and roster_day = '%d' 
								and place = '$place';", $day);
                        $res = mysql_query($sql) or die(mysql_error());
                        $data = array(
                            'position_id' => $position_id
                        );
                        if ($row = mysql_fetch_assoc($res)) {
                            // existing record, update them
                            db_perform('roster_position', $data, 'update', 'id=' . $row['id']);
                        } else {
                            // not existing record, insert
                            $data['site_id'] = $site_id;
                            $data['place'] = $place;
                            $data['roster_month'] = $roster_month;
                            $data['roster_day'] = $day;
                            db_perform('roster_position', $data);
                        }
                    }
                }
            }
            foreach ($_POST['projected'] as $day => $jumps) {
                if ($day == 0) continue;
                if (empty($jumps)) {
                    mysql_query(sprintf("DELETE FROM roster_target WHERE site_id = '$site_id' AND roster_date = '$current_date-%02d';", $day));
                } else {
                    // check database record for this day
                    $sql = sprintf("SELECT id FROM roster_target WHERE site_id = '$site_id' AND roster_date = '$current_date-%02d';", $day);
                    $res = mysql_query($sql) or die(mysql_error());
                    $data = array(
                        'jumps' => $jumps
                    );
                    if ($row = mysql_fetch_assoc($res)) {
                        // existing record, update them
                        db_perform('roster_target', $data, 'update', 'id=' . $row['id']);
                    } else {
                        // not existing record, insert
                        $data['site_id'] = $site_id;
                        $data['roster_date'] = $current_date . sprintf("-%02d", $day);
                        db_perform('roster_target', $data);
                    }
                }
            }
            // save roster notes
            $sql = "SELECT id FROM roster_notes WHERE roster_month = '$roster_month';";
            $action = 'insert';
            $where = '';
            $data = array(
                'notes' => stripslashes($_POST['notes'])
            );
            $res = mysql_query($sql) or die(mysql_error());
            if ($row = mysql_fetch_assoc($res)) {
                $action = 'update';
                $where = 'id=' . $row['id'];
            } else {
                $data['roster_month'] = $roster_month;
            }
            db_perform('roster_notes', $data, $action, $where);
            //Header("Location: /Roster/?date=$current_date");
            die();
            break;
    }
}
// fill minimum staff fields
$roster_staff = array();
for ($i = 0; $i < 15; $i++) {
    $roster_staff[$i] = array();
}
// fill staff array
$sql = "SELECT * FROM roster_staff WHERE site_id = '$site_id' AND roster_month = $cmonth ORDER BY place ASC;";
$res = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_assoc($res)) {
    $roster_staff[$row['place']] = $row;
}
$roster_size = sizeof($roster_staff) - 1;
if (!empty($roster_staff[$roster_size]) && !empty($roster_staff[$roster_size]['staff_id'])) {
    $roster_staff[] = array();
}
mysql_free_result($res);
// EOF FILL STAFF

// fill STAFF POSITIONS
$sql = "SELECT *
		FROM roster_position rs
		WHERE 
			site_id = '$site_id' AND roster_month = '$cmonth'
		ORDER BY roster_day ASC;";
$res = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_assoc($res)) {
    $roster_staff[$row['place']]['positions'][$row['roster_day']] = $row;
}
mysql_free_result($res);
// EOF fill STAFF POSITION

// Load staff Names list
$staff_names = BJHelper::getStaffList('UserID', true);
// EOF load staff names list
$position_array = BJHelper::getRosterPositionsDropDown();
$positions_by_id = array();
foreach ($position_array as $p) {
    $positions_by_id[$p['id']] = $p;
}
// load position list
// load targets

// TODO Group the parts of code in to functions and throw things in to classes

//generate an array of dates with default values
//then merge the results with the results from projected

$projectedByStaff = getProjected($site_id, $d);
$defaultProjected = defaultProjectedValues($site_id, $d);

//replace the default projected values with the ones written by staff if they exist
//note: array_replace will not rekey the array like array_merge which helps in the situation
$projected = array_replace($defaultProjected, $projectedByStaff);


$bookings = getRosterBookings($site_id, $d);

// Daily keys
$daily = BJHelper::getRosterTemplates();
// notes
$roster_month = str_replace("-", "", $current_date);
$sql = "select * FROM roster_notes WHERE roster_month='$roster_month';";
$res = mysql_query($sql) or die(mysql_error());
$notes = '';
if ($row = mysql_fetch_assoc($res)) {
    $notes = $row['notes'];
}
// Minakami + Sarugakyo
$places = BJHelper::getPlaces();
$places_by_id = array();
foreach ($places as $p) {
    $places_by_id[$p['id']] = $p;
}

$employed = array();
$pid = CURRENT_SITE_ID;
/*
 * This seems to select all of the days that are not assigned to the current site.
 * What makes little sense is that there is a site id and also a place. We have an array of places
 * which are all of the current site ids but 'place' does not correspond to site ids. It goes up to 17.
 *
 * Position is the role the person is playing on the site. Ie JM, OFC etc.
 * $employed appears to be an array of 'away' work days from the present location.
 * roster_staff appears to contain present days
 * How roster_position are paired to user ids in roster_staff makes no sense as there is no identifying info in roster staff
    #SARUGAKYO
    SELECT rs.staff_id, rp.roster_day, rs.site_id
                    FROM roster_staff rs, roster_position rp
                    WHERE
                        rs.site_id <> '2' AND rs.roster_month = '201502'
                        AND rs.place = rp.place
                        AND rp.site_id = rs.site_id
                        AND rp.roster_month = '201502'
                        AND staff_id = 10000071
                    ORDER BY roster_day ASC;


    #MINAKAMI
    #What is place? We have site_id, position is the role they are in. What is the place????
    SELECT rs.staff_id, rp.roster_day, rs.site_id
                    FROM roster_staff rs, roster_position rp
                    WHERE
                        rs.site_id <> '1' AND rs.roster_month = '201502'
                        AND rs.place = rp.place
                        AND rp.site_id = rs.site_id
                        AND rp.roster_month = '201502'
                        AND staff_id = 10000071

                    ORDER BY roster_day ASC;
 */
if (!is_null($pid)) {
    $sql = "SELECT rs.staff_id, rp.roster_day, rs.site_id
				FROM roster_staff rs, roster_position rp
				WHERE
					/*rs.site_id <> '$pid' AND*/ rs.roster_month = '$cmonth'/*By removing the rs.site_id is not = pid it fixes the issue we have with some wrongs off days counts*/
					AND rs.place = rp.place
					AND rp.site_id = rs.site_id
					AND rp.roster_month = '$cmonth'
				ORDER BY roster_day ASC;";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        $employed[$row['staff_id']][$row['roster_day']] = $places_by_id[$row['site_id']]['short_name'];
    }
    mysql_free_result($res);
}

//echo $sql;
//	}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo strtoupper(CURRENT_SITE_DISPLAY_NAME); ?> MONTHLY ROSTER - <?php echo date("F Y", $current_time); ?></title>
    <?php include "../includes/head_scripts.php"; ?>
    <script language="javascript" src="/js/tooltip.js"></script>
    <style>
        /* TOOLTIP */
        #tt {
            position: absolute;
            display: block;
            background: url(/img/tt_left.gif) top left no-repeat
        }

        #tttop {
            display: block;
            height: 5px;
            margin-left: 5px;
            background: url(/img/tt_top.gif) top right no-repeat;
            overflow: hidden
        }

        #ttcont {
            display: block;
            padding: 2px 12px 3px 7px;
            margin-left: 5px;
            background: #666;
            color: #FFF
        }

        #ttbot {
            display: block;
            height: 5px;
            margin-left: 5px;
            background: url(/img/tt_bottom.gif) top right no-repeat;
            overflow: hidden
        }

        /* EOF TOOLTIP */
        .vertical-align {
            display: block;
            margin: 0px;
            padding: 0px;
            font-size: 11px;
            font-family: Arial;
            font-weight: bold;
            width: 150px;
            left: -40px;
            position: relative;
            -webkit-transform: rotate(90deg);
            -moz-transform: rotate(90deg);
            -ms-transform: rotate(90deg);
            -o-transform: rotate(90deg);
            transform: rotate(90deg);
        }

        .va2 {
            display: block;
            margin: 0px;
            padding: 0px;
            font-size: 10px;
            font-family: Arial;
            -webkit-transform: rotate(-90deg);
            -moz-transform: rotate(-90deg);
            -ms-transform: rotate(-90deg);
            -o-transform: rotate(-90deg);
            transform: rotate(-90deg);
        }

        .small-header {
            font-size: 10px;
        }

        #roster-table {
            border: none;
            font-size: 11px;
            font-family: Arial;
            text-align: center;
            border-collapse: collapse;
            margin-left: auto;
            margin-right: auto;
            width: 1000px;
        }

        #roster-table th {
            /*border: 1px solid black;
            border-collapse: collapse;*/
            background-color: white;;
            color: black;
            overflow: hidden;
            max-width: 60px;
            font-size: 11px;
            font-family: Arial;
            border: 1px solid black;
        }

        select {
            width: 99%;
            height: 99%;
            font-size: 10px;
            border: none;
            text-align: left;
            text-indent: 0px;
        }

        .BookingType, .CollectPay, .RateToPayQTY, .Rate, .Agent {
            min-width: 60px;
        }

        .Notes {
            min-width: 100px;
        }

        #roster-table td input {
            width: 99%;
            height: 99%;
            max-width: 100px;
            font-size: 10px;
            border: 1px solid gray;
            text-align: center;
            table-layout: fixed;
        }

        .days td {
            background-color: white;
            border: 1px solid black;
        }

        #roster-table td select option {
            background-color: white !important;
        }

        #roster-table .even td, #roster-table .even td *, #roster-table .odd td, #roster-table .odd td * {
            background-color: gray;
        }

        .even .staff, .even .staff *, .even .selected, .even .selected * {
            background-color: lightyellow !important;
        }

        .odd .staff, .odd .staff *, .odd .selected, .odd .selected * {
            background-color: lightcyan !important;
        }

        #roster-table td select {
            width: 200%;
            padding-right: 0px;
        }

        #roster-table td.staff_position select {
            width: 300%;
            position: relative;
            left: -80%;
            text-align: center;
            padding-right: 0px;
        }

        #roster-table .second-table select {
            width: 300%;
            position: relative;
            left: -80%;
            text-align: center;
            padding-right: 0px;
        }

        #roster-table td {
            overflow: hidden;
            border: 1px solid black;
        }

        #roster-table .separator th {
            border-right: none !important;
            border-left: none !important;
            border-top: 2px solid black;
            border-bottom: 2px solid black;
        }

        .projected td, .projected td *, .daily td, .daily td * {
            background-color: silver;
        }

        .second-table input {
            border: none !important;
        }

        .diff td, .diff td * {
            background-color: lightgreen;
        }

        .greater, .greater * {
            background-color: #f9c !important;
        }

        .lesser, .lesser * {
            background-color: red !important;
        }

        .bottom-header th {
            border: 2px solid black !important;
        }

        tr.lunch td {
            background-color: black !important;
            color: white !important;
        }

        .left td {
            text-align: left;
            padding-left: 2px;
        }

        .border-bottom td {
            border-bottom: 2px solid black !important;
        }

        .last-cell td {
            padding-bottom: 50px;
        }

        .border2 {
            border: 2px solid black !important;
        }

        #special-events {
            vertical-align: top;
            text-align: left;
            padding-left: 5px;
        }

        .days, .staff_position {
            width: 30px;
            min-width: 30px;
            max-width: 30px;
        }

        .days-header {
            cursor: pointer;
        }

        #funcs-drop, #pos-drop, #template-drop {
            display: block;
            background-color: white !important;
            color: black;
            width: 80px;
            height: auto;
            font: Arial 10px;
        }

        #funcs-drop ul, #pos-drop ul, #template-drop ul {
            margin: 0px;
            padding: 0px;
            border-collapse: collapse;
        }

        #funcs-drop ul li, #pos-drop ul li {
            display: block;
            list-style: none;
            margin: 0px;
            padding: 0px;
            border: 1px solid black;
            text-align: center;
            font-family: Arial;
            font-size: 10px;
            cursor: pointer;
            background-color: white !important;
            height: 14px;
        }

        .pos-selected {
            background-color: #eee !important;
        }

        #buttons {
            width: 1000px;
            margin-left: auto;
            margin-right: auto;
            margin-top: 20px;
        }

        #buttons button {
            background-color: yellow;
            border: 1px solid black;
            border-radius: 5px;
            padding: 5px;
            cursor: pointer;
        }

        #update {
            float: right;
            width: 100px;
        }

        #notes {
            width: 99%;
        }

        .disabled {
            background-color: #a55 !important;
        }

        .selected.error {
            /*background-color: #F00 !important; */
        }

        .notes-field {
            text-align: left;
            vertical-align: top;
        }

        .off {
            color: white !important;
        }

        #manage {
            margin-left: 30px;
        }

        /*This class is added to the current day by js so that it is highlighted*/
        #roster-table th[class~="highlighted-day"] {
            font-size: 20px;
            background-color: green;
        }
    </style>
    <script>
        if (!Array.prototype.indexOf) {
            Array.prototype.indexOf = function (elt /*, from*/) {
                var len = this.length >>> 0;

                var from = Number(arguments[1]) || 0;
                from = (from < 0)
                    ? Math.ceil(from)
                    : Math.floor(from);
                if (from < 0)
                    from += len;

                for (; from < len; from++) {
                    if (from in this &&
                        this[from] === elt)
                        return from;
                }
                return -1;
            }
        }

        var positions = <?php echo json_encode($position_array); ?>;
        var employed = <?php echo json_encode($employed); ?>;

        $(document).ready(function () {
            //highlight the current day in the roster
            var date = new Date();
            var dayOfMonth = date.getDate();          // These all depends on the current timezone of the computer. 
            //var dayOfMonth = date.getUTCDate();     // that return the UTC value rather than the values adapted to your current timezone. comment out - 20190910

            $("th[custom-day='"+ dayOfMonth +"']").addClass("highlighted-day");

            $('.days-header').click(function () {
                var cday = $(this).attr('custom-day');
                if (cday.length == 1) cday = '0' + cday;
                document.location = 'daily_roster.php?date=<?php echo $current_date; ?>-' + cday;
            });

            $('.staff select').change(function () {
                var place = $(this).parent().parent().attr('custom-rel');
                $('select[name="staff[' + place + ']"]').val($(this).val());
            });

            //fill_employed();
            fill_projected();

            for (var i = 1; i < <?php echo $month_days + 1; ?>; i++) {
                setTimeout("update_avail(" + i + ")", 10 * i);
            }

            $(".projected input").keyup(update_projected);
            // update OFF column
            setTimeout(update_off, 100);
            $('#update').click(save_monthly_roster);

            $('#notes').css('height', $('#notes').parent().height() - 4);
            <?php if ($read_only) { ?>
            // fill all select and inputs with its content

            $("input").each(function () {
                var val = $(this).val();
                $(this).parent().html(val);
            });

            $("select").each(function () {
                if ($(this).attr('id') == 'target_site') return;
                var val = $(this).children(':selected').html();
                $(this).parent().html(val);
            });

            $("textarea").each(function () {
                var val = $(this).html();
                $(this).parent().html(val.replace(/\n/gi, "<br />"));
            });

            <?php } else { ?>
            $(".staff_position").on('click', show_pos_drop_down);
            <?php } ?>
        });

        function fill_employed() {
            $(".staff select").each(function () {
                if (employed.hasOwnProperty($(this).val())) {
                    var place = $(this).parent().parent().attr('custom-rel');
                    var values = employed[$(this).val()];
                    for (i in values) {
                        if (values[i].length > 0) {
                            if (!$('tr[custom-rel=' + place + '] td.staff_position[custom-day=' + i + ']').hasClass('selected')) {
                                //$('tr[custom-rel='+place+'] td.staff_position[custom-day='+i+']').addClass('disabled').html(values[i]);
                            } else {
                                $('tr[custom-rel=' + place + '] td.staff_position[custom-day=' + i + ']').addClass('error').html(values[i]);
                            }
                        }
                    }
                }
            });
        }

        function save_monthly_roster() {
            $(this).html('Updating...').attr('disabled', 'disabled');
            var data = {
                'staff': [],
                'position': [],
                'projected': [],
                'action': 'update',
                'date': '<?php echo $current_date; ?>'
            }

            $('td.staff').each(function () {
                var place = $(this).parent().attr('custom-rel');
                data.staff[place] = $(this).children('select').val();
                data.position[place] = [];
                $(this).parent().children('td.staff_position').each(function () {
                    data.position[place][$(this).attr('custom-day')] = $(this).attr('custom-pos-id');
                });
            });

            $('.projected input').each(function () {
                var day = $(this).parent().attr('custom-day');
                data.projected[day] = $(this).val();
            });

            data.notes = $('#notes').val();

            $.ajax({
                'method': 'POST',
                'url': '<?php echo $PHP_SELF; ?>?<?php echo $params; ?>',
                'data': data,
                'success': function () {
                    window.alert('Information updated.');
                    document.location = '<?php echo $PHP_SELF; ?>?<?php echo $params; ?>';
                    //$('#update').html('Update');
                    //$('#update').attr('disabled', false);
                }
            });

        }

        function show_pos_drop_down(e) {
            $('#pos-drop').remove();
            if (e.target.nodeName != 'TD') return;
            if ($(this).hasClass('disabled')) return;
            var curr = $(this);
            var dd_div = $("<div id='pos-drop'>");
            dd_div.html("<ul><li custom-pos-id=''></li></ul>");
            for (i in positions) {
                if (!positions.hasOwnProperty(i)) continue;
                if (!positions[i].id) continue;
                dd_div.children("ul").append("<li " +
                    "custom-pos-id='" + positions[i].id + "'>" +
                    (positions[i].text ? positions[i].text : '') + '</li>');
            }

            var ot = curr.offset();
            dd_div.css({
                'position': 'absolute',
                'left': ot.left + 5,
                'top': ot.top + 18,
                'background-color': 'white'
            });

            curr.append(dd_div);

            $('#pos-drop li').click(on_select_pos);

            if (curr.hasClass('pos-selected')) {
                curr.removeClass('pos-selected');
            } else {
                curr.addClass('pos-selected');
            }
            if ($('.pos-selected').length == 0) $('#pos-drop').remove();
        }

        function on_select_pos(e) {
            if (e.target.nodeName != 'LI') return;
            var parent_td = $(this).parent().parent().parent();
            parent_td = $(".staff_position.pos-selected");
            var val = $(this).html();
            var cpos = $(this).attr('custom-pos-id');

            parent_td.each(function () {
                $(this).attr('custom-pos-id', cpos);
                $(this).removeClass('pos-selected');
                if (val == '') {
                    $(this).removeClass('selected');
                } else {
                    $(this).addClass('selected');
                }
                $(this).html(val);
            });
            for (var i = 1; i < <?php echo $month_days + 1; ?>; i++) {
                update_avail(i);
            }
            update_off();
        }

        function update_off(row) {
            var mdays = <?php echo $month_days; ?>;
            if (row) {
                var total = $("tr[custom-rel=" + row + "] > td.staff_position.selected, tr[custom-rel=" + row + "] > td.staff_position.disabled").length;
                $("tr[custom-rel=" + row + "] > td.off").html(mdays - total);
            } else {
                var i = 0;
                while ($("tr[custom-rel=" + i + "] > td.staff").length) {
                    var total = $("tr[custom-rel=" + i + "] > td.staff_position.selected, tr[custom-rel=" + i + "] > td.staff_position.disabled").length;
                    $("tr[custom-rel=" + i + "] > td.off").html(mdays - total);
                    i++;
                }
            }
        }

        function update_projected() {
            var daily = <?php echo json_encode($daily); ?>;
            var day = $(this).parent().attr('custom-day');
            var value = parseInt($(this).val());
            var current_key = {min_jumps: 0, max_jumps: 0, template_name: '', person: 0, template_color: ''}
            for (var j in daily) {
                if (daily.hasOwnProperty(j)) {
//alert(value + ' - ' + daily[j].min_jumps + ':' + daily[j].max_jumps);
                    if (value >= parseInt(daily[j].min_jumps) && value <= parseInt(daily[j].max_jumps)) {
                        current_key = daily[j];
                    }
                }
            }
            if (current_key.min_jumps == 0 && value > 0) {
                current_key = daily[0];
            }
//alert(value);
            if (value == 0) {
                current_key = {min_jumps: 0, max_jumps: 0, template_name: '', person: 0, template_color: ''}
            }
            // current key selected, just fill values
            //$("#daily"+day).html(current_key.template_name);
            $("#req" + day).html(current_key.person);
            $("#daily" + day + ", .projected" + day + ", #projected" + day + " input, #days-header-" + day).css('background-color', current_key.template_color);
        }
        function fill_projected() {
            var projected = <?php echo json_encode($projected); ?>;
            var daily = <?php echo json_encode($daily); ?>;
            for (var i = 1; i <= <?php echo $month_days; ?>; i++) {
                var day = new String(i);
                if (day.length < 2) day = '0' + day;
                var proj_field = $("<input>").prop('name', 'projected[' + i + ']');
                if (projected[day] > 0) {
                    proj_field.val(projected[day]);
                    $("#projected" + i).html(proj_field);
                    // if there is values, we need to fill them
                    var current_key = {jumps: 0, name: '', person: 0, color: ''}
                    for (var j in daily) {
                        if (daily.hasOwnProperty(j)) {
                            if (parseInt(projected[day]) >= parseInt(daily[j].min_jumps) && parseInt(projected[day]) <= parseInt(daily[j].max_jumps)) {
                                current_key = daily[j];
                            };
                        };
                    };
                    // current key selected, just fill values
                    //$("#daily"+i).html(current_key.template_name);
                    $("#req" + i).html(current_key.person);
                    $("#daily" + i + ", #projected" + i + ", #projected" + i + " input, #days-header-" + i).css('background-color', current_key.template_color);
                } else {
                    proj_field.val('');
                    $("#projected" + i).html(proj_field);
                }
            }
        }
        function update_avail(day) {
            var total_avail = 0;
            $("#avail" + day).val('');
            $("td.staff_position[custom-day=" + day + "]").each(function () {
                if ($(this).hasClass('selected')) {
                    var val = $(this).text();
                    var not_count = [
                        'OFC',
                        'TRN',
                        'TYO',
                        'MK',
                        'SG',
                        'IB',
                        'KY',
                        'NR',
                        'JPN',
                        'INTL',
                        'P&V1',
                        'P&V2',
                        'BM',
                        'CRD',
                        'OFC',
                        'CC',
                        'GI'
                    ];

                    if (not_count.indexOf(val) == -1) {
                        total_avail++;
                    }
                }
            });
            $("#avail" + day).html(total_avail);
            update_diff(day);
        }
        function update_diff(day) {
            var diff = 'N/A';
            try {
                var req = $("#req" + day).html();
                var avail = $("#avail" + day).html();
                if (req == '') {
                    req = 0;
                } else {
                    req = parseInt(req);
                }
                if (avail == '') {
                    avail = 0;
                } else {
                    avail = parseInt(avail);
                }
                if (typeof req == 'NaN' || typeof avail == 'NaN') {
                    diff = 0;
                } else {
                    diff = avail - req;
                };
            } catch (e) {
                diff = 'N/A';
            }
            $("#diff" + day).removeClass('lesser').removeClass('greater');
            if (diff > 0) {
                $("#diff" + day).addClass('greater');
            }
            if (diff < 0) {
                $("#diff" + day).addClass('lesser');
            }
            $("#diff" + day).html(diff);
        }
    </script>
</head>
<body>
<?php include 'includes/main_menu.php'; ?>
<table id="roster-table">
    <tr>
        <th colspan="<?php echo $month_days + 3; ?>">
            <h1><?php echo strtoupper(CURRENT_SITE_DISPLAY_NAME); ?> MONTHLY ROSTER
                <br/><?php echo strtoupper(date("F Y", $current_time)); ?></h1>
        </th>
    </tr>
    <tr>
        <th colspan="<?php echo $month_days + 3; ?>">
            <a href="?date=<?php echo $prev_date = date("Y-m", $current_time - 30 * 24 * 60 * 60); ?>" style="float: left;">&lt;&lt;&lt; <?php echo $prev_date; ?></a>
            <a href="?date=<?php echo $next_date = date("Y-m", $current_time + 30 * 24 * 60 * 60); ?>" style="float: right;"><?php echo $next_date; ?> &gt;&gt;&gt;</a>
        </th>
    </tr>
    <tr class="dates">
        <th>DATES</th>
        <?php for ($i = 1; $i <= $month_days; $i++) { ?>
            <th class="days-header" custom-day="<?php echo $i; ?>"><?php echo $i; ?></th>
        <?php } ?>
        <th>DATES</th>
        <th rowspan="2">OFF</th>
    </tr>
    <tr class="days">
        <th>DAYS</th>
        <?php for ($i = 1; $i <= $month_days; $i++) { ?>
            <td custom-day="<?php echo $i; ?>" id="days-header-<?php echo $i; ?>" class="days-header"><?php echo substr(date("D", $current_time - (15 - $i) * 24 * 60 * 60), 0, 1); ?></td>
        <?php } ?>
        <th>DAYS</th>
    </tr>
    <?php
    //prints the staff members rows
    foreach ($roster_staff as $place => $row) {
        echo "<tr custom-rel='$place' class='" . (($place % 2) ? 'even' : 'odd') . "'>\n";
        echo "\t<td class='staff'>" .
            draw_pull_down_menu("staff[$place]", $staff_names, (array_key_exists('staff_id', $row) ? $row['staff_id'] : 0)) .
            "</td>\n";
        for ($i = 1; $i <= $month_days; $i++) {
            $cday = $i;
            $position_id = '0';

            //Get the position if it exists in $roster_staff position array
            if (array_key_exists('positions', $row) && array_key_exists($cday, $row['positions'])) {
                $position_id = $row['positions'][$cday]['position_id'];
            }
            $position = (array_key_exists($position_id, $positions_by_id)) ? $positions_by_id[$position_id]['text'] : '';

            $add_class = empty($position) ? '' : ' selected';
            //$employed is an array of staff. Each staff entry has an array of dates and jump location ie: [1 => "MK", 2 => "SG"]
            //Dates that have no location are not in the array, ie: if staff member is nowhere on the 4th there will
            //be no entry at number 4.
            if (isset($employed[$row['staff_id']][$cday]) && $employed[$row['staff_id']][$cday] != '') {
                if (empty($add_class)) {
                    $add_class .= ' disabled';
                    $position = $employed[$row['staff_id']][$cday];
                }
            }
            echo "\t<td class='staff_position$add_class' custom-day='$i' custom-pos-id='$position_id'>" . $position . "</td>";
        }
        echo "\t<td class='staff'>" .
            draw_pull_down_menu("staff[$place]", $staff_names, (array_key_exists('staff_id', $row) ? $row['staff_id'] : 0)) .
            "</td>\n";
        echo "<td class='off'></td>";
        echo "</tr>\n";
    }
    ?>
    <tr class="separator">
        <th colspan="<?php echo $month_days + 3; ?>">&nbsp;</th>
    </tr>
    <?php
    $fields = array('PROJECTED', 'ACTUAL', 'REQ', 'AVAIL', 'DIFF');
    foreach ($fields as $field) {
        $lfield = strtolower($field);
        ?>
        <tr class="second-table <?php echo $lfield; ?>">
            <th><?php echo $field; ?></th>
            <?php for ($i = 1; $i <= $month_days; $i++) { ?>
                <td custom-day="<?php echo $i; ?>" id="<?php echo $lfield; ?><?php echo $i; ?>"><?php echo ($lfield == 'actual' && array_key_exists(sprintf("%02d", $i), $bookings)) ? $bookings[sprintf("%02d", $i)] : ''; ?></td>
            <?php } ?>
            <th><?php echo $field; ?></th>
            <th>&nbsp;</th>
        </tr>
        <?php
    }
    ?>
    <tr class="separator">
        <th colspan="<?php echo $month_days + 3; ?>">&nbsp;</th>
    </tr>
    <tr class="bottom-header">
        <th colspan="2" class='border2'>DAILY KEY</th>
        <th colspan="8" class='border2'>OPERATING HOURS</th>
        <th colspan="12" class='border2'>CALENDAR EVENTS</th>
        <th colspan="<?php echo $month_days - 19; ?>" class='border2'>NOTES</th>
    </tr>
    <?php
    $special_events = array();
    for ($i = 1; $i <= $month_days; $i++) {
        $event_date = $d . '-' . sprintf("%02d", $i);
        $sql = "SELECT * FROM calendar_events
			WHERE 
				site_id = '" . CURRENT_SITE_ID . "'
				and event_date <= '$event_date' 
				and event_finish_date >= '$event_date'
			order by all_day_event DESC, start_time ASC;";
        $res = mysql_query($sql) or die(mysql_error());
        while ($row = mysql_fetch_assoc($res)) {
            // short info
            $row['title'] = mb_convert_encoding($row['title'], 'UTF-8', 'SJIS');
            $row['description'] = mb_convert_encoding($row['description'], 'UTF-8', 'SJIS');
            $event_info = date("jS", mktime(0, 0, 0, substr($event_date, 5, 2), substr($event_date, 8, 2), substr($event_date, 0, 4)));
            $event_info .= ': ' . $row['title'];
            // full event info for tooltip
            $full_event_details = "<div id='tooltip-{$row['id']}'>";
            $full_event_details .= "<div class='event-title'>" . $row['title'] . "</div>";
            $full_event_details .= "<div class='event-date'>" . $row['event_date'];
            $full_event_details .= (($row['all_day_event']) ? ' (All day)' : ' (' . $row['start_time'] . ' - ' . $row['end_time'] . ')') . "</div>";
            $full_event_details .= "<div class='event-description'>" . str_replace(array("\n", "\r"), "", $row['description']) . "</div>";
            $full_event_details .= "</div>";
            $special_events[] = "<div id='event-{$row['id']}' onmouseover=\"tooltip.show('" . htmlspecialchars(str_replace("'", '"', $full_event_details)) . "');\" onmouseout=\"tooltip.hide();\">" . $event_info . "</div>";
        }
        mysql_free_result($res);
    }
    $prev_level = 120;
    $daily_rev = array_reverse($daily);
    $daily_rev = $daily;
    foreach ($daily_rev as $key => $level) {
        echo "\t<tr class='daily-table'>\n";
        echo "\t\t<td style='background-color: {$level['template_color']}'>{$level['template_name']} {$level['min_jumps']}-{$level['max_jumps']}</td>";
        echo "\t\t<td>{$level['person']}人</td>";
        echo "\t\t<td colspan='2'>{$level['start_time']}-{$level['end_time']}</td>";
        if ($key == 0) {
            echo "\t\t<td colspan='6' rowspan='" . count($daily_rev) . "'>*These hours are subject to change. Hours will be confirmed by the previous day</td>";
            echo "\t\t<td colspan='12' rowspan='" . (count($daily_rev) + 7) . "' class='border2' id='special-events'>" . implode("\n", $special_events) . "</td>";
            echo "\t\t<td colspan='" . ($month_days - 19) . "' rowspan='" . (count($daily_rev) + 7) . "' class='border2 notes-field'><textarea id='notes'>" . htmlspecialchars($notes) . "</textarea></td>";
        }
        echo "\t</tr>\n";
        $prev_level = $level['jumps'];
    }
    ?>
    <tr class='lunch'>
        <td colspan="10">LUNCH</td>
    </tr>
    <tr class="left border-bottom">
        <td>DAY</td>
        <td colspan="3">POSITION</td>
        <td colspan="3">WKDAY</td>
        <td colspan="3">WKEND</td>
    </tr>
    <tr class="left">
        <td>A+, A, B</td>
        <td colspan="3">RECEP.</td>
        <td colspan="3">Varied</td>
        <td colspan="3">Varied</td>
    </tr>
    <tr class="left border-bottom">
        <td>&nbsp;</td>
        <td colspan="3">OPS.</td>
        <td colspan="3">Varied</td>
        <td colspan="3">Varied</td>
    </tr>
    <tr class="left">
        <td>C, D, E</td>
        <td colspan="3">RECEP.</td>
        <td colspan="3">11:30 - 12:15</td>
        <td colspan="3">11:30 - 12:30</td>
    </tr>
    <tr class="left border-bottom">
        <td>&nbsp;</td>
        <td colspan="3">OPS.</td>
        <td colspan="3">12:15 - 1:00</td>
        <td colspan="3">12:00 - 1:00</td>
    </tr>
    <tr class="left border-bottom last-cell">
        <td colspan="10">** The JM can call lunch early, but
            <u>must</u> communicate with reception stating what time lunch will commence.
        </td>
    </tr>
</table>
<div id="buttons">
    <?php if (!$read_only) { ?>
        <button name='action' value='update' type="button" id="update">Update</button>
    <?php } ?>
    <button onClick="javascript: document.location='/'" id="back">Back</button>
    <button onClick="javascript: document.location='/Roster/index.php/?logout=true'" id="logout">Logout</button>
    <?php if (!$read_only) { ?>
        <button onClick="javascript: document.location='/ManageRoster/'" id="manage">Manage Rosters</button>
    <?php } ?>
</div>
<?php include("ticker.php"); ?>
</body>
</html>

<?php
//this has the side effect of kicking the user out if htey want to see any other roster page
//We make roster viewable for everyone on main, but if the users is not a GM, SM, SysAd log them out after viewing
//roster since the user
//$user = new BJUser();
//if(CURRENT_SITE_ID == 0) {
//    echo "main";
//    if ((!$user->hasRole('Site Manager') && !$user->hasRole('General Manager') && !$user->hasRole('SysAdmin'))) {
//        echo " unset";
//        session_destroy();
//        session_start();
//    }
//}

echo '';
?>
