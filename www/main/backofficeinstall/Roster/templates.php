<?php
	include '../includes/application_top.php';

	$action = $_POST['action'];
	switch ($action) {
		case 'update':
			Header("Location: /Roster/templates.php");
			die();
			break;
	};
	$roster_position = BJHelper::getListWithAdditionalFields('roster', 'position');
	// fill minimum staff fields
	$roster_staff = array();
	for ($i = 0; $i < 15; $i++) {
		$roster_staff[$i] = array();
	};
	// fill staff array
	$sql = "SELECT * FROM roster_staff WHERE roster_month = $cmonth ORDER BY place ASC;";
	$res = mysql_query($sql) or die(mysql_error());
	while ($row = mysql_fetch_assoc($res)) {
		$roster_staff[$row['place']] = $row;
	};
	mysql_free_result($res);
	// EOF FILL STAFF

	// fill STAFF POSITIONS
	$sql = "SELECT * 
		FROM roster_position rs
		WHERE 
			roster_month = $cmonth
		ORDER BY roster_day ASC;";
	$res = mysql_query($sql) or die(mysql_error());
	while ($row = mysql_fetch_assoc($res)) {
		$roster_staff[$row['place']]['positions'][$row['roster_day']] = $row;
	};
	mysql_free_result($res);
	// EOF fill STAFF POSITION

	// Load staff Names list
    $staff_names = array();
    $sql = "select * from users WHERE StaffListName <> '' order by StaffListName ASC;";
    $res = mysql_query($sql) or die(mysql_error());
    $staff_names[] = array(
        'id'    => '0',
        'text' => ''
    );
    while ($row = mysql_fetch_assoc($res)) {
        $staff_names[] = array(
            'id'    => $row['UserID'],
            'text' => $row['StaffListName'],
        );
    };
	// EOF load staff names list
	$filename = 'txt/position.txt';
	if (file_exists($filename)) {
		$v = explode("\n", file_get_contents($filename));
		array_walk($v, function (&$item) {$item = trim($item);});
		$position_list = array_map(function ($item) {
			return array(
				'id'    => $item,
				'text'  => $item
				);
		}, $v);
		$position_array = $v;
	};
	// load posiotion list
	// load targets
	$projected = array();
	$sql = "SELECT * FROM roster_target WHERE roster_date like '$d-%' ORDER BY roster_date ASC;";
	$res = mysql_query($sql) or die(mysql_error());
	while ($row = mysql_fetch_assoc($res)) {
		$projected[substr($row['roster_date'], 8, 2)] = $row['jumps'];
	};
	mysql_free_result($res);
	// load jumps if not set in targets
	$sql = "SELECT BookingDate, sum(NoOfJump) as projected FROM customerregs1 WHERE BookingDate like '$d-%' and DeleteStatus = 0 GROUP BY BookingDate ORDER BY BookingDate ASC;";
	$res = mysql_query($sql) or die(mysql_error());
	while ($row = mysql_fetch_assoc($res)) {
		$day = substr($row['BookingDate'], 8, 2);
		if (!array_key_exists($day, $projected)) {
			$projected[$day] = $row['projected'];
		};
	};
	mysql_free_result($res);
	for ($i = 1; $i <= $month_days; $i++) {
		$key = sprintf("%02d", $i);
		if (!array_key_exists($key, $projected)) $projected[$key] = 0;
	};
// Daily keys
	$daily = array (
		array(
			'jumps'	=> 1,
			'name'	=> 'E',
			'person'=> 5,
			'start'	=> '9:00',
			'end'	=> '18:00',
			'color'	=> '#900'
		),
		array(
			'jumps'	=> 31,
			'name'	=> 'D',
			'person'=> 6,
			'start'	=> '9:00',
			'end'	=> '18:00',
			'color'	=> '#C8A2C8'
		),
		array(
			'jumps'	=> 51,
			'name'	=> 'C',
			'person'=> 7,
			'start'	=> '9:00',
			'end'	=> '18:00',
			'color'	=> '#FBCEB1'
		),
		array(
			'jumps'	=> 66,
			'name'	=> 'B',
			'person'=> 7,
			'start'	=> '8:00',
			'end'	=> '18:00',
			'color'	=> 'yellow'
		),
		array(
			'jumps'	=> 81,
			'name'	=> 'A',
			'person'=> 8,
			'start'	=> '8:00',
			'end'	=> '18:00',
			'color'	=> 'red'
		),
		array(
			'jumps'	=> 101,
			'name'	=> 'A+',
			'person'=> 8,
			'start'	=> '7:30',
			'end'	=> '18:00',
			'color'	=> 'cyan'
		),
	);
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo SYSTEM_SUBDOMAIN; ?> MONTHLY ROSTER - <?php echo date("F Y", $current_time); ?></title>
<?php include "../includes/head_scripts.php"; ?>
	<script language="javascript" src="/js/tooltip.js"></script>
<style>
/* TOOLTIP */
#tt {position:absolute; display:block; background:url(/img/tt_left.gif) top left no-repeat}
#tttop {display:block; height:5px; margin-left:5px; background:url(/img/tt_top.gif) top right no-repeat; overflow:hidden}
#ttcont {display:block; padding:2px 12px 3px 7px; margin-left:5px; background:#666; color:#FFF}
#ttbot {display:block; height:5px; margin-left:5px; background:url(/img/tt_bottom.gif) top right no-repeat; overflow:hidden}
/* EOF TOOLTIP */
.vertical-align {
	display: block;
	margin: 0px;
	padding: 0px;
	font-size: 11px;
	font-family: Arial;
	font-weight: bold;
	width: 150px;
	left: -40px;
	position: relative;
-webkit-transform: rotate(90deg);	
-moz-transform: rotate(90deg);
-ms-transform: rotate(90deg);
-o-transform: rotate(90deg);
transform: rotate(90deg);
}
.va2 {
	display: block;
	margin: 0px;
	padding: 0px;
	font-size: 10px;
	font-family: Arial;
-webkit-transform: rotate(-90deg);	
-moz-transform: rotate(-90deg);
-ms-transform: rotate(-90deg);
-o-transform: rotate(-90deg);
transform: rotate(-90deg);
}
.small-header {
	font-size: 10px;
}
#roster-table {
	border:none;
	font-size: 11px;
	font-family: Arial;
	text-align: center;
	border-collapse: collapse;
}
#roster-table th {
	/*border: 1px solid black;
	border-collapse: collapse;*/
	background-color: white;;
	color: black;
	overflow: hidden;
	max-width: 60px;
	font-size: 11px;
	font-family: Arial;
	border: 1px solid black;
}
select {
	width: 99%;
	height: 99%;
	font-size: 10px;
	border: none;
	text-align: left;
	text-indent: 0px;
}
.BookingType, .CollectPay, .RateToPayQTY, .Rate, .Agent {
	min-width: 60px;
}
.Notes {
	min-width: 100px;
}
#roster-table td input {
	width: 99%;
	height: 99%;
	max-width: 100px;
	font-size: 10px;
	border: 1px solid gray;
	text-align: center;
	table-layout: fixed;
}
.days td {
	background-color: white;
	border: 1px solid black;
}
#roster-table td select option {
	background-color: white !important;
}
#roster-table .even td, #roster-table .even td *, #roster-table .odd td, #roster-table .odd td * {
	background-color: gray;
}
.even .staff, .even  .staff *, .even .selected, .even .selected * {
	background-color: lightyellow !important;
}
.odd .staff, .odd .staff *, .odd .selected, .odd .selected * {
	background-color: lightcyan !important;
}
#roster-table td select {
	width: 200%;
	padding-right: 0px;
}
#roster-table td.staff_position select {
	width: 300%;
	position: relative;
	left: -80%;
	text-align: center;
	padding-right: 0px;
}
#roster-table .second-table select {
	width: 300%;
	position: relative;
	left: -80%;
	text-align: center;
	padding-right: 0px;
}
#roster-table td {
	overflow: hidden;
	border: 1px solid black;
}
#roster-table .separator th {
	border-right: none !important;
	border-left: none !important;
	border-top: 2px solid black;
	border-bottom: 2px solid black;
}
.projected td, .projected td *, .daily td, .daily td * {
	background-color: silver;
}
.second-table input {
	border: none !important;
}
.diff td, .diff td * {
	background-color: lightgreen;
}
.greater, .greater * {
	background-color: #f9c !important;
}
.lesser, .lesser * {
	background-color: red !important;
}
.bottom-header th {
	border: 2px solid blzck !important;
}
tr.lunch td {
	background-color: black !important;
	color: white !important;
}
.left td {
	text-align: left;
	padding-left: 2px;
}
.border-bottom td {
	border-bottom: 2px solid black !important;
}
.last-cell td {
	padding-bottom: 50px;
}
.border2 {
	border: 2px solid black !important;
}
#special-events {
	vertical-align: top;
	text-align: left;
	padding-left: 5px;
}
</style>
<script>
$(document).ready(function () {
	//replace_td_select();
	$(".staff_position").on('click', function (e) {
		if (e.target.nodeName != 'TD') return;
		e.preventDefault();
		$(".staff_position > select").each(function () {
			$(this).change();
		});
       	var place = $(this).parent().attr('custom-rel');
       	var day = $(this).attr('custom-day');
       	var content = $(this).children('.value').html();
       	var sel = $('<select>').prop('name', 'position[' + place + '][' + day + ']');
       	var options = <?php echo json_encode($position_array); ?>;
       	for (var i in options) {
           	if (!options.hasOwnProperty(i)) {
               	continue;
           	}
           	var op = $('<option>').prop('value', options[i]).prop('text', options[i]);
           	sel.append(op);
       	};
       	if (content != '') {
           	sel.val(content);
           	$(this).addClass('selected');
       	}
		sel.on('change', function (e) {
			//e.preventDefault();
        	var val = $(this).val();
        	if (val == '') {
            	$(this).parent().removeClass('selected');
        	} else {
            	$(this).parent().addClass('selected');
        	};
			var d = $('<div>').addClass('value').html(val);
			var inp = $('<input>').prop('name', 'position[' + place + '][' + day + ']').prop('value', val).prop('type', 'hidden');
			var p = $(this).parent();
			p.html(d);
			p.append(inp);
        	update_aval(day);
        	update_off(place);
		});
       	$(this).html(sel);
		// emulate click
		sel = $(this).children('select');
		sel.css("position", "absolute");
		sel.css("zIndex", 9999);
		sel.css("width", 100);
		sel.css("height", 'auto');
		sel.offset($(this).position());
		sel.attr('size', sel.children('option').length - 2);
		sel.focus();
	});

	$(".staff select").change(function () {
		var val = $(this).val();
		$(this).parent().parent().children(".staff").each(function () {
			$(this).children("select").val(val);
		});
	});
	fill_projected();
	for (var i = 1; i < <?php echo $month_days + 1; ?>; i++) {
		update_aval(i);
		update_diff(i);
	}
	$(".projected input").keyup(update_projected);
	//$(".projected input").change(update_projected);
	// update OFF column
	update_off();
});
function update_off(row) {
	var mdays = <?php echo $month_days; ?>;
	if (row) {
		var total = $("tr[custom-rel="+row+"] > td.selected").length;
		$("tr[custom-rel="+row+"] > td.off").html(mdays - total);
	} else {
		var i = 0;
		while ($("tr[custom-rel="+i+"] > td.staff").length) {
			var total = $("tr[custom-rel="+i+"] > td.selected").length;
			$("tr[custom-rel="+i+"] > td.off").html(mdays - total);
			i++;
		};
	};
}
function update_projected() {
	var daily = <?php echo json_encode($daily); ?>;
	var day = $(this).parent().attr('custom-day');
	var value = $(this).val();
	var current_key = {jumps: 0, name: '', person: 0, color: ''};
	for (var j in daily) {
		if (daily.hasOwnProperty(j)) {
			if (value >= daily[j].jumps) {
				current_key = daily[j];
			};
		};
	};
	// current key selected, just fill values
 	$("#daily"+day).html(current_key.name);
	$("#req"+day).html(current_key.person);
	$("#daily"+day+", .projected"+day+", #projected"+day+" input, #days-header-"+day).css('background-color', current_key.color);
}
function fill_projected() {
	var projected = <?php echo json_encode($projected); ?>;
	var daily = <?php echo json_encode($daily); ?>;
	for (var i = 1; i <= <?php echo $month_days; ?>; i++) {
		var day = new String(i);
		if (day.length < 2) day = '0' + day;
		var proj_field = $("<input>").prop('name', 'projected['+i+']');
		if (projected[day] > 0) {
			proj_field.val(projected[day]); 
			$("#projected"+i).html(proj_field);
			// if there is values, we need to fill them
			var current_key = {jumps: 0, name: '', person: 0, color: ''};
			for (var j in daily) {
				if (daily.hasOwnProperty(j)) {
					if (parseInt(projected[day]) >= parseInt(daily[j].jumps)) {
						current_key = daily[j];
					};
				};
			};
			// current key selected, just fill values
			$("#daily"+i).html(current_key.name);
			$("#req"+i).html(current_key.person);
			$("#daily"+i+", #projected"+i+", #projected"+i+" input, #days-header-"+i).css('background-color', current_key.color);
		} else {
			proj_field.val('');
			$("#projected"+i).html(proj_field);
		};
	};
}
function update_aval(day) {
	var total_aval = 0;
	$("#aval"+day).val('');
	$("td.staff_position[custom-day="+day+"]").each(function () {
		if ($(this).children('.value').html() != '') total_aval++;
	});
	$("#aval"+day).html(total_aval);
	update_diff(day);
}
function update_diff(day) {
	var diff = 'N/A';
	try {
		var req = $("#req"+day).html();
		var aval = $("#aval"+day).html();
		if (req == '') {
			req = 0; 
		} else {
			req = parseInt(req);
		}
		if (aval == '') {
			aval = 0; 
		} else {
			aval = parseInt(aval);
		}
		if (typeof req == 'NaN' || typeof aval == 'NaN') {
			diff = 0;
		} else {
			diff = aval-req;
		};
	} catch (e) {
		diff = 'N/A';
	};
	$("#diff"+day).removeClass('lesser').removeClass('greater');
	if (diff > 0) {
		$("#diff"+day).addClass('greater');
	};
	if (diff < 0) {
		$("#diff"+day).addClass('lesser');
	};
	$("#diff"+day).html(diff);
}
</script>
</head>
<body>
<?php include 'includes/main_menu.php'; ?>
<form method="post" action="?date=<?php echo $current_date; ?>">
<table id="roster-table">
<tr>
	<th colspan="<?php echo $month_days+3; ?>">
		<h1><?php echo SYSTEM_SUBDOMAIN; ?> MONTHLY ROSTER <br /><?php echo strtoupper(date("F Y", $current_time)); ?></h1>
	</th>
</tr>
<tr>
    <th colspan="<?php echo $month_days+3; ?>">
		<a href="?date=<?php echo $prev_date = date("Y-m", $current_time - 30*24*60*60); ?>" style="float: left;">&lt;&lt;&lt; <?php echo $prev_date; ?></a>
		<a href="?date=<?php echo $next_date = date("Y-m", $current_time + 30*24*60*60); ?>" style="float: right;"><?php echo $next_date; ?> &gt;&gt;&gt;</a>
	</th>
</tr>
<tr class="dates">
	<th>DATES</th>
<?php for ($i = 1; $i <= $month_days; $i++) { ?>
	<th><?php echo $i; ?></th>
<?php }; ?>
	<th>DATES</th>
	<th rowspan="2">OFF</th>
</tr>
<tr class="days">
	<th>DAYS</th>
<?php for ($i = 1; $i <= $month_days; $i++) { ?>
	<td custom-day="<?php echo $i; ?>" id="days-header-<?php echo $i; ?>"><?php echo substr(date("D", $current_time - (15 - $i) * 24 * 60 * 60), 0, 1); ?></td>
<?php }; ?>
	<th>DAYS</th>
</tr>
<?php
	foreach ($roster_staff as $place => $row) {
		echo "<tr custom-rel='$place' class='".(($place % 2)?'even':'odd')."'>\n";
		echo "\t<td class='staff'>" . 
			draw_pull_down_menu("staff[$place]", $staff_names, (array_key_exists('staff_id', $row)?$row['staff_id']:0)) .
			"</td>\n";
		for ($i = 1; $i <= $month_days; $i++) {
			$position = '';
			$cday = $i;
			if (array_key_exists('positions', $row) && array_key_exists($cday, $row['positions'])) {
				$position = $row['positions'][$cday]['position'];
			};
			$add_class = empty($position) ? '' : ' selected';
			echo "\t<td class='staff_position$add_class' custom-day='$i'>" . 
				"<div class='value'>" . $position . "</div>" . 
				"<input name='position[$place][$day]' type='hidden' value='$position'>" . 
				"</td>";
		};
		echo "\t<td class='staff'>" .
			draw_pull_down_menu("staff[$place]", $staff_names, (array_key_exists('staff_id', $row)?$row['staff_id']:0)) .
			"</td>\n";
		echo "<td class='off'></td>";
		echo "</tr>\n";
	};
?>
<tr class="separator">
    <th colspan="<?php echo $month_days+3; ?>">&nbsp;</th>
</tr>
<?php
	$fields = array('PROJECTED', 'DAILY', 'REQ', 'AVAL', 'DIFF');
	foreach ($fields as $field) {
		$lfield = strtolower($field);
?>
<tr class="second-table <?php echo $lfield; ?>">
	<th><?php echo $field; ?></th>
<?php for ($i = 1; $i <= $month_days; $i++) { ?>
	<td custom-day="<?php echo $i; ?>" id="<?php echo $lfield; ?><?php echo $i; ?>"></td>
<?php }; ?>
	<th><?php echo $field; ?></th>
	<th>&nbsp;</th>
</tr>
<?php
	};
?>
<tr class="separator">
    <th colspan="<?php echo $month_days+3; ?>">&nbsp;</th>
</tr>
<tr class="bottom-header">
	<th colspan="2" class='border2'>DAILY KEY</th>
	<th colspan="8" class='border2'>OPERATING HOURS</th>
	<th colspan="12" class='border2'>SPECIAL EVENTS (Film crews, large groups etc)</th>
	<th colspan="<?php echo $month_days-19; ?>" class='border2'>TRAINING</th>
</tr>
<?php
	$special_events = array();
	$sql = "SELECT * FROM calendar_events WHERE event_date like '$d-%' order by event_date ASC, all_day_event DESC, start_time ASC;";
	$res = mysql_query($sql) or die(mysql_error());
	while ($row = mysql_fetch_assoc($res)) {
// short info
		$event_info = date("jS", mktime(0,0,0,substr($row['event_date'], 5,2), substr($row['event_date'], 8,2), substr($row['event_date'], 0,4)));
		$event_info .= ': ' . $row['title'];
// full event info for tooltip
		$full_event_details = "<div id='tooltip-{$row['id']}'>";
		$full_event_details .= "<div class='event-title'>" . $row['title'] . "</div>";
		$full_event_details .= "<div class='event-date'>" . $row['event_date'];
		$full_event_details .= (($row['all_day_event']) ? ' (All day)' : ' (' . $row['start_time'] . ' - ' . $row['end_time'] . ')') . "</div>";
		$full_event_details .= "<div class='event-description'>" . $row['description'] . "</div>";
		$full_event_details .= "</div>";
		$special_events[] = "<div id='event-{$row['id']}' onmouseover=\"tooltip.show('".htmlspecialchars(str_replace("'", '"', $full_event_details))."');\" onmouseout=\"tooltip.hide();\">" . $event_info . "</div>";
	};
	$prev_level = 120;
	$daily_rev = array_reverse($daily);
	foreach ($daily_rev as $key => $level) {
		echo "\t<tr class='daily-table'>\n";
		echo "\t\t<td style='background-color: {$level['color']};'>{$level['name']} {$level['jumps']}-$prev_level</td>";
		echo "\t\t<td>{$level['person']}人</td>";
		echo "\t\t<td colspan='2'>{$level['start']} - {$level['end']}</td>";
		if ($key == 0) {
			echo "\t\t<td colspan='6' rowspan='".count($daily_rev)."'>*These hours are subject to change. Hours will be confirmed by the previous day</td>";
			echo "\t\t<td colspan='12' rowspan='".(count($daily_rev)+7)."' class='border2' id='special-events'>".implode("\n", $special_events)."</td>";
			echo "\t\t<td colspan='".($month_days-19)."' rowspan='".(count($daily_rev)+7)."' class='border2'>".'show training events there'."</td>";
		};
		echo "\t</tr>\n";
		$prev_level = $level['jumps'];
	};
?>
<tr class='lunch'>
	<td colspan="10">LUNCH</td>
</tr>
<tr class="left border-bottom">
	<td>DAY</td>
	<td colspan="3">POSITION</td>
	<td colspan="3">WKDAY</td>
	<td colspan="3">WKEND</td>
</tr>
<tr class="left">
	<td>A+, A, B</td>
	<td colspan="3">RECEP.</td>
	<td colspan="3">Varied</td>
	<td colspan="3">Varied</td>
</tr>
<tr class="left border-bottom">
	<td>&nbsp;</td>
	<td colspan="3">OPS.</td>
	<td colspan="3">Varied</td>
	<td colspan="3">Varied</td>
</tr>
<tr class="left">
	<td>C, D, E</td>
	<td colspan="3">RECEP.</td>
	<td colspan="3">11:30 - 12:15</td>
	<td colspan="3">11:30 - 12:30</td>
</tr>
<tr class="left border-bottom">
	<td>&nbsp;</td>
	<td colspan="3">OPS.</td>
	<td colspan="3">12:15 - 1:00</td>
	<td colspan="3">12:00 - 1:00</td>
</tr>
<tr class="left border-bottom last-cell">
	<td colspan="10">** The JM can call lunch early, but <u>must</u> communicate with reception stating what time lunch will commence.</td>
</tr>
</table>
<input type="hidden" name='date' value='<?php echo $current_date; ?>'>
<button name='action' value='update'>Update</button>
</form>
<button onClick="javascript: document.location='/'" id="back">Back</button>
<?php include ("ticker.php"); ?>
</body>
</html>
