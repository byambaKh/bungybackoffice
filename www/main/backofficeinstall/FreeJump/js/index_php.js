$(document).ready(function () {
    $('.date-selection').datepicker({
        showAnim: 'fade',
        numberOfMonths: 1,
        //dateFormat: 'dd/mm/yy',
        dateFormat: 'yy-mm-dd (D)',
        currentText: 'Today',
        minDate: '0d',
        //minDate: new Date(2011, 12 , 31),
        maxDate: new Date(maxYear, maxMonth, maxDay),
        beforeShowDay: checkAvailability,
        onSelect: getBookTimes
    });
    $('.date-selection').each(function () {
        var d = new Date($(this).val());
        $(this).val($.datepicker.formatDate('yy-mm-dd (D)', d));
    });
    getBookTimes($('#jump-date-input').val());

    $('#free-jump-form').on('submit', function () {
        if ($("#jump-first-input").val() == '' || $("#jump-last-input").val() == '') {
            alert("You must type First and Last Name in order to register free jump.");
            return false;
        }
        ;
    });
});

function getBookTimes(bDate, inst) {
    bDate = bDate.replace(/([^-^0-9])/g, "");
    $.ajax({
        url: '../includes/ajax_helper.php',
        dataType: 'json',
        data: 'action=getBookTimes&bDate=' + encodeURIComponent(bDate) + '&regid=' + regid,
        success: function (data) {
            if (data['result'] == 'success') {
                var selected = $('#chkInTime').val();
                $('#chkInTime > option').remove();
                for (i in data['options']) {
                    if (data['options'].hasOwnProperty(i)) {
                        var option = $("<option></option>");
                        var oval = data['options'][i].value;
                        if (oval == "") {
                            option.val(oval).text(data['options'][i].text).appendTo('#chkInTime');
                            continue;
                        }
                        var avail = parseInt(data['options'][i].avail);
                        var otext = data['options'][i].value + ' (' + avail + ')';
                        if (data['options'][i].open == 0) {
                            otext = data['options'][i].value + ' (B)';
                        }
                        ;
                        option.prop('selected', (data['options'][i].value === selected) ? 'selected' : '');
                        if (data['options'][i].open == 0 || avail < -5) {
                            option.prop('disabled', 'disabled');
                        }
                        ;
                        option.attr('avail', avail);
                        option.val(oval).text(otext).appendTo('#chkInTime');
                    }
                }
                $('#chkInTime').change(function () {
                    var avail = $('#chkInTime > option:selected').attr('avail');
                    var size = parseInt(avail) + 6;
                    var selected = $('#noOfJump').val();
                    $('#noOfJump > option').remove();
                    for (var i = 1; i <= size; i++) {
                        var option = $("<option></option>");
                        option.val(i).text(i).appendTo('#noOfJump');
                    }
                    ;
                    $('#noOfJump').val(selected);
                });
                $('#chkInTime').change();
            }
        }
    });
}

var myBadDates = bDates;
var myDisabledDates = disabled_days;

function checkAvailability(mydate) {
    var r = true;
    var rc = "available";
    //$checkdate = $.datepicker.formatDate('dd/mm/yy', mydate);
    checkdate = $.datepicker.formatDate('yy-mm-dd', mydate);
    for (var i = 0; i < myBadDates.length; i++) {
        if (myBadDates[i] == checkdate) {
            r = false;
            rc = "unavailable";
            return [r, rc, "Dates Locked"];
        }
    }
    for (var i = 0; i < myDisabledDates.length; i++) {
        if (myDisabledDates[i] == checkdate) {
            r = false;
            rc = "unavailable";
            return [r, rc, "Free Jumps Locked"];
        }
    }
    return [r, rc];
}
