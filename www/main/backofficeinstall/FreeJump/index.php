<?php
include '../includes/application_top.php';

/*
Description
    This enables staff to book a limited number of free jumps. If the user tries to book more than is has been
    assigned to them, it will prevent them from doing so

Functionality & Tests Performed:
    - Booking a free jump
    - Trying to over book
    - Checking that the bookings were made correctly

Fixed issues :
    - PHPStorm Auto Reformat
    - extract css to File in /css/ current directory
    - File Encoding to UTF8 (Already)
    - Delete useless looking commented out code
    - Group code into functions
    - Removed the hardcoded country name from the file
    - extract js to file in /js/ current directory

Problems :
    - Contains mysql() functions
    - Some mixed PHP and HTML but not too bad
    - Moving Logo Images to img/china folder

Notes:
    For this to work locally mysql strictmode must be disabled.
    The query to show times fails if not done.
*/

$site_id = CURRENT_SITE_ID;

// get allowed jumps in current or selected month
/**
 * @return false|null|string|string[]
 */
function getBookingYear()
{
    if (isset($_POST['jump_date'])) {
        $cyear = preg_replace("/(([\d]{4}).+)/", "\\2", $_POST['jump_date']);
    } else {
        $cyear = date("Y");
    };
    return $cyear;
}

$cyear = getBookingYear();
// check allowed jumps count based on month
/**
 * @param $cyear
 * @param $userId
 * @return array
 */
function getJumpsAllowedForUser($cyear, $userId)
{
    $jumps_allowed = 0;
    $sql = "SELECT SUM(jumps) as total
		FROM free_jumps 
		WHERE 
			year = $cyear
			and user_id = {$userId};";
    $res = i_query($sql) or die(i_error());
    if ($row = i_fetch_assoc($res)) {
        $jumps_allowed = $row['total'];
    };

    i_free($res);
    return $jumps_allowed;
}

$userId = $_SESSION['uid'];
$jumps_allowed = getJumpsAllowedForUser($cyear, $userId);
/**
 * @param $cyear
 * @param $userId
 * @return array
 */
function getFreeJumpsBookedOnAllSitesForUserThisYear($cyear, $userId)
{
// All sites stats
    $jumps = array();
    $jumps_booked = 0;
    $jumps_exists_any = false;
// get already booked jumps count
    $sql = "SELECT 
			sum(IF(cr.DeleteStatus = 0, cr.NoOfJump, 0)) as total_active,
			sum(cr.NoOfJump) as total_booked
		FROM free_jumps_booked fjb
		LEFT JOIN customerregs1 cr on (fjb.regid = cr.CustomerRegID and cr.BookingDate like '$cyear-%')
		WHERE 
			fjb.user_id = {$userId}";
    $res = i_query($sql) or die(i_error());
    if ($row = i_fetch_assoc($res)) {
        $jumps_booked += (int)$row['total_active'];
        $jumps_exists = ((int)$row['total_booked'] > 0);
    };
    i_free($res);
    return array($jumps, $jumps_booked, $jumps_exists_any, $jumps_exists);
}

list($jumps, $jumps_booked, $jumps_exists_any, $jumps_exists) = getFreeJumpsBookedOnAllSitesForUserThisYear($cyear, $userId);

if ($jumps_exists) {
    $sql = "SELECT p.name as site, cr.*
           	FROM
               	free_jumps_booked fjb, customerregs1 cr, sites p
           	WHERE
               	fjb.user_id = '{$userId}'
               	and fjb.regid = cr.CustomerRegID
				and cr.BookingDate like '$cyear-%'
				and cr.site_id = p.id
           	ORDER BY BookingDate ASC, BookingTime ASC;";
    $res = i_query($sql) or die(i_error());
    while ($row = i_fetch_assoc($res)) {
        $jumps[$row['BookingDate'] . ' ' . $row['BookingTime']][] = $row;
    };
};

$jumps_exists_any = $jumps_exists_any || $jumps_exists;
ksort($jumps);

// Get disabled free jump days
/**
 * @param $cyear
 * @param $site_id
 * @return array
 */
function getDaysWhereFreeJumpsAreNotAllowed($cyear, $site_id)
{
    $disabled_days = array();
    $cdate = preg_replace("/([\d]{4})([\d]{2})/", "\\1-\\2", $cyear);
    $sql = "SELECT * FROM free_jumps_calendar WHERE site_id = '$site_id' AND fj_date like '$cdate-%' order by fj_date ASC;";
    $res = i_query($sql) or die(i_error());
    while ($row = i_fetch_assoc($res)) {
        if ($row['status'] == 0) {
            $disabled_days[] = $row['fj_date'];
        };
    };
    return $disabled_days;
}

$disabled_days = getDaysWhereFreeJumpsAreNotAllowed($cyear, $site_id);

/**
 * @param $disabled_days
 */
function checkBookingIsNotOnADisabledDay($disabled_days)
{
    if (in_array($_POST['jump_date'], $disabled_days)) {
        $_SESSION['free_jump_error'] = 'You cannot book on ' . $_POST['jump_date'] . '. This date is disabled for free jumps.';
        Header('Location: /FreeJump/');
        die();
    };
}

/**
 * @param $jumps_allowed
 * @param $jumps_booked
 */
function checkUserIsNotOutOfFreeJumps($jumps_allowed, $jumps_booked)
{
    if ($jumps_allowed - $jumps_booked < $_POST['jump_count']) {
        // throw error to user
        $_SESSION['free_jump_error'] = 'You cannot book ' . $_POST['jump_count'] . ' jumps. Maximum allowed ' . ($jumps_allowed - $jumps_booked);
        Header('Location: /FreeJump/');
        die();
    };
}

/**
 * @param $site_id
 */
function bookFreeJump($site_id)
{
    $data = array(
        'site_id' => $site_id,
        'BookingDate' => preg_replace("([^\d^-]+)", "", $_POST['jump_date']),
        'BookingTime' => $_POST['jump_time'],
        'NoOfJump' => $_POST['jump_count'],
        'RomajiName' => strtoupper($_POST['last_name']) . ' ' . strtoupper($_POST['first_name']),
        'CustomerLastName' => strtoupper($_POST['last_name']),
        'CustomerFirstName' => strtoupper($_POST['first_name']),
        'ContactNo' => $_POST['phone'],
        'BookingType' => 'Free Jump',
        'Rate' => 0,
        'CollectPay' => 'Onsite',
        'Agent' => 'NULL',
        'foc' => 'Bungy',
        'UserName' => $_SESSION['myusername'],
        'Notes' => $_POST['notes'] . " [ Free Jump by {$_SESSION['myusername']} " . date("Y/m/d") . " ] ",
    );
    db_perform('customerregs1', $data);
}

/**
 * @param $userId
 * @param $regid
 */
function insertFreeBookingUserIdAndBookingId($userId, $regid)
{
    $jb_data = array(
        'user_id' => $userId,
        'regid' => $regid
    );
    db_perform('free_jumps_booked', $jb_data);
}

/**
 * @param $jumps_exists_any
 * @param $jumps
 */
function drawJumpsBooked($jumps_exists_any, $jumps)
{
    if ($jumps_exists_any) {
        foreach ($jumps as $jumps_array) {
            foreach ($jumps_array as $row) {
                $add_class = '';
                if ($row['Checked']) {
                    $add_class .= ' checked';
                };
                if ($row['DeleteStatus']) {
                    $add_class = ' deleted';
                };
                echo "<tr><td colspan='2' class='booked$add_class'>{$row['BookingDate']} {$row['BookingTime']} [{$row['site']}] {$row['RomajiName']} ({$row['NoOfJump']})</td></tr>\n";
            };
        };
    };
}


if (isset($_POST['action'])) {
    switch ($_POST['action']) {
        case 'book-jump':
        case 'Book Jump':
            // check if user can book so many jumps
            checkUserIsNotOutOfFreeJumps($jumps_allowed, $jumps_booked);
            checkBookingIsNotOnADisabledDay($disabled_days);
            bookFreeJump($site_id);

            $regid = i_inserted_id();

            insertFreeBookingUserIdAndBookingId($userId, $regid);

            $_SESSION['free_jump_message'] = "You sucessfully booked " . $_POST['jump_count'] . " jumps.";
            Header('Location: /FreeJump/');

            die();
            break;
    };
};

$bDates = array();
$bDates = getCalendarState();

//Todo, move this in to functions
list($maxYear, $maxMonth, $maxDay) = explode(', ', $maxDate);


//$bDates = array_unique(array_merge($bDates, getCalendarStateFreeJumps()))
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo SYSTEM_SUBDOMAIN; ?> Free Jumps</title>
    <?php include "../includes/head_scripts.php"; ?>
    <script language="javascript" src="../js/tooltip.js"></script>
    <link rel="stylesheet" href="css/index_php.css" type="text/css" media="all">
    <style>
    </style>
    <script>
        var maxYear = <?= $maxYear ?>;
        var maxMonth = <?= $maxMonth ?>;
        var maxDay = <?= $maxDay ?>;
        var regid = 0;//This is never set and defaults to zero in the function called by ajax_helper.php
        var bDates = <?= json_encode($bDates) ?> ;
        var disabled_days = <?= json_encode($disabled_days) ?>;
    </script>

    <script src="js/index_php.js"></script>
</head>
<body>
<?php include '../includes/main_menu.php'; ?>
<div id="container">
    <img src="/img/index.jpg">
    <h1><?php echo SYSTEM_SUBDOMAIN; ?> Book Free Jump</h1>
    <div id="free-jump-container">
        <form id="free-jump-form" method="post">
            <table>
                <tr>
                    <td class="center">Jumps Available: <?php echo $jumps_allowed - $jumps_booked; ?></td>
                    <td class="center">Jumps Booked: <?php echo $jumps_booked; ?></td>
                </tr>
                <?php

                drawJumpsBooked($jumps_exists_any, $jumps);
                if ($jumps_allowed > $jumps_booked) {

                    ?>
                    <tr>
                        <td colspan="2"><h2>Book new jump</h2></td>
                    </tr>
                    <tr>
                        <td class="field-name">Jump Date:</td>
                        <td><input name="jump_date" value="<?php echo date("Y-m-d"); ?>" class="date-selection"
                                   id="jump-date-input"></td>
                    </tr>
                    <tr>
                        <td class="field-name">Jump Time:</td>
                        <td><select name="jump_time" id="chkInTime"/></td>
                    </tr>
                    <tr>
                        <td class="field-name">Jump count:</td>
                        <td>
                            <select name="jump_count" id="jump-count-select">
                            <?php
                            $i = 1;

                            while ($i <= ($jumps_allowed - $jumps_booked)) {
                                echo "\t\t\t\t<option value='$i'>$i</option>\n";
                                $i++;
                            }
                            ?>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td class="field-name">Last Name:</td>
                        <td><input name="last_name" id="jump-last-input"></td>
                    </tr>
                    <tr>
                        <td class="field-name">First Name:</td>
                        <td><input name="first_name" id="jump-first-input"></td>
                    </tr>
                    <tr>
                        <td class="field-name">Phone No:</td>
                        <td><input name="phone" id="jump-phone-input"></td>
                    </tr>
                    <tr>
                        <td class="field-name">Notes:</td>
                        <td><textarea name="notes" id="jump-notes-textarea"></textarea></td>
                    </tr>
                <?php }; ?>
                <tr>
                    <td align="left">
                        <button onClick="javascript: document.location='/'; return false;" id="back" type="button">
                            Back
                        </button>
                    </td>
                    <td align="right">
                        <?php if ($jumps_allowed > $jumps_booked) { ?>
                        <button name="action" value="book-jump" id="book-jump">Book Jump</button>
                    </td>
                    <?php }; ?>
                </tr>
            </table>
        </form>
    </div>
</div>
<br/>
<br/>
<?php include("ticker.php"); ?>
</body>
</html>
