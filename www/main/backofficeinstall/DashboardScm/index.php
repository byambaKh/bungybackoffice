<?php
    include '../includes/application_top.php';
    require("../Roster/functions.php");
$current_time = time();
$myDate = date('m',(strtotime ( '+1 day' , strtotime ( date('Y-m-d')) ) ));
$curMonth = date('m');

if (strtotime($myDate) == strtotime($curMonth)){
    $d = date('Y-m',(strtotime ( '+1 day' , strtotime ( date('Y-m-d')) ) ));
} else {
    $d = date('Y-m', $current_time);
}

$string = date('Y-m-d',(strtotime ( '+1 day' , strtotime ( date('Y-m-d')) ) ));
$timestamp = strtotime($string);
$date = date("d", $timestamp);
$minkbookings = getRosterBookings(1, $d);
$sarubookings = getRosterBookings(2, $d);
$ryujbookings = getRosterBookings(3, $d);
$itsubookings = getRosterBookings(4, $d);
$yambbookings = getRosterBookings(9, $d);
$narabookings = getRosterBookings(10, $d);
$fujibookings = getRosterBookings(12, $d);
$gifubookings = getRosterBookings(13, $d);

$allDataMina = getScmData(1);
$allDataSaru = getScmData(2);
$allDataRyuj = getScmData(3);
$allDataItsu = getScmData(4);
$allDataYamb = getScmData(9);
$allDataNara = getScmData(10);
$allDataFuji = getScmData(12);
$allDataGifu = getScmData(13);

?>

<html>
<head>
    <!-- Meta Values -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="Dashboard for Self Checkin Machines">
    <meta name="author" content="Bungee Japan">    

    <title>SCM - Dashboard</title>

    <!-- CSS -->
    <link rel="stylesheet" href="css/customize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="css/animate.css">
    <link href="css/plugins/iCheck/custom.css" rel="stylesheet">
    
    <meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate" />
	<meta http-equiv="Pragma" content="no-cache" />
	<meta http-equiv="Expires" content="0" />
</head>
<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui-1.10.4.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>
<script src="js/plugins/iCheck/icheck.min.js"></script>

<body>

  <!-- Header -->
  <div class="mishead">
      <div class="container">
          <h1 class="page-head text-center">SCM Overview</h1>
      </div>
  </div>

  <!-- Site Status -->
  <section id="site-status">
    <div class="container inner-sm">
        <div class="row">
            <div class="col-md-6">
                <div class="site">
                    <div class="text-center name"><span>Minakami | Bookings: <?php echo $minkbookings[$date]; ?><span></div>
                    <div class="machines">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Number</th>
                                <th>Status</th>
                                <th>Condition</th>
                                <th>Up-time</th>
                                <th>No. of restarts</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($allDataMina as $data): ?>

                            <tr>
                                <td class="text-center" ><?php echo $data['scm']; ?></td>
                                <?php if ($data['status']): ?>
                                <td class="text-center"><span class="label label-success" data-toggle="modal" data-target="#change" data-myvalue="Minakami" data-myvar="<?php echo $data['site_id']; ?>" data-scm="<?php echo $data['scm']; ?>"> Ok </span></td>
                                <?php else: ?>
                                <td class="text-center"><span class="label label-danger tool-ch" data-tooltip="tooltip" title="<?php echo $data['scm_reason']; ?>" data-toggle="modal" data-target="#change" data-myvalue="Minakami" data-myvar="<?php echo $data['site_id']; ?>" data-scm="<?php echo $data['scm']; ?>"> Error</span></td>
                                <?php endif; ?>
                                <?php if ($data['condition']): ?>
                                    <td class="text-center"><i class="fa fa-lg fa-lightbulb-o faa-vertical faa-flash green" aria-hidden="true"></i></td>
                                <?php else: ?>
                                    <td class="text-center"><i class="fa fa-lightbulb-o faa-vertical red" aria-hidden="true"></i></td>
                                <?php endif; ?>
                                <td class="text-center" ><?php echo $data['up_time']; ?> hrs</td>
                                <td class="text-center" ><?php echo $data['number_of_restarts']; ?></td>
                                <td class="text-left">
                                    <?php if ($data['status']): ?>
                                    <?php else: ?>
                                    <span class="label label-default tool-ch" data-tooltip="tooltip" title="<?php echo $data['description']; ?>" > <?php 
                                            $desc = preg_split('/[\s,]+/', $data['description'], 3); 
                                            $word = preg_replace('/(?<!\ )[A-Z]/', ' $0', $desc[1]);
                                            echo $desc[0]." ".$desc[1]; ?></span>   
                                    <?php endif; ?> 
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="site">
                    <div class="text-center name"><span>Sarugakyo | Bookings: <?php echo $sarubookings[$date]; ?><span></div>
                    <div class="machines">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Number</th>
                                <th>Status</th>
                                <th>Condition</th>
                                <th>Up-time</th>
                                <th>No. of restarts</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($allDataSaru as $data): ?>
                            <tr>
                               <td class="text-center" ><?php echo $data['scm']; ?></td>
                                <?php if ($data['status']): ?>
                                <td class="text-center"><span class="label label-success" data-toggle="modal" data-target="#change" data-myvalue="Sarugakyo" data-myvar="<?php echo $data['site_id']; ?>" data-scm="<?php echo $data['scm']; ?>"> Ok </span></td>
                                <?php else: ?>
                                <td class="text-center"><span class="label label-danger tool-ch" data-tooltip="tooltip" title="<?php echo $data['scm_reason']; ?>" data-toggle="modal" data-target="#change" data-myvalue="Sarugakyo" data-myvar="<?php echo $data['site_id']; ?>" data-scm="<?php echo $data['scm']; ?>"> Error</span></td>
                                <?php endif; ?>
                                <?php if ($data['condition']): ?>
                                    <td class="text-center"><i class="fa fa-lg fa-lightbulb-o faa-vertical faa-flash green" aria-hidden="true"></i></td>
                                <?php else: ?>
                                    <td class="text-center"><i class="fa fa-lightbulb-o faa-vertical red" aria-hidden="true"></i></td>
                                <?php endif; ?>
                                <td class="text-center" ><?php echo $data['up_time']; ?> hrs</td>
                                <td class="text-center" ><?php echo $data['number_of_restarts']; ?></td>
                                <td class="text-left">
                                    <?php if ($data['status'] == '0')
                                    {
                                    ?>
                                    <span class="label label-default tool-ch" data-tooltip="tooltip" title="<?php echo $data['description']; ?>" > <?php 
                                            $desc = preg_split('/[\s,]+/', $data['description'], 3); 
                                            $word = preg_replace('/(?<!\ )[A-Z]/', ' $0', $desc[1]);
                                            echo $desc[0]." ".$desc[1]; ?></span>   
                                    <?php }?> 

                                </td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="site">
                    <div class="text-center name"><span>Ryujin | Bookings: <?php echo $ryujbookings[$date]; ?><span></div>
                    <div class="machines">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Number</th>
                                <th>Status</th>
                                <th>Condition</th>
                                <th>Up-time</th>
                                <th>No. of restarts</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($allDataRyuj as $data): ?>
                            <tr>
                                 <td class="text-center" ><?php echo $data['scm']; ?></td>
                                <?php if ($data['status']): ?>
                                <td class="text-center"><span class="label label-success" data-toggle="modal" data-target="#change" data-myvalue="Ryujin" data-myvar="<?php echo $data['site_id']; ?>" data-scm="<?php echo $data['scm']; ?>"> Ok </span></td>
                                <?php else: ?>
                                <td class="text-center"><span class="label label-danger tool-ch" data-tooltip="tooltip" title="<?php echo $data['scm_reason']; ?>" data-toggle="modal" data-target="#change" data-myvalue="Ryujin" data-myvar="<?php echo $data['site_id']; ?>" data-scm="<?php echo $data['scm']; ?>"> Error</span></td>
                                <?php endif; ?>
                                <?php if ($data['condition']): ?>
                                    <td class="text-center"><i class="fa fa-lg fa-lightbulb-o faa-vertical faa-flash green" aria-hidden="true"></i></td>
                                <?php else: ?>
                                    <td class="text-center"><i class="fa fa-lightbulb-o faa-vertical red" aria-hidden="true"></i></td>
                                <?php endif; ?>
                                <td class="text-center" ><?php echo $data['up_time']; ?> hrs</td>
                                <td class="text-center" ><?php echo $data['number_of_restarts']; ?></td>
                                <td class="text-left">
                                    <?php if ($data['status'] == '0')
                                    {
                                    ?>
                                    <span class="label label-default tool-ch" data-tooltip="tooltip" title="<?php echo $data['description']; ?>" > <?php 
                                            $desc = preg_split('/[\s,]+/', $data['description'], 3); 
                                            $word = preg_replace('/(?<!\ )[A-Z]/', ' $0', $desc[1]);
                                            echo $desc[0]." ".$desc[1]; ?></span>   
                                    <?php }?>    
                                </td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="site">
                    <div class="text-center name"><span>Fuji | Bookings: <?php echo $fujibookings[$date]; ?><span></div>
                    <div class="machines">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Number</th>
                                <th>Status</th>
                                <th>Condition</th>
                                <th>Up-time</th>
                                <th>No.of restarts</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($allDataFuji as $data): ?>
                            <tr>
                                <td class="text-center" ><?php echo $data['scm']; ?></td>
                                <?php if ($data['status']): ?>
                                <td class="text-center"><span class="label label-success" data-toggle="modal" data-target="#change" data-myvalue="Fuji" data-myvar="<?php echo $data['site_id']; ?>" data-scm="<?php echo $data['scm']; ?>"> Ok </span></td>
                                <?php else: ?>
                                <td class="text-center"><span class="label label-danger tool-ch" data-tooltip="tooltip" title="<?php echo $data['scm_reason']; ?>" data-toggle="modal" data-target="#change" data-myvalue="Fuji" data-myvar="<?php echo $data['site_id']; ?>" data-scm="<?php echo $data['scm']; ?>"> Error</span></td>
                                <?php endif; ?>
                                <?php if ($data['condition']): ?>
                                    <td class="text-center"><i class="fa fa-lg fa-lightbulb-o faa-vertical faa-flash green" aria-hidden="true"></i></td>
                                <?php else: ?>
                                    <td class="text-center"><i class="fa fa-lightbulb-o faa-vertical red" aria-hidden="true"></i></td>
                                <?php endif; ?>
                                <td class="text-center" ><?php echo $data['up_time']; ?> hrs</td>
                                <td class="text-center" ><?php echo $data['number_of_restarts']; ?></td>
                                <td class="text-left">
                                     <?php if ($data['status'] == '0')
                                    {
                                    ?>
                                    <span class="label label-default tool-ch" data-tooltip="tooltip" title="<?php echo $data['description']; ?>" > <?php 
                                            $desc = preg_split('/[\s,]+/', $data['description'], 3); 
                                            $word = preg_replace('/(?<!\ )[A-Z]/', ' $0', $desc[1]);
                                            echo $desc[0]." ".$desc[1]; ?></span>   
                                    <?php }?>  
                                    </td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="site">
                    <div class="text-center name"><span>Itsuki | Bookings: <?php echo $itsubookings[$date]; ?><span></div>
                    <div class="machines">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Number</th>
                                <th>Status</th>
                                <th>Condition</th>
                                <th>Up-time</th>
                                <th>No. of restarts</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php foreach ($allDataItsu as $data): ?>
                            <tr>
                                <td class="text-center" ><?php echo $data['scm']; ?></td>
                                <?php if ($data['status']): ?>
                                <td class="text-center"><span class="label label-success" data-toggle="modal" data-target="#change" data-myvalue="Itsuki" data-myvar="<?php echo $data['site_id']; ?>" data-scm="<?php echo $data['scm']; ?>"> Ok </span></td>
                                <?php else: ?>
                                <td class="text-center"><span class="label label-danger tool-ch" data-tooltip="tooltip" title="<?php echo $data['scm_reason']; ?>" data-toggle="modal" data-target="#change" data-myvalue="Itsuki" data-myvar="<?php echo $data['site_id']; ?>" data-scm="<?php echo $data['scm']; ?>"> Error</span></td>
                                <?php endif; ?>
                                <?php if ($data['condition']): ?>
                                    <td class="text-center"><i class="fa fa-lg fa-lightbulb-o faa-vertical faa-flash green" aria-hidden="true"></i></td>
                                <?php else: ?>
                                    <td class="text-center"><i class="fa fa-lightbulb-o faa-vertical red" aria-hidden="true"></i></td>
                                <?php endif; ?>
                                <td class="text-center" ><?php echo $data['up_time']; ?> hrs</td>
                                <td class="text-center" ><?php echo $data['number_of_restarts']; ?></td>
                                <td class="text-left">
                                     <?php if ($data['status'] == '0')
                                    {
                                    ?>
                                    <span class="label label-default tool-ch" data-tooltip="tooltip" title="<?php echo $data['description']; ?>" > <?php 
                                            $desc = preg_split('/[\s,]+/', $data['description'], 3); 
                                            $word = preg_replace('/(?<!\ )[A-Z]/', ' $0', $desc[1]);
                                            echo $desc[0]." ".$desc[1]; ?></span>   
                                    <?php }?>  
                                        
                                    </td>
                            </tr>
                            <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="site">
                    <div class="text-center name"><span>Kaiun | Bookings: <?php echo $narabookings[$date]; ?><span></div>
                    <div class="machines">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Number</th>
                                <th>Status</th>
                                <th>Condition</th>
                                <th>Up-time</th>
                                <th>No.of restarts</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                            <tbody>

                            <?php foreach ($allDataNara as $data): ?>
                            <tr>
                               <td class="text-center" ><?php echo $data['scm']; ?></td>
                                <?php if ($data['status']): ?>
                                <td class="text-center"><span class="label label-success" data-toggle="modal" data-target="#change" data-myvalue="Kaiun" data-myvar="<?php echo $data['site_id']; ?>" data-scm="<?php echo $data['scm']; ?>"> Ok </span></td>
                                <?php else: ?>
                                <td class="text-center"><span class="label label-danger tool-ch" data-tooltip="tooltip" title="<?php echo $data['scm_reason']; ?>" data-toggle="modal" data-target="#change" data-myvalue="Kaiun" data-myvar="<?php echo $data['site_id']; ?>" data-scm="<?php echo $data['scm']; ?>"> Error</span></td>
                                <?php endif; ?>
                                <?php if ($data['condition']): ?>
                                    <td class="text-center"><i class="fa fa-lg fa-lightbulb-o faa-vertical faa-flash green" aria-hidden="true"></i></td>
                                <?php else: ?>
                                    <td class="text-center"><i class="fa fa-lightbulb-o faa-vertical red" aria-hidden="true"></i></td>
                                <?php endif; ?>
                                <td class="text-center" ><?php echo $data['up_time']; ?> hrs</td>
                                <td class="text-center" ><?php echo $data['number_of_restarts']; ?></td>
                                <td class="text-left">
                                     <?php if ($data['status'] == '0')
                                    {
                                    ?>
                                    <span class="label label-default tool-ch" data-tooltip="tooltip" title="<?php echo $data['description']; ?>" > <?php 
                                            $desc = preg_split('/[\s,]+/', $data['description'], 3); 
                                            $word = preg_replace('/(?<!\ )[A-Z]/', ' $0', $desc[1]);
                                            echo $desc[0]." ".$desc[1]; ?></span>   
                                    <?php }?>  
                                        
                                    </td>
                            </tr>
                            <?php endforeach; ?>
                           
                            </tbody>
                        </table>
                    </div>
                </div>
            </div> 
            <div class="col-md-6">
                <div class="site">
                    <div class="text-center name"><span>Yamba | Bookings: <?php echo $yambbookings[$date]; ?><span></div>
                    <div class="machines">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Number</th>
                                <th>Status</th>
                                <th>Condition</th>
                                <th>Up-time</th>
                                <th>No.of restarts</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                            <?php foreach ($allDataYamb as $data): ?>
                            <tr>
                                <td class="text-center" ><?php echo $data['scm']; ?></td>
                                <?php if ($data['status']): ?>
                                <td class="text-center"><span class="label label-success" data-toggle="modal" data-target="#change" data-myvalue="Yamba">Ok</span></td>
                                <?php else: ?>
                                <td class="text-center"><span class="label label-danger tool-ch" data-tooltip="tooltip" title="<?php echo $data['scm_reason']; ?>" data-toggle="modal" data-target="#change" data-myvalue="Itsuki">Error</span></td>
                                <?php endif; ?>
                                <?php if ($data['condition']): ?>
                                    <td class="text-center"><i class="fa fa-lg fa-lightbulb-o faa-vertical faa-flash green" aria-hidden="true"></i></td>
                                <?php else: ?>
                                    <td class="text-center"><i class="fa fa-lightbulb-o faa-vertical red" aria-hidden="true"></i></td>
                                <?php endif; ?>
                                <td class="text-center" ><?php echo $data['up_time']; ?> hrs</td>
                                <td class="text-center" ><?php echo $data['number_of_restarts']; ?></td>
                                <td class="text-center" >
                                     <?php if ($data['status'] == '0')
                                    {
                                    ?>
                                    <span class="label label-default tool-ch" data-tooltip="tooltip" title="<?php echo $data['description']; ?>" > <?php 
                                            $desc = preg_split('/[\s,]+/', $data['description'], 3); 
                                            $word = preg_replace('/(?<!\ )[A-Z]/', ' $0', $desc[1]);
                                            echo $desc[0]." ".$desc[1]; ?></span>   
                                    <?php }?>  
                                </td>
                            </tr>
                            <?php endforeach; ?>                           
                           
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="site">
                    <div class="text-center name"><span>Gifu | Bookings: <?php echo $gifubookings[$date]; ?><span></div>
                    <div class="machines">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Number</th>
                                <th>Status</th>
                                <th>Condition</th>
                                <th>Up-time</th>
                                <th>No.of restarts</th>
                                <th>Description</th>
                            </tr>
                            </thead>
                            <tbody>
                            
                            <?php foreach ($allDataGifu as $data): ?>
                            <tr>
                                <td class="text-center" ><?php echo $data['scm']; ?></td>
                                <?php if ($data['status']): ?>
                                <td class="text-center"><span class="label label-success" data-toggle="modal" data-target="#change" data-myvalue="Gifu">Ok</span></td>
                                <?php else: ?>
                                <td class="text-center"><span class="label label-danger tool-ch" data-tooltip="tooltip" title="<?php echo $data['scm_reason']; ?>" data-toggle="modal" data-target="#change" data-myvalue="Itsuki">Error</span></td>
                                <?php endif; ?>
                                <?php if ($data['condition']): ?>
                                    <td class="text-center"><i class="fa fa-lg fa-lightbulb-o faa-vertical faa-flash green" aria-hidden="true"></i></td>
                                <?php else: ?>
                                    <td class="text-center"><i class="fa fa-lightbulb-o faa-vertical red" aria-hidden="true"></i></td>
                                <?php endif; ?>
                                <td class="text-center" ><?php echo $data['up_time']; ?> hrs</td>
                                <td class="text-center" ><?php echo $data['number_of_restarts']; ?></td>
                                <td class="text-center" >
                                     <?php if ($data['status'] == '0')
                                    {
                                    ?>
                                    <span class="label label-default tool-ch" data-tooltip="tooltip" title="<?php echo $data['description']; ?>" > <?php 
                                            $desc = preg_split('/[\s,]+/', $data['description'], 3); 
                                            $word = preg_replace('/(?<!\ )[A-Z]/', ' $0', $desc[1]);
                                            echo $desc[0]." ".$desc[1]; ?></span>   
                                    <?php }?>  
                                </td>
                            </tr>
                            <?php endforeach; ?>                           
                           
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
  </section>

  <!-- Status Modal -->
  <div class="modal fade" id="change" tabindex="-1" role="dialog" aria-labelledby="change" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
          <form class="form-group" method="POST">    
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                <h4 class="modal-title" id="change"><span id="modal-myvalue"></span><span id="modal-scm" style="display: none;"></span></h4>
                <input type="hidden" name="site_id" id="hiddenID" value="">
                <input type="hidden" name="scm_name" id="hiddenScm" value="">                
            </div>
            <div class="modal-body">
                <section id="status">
                    <div class="row">
                        <div class="form-group"><label class="col-xs-12 control-label" style="margin-bottom:20px;"> Change Status of the machine</label>
                            <div class="col-xs-4">
                                <label><input type="radio" name="status" value="ok"> <i></i> OK </label>
                            </div>
                            <div class="col-xs-4">
                                <label><input type="radio" name="status" value="error"> <i></i> Error </label>
                            </div>
                            <div class="col-xs-4">
                                <label><input type="radio" name="status" value="na"> <i></i> N/A </label>
                            </div>
                        </div>
                        <div style="display:none" id="reason" class="error-form">
                            <div class="form-group">
                                <label for="res">Reason for Error:</label>
                                <!-- <input type="text" class="form-control" id="res">-->
                                <select name="reason">
                                    <option>Screen</option>                                        
                                    <option>Printer</option>
                                    <option>Cash box</option>
                                    <option>Won't start</option>
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="comment">Description:</label>
                                <textarea class="form-control" rows="5" id="desc" name="description"></textarea>
                            </div>
                        </div>
                </section>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="submit" class="btn btn-primary" name="saveReason" >Save Changes</button>
            </div>
          </form>      
        </div>
    </div>
  </div>  
  <script lanugage="javascript">

      // data-* attributes to scan when populating modal values
    var ATTRIBUTES = ['myvalue', 'myvar', 'scm'];

    $('[data-toggle="modal"]').on('click', function (e) {
      // convert target (e.g. the button) to jquery object
      var $target = $(e.target);
      // modal targeted by the button
      var modalSelector = $target.data('target');

      // iterate over each possible data-* attribute
      ATTRIBUTES.forEach(function (attributeName) {
        // retrieve the dom element corresponding to current attribute
        var $modalAttribute = $(modalSelector + ' #modal-' + attributeName);        
        var dataValue = $target.data(attributeName);    
        
        if(attributeName == 'scm')
        {
           document.getElementById("hiddenScm").value = dataValue;       
        }

        if(Number.isInteger(dataValue))
        {            
            document.getElementById("hiddenID").value = dataValue;                                     
        }

        // if the attribute value is empty, $target.data() will return undefined.
        // In JS boolean expressions return operands and are not coerced into
        // booleans. That way is dataValue is undefined, the left part of the following
        // Boolean expression evaluate to false and the empty string will be returned
        $modalAttribute.text(dataValue || '');
      });

      //var value = $('#hiddenID').val();
      var value = $('#modal-myvar').val();
      //alert(value);
    });
  </script>

  <?php
   if (isset($_POST['saveReason'])) {        

        $status = $_POST['status'];                
        $desc = mysql_real_escape_string($_POST['description']);
        $siteid = $_POST['site_id'];
        $reason = mysql_real_escape_string($_POST['reason']);
        $scm = $_POST['scm_name'];

        switch ($status) {
        case "ok":
            $status = "1";
            updateScmData($scm, $siteid, $status, 'null', '1', '');
            break;
        case "error":
            $statusupdateScmData = "0";
            updateScmData($scm, $siteid, $status, $reason, '0', $desc);
            break;
        case "na":
            $status = "2";
            updateScmData($scm, $siteid, $status, $reason, '1', '');
            break;    
        }   
       
       $page = $_SERVER['PHP_SELF'];
       echo "<meta http-equiv=\"refresh\" content=\"0;URL=".$page."\">";       
    }

  ?>

  <!-- Daily Report reading from text files -->
  <div id="daily-report" style="margin-bottom: 100px;">
    <div class="container">
      <h2 class="text-center"> Today's Update </h2>
      <div class="text-right">
         <button style="margin-bottom: 10px;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#report">Update Today's Report</button>
      </div>
      <pre id="fileDisplayUpdate"></pre>
      <h2 class="text-center"> Site Visit Schedule </h2>
      <div class="text-right">
         <button style="margin-bottom: 10px;" type="button" class="btn btn-primary" data-toggle="modal" data-target="#visit">Update Site Visit Schedule</button>
      </div>
      <pre id="fileDisplayArea"></pre>
    </div>
  </div>

  <!-- Modal for daily report-->
  <div class="modal fade" id="report" tabindex="-1" role="dialog" aria-labelledby="change" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                <h4 class="modal-title text-center" id="change">Update Today's Report</h4>
            </div>
            <div class="modal-body">
                <section id="update">
                    <div class="row">
                        <form class="form-group" method="POST">
                          <div id="reason" class="error-form">
                              <div class="form-group">
                                  <label for="res">Todays Update :</label>
                                  <textarea name="update1" class="form-control" rows="7" id="desc"></textarea>
                              </div>
                              <div class="text-center">
                                <button type="submit" name="submit1" class="btn btn-primary">Save Changes</button>
                              </div>
                          </div>
                        </form>
                      </div>
                </section>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
      </div>
  </div>

  <?php 
    if (isset($_POST['submit1'])) {
        $txt1 = $_POST['update1'] ;
        $date1 = date('M-d-Y'."\n");
        $myfile = fopen("report.txt", "w+") or die("Unable to open file!");
        fwrite($myfile, $date1);
        fwrite($myfile, $txt1);
        fclose($myfile);
    }
  ?>

  <!-- Modal for daily report-->
  <div class="modal fade" id="visit" tabindex="-1" role="dialog" aria-labelledby="change" aria-hidden="true">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true"><i class="fa fa-times"></i></span></button>
                <h4 class="modal-title text-center" id="change">Update Today's Report</h4>
            </div>
            <div class="modal-body">
                <section id="update">
                    <div class="row">
                        <form class="form-group" method="POST">
                          <div id="reason" class="error-form">
                            <div class="form-group">
                                <label for="comment">Site Visit Schedule :</label>
                                <textarea name="update2" class="form-control" rows="7" id="desc"></textarea>
                            </div>
                            <div class="text-center">
                                <button type="submit" name="submit2" class="btn btn-primary">Save Changes</button>
                            </div>
                          </div>
                        </form>
                      </div>
                </section>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
  </div>

   <?php 
    if (isset($_POST['submit2'])) {
        $txt2 = $_POST['update2'] ;
        $date2 = date('M-d-Y'."\n");
        $myfile = fopen("visit.txt", "w+") or die("Unable to open file!");
        fwrite($myfile, $date2);
        fwrite($myfile, $txt2);
        fclose($myfile);
    }
  ?>

  <!-- Script to read file from txt file to HTML -->
  <script>
    var fileDisplayUpdate = document.getElementById('fileDisplayUpdate');
    var fileDisplayArea = document.getElementById('fileDisplayArea');

    function readTextFile1(file)
    {
        var rawFile = new XMLHttpRequest();
        rawFile.open("GET", file, false);
        rawFile.onreadystatechange = function ()
        {
            if(rawFile.readyState === 4)
            {
                console.log("hi");
                if(rawFile.status === 200 || rawFile.status == 0)
                {
                    console.log("bye");
                    var allText = rawFile.responseText;
                    fileDisplayUpdate.innerText = allText;

                }
            }
        }
        rawFile.send(null);
    }
    function readTextFile2(file)
    {
        var rawFile = new XMLHttpRequest();
        rawFile.open("GET", file, false);
        rawFile.onreadystatechange = function ()
        {
            if(rawFile.readyState === 4)
            {
                if(rawFile.status === 200 || rawFile.status == 0)
                {
                    var allText = rawFile.responseText;
                    fileDisplayArea.innerText = allText;
                }
            }
        }
        rawFile.send(null);
    }
    readTextFile1("report.txt");
    readTextFile2("visit.txt");

  </script>
</body>
</html>