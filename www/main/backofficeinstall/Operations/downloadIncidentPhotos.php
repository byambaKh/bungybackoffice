<?php
require_once('../includes/application_top.php');
$upload = array();

if(isset($_GET['fileId'])){
    //get the file name from the
    //filter for the site so that someone cannot download files that do not belong to them by modifying the URL
    $fileSql = "SELECT * FROM uploads WHERE id = {$_GET['fileId']} AND site_id = ".CURRENT_SITE_ID.";";
    $upload = queryForRows($fileSql)[0];
}

if(count($upload)){
    $filename      = $upload['filename'];
    $fileDirectory = $upload['directory'];
    $storageName   = $upload['storageName'];
    $storageName   = $upload['storageName'];

    $filePath = $fileDirectory.$storageName;
    //get the file specified by the database

    header("Content-disposition: attachment; filename=".str_replace(" ", "_", $filename));
    header("Content-type: .zip    application/zip, application/octet-stream");
    readfile($filePath);
}
