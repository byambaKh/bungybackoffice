<?php
include '../includes/application_top.php';

mysql_set_charset('utf8');

if (isset($_GET['rid'])) {
    $reportDateOrId = $_GET['rid'];

} else if (isset($_GET['date'])) {
    $reportDateOrId = $_GET['date'];

} else $reportDateOrId = date('Y-m-d');

$report = new PlatformSafetyCheck(CURRENT_SITE_ID, $reportDateOrId);

if (array_key_exists('report', $_POST)) {
    $report->save($_POST['report']);
    if ($report->shouldSendEmail)
    {
       $report->sendReportEmailTo([$config['site_manager_email'], $config['general_manager_email']]);
       $report->sendReportEmailTo(["deji@standardmove.com"]); 

       $_SESSION['message'] = 'The report has been saved.'; 
    }       
}

$allowEdit = (isset($_GET['action']) && $_GET['action'] == 'view') ? true : false;
$staff_names = BJHelper::getStaffListByRolesAndSite('StaffListName',
    array("Staff +", "Site Manager"), CURRENT_SITE_ID, true);

$sessionMessage = '';
if (array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])) {
    $sessionMessage = "<div class='info_message'><span>{$_SESSION['message']}</span></div>";
    unset($_SESSION['message']);
};

?>
<!DOCTYPE html>
<html>
<head>
    <?php include 'includes/header_tags.php'; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=8">
    <script src="js/functions.js" type="text/javascript"></script>
    <?php include '../includes/head_scripts.php'; ?>
    <title>Operations</title>
    <style>

        .hide {
            display: none;
        }

        .answer-line {
            width: 700px; /* 100%; */
            background-color: #808080;
            float: left;
            text-align: left;
            padding-left: 40px;
            padding-right: 80px;
            padding-bottom: 40px;
            overflow: auto;
        }

        .answer-line textarea {
            width: 100%;
            vertical-align: top;
        }

        #total-container * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -ms-box-sizing: border-box;
            z-index: -100;
            font-family: helvetica, sans-serif;
            font-size: 10pt;
        }

        #total-container * {
            z-index: 100;
        }

        #total-container {
            width: 900px;
            text-align: center;
            margin-left: auto;
            margin-right: auto;
            position: relative;
        }

        #total-container div {
            /*height: 20px;*/
        }

        #total-container * input {
            /*
            width: 99%;
            height: 99%;
            */
            border: 1px solid #EEEEEE;
        }

        #total-container * textarea {
            width: 99%;
            height: 99%;
            border: 1px solid #EEEEEE;
            height: 180px;
        }

        #report-title {
            padding-top: 20px;
            padding-bottom: 40px;
            font-size: 16pt;
            font-weight: bold;
            text-decoration: underline;
            text-align: center;
        }

        .left {
            float: left;
            height: 20px;
        }

        #report-date {
            width: 80px;
        }

        #incident-date-value {
            width: 100px;
            height: 20px;
            margin-right: auto;
            float: left;
        }

        #incident-date-value input {
            margin-left: 0px !important;
        }

        #report-container {
            border: 1px solid black;
            margin: 0px;
            margin-top: 20px;
            padding: 0px;
        }

        #report-header {
            /* border: 1px solid black; */
            margin: 0px;
            padding: 0px;
            clear: both;
            height: 60px;
        }

        #total-container * .item-number {
            float: left;
            height: 100%;
            width: 40px;
            border-right: 2px solid black;
            font-size: 16px;
            line-height: 60px;
        }

        .item-value {
            float: left;
            height: 60px;
            width: 80px;
            border-left: 2px solid black;  
            margin-left: 20px;
            padding-top: 2px;
            border-right: 2px solid black;   
        }

        #report-header .item-value {
            padding-top: 10px;
            font-size: 18px;            
        }

        #staff-header {
            float: left;
            height: 60px;
            width: 200px;
            border-right: 1px solid black;
            padding-top: 10px;
        }

        #staff-inspector {
            float: left;
            height: 40px;
            width: 351px;
            padding-top: 4px;
            text-align: left;
            margin-left: 10px;
        }

        #staff-inspector select {
            font-size: 24px;
            height: 30px;
            width: 350px;
        }
        
        /* add - 2020-02-16 */
        #report-header .staff-names {
            padding-top: 10px;            
            height: 60px;            
            margin-left: 700px;            
        }

        #inspectorDDL {                       
            width: 150px;
            float: center;
            height: 30px;
            padding-top: 4px;
            text-align: left;
            margin-left: 5px;
            font-size: 20px;
        }
        /* end - 2020-02-16 */

        #staff-inspector select option {
            font-size: 24px;
        }

        #total-container * .item-value select {
            font-size: 20px;
            height: 30px;
            width: 75px;
            text-indent: 10px;
            padding: 0px;                                  
        }

        #total-container * .item-value select option {
            font-size: 26px;            
        }

        .report-line {
            border: 1px solid black;
            margin: 0px;
            padding: 0px;
            height: 42px;
            clear: both;
        }

        .extended-line {
            height: 200px !important;
        }

        .item-question-container {
            width: 560px;
            float: left;
        }

        .item-question-jp {
            float: left;
            width: 580px;
            border-bottom: 1px solid black;
            line-height: 20px;
            text-align: left;
            padding-left: 10px;            
        }

        .item-question-en {
            float: left;
            width: 580px;
            line-height: 20px;
            text-align: left;
            padding-left: 10px;            
        }

        .report-footer {
            border: 1px solid black;
            margin: 0px;
            padding: 0px;
            clear: both;
        }

        #report-notes-title {
            text-align: left;
            padding-left: 0.5%;
        }

        .required-items-list {
            margin-top: 42px;
            width: 100%;
            height: 200px;
            border-top: 2px solid black;
            padding-top: 5px;
        }

        .items-list {
            width: 200px;
            float: left;
            margin-left: 50px;
            text-align: left;
        }

        #buttons {
            clear: both;
            margin-top: 30px;
            margin-bottom: 60px;
            height: 49px;
            border: 1px solid #EEEEEE;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
        }

        #buttons button {
            float: right;
            background-color: #FFFF66;
            border: 1px solid black;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            font-size: 14pt;
            padding: 5px;
            margin: 5px;
            padding-left: 10px;
            padding-right: 10px;
        }

        #buttons #back {
            float: left;
        }

        .info_message {
            text-align: center;
            font-size: 1.5em;
            font-weight: 700;
            width: 100%;
            background-color: green;

        }

        .info-message span {
            height: 2em;
            margin-top: 0.25em;
        }

        <?php if (isset($_GET['action']) && $_GET['action'] == 'view') { ?>
        /*
        We should not be mixing CSS with PHP!!!!
        This can be removed by creating classes that are assigned to inputs based on
        the $_GET['action']

        if (isset($_GET['action']) && $_GET['action'] == 'view') {
            $hidden = 'hidden class name';
        }

        Then in the HTML/PHP...
        <button class = 'some classes and
        <?= '$hidden'?>
        '>button </button>
                */

        #buttons #submit {
            visibility: hidden;
        }

        * input:disabled, * textarea:disabled {
            background-color: #FEFEFE;
            color: black;
            opacity: 1;
        }

        <?php
        };
        ?>
    </style>
    <script>
        var today = "<?= date("Y-m-d")?>";
        function submitReport() {
        }

        $(document).ready(function () {

            if (<?=json_encode($allowEdit)?>) {
                $("input").prop('disabled', 'disabled');
                $("textarea").prop('disabled', 'disabled');
            }

            $('#back').click(function () {
                document.location = 'operationActivities.php?search_reports=1';
                return false;
            });

            $('#history').click(function () {
                window.open("psc_report_history.php",
                    "_blank",
                    "height=700, width=930, menubar=yes, titlebar=yes, scrollbars=yes, resizeable=yes, toolbar=yes");
                return false;
            });

            $('#submit').click(function () {
                document.empty_select = null;
                $("#psc-report-form select").each(function () {
                    //alert($(this).val());
                    if ($(this).val() == '') document.empty_select = $(this).prop('id');
                });
                //alert(document.empty_select);
                if (document.empty_select != null) {
                    alert('You must select all drop-downs value on this page');
                    return false;
                }
                $('#psc-report-form').submit();
                return true;
            });

            $('.resolve-issue').click(function (e) {
                //make an ajax request that submits the form via post to
                var questionId = $(this).attr('rel');
                $('#good-condition-select-' + questionId).val('y');
                $('#answer-line-' + questionId).hide(400, function () {});

                $(this).parent().children('.answer-date-value').val(today);
                var formdata = $('#psc-report-form').serialize();
                $.post('ajax.php?action=save-psc-report', $('#psc-report-form').serialize());
            });

            $('.yesNo').change(function () {
                var questionId = $(this).attr('rel');

                if (($(this).val() == "y") || ($(this).val().length == 0)) {//if there is no issue hide the reporting display box
                    $('#answer-line-' + questionId).hide(400, function () {});

                } else if ($(this).val() == "n") {//if there is an issue show it the report box
                    $('#answer-line-' + questionId).show(400, function () {});

                }
            });

            $("#incident-date-value input").datepicker({
                showAnim: 'fade',
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd',
                currentText: '<?= $report->details['date']?>',
                onSelect: function (d, inst) {
                    document.location = '/Operations/psc_report_plus.php?date=' + encodeURIComponent(d);
                }
            });

            //bind the date picker to the input boxes of answer inputs
            $(".answer-date-value").datepicker({
                showAnim: 'fade',
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd',
                currentText: ''

            });

        });
    </script>
</head>
<body>
<?php include '../includes/main_menu.php'; ?>
<br/>
<?= $sessionMessage ?>
<div id="total-container">
    <form id="psc-report-form" method="POST">
        <div id="report-title">Daily Checksheet</div>
        <div id="report-date" class="left">Report Date:</div>
        <div id="incident-date-value">
            <input type="text" name="<?php echo "report[details][date]" ?>" value="<?php echo $report->details['date'] ?>">
        </div>
        <button type="button" class="left" id="history">History</button>
        <br/>
        <br/>
        <?php 
            $Managers = BJHelper::getManagersByRole("Site Manager", true);             
        ?>
        <div id="report-container">
            <div id="report-header">
                <div class="item-number"></div>
                <div id="staff-header"> SM / JM name</div>
                <div id="staff-inspector"><?php echo draw_pull_down_menu("report[details][siteManagers]", $Managers, $report->details['siteManagers'], 'id="SiteManagers"'); 
                ?></div>
                <?= Form::hidden("report[details][site_id]", CURRENT_SITE_ID) ?>
                <div class="item-value">良・否</div>
                <div class="staff-names">点検実施者 <br> Inspecting Staff </div>                                
            </div>            
            <?= $report->render() ?>
            <div id="buttons">
                <button id="back">Back</button>
                <button id="submit">Save</button>
            </div>
        </div>
    </form>
</div>
</body>
</html>