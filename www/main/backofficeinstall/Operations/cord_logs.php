<?
include '../includes/application_top.php';

$cDate = date("Y-m-d");


?>
<!DOCTYPE html>
<html>
<head>
    <?php include 'includes/header_tags.php'; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=8">
    <link href="css/style.css" rel="stylesheet" media="screen" type="text/css"/>
    <script src="js/functions.js" type="text/javascript"></script>

    <?php include '../includes/head_scripts.php'; ?>
    <title>Operations :: Cord Logs</title>
    <script type="text/javascript" charset="utf-8">
        // Buttons actions
        $(document).ready(function () {
            $("#back").click(function () {
                document.location = 'operationActivities.php';
            });
            $("#new-log").click(function () {
                document.location = 'cord_logs_report.php';
            });
        });
    </script>
    <style>
        #cord-logs-table {
            text-align: center;
            background-color: white;
            width: 600px;
        }

        #cord-logs-table th {
            text-align: center;
            background-color: black;
            color: white;
        }

        #cord-logs-table td.bdate, #cord-logs-table th.bdate {
            text-align: right;
            padding-right: 10px;
        }

        #cord-logs-table td {
            background-color: red;
            text-align: center;
            color: white;
        }

        pre {
            margin: 0px;
        }

        #info_message {
            text-align: center;
        }
    </style>
</head>
<body>
<?php include '../includes/main_menu.php'; ?>
<br>
<?php
if (array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])) {
    echo "<div id='info_message'>{$_SESSION['message']}</div>";
    unset($_SESSION['message']);
};
?>
<form>
    <table align="center" border="0" width="550px">

        <tr>
            <td align="center">
                <?php
                ?>
                <div id="page-title">Cord Logs</div>
                <table cellspacing="1" cellpadding="3" id="cord-logs-table" align="center">
                    <tr>
                        <th>Test Date</th>
                        <th>Cord ID</th>
                        <!--<th>Date of 1st Use</th>-->
                        <th>Status</th>
                        <th>Total Jumps</th>
                        <th>Total U.V. Hours</th>
                        <th>Action</th>
                    </tr>
                    <?php
                    $sql = "SHOW TABLES LIKE 'cord_logs';";
                    $res = mysql_query($sql) or die(mysql_error());
                    $reports_found = false;
                    if (mysql_num_rows($res)) {
                        $where = '';
                        if (defined("CURRENT_SITE_ID") && CURRENT_SITE_ID > 0) {
                            $where = " WHERE site_id = '" . CURRENT_SITE_ID . "' ";
                        };
                        $sql = "select * from cord_logs $where ORDER BY status DESC, date DESC;";
                        $res = mysql_query($sql) or die(mysql_error());
                        while ($row = mysql_fetch_assoc($res)) {
                            $reports_found = true;
                            $file = "cord_logs_report.php";
                            if ($row['status'] == 1) {//if the cord is active, get the cord info from the daily reports
                                // get last values for active cords
                                $info = getLatestCordInfo($row['cid2'], $row['cid3']);
                                $row['total_jumps'] = ($info['data']['total']) ? $info['data']['total'] : 0;
                                $row['tuvh'] = ($info['data']['uvt_total']) ? $info['data']['uvt_total'] : 0;
                                //$row['total_jumps'] = $info['data']['total'];
                            };
                            ?>
                            <tr>
                                <td><?php echo $row['date']; ?></td>
                                <td><?php echo $row['cid1'] . $row['cid2'] . $row['cid3']; ?></td>
                                <!--<td><?= "Insert Date" ?></td>-->
                                <td><?php
                                    switch ($row['status']) {
                                        case 1:
                                            echo 'Active / 使用中';
                                            break;
                                        case 0:
                                            echo 'Retired / 使用済';
                                            break;
                                        case 2:
                                            echo 'Ready / 用意';
                                            break;
                                    };
                                    ?></td>
                                <td><?php echo $row['total_jumps']; ?></td>
                                <td><?php echo $row['tuvh']; ?></td>
                                <td>
                                    <a href="<?php echo $file; ?>?rid=<?php echo $row['id']; ?>&action=view">View</a>
                                    / <a href="<?php echo $file; ?>?rid=<?php echo $row['id']; ?>&action=edit">Edit</a>
                                </td>
                            </tr>
                            <?php
                        };
                    };
                    if (!$reports_found) {
                        echo "<tr><td colspan=\"7\">No cord logs found</td></tr>";
                    };

                    ?>
                    <tr>
                        <td colspan=7>
                            <table cellspacing="0" cellpadding="0" align="center" width="100%">
                                <tr>
                                    <td colspan="3" align="left">
                                        <button formaction="operationActivities.php" style="height: 40px;" id="back" type="button">Back</button>
                                    </td>
                                    <td colspan="4" align="right">
                                        <button formaction="cord_logs_report.php" type="button" id="new-log">Create New cord Log<br/>新規作成
                                        </button>
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>
                </table>
</form>
<?php
include("ticker.php");
?>
</body>
</html>
