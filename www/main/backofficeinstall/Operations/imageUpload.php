<?php
require_once('../includes/application_top.php');
$date = ifIsSet($_GET['date']);


//We need to get the date of the incident and site number
?>
<!DOCTYPE html>
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<link href="/js/dropzone/dropzone.min.css" rel="stylesheet" type="text/css">
		<script src="/js/dropzone/dropzone.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
        <?php
        //echo Html::css("bootstrap/3.3.2/css/bootstrap.min.css");
        ?>
		<title>Dropzone</title>
		<style>
			form{
				width: 100%;
				background-color: grey;
				border: 1px;
				border-color: black;
			}
		</style>
	</head>
	<body>
			<form action="uploadIncidentPhotos.php" method="post" enctype="multipart/form-data">
			<h1><?=SYSTEM_SUBDOMAIN_REAL?> Incident Report: <?=$date?></h1>
            <p>Please select a zip of all of the Photos</p>
            <p>Please wait for the success message before closing this window.</p>
			<!--<form action="upload.php" id="uploadForm">Form must have an action or this does not work-->
            <!--<h1>Drag &amp; Drop zip files and wait for upload to finish before closing this window</h1>-->
			<!--<input type="file" name="" accept="">-->
            <?php
                echo Form::upload("Upload", "uploadIncidentPhotos.php");
                echo Form::hidden("site_id", CURRENT_SITE_ID);
                echo Form::hidden("date", $date);
                echo Form::submitButton('submit');
            ?>

            <!--Dropzone will automatically create input elements-->
		</form>
	</body>
    <script>
		/*
        var myDropzone = new Dropzone("#uploadForm", {url: "/Operations/ajax.php?action=upload-incident-photo&date=<?=$date?>&site=<?=$currentSiteId?>"});
        //myDropzone.options.uploadMultiple = true;
        myDropzone.options.acceptedFiles		= ".zip";
        myDropzone.options.dictInvalidFileType	= "Only Zip Files May Be Uploaded";

        myDropzone.on("queuecomplete", function(file) {
			console.log("Turn the window green and show a success message");
            console.log('all files complete, time to tell the server to zip the photos and delete afterwards');
            return $.ajax(
                'ajax.php?action=finish-upload&date=<?=date?>site=<?=$currentSiteId?>',
                {
                    dataType: 'json',
                    type: 'POST',
                    async: false,
                    cache: false,
                    success: function (data) {
                        //show some kind of upload complete
                        return true;
                    }
                }
            );
        });

        myDropzone.on("totaluploadprogress", function(totalUploadProgress, totalBytes, totalBytesSent) {
            console.log('upload progress. ' + totalUploadProgress + " " + totalBytes + " " + totalBytesSent + " ");
            //this.emit("totaluploadprogress", totalUploadProgress, totalBytes, totalBytesSent);

        });
		*/
    </script>
</html>
