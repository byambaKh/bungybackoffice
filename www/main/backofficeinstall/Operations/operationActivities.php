<?php
include '../includes/application_top.php';
session_start();
$user = new BJUser();
$edit_reports = false;

if ($user->hasRole("Site Manager")
    || $user->hasRole("SysAdmin")
    || $user->hasRole("General Manager")
    || $user-> hasRole("JM")
    || in_array($user->getUserName(),  ["RomainD", "Bernard", "Manabu", "Shailesh", "Vladimir"])) 
{
    $edit_reports = true;
}

$cDate = date("Y-m-d");

function display_db_query($sql_select, $conn, $header_bool, $table_params) 
{
    $result_select = mysql_query($sql_select, $conn)
    or die("display_db_query:" . mysql_error());

    // find out the number of columns in result
    $column_count = mysql_num_fields($result_select)
    or die("display_db_query:" . mysql_error());

    print("<TABLE $table_params width='550px' align='center'>");
    // optionally print a bold header at top of table
    if ($header_bool) {

        print("<TR>");
        //print("<TH bgcolor='#87CEFA'></TH>");
        for ($column_num = 0; $column_num < $column_count - 4; $column_num++) 
        {
            $field_name = mysql_field_name($result_select, $column_num);
            print("<TH bgcolor='#87CEFA'>$field_name</TH>");
        }
        print("</TR>");
    }
    //Check ROW Count
    $num_rows = mysql_num_rows($result_select);
    if ($num_rows == 0) 
    {
        echo "<center><font color='#FF0000'>No Records Found!!!</font><center>";
    } 
    else 
    {
        //echo "<center><font color='#FF0000'>Click Check-Box to continue.</font><center>";
        //print ("<font color='#FF0000'><b>Records in color GREEN are already CHECKED-IN.</b></font>");
        while ($row = mysql_fetch_row($result_select)) 
        {
            //$_POST["bookingtime"] = $row[0];
            //$_SESSION["bookingtime"] = $_POST["bookingtime"];
            print("<TR ALIGN=CENTER VALIGN=TOP>");
            /*print("<TD style='background-color:#32CD32' valign='middle'>
                <a href='/Booking2/Reception/disclaimerExplainedIE.php?bookingtime=$row[0]&cname=$row[1]&jumpno=$row[2]'>CHECK IN</a></TD>");
*/            /*if($row[10] == '1'){
				print("<TD style='background-color:#008000' valign='middle'>
				<input type='checkbox' name='chks' value='$row[7]' disabled='disabled'></a></TD>");
			}elseif($row[7] == $rg[0]){
				print("<TD style='background-color:#00FF00' valign='middle'>
				<input type='checkbox' name='chks' value='$row[7]' checked='checked'></a></TD>");	
			}else{
				print("<TD style='background-color:#87CEFA' valign='middle'>
				<input type='checkbox' name='chkIN[]' value='$row[7]' onclick='process();'></a></TD>");		
			}*/

            for ($column_num = 0; $column_num < $column_count - 4; $column_num++) 
            {
                //echo $row[1];
                //echo "-----".$row[3].$column_num[4];
                //if($row[1].$column_num[2] == 'NULL NULL'){
                //print("<TD style='background-color:#FFFFFF' valign='middle' >$row[$column_num]</TD>");
                //}elseif ($row[3].$column_num[5] == '1'){
                //print("<TD style='background-color:#FF0000' valign='middle' >$row[$column_num]</TD>");
                //}else{
                //print("<TD style='background-color:khaki' valign='middle' >$row[$column_num]</TD>");
                //}
                if ($row[9] == '1') 
                {
                    print("<TD style='background-color:#008000' valign='middle'>$row[$column_num]</TD>");
                } 
                else 
                {
                    print("<TD style='background-color:#FF0000' valign='middle' ><font color='#FFFFFF'>$row[$column_num]</font></TD>");
                }

            }
            print("</TR>");

        }

    }

    print("</TABLE>");
}

function display_db_table($tablename, $conn, $header_bool, $table_params) {

    $nv = "NULL";
    $oDate = isset($_GET['oDate']) ? $_GET['oDate'] : date('Y-m-d');
    $sql_select_def = "SELECT bookingtime as 'Time',romajiname as 'Name', noofjump as 'Jumps Booked', /*agent as 'Agent', rate as 'Price',*/ photos as 'Photos',
							video as 'Video',
							Notes,
							/*collectpay as 'On/OffSite',*/ customerregid, customerlastname, customerfirstname, checked as 'CHK'" .
        " FROM customerregs1 " .
        " WHERE site_id = '" . CURRENT_SITE_ID . "'
						AND bookingdate = '" . $oDate . "'" .
        " and RomajiName != '" . $nv . "'" .
        " and deletestatus = 0 and noofjump > 0 order by bookingtime asc";
    //echo $sql_select_def;
    display_db_query($sql_select_def, $conn, $header_bool, $table_params);
    //}

}

?>

<html>
<head>
    <?php include '../includes/header_tags.php'; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=8">
    <link href="css/style.css" rel="stylesheet" media="screen" type="text/css"/>
    <script src="js/functions.js" type="text/javascript"></script>

    <?php include '../includes/head_scripts.php'; ?>
    <title>Operations</title>
    <script type="text/javascript" charset="utf-8">
        // Buttons actions
        $(document).ready(function () {
            $('#daily-report').on('click', function () {
                document.location = 'daily_report.php';
            });
            $('#incident-report').click(function () {
                document.location = 'incident_report_list.php';
            });
            $('#psc-report').click(function () {
                document.location = 'psc_report.php';
            });
            $('#psc-report-plus').click(function () {
                document.location = 'psc_report_plus.php';
            });
            $('#traffic-map').click(function () {
                document.location = 'traffic_map.php';
            });
            $('#weather-info').click(function () {
                document.location = 'weather_info.php';
            });
            $('#search-reports').click(function () {
                document.location = '/Operations/operationActivities.php?search_reports=1';
            });
            $('#cord-logs').click(function () {
                document.location = 'cord_logs.php';
            });
            $('#oDate').datepicker({
                showAnim: 'fade',
                numberOfMonths: 1,
                //dateFormat: 'dd/mm/yy',
                dateFormat: 'yy-mm-dd',
                currentText: 'Today',
                altField: '#alternate',
                altFormat: 'yy-mm-dd',
                minDate: '0d',
                maxDate: new Date(<?php echo $maxDate?>),
                onSelect: function (d, inst) {
                    document.location = '/Operations/operationActivities.php?oDate=' + encodeURIComponent(d);
                }
            });
            $('#rDate').datepicker({
                showAnim: 'fade',
                numberOfMonths: 1,
                //dateFormat: 'dd/mm/yy',
                dateFormat: 'yy-mm-dd',
                currentText: 'Today',
                maxDate: new Date(<?php echo $maxDate?>),
                onSelect: function (d, inst) {
                    document.location = '/Operations/operationActivities.php?search_reports=1&rDate=' + encodeURIComponent(d);
                }
            });

        });
        function OnSubmitForm() {
            if (document.getElementById('calendar')) {
                document.lsform.action = "operationActivities.php";
                document.lsform.submit();
                return true;
            }
        }
        function procBack() {
            document.location = '/';
            return;
        }
        function toggle(id) {
            var state = document.getElementById(id).style.display;
            if (state == 'block') {
                document.getElementById(id).style.display = 'none';
            } else {
                document.getElementById(id).style.display = 'block';
            }
        }
    </script>
    <style>
        #last-bookings-table {
            text-align: center;
            background-color: white;
            width: 300px;
        }

        #last-bookings-table th {
            text-align: center;
            background-color: black;
            color: white;
        }

        #last-bookings-table td.bdate, #last-bookings-table th.bdate {
            text-align: right;
            padding-right: 10px;
        }

        #last-bookings-table td {
            background-color: red;
            text-align: center;
            color: white;
        }

        pre {
            margin: 0px;
        }

        #info_message {
            text-align: center;
            width: 100%;
            background-color: greenyellow;
            height: 2em;
        }
    </style>
</head>
<body>
<?php include '../includes/main_menu.php'; ?>
<br>
<?php
if (array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])) {
    echo "<div id='info_message'>{$_SESSION['message']}</div>";
    unset($_SESSION['message']);
};
?>
<form method="post" name="lsform" onsubmit="return OnSubmitForm();">

<table border="0" align="center" width="420px">
    <tbody>
    <tr>
    </tr>
    <tr>
    </tr>
    <tr align="center" bgcolor="#000000">
        <td colspan="2"><font color="#FFFFFF"><b>Please select the activity from below:</b></font></td>
    </tr>
    <tr bgcolor="#FF0000">
        <td align="center" width="210" style="width: 210px;">
            <input style="WIDTH: 150px; HEIGHT: 25px; background-color:#FFFF66;border-style:ridge;border-color:#000000;" name="dam_info" id="damn_info" value="Dam Information" type="submit">
        </td>
        <td align="center" width="210" style="width: 210px;"><?php if ($edit_reports) { ?>
                <input style="WIDTH: 150px; HEIGHT: 25px; background-color:#FFFF66;border-style:ridge;border-color:#000000;" name="daily_report" id="daily-report" value="Daily Site Report" type="button"><?php } else { ?>&nbsp;<?php }; ?>
        </td>
    </tr>
    <tr bgcolor="#FF0000">
        <td align="center">
            <input style="WIDTH: 150px; HEIGHT: 25px; background-color:#FFFF66;border-style:ridge;border-color:#000000;" name="last_bookings" id="last-bookings" value="Bookings Received" type="submit">
        </td>
        <td align="center"><?php if ($edit_reports) { ?>
                <input style="WIDTH: 150px; HEIGHT: 25px; background-color:#FFFF66;border-style:ridge;border-color:#000000;" name="psc_report" id="psc-report-plus" value="Daily Checks" type="button"><?php } else { ?>&nbsp;<?php }; ?>
        </td>
    </tr>
    <tr bgcolor="#FF0000">
        <td align="center">
            <input style="WIDTH: 150px; HEIGHT: 25px; background-color:#FFFF66;border-style:ridge;border-color:#000000;" name="calendar" id="calendar" value="Daily View" type="submit">
        </td>
        <td align="center"><?php if ($edit_reports) { ?>
                <input style="WIDTH: 151px; HEIGHT: 25px; background-color:#FFFF66;border-style:ridge;border-color:#000000;" name="cord_logs" id="cord-logs" value="Cord Logs" type="button"><?php } else { ?>&nbsp;<?php }; ?>
        </td>
    </tr>
    <tr bgcolor="#FF0000">
        <td align="center">
            <input style="WIDTH: 150px; HEIGHT: 25px; background-color:#FFFF66;border-style:ridge;border-color:#000000;" name="traffic_map" id="traffic-map" value="Traffic map" type="button">
        </td>
        <td align="center"><?php if ($edit_reports) { ?>
                <input style="WIDTH: 150px; HEIGHT: 25px; background-color:#FFFF66;border-style:ridge;border-color:#000000;" name="incident_report" id="incident-report" value="Incident Report" type="button"><?php } else { ?>&nbsp;<?php }; ?>
        </td>
    </tr>
    <tr bgcolor="#FF0000">
        <td align="center">
            <input style="WIDTH: 150px; HEIGHT: 25px; background-color:#FFFF66;border-style:ridge;border-color:#000000;" name="weather_info" id="weather-info" value="Weather Info" type="button">
        </td>
        <td align="center">
            <input style="WIDTH: 150px; HEIGHT: 25px; background-color:#FFFF66;border-style:ridge;border-color:#000000;" name="search_reports" id="search-reports" value="Search Reports" type="button">
        </td>
    </tr>
    <!--
    <tr bgcolor="#FF0000">
        <td align="center">
        </td>
        <td align="center">
            <input style="WIDTH: 150px; HEIGHT: 25px; background-color:#FFFF66;border-style:ridge;border-color:#000000;" name="search_reports" id="psc-report-plus" value="Daily Checks Plus" type="button">
        </td>
    </tr>
    -->
    <tr bgcolor="#000000">
        <td align="center" colspan="2">
            <input style="WIDTH: 80px; HEIGHT: 25px; background-color:#FFFF66;border-style:ridge;border-color:#FF0000;" id="back" value="Back" onclick="procBack();" type="button">
        </td>
    </tr>

    </tbody>
</table>

<table align="center" border="0" width="550px">

<tr>
    <td align="center">
        <?php
        switch (TRUE) {
            case (isset($_POST['dam_info']) || isset($_GET['dam_info'])):
                echo "<br />";
                echo "<h1>Fujiwara Dam Information</h1>";
                echo "<table cellspacing='0' cellpadding='0' align='center'><tr><td valign='top'>";
                //echo "<img src='http://www3.ktr.mlit.go.jp/tonedamu/teikyo/damstss/fujiwara.png' align='center' />";
                echo "<img src='http://www.ktr.mlit.go.jp/tonedamu/teikyo/damstss/fujiwara.png' align='center' />";
                echo "<img src='http://www.ktr.mlit.go.jp/tonedamu/teikyo/damstss/aimata.png' align='center' />";
                echo "</td><td valign='top'>River level in Meters";
                $cache_filename = 'data/river_level.html';
                // 10 mins cache

                $rinfo = file_get_contents("http://www.uryou-gunma.jp/k/11040113.htm");
                //$rinfo = file_get_contents("http://www.ktr.mlit.go.jp/tonedamu/teikyo/visual/aimata.html");
                if (preg_match_all("/<pre>(.*?)<\/pre>/is", $rinfo, $matches)) {
                    $river_level_text = $matches[1][1];
                    file_put_contents($cache_filename, $river_level_text);
                } else {
                    $river_level_text = "<font color=red>Cannot retrive levels info</font>\n";
                    $river_level_text .= "<font color=red>Cached information:</font>\n";
                    $river_level_text .= file_get_contents($cache_filename);
                };
                $river_level_text = mb_convert_encoding($river_level_text, 'UTF-8', 'SJIS');
                $river_level_text = trim($river_level_text);
                $river_level_text = preg_replace("/\n([^\n]*\*\*[^\n]*)/", "", $river_level_text);
                echo '<pre>' . $river_level_text . '</pre>';
                echo "</td></tr></table>";
                break;
            case (isset($_POST['last_bookings']) || isset($_GET['last_bookings'])):
                $today = time();
                $start_date = date("Y-m-d", $today - 7 * 24 * 60 * 60);
                $end_date = date("Y-m-d", $today);
                $sql = "
						SELECT DATE(`BookingReceived`) AS bDate,
  								count(*) AS total_records,
  								sum(NoOfJump) AS total_jumps
  						FROM `customerregs1` 
  						WHERE 
							site_id = '" . CURRENT_SITE_ID . "'
							AND `BookingReceived` BETWEEN '" . $start_date . " 00:00:00' AND '" . $end_date . " 00:00:00'
							AND NoOfJump > 0
							AND DeleteStatus = 0
  						GROUP BY bDate
						ORDER BY bDate";
                $res = mysql_query($sql);
                ?>
                <table cellspacing="1" cellpadding="3" id="last-bookings-table" align="center">
                    <tr>
                        <th>Date</th>
                        <th>Records</th>
                        <th>Jumps</th>
                    </tr>
                    <?php
                    $values = array();
                    $totals = array(
                        'records' => 0,
                        'jumps' => 0
                    );
                    while ($row = mysql_fetch_assoc($res)) {
                        $values[$row['bDate']] = $row;
                    };
                    for ($i = 1; $i < 8; $i++) {
                        $current_date = date("Y-m-d", $today - $i * 24 * 60 * 60);
                        $display_date = date("D d M", $today - $i * 24 * 60 * 60);
                        $current_records = $current_jumps = 0;
                        if (array_key_exists($current_date, $values)) {
                            $current_records = $values[$current_date]['total_records'];
                            $current_jumps = $values[$current_date]['total_jumps'];
                        };
                        $totals['records'] += $current_records;
                        $totals['jumps'] += $current_jumps;
                        ?>
                        <tr>
                            <td class="bdate"><?php echo $display_date; ?>:</td>
                            <td class="brec"><?php echo $current_records; ?></td>
                            <td class="bjump"><?php echo $current_jumps; ?></td>
                        </tr>
                    <?php
                    };
                    ?>
                    <tr>
                        <th class="bdate">Total:</th>
                        <th><?php echo $totals['records']; ?></th>
                        <th><?php echo $totals['jumps']; ?></th>
                    </tr>
                </table>
                <?php
                break;
            case (isset($_GET['search_reports'])):
                if (!isset($_GET['rDate'])) {
                    $report_date = date('Y-m-d');
                } else {
                    $report_date = $_GET['rDate'];
                };
                ?>
                <div id="page-title">Reports search</div>
                <div id="reports-date"><input type="text" value="<?php echo $report_date; ?>" name="rDate" id="rDate">
                </div>
                <table cellspacing="1" cellpadding="3" id="last-bookings-table" align="center">
                    <tr>
                        <th>Type</th>
                        <th>ID</th>
                        <th>Date</th>
                        <th>Actions</th>
                    </tr>
                    <?php
                    $where = '';
                    if (defined("CURRENT_SITE_ID") && CURRENT_SITE_ID > 0) {
                        $where = " site_id = '" . CURRENT_SITE_ID . "' AND ";
                    };

                    $sql = "select 'Daily' as report_type, id, `date` from daily_reports WHERE $where `date` = '$report_date'
	UNION 	select 'Incident' as report_type, id, `date` from incident_reports WHERE $where `date` = '$report_date'
	UNION 	select 'PSC' as report_type, id, `date` from psc_reports WHERE $where `date` = '$report_date'
	UNION   select 'Checks' as report_type, id, `date` from psc_report_details WHERE $where  `date` = '$report_date';";
                    $res = mysql_query($sql) or die('No reports found');
                    $reports_found = false;
                    while ($row = mysql_fetch_assoc($res)) {
                        $reports_found = true;
                        switch ($row['report_type']) {
                            case 'Daily':
                                $file = 'daily_report.php';
                                break;
                            case 'Incident':
                                $file = 'incident_report.php';
                                break;
                            case 'PSC':
                                $file = 'psc_report.php';
                                break;
                            case 'Checks':
                                $file = 'psc_report_plus.php';
                                break;
                            default:
                                $file = 'daily_report.php';
                        };
                        ?>
                        <tr>
                            <td><?php echo $row['report_type']; ?></td>
                            <td><?php echo $row['id']; ?></td>
                            <td><?php echo $row['date']; ?></td>
                            <td>
                                <a href="<?php echo $file; ?>?rid=<?php echo $row['id']; ?>&action=view">View</a> <?php if ($edit_reports) { ?>
                                / <a href="<?php echo $file; ?>?rid=<?php echo $row['id']; ?>&action=edit">Edit</a> <?php }; ?>
                            </td>
                        </tr>
                    <?php
                    };
                    if (!$reports_found) {
                        echo "<tr><td colspan=\"4\">No reports found for this date</td></tr>";
                    };
                    ?>
                </table>
                <?php
                break;
            default:
                $oDate = isset($_GET['oDate']) ? $_GET['oDate'] : date('Y-m-d');
                $sql_am = "SELECT count(*) as total, sum(noofjump) as total_jumps " .
                    " FROM customerregs1 " .
                    " WHERE
							site_id = '" . CURRENT_SITE_ID . "'
							and bookingdate = '" . $oDate . "'" .
                    " and RomajiName != 'NULL'
                    	  and bookingtime >= '07:00 AM' and bookingtime <= '12:30 PM' " .
                    " and deletestatus = 0 and noofjump > 0 group by bookingdate";
                $res_am = mysql_query($sql_am) or die(mysql_error());
                $text_am_bookings = "No recs";
                if ($row = mysql_fetch_assoc($res_am)) {
                    $text_am_bookings = "{$row['total_jumps']}";
                };
                $sql_pm = "SELECT count(*) as total, sum(noofjump) as total_jumps " .
                    " FROM customerregs1 " .
                    " WHERE
							site_id = '" . CURRENT_SITE_ID . "'
							and bookingdate = '" . $oDate . "'" .
                    " and RomajiName != 'NULL'
                    	  and bookingtime >= '13:00 PM' and bookingtime <= '18:30 PM' " .
                    " and deletestatus = 0 and noofjump > 0 group by bookingdate";
                $res_pm = mysql_query($sql_pm) or die(mysql_error());
                $text_pm_bookings = "No recs";
                if ($row = mysql_fetch_assoc($res_pm)) {
                    $text_pm_bookings = "{$row['total_jumps']}";
                };

                echo "<div style=\"text-align: center; margin-bottom: 10px; height: 113px;\">
	<br />
	<div style=\"width: 406px; text-align: center; background-color: black; color: white;margin-left: auto; margin-right: auto; padding: 5px; margin-bottom: 2px;\">
		Daily View
	</div>
	<div style=\"width: 406px; text-align: center; background-color: red; color: white;margin-left: auto; margin-right: auto; padding: 5px; margin-bottom: 2px;\">
		Please select date: <input name=\"oDate\" id=\"oDate\" value=\"$oDate\">
	</div>
	<div style=\"width: 416px; background-color: white; margin-left: auto; margin-right: auto; padding: 0px; color: white;\">
		<div style=\"width: 196px; background-color: red; float: left; margin-right: 2px; padding: 5px;\">AM Bookings: $text_am_bookings</div>
		<div style=\"width: 198px; background-color: red; float: left; padding: 5px;\">PM Bookings: $text_pm_bookings</div>
	</div>
</div>";
                $table = "table1";
                display_db_table($table, $conn, TRUE, "border='0'");
                break;
        };
        ?>


    </td>
</tr>
</table>

</form>
</body>

</html>
<?php
include("ticker.php");
?>
