<?php
/**
 * This function will take a table name ($table) and an array ($report) and store the contents in the table.
 *
 * If the table does not exist it will create a new table and store the information there, auto generating the columns
 * from the column names in the associative array. However all of these will be text fields.
 *
 * I think that this is inventive but lazy programming and a very bad idea as you should know what you are saving and where it is
 * going. A wrongly generated field name will result in adding a new column to the table.
 *
 * @param $table the name of the table that will data will be inserted in to
 * @param $report the data in the form of an associative array with column names and values
 * @return $value the value returned from db_perform() on the data
 */
function save_report($table, $report) {
	// check if table exists
	$sql = "show tables like '".$table."'";
	$res = mysql_query($sql);
	if ($table_info = mysql_fetch_assoc($res)) {
		//print_r($table_info);
	} else {
		$sql = "CREATE TABLE $table (
			`id`	INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY comment 'primary id for reports',
			`date`	DATE NOT NULL,
			INDEX idx_date (`date`)
		) CHARACTER SET 'utf8'";
		mysql_query($sql) or die(mysql_error());
	};
	// check if all table fields exists
	$sql = "explain $table;";
	$res = mysql_query($sql) or die(mysql_error());
	$efields = array();
	while ($row = mysql_fetch_assoc($res)) {
			$efields[] = $row['Field'];
	};
	// needed fields
	$nfields = array_keys($report);
	// non existing fields
	$dfields = array();
	foreach($nfields as $field) {
		if (!in_array($field, $efields)) {
			// if there is not existing fields, create them
			$dfields[] = $field;
			$sql = "ALTER TABLE $table ADD `$field` TEXT NOT NULL DEFAULT '';";
			mysql_query($sql) or die(mysql_error());
		};
	};
	// check if date fields exists
	if (!in_array('date', $nfields)) {
		$report['date'] = date("Y-m-d");
	};
	if (in_array('id', $nfields) && !empty($report['id'])) {
		$action = 'update';
		$where = 'id = ' . (int)$report['id'];
	} else {
		$action = 'insert';
		$where = '';
	}; 
	return db_perform($table, $report, $action, $where);
}
?>
