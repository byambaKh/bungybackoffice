<?php
require_once('../includes/application_top.php');

$sql = "
SELECT prd.date, siteManagers, description, reportId, questionId, prh.date AS dateOpened, prd.date AS dateClosed FROM psc_report_details AS prd
	INNER JOIN psc_report_histories AS prh ON (prd.id = prh.reportId)
	WHERE site_id = $currentSiteId
	ORDER BY prh.date;
";

$histories = queryForRows($sql);
echo Html::head("Platform Safety Check Report History", array("operations.css"));
?>
<div>
    <h1 class="centered">Platform Safety Check History</h1>
    <?php
    if(count($histories) == 0) {
        echo "<h3 class='centered'>No Issues Have Been Closed Yet.</h3>";
    } else {
    ?>    
    <table id="psc-history">
        <tr>
            <td><h3>Date Opened</h3></td>
            <td><h3>Date Closed</h3></td>
            <td><h3>Site Manager</h3></td>
            <td><h3>Issue</h3></td>
            <td><h3>Link</h3></td>
        </tr>
        <?php
        foreach ($histories as $history) {
            $link = '';
            if ($history['reportType'] == 'PSC') {
                $link = "psc_report.php?rid={$history['id']}";

            } else if ($history['reportType'] == 'Check') {
                $link = "}";

            }
            echo "
            <tr>
                <td>{$history['dateOpened']}</td>
                <td>{$history['dateClosed']}</td>
                <td>{$history['siteManagers']}</td>
                <td>{$history['description']}</td>
                <td><a href='psc_report_view_issue.php?date={$history['date']}&question={$history['questionId']}'>View Report<a></td>
            </tr>
            ";
        }
    }
    ?>
    </table>
</div>
<br>
<?php require_once("../includes/pageParts/standardFooter.php");?>
