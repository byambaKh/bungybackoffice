<?php
require_once("../includes/application_top.php");
//this can go in to the ajax handler
$date = ifIsSet($_GET['date']);
$siteId = ifIsSet($_GET['site']);

$ds = DIRECTORY_SEPARATOR;
 
$storeFolder = __DIR__ . "/../uploads/incidentReports/$siteId/$date/";//need to create a folder with the incident report id
if(!file_exists($storeFolder))
    mkdir($storeFolder, 0777, true);//TODO set secure permissions

if (!empty($_FILES)) {
    $tempFile = $_FILES['file']['tmp_name'];
    $targetFile =  $storeFolder . $_FILES['file']['name'];
    $successfulUpload = move_uploaded_file($tempFile,$targetFile);

    $upload = array(
        'userId'        => $_SESSION['uid'],
        'dateTime'      => $date,
        'siteId'        => CURRENT_SITE_ID,
        'url'           => $storeFolder,
        'filename'      => $_FILES['file']['name'],
        'originalName'  => $_FILES['file']['name'],
        'storageName'   => $_FILES['file']['name'],
        'category'      => 'incidentReport',
    );
    db_perform("uploads", $upload);
}
//should we create a zip of all of the photos? and link to it from the form?
