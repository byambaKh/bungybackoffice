<?
function drawActionButtons()
{
    $states = [
        0 => 'Retire / 使用済',
        1 => 'Active / 使用中',
        2 => 'Ready / 用意'
    ];

    $colors = [
        0 => 'gray',
        1 => 'gray',
        2 => 'gray'
    ];

    $ids = [
        0 => 'retire',
        1 => 'active',
        2 => 'ready'
    ];

    $output = '';

    foreach ($states as $stateId => $state) {
        $output .= "<button id='{$ids[$stateId]}' class='state-button {$colors[$stateId]}'>$state</button>\n";
    }

    return $output;
}

?>
<html>
<head>
    <?php include 'includes/header_tags.php'; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=8">
    <script src="js/functions.js" type="text/javascript"></script>
    <?php include '../includes/head_scripts.php'; ?>
    <title>Operations</title>
    <style>
        #total-container * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -ms-box-sizing: border-box;
            z-index: -100;
            font-family: helvetica, sans-serif;
            font-size: 10pt;
        }

        #total-container * {
            z-index: 100;
        }

        #total-container {
            width: 700px;
            text-align: center;
            margin-left: auto;
            margin-right: auto;
            position: relative;
        }

        #total-container * select {
            width: 99%;
            height: 99%;
            border: none;
            border-bottom: 1px solid black;
        }

        #total-container div {
            /*height: 20px;*/
        }

        #total-container * input {
            width: 99%;
            height: 99%;
            border: 1px solid #EEEEEE;
        }

        #total-container * textarea {
            width: 100%;
            height: 100%;
            border: 1px solid silver;
        }

        .left {
            float: left;
            height: 20px;
        }

        .mister-table {
            width: 100%;
            display: table;
        }

        .mister-table div {
            width: 100%;
            display: table-row;
            line-height: 17px;
            margin: 0px;
            padding: 0px;
        }

        .mister-table div div {
            display: table-cell;
            float: left;
            padding-top: 5px;
            padding-bottom: 5px;
            border: none;
        }

        #report-title {
            padding-top: 20px;
            padding-bottom: 40px;
            font-size: 16pt;
            font-weight: bold;
            text-decoration: underline;
            text-align: left;
        }

        #cord-log-container {
            border: 2px solid black;
        }

        #cord-id-title {
            width: 80px;
            text-align: right;
        }

        #cord-id-value {
            width: 140px;
            height: 20px;
        }

        #total-container #cord-id-value select, #total-container #cord-id-value input {
            width: 45px;
            height: 18px;
            padding: 0px;
            vertical-align: middle;
        }

        #icid3 {
            width: 20px;
        }

        #total-container #cord-id-value #icid1, #total-container #cord-id-value #icid3 {
            border: 1px solid black;
        }

        #total-container #cord-id-value #icid2 {
            border-top: 1px solid black;
        }

        #test-date-title {
            width: 150px;
            text-align: right;
        }

        #test-date-value {
            width: 80px;
        }

        #water-level-title {
            width: 150px;
            text-align: right;
        }

        #water-level-value {
            width: 60px;
        }

        #water-level-valuem {
            width: 20px;
        }

        select#ijm, select#ijo, select#iraft {
            border: 1px solid black;
        }

        .mister-table div.border-br {
            border: none;
            border-bottom: 1px solid black;
            border-right: 1px solid black;
        }

        .mister-table div.cord-table-cell {
            width: 160px;
            height: 42px;
            padding: 1px;
        }

        .mister-table div.cord-table-cell.first-row {
            height: 21px;
            padding: 1px;
        }

        .mister-table div.border-t {
            border-top: 1px solid black;
        }

        .mister-table div.border-l {
            border-left: 1px solid black;
        }

        .mister-table div.double-size {
            height: 52px;
            padding-top: 9px;
            padding-bottom: 9px;
        }

        .mister-table div.single-size {
            height: 36px;
            padding-top: 9px;
            padding-bottom: 9px;
        }

        .mister-table div.single-size.no-padding {
            height: 18px;
            padding-top: 0px;
            padding-bottom: 0px;
        }

        .mister-table div.single-size.no-padding-bottom {
            height: 27px;
            padding-top: 9px;
            padding-bottom: 0px;
        }

        .mister-table div.single-size.no-padding-top {
            height: 27px;
            padding-top: 0px;
            padding-bottom: 9px;
        }

        .mister-table div.border1, .mister-table.border1 {
            border: 1px solid black;
        }

        .mister-table div.no-padding div {
            padding: 0px;
        }

        .mister-table div.bold {
            font-weight: bold;
        }

        #total-container * .cord-attr input {
            text-align: center;
            font-size: 30px;
        }

        select#ica_scs_1, select#ica_scs_2, select#ica_scs_3 {
            border: none;
            font-size: 30px;
            line-height: 30px;
            text-align: center;
        }

        #buttons {
            margin-top: 30px;
            margin-bottom: 60px;
            /*
            border: 1px solid #EEEEEE;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            */
        }

        #buttons button {
            float: right;
            background-color: #FFFF66;
            border: 1px solid black;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            font-size: 14pt;
            padding: 5px;
            margin: 5px;
            padding-left: 10px;
            padding-right: 10px;
        }

        #buttons #back {
            float: left;
        }

        .state-button{
            width: 100%;
            display: block;
        }

        .red {
            background-color: red !important;
        }

        .amber {
            background-color: #FFFF66 !important;
        }

        .green {
            background-color: green !important;
        }

        .gray {
            background-color: gray;
        }

        .blue {
            background-color: dodgerblue;
        }

        <?php
        if (isset($_GET['action']) && $_GET['action'] == 'view') {
        ?>
        #buttons #save, #buttons #retire {
            visibility: hidden;
        }

        * input:disabled, * textarea:disabled {
            background-color: #FEFEFE;
            color: black;
            opacity: 1;
        }

        <?php
        };
        ?>
    </style>
    <script>

        var shouldGetCordInfo = <?= $shouldGetCordInfo ? "true" : "false"?>;
        var allowEdit         = <?= $allowEdit ? "true" : "false" ?>;

        function getCordInfo() {
            $.ajax({
                url: '../includes/ajax_helper.php',
                dataType: 'json',
                data: 'action=getCordInfo&cid3=' + encodeURIComponent($('#icid3').val()) + '&cid2=' + encodeURIComponent($('#icid2').val()),
                success: function (data) {
                    if (data['result'] == 'success') {
                        $("#idate_of_lu").val(data['data']['date']);
                        $("#itotal_jumps").val(data['data']['total']);
                        $("#ituvh").val(data['data']['uvt_total']);
                    } else {
                        $("#idate_of_lu").val('');
                        $("#itotal_jumps").val('');
                        $("#ituvh").val('');
                    }
                }
            });
        }

        $(document).ready(function () {

            if(shouldGetCordInfo) {
                getCordInfo();
            }

            if(!allowEdit) {
                $("input").prop('disabled', 'disabled');
                //$("textarea").prop('disabled', 'disabled');
            }

            $('#back').click(function () {
                document.location = 'cord_logs.php';
                return false;
            });
            $('#daily').click(function () {
                var cld_window = window.open('cord_logs_daily.php?id=<?php echo $_report['id']; ?>', 'cld_window', 'height=500,width=900,location=0,menubar=0,status=0,toolbar=0,scrollbars=1');
                cld_window.focus();
            });
            $('#save').click(function () {
                $('#incident-report-form').submit();
                return true;
            });
            $('#retire').click(function () {
                if (confirm("Are you sure you want to RETIRE this cord?") === true) {
                    $('#istatus').val(0);
                    $('#incident-report-form').submit();
                    return true;
                } else {
                    return false;
                }
            });

            $('#retire').click(function () {
                if (confirm("Are you sure you want to RETIRE this cord?") === true) {
                    $('#istatus').val(0);
                    $('#cord-logs-form').submit();
                    return true;
                } else {
                    return false;
                }
            });

            $('#active').click(function () {
                if (confirm("Are you sure you want to ACTIVATE this cord?") === true) {
                    $('#istatus').val(1);
                    $('#cord-logs-form').submit();
                    return true;
                } else {
                    return false;
                }
            });

            $('#ready').click(function () {
                if (confirm("Are you sure you want to make this cord READY?") === true) {
                    $('#istatus').val(2);
                    $('#cord-logs-form').submit();
                    return true;
                } else {
                    return false;
                }
            });

        });


    </script>
</head>
<body>
<?php include '../includes/main_menu.php'; ?>
<br/>

<div id="total-container">
    <form id="cord-logs-form" method="POST">
        <?php
        if (isset($_GET['rid'])) {
            echo "<input type=\"hidden\" name=\"i[id]\" value=\"" . (int)$_GET['rid'] . "\">";
        };
        ?>
        <div id="report-title">Cord Log</div>
        <div id="cord-log-container">
            <div class="mister-table">
                <div>
                    <div id="cord-id-title">Cord ID:</div>
                    <div id="cord-id-value">
                        <?php draw_select('cid1', array("MK", "SG", "IB", "KY", "NR", "FJ", "YB", "GF"), 'icid1', $_report); ?><?php draw_select('cid2', array("SL", "L", "H", "SH"), 'icid2', $_report); ?><?php draw_input_field('cid3', 'icid3', $_report, 'onChange="getCordInfo();"'); ?>
                    </div>
                    <div id="test-date-title">Test Date / テスト日:</div>
                    <div id="test-date-value">
                        <?php draw_input_field('date', 'idate', $_report); ?>
                    </div>
                    <div id="water-level-title">Water Level / 水位:</div>
                    <div id="water-level-value">
                        <?php draw_input_field('wl', 'iwl', $_report); ?>
                    </div>
                    <div id="water-level-valuem">m</div>
                </div>
                <div>
                    <div style="width:5%;text-align:right;">JM:</div>
                    <div style="width:28%;"><?php draw_select('jm', $staff_names, 'ijm', $_report); ?></div>
                    <div style="width:5%;text-align:right;">JO:</div>
                    <div style="width:28%;"><?php draw_select('jo', $staff_names, 'ijo', $_report); ?></div>
                    <div style="width:5%;text-align:right;">Raft:</div>
                    <div style="width:28%;"><?php draw_select('raft', $staff_names, 'iraft', $_report); ?></div>
                </div>
            </div>
            <div class="mister-table cord-attr" style="margin-left: auto; margin-right: auto; margin-top: 30px; margin-bottom: 30px; width: 640px;">
                <?php
                $x = array(
                    ''  => "",
                    '1' => "1st / 1回目	",
                    '2' => "2nd / 2回目	",
                    '3' => "3rd / 3回目	"
                );
                $y = array(
                    ''    => "",
                    'w'   => "Weight / 重量  (kg)",
                    'h'   => "Height /  高さ  (m)",
                    'scs' => "Shock Cord Setting /<br /> ショックコード設定",
                );
                foreach ($y as $ri => $row) {
                    echo "\t\t<div>\n";
                    foreach ($x as $ci => $col) {
                        $add_class = "";
                        $text = "&nbsp;";
                        switch (TRUE) {
                            case ($ri == '' && $ci == ''):
                                $add_class .= ' first-row';
                                break;
                            case ($ri == ''):
                                $add_class .= ' first-row border-t';
                                $text = $col;
                                break;
                            case ($ci == ''):
                                $add_class .= " border-l";
                                $style = '';
                                if ($ri != 'scs') {
                                    $style = ' style="line-height: 30px;"';
                                } else {
                                    $style = ' style="padding: 2px;"';
                                };
                                $text = '<div' . $style . '>' . $row . '</div>';
                                break;
                            case ($ci != '' && $ri == 'scs'):
                                $name = 'ca_' . $ri . '_' . $ci;
                                $text = draw_select($name, array("Top", "Middle", "Bottom"), 'i' . $name, $_report, '', false);
                                break;
                            case ($ci != '' && $ri != ''):
                                $name = 'ca_' . $ri . '_' . $ci;
                                $text = draw_input_field($name, 'i' . $name, $_report, '', false);
                                break;
                        };
                        echo "\t\t\t<div class=\"border-br cord-table-cell$add_class\">$text</div>\n";
                    };
                    echo "\t\t</div>\n";
                };
                ?>
                <div>
                    <div class="border-br border-l" style="width: 640px; height: 150px; text-align: left;">Notes:
                        <div style="height: 130px;"><?php draw_textarea('ca_notes', 'ica_notes', $_report); ?></div>
                    </div>
                </div>
            </div>
            <div style="border: 1px solid black; height: 2px;"></div>
            <div class="mister-table">
                <div>
                    <div style="width: 300px; clear: both;">
                        <div style="width: 100px; float: left;" class="double-size">Site / 場所<br/>Height / 高さ</div>
                        <div style="width: 130px; float: left;" class="double-size"><?php draw_select('sh', array('MK (42m)', 'SG (60m)', 'IB (100m)', 'KY (65m)', 'NR (25m)', 'FJ (54m)', 'YB (45m),', 'GF (215m)'), 'ish', $_report, 'style="height: 34px; font-size: 25px; border: 1px solid black;"'); ?></div>
                        <br style="clear: both; "/>

                        <div style="width: 120px; float: left; text-align: right;" class="single-size">Man. Date / 製作日:</div>
                        <div style="width: 110px; float: left;" class="single-size"><?php draw_input_field('man_date', 'iman_date', $_report, 'style="border: 1px solid black;"'); ?></div>
                        <br style="clear: both; "/>

                        <!--change the input name to match the new value this field is taking-->
                        <div style="width: 120px; float: left; text-align: right;" class="single-size">Lot No / 製造番号:</div>
                        <div style="width: 110px; float: left;" class="single-size"><?php draw_input_field('lot_number', 'ilot_number', $_report, 'style="border: 1px solid black;"'); ?></div>
                        <br style="clear: both; "/>

                        <!--change the input name to match the new value this field is taking-->
                        <div style="width: 120px; float: left; text-align: right;" class="single-size">Serial No / 箱番号:</div>
                        <div style="width: 110px; float: left;" class="single-size"><?php draw_input_field('serial_number', 'iserial_number', $_report, 'style="border: 1px solid black;"'); ?></div>
                        <br style="clear: both; "/>

                        <div style="width: 150px; float: left; text-align: right;" class="double-size">Manufactured Length /&nbsp;
                            <br/> レイアウト終了後の長さ:
                        </div>
                        <div style="width: 80px; float: left;" class="double-size"><?php draw_input_field('man_len', 'iman_len', $_report, 'style="height: 34px; font-size: 25px; border: 1px solid black; text-align: center;"'); ?></div>
                        <div style="width: 10px; float: left;" class="double-size"><br/>m</div>
                        <br style="clear: both; "/>

                        <div style="width: 150px; float: left; text-align: right;" class="double-size">MAX Load Pressure /&nbsp;<br/> 最大負担値:
                        </div>
                        <div style="width: 80px; float: left;" class="double-size"><?php draw_input_field('max_load', 'imax_load', $_report, 'style="height: 34px; font-size: 25px; border: 1px solid black; text-align: center;"'); ?></div>
                        <div style="width: 30px; float: left;" class="double-size"><br/>kg / f</div>
                        <br style="clear: both; "/>
                        <br style="clear: both; "/>
                    </div>
                    <div style="width: 370px;">
                        <div style="width: 130px; float: left; text-align: right;" class="double-size">Cord Length /&nbsp;<br/>コードの長さ:
                        </div>
                        <div style="width: 110px; float: left;" class="double-size"><?php draw_input_field('cord_length', 'iweight_range', $_report, 'style="height: 34px; font-size: 25px; border: 1px solid black; text-align: center;"'); ?> </div>
                        <br style="clear: both; "/>

                        <div style="width: 130px; float: left; text-align: right;" class="double-size">Weight Range /&nbsp;<br/>最小体重:
                        </div>
                        <!--change the input name to match the new value this field is taking-->
                        <div style="width: 110px; float: left;" class="double-size"><?php draw_input_field('weight_range', 'iweight_range', $_report, 'style="height: 34px; font-size: 25px; border: 1px solid black; text-align: center;"'); ?> </div>
                        <br style="clear: both; "/>

                        <div style="width: 310px; float: left; text-align: left;" class="single-size no-padding-bottom">Manufactured By / 製作者</div>
                        <br style="clear: both; "/>

                        <div style="width: 180px; float: left; text-align: right;" class="single-size no-padding">Layout / レイアウト:</div>
                        <div style="width: 150px; float: left;" class="single-size no-padding"><?php draw_select('layout_by', $staff_names, 'ilayout_by', $_report, 'style="border: 1px solid black;"'); ?></div>

                        <br style="clear: both; "/>

                        <div style="width: 180px; float: left; text-align: right;" class="single-size no-padding">Binding / バインディング:</div>
                        <div style="width: 150px; float: left;" class="single-size no-padding-top"><?php draw_select('binding_by', $staff_names, 'ibinding_by', $_report, 'style="border: 1px solid black;"'); ?></div>
                        <br style="clear: both; "/>

                        <div style="width: 180px; float: left; text-align: right;" class="double-size">Operational Length /&nbsp;
                            <br/> 完成後の長さ:
                        </div>
                        <div style="width: 80px; float: left;" class="double-size"><?php draw_input_field('oper_len', 'ioper_len', $_report, 'style="height: 34px; font-size: 25px; border: 1px solid black; text-align: center;"'); ?></div>
                        <div style="width: 10px; float: left;" class="double-size"><br/>m</div>
                        <br style="clear: both; "/>

                        <div style="width: 180px; float: left; text-align: right;" class="double-size">Number of Ply /&nbsp;<br/> プライ数:
                        </div>
                        <div style="width: 80px; float: left;" class="double-size"><?php draw_select('num_of_ply', array("16", "18", "20", "22", "24", "26", "28", "30"), 'inum_of_ply', $_report, 'style="height: 34px; font-size: 25px; border: 1px solid black; text-align: center;"'); ?></div>
                    </div>
                </div>
            </div>
            <div class="mister-table border1" style="margin-left: auto; margin-right: auto; width: 602px;">
                <div>
                    <div style="width: 200px;" class="border1 no-padding">
                        <div class="bold">Date of Last Use</div>
                        <div>最終使用日</div>
                    </div>
                    <div style="width: 200px;" class="border1 no-padding">
                        <div class="bold">Total Jumps</div>
                        <div>ジャンプ合計</div>
                    </div>
                    <div style="width: 200px;" class="border1 no-padding">
                        <div class="bold">Total U.V. Hours</div>
                        <div>U.V.合計</div>
                    </div>
                </div>
                <div>
                    <div style="width: 200px;" class="border1"><?php draw_input_field('date_of_lu', 'idate_of_lu', $_report, 'style="height: 34px; font-size: 25px; text-align: center;"'); ?></div>
                    <div style="width: 200px;" class="border1"><?php draw_input_field('total_jumps', 'itotal_jumps', $_report, 'style="height: 34px; font-size: 25px; text-align: center;"'); ?></div>
                    <div style="width: 200px;" class="border1"><?php draw_input_field('tuvh', 'ituvh', $_report, 'style="height: 34px; font-size: 25px; text-align: center;"'); ?></div>
                </div>
            </div>
            <div class="mister-table border1" style="margin-left: auto; margin-right: auto; width: 602px; margin-top: 30px; margin-bottom: 30px;">
                <div>
                    <div style="width: 600px; height: 250px; text-align: left;">Notes:
                        <div style="height: 230px;"><?php draw_textarea('rep_notes', 'irep_notes', $_report); ?></div>
                    </div>
                </div>
            </div>
        </div>

        <div id="buttons">
            <?

            if (!isset($_GET['action']) || (isset($_GET['action']) && $_GET['action'] != 'view')) {
                echo drawActionButtons();
            }

            ?>
            <button id="back" type="button">Back</button>
            <button id="save" type="submit">Save</button>
            <?php if ($_report['id']) { ?>
                <button id="daily" type="button">Daily Usage</button>
            <?php }; ?>
        </div>
        <input type="hidden" name="i[status]" value="<?php echo (array_key_exists('status', $_report)) ? $_report['status'] : 2; ?>" id="istatus">


    </form>
</div>
</body>
</html>
