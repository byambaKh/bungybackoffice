<?php
include '../includes/application_top.php';
mysql_query("SET NAMES UTF8;");

/*
 * This could be made into an uploads class to handle uploads generically
 * And we could also have a download class
 */
$temp = explode(".", $_FILES["file"]["name"]);
$originalName = $_FILES["file"]["name"];
$extension = end($temp);

$uploadDirectory = $_SERVER['DOCUMENT_ROOT'].'/uploads/incidentReports/';

if ($extension == "zip") {
	if ($_FILES["file"]["error"] > 0) {
		//echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
	} else {
        $storageName = $user->getID()."_".date('YmdHis').".zip";//A unique name for storing the file
		if(move_uploaded_file($_FILES["file"]["tmp_name"], $uploadDirectory . $storageName)){
            $user = new BJUser();

            $data = array(
                'originalName' => $originalName,
                'storageName' => $storageName,
                'filename' => $originalName,
                'directory' => $uploadDirectory,
                'userId' => $user->getID(),
                'dateTime' => date('Y-m-d H:i:s'),
                'category' => 'Incident Report',
                'meta1' => $_POST['date'],
                'site_id' => $_POST['site_id']
            );

            db_perform('uploads', $data);
            //successful upload message
            echo "Image zip file Successfully Uploaded. You may now close this window.<br> ";
        } else {
            echo "There was an error uploading your file, please check your internet connection and try again later.<br> ";

        }
		//echo "<a href = 'index.php'>&lt;&lt; Back</a> ";
	}

} else {
	//upload failed message
	echo "Invalid file";
}
?>
