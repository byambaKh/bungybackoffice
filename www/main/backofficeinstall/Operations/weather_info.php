<?php
	include '../includes/application_top.php';
?>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">
<!--link href="css/style.css" rel="stylesheet" media="screen" type="text/css" /-->
<script src="js/functions.js" type="text/javascript"></script>

<?php include '../includes/head_scripts.php'; ?>
<title>Operations :: Weather Information</title>
<script>
function WriteSWF (swf, width, height, flashvars, flashver) {

    //var cookie = getCookie('mdbauth');
    var cookie = null;
    var statusLogin = 0;
    if(cookie)
      statusLogin = 1;
    var flashversion = 6;
    if(flashver)
      flashversion = flashver;
    var chDate = '<?php echo date('Ymd'); ?>';
    var commentDate = '0812051';
    var code = '<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000" width=';
    code += width + ' height=' + height;
    code += ' id="flashmovie" ';
    code += 'codebase="http://download.macromedia.com/pub/shockwave/cabs/flash/swflash.cab#version=' + flashversion + ',0,0,0">\n';
    code += '<param name="bgcolor" value="#ffffff">\n';
    code += '<param name="flashvars" value="' + flashvars + '&chDate=' + chDate + '&statusLogin=' + statusLogin + '&commentDate=' + commentDate + '">\n';
    code += '<param name="MOVIE" value="' + swf + '">\n';
    code += '<param name="PLAY" value="true">\n';
    code += '<param name="LOOP" value="false">\n';
    code += '<param name="QUALITY" value="best">\n';
    code += '<param name="MENU" value="false">\n';
    code += '<param name="allowScriptAccess" value="always">\n';

    code += '<embed src="' + swf + '"\n';
    code += 'width="' + width + '"\n';
    code += 'height="' + height + '"\n';
    code += 'PLAY="true"\n';
    code += 'MENU="false"\n';
    code += 'LOOP="false"\n';
    code += 'QUALITY="best"\n';
    code += 'BGCOLOR="#ffffff"\n';
    code += 'flashvars=' + flashvars + '&chDate=' + chDate + '&statusLogin=' + statusLogin + '&commentDate=' + commentDate + '\n';
    code += 'type="application/x-shockwave-flash"\n';
    code += 'allowScriptAccess="always"\n';
    code += 'PLUGINSPAGE="http://www.macromedia.com/go/getflashplayer">\n';
    code += '</embed>\n</object>\n';

    if(document.all){ document.all('map').innerHTML = code }
    if(document.getElementById){ document.getElementById('map').innerHTML = code }
}
</script>
<style>
body {
background-color: white;
}
#total-container * {
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -ms-box-sizing: border-box;
    font-family: helvetica,sans-serif;
    font-size: 12px;
}
#total-container {
    width: 530px;
    text-align: center;
    margin-left: auto;
    margin-right: auto;
}
#map-title h1 {
	font-size: 20px;
}
#map-container {
    width: 530px;
    text-align: center;
    margin-left: auto;
    margin-right: auto;
	position: relative;
	overflow: hidden;
	height: 430px;
}
#map {
	width : 530px;
	position: absolute;
	height: 535px;
	top: -100px;	
	left: 0px;
	overflow: hidden;
    margin-left: auto;
    margin-right: auto;
}
#buttons {
	width: 100%;
/*
	top: 530px;
	position: absolute;
*/
	clear: both;
    margin-bottom: 60px;
    height: 49px;
    border: 1px solid #EEEEEE;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}
#buttons button {
    float: left;
    background-color: #FFFF66;
    border: 1px solid black;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    font-size: 14pt;
    padding: 5px;
    margin: 5px;
    padding-left: 10px;
    padding-right: 10px;
}
#buttons #back {
    float:left;
}
</style>
</head>
<body onLoad="WriteSWF('http://weathernews.jp/pinpoint/swf/pinpoint2.swf?201006172', '760', '585', '&pointName=水上&obsOrg=42091&xmlFile=../xml/42091_410000028.xml&areaNo=42&baseURL=' + encodeURIComponent('http://weathernews.jp/'));">

<?php include '../includes/main_menu.php'; ?>
<br />
<div id="total-container">
	<div id="map-title"><h1>Weather Info</h1></div>	
    <!--<div id="map-container">
	<div id="map">
     <div id="swfCode"></div> -->
     <?php
      $site_id = CURRENT_SITE_ID;

      switch ($site_id) {
        case (1): 
            //MK
            echo "<a class='weatherwidget-io' href='https://forecast7.com/ja/36d68139d00/minakami/'' data-label_1='MINAKAMI' data-label_2='WEATHER'     data-theme='original'>MINAKAMI WEATHER</a>" ;
            break;
        case (2):            
            //SK
            echo "<a class='weatherwidget-io' href='https://forecast7.com/ja/36d68139d00/minakami/'' data-label_1='MINAKAMI' data-label_2='WEATHER'     data-theme='original'>MINAKAMI WEATHER</a>" ;
            break;
        case (3):
            //IB
             echo "<a class='weatherwidget-io' href='https://forecast7.com/en/36d54140d53/hitachiota/' data-label_1='HITACHI-ŌTA' data-label_2='WEATHER' data-theme='original' >HITACHI-ŌTA WEATHER</a>";
            break;
        case (4):
            //Itsu
            echo "<a class='weatherwidget-io' href='https://forecast7.com/ja/32d80130d71/kumamoto/' data-label_1='ITSUKI MURA' data-label_2='WEATHER' data-theme='original' >ITSUKI MURA WEATHER</a>";            
            break;
        case (9):
            //YB
            echo "<a class='weatherwidget-io' href='https://forecast7.com/ja/36d39139d06/gunma-prefecture/' data-label_1='NAGANOHARA' data-label_2='WEATHER' data-theme='original' >NAGANOHARA WEATHER</a>" ;
            break;
        case (10):
            //Nara
            echo "<a class='weatherwidget-io' href='https://forecast7.com/ja/34d69135d50/osaka/' data-label_1='SANGO CHO' data-label_2='WEATHER' data-theme='original' >SANGO CHO WEATHER</a>";
            break;
        case (12):
            //FJ
            echo "<a class='weatherwidget-io' href='https://forecast7.com/ja/35d16138d68/fuji/' data-label_1='FUJI' data-label_2='WEATHER' data-theme='original' >FUJI WEATHER</a>";
            break;
        case (13):
            //GF
            echo "<a class='weatherwidget-io' href='https://forecast7.com/en/35d48137d14/yaotsu/' data-label_1='YAOTSU CHO' data-label_2='WEATHER' data-theme='original' >YAOTSU CHO WEATHER</a>";
            break;
    };
      //
     
     ?>
     
    <script>
    !function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src='https://weatherwidget.io/js/widget.min.js';fjs.parentNode.insertBefore(js,fjs);}}(document,'script','weatherwidget-io-js');
    </script>

	</div>
	</div>
	<br style="clear:both;" />
  <div id="buttons">
    <button id="back">Back</button>
  </div>
</div>
<script>
$(document).ready(function () {
    $('#back').click(function () {
        document.location = 'operationActivities.php';
        return false;
    });
});
</script>

</body>
</html>
