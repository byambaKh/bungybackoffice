<?php
include '../includes/application_top.php';
mysql_set_charset('utf8');
//if POST is not empty then save date
if (!empty($_POST) && array_key_exists('i', $_POST)) {
    include 'reports_save.php';
    $_POST['i']['site_id'] = CURRENT_SITE_ID;
    if (!array_key_exists('id', $_POST['i']) || empty($_POST['i']['id'])) {
        $sql = "select * from daily_reports where site_id = '" . CURRENT_SITE_ID . "' and `date` = '{$_POST['i']['date']}';";
        $res = mysql_query($sql) or die(mysql_error());
        if ($row = mysql_fetch_assoc($res)) {
            $_POST['id'] = $row['id'];
        }
    }

    //perform the image upload
    $uploader = new fileUploadHandler();
    $uploader->restrictedExtensions = ["jpeg", "jpg", "png", "gif"];
    $uploader->uploadDirectory = "dailyReports/";
    $uploader->category = "Daily Report";
    $uploader->arrayName= "files";
    $uploader->upload();

    if (save_report('daily_reports', $_POST['i'])) {
        $_SESSION['message'] = "Daily report successfully saved!";
    } else {
        $_SESSION['message'] = "There is error for saving daily report (((";
    };
    Header('Location: operationActivities.php');
    die();
};
//if reportId is set, get the report specified
if (isset($_GET['rid'])) {
    //$rid = $_GET['rid'];
    $sql = "SELECT * FROM daily_reports WHERE id='" . (int)$_GET['rid'] . "'";
    $res = mysql_query($sql) or die(mysql_error());
    $_report = mysql_fetch_assoc($res);
} else {//Create a blank report for today or the date specified in GET
    $_report = array(
        'date' => $_GET['date'] ? $_GET['date'] : date('Y-m-d'),
        'day' => mb_convert_encoding(strftime('%A'), 'UTF-8', 'SJIS'),
        'stime' => '9:00',
        'etime' => '18:00'
    );
    //Try to find that report in the database and if it is there, replace the blank report above with what is found
    //and set $_GET['rid'] to be the found reports id
    $sql = "select * from daily_reports where site_id = '" . CURRENT_SITE_ID . "' and `date` = '{$_report['date']}';";
    $res = mysql_query($sql) or die(mysql_error());
    if ($row = mysql_fetch_assoc($res)) {
        $_report = $row;
        $_GET['rid'] = $_report['id'];
    };
};
$where = '';
//if we are not at main, specify a WHERE clause
if (defined("CURRENT_SITE_ID") && CURRENT_SITE_ID > 0) {
    $where = " WHERE site_id = '" . CURRENT_SITE_ID . "' ";
};

//get the most recent report for that site or set $last_rep to be null
$sql = "select * from daily_reports $where order by date DESC limit 1";
$res = mysql_query($sql) or die(mysql_error());
if (!$last_rep = mysql_fetch_assoc($res)) {
    $last_rep = null;
};
//get all of the staff
$staff_names = BJHelper::getStaffList('StaffListName', true);
?>
<!DOCTYPE html>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<!--meta http-equiv="X-UA-Compatible" content="IE=8"-->
<!--link href="css/style.css" rel="stylesheet" media="screen" type="text/css" /-->
<script src="js/functions.js" type="text/javascript"></script>

<?php include '../includes/head_scripts.php'; ?>
<title>Operations</title>
<link rel="stylesheet" type="text/css" href="css/_daily_report.css" media="all"/>
<style>
<?php
if (isset($_GET['action']) && $_GET['action'] == 'view') {
?>
#buttons #submit {
    visibility: hidden;
}

* input:disabled, * textarea:disabled {
    background-color: #FEFEFE;
    color: black;
}

<?php
};
?>
</style>
<?php
if (isset($_GET['action']) && $_GET['action'] == 'view') {
    ?>
    <script>
        $(document).ready(function () {
            $("input").prop('disabled', 'disabled');
            $("textarea").prop('disabled', 'disabled');
        });
    </script>
<?php
};
?>
</head>
<body>
<?php include '../includes/main_menu.php'; ?>
<br/>

<div id="total-container">
<form id="daily-report-form" method="POST" enctype="multipart/form-data">
<?php
if (isset($_GET['rid'])) {
    echo "<input type=\"hidden\" name=\"i[id]\" value=\"" . (int)$_GET['rid'] . "\">";
};
?>
<div id="report-title">DAILY SITE REPORT</div>
<div id="date-time-container">
    <div class="datetime">Date <?php draw_input_field('date', "idate", $_report); ?></div>
    <div class="datetime">Day <?php draw_input_field('day', "iday", $_report); ?></div>
    <div class="datetime">Start Time <?php draw_select('stime', array('7:30', '8:00', '8:30', '9:00'), "istime", $_report); ?></div>
    <div class="datetime">Finish Time <?php draw_select('etime', array('17:30', '18:00', '18:30', '19:00'), "ietime", $_report); ?></div>
</div>
<div id="staff-container">
    <div id="staff-title" class="bold">STAFF</div>
    <div id="staff-values">
        <?php
        $staff = array(
            'set_up' => 'Set Up',
            'pack_down' => 'Pack Down',
            'jc' => 'Jump Controller',
            'jm' => 'Jump Master',
            'jo' => 'Jump Operator',
            'h' => 'Harness',
            'r' => 'Recovery',
            'sc' => 'Call Center (MK)',
            're' => 'Reception',
            'ph' => 'Photographer',
        );

        foreach ($staff as $id => $title) {
            echo "\n\t<div class=\"staff-field\">$title</div>";
            for ($i = 1; $i < 5; $i++) {
                $name = "staff_$id$i";
                echo "\n\t\t<div class=\"staff-field\">";
                draw_select($name, $staff_names, 'i' . $name, $_report);
                echo "</div>";
            };
        };
        ?>
    </div>
</div>
<div id="weather-container">
    <div id="levels">
        <div id="water-levels-container" class="border-solid">
            <div class="title bold">WATER LEVELS</div>
            <div class="small-title half">A.M.</div>
            <div class="small-title half">P.M.</div>
            <div class="value half"><?php draw_input_field('wl_am', "iwl_am", $_report); ?>m</div>
            <div class="value half"><?php draw_input_field('wl_pm', "iwl_pm", $_report); ?>m</div>
        </div>
        <div id="levels-container" class="border-solid">
            <div class="small-title half">Level</div>
            <div class="small-title half">Level</div>
            <div class="value half"><?php draw_input_field('level_1', "ilevel_1", $_report); ?></div>
            <div class="value half"><?php draw_input_field('level_2', "ilevel_2", $_report); ?></div>
        </div>
    </div>
    <div id="weather" class="border-solid">
        <div class="title bold third">WEATHER</div>
        <div class="title bold third">A.M.</div>
        <div class="title bold third">P.M.</div>
        <?php
        $weather = array(
            'h' => 'Humidity',
            'r' => 'Rain',
            'w' => 'Wind',
            't' => 'Temp',
        );
        foreach ($weather as $id => $title) {
            echo "<div class=\"title third center\">$title</div>";
            foreach (array('am', 'pm') as $part) {
                $name = "weather_{$id}_{$part}";
                echo "<div class=\"title third input-inside\">";
                switch ($id) {
                    case ('h') :
                        draw_select($name, array('', 'Low', 'Medium', 'High'), "i$name", $_report, 'class="center"');
                        break;
                    case ('r') :
                        draw_select($name, array('', 'Light', 'Medium', 'Heavy'), "i$name", $_report, 'class="center"');
                        break;
                    case ('w') :
                        draw_select($name, array('', 'Light', 'Medium', 'Strong'), "i$name", $_report, 'class="center"');
                        break;
                    default:
                        draw_input_field($name, "i$name", $_report, 'class="center"');
                };
                echo "</div>";
            };
        };
        ?>
    </div>
    <div id="weather-description" class="border-solid">
        <div class="title bold">Weather Description</div>
        <div class="value"><?php draw_textarea('weather_description', 'iweather_description', $_report); ?></div>
    </div>
</div>
<br style="clear:both;">
<?php
//if (!array_key_exists('j_non_jumps', $_report)) {
$sql = "SELECT DISTINCT w.id FROM customerregs1 cr, waivers w, non_jumpers nj
      WHERE 
        cr.site_id = " . CURRENT_SITE_ID . "
        AND cr.BookingDate='" . $_report['date'] . "'
        AND Checked = 1
        AND DeleteStatus = 0
        AND cr.CustomerRegID = w.bid
        AND w.id = nj.waiver_id;";
$res = mysql_query($sql) or die(mysql_error());
$_report['j_non_jumps'] = mysql_num_rows($res);
//};
//if (!array_key_exists('j_total_jumps', $_report)) {
$_report['j_total_jumps'] = 0;
$_report['j_test_jumps'] = 0;
$_report['j_2nd_jumps'] = 0;
$_report['j_foc_jumps'] = 0;
$sql = "SELECT
        BookingDate,
        sum(NoOfJump + 2ndj_qty) as total_jumps, 
        sum(IF(Rate NOT IN (0, {$config['second_jump_rate']}), NoOfJump, 0)) as total_paid, 
        sum(IF(Rate = '{$config['second_jump_rate']}', NoOfJump, 0)+2ndj_qty) as total_2nd,
        sum(IF(Rate = 0, NoOfJump, 0)) as total_foc, 
        sum(IF(Agent like '___Bungy' AND CollectPay = 'Offsite', NoOfJump, 0)) as total_combo 
      FROM customerregs1 WHERE site_id = " . CURRENT_SITE_ID . " AND BookingDate='" . $_report['date'] . "' AND NoOfJump > 0 and Checked = 1 and DeleteStatus = 0
      GROUP BY BookingDate;";
//get the number of jumps
$res = mysql_query($sql) or die(mysql_error());
if ($row = mysql_fetch_assoc($res)) {
    $_report['j_total_jumps'] = $row['total_jumps'];
    $_report['j_test_jumps'] = $row['total_combo'] ? $row['total_combo'] : 0;
    $_report['j_2nd_jumps'] = $row['total_2nd'];
    $_report['j_foc_jumps'] = $row['total_foc'];
};
$sql = "SELECT DATE(sale_time) AS sale_date, sum(sale_total_qty_2nd_jump) AS total_2nd
        FROM merchandise_sales 
        WHERE site_id = " . CURRENT_SITE_ID . " AND sale_time LIKE '" . $_report['date'] . "%'
        GROUP BY sale_date;";
$res = mysql_query($sql) or die(mysql_error());
if ($row = mysql_fetch_assoc($res)) {
    if (!array_key_exists('j_2nd_jumps', $_report)) {
        $_report['j_2nd_jumps'] = 0;
    };
    $_report['j_total_jumps'] += $row['total_2nd'];
    $_report['j_2nd_jumps'] += $row['total_2nd'];
};
$_report['j_insurance_no'] = $_report['j_total_jumps'] - $_report['j_test_jumps'] - $_report['j_2nd_jumps'] - $_report['j_non_jumps'];
$_report['j_daily_total'] = $_report['j_total_jumps'] - $_report['j_non_jumps'];
//};
?>
<div id="jumpers-container" class="border-solid spacer">
    <div class="title2 bold">JUMPERS</div>
    <div class="value quarter">Total Jumps</div>
    <div class="value quarter input-inside"><?php draw_input_field('j_total_jumps', "ij_total_jumps", $_report, 'style="text-align: center;" readonly'); ?></div>
    <div class="value quarter">First Jump</div>
    <div class="value quarter input-inside"><?php draw_input_field('j_first_jump', "ij_first_jump", $_report, 'style="text-align: center;"'); ?></div>
    <div class="value quarter align-left">- Combos</div>
    <div class="value quarter input-inside"><?php draw_input_field('j_test_jumps', "ij_test_jumps", $_report, 'style="text-align: center;" readonly'); ?></div>
    <div class="value quarter">Last Jump</div>
    <div class="value quarter input-inside"><?php draw_input_field('j_last_jump', "ij_last_jump", $_report, 'style="text-align: center;"'); ?></div>
    <div class="half">
        <div class="value half align-left">- 2nd Jumps</div>
        <div class="value half input-inside"><?php draw_input_field('j_2nd_jumps', "ij_2nd_jumps", $_report, 'style="text-align: center;" readonly'); ?></div>
        <div class="value half align-left">- Non Jumpers</div>
        <div class="value half input-inside"><?php draw_input_field('j_non_jumps', "ij_non_jumps", $_report, 'style="text-align: center;" readonly'); ?></div>
        <div class="value half align-left">= Insurance No.</div>
        <div class="value half input-inside"><?php draw_input_field('j_insurance_no', "ij_insurance_no", $_report, 'style="text-align: center;" readonly'); ?></div>
    </div>
    <?php
    // fill old total for monthly jumps
    $month_date = preg_replace("/([\d]{2})$/", "", $_report['date']);
    $name = 'j_old_monthly_total';
    $sql = "SELECT
      sum(NoOfJump + 2ndj_qty) AS total_jumps
    FROM customerregs1 
    WHERE site_id = " . CURRENT_SITE_ID . "
      AND BookingDate LIKE '" . $month_date . "%'
      AND BookingDate < '" . $_report['date'] . "'
      AND Checked = 1
      AND DeleteStatus = 0
    ;";
    $res = mysql_query($sql) or die(mysql_error());
    $_report['j_old_monthly_total'] = 0;
    if ($row = mysql_fetch_assoc($res)) {
        $_report['j_old_monthly_total'] = $row['total_jumps'];
    };
    $sql = "SELECT sum(sale_total_qty_2nd_jump) AS total_2nd
      FROM merchandise_sales 
      WHERE site_id = " . CURRENT_SITE_ID . "
        AND sale_time LIKE '" . $month_date . "%'
        AND sale_time < '" . $_report['date'] . " 00:00:00'
      ;";
    $res = mysql_query($sql) or die(mysql_error());
    if ($row = mysql_fetch_assoc($res)) {
        $_report['j_old_monthly_total'] += $row['total_2nd'];
    };
    $_report['j_monthly_total'] = $_report['j_old_monthly_total'] + $_report['j_daily_total'];
    ?>
    <div class="half">
        <div class= "value half">Pack Down</div>
        <div class= "value half input-inside"><?php draw_input_field('j_packdown', "ij_packdown", $_report, 'style="text-align: center;"'); ?></div>
        <div id="daily-jump-total-title" class="third">Daily<br/>Jump<br/>Total</div>
        <div id="old-monthly-jump-total-title" class="third">Old Mon<br/>Jump<br/>Total</div>
        <div id="monthly-jump-total-title" class="third">Monthly<br/>Jump<br/>Total</div>
        <div class="total-value third input-inside"><?php draw_input_field('j_daily_total', "ij_daily_total", $_report, 'class="center" readonly'); ?></div>
        <div class="total-value third input-inside"><?php draw_input_field('j_old_monthly_total', "ij_old_monthly_total", $_report, 'class="center" readonly'); ?></div>
        <div class="total-value third input-inside"><?php draw_input_field('j_monthly_total', "ij_monthly_total", $_report, 'class="center" readonly'); ?></div>
    </div>
</div>
<div id="equipment-changes-container" class="border-solid spacer">
    <div class="title2 bold">Equipment Changes</div>
    <div id="equipment-changes"><?php draw_textarea('equipment_changes', 'iequipment_changes', $_report); ?></div>
    <div class="width20 verysmall border-bottom border-right center bold">MK</div>
    <div class="width20 verysmall border-bottom border-right center bold">SG</div>
    <div class="width20 verysmall border-bottom border-right center bold">IB</div>
    <div class="width20 verysmall border-bottom border-right center bold">Old Tot.</div>
    <div class="width20 verysmall border-bottom border-right center bold">New Tot.</div>
    <?php
    $eq = array(
        'cwr_use' => array('CWR', 'Lt. Sh.', 'SL Sh.'),
        'sc_use' => array('SCR', 'Rt. Sh.', 'L Sh.'),
        'rp' => array('Pully', '-', 'H Sh.'),
    );
    foreach ($eq as $id => $title_ar) {
        foreach ($title_ar as $title) {
            echo "<div class=\"width20 verysmall border-bottom border-right center\">$title</div>";
        };
        // fill old total for monthly jumps
        $name = 'eq_' . $id . '_old';
        if (!array_key_exists($name, $_report) && !is_null($last_rep)) {
            $_report[$name] = $last_rep['eq_' . $id . '_now'];
        };
        foreach (array("old", "now") as $old) {
            $name = "eq_{$id}_$old";
            echo "<div class=\"width20 small border-bottom border-right input-inside\">";
            draw_input_field($name, "i$name", $_report, 'class="center"');
            echo "</div>";
        };
    };
    ?>
    <!--div class="half small border-bottom border-right center">First aid supplies</div>
    <div class="half small border-bottom border-right center">
      <div class="half center"><label for="eq-fas-yes"><input type="radio" name="i[eq_fas]" value="Yes" id="eq-fas-yes"<?php echo ($_report['eq_fas'] == 'Yes') ? ' checked="checked"' : ''; ?>>&nbsp;Yes</label></div>
      <div class="half center"><label for="eq-fas-no"><input type="radio" name="i[eq_fas]" value="No" id="eq-fas-no"<?php echo ($_report['eq_fas'] == 'No') ? ' checked="checked"' : ''; ?>>&nbsp;No</label></div>
    </div-->
    <div class="verysmall border-bottom border-right" style="float: left; width: 100%; overflow: hidden;">FA supplies used: <?php draw_input_field('eq_what', "ieq_what", $_report, 'style="width: 65%; border: 1px solid silver;"'); ?></div>
</div>
<div id="bungy-cords" class="border-solid spacer left">
    <div class="width20 border-right border-left title bold">BUNGY CORDS</div>
    <?php
    $cords = array('yellow', 'red', 'blue', 'black');
    foreach ($cords as $color) {
        echo "<div class=\"width15 border-right border-left title bold\">" . strtoupper($color) . "</div>";
    };
    ?>
    <div class="width20 border-right border-left title bold">CORD USE TOTAL</div>
    <div class="width80">
        <?php
        $fields = array(
            "tw" => "Weight Range",
            "cc" => "CORD CODE",
            "jn" => "CORD USE",
            "ot" => "OLD USE TOTAL",
            "ct" => "NEW USE TOTAL",
            "uvh" => "U.V. HOURS",
            "olduvt" => "OLD U.V. TOTAL",
            "uvt" => "NEW U.V. TOTAL"
        );
        $text['cc'] = array(
            'yellow' => "SL",
            'red' => 'L',
            'blue' => 'H',
            'black' => 'SH'
        );

        foreach ($fields as $id => $title) {
            $add_class = '';
            if ($id == 'ct') $add_class = ' border-bottom3';
            echo "<div class=\"quarter border-right border-left title bold center$add_class\">$title</div>";
            foreach ($cords as $color) {
                $name = 'eq_cord_' . $id . '_' . $color . '';
                // fill old total if not filled
                $cord_id_field = 'eq_cord_cc_' . $color . '_num';
                // get active cord id
                //if the report does not have anything in this field
                if (!array_key_exists($cord_id_field, $_report) || empty($_report[$cord_id_field])) {
                    //cords have 3 identifiers in the cord_logs table
                    //If there is no active cord OR there is no active cord this year, the search for the active cord fails as
                    //we search for active cords with date('Y-01-01') which only looks for this years cords
                    //this failing will cause getCordInfo to fail and there to be no cord data retrieved although the cord is active.
                    $_report[$cord_id_field] = getActiveCordCID3($text['cc'][$color]);//GET the active cord id 3 for the cord on this site of color $color
                };
                //if id == ot (Old use total) and $name (ie eq_cord_cc_yellow) is not in the reports and there is a last reported
                if ($id == 'ot' && !array_key_exists($name, $_report) && !is_null($last_rep)) {
                    //cord id filled ie :eq_cord_tw_yellow exists in the report and the it exists in last report and the values are equal
                    if (array_key_exists($cord_id_field, $_report) && array_key_exists($cord_id_field, $last_rep) && $_report[$cord_id_field] == $last_rep[$cord_id_field]) {
                        $_report[$name] = $last_rep['eq_cord_ct_' . $color];//for some reason th
                    } else {
                        $cord_info = getCordInfo($text['cc'][$color], $_report[$cord_id_field]);//get cord info fails because cordId3 is not set
                        if ($cord_info['result'] == 'success' && !empty($cord_info['data']['cid'])) {
                            $_report[$name] = $cord_info['data']['total'];
                        } else {
                            $_report[$name] = 0;
                        };
                    };
                };
                //old usage value total
                if ($id == 'olduvt' && !array_key_exists($name, $_report) && !is_null($last_rep)) {
                    if (array_key_exists($cord_id_field, $_report) && array_key_exists($cord_id_field, $last_rep) && $_report[$cord_id_field] == $last_rep[$cord_id_field]) {
                        $_report[$name] = $last_rep['eq_cord_uvt_' . $color];
                    } else {
                        $cord_info = getCordInfo($text['cc'][$color], $_report[$cord_id_field]);
                        if ($cord_info['result'] == 'success' && !empty($cord_info['data']['cid'])) {
                            $_report[$name] = $cord_info['data']['uvt_total'];
                        } else {
                            $_report[$name] = 0;
                        };
                    };
                };
                echo "<div class=\"border-right border-left value  center input-inside left$add_class\" style=\"width: 18.75%;\">";
                if (array_key_exists($id, $text)) {
                    $name .= '_num';
                    echo $text[$id][$color];
                    draw_input_field($name, "i$name", $_report, 'style="margin-right:4px;width: 45%;text-align: center; float: right;"');
                } else {
                    if ($id != 'tw') {
                        draw_input_field($name, "i$name", $_report, 'class="center"');
                    } else {
                        if (
                            !array_key_exists($name, $_report) &&
                            !is_null($last_rep) &&
                            array_key_exists($cord_id_field, $_report) &&
                            array_key_exists($cord_id_field, $last_rep) &&
                            $_report[$cord_id_field] == $last_rep[$cord_id_field]
                        ) {
                            $_report[$name . '_low'] = $last_rep[$name . '_low'];
                            $_report[$name] = $last_rep[$name];
                        } else {
                            $cord_info = getCordInfo($text['cc'][$color], $_report[$cord_id_field]);
                            if ($cord_info['result'] == 'success' && !empty($cord_info['data']['cid'])) {
                                $_report[$name . '_low'] = $cord_info['data']['wr_low'];
                                $_report[$name] = $cord_info['data']['wr_high'];
                            };
                        };
                        draw_input_field($name . '_low', "i$name" . '_low', $_report, 'style="width: 45%;text-align: center;"');
                        draw_input_field($name, "i$name", $_report, 'style="margin-left:2px;width: 45%;text-align: center;"');
                    };
                };
                echo "</div>";
            };
        };
        ?>
    </div>
    <div class="width20 border-right border-left title bold" style="height: 232px; padding:0px;">
        <div style="height: 86px; padding: 5px;"><?php draw_textarea('cord_total', 'cord-total', $_report); ?></div>
        <div style="border-top: 1px solid black; border-bottom: 1px solid black; margin: 0px; padding: 6px; height: 30px;">Incident reported</div>
        <div style="margin: 0px; padding: 0px; height: 30px; vertical-align: middle;" class="width50">
            <div class="half center">
                <label for="eq-ir-no"><input type="radio" name="i[eq_ir]" value="No" id="eq-ir-no"<?php echo ($_report['eq_ir'] == 'No') ? ' checked="checked"' : ''; ?>>&nbsp;NO</label>
            </div>
            <div class="half center">
                <label for="eq-ir-yes"><input type="radio" name="i[eq_ir]" value="Yes" id="eq-ir-yes"<?php echo ($_report['eq_ir'] == 'Yes') ? ' checked="checked"' : ''; ?>>&nbsp;YES</label>
            </div>
        </div>
        <div class="level">
            <label for="eq-lev3-yes">Level 3&nbsp;<input type="checkbox" name="i[eq_ir_lev3]" value="Yes" id="eq-lev3-yes"<?php echo ($_report['eq_ir_lev3'] == 'Yes') ? ' checked="checked"' : ''; ?>></label>
        </div>
        <div class="level">
            <label for="eq-lev2-yes">Level 2&nbsp;<input type="checkbox" name="i[eq_ir_lev2]" value="Yes" id="eq-lev2-yes"<?php echo ($_report['eq_ir_lev2'] == 'Yes') ? ' checked="checked"' : ''; ?>></label>
        </div>
        <div class="level">
            <label for="eq-lev1-yes">Level 1&nbsp;<input type="checkbox" name="i[eq_ir_lev1]" value="Yes" id="eq-lev1-yes"<?php echo ($_report['eq_ir_lev1'] == 'Yes') ? ' checked="checked"' : ''; ?>></label>
        </div>
    </div>
</div>
<div id="today-bookings" class="border-solid spacer left">
    <div class="width20 border-right title bold">Today's Bookings</div>
    <div class="width80 border-bottom border-right" style="height: 29px;">
        <?php
        $books = array(
            'am' => 'AM',
            'pm' => 'PM',
            '' => 'TOTAL',
            'fb' => 'First',
            'lb' => 'Last'
        );
        foreach ($books as $id => $value) {
            $name = 'today_bookings';
            if (!empty($id)) $name .= '_' . $id;
            echo '<div class="width10 value left input-inside" style="border: none; font-weight: bold;">' . $value . '</div>';
            echo '<div class="width10 value left input-inside" style="border: none;">';
            draw_input_field($name, "i$name", $_report);
            echo "</div>";
        };
        ?>
    </div>
</div>
<div id="reports" class="border-solid spacer left">
    <?php
    for ($i = 1; $i < 6; $i++) {
        ?>
        <div class="width20 border-right title bold">Report <?php echo $i; ?></div>
        <div class="width80 value left input-inside"><?php draw_input_field('report_' . $i, "ireport_" . $i, $_report, 'style="width: 99%;"'); ?></div>
    <?php
    };
    ?>
</div>
<div id="incident_report" class="border-solid spacer left">
    <div class="width20 border-right title bold" style="height: 60px; padding-top: 20px;">Reception Report</div>
    <div class="width80 border-bottom border-right" style="height: 60px;"><?php draw_textarea('incident_report', 'incident-report-textarea', $_report); ?></div>
    <!-- image upload code -->

    Photos For City To View:
    <input type='file' class='fileInput' multiple name='files[]'>
    <a href="//<?=$subdomain?>.bungyjapan.com/City/">View Uploaded Photos</a>


</div>
<br style="clear:both;"/>

<div id="buttons">
    <button id="back">Back</button>
    <button id="submit">Save</button>
</div>
</form>
</div>
<script>
    $(document).ready(function () {
        $('#back').click(function () {
            document.location = 'operationActivities.php?search_reports=1';
            return false;
        });
        $('#submit').click(function () {
            $('#daily-report-form').submit();
            return true;
        });
    });
</script>
<?php
// If there is report id set, update info in DataBase
if (isset($_GET['rid'])) {
    include 'reports_save.php';
    save_report('daily_reports', $_report);
};
?>

</body>
</html>
