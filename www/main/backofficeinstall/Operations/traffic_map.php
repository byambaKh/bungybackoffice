<?php
	include '../includes/application_top.php';
?>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">
<!--link href="css/style.css" rel="stylesheet" media="screen" type="text/css" /-->
<script src="js/functions.js" type="text/javascript"></script>

<?php include '../includes/head_scripts.php'; ?>
<title>Operations :: Traffic Map</title>
<style>
body {
background-color: white;
}
#total-container * {
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -ms-box-sizing: border-box;
    font-family: helvetica,sans-serif;
    font-size: 12px;
}
#total-container {
    width: 700px;
    text-align: center;
    margin-left: auto;
    margin-right: auto;
}
#map-title h1 {
	font-size: 20px;
}
#map-container {
    width: 800px;
    text-align: center;
    margin-left: auto;
    margin-right: auto;
	position: relative;
	overflow: hidden;
	height: 650px;
}
#map {
	width : 1000px;
	position: absolute;
	height: 800px;
	top: -30px;	
	left: -230px;
	overflow: hidden;
}
#map img {
	float: left;
	margin: 0px;
	padding: 0px;
	border: 0px;
	width: 250px;
	height: 205px;
}
#buttons {
	width: 100%;
/*
	top: 530px;
	position: absolute;
*/
	clear: both;
    margin-bottom: 60px;
    height: 49px;
    border: 1px solid #EEEEEE;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}
#buttons button {
    float: left;
    background-color: #FFFF66;
    border: 1px solid black;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    font-size: 14pt;
    padding: 5px;
    margin: 5px;
    padding-left: 10px;
    padding-right: 10px;
}
#buttons #back {
    float:left;
}
</style>
</head>
<body>
<?php include '../includes/main_menu.php'; ?>
<br />
<div id="total-container">
	<div id="map-title"><h1>Traffic Map</h1></div>
	<div id="map-container">
	<div id="map">
		<?php

          $site_id = CURRENT_SITE_ID;          

          if ($site_id == "1"|| $site_id == "2" || $site_id == "3" || $site_id == "9")
          { //MK SK Ya Ryu
            $map = "rhp100301";
            
          }else if($site_id == "4")
          {//Itsu
            $map = "rhp100901";            
            
          }else if($site_id == "10")
          {//Ka
            $map = "rhp100601";            
          }else
          {//
            $map = "rhp100501";                        
          }

          for ($i = 0; $i < 4; $i++) {
                for ($j = 0; $j < 4; $j++) {
                    echo '<img src="http://www.jartic.or.jp/map/highway/'.$map.'/'.$j.'_'.$i.'.gif?dummy='.rand(0,1).'" width="250" height="205" />' . "\n";
                };
            };
		?>
	</div>
	</div>
	<br style="clear:both;" />
  <div id="buttons">
    <button id="back">Back</button>
  </div>
</div>
<script>
$(document).ready(function () {
    $('#back').click(function () {
        document.location = 'operationActivities.php';
        return false;
    });
});
</script>

</body>
</html>
