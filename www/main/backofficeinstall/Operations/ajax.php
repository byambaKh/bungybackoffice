<?php
include '../includes/application_top.php';
mysql_query("SET NAMES UTF8;");
$result = array();
switch (TRUE) {
    case (isset($_GET['action']) && $_GET['action'] == 'save-psc-report'):
        $date = $_POST['report']['details']['date'];
        $siteId = $_POST['report']['details']['site_id'];

        $report = new PlatformSafetyCheck($siteId, $date);
        $report->save($_POST['report']);
        break;

    case (isset($_GET['action']) && $_GET['action'] == 'finish-upload'):
        //zip the contents of ...directory
        $directory = __DIR__ . "/../uploads/incidentReports/1/2015-04-03";
        if (file_exists($directory . ".zip")) {
            //Add the new uploads to the archive
        } else {
            //Create a new archive with the new files
            //Delete the image files
        }

        if (!empty($_FILES)) {
            $tempFile = $_FILES['file']['tmp_name'];
            $targetFile = $storeFolder . $_FILES['file']['name'];
            $successfulUpload = move_uploaded_file($tempFile, $targetFile);

            $upload = array(
                'userId'       => $_SESSION['uid'],
                'dateTime'     => $date,
                'siteId'       => CURRENT_SITE_ID,
                'url'          => $storeFolder,
                'filename'     => $_FILES['file']['name'],
                'originalName' => $_FILES['file']['name'],
                'storageName'  => $_FILES['file']['name'],
                'category'     => 'incidentReport',
            );
            db_perform("uploads", $upload);
        }

        //$options = array('add_path' => './', 'remove_all_path' => TRUE);
        //$zipFolder = new ZipArchive();
        //$zipFolder->open(dirname(__DIR__) . "/uploads/incidentReports/1/2015-04-03.zip", ZipArchive::OVERWRITE);
        //$zipFolder->addGlob("$directory/*.{png,jpg,jpeg,PNG,JPG,JPEG}", GLOB_BRACE, $options);
        //$zipFolder->close();

        //delete the files
        exec("rm -r $directory");
        echo '';
        //exec();
        break;

    case (isset($_GET['action']) && $_GET['action'] == 'upload-incident-photo'):
        $date = ifIsSet($_GET['date']);
        $siteId = ifIsSet($_GET['site']);

        $ds = DIRECTORY_SEPARATOR;

        $storeFolder = __DIR__ . "/../uploads/incidentReports/$siteId/$date/";//need to create a folder with the incident report id
        if (!file_exists($storeFolder))
            mkdir($storeFolder, 0777, true);//TODO set secure permissions

        if (!empty($_FILES)) {
            $tempFile = $_FILES['file']['tmp_name'];
            $targetFile = $storeFolder . $_FILES['file']['name'];
            $successfulUpload = move_uploaded_file($tempFile, $targetFile);

            $upload = array(
                'userId'       => $_SESSION['uid'],
                'dateTime'     => $date,
                'siteId'       => CURRENT_SITE_ID,
                'url'          => $storeFolder,
                'filename'     => $_FILES['file']['name'],
                'originalName' => $_FILES['file']['name'],
                'storageName'  => $_FILES['file']['name'],
                'category'     => 'incidentReport',
            );
            db_perform("uploads", $upload);
        }
        break;
};
Header("Content-type: application/json; encoding: utf-8");
echo json_encode($result);
