<?
include '../includes/application_top.php';
mysql_set_charset('utf8');

function setCurrentStatusActiveIfNoActiveCord()
{
// if we added new cord log, but there is no active cords exists for same CID1 and CID2
    //
    // 0:'Retired / 使用済';
    // 1:'Active  / 使用中';
    // 2:'Ready   / 用意';
    //
    //By default cords have a status of 2 (ready unless there is a status saved to the report)
    //search for the line with 'idstatus' in the form where this is set
    if ($_POST['i']['status'] == 2) {
        // Check if there is any active cord available
        $sql = "SELECT COUNT(*) AS total FROM cord_logs
				WHERE site_id = '" . CURRENT_SITE_ID . "'
					AND cid1 = '{$_POST['i']['cid1']}'
					AND cid2 = '{$_POST['i']['cid2']}'
					AND status = 1;";
        $res = mysql_query($sql) or die(mysql_error());
        //if there are no active cords, the status is set to 1 (active)
        if (($row = mysql_fetch_assoc($res)) && $row['total'] == 0) {
            $_POST['i']['status'] = 1;
        }
    }
}

function activateNextCordIfCurrentCordRetired()
{
// if we retire cord, then search next that can be active and make them active
    if ($_POST['i']['status'] == 0) {
        $sql = "SELECT * FROM cord_logs
				WHERE site_id = '" . CURRENT_SITE_ID . "'
					AND cid1 = '{$_POST['i']['cid1']}'
					AND cid2 = '{$_POST['i']['cid2']}'
					AND status = 2
				ORDER BY cid3 ASC
				LIMIT 1;";
        $res = mysql_query($sql) or die(mysql_error());
        if ($row = mysql_fetch_assoc($res)) {
            $data = array('status' => 1);
            db_perform('cord_logs', $data, 'update', 'id=' . $row['id']);
        }
    }
}

if (!empty($_POST) && array_key_exists('i', $_POST)) {
    include 'reports_save.php';
    $_POST['i']['site_id'] = CURRENT_SITE_ID;

    setCurrentStatusActiveIfNoActiveCord();

    // EOF Auto status from Ready to Active
    if ($res = save_report('cord_logs', $_POST['i'])) {
        $_SESSION['message'] = "Cord Log successfully saved!";
    } else {
        $_SESSION['message'] = "There is error for saving incident report";
    }
    activateNextCordIfCurrentCordRetired();

    header('Location: cord_logs.php');
    die();
}

if (isset($_GET['rid'])) {
    $sql = "SELECT * FROM cord_logs WHERE id='" . (int)$_GET['rid'] . "'";
    $res = mysql_query($sql) or die(mysql_error());
    $_report = mysql_fetch_assoc($res);
} else {
    $_report = array(
        'date' => date('Y-m-d')
    );
}

$staff_names = BJHelper::getStaffList('StaffListName', true);

$shouldGetCordInfo = (isset($_GET['rid']) && $_report['status'] == 1);
$allowEdit = !(isset($_GET['action']) && $_GET['action'] == 'view');

//After 2015-09-01 we change over to the new style of report
if (strtotime($_report['date']) < strtotime('2015-09-01')) {
    //get the old report
    require("cord_logs_report_version_1.php");
} else if (strtotime($_report['date']) < strtotime('2016-03-01')) {
    //the new cordlog report by Hiroshi, this was not completed due to time and safety concerns.
    //get the new report type
    require("cord_logs_report_version_2.php");
} else {
    //get the new report type
    require("cord_logs_report_version_2.php");
    //require("cord_logs_report_version_3.php");
}

