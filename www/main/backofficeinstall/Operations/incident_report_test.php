<?php
/*
// for shell exec
if ($HTTP_SERVER_VARS['argc'] > 2 && $HTTP_SERVER_VARS['argv'][1] == 'rid' && is_numeric($HTTP_SERVER_VARS['argv'][2])) {
    $_GET['action'] = 'view';
    $_GET['rid'] = $HTTP_SERVER_VARS['argv'][2];
    extract($_GET);
};*/

//to work locally on my machine as HTTP_SERVER_VARS does not exist...
//extract($_GET);

$lang = 0;
$hours = array(
    '1',
    '2',
    '3',
    '4',
    '5',
    '6',
    '7',
    '8',
    '9',
     '10',
     '11',
     '12',
     '13',
     '14',
     '15',
     '16',
     '17',
     '18',
     '19',
     '20',
     '21',
     '22',
     '23',
     '24',
);

$staff_positions = array('Jump Deck staff', 'Harnessing staff', 'Reception Staff', 'multiple', 'pond v','Participants non staff');

$weather_conditions = array('WeatherStation winch temp', 'DB meter', 'light meter', 'ane monster');

$descriptions = array('Injury', 'Operational Error', 'Suicide', 'Death', 'Equipment failure', 'Weather event', 'Car accident', 'Other');

$actions = array('Police notified', 'Ambulance', 'Both', 'None', 'Other');

$outsiders = array('Customer', 'Local', 'Tourist', 'Official');

$captions = array(
    'main_incident'                => 'INCIDENT REPORT',
    'date'                         => 'Report Date:',
    'pictures_supplied'            => 'Pictures Supplied',
    'noted_name'                   => 'Name of person who first noted incident:',
    'completed_name'               => 'Person completing report:',
    'noted_info'                   => 'Site, Date and time incident noted:',
    'site_closed_time'             => 'Site closed? At what time?',
    'crew_members_position'        => 'Crew members and positions:',
    'time_management_notified'     => 'Time management notified:',
    'weather_conditions'           => 'Weather conditions:',
    'incident_classification_code' => 'Classification of Incident - (Circle which applies)',
    'incident_description'         => 'Description of Incident: (include all equipment in use)',
    'action_taken'                 => 'Action taken:',
    'outside_services'             => 'Outside services required? Who, when were they notified and their response time?',
    'emergency_log'                => 'Emergency Log attached: (YES/NO)',
    'site_reopened_datetime'       => 'Date and time site reopened:',
    'reception_notified_time'      => 'Time Reception notified:',
    'completed_datetime'           => 'Date and Time completed:',
    'checked_and_signed'           => 'Checked and signed by JC and Management:',
    'management_follow_up'         => 'Management Follow Up',
    'incident_cause'               => 'What caused the incident?:',
    'system_fail'                  => 'Do you think one of our systems failed?',
    'customer_action'			   => 'Did the customers own actions cause their injury?',
    'zip_file'                     => 'Add new zip file',
    'back_button'                  => 'Back',
    'save_button'                  => 'Save',
    'Yes'                          => 'YES',
    'No'                           => 'NO'
);

$ic_codes = array(
    'CODE1' => array(
        'type'    => 'Major incident',
        'op_type' => 'Site closed indefinitely'
    ),
    'CODE2' => array(
        'type'    => 'Site standby',
        'op_type' => 'Site closed temporarily'
    ),
    'CODE3' => array(
        'type'    => 'Minor incident',
        'op_type' => 'Operations continue'
    ),
);

if ($_GET['language'] == 1){
    $lang = 1;
    $captions = array(
        'main_incident'                => 'インシデントレポート',
        'date'                         => '日付',
        'pictures_supplied'            => '写真あり？',
        'noted_name'                   => '最初にインシデントを発見した人',
        'completed_name'               => 'レポートを書いた??',
        'noted_info'                   => '日付・時間',
        'site_closed_time'             => 'サイト閉鎖？何時に？',
        'crew_members_position'        => 'クルーの名前とポジション',
        'time_management_notified'     => '発生した時間 ',
        'weather_conditions'           => '天候状況 ',
        'incident_classification_code' => 'インシデントの種類 － （○で囲んでください）',
        'incident_description'         => 'インシデントの説明 ',
        'action_taken'                 => 'どういう対応をすればよいか ',
        'outside_services'             => '緊急機関への連絡は必要か？誰がいつ連絡を取り、何分後にくるか？ ',
        'emergency_log'                => '緊急時用ログはありますか',
        'site_reopened_datetime'       => '営業再開の日付と時間',
        'reception_notified_time'      => '受付が通知する時間',
        'completed_datetime'           => 'レポート完成日・時間',
        'checked_and_signed'           => 'ジャンプコントローラーサイン<br /> マネージャーサイン',
        'management_follow_up'         => 'マネージャーからのアクション',
        'incident_cause'               => '事故の原因は何ですか？',
        'system_fail'                  => '会社の手順方針に落ち度があったと思いますか？',
        'customer_action'			   => 'それともお客様自身の行動が事故を招いたのですか？',
        'zip_file'                     => 'Zipファイルを添付する',
        'back_button'                  => '戻る',
        'save_button'                  => '保存',
        'Yes'                          => 'はい',
        'No'                           => 'いいえ'
    );

    $ic_codes = array(
        'CODE1' => array(
            'type' => '重大な事態',
            'op_type' => 'サイト無期限閉店'
        ),
        'CODE2' => array(
            'type' => 'サイト一時停止',
            'op_type' => 'サイト一時停止'
        ),
        'CODE3' => array(
            'type' => '軽度のもの',
            'op_type' => 'オペレーション進む'
        ),
    );

    $staff_positions = array('ジャンプデッキスタッフ', 'ハーネサー', '受付スタッフ', '複数', '池','参加者はスタッフではない');

    $weather_conditions = array('気象観測所 ウィンチ温度', 'DBメーター', 'ライトメーター', '??');

    $descriptions = array('怪我', 'オペレーション上の錯誤', '自殺', '致死', '機材故障', '気象現象', '自動車事故', 'その他');

    $actions = array('警察へ報告', '救急車', '両方', 'なし', 'その他');

    $outsiders = array('お客様', '現地在住の方', '観光客', '公職員');

}


$field_captions = array(
    'id'                           => 'Report ID:',
    'date'                         => 'Report Date:',
    'pictures_supplied'            => 'Pictures Supplied',
    'noted_name'                   => 'Name of person who first noted incident:<br /> 最初にインシデントを発見した人',
    'completed_name'               => 'Person completing report:<br /> レポートを書いた??',
    'noted_info'                   => 'Site, Date and time incident noted:<br /> 日付・時間',
    'site_closed_time'             => 'Site closed? At what time?<br /> サイト閉鎖？何時に？',
    'crew_members_position'        => 'Crew members and positions:<br /> クルーの名前とポジション',
    'time_management_notified'     => 'Time management notified:<br /> 発生した時間 ',
    'weather_conditions'           => 'Weather conditions:<br /> 天候状況 ',
    'incident_classification_code' => 'Classification of Incident - (Circle which applies)<br /> インシデントの種類 － （○で囲んでください）',
    'incident_description'         => 'Description of Incident: (include all equipment in use)<br /> インシデントの説明 ',
    'action_taken'                 => 'Action taken:<br /> どういう対応をすればよいか ',
    'outside_services'             => 'Outside services required? Who, when were they notified and their response time?<br /> 緊急機関への連絡は必要か？誰がいつ連絡を取り、何分後にくるか？ ',
    'emergency_log'                => 'Emergency Log attached: (YES/NO)<br /> 緊急時用ログはありますか',
    'site_reopened_datetime'       => 'Date and time site reopened:<br /> 営業再開の日付と時間',
    'reception_notified_time'      => 'Time Reception notified:<br /> 受付が通知する時間',
    'completed_datetime'           => 'Date and Time completed:<br /> レポート完成日・時間',
    'checked_and_signed'           => 'Checked and signed by JC and Management:<br /> ジャンプコントローラーサイン<br /> マネージャーサイン',
    'management_follow_up'         => 'Management Follow Up',
    'incident_cause'               => 'Cause of incident: <br /> 事故の原因は何ですか？',
    'system_fail'                  => 'Do you think one of our systems failed? <br /> 会社の手順方針に落ち度があったと思いますか？',
    'customer_action'			   => 'Did the customers own actions cause their injury? <br />それともお客様自身の行動が事故を招いたのですか？'
);
//$ic_codes = array(
//    'CODE1' => array(
//        'type'    => 'Major incident',
//        'type-jp' => '重大な事態',
//        'op_type' => 'Site closed indefinitely'
//    ),
//    'CODE2' => array(
//        'type'    => 'Site standby',
//        'type-jp' => 'サイト一時停止',
//        'op_type' => 'Site closed temporarily'
//    ),
//    'CODE3' => array(
//        'type'    => 'Minor incident',
//        'type-jp' => '軽度のもの',
//        'op_type' => 'Operations continue'
//    ),
//);
include '../includes/application_top.php';

mysql_set_charset('utf8');
if (!empty($_POST) && array_key_exists('i', $_POST)) {
    include 'reports_save.php';
    $_POST['i']['site_id'] = CURRENT_SITE_ID;

    $_POST['i']['pictures_supplied'] = ($_POST['i']['pictures_supplied'] ? "YES" : "NO");
    if ($res = save_report('incident_reports', $_POST['i'])) {
        //don't show these in the email

        $managementFollowUp = $_POST['i']['updated_management_follow_up'];//save it before we unset it
        unset($_POST['i']['updated_management_follow_up']);
        unset($_POST['i']['site_id']);
        $_SESSION['message'] = "Incident report successfully saved!";

        $content = '';
        foreach ($_POST['i'] as $key => $value) {
            //convert new lines to breaks
            $value = str_replace("\n", "<br />", $value);

            if ($key == 'incident_classification_code') {
                $value = implode("<br />", $ic_codes[$value]);
            };
            $content .= $field_captions[$key] . "<br />";
            $content .= '<b>' . $value . '</b><br /><br />';
        };

        $id = mysql_insert_id();//get the id for newly inserted rows
        if ($id == 0) {//if it is an update mysql insert id is zero so use the id in POST
            $id = $_POST['i']['id'];
        }

        $id = str_pad($id, 7, "0", STR_PAD_LEFT);

        $siteManagerEmail = $config['site_manager_email'];
        $generalManagerEmail = $config['general_manager_email'];

        //The iPhone version displays the wrong encoding the below fix solved that
        //http://stackoverflow.com/questions/5193599/php-utf8-mail-gets-garbled-on-iphone-mail-app
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=UTF-8\r\n";
        $headers .= "Content-Transfer-Encoding: quoted-printable\r\n";
        $title = SYSTEM_SUBDOMAIN . ': Incident Report #' . $id;
        if ($managementFollowUp == 1) $title .= " [Follow-up]";

        //This appears fine on the desktop but jumbled on the iPhone
        mail("beau@bungyjapan.com, $siteManagerEmail, dez@bungyjapan.com", $title, quoted_printable_encode($content), $headers);
        //mail("developer@standardmove.com", $title, quoted_printable_encode($content), $headers);

    } else {
        $_SESSION['message'] = "There is error for saving incident report (((";
    };

    header('Location: incident_report_list.php');
    die();
};
if (isset($_GET['rid'])) {
    $sql = "SELECT * FROM incident_reports WHERE id='" . (int)$_GET['rid'] . "'";
    $res = mysql_query($sql) or die(mysql_error());
    $_report = mysql_fetch_assoc($res);
} else {
    $_report = array(
        'date' => date('Y-m-d')
    );
};

$sql = "SELECT * FROM uploads WHERE site_id = " . CURRENT_SITE_ID . " AND meta1 = '{$_report['date']}' AND category = 'Incident Report'";
$uploads = queryForRows($sql);

$staff_names = BJHelper::getStaffList('StaffListName', true);
?>
<!DOCTYPE html>
<html>
<head>
    <?php include 'includes/header_tags.php'; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=8">
    <script src="js/functions.js" type="text/javascript"></script>
    <?php include '../includes/head_scripts.php'; ?>
    <title>Operations</title>
    <link rel="stylesheet" type="text/css" href="css/incident_report.css" media="all"/>
    <style>
        <?php
        if (isset($_GET['action']) && $_GET['action'] == 'view') {
        ?>
        #buttons #submit {
            visibility: hidden;
        }

        * input:disabled, * textarea:disabled {
            background-color: #FEFEFE;
            color: black;
            opacity: 1;
        }

        <?php
        };
        ?>
    </style>
    <script>
        $(function () {
            $mydate1 = $('#idate').datepicker({
                showAnim: 'fade',
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd',
                currentText: '<?php echo $_report['date']; ?>'
                //dateFormat: 'dd/mm/yy',
                //altField: '#alternate',
                //altFormat: 'yy-mm-dd',
                //minDate: '0d',
            });
        });

        $(document).ready(function () {
            //$("input").prop('disabled', 'disabled');
            //$("textarea").prop('disabled', 'disabled');
            $("#management_follow_up").change(function () {
                $("#updated_management_follow_up").val(1);
            });
        });
    </script>

    <?php
    if (isset($_GET['action']) && $_GET['action'] == 'view') {
        ?>
        <script>
            $(document).ready(function () {
                //$("input").prop('disabled', 'disabled');
                //$("textarea").prop('disabled', 'disabled');
            });
        </script>
        <?php
    };
    ?>
</head>
<body>
<?php include '../includes/main_menu.php'; ?>
<br/>

<div id="total-container">
    <div id="colorstrip">
        <a href="incident_report_test.php?language=1"> <img src="css/jp_flag.jpg" style="width:28px;height:20px;"> </a> &nbsp;&nbsp;
        <a href="incident_report_test.php?language=0"> <img src="css/en_flag.jpg" style="width:28px;height:20px;"> </a>
        <br>
    </div>

    <form id="incident-report-form" method="POST">
        <?php
        if (isset($_GET['rid'])) {
            echo "<input type=\"hidden\" name=\"i[id]\" value=\"" . (int)$_GET['rid'] . "\">";
        };
        ?>
        <div id="report-title"><?php echo SYSTEM_SUBDOMAIN.' '.$captions['main_incident']; ?></div>
        <div class="half">
            <div id="incident-date" class="left"><?php echo $captions['date']; ?></div>
            <div id="incident-date-value">
                <input type="text" id='idate' style="position: relative; z-index: 100000;" name="i[date]" value="<?php echo $_report['date']; ?>">
            </div>
        </div>
        <div id="picturesSupplied" class="half">
            <select name="i[pictures_supplied]">
                <option selected disabled><?php echo $captions['pictures_supplied']; ?></option>
                <option value="Yes"> <?php echo $captions['Yes']; ?> </option>
                <option value="No"> <?php echo $captions['No']; ?> </option>
            </select>
        </div>
        <div id="noted-name" class="left"><?php echo $captions['noted_name']; ?></div>
        <div id="noted-value" class="borderb margin275" style="border: none;"><?php draw_select('noted_name', $staff_names, 'inoted_name', $_report); ?></div>
<!--        <div id="noted-name-jp" class="trans-jp">最初にインシデントを発見した人</div>-->
        <br><br>
        <div class="half">
            <div id="completed-name" class="left"><?php echo $captions['completed_name']; ?></div>
            <div id="completed-value" class="borderb margin160"
                 style="border: none;"><?php draw_select('completed_name', $staff_names, 'icompleted_name', $_report); ?></div>
<!--            <div id="noted-datetime-jp" class="trans-jp">レポートを書いた人</div>-->
        </div>
        <div class="half">
<!--            <div id="noted-datetime" class="left">--><?php //echo $captions['noted_info']; ?><!--</div>-->
            <div id="noted-datetime-value" class="borderb margin205">
<!--                <input type="text" name="i[noted_info]"-->
<!--                                                                            value="--><?php //echo $_report['noted_info']; ?><!--">-->
                <select name="i[noted_infor]">
                        <option selected disabled><?php echo $captions['noted_info']; ?></option>
                    <?php foreach ($hours as $hour): ?>
                        <option value="<?php echo $hour; ?>"><?php echo $hour; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
<!--            <div id="noted-datetime-jp" class="trans-jp">日付・時間</div>-->
        </div>
        <div class="half">
<!--            <div id="site-closed-time" class="left"></div>-->
            <div id="site-closed-time-value" class="borderb margin160">
<!--                <input type="text" name="i[site_closed_time]" value="--><?php //echo $_report['site_closed_time']; ?><!--">-->
                <select name="i[site_closed_time]">
                        <option selected disabled><?php echo $captions['site_closed_time']; ?></option>
                    <?php foreach ($hours as $hour): ?>
                        <option value="<?php echo $hour; ?>"><?php echo $hour; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
<!--            <div id="crew-members-position-jp" class="trans-jp">サイト閉鎖？何時に？</div>-->
        </div>
        <div class="half">
<!--            <div id="crew-members-posiotion" class="left">--><?php //echo $captions['crew_members_position']; ?><!--</div>-->
            <div id="crew-members-posiotion-value" class="borderb margin175">
<!--                <input type="text" name="i[crew_members_position]" value="--><?php //echo $_report['crew_members_position']; ?><!--">-->
                <select name="i[crew_members_position]">
                    <option selected disabled><?php echo $captions['crew_members_position']; ?></option>
                    <?php foreach ($staff_positions as $staff_position): ?>
                        <option value="<?php echo $staff_position; ?>"><?php echo $staff_position; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
<!--            <div id="crew-members-position-jp" class="trans-jp">クルーの名前とポジション</div>-->
        </div>

        <div class="half">
            <div id="time-management-notified" class="left"><?php echo $captions['time_management_notified']; ?></div>
            <div id="time-management-notified-value" class="borderb margin160"><input type="text"
                                                                                      name="i[time_management_notified]"
                                                                                      value="<?php echo $_report['time_management_notified']; ?>">
            </div>
<!--            <div id="weather-conditions-jp" class="trans-jp">発生した時間</div>-->
        </div>
        <div class="half">
<!--            <div id="weather-conditions" class="left">--><?php //echo $captions['weather_conditions']; ?><!--</div>-->
            <div id="weather-conditions-value" class="borderb margin120">
<!--                <input type="text" name="i[weather_conditions]" value="--><?php //echo $_report['weather_conditions']; ?><!--">-->
                <select name="i[weather_conditions]">
                    <option selected disabled><?php echo $captions['weather_conditions']; ?></option>
                    <?php foreach ($weather_conditions as $weather_condition): ?>
                        <option value="<?php echo $weather_condition; ?>"><?php echo $weather_condition; ?></option>
                    <?php endforeach; ?>
                </select>
            </div>
<!--            <div id="weather-conditions-jp" class="trans-jp">天候状況</div>-->
        </div>
        <br /> <br/>
        <div id="incident-classification">
            <div id="incedent-classification-title" class="alignleft"><?php echo $captions['incident_classification_code']; ?></div>
<!--            <div id="incedent-classification-title-jp" class="trans-jp">インシデントの種類 － （○で囲んでください）</div>-->
            <div id="incedent-classification-title-values">
                <?php
                foreach ($ic_codes as $code => $value) {
                    $checked = ($code == $_report['incident_classification_code']) ? ' checked' : '';
                    ?>
                    <div><label for="cc-code-<?php echo strtolower($code); ?>"><input type="radio" id="cc-code-<?php echo strtolower($code); ?>" name="i[incident_classification_code]" value="<?php echo $code; ?>"<?php echo $checked; ?>><br/>
                            <?php echo $value['type']; ?><br/>
<!--                            --><?php //echo $value['type-jp']; ?><!--<br/>-->
                            <?php echo $value['op_type']; ?><br/></label>
                    </div>
                    <?php
                };
                ?>
            </div>
        </div><br/><br/>

<!--        <div id="incident-description-title" class="alignleft">--><?php //echo $captions['incident_description']; ?><!--</div>-->
<!--        <div id="incident-description-value">-->
<!--            <textarea name="i[incident_description]">--><?php //echo $_report['incident_description']; ?><!--</textarea></div>-->

        <select name="i[incident_description]">
            <option selected disabled><?php echo $captions['incident_description']; ?></option>
            <?php foreach ($descriptions as $description): ?>
                <option value="<?php echo $description; ?>"><?php echo $description; ?></option>
            <?php endforeach; ?>
        </select>
        <br>
        <br>

        <select name="i[action_taken]">
            <option selected disabled><?php echo $captions['action_taken']; ?></option>
            <?php foreach ($actions as $action): ?>
                <option value="<?php echo $action; ?>"><?php echo $action; ?></option>
            <?php endforeach; ?>
        </select>
        <br>
        <br>

<!--        <div id="action-taken-title" class="alignleft">--><?php //echo $captions['action_taken']; ?><!--</div>-->
<!--        <div id="incident-description-value">-->
<!--            <textarea name="i[action_taken]">--><?php //echo $_report['action_taken']; ?><!--</textarea></div>-->

        <select name="i[outside_services]">
            <option selected disabled><?php echo $captions['outside_services']; ?></option>
            <?php foreach ($outsiders as $outsider): ?>
                <option value="<?php echo $outsider; ?>"><?php echo $outsider; ?></option>
            <?php endforeach; ?>
        </select>
        <br>
        <br>

<!--        <div id="ourside-services-title" class="alignleft">--><?php //echo $captions['outside_services']; ?><!--</div>-->
<!--        <div id="outside-services-value">-->
<!--            <textarea name="i[outside_services]">--><?php //echo $_report['outside_services']; ?><!--</textarea></div>-->

        <select name="i[emergency_log]">
            <option selected disabled><?php echo $captions['emergency_log']; ?></option>
            <option value="Yes"> <?php echo $captions['Yes']; ?> </option>
            <option value="No"> <?php echo $captions['No']; ?> </option>
        </select>
        <br>
        <br>

<!--        <div style="height: 10px;"></div>-->
<!--        <div id="emergency-log" class="left">--><?php //echo $captions['emergency_log']; ?><!--</div>-->
<!--        <div id="emergency-log-value" class="borderb" style="margin-left: 210px;"><input type="text"-->
<!--                                                                                         name="i[emergency_log]"-->
<!--                                                                                         value="--><?php //echo $_report['emergency_log']; ?><!--">-->
<!--        </div>-->
<!--        <div id="-jp" class="trans-jp">緊急時用ログはありますか</div>-->

        <select name="i[site_reopened_datetime]">
            <option selected disabled><?php echo $captions['site_reopened_datetime']; ?></option>
            <?php foreach ($hours as $hour): ?>
                <option value="<?php echo $hour; ?>"><?php echo $hour; ?></option>
            <?php endforeach; ?>
        </select>
        <br>
        <br>

<!--        <div id="site-reopened" class="left">--><?php //echo $captions['site_reopened_datetime']; ?><!--</div>-->
<!--        <div id="site-reopened-value" class="borderb margin175"><input type="text" name="i[site_reopened_datetime]"-->
<!--                                                                       value="--><?php //echo $_report['site_reopened_datetime']; ?><!--">-->
<!--        </div>-->
<!--        <div id="site-reopened-jp" class="trans-jp">営業再開の日付と時間</div>-->

        <select name="i[reception_notified_time]">
            <option selected disabled><?php echo $captions['reception_notified_time']; ?></option>
            <?php foreach ($hours as $hour): ?>
                <option value="<?php echo $hour; ?>"><?php echo $hour; ?></option>
            <?php endforeach; ?>
        </select>
        <br>
        <br>

<!--        <div id="reception-notified" class="left">--><?php //echo $captions['reception_notified_time']; ?><!--</div>-->
<!--        <div id="reception-notified-value" class="borderb margin175"><input type="text"-->
<!--                                                                            name="i[reception_notified_time]"-->
<!--                                                                            value="--><?php //echo $_report['reception_notified_time']; ?><!--">-->
<!--        </div>-->
<!--        <div id="reception-notified-jp" class="trans-jp">受付が通知する時間</div>-->

        <select name="i[completed_datetime]">
            <option selected disabled><?php echo $captions['completed_datetime']; ?></option>
            <?php foreach ($hours as $hour): ?>
                <option value="<?php echo $hour; ?>"><?php echo $hour; ?></option>
            <?php endforeach; ?>
        </select>
        <br>
        <br>

<!--        <div id="completed-datetime" class="left">--><?php //echo $captions['completed_datetime']; ?><!--</div>-->
<!--        <div id="completed-datetime-value" class="borderb margin175"><input type="text" name="i[completed_datetime]"-->
<!--                                                                            value="--><?php //echo $_report['completed_datetime']; ?><!--">-->
<!--        </div>-->
<!--        <div id="completed-datetime-jp" class="trans-jp">レポート完成日・時間</div>-->

        <div id="incident-cause" class="left"><?php echo $captions['incident_cause']; ?></div>
        <div id="incident-cause-value" class="borderb margin175"><input type="text" name="i[incident_cause]"
                                                                        value="<?php echo $_report['incident_cause']; ?>">
        </div>
<!--        <div id="incident-cause-jp" class="trans-jp">事故の原因は何ですか？</div>-->

        <div id="system-fail" class="left"><?php echo $captions['system_fail']; ?></div>
        <div id="system-fail-value" class="borderb margin275"><input type="text" name="i[system_fail]"
                                                                     value="<?php echo $_report['system_fail']; ?>">
        </div>
<!--        <div id="system-fail-jp" class="trans-jp">会社の手順方針に落ち度があったと思いますか</div>-->

        <div id="customer-action" class="left"><?php echo $captions['customer_action']; ?></div>
        <div id="customer-action-value" class="borderb margin300"><input type="text" name="i[customer_action]"
                                                                         value="<?php echo $_report['customer_action']; ?>">
        </div>
<!--        <div id="customer-action-jp" class="trans-jp">それともお客様自身の行動が事故を招いたのですか？</div>-->


        <div id="checked-and-signed" class="left"><?php echo $captions['checked_and_signed']; ?></div>
        <div id="checked-and-signed-value" class="borderb"
             style="margin-left: 265px; border: none;"><?php draw_select('checked_and_signed', $staff_names, 'ichecked_and_signed', $_report); ?></div>
<!--        <div id="checked-and-signed-jp" class="trans-jp">ジャンプコントローラーサイン</div>-->
<!--        <div class="trans-jp">マネージャーサイン</div>-->

        <div id="management-follow-up-title" class="alignleft"><?php echo $captions['management_follow_up']; ?><br/></div>
        <div id="management-follow-up-value">
            <textarea id="management_follow_up" name="i[management_follow_up]"><?php echo $_report['management_follow_up']; ?></textarea>
        </div>
        <input type="hidden" name="i[updated_management_follow_up]" id="updated_management_follow_up" value="0">

    </form>
    <a href="#upload-zip" id="upload-zip"><button id="add-zip-file" type="button"><?php echo $captions['zip_file']; ?></button></a>
    <br>
    <br>
    <?php
    foreach ($uploads as $key => $upload) {
        echo "<a href='downloadIncidentPhotos.php?fileId={$upload['id']}'>{$upload['filename']}</a> <br>";
    }
    ?>
    <div id="buttons">
        <button id="back"><?php echo $captions['back_button']; ?></button>
        <button id="submit"><?php echo $captions['save_button']; ?></button>
    </div>

</div>
<script>
    $(document).ready(function () {
        $('#upload-zip').click(function (event) {
            event.preventDefault();
            window.open("imageUpload.php?date=<?=$_report['date']?>", "_blank", "location=no, menubar=no, status=no, height=250, width=300");
        });

        $('#back').click(function () {
            document.location = 'incident_report_list.php';
            return false;
        });

        $('#submit').click(function () {
            $('#incident-report-form').submit();
            return true;
        });
    });
</script>
</body>
</html>
