<?php
include '../includes/application_top.php';
mysql_set_charset('utf8');
if (!empty($_POST) && array_key_exists('i', $_POST)) {
    include 'reports_save.php';
    $_POST['i']['site_id'] = CURRENT_SITE_ID;
    if (save_report('psc_reports', $_POST['i'])) {
        $_SESSION['message'] = "Platform Safety Checklist report successfully saved!";
    } else {
        $_SESSION['message'] = "There is error for saving Platform Safety Checklist report (((";
    };
    Header('Location: operationActivities.php');
    die();
};
if (isset($_GET['rid'])) {
    $sql = "SELECT * FROM psc_reports WHERE id='" . (int)$_GET['rid'] . "'";
    $res = mysql_query($sql) or die(mysql_error());
    $_report = mysql_fetch_assoc($res);
} else {
    $_report = array(
        'date' => date('Y-m-d')
    );
};
$staff_names = BJHelper::getStaffList('StaffListName', true);
function draw_select_yn($name, $id, $report, $params = '')
{
    if (!empty($params)) $params = ' ' . $params;
    $value = '';
    if (array_key_exists($name, $report)) {
        $value = $report[$name];
    };
    $values = array(
        ''  => '',
        'y' => '良',
        'n' => '否'
    );
    echo "<select name=\"i[$name]\"$params>";
    foreach ($values as $v => $title) {
        $selected = ($v == $value) ? ' selected="selected"' : '';
        echo "<option value=\"$v\"$selected>$title</option>";
    };
    echo "</select>";
}

?>
<!DOCTYPE html>
<html>
<head>
    <?php include 'includes/header_tags.php'; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=8">
    <script src="js/functions.js" type="text/javascript"></script>
    <?php include '../includes/head_scripts.php'; ?>
    <title>Operations</title>
    <style>
        #total-container * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -ms-box-sizing: border-box;
            z-index: -100;
            font-family: helvetica, sans-serif;
            font-size: 10pt;
        }

        #total-container * {
            z-index: 100;
        }

        #total-container {
            width: 730px;
            text-align: center;
            margin-left: auto;
            margin-right: auto;
            position: relative;
        }

        #total-container div {
            /*height: 20px;*/
        }

        #total-container * input {
            width: 99%;
            height: 99%;
            border: 1px solid #EEEEEE;
        }

        #total-container * textarea {
            width: 99%;
            height: 99%;
            border: 1px solid #EEEEEE;
            height: 180px;
        }

        #report-title {
            padding-top: 20px;
            padding-bottom: 40px;
            font-size: 16pt;
            font-weight: bold;
            text-decoration: underline;
            text-align: center;
        }

        .left {
            float: left;
            height: 20px;
        }

        #report-date {
            width: 80px;
        }

        #incident-date-value {
            width: 100px;
            height: 20px;
            margin-right: auto;
            float: left;
        }

        #incident-date-value input {
            margin-left: 0px !important;
        }

        #report-container {
            border: 1px solid black;
            margin: 0px;
            margin-top: 20px;
            padding: 0px;
        }

        #report-header {
            border: 1px solid black;
            margin: 0px;
            padding: 0px;
            clear: both;
            height: 42px;
        }

        #total-container * .item-number {
            float: left;
            height: 100%;
            width: 40px;
            border-right: 2px solid black;
            font-size: 16px;
            line-height: 40px;
        }

        .item-value {
            float: right;
            height: 100%;
            width: 80px;
            border-left: 2px solid black;
            padding-top: 2px;
        }

        #report-header .item-value {
            padding-top: 10px;
            font-size: 18px;
        }

        #staff-header {
            float: left;
            height: 40px;
            width: 200px;
            border-right: 1px solid black;
            padding-top: 10px;
        }

        #staff-inspector {
            float: left;
            height: 40px;
            width: 350px;
            padding-top: 4px;
            text-align: left;
            margin-left: 10px;
        }

        #staff-inspector select {
            font-size: 24px;
            height: 30px;
            width: 350px;
        }

        #staff-inspector select option {
            font-size: 24px;
        }

        #total-container * .item-value select {
            font-size: 26px;
            height: 36px;
            width: 75px;
            text-indent: 0px;
            padding: 0px;
        }

        #total-container * .item-value select option {
            font-size: 26px;
        }

        .report-line {
            border: 1px solid black;
            margin: 0px;
            padding: 0px;
            height: 42px;
            clear: both;
        }

        .extended-line {
            height: 200px !important;
        }

        .item-question-container {
            height: 40px;
            width: 606px;
            float: left;
        }

        .item-question-jp {
            float: left;
            width: 606px;
            border-bottom: 1px solid black;
            height: 20px;
            line-height: 20px;
            text-align: left;
            padding-left: 10px;
        }

        .item-question-en {
            float: left;
            width: 606px;
            height: 20px;
            line-height: 20px;
            text-align: left;
            padding-left: 10px;
        }

        .report-footer {
            border: 1px solid black;
            margin: 0px;
            padding: 0px;
            clear: both;
        }

        #report-notes-title {
            text-align: left;
            padding-left: 0.5%;
        }

        .required-items-list {
            margin-top: 42px;
            width: 100%;
            height: 200px;
            border-top: 2px solid black;
            padding-top: 5px;
        }

        .items-list {
            width: 200px;
            float: left;
            margin-left: 50px;
            text-align: left;
        }

        #buttons {
            clear: both;
            margin-top: 30px;
            margin-bottom: 60px;
            height: 49px;
            border: 1px solid #EEEEEE;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
        }

        #buttons button {
            float: right;
            background-color: #FFFF66;
            border: 1px solid black;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            font-size: 14pt;
            padding: 5px;
            margin: 5px;
            padding-left: 10px;
            padding-right: 10px;
        }

        #buttons #back {
            float: left;
        }

        <?php
        if (isset($_GET['action']) && $_GET['action'] == 'view') {
        ?>
        #buttons #submit {
            visibility: hidden;
        }

        * input:disabled, * textarea:disabled {
            background-color: #FEFEFE;
            color: black;
            opacity: 1;
        }

        <?php
        };
        ?>
    </style>
    <?php
    if (isset($_GET['action']) && $_GET['action'] == 'view') {
        ?>
        <script>
            $(document).ready(function () {
                $("input").prop('disabled', 'disabled');
                //$("textarea").prop('disabled', 'disabled');
            });
        </script>
    <?php
    } else {
    ?>
        <script>
            $(document).ready(function () {
                $("#incident-date-value input").datepicker({
                    showAnim: 'fade',
                    numberOfMonths: 1,
                    dateFormat: 'yy-mm-dd',
                    currentText: 'Today'
                });
            });
        </script>
    <?php
    };
    ?>
</head>
<body>
<?php include '../includes/main_menu.php'; ?>
<br/>

<div id="total-container">
    <form id="psc-report-form" method="POST" action="psc_report.php">
        <?php
        if (isset($_GET['rid'])) {
            echo "<input type=\"hidden\" name=\"i[id]\" value=\"" . (int)$_GET['rid'] . "\">";
        };
        ?>
        <div id="report-title">Daily Checksheet</div>
        <div id="report-date" class="left">Report Date:</div>
        <div id="incident-date-value"><input type="text" name="i[date]" value="<?php echo $_report['date']; ?>"></div>
        <br/>
        <br/>

        <div id="report-container">
            <div id="report-header">
                <div class="item-number"></div>
                <div id="staff-header">点検実地者・Inspecting Staff:</div>
                <div id="staff-inspector"><?php echo draw_pull_down_menu('i[staff_inspector]', $staff_names, $_report['staff_inspector'], 'id="istaff_inspector"'); ?></div>
                <div class="item-value">良・否</div>
            </div>
            <div class="report-line">
                <div class="item-number">1)</div>
                <div class="item-question-container">
                    <div class="item-question-jp">壊れていないか？</div>
                    <div class="item-question-en">Visual check…All normal?</div>
                </div>
                <div class="item-value"><?php draw_select_yn('visual_check', 'ivisual-check', $_report); ?></div>
            </div>
            <div class="report-line">
                <div class="item-number">2)</div>
                <div class="item-question-container">
                    <div class="item-question-jp">異常(動かない、きしみ、傾き）はないか？</div>
                    <div class="item-question-en">No loose parts, no creaking noises; nothing sitting at a strange angle?</div>
                </div>
                <div class="item-value"><?php draw_select_yn('loose_parts', 'iloose-parts', $_report); ?></div>
            </div>
            <div class="report-line">
                <div class="item-number">3)</div>
                <div class="item-question-container">
                    <div class="item-question-jp">床面に凸凹、段差ないか？</div>
                    <div class="item-question-en">The floor is level; no uneven parts to cause one to trip?</div>
                </div>
                <div class="item-value"><?php draw_select_yn('floor_level', 'ifloor-level', $_report); ?></div>
            </div>
            <div class="report-line">
                <div class="item-number">4)</div>
                <div class="item-question-container">
                    <div class="item-question-jp">床面に隙間がないか？</div>
                    <div class="item-question-en">No gaps in the flooring; floor connections are intact?</div>
                </div>
                <div class="item-value"><?php draw_select_yn('floor_gaps', 'ifloor-gaps', $_report); ?></div>
            </div>
            <div class="report-line">
                <div class="item-number">5)</div>
                <div class="item-question-container">
                    <div class="item-question-jp">手すりがきちんと固定されているか？</div>
                    <div class="item-question-en">Handrail is secure, hangrail hinges are intact and sliding bolts in place?</div>
                </div>
                <div class="item-value"><?php draw_select_yn('handrail_secure', 'ihandrail-secure', $_report); ?></div>
            </div>
            <div class="report-line">
                <div class="item-number">6)</div>
                <div class="item-question-container">
                    <div class="item-question-jp">突起物，角などの接触した時に危険な所はないか？</div>
                    <div class="item-question-en">Corners &amp; edges are smooth &amp; even; no sharp edges to damage cord?</div>
                </div>
                <div class="item-value"><?php draw_select_yn('corners_edges', 'icorners-edges', $_report); ?></div>
            </div>
            <div class="report-line">
                <div class="item-number">7)</div>
                <div class="item-question-container">
                    <div class="item-question-jp">プラットホーム内に不要物が置かれてないか？</div>
                    <div class="item-question-en">No unnecessary articles inside the platform?</div>
                </div>
                <div class="item-value"><?php draw_select_yn('no_unnecessary_articles', 'ino-unnecessary-articles', $_report); ?></div>
            </div>
            <div class="report-line">
                <div class="item-number">8)</div>
                <div class="item-question-container">
                    <div class="item-question-jp">ボルト・ナットなどの緩み，脱落、パーツの型付はないか？
                        （※部材G, H①②③④、A①⑤、E②⑥）
                    </div>
                    <div class="item-question-en">Nuts &amp; Bolts are appear tight; haven't loosened due to bridge movement?</div>
                </div>
                <div class="item-value"><?php draw_select_yn('nuts_bolts', 'inuts-bolts', $_report); ?></div>
            </div>
            <div class="report-line">
                <div class="item-number">9)</div>
                <div class="item-question-container">
                    <div class="item-question-jp">ひび、破損、腐食、劣化、塗料の剥離などの部材の異常はないか？</div>
                    <div class="item-question-en">No cracks, fissures, corrosion, peeling or discoloured galvanizing?</div>
                </div>
                <div class="item-value"><?php draw_select_yn('no_cracks', 'ino-cracks', $_report); ?></div>
            </div>
            <div class="report-line">
                <div class="item-number">10)</div>
                <div class="item-question-container">
                    <div class="item-question-jp">落下防止ワイヤーが外れていないか？</div>
                    <div class="item-question-en">Safety lines are present and securely connected?</div>
                </div>
                <div class="item-value"><?php draw_select_yn('safety_lines', 'isafety-lines', $_report); ?></div>
            </div>
            <div class="report-line">
                <div class="item-number">11)</div>
                <div class="item-question-container">
                    <div class="item-question-jp"> レスキュー用具が全て揃っている?</div>
                    <div class="item-question-en">All required items are present in the Rescue Bag?</div>
                    <!--div class="required-items-list">
                        <div class="items-list">
                            セルフブレーキ下降器 X 1<br />
                            スチールカラビナ(大) X 1<br />
                            スチールカラビナ(小) X 1<br />
                            アルミカラビナ X 5<br />
                            プルージックコード(短) X 1<br />
                            プルージックコード(長) X １<br />
                            ジュマー X 1<br />
                            Ｖブレーキ ウェビング X 3<br />
                            下降用ロープ(60m) X 1
                        </div>
                        <div class="items-list">
                            ID Descender X 1<br />
                            Steel Carabiner (Big) X 1<br />
                            Steel Carabiner (Small) X 1<br />
                            Alloy Carabiner X 5<br />
                            Prusik Cord (Short) X 1<br />
                            Prusik Cord (Long) X 1<br />
                            Jumar Ascender X 1<br />
                            V-Brake Webbing X 3<br />
                            Rope (60m) X 1
                        </div>
                    </div-->
                </div>
                <div class="item-value"><?php draw_select_yn('items_present', 'items-present', $_report); ?></div>
            </div>
            <div class="report-line">
                <div class="item-number">12)</div>
                <div class="item-question-container">
                    <div class="item-question-jp">OPS用具が全て揃っている?</div>
                    <div class="item-question-en">All required items are present in the OPS Bag?</div>
                </div>
                <div class="item-value"><?php draw_select_yn('items_present_ops', 'items-present-ops', $_report); ?></div>
            </div>
            <div class="report-line">
                <div class="item-number">13)</div>
                <div class="item-question-container">
                    <div class="item-question-jp">リカバリー用具が全て揃っている(MKのみ)?</div>
                    <div class="item-question-en">All required items are present in the Recovery Bag (MK only)?</div>
                </div>
                <div class="item-value"><?php draw_select_yn('items_present_recovery', 'items-present-recovery', $_report); ?></div>
            </div>
            <div class="report-line">
                <div class="item-number">14)</div>
                <div class="item-question-container">
                    <div class="item-question-jp">全てのハーネスが揃っている?</div>
                    <div class="item-question-en">All harnesses are present and in good condition?</div>
                </div>
                <div class="item-value"><?php draw_select_yn('items_present_harn', 'items-present-harn', $_report); ?></div>
            </div>
            <div class="report-line">
                <div class="item-number">15)</div>
                <div class="item-question-container">
                    <div class="item-question-jp">全てのバンジーコードが揃っている?</div>
                    <div class="item-question-en">All bungy cords are present and in good condition?</div>
                </div>
                <div class="item-value"><?php draw_select_yn('items_present_cords', 'items-present-cords', $_report); ?></div>
            </div>
            <div class="report-footer">
                <div id="report-notes-title">備考・Notes:</div>
                <div id="report-notes-value"><?php draw_textarea('notes', 'inotes', $_report); ?></div>
            </div>
        </div>

        <div id="buttons">
            <button id="back">Back</button>
            <button id="submit">Save</button>
        </div>


    </form>
</div>
<script>
    $(document).ready(function () {
        $('#back').click(function () {
            document.location = 'operationActivities.php?search_reports=1';
            return false;
        });
        $('#submit').click(function () {
            document.empty_select = null;
            $("#psc-report-form select").each(function () {
                //alert($(this).val());
                if ($(this).val() == '') document.empty_select = $(this).prop('id');
            });
            //alert(document.empty_select);
            if (document.empty_select != null) {
                alert('You must select all drop-downs value on this page');
                return false;
            }
            $('#psc-report-form').submit();
            return true;
        });
    });
</script>
</body>
</html>
