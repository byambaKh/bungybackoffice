<?php
  include '../includes/application_top.php';

	mysql_set_charset('utf8');

$cDate = date("Y-m-d");


?>
<!DOCTYPE html>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">
<link href="css/style.css" rel="stylesheet" media="screen" type="text/css" />
<script src="js/functions.js" type="text/javascript"></script>

<?php include '../includes/head_scripts.php'; ?>
<title>Incident Reports</title>
<script type="text/javascript" charset="utf-8">
// Buttons actions
$(document).ready(function () {
	$(".edit, .view").on('click', function () {
		document.location = 'incident_report.php?action=' + $(this).attr('class') + '&rid=' + $(this).attr('custom-id');
		return false;
	});
	$("#back").on('click', function () {
		document.location = '/Operations/';
		return false;
	});
	$("#new-report").on('click', function () {
		document.location = 'incident_report.php';
		return false;
	});
});
</script>
<style>
#container {
	width: 700px;
	margin-left: auto;
	margin-right: auto;
}
h1 {
	width: 100%;
	text-align: center;
}
#last-bookings-table {
	text-align: center;
	background-color: white;
	width: 700px;
}
#last-bookings-table th {
	text-align: center;
	background-color: black;
	color: white;
}
#last-bookings-table td.bdate, #last-bookings-table th.bdate {
	text-align: right;
	padding-right: 10px;
}
#last-bookings-table td {
	background-color: red;
	text-align: center;
	color: white;
}
pre {
	margin: 0px;
}
#info_message {
	text-align: center;
}
.descr {
	text-align: left !important;
}
#buttons {
    margin-top: 10px;
    margin-bottom: 20px;
    height: 49px;
    border: 1px solid #EEEEEE;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}
#buttons button {
    float: left;
    background-color: #FFFF66;
    border: 1px solid black;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    font-size: 14pt;
    padding: 5px;
    margin: 5px;
    padding-left: 10px;
    padding-right: 10px;
}
#buttons #new-report {
    float:right;
}

</style>
</head>
<body>
<?php include '../includes/main_menu.php'; ?>
<?php
	if (array_key_exists('message', $_SESSION) && !empty($_SESSION['message'])) {
		echo "<div id='info_message'>{$_SESSION['message']}</div>";
		unset($_SESSION['message']);
	};
?>
<div id="container">
<form method="post" name="lsform" onsubmit="return OnSubmitForm();">
<div id="page-title"><h1>Incident and Issue reports</h1></div>
  <div id="buttons">
    <button id="back">Back</button>
    <button id="new-report">Create New Report</button>
  </div>
<table cellspacing="1" cellpadding="3" id="last-bookings-table" align="center">
<tr>
    <th>ID</th>
    <th>Date</th>
    <th>Description</th>
    <th>Management <br> Follow Up</th>
    <th>Actions</th>
</tr>
<?php
	$where = '';
	if (defined("CURRENT_SITE_ID") && CURRENT_SITE_ID > 0) {
		$where = " WHERE site_id = '" . CURRENT_SITE_ID . "' ";
	};

	$sql = "select 'Incident' as report_type, id, `date`, incident_description, management_follow_up from incident_reports $where ORDER BY `date` DESC";
	$res = mysql_query($sql) or die('No reports found');
	$reports_found = false;
	$file = 'incident_report.php';
	mb_internal_encoding('UTF-8');
	while ($row = mysql_fetch_assoc($res)) {
		$reports_found = true;
		$descr = mb_substr($row['incident_description'], 0, 270);
		mb_regex_encoding('UTF-8');
		$descr = mb_ereg_replace("/([^\s^\n]{40})/u", "\\1 ", $descr);
?>
<tr>
    <td><?php echo $row['id']; ?></td>
    <td><?php echo $row['date']; ?></td>
    <td class="descr"><?php echo $descr; ?></td>
    <td>
        <?php
            if(strlen($row['management_follow_up'])) echo "<img src='/img/checkmark.png' width='50' height='50'>";
        ?>
    </td>
    <td>
<nobr>
		<button class="view" custom-id="<?php echo $row['id']; ?>">View</button>
		<button class="edit" custom-id="<?php echo $row['id']; ?>">Edit</button>
</nobr>
	</td>
</tr>
<?php
	};
	if (!$reports_found) {
		echo "<tr><td colspan=\"5\">No reports found for this date</td></tr>";
	};
?>
</table>
	 		
</form>
</div>
</body>

</html>
<?php 
include ("ticker.php");
?>
