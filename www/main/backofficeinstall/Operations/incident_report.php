<?php
/*
// for shell exec
if ($HTTP_SERVER_VARS['argc'] > 2 && $HTTP_SERVER_VARS['argv'][1] == 'rid' && is_numeric($HTTP_SERVER_VARS['argv'][2])) {
    $_GET['action'] = 'view';
    $_GET['rid'] = $HTTP_SERVER_VARS['argv'][2];
    extract($_GET);
};*/

//to work locally on my machine as HTTP_SERVER_VARS does not exist...
//extract($_GET);

$field_captions = array(
    'id'                           => 'Report ID:',
    'date'                         => 'Report Date:',
    'pictures_supplied'            => 'Pictures Supplied',
    'noted_name'                   => 'Name of person who first noted incident:<br /> 最初にインシデントを発見した人',
    'completed_name'               => 'Person completing report:<br /> レポートを書いた??',
    'noted_info'                   => 'Site, Date and time incident noted:<br /> 日付・時間',
    'site_closed_time'             => 'Site closed? At what time?<br /> サイト閉鎖？何時に？',
    'crew_members_position'        => 'Crew members and positions:<br /> クルーの名前とポジション',
    'time_management_notified'     => 'Time management notified:<br /> 発生した時間 ',
    'weather_conditions'           => 'Weather conditions:<br /> 天候状況 ',
    'incident_classification_code' => 'Classification of Incident - (Circle which applies)<br /> インシデントの種類 － （○で囲んでください）',
    'incident_description'         => 'Description of Incident: (include all equipment in use)<br /> インシデントの説明 ',
    'action_taken'                 => 'Action taken:<br /> どういう対応をすればよいか ',
    'outside_services'             => 'Outside services required? Who, when were they notified and their response time?<br /> 緊急機関への連絡は必要か？誰がいつ連絡を取り、何分後にくるか？ ',
    'emergency_log'                => 'Emergency Log attached: (YES/NO)<br /> 緊急時用ログはありますか',
    'site_reopened_datetime'       => 'Date and time site reopened:<br /> 営業再開の日付と時間',
    'reception_notified_time'      => 'Time Reception notified:<br /> 受付が通知する時間',
    'completed_datetime'           => 'Date and Time completed:<br /> レポート完成日・時間',
    'checked_and_signed'           => 'Checked and signed by JC and Management:<br /> ジャンプコントローラーサイン<br /> マネージャーサイン',
    'management_follow_up'         => 'Management Follow Up',
    'incident_cause'               => 'Cause of incident: <br /> 事故の原因は何ですか？',
    'system_fail'                  => 'Do you think one of our systems failed? <br /> 会社の手順方針に落ち度があったと思いますか？',
    'customer_action'			   => 'Did the customers own actions cause their injury? <br />それともお客様自身の行動が事故を招いたのですか？'  
    
);
$ic_codes = array(
    'CODE1' => array(
        'type'    => 'Major incident',
        'type-jp' => '重大な事態',
        'op_type' => 'Site closed indefinitely'
    ),
    'CODE2' => array(
        'type'    => 'Site standby',
        'type-jp' => 'サイト一時停止',
        'op_type' => 'Site closed temporarily'
    ),
    'CODE3' => array(
        'type'    => 'Minor incident',
        'type-jp' => '軽度のもの',
        'op_type' => 'Operations continue'
    ),
);
include '../includes/application_top.php';

mysql_set_charset('utf8');
if (!empty($_POST) && array_key_exists('i', $_POST)) {
    include 'reports_save.php';
    $_POST['i']['site_id'] = CURRENT_SITE_ID;

    $_POST['i']['pictures_supplied'] = ($_POST['i']['pictures_supplied'] ? "YES" : "NO");
    if ($res = save_report('incident_reports', $_POST['i'])) {
        //don't show these in the email

        $managementFollowUp = $_POST['i']['updated_management_follow_up'];//save it before we unset it
        unset($_POST['i']['updated_management_follow_up']);
        unset($_POST['i']['site_id']);
        $_SESSION['message'] = "Incident report successfully saved!";

        $content = '';
        foreach ($_POST['i'] as $key => $value) {
            //convert new lines to breaks
            $value = str_replace("\n", "<br />", $value);

            if ($key == 'incident_classification_code') {
                $value = implode("<br />", $ic_codes[$value]);
            };
            $content .= $field_captions[$key] . "<br />";
            $content .= '<b>' . $value . '</b><br /><br />';
        };

        $id = mysql_insert_id();//get the id for newly inserted rows
        if ($id == 0) {//if it is an update mysql insert id is zero so use the id in POST
            $id = $_POST['i']['id'];
        }

        $id = str_pad($id, 7, "0", STR_PAD_LEFT);

        $siteManagerEmail = $config['site_manager_email'];
        $generalManagerEmail = $config['general_manager_email'];

        //The iPhone version displays the wrong encoding the below fix solved that
        //http://stackoverflow.com/questions/5193599/php-utf8-mail-gets-garbled-on-iphone-mail-app
        $headers = "MIME-Version: 1.0\r\n";
        $headers .= "Content-type: text/html; charset=UTF-8\r\n";
        $headers .= "Content-Transfer-Encoding: quoted-printable\r\n";
        $title = SYSTEM_SUBDOMAIN . ': Incident or Issue Report #' . $id;
        if ($managementFollowUp == 1) $title .= " [Follow-up]";

        //This appears fine on the desktop but jumbled on the iPhone
        mail("beau@bungyjapan.com, $siteManagerEmail, dez@bungyjapan.com", $title, quoted_printable_encode($content), $headers);
        //mail("developer@standardmove.com", $title, quoted_printable_encode($content), $headers);

    } else {
        $_SESSION['message'] = "There was an error saving the incident report (((";
    };

    header('Location: incident_report_list.php');
    die();
};
if (isset($_GET['rid'])) {
    $sql = "SELECT * FROM incident_reports WHERE id='" . (int)$_GET['rid'] . "'";
    $res = mysql_query($sql) or die(mysql_error());
    $_report = mysql_fetch_assoc($res);
} else {
    $_report = array(
        'date' => date('Y-m-d')
    );
};

$sql = "SELECT * FROM uploads WHERE site_id = " . CURRENT_SITE_ID . " AND meta1 = '{$_report['date']}' AND category = 'Incident Report'";
$uploads = queryForRows($sql);

$staff_names = BJHelper::getStaffList('StaffListName', true);
?>
<!DOCTYPE html>
<html>
<head>
    <?php include 'includes/header_tags.php'; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=8">
    <script src="js/functions.js" type="text/javascript"></script>
    <?php include '../includes/head_scripts.php'; ?>
    <title>Operations</title>
    <link rel="stylesheet" type="text/css" href="css/incident_report.css" media="all"/>
    <style>
        <?php
        if (isset($_GET['action']) && $_GET['action'] == 'view') {
        ?>
        #buttons #submit {
            visibility: hidden;
        }

        * input:disabled, * textarea:disabled {
            background-color: #FEFEFE;
            color: black;
            opacity: 1;
        }

        <?php
        };
        ?>
    </style>
    <script>
        $(function () {
            $mydate1 = $('#idate').datepicker({
                showAnim: 'fade',
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd',
                currentText: '<?php echo $_report['date']; ?>'
                //dateFormat: 'dd/mm/yy',
                //altField: '#alternate',
                //altFormat: 'yy-mm-dd',
                //minDate: '0d',
            });
        });

        $(document).ready(function () {
            //$("input").prop('disabled', 'disabled');
            //$("textarea").prop('disabled', 'disabled');
            $("#management_follow_up").change(function () {
                $("#updated_management_follow_up").val(1);
            });
        });
    </script>

    <?php
    if (isset($_GET['action']) && $_GET['action'] == 'view') {
        ?>
        <script>
            $(document).ready(function () {
                //$("input").prop('disabled', 'disabled');
                //$("textarea").prop('disabled', 'disabled');
            });
        </script>
    <?php
    };
    ?>
</head>
<body>
<?php include '../includes/main_menu.php'; ?>
<br/>

<div id="total-container">
    <form id="incident-report-form" method="POST">
        <?php
        if (isset($_GET['rid'])) {
            echo "<input type=\"hidden\" name=\"i[id]\" value=\"" . (int)$_GET['rid'] . "\">";
        };
        ?>
        <div id="report-title"><?php echo SYSTEM_SUBDOMAIN; ?> INCIDENT & ISSUE REPORT</div>
        <div class="half">
            <div id="incident-date" class="left">Report Date:</div>
            <div id="incident-date-value">
                <input type="text" id='idate' style="position: relative; z-index: 100000;" name="i[date]" value="<?php echo $_report['date']; ?>">
            </div>
        </div>
        <div id="picturesSupplied" class="half">
            <span class="picturesLabel">Pictures Supplied?</span>
            <input class="radioPhoto" type="radio" name="i[pictures_supplied]" value="1" <?php if ($_report['pictures_supplied'] == 1) echo 'checked' ?>>
            <span class="radioLabels">Yes</span>
            <input type="radio" class="radioPhoto" name="i[pictures_supplied]"
                   value="0" <?php if ($_report['pictures_supplied'] == 0) echo 'checked' ?>>
            <span class="radioLabels">No</span>
        </div>

        <div id="noted-name" class="left">Name of person who first noted incident: </div>
        <div id="noted-value" class="borderb" style="border: none; width: 180px">
                <?php draw_select('noted_name', $staff_names, 'inoted_name', $_report); ?>
             </div>             
             <div>Time Reception Notified: <input type="text" name="i[reception_time]" value="<?php echo $_report['reception_time']; ?>" style="width: 120px;"> </div>             
        <div id="noted-name-jp" class="trans-jp">最初にインシデントを発見した人
            <span style="margin-left: 230px;">受付への通知時刻</span>                         
        </div>
        <div class="half">
            <div id="completed-name" class="left">Person completing report:</div>
            <div id="completed-value" class="borderb margin160"
                 style="border: none;"><?php draw_select('completed_name', $staff_names, 'icompleted_name', $_report); ?></div>
            <div id="noted-datetime-jp" class="trans-jp">レポート作成者</div>
        </div>
        <div class="half">
            <div id="noted-datetime" class="left">Site, Date and time incident noted:</div>
            <div id="noted-datetime-value" class="borderb margin205"><input type="text" name="i[noted_info]"
                                                                            value="<?php echo $_report['noted_info']; ?>">
            </div>
            <div id="noted-datetime-jp" class="trans-jp">日付・時間</div>
        </div>
        <div style="width:40%; float: left">
            <div id="site-closed-time" class="left">Site closed? At what time?</div>
            <div id="site-closed-time-value" class="borderb margin160"><input type="text" name="i[site_closed_time]"
                                                                              value="<?php echo $_report['site_closed_time']; ?>">
            </div>
            <div id="crew-members-position-jp" class="trans-jp">サイト閉鎖？何時に？</div>
        </div>
        <div class="half">            
            <table width="420px" style="border: 1px;" rules="none">
                <tr>
                    <td align="left" width="100px">Crew members and positions:</td>
                    <td align="center" style="outline: thin solid"> JM</td>
                    <td align="center" style="outline: thin solid"> JO</td>
                    <td align="center" style="outline: thin solid"> CS</td>
                    <td align="center" style="outline: thin solid"> REC</td>
                </tr>
                <tr >
                    <td align="left">クルーの名前とポジション</td> 
                    <td style="outline: thin solid">
                        <?php draw_select('JMstaff', $staff_names, 'JMstaff', $_report); ?>                            
                    </td>
                    <td style="outline: thin solid">
                        <?php draw_select('JOstaff', $staff_names, 'JOstaff', $_report); ?>
                    </td>
                    <td style="outline: thin solid">
                        <?php draw_select('CSstaff', $staff_names, 'CSstaff', $_report); ?>
                    </td>
                    <td style="outline: thin solid">
                        <?php draw_select('RECstaff', $staff_names, 'RECstaff', $_report); ?>
                    </td>
                </tr>

            </table>
            <!-- <div id="crew-members-posiotion" class="left">Crew members and positions:</div>
            <div id="crew-members-posiotion-value" class="borderb margin175">
                <input type="text" name="i[crew_members_position]" value="<?php //echo $_report['crew_members_position']; ?>"> 
                JM                
            </div>
             -->            
        </div>

        <div class="half">
            <div id="time-management-notified" class="left">Time management notified:</div>
            <div id="time-management-notified-value" class="borderb margin160"><input type="text"
                                                                                      name="i[time_management_notified]"
                                                                                      value="<?php echo $_report['time_management_notified']; ?>">
            </div>
            <div id="weather-conditions-jp" class="trans-jp">マネジメントへの通知時刻</div>
        </div>
        <div class="half">
            <div id="weather-conditions" class="left">Weather conditions:</div>
            <div id="weather-conditions-value" class="borderb margin120"><input type="text" name="i[weather_conditions]"
                                                                                value="<?php echo $_report['weather_conditions']; ?>">
            </div>
            <div id="weather-conditions-jp" class="trans-jp">天候状況</div>
        </div>

        <div id="incident-classification">
            <div id="incedent-classification-title" class="alignleft">Classification of Incident - (Circle which
                applies)
            </div>
            <div id="incedent-classification-title-jp" class="trans-jp">インシデントの種類 － （○で囲んでください）</div>
            <div id="incedent-classification-title-values">
                <?php
                foreach ($ic_codes as $code => $value) {
                    $checked = ($code == $_report['incident_classification_code']) ? ' checked' : '';
                    ?>
                    <div><label for="cc-code-<?php echo strtolower($code); ?>"><input type="radio"
                                                                                      id="cc-code-<?php echo strtolower($code); ?>"
                                                                                      name="i[incident_classification_code]"
                                                                                      value="<?php echo $code; ?>"<?php echo $checked; ?>><br/>
                            <?php echo $value['type']; ?><br/>
                            <?php echo $value['type-jp']; ?><br/>
                            <?php echo $value['op_type']; ?><br/></label>
                    </div>
                <?php
                };
                ?>
            </div>
        </div>

        <div id="incident-description-title" class="alignleft">Description of Incident: (include all equipment in
            use)<br/>
            インシデント詳細
        </div>        
        <div style="border: 1px; border-style: solid; height: 30px;">             
               <?php                     
                    $ddlData = BJHelper::getDescriptionData(true);
                    draw_select('description_ddl', $ddlData, 'description_ddl', $_report); 
               ?>                      
        </div>      
        <br>
        <div id="incident-description-value">
            <textarea name="i[incident_description]"><?php echo $_report['incident_description']; ?></textarea></div>        
        <div id="action-taken-title" class="alignleft">Action taken: <br/> 対処時の詳細</div>
        <div id="incident-description-value">
            <textarea name="i[action_taken]"><?php echo $_report['action_taken']; ?></textarea></div>
        <div id="ourside-services-title" class="alignleft">Outside services required? Who, when were they notified and
            their response time?<br/>
            緊急機関の必要有無、連絡者名、到着予定時刻
        </div>
        <div id="outside-services-value">
            <textarea name="i[outside_services]"><?php echo $_report['outside_services']; ?></textarea></div>

        <div style="height: 10px;"></div>
        <div id="emergency-log" class="left">Emergency Log attached: (YES/NO)</div>
        <div id="emergency-log-value" class="borderb" style="margin-left: 210px;"><input type="text"
                                                                                         name="i[emergency_log]"
                                                                                         value="<?php echo $_report['emergency_log']; ?>">
        </div>
        <div id="-jp" class="trans-jp">緊急時用ログはありますか</div>
        <div id="injury_occurred" class="left">Was the Customer Injured: (YES/NO)</div>
        <div id="injury_occurred-log-value" class="borderb" style="margin-left: 220px;"><input type="text"
                                                                                         name="i[injury_occurred]"
                                                                                         value="<?php echo $_report['injury_occurred']; ?>">
        </div>
        <div id="customer-injured-jp" class="trans-jp">お客様の怪我の有無</div>                 
        <div id="site-reopened" class="left">Date and time site reopened:</div>
        <div id="site-reopened-value" class="borderb margin175"><input type="text" name="i[site_reopened_datetime]"
                                                                       value="<?php echo $_report['site_reopened_datetime']; ?>">
        </div>
        <div id="site-reopened-jp" class="trans-jp">営業再開の日付と時間</div>

        <div id="reception-notified" class="left">Time Incident was Completed:</div>
        <div id="reception-notified-value" class="borderb margin175"><input type="text"
                                                                            name="i[reception_notified_time]"
                                                                            value="<?php echo $_report['reception_notified_time']; ?>">
        </div>
        <div id="reception-notified-jp" class="trans-jp">対応完了時刻</div>

        <div id="completed-datetime" class="left">Date & Time Incident Report Completed:</div>
        <div id="completed-datetime-value" class="borderb margin250"><input type="text" name="i[completed_datetime]"
                                                                            value="<?php echo $_report['completed_datetime']; ?>">
        </div>
        <div id="completed-datetime-jp" class="trans-jp">レポート作成完了時刻</div>
      
        <div id="incident-cause" class="left">What caused the incident?</div>
        <div id="incident-cause-value" class="borderb margin175"><input type="text" name="i[incident_cause]"
                                                                            value="<?php echo $_report['incident_cause']; ?>">
        </div>
        <div id="incident-cause-jp" class="trans-jp">事故の原因は何ですか？</div>

        <div id="system-fail" class="left">Do you think one of our systems failed?</div>
        <div id="system-fail-value" class="borderb margin275"><input type="text" name="i[system_fail]"
                                                                        value="<?php echo $_report['system_fail']; ?>">
        </div>
        <div id="system-fail-jp" class="trans-jp">会社の手順方針に落ち度があったと思いますか</div>
      
        <div id="customer-action" class="left">Did the customers own actions cause their injury?</div>
        <div id="customer-action-value" class="borderb margin300"><input type="text" name="i[customer_action]"
                                                                        value="<?php echo $_report['customer_action']; ?>">
        </div>
        <div id="customer-action-jp" class="trans-jp">それともお客様自身の行動が事故を招いたのですか？</div>
        

        <div id="checked-and-signed" class="left">Checked and signed by JC and Management:</div>
        <div id="checked-and-signed-value" class="borderb"
             style="margin-left: 265px; border: none;"><?php draw_select('checked_and_signed', $staff_names, 'ichecked_and_signed', $_report); ?></div>
        <div id="checked-and-signed-jp" class="trans-jp">ジャンプコントローラーサイン</div>
        <div class="trans-jp">マネージャーサイン</div>

        <div id="management-follow-up-title" class="alignleft">Management Follow Up<br/></div>
        <div id="management-follow-up-value">
            <textarea id="management_follow_up" name="i[management_follow_up]"><?php echo $_report['management_follow_up']; ?></textarea>
        </div>
        <input type="hidden" name="i[updated_management_follow_up]" id="updated_management_follow_up" value="0">

    </form>
    <a href="#upload-zip" id="upload-zip"><button id="add-zip-file" type="button">Add New Zip File</button></a>
    <br>
    <br>
    <?php
    foreach ($uploads as $key => $upload) {
        echo "<a href='downloadIncidentPhotos.php?fileId={$upload['id']}'>{$upload['filename']}</a> <br>";
    }
    ?>
    <div id="buttons">
        <button id="back">Back</button>
        <button id="submit">Save</button>
    </div>

</div>
<script>
    $(document).ready(function () {
        $('#upload-zip').click(function (event) {
            event.preventDefault();
            window.open("imageUpload.php?date=<?=$_report['date']?>", "_blank", "location=no, menubar=no, status=no, height=250, width=300");
        });

        $('#back').click(function () {
            document.location = 'incident_report_list.php';
            return false;
        });

        $('#submit').click(function () {
            $('#incident-report-form').submit();
            return true;
        });
    });
</script>
</body>
</html>
