<?
  include '../includes/application_top.php';

  $cDate = date("Y-m-d");

	if (!isset($_GET['id'])) {
		die('No cord ID provided');
	};
	$id = (int)$_GET['id'];
	$sql = "SELECT * FROM cord_logs WHERE id = '{$id}'";
	$res = mysql_query($sql) or die(mysql_error());
	if ($cord = mysql_fetch_assoc($res)) {
		$cord_name = $cord['cid1'] . $cord['cid2'] . $cord['cid3'];
		list($year) = explode('-', $cord['date']);
	} else {
		die('No cord found');
	};
	switch ($cord['status']) {
		case 1: 
			$cord_status = 'Active / 使用中';
			break;
		case 0: 
			$cord_status = 'Retired / 使用済';
			break;
		case 2: 
			$cord_status = 'Ready / 用意';
			break;
	};

?>
<!DOCTYPE html>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">
<link href="css/style.css" rel="stylesheet" media="screen" type="text/css" />
<script src="js/functions.js" type="text/javascript"></script>

<?php include '../includes/head_scripts.php'; ?>
<title>Operations :: Cord Logs :: <?php echo $cord_name; ?> Daily Usage</title>
<script type="text/javascript" charset="utf-8">
// Buttons actions
	$(document).ready(function () {
		$("#close").click(function () {	
			window.close();
		});
	});
</script>
<style>
table {
	border-collapse: collapse;
}
table td.border {
	border: 1px solid black;
	margin: 2px;
	text-align: center;
}
</style>
</head>
<body>
<br>
<table align="center" border="0" width="550px">
<?php 
	if ($cord_name) { 
?>
	<tr>
		<td><b>Cord Code:</b> <?php echo $cord_name; ?></td>
		<td>&nbsp;</td>
		<td><b>Status:</b> <?php echo $cord_status; ?></td>
	</tr>
	<tr>
		<td colspan="3">&nbsp;</td>
	</tr>
<?php
    $colors = array(
        'SL'    => 'yellow',
        'L'     => 'red',
        'H'     => 'blue',
        'SH'    => 'black'
    );
    $cord_id_field = "eq_cord_cc_%1\$s_num";
    $uvt_field = "eq_cord_uvt_%1\$s";
    $ct_field = "eq_cord_ct_%1\$s";
    $low_field = "eq_cord_tw_%1\$s_low";
    $high_field = "eq_cord_tw_%1\$s";
    $uvh_field = "eq_cord_uvh_%1\$s";
    $jn_field = "eq_cord_jn_%1\$s";

	$site_id = CURRENT_SITE_ID;
	$cid2 = $cord['cid2'];
	$cid3 = $cord['cid3'];
    foreach ($colors as $key => $c) {
        if ($key != $cid2) continue;
        $sql = sprintf(
            "SELECT $cord_id_field as cid, $uvh_field as uvh_total, $jn_field as jumps, `date`
                FROM daily_reports
                WHERE
                    site_id = '$site_id'
                    and $cord_id_field = '$cid3' and
                    `date` <= '$year-12-31'
                    and `date` >= '$year-01-01'
                ORDER BY `date` DESC
                ",
        $c);
		$res = mysql_query($sql) or die(mysql_error());
		$headers = true;
		while ($row = mysql_fetch_assoc($res)) {
			if ($headers) {
				echo "<tr>\n";
				echo "\t<td class='border'><b>Date</b></td>\n";
				echo "\t<td class='border'><b>Total Jumps</b></td>\n";
				echo "\t<td class='border'><b>Total UV hours</b></td>\n";
				echo "</tr>\n";
				$headers = false;
			};
			echo "<tr>\n";
			echo "\t<td class='border'>{$row['date']}</td>\n";
			echo "\t<td class='border'>" . $row['jumps']."</td>\n";
			echo "\t<td class='border class='border''>" . $row['uvh_total']."</td>\n";
			echo "</tr>\n";
		}; 
	};

?>
<?php
	} else {
?>
<tr><td>No cord found</td></tr>
<?php }; ?>
</table>
<br><br>
<?php 
include ("ticker.php");
?>
</body>
</html>
