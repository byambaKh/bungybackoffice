<?
require_once("../includes/application_top.php");

$query = "SELECT DATE(dateTime) AS date FROM uploads
			WHERE site_id = " . CURRENT_SITE_ID . "
			AND category = 'Daily Report'
			GROUP BY DATE(dateTime)
			ORDER BY dateTime DESC; ";

$dates = queryForRows($query);
echo '';
?>
<html>
<head>
    <?
    echo Html::css("bootstrap/3.3.2/css/bootstrap.min.css");
    ?>
    <style>
        h1, h2{
            text-align: center;
        }

        table{
            width: 400px;
            margin-left: auto;
            margin-right: auto;
            display: block;
        }
        td {
            width: 400px;
            height: auto;
        }
        td a{
            text-align: center;
            display: block;
        }
        body {
            text-align: center;
        }
    </style>
</head>
<body>
<h1>Photos for <?=CURRENT_SITE_DISPLAY_NAME?></h1>
    <?
    if(count($dates)) {
        echo "<table>";
        foreach ($dates as $date)
        {
            echo "<tr><td><a href='gallery.php?date={$date['date']}'>{$date['date']}</a></td></tr>";
        }
        echo "</table>";
    } else {
        echo "<h2>No photos have been uploaded from the daily reports.</h2>";
    }
    ?>

<a href="/?logout=true">logout</a>
</body>
</html>
