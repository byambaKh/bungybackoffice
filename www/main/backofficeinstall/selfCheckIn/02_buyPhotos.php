<?php
$pageName = "buyPhotos";
require_once('selfCheckInHeader.php');
//pr($_SESSION);

$photoPath = 'img/selfcheckin_montage_'.strtolower(SYSTEM_SUBDOMAIN).'.jpg';
?>
<style>
    #del {
        float: left;
        margin-left: 0;
        text-align: right;
        width: 542px;
    }

    #yes button {
        background-color: #0f0;
        border: 2px solid white;
        color: black;
        font-family: "pussycat_snickers";
        font-size: 90px;
        height: 90px;
        line-height: 100px;
        text-align: center;
        width: 465px;
        clear: none;
    }

    #no button {
        background-color: red;
        border: 2px solid white;
        color: white;
        font-family: "pussycat_snickers";
        font-size: 90px;
        height: 90px;
        line-height: 100px;
        text-align: center;
        width: 465px;
        clear: none;
    }

    .header-container{
        margin-bottom: 0px;
        margin-top: 10px;
    }

    #no{
        display: inline;
        width:140px;
    }

    #no button{
        width:200px;
    }

    #yes{
        display: inline;
        width:140px
    }

    #yes button{
        width:200px;
    }
</style>


<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">
    <input type="hidden" name="includePhotos" value="">
    <input type="hidden" name="includePhotosPrice" value="<?php echo $config['price_photo']?>">

    <div id="step3">
        <div class="header-container">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en" style='font-family: "Arial", "Sans-Serif", "Helvetica"; font-size:50px;'>Would you like to purchase photos? (&yen;<?php echo $config['price_photo']?>)</span>
                    <span class="lang_ja" style='font-family: "Arial", "Sans-Serif", "Helvetica"'>写真をご注文しますか？ (&yen;<?php echo $config['price_photo']?>)</span>
                </h1>
                <!--
                <h2>
                    <span class="lang_en">High quality professionally taken photos from start to finish all on a CD, approx. 25 photos <?php echo $config['price_photo']?> Yen! </span>
                    <span class="lang_ja">お写真をご購入された方は左手首の番号をご入力ください。</span>
                </h2>
                -->
            <img src='<?php echo $photoPath?>'>
            </div>
        </div>

        <div id="no">
            <button style="width:300px">
                <span class="lang_en">NO</span>
                <span class="lang_ja" >いいえ</span>
            </button>
        </div>

        <div id="yes">
            <button>
                <span class="lang_en">YES</span>
                <span class="lang_ja">はい</span>
            </button>
        </div>

        <div class="back">
            <button>
                <span class="lang_en">&lt;&lt;BACK</span>
                <span class="lang_ja">&lt;&lt;戻る</span>
            </button>
        </div>
    </div>
</form>

<script>
    var pageName = "buyPhotos";

    $(document).ready(function () {
        $('#step3 #yes button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            document.waiverForm.includePhotos.value = 1;//true and false are getting sent as strings
            submitForm();
        });

        $('#step3 #no button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            document.waiverForm.includePhotos.value = 0;
            submitForm();
        });
    });
</script>
<?php require_once('selfCheckInFooter.php'); ?>

