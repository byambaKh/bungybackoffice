<?php
require_once('selfCheckInHeader.php');

//updateScmData($scm,$site_id,$status,$reason,$condition);

  if($_POST['submit']){
      if($_POST['scm'] && $_POST['problem']){
        updateScmData($_POST['scm'],(int)CURRENT_SITE_ID,0,$_POST['problem'], '0', '', '1', '');        
        echo '<h2> Report submitted! Thank you</h2>';
      } else {
        echo '<h2> Please fill all the data and try again!</h2>';
      }
  }

?>

<style>
    #yes button {
        background-color: #0f0;
        border: 2px solid white;
        color: black;
        font-family: "pussycat_snickers";
        font-size: 90px;
        height: 90px;
        line-height: 100px;
        text-align: center;
        width: 400px;
        clear: none;
    }
  
  #no button {
        background-color: red;
        border: 2px solid white;
        color: white;
        font-family: "pussycat_snickers";
        font-size: 90px;
        height: 90px;
        line-height: 100px;
        text-align: center;
        width: 265px;
        clear: none;
    }
  select {
        font-family: "pussycat_snickers";
        font-size: 35px;
        font-style: italic;
    	width: 465px;
    }
</style>

<html>
<head></head>
<title>SCM Problem Report</title>
<body>
<form accept-charset="utf8" method='post'>
  <div class="header-container">
            <div class="head-image-container">
<h1>
    <span class="lang_en">Please specify the problems</span>
</h1>
    </div>
  </div>
<h1>SCM Number :</h1>
<select name='scm'>
  <option selected disabled>Select</option>}
  <option value="SCM-1">SCM-1</option>
  <option value="SCM-2">SCM-2</option>
  <option value="SCM-3">SCM-3</option>
</select>
  <br>
  <br>
<h1>Problem :</h1>
<select name='problem'>
  <option selected disabled>Select Problem</option>}
  <option value="Unknown">Unknown</option>
  <option value="Screen">Screen</option>
  <option value="Printer">Printer</option>
  <option value="Cash Box">Cash Box</option>
  <option value="Sale Problem">Scale problem</option>
  <option value="Won't start">Won't start</option>
</select>  
  <br><br>
      <div id="yes">
            <button name ='submit' onClick="javascript:this.form.submit();" value='submit'>Submit</button>
      </div>
   <br>
   <br>
   <div class="back" id="no">
            <button>Cancel</button>
        </div>
</form>
</body>
<html>