<?php
$pageName = "noOpenJumps";
require_once('selfCheckInHeader.php')
?>
<script>
    //setTimeout(function () { submitForm("action=finishWaiver"); }, 10000);
</script>
<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">

    <div id="step17">
        <div class="header-container">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en">Please speak to the reception staff and ask when the next available time is.</span>
					<span class="lang_ja">次の空き時間をスタッフにお尋ねください。</span>
                </h1>
            </div>
        </div>
    </div>
</form>
<script>
    var pageName = "exit";
    $(document).ready(function () {
    });
</script>
<script src="/js/reception.js" type="text/javascript"></script>

<?php require_once('selfCheckInFooter.php'); ?>

