<?php
$pageName = "monthAndDateOfBirth";
require_once('selfCheckInHeader.php')
?>
<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">

    <div id="step6">
        <div class="header-container">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en">please enter your birthday</span>
                    <span class="lang_ja">誕生日をご入力ください。</h1>
            </div>
        </div>
        <div id="birth-month-container">
            <div id="birth-month">
                <div id="birth-month-value"><input name="birth_month"></div>
            </div>
            <div class="keyboard-container">
                <div class="keyboard-button lang_en">
                    <button rel="JAN">JAN</button>
                </div>
                <div class="keyboard-button lang_en">
                    <button rel="FEB">FEB</button>
                </div>
                <div class="keyboard-button lang_en">
                    <button rel="MAR">MAR</button>
                </div>
                <div class="keyboard-button lang_en">
                    <button rel="APR">APR</button>
                </div>
                <div class="keyboard-button lang_en">
                    <button rel="MAY">MAY</button>
                </div>
                <div class="keyboard-button lang_en">
                    <button rel="JUN">JUN</button>
                </div>
                <div class="keyboard-button lang_en">
                    <button rel="JUL">JUL</button>
                </div>
                <div class="keyboard-button lang_en">
                    <button rel="AUG">AUG</button>
                </div>
                <div class="keyboard-button lang_en">
                    <button rel="SEP">SEP</button>
                </div>
                <div class="keyboard-button lang_en">
                    <button rel="OCT">OCT</button>
                </div>
                <div class="keyboard-button lang_en">
                    <button rel="NOV">NOV</button>
                </div>
                <div class="keyboard-button lang_en">
                    <button rel="DEC">DEC</button>
                </div>
                <div class="keyboard-button lang_ja">
                    <button rel="JAN">1月</button>
                </div>
                <div class="keyboard-button lang_ja">
                    <button rel="FEB">2月</button>
                </div>
                <div class="keyboard-button lang_ja">
                    <button rel="MAR">3月</button>
                </div>
                <div class="keyboard-button lang_ja">
                    <button rel="APR">4月</button>
                </div>
                <div class="keyboard-button lang_ja">
                    <button rel="MAY">5月</button>
                </div>
                <div class="keyboard-button lang_ja">
                    <button rel="JUN">6月</button>
                </div>
                <div class="keyboard-button lang_ja">
                    <button rel="JUL">7月</button>
                </div>
                <div class="keyboard-button lang_ja">
                    <button rel="AUG">8月</button>
                </div>
                <div class="keyboard-button lang_ja">
                    <button rel="SEP">9月</button>
                </div>
                <div class="keyboard-button lang_ja">
                    <button rel="OCT">10月</button>
                </div>
                <div class="keyboard-button lang_ja">
                    <button rel="NOV">11月</button>
                </div>
                <div class="keyboard-button lang_ja">
                    <button rel="DEC">12月</button>
                </div>
            </div>
        </div>
        <div id="birth-day-container">
            <div id="birth-day">
                <div id="bidth-day-value"><input name="birth_day"></div>
                <div id="birth-day-del-button">
                    <button><span class="lang_en">DEL</span><span class="lang_ja smaller">訂正</span></button>
                </div>
            </div>
            <div class="keyboard-container">
                <div class="keyboard-button">
                    <button>1</button>
                </div>
                <div class="keyboard-button">
                    <button>2</button>
                </div>
                <div class="keyboard-button">
                    <button>3</button>
                </div>
                <div class="keyboard-button">
                    <button>4</button>
                </div>
                <div class="keyboard-button">
                    <button>5</button>
                </div>
                <div class="keyboard-button">
                    <button>6</button>
                </div>
                <div class="keyboard-button">
                    <button>7</button>
                </div>
                <div class="keyboard-button">
                    <button>8</button>
                </div>
                <div class="keyboard-button">
                    <button>9</button>
                </div>
                <div class="keyboard-button">
                    <button>0</button>
                </div>
            </div>
        </div>
        <div id="enter-container">
            <div id="enter">
                <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
            </div>
        </div>
        <div class="back">
            <button>
                <span class="lang_en">&lt;&lt;BACK</span>
                <span class="lang_ja">&lt;&lt;戻る</span>
            </button>
        </div>
    </div>
</form>
<script>
    var pageName = "monthAndDateOfBirth";

    $(document).ready(function () {
        $('#step6 #birth-month-container button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            $('#step6 #birth-month input').val($(this).attr('rel'));
        });
        $('#step6 #birth-day-container .keyboard-container button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            if ($('#step6 #birth-day input').val().length == 2) return;
            $('#step6 #birth-day input').val('' + $('#step6 #birth-day input').val() + $(this).html());
        });
        $('#step6 #birth-day-del-button button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            var val = $('#step6 #birth-day input').val();
            if (val.length > 0) {
                $('#step6 #birth-day input').val(val.substr(0, val.length - 1));
            }
            ;
        });
        $('#step6 #enter button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            var bmonth = $('#step6 #birth-month input').val();
            var bday = $('#step6 #birth-day input').val();
            var error = false;
            if (bmonth.length == 0) {
                show_info("validmonth");
                error = true;
            }
            ;
            if (!error && bday.length == 0) {
                show_info("validday");
                error = true;
            }
            ;
            if (!error && bday > 31) {
                show_info("validday31");
                error = true;
            }
            ;
            if (!error) {
                submitForm();
                //saveWaiverDataToSession({birth_day: bday});
                //saveWaiverDataToSessionAndGoToNextStep({birth_month: bmonth});
                //save_var('birth_month', bmonth);
                //save_var('birth_day', bday);
                //goToNextStep();
            }
            ;
        });
    });
</script>
<?php require_once('selfCheckInFooter.php'); ?>
