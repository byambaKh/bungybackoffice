<?php
$pageName = "exit";
require_once('selfCheckInHeader.php');
/*

//load the printer names
$loadedResults = mysql_query('SELECT * FROM settings WHERE site_id = ' . CURRENT_SITE_ID . ' AND key_name = "printerTicketAndCertificate"');
$settingsRow = mysql_fetch_assoc($loadedResults);
$savedPrintersArray = explode("\n", $settingsRow['value']);

$certificatePrinter = $ticketPrinter = '';//set them to an empty string if there are not results

$findPrintersResultsCount = mysql_num_rows($loadedResults);

if ($findPrintersResultsCount) {
    $certificatePrinter = trim($savedPrintersArray[0]);
    $ticketPrinter = trim($savedPrintersArray[1]);
}
*/
?>
<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">

    <div id="step15">
        <div class="header-container">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en">Thank you. Registration complete!</span>
                    <span class="lang_ja">登録が完了しました。</span>
                </h1>

                <h2>
                    <span class="lang_en">Please give your name to the reception staff.</span>
                    <span class="lang_ja">お客様の氏名をスタッフまで<br/> お申し付けください。</span>
                </h2>
            </div>
        </div>
        <div class="restart-container">
            <!--div>This form will restart in <span id="restart-in">5</span> seconds</div-->
            <div id="step15progress"></div>
            <?php
            $date = urlencode(date("Y-m-d"));
            $signatureUrl = urlencode('');

            //$time 	  urlencode(      = $_SESSION['data']['time']);//for a walkin is time know, it is known for a preBooking
            //$weight 	= urlencode($_SESSION['data']['weight']);//need to get from scales
            $bookingType = "WalkIn";
            $jumpNumber = "34";//need to get from database
            $photoNumber = "12";
            $cordColor = "Yellow";
            $groupOfNumber = "7";
            $firstName = "Tim";
            $lastName = "Smith";
            $fullName = urlencode($firstName . " " . $lastName);

            //TODO temp placeholder variables that I must get
            $time = "13:30";
            $weight = "70";
            $cordColor = "Yellow";
            $groupOfNumber = 4;
            $jumpNumber = 47;

            $certificateUrl = "../Reception/certificate.php?fullName=$fullName&date=$date&signatureUrl=$signatureUrl";
            $miniTicketUrl = "../Reception/ticketLayout.php?time=$time&weight=$weight&bookingType=$bookingType&photoNumber=$photoNumber&jumpNumber=$jumpNumber&groupOfNumber=$groupOfNumber&firstName=$firstName&lastName=$lastName&cordColor=$cordColor";

            $certificateUrl = "testpage.html";
            $miniTicketUrl = "testpage.html";

            ?>

            <iframe class="hiddenCertificateFrame" name="certificate" id="certificate" src="<?=$certificateUrl?>"></iframe>
            <iframe name="ticketmini" id="ticketmini" src="<?=$miniTicketUrl?>"></iframe>
        </div>
    </div>
</form>
<script>
    function createPrinterDropDown(savedPrinter) {
        var printers = jsPrintSetup.getPrintersList();
        //console.log(printers);

        var printerArray = printers.split(",");
        //console.log(printerArray);

        var selectHtmlString = '';

        for (var printerIndex = 0; printerIndex < printerArray.length; printerIndex++) {
            //(redirected 2) seems to indicate that the printer is installed but not connected
            //so we filter those out as they are not options
            var printerString = printerArray[printerIndex].trim();
            if (printerString.indexOf("(redirected 2)") == -1)
                if (printerString == savedPrinter)//select the selected option
                    selectHtmlString = selectHtmlString + "<option value='" + printerString + "'selected>" + printerString + "</option>\n";
                else
                    selectHtmlString = selectHtmlString + "<option value='" + printerString + "'>" + printerString + "</option>\n";
        }

        return selectHtmlString;
    }

    $(document).ready(function () {
        var ticketPrinter = 'Brother MFC-J6570CDW Printer';
        var certificatePrinter = 'Brother MFC-J6570CDW Printer';
        //console.log(jsPrintSetup.getPrintersList());

        /*
         $("#testPrint").click(function(){
         testPrint(ticketPrinter, certificatePrinter);
         console.log('TEST' + ' ' +ticketPrinter + ' ' + certificatePrinter);
         });
         */
        //populate the dropdown with printer names
        //select the one that matches what is stored in the database
        //printTicket('<?php //echo $ticketPrinter;?>', '<?php //echo $certificatePrinter;?>');

        /*
        jsPrintSetup.setPrinter(ticketPrinter);
        jsPrintSetup.setSilentPrint(true);

        jsPrintSetup.setOption('headerStrLeft', 'Bungy Japan Certificate');
        jsPrintSetup.setOption('headerStrCenter', 'Bungy Japan Certificate');
        jsPrintSetup.setOption('headerStrRight', 'Bungy Japan Certificate');
        // set empty page footer
        jsPrintSetup.setOption('footerStrLeft', 'Bungy Japan Certificate');
        jsPrintSetup.setOption('footerStrCenter', 'Bungy Japan Certificate');
        jsPrintSetup.setOption('footerStrRight', 'Bungy Japan Certificate');

        jsPrintSetup.printWindow(window.frames[0]);
        //printForDemo();
        //clearSessionWaiverData();
        */

    });
</script>
<script src="/js/reception.js" type="text/javascript"></script>

<?php require_once('selfCheckInFooter.php'); ?>
