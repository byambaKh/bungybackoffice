<?php
$pageName = "email";
require_once('selfCheckInHeader.php')
?>

<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">

    <div id="step13">
        <div class="header-container">
            <div class="head-image-container">
                <h1><span class="lang_en">Please enter your email address</span><span
                        class="lang_ja">メールアドレスをご入力ください。</span></h1></div>
        </div>
        <!--div id="rather-not">
          <button><span class="lang_en">No, thank you</span><span class="lang_ja">入力不要</span></button>
        </div-->
        <div id="email-container">
            <div id="email-input-container"><input name="email"></div>
        </div>
        <div id="keyboard-container">
            <?php
            $keyboard = array(
                array('1', '2', '3', '4', '5', '6', '7', '8', '9', '0'),
                array('q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'),
                array('a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', '-'),
                array('z', 'x', 'c', 'v', 'b', 'n', 'm', '.', '@', '_')
            );
            $i = 0;
            foreach ($keyboard as $line) {
                echo '<div class="keyboard-line' . $i . '">';
                foreach ($line as $letter) {
                    echo "<div class=\"keyboard-button\"><button>" . strtoupper($letter) . "</button></div>";
                };
                echo "</div>";
                $i = '';
            };
            ?>
        </div>
        <div id="domains-container">
            <div class="domains-cell first-cell">
                <button>.com</button>
            </div>
            <div class="domains-cell first-cell">
                <button>.co.jp</button>
            </div>
            <div class="domains-cell first-cell">
                <button>.ne.jp</button>
            </div>
            <div class="domains-cell second-cell">
                <button>@gmail.com</button>
            </div>
            <div class="domains-cell second-cell">
                <button>@i.softbank.jp</button>
            </div>
            <div class="domains-cell second-cell">
                <button>@ezweb.ne.jp</button>
            </div>
            <div class="domains-cell third-cell">
                <button>@yahoo.co.jp</button>
            </div>
            <div class="domains-cell third-cell">
                <button>@hotmail.ne.jp</button>
            </div>
            <div class="domains-cell third-cell">
                <button>@docomo.ne.jp</button>
            </div>
            <!--
            <div class="domains-row">
                <div class="domains-cell first-cell">
                    <button>.com</button>
                </div>
                <div class="domains-cell second-cell">
                    <button>@i.softbank.jp</button>
                </div>
                <div class="domains-cell third-cell">
                    <button>@yahoo.co.jp</button>
                </div>
            </div>
            <div class="domains-row">
                <div class="domains-cell first-cell">
                    <button>.ne.jp</button>
                </div>
                <div class="domains-cell second-cell">
                    <button>@gmail.com</button>
                </div>
                <div class="domains-cell third-cell">
                    <button>@hotmail.ne.jp</button>
                </div>
            </div>
            <div class="domains-row">
                <div class="domains-cell first-cell">
                    <button>.co.jp</button>
                </div>
                <div class="domains-cell second-cell">
                    <button>@ezweb.ne.jp</button>
                </div>
                <div class="domains-cell third-cell">
                    <button>@docomo.ne.jp</button>
                </div>
            </div>
            -->
        </div>
        <div id="enter-del-container">
            <div id="enter">
                <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
            </div>
            <div id="del">
                <button><span class="lang_en">DELETE</span><span class="lang_ja">訂正</span></button>
            </div>
        </div>
        <div class="back">
            <button>
                <span class="lang_en">&lt;&lt;BACK</span>
                <span class="lang_ja">&lt;&lt;戻る</span>
            </button>
        </div>
    </div>
</form>
<script>
    var pageName = "email";

    $(document).ready(function () {
        if (pc_version) {
            //this was orignially in the $(document).ready(function(){}) of at the bottom of selfCheckinHeader.php
            //caused an error as this only appears on this page
            //moved here to fix it
            $("#domains-container").css("top", 55 + parseInt($("#domains-container").css("top").replace('px', '')));
        }

        $('#step13 .keyboard-button button, #step13 #domains-container button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            $('#step13 input').val('' + $('#step13 input').val() + $(this).html().toLowerCase());
        });

        $('#step13 #del button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            var val = $('#step13 input').val();
            if (val.length > 0) {
                $('#step13 input').val(val.substr(0, val.length - 1));
            }
            ;
        });

        $('#step13 #enter button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            if (!/(.+)@(.+\..+)/.test($("#step13 input").val())) {
                show_info('validemail');
                return;
            }
            ;
            submitForm();
            //saveWaiverDataToSessionAndGoToNextStep({email: $("#step13 input").val()});
        });
    });
</script>
<?php require_once('selfCheckInFooter.php'); ?>
