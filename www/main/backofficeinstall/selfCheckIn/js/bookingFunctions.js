function lockBooking(bookingId) {

    var message = {
        "from": machineName,
        "to": "server",
        "device": "browser",
        "message": machineName + " lock_booking " + bookingId,
        "messageIdentifier": createMessageId()
    };

    socket.send(JSON.stringify(message));
}

function unlockBooking(bookingId) {

    var message = {
        "from": machineName,
        "to": "server",
        "device": "browser",
        "message": machineName + " unlock_booking " + bookingId,
        //"messageIdentifier": 'unlockBooking'
        "messageIdentifier": createMessageId()
    };

    socket.send(JSON.stringify(message));
}

function setBookingPaid(bookingId) {

    var message = {
        "from": machineName,
        "to": "server",
        "device": "browser",
        "message": machineName + " set_booking_paid " + bookingId,
        //"messageIdentifier": 'setBookingPaid'
        "messageIdentifier": createMessageId()
    };

    socket.send(JSON.stringify(message));
}

function bookingCheckIfLockedAndPaid(bookingId) {
    var message = {
        "from": machineName,
        "to": "server",
        "device": "browser",
        "message": machineName + " booking_check_if_locked_and_paid " + bookingId,
        "messageIdentifier": createMessageId()
    };

    socket.send(JSON.stringify(message));
}
