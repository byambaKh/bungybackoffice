var socket;
var transactionId;

function init() {
    var host = "ws://minakami.bungyjapan.com:8080";

    try {
        socket = new WebSocket(host);
        log('WebSocket - status ' + socket.readyState);

        socket.onopen = function (msg) {
            if (this.readyState == 1) {
                log("We are now connected to websocket server. readyState = " + this.readyState);
            }

            bookingCheckIfLockedAndPaid(bookingId);

        };

        socket.onmessage = function (msg) {
            try{

                var messageObject = JSON.parse(msg.data);
                if(messageObject.message.hasOwnProperty('function')){

                    if(messageObject.message.function == 'booking_check_if_locked_and_paid' && messageObject.message.bookingId == bookingId){
                        if (messageObject.message.booking_lock == false) {
                            //if fully paid ...
                            if(messageObject.message.booking_paid) { //booking already paid
                                //submitForm();//go to the next step
                                document.location = '13_4_weight_websocket.php';//TODO Go to next page

                            } else { //booking not paid
                                //pay here?
                                //showPayHereMessage();
                                showPayHereQuestion();
                            }

                        } else {
                            console.log('This booking is currently being paid at another machine. Check in will continue when booking has been paid');
                            showPaymentInProcess();
                            console.log("Staying on this page till payment done");

                        }
                    }
                }

            } catch(ex) {

            }

            //Try again?
            //pay at the next machine
            console.log(msg);
        };

        socket.onclose = function (event) {
            log("Disconnected - status " + this.readyState);

            var reason;
            //alert(event.code);
            // See http://tools.ietf.org/html/rfc6455#section-7.4.1
            if (event.code == 1000)
                reason = "Normal closure, meaning that the purpose for which the connection was established has been fulfilled.";
            else if (event.code == 1001)
                reason = "An endpoint is \"going away\", such as a server going down or a browser having navigated away from a page.";
            else if (event.code == 1002)
                reason = "An endpoint is terminating the connection due to a protocol error";
            else if (event.code == 1003)
                reason = "An endpoint is terminating the connection because it has received a type of data it cannot accept (e.g., an endpoint that understands only text data MAY send this if it receives a binary message).";
            else if (event.code == 1004)
                reason = "Reserved. The specific meaning might be defined in the future.";
            else if (event.code == 1005)
                reason = "No status code was actually present.";
            else if (event.code == 1006)
                reason = "The connection was closed abnormally, e.g., without sending or receiving a Close control frame";
            else if (event.code == 1007)
                reason = "An endpoint is terminating the connection because it has received data within a message that was not consistent with the type of the message (e.g., non-UTF-8 [http://tools.ietf.org/html/rfc3629] data within a text message).";
            else if (event.code == 1008)
                reason = "An endpoint is terminating the connection because it has received a message that \"violates its policy\". This reason is given either if there is no other sutible reason, or if there is a need to hide specific details about the policy.";
            else if (event.code == 1009)
                reason = "An endpoint is terminating the connection because it has received a message that is too big for it to process.";
            else if (event.code == 1010) // Note that this status code is not used by the server, because it can fail the WebSocket handshake instead.
                reason = "An endpoint (client) is terminating the connection because it has expected the server to negotiate one or more extension, but the server didn't return them in the response message of the WebSocket handshake. <br /> Specifically, the extensions that are needed are: " + event.reason;
            else if (event.code == 1011)
                reason = "A server is terminating the connection because it encountered an unexpected condition that prevented it from fulfilling the request.";
            else if (event.code == 1015)
                reason = "The connection was closed due to a failure to perform a TLS handshake (e.g., the server certificate can't be verified).";
            else
                reason = "Unknown reason";
            console.log(event.code + ": " + reason);

            //$("#thingsThatHappened").html($("#thingsThatHappened").html() + "<br />" + "The connection was closed for reason: " + reason);
        };

        socket.onerror = function () {
            log("Some error");
        }
    }

    catch (ex) {
        log('An Exception Occurred: ' + ex);
    }

    //document.getElementById("msg").focus();
}

function send() {
    var txt, msg;
    txt = $("msg");
    msg = txt.value;

    if (!msg) {
        alert("Message can not be empty");
        return;
    }

    txt.value = "";
    txt.focus();

    try {
        socket.send(msg);
        log('Sent : ' + msg);
    }
    catch (ex) {
        log(ex);
    }
}

function quit() {
    if (socket != null) {
        //log("Goodbye!");
        socket.close();
        socket = null;
    }
}

function reconnect() {
    quit();
    init();
}

function log(logMessage) {
    console.log(logMessage);
}

function createMessageId() {
    return Date.now() + machineName;
}

function showPayHereQuestion() {
    $('.screen-message').hide();
    $('.pay-here-question').show();
}

function showPaymentInProcess() {
    $('.screen-message').hide();
    $('.payment-in-process').show();
}
