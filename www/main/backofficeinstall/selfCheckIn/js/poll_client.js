var socket;
var transactionId;

function init() {
    //var host = "ws://localhost:8080"; // SET THIS TO YOUR SERVER
    var host = "ws://minakami.bungyjapan.com:8080"; // SET THIS TO YOUR SERVER

    try
    {
        socket = new WebSocket(host);
        log('WebSocket - status ' + socket.readyState);

        socket.onopen = function(msg)
        {
            console.log("Opened");
            if(this.readyState == 1)
            {
                setInterval(function(){
                    d = new Date;
                    var hours = d.getHours();
                    var minutes = d.getMinutes();
                    var seconds = d.getSeconds();

                    var timestamp = hours + ":" + minutes + ":" + seconds;

                    var message = {
                        "from" : "MINA03",
                        "to" : "MINA03", //TODO this will be to machineName as it will be
                        "device" : "browser",
                        "message" : timestamp,
                        "messageIdentifier": createMessageId()
                    };

                    socket.send(JSON.stringify(message));
                }, 120000);

            }

        };

        //Message received from websocket server
        socket.onmessage = function(msg)
        {

        };

        //Connection closed
        socket.onclose = function(event)
        {
            log("Disconnected - status " + this.readyState);

            var reason;
            //alert(event.code);
            // See http://tools.ietf.org/html/rfc6455#section-7.4.1
            if (event.code == 1000)
                reason = "Normal closure, meaning that the purpose for which the connection was established has been fulfilled.";
            else if(event.code == 1001)
                reason = "An endpoint is \"going away\", such as a server going down or a browser having navigated away from a page.";
            else if(event.code == 1002)
                reason = "An endpoint is terminating the connection due to a protocol error";
            else if(event.code == 1003)
                reason = "An endpoint is terminating the connection because it has received a type of data it cannot accept (e.g., an endpoint that understands only text data MAY send this if it receives a binary message).";
            else if(event.code == 1004)
                reason = "Reserved. The specific meaning might be defined in the future.";
            else if(event.code == 1005)
                reason = "No status code was actually present.";
            else if(event.code == 1006)
                reason = "The connection was closed abnormally, e.g., without sending or receiving a Close control frame";
            else if(event.code == 1007)
                reason = "An endpoint is terminating the connection because it has received data within a message that was not consistent with the type of the message (e.g., non-UTF-8 [http://tools.ietf.org/html/rfc3629] data within a text message).";
            else if(event.code == 1008)
                reason = "An endpoint is terminating the connection because it has received a message that \"violates its policy\". This reason is given either if there is no other sutible reason, or if there is a need to hide specific details about the policy.";
            else if(event.code == 1009)
                reason = "An endpoint is terminating the connection because it has received a message that is too big for it to process.";
            else if(event.code == 1010) // Note that this status code is not used by the server, because it can fail the WebSocket handshake instead.
                reason = "An endpoint (client) is terminating the connection because it has expected the server to negotiate one or more extension, but the server didn't return them in the response message of the WebSocket handshake. <br /> Specifically, the extensions that are needed are: " + event.reason;
            else if(event.code == 1011)
                reason = "A server is terminating the connection because it encountered an unexpected condition that prevented it from fulfilling the request.";
            else if(event.code == 1015)
                reason = "The connection was closed due to a failure to perform a TLS handshake (e.g., the server certificate can't be verified).";
            else
                reason = "Unknown reason";
            console.log(event.code + ": " + reason);

            //$("#thingsThatHappened").html($("#thingsThatHappened").html() + "<br />" + "The connection was closed for reason: " + reason);
        };

        socket.onerror = function()
        {
            log("Some error");
        }
    }

    catch(ex)
    {
        log('An Exception Occurred: '  + ex);
    }

    //document.getElementById("msg").focus();
}

function send()
{
    var txt, msg;
    txt = $("msg");
    msg = txt.value;

    if(!msg)
    {
        alert("Message can not be empty");
        return;
    }

    txt.value="";
    txt.focus();

    try
    {
        socket.send(msg);
        log('Sent : ' + msg);
    }
    catch(ex)
    {
        log(ex);
    }
}

function quit()
{
    if (socket != null)
    {
        //log("Goodbye!");
        socket.close();
        socket=null;
    }
}

function reconnect()
{
    quit();
    init();
}

function log(logMessage)
{
   console.log(logMessage);
}


function requestWeight() {
    setTimeout(function () {
        transactionStarted();
    }, 2000);
    var message = {
        "from" : machineName,
        "to" : machineName,
        "device" : "scales",
        "message" : "getWeight",
        "messageIdentifier": createMessageId()
    };

    socket.send(JSON.stringify(message));
}

function cancelTransactionForId() {
    var message = {
        "from" : machineName,
        "to" : "server",
        "device" : "cashMachine",
        "message" : machineName + " transaction_cancel " + transactionId,
        "messageIdentifier": createMessageId()
    };

    socket.send(JSON.stringify(message));
}

function newCashPayment(totalAmount) {
    var message = {
        "from" : machineName,
        "to" : "server", //TODO this will be to machineName as it will be
        "device" : "cashMachine",
        "message" : machineName + " transaction_create_cash " + totalAmount,//TODO this number will have to come from the previous page
        "messageIdentifier": createMessageId()
    };
    socket.send(JSON.stringify(message));
}

//presentational functions
function onlyShowTransactionDiv(showDiv){
    var divs = [
        "#transaction-new",
        "#transaction-started",
        "#transaction-change",
        "#transaction-cancel",
        "#transaction-cancelled",
        "#transaction-completed"
    ];

    for(var i = 0; i < divs.length; i++) {
        if(divs[i] != showDiv) $(divs[i]).hide();
        else $(divs[i]).show();
    }
}

function transactionNew() {
    onlyShowTransactionDiv("#transaction-new");
    setTimeout(function () {
        transactionStarted();
    }, 5000);
    //hide other divs
    //show please wait div for 5 seconds
    //fade out
}

function transactionStarted() {
    onlyShowTransactionDiv("#transaction-started");
    //hide other divs
    //show initial amount to be paid
}

function transactionChange() {
    onlyShowTransactionDiv("#transaction-change");
    //hide other divs
    //Show returning change balance due

}

function transactionCancel() {
    onlyShowTransactionDiv("#transaction-cancel");
    transactionCancelled();
    //setTimeout(function() {
    //}, 5000);
}

function transactionCancelled() {
    cancelTransactionForId();
    onlyShowTransactionDiv("#transaction-cancelled");
    //hide other divs

}

function transactionComplete() {
    onlyShowTransactionDiv("#transaction-complete");
    //hide other divs

}


function recalculateBalance(newPayment) {
    amountPaid += parseInt(newPayment);
    amountRemaining = amountInitial - amountPaid;

    console.log("amountPaid: " + amountPaid);
    $(".transaction-amount-paid").html(amountPaid);

    console.log("amountInitial: " + amountInitial);
    $(".transaction-amount-initial").html(amountInitial);

    console.log("amountRemaining: " + amountRemaining);
    $(".transaction-amount-remaining").html(amountRemaining);

    if(amountRemaining <= 0) onCompletion();
}

//message Id is used to identify a messages response from the server
//the message ID of the original message and it's response will be the same
function createMessageId() {
    return Date.now() + machineName;
}

function listenForMessage() {
    //onwebsocket recieves messages

}
 
init();
