<?php
$pageName = "phoneNumber";
require_once('selfCheckInHeader.php')
?>
<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">

    <div id="step12">
        <div class="header-container">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en"><span style="font-size: 80px;">please enter your</span> phone number</span>
                    <span class="lang_ja" style="font-size: 60px;">ご本人の電話番号をご入力ください。</span>
                </h1>
            </div>
        </div>
        <div id="content-container">
            <div id="phone-number"><input name="phone_number"></div>
        </div>
        <div id="keyboard-container">
            <?php
            $keyboard = array(
                array('1', '2', '3', '4', '5'),
                array('6', '7', '8', '9', '0')
            );
            foreach ($keyboard as $line) {
                echo '<div class="keyboard-line">';
                foreach ($line as $letter) {
                    echo "<div class=\"keyboard-button\"><button>" . strtoupper($letter) . "</button></div>";
                };
                echo "</div>";
            };
            ?>
            <div id="enter-del-container">
                <div id="del">
                    <button><span class="lang_en">DELETE</span><span class="lang_ja">訂正</span></button>
                </div>
                <div id="enter">
                    <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
                </div>
            </div>
        </div>
        <div class="back">
            <button>
                <span class="lang_en">&lt;&lt;BACK</span>
                <span class="lang_ja">&lt;&lt;戻る</span>
            </button>
        </div>
    </div>
</form>
<script>
    var pageName = "phoneNumber";

    $(document).ready(function () {
        $('#step12 .keyboard-button button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            var phoneNumber       = $('#step12 #phone-number input').val();

            if((phoneNumber.length == 3)||(phoneNumber.length == 8))
                $('#step12 #phone-number input').val('' + phoneNumber + '-');//add hypens to the phone numbers at positions 4 and 8


            if ($('#step12 #phone-number input').val().length == 13) return;
            $('#step12 #phone-number input').val('' + $('#step12 #phone-number input').val() + $(this).html());
        });

        $('#step12 #del  button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            var phoneNumber          = $('#step12 #phone-number input').val();

            if (phoneNumber.length > 0) {
                if(phoneNumber.charAt(phoneNumber.length - 2) == "-"){//if the number is preceeded by a hyphen
                    $('#step12 #phone-number input').val(phoneNumber.substr(0, phoneNumber.length - 2));//get rid of the number and the hyphen
                } else $('#step12 #phone-number input').val(phoneNumber.substr(0, phoneNumber.length - 1));
            }
        });

        $('#step12 #enter  button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            var phoneNumber         = $('#step12 #phone-number input').val();
            if (phoneNumber.length >= 11 && phoneNumber.length <= 13) {
                submitForm();
            } else {
                show_info("validphone");
            }
        });
    });
</script>
<?php require_once('selfCheckInFooter.php'); ?>
