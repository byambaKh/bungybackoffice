<?php
//load settings from the database
require_once('../includes/application_top.php');
function loadSettings() {
    $sql = "SELECT * FROM settings WHERE site_id = ".CURRENT_SITE_ID." AND key_name = 'self_check_in';";
    $settingsResults = queryForRows($sql);

    if(count($settingsResults))
        return json_decode($settingsResults[0]['value'], true);
    else return array();//return an empty array
}

function saveSettings($settings) {
    //stash as JSON
    $settings['key_name'] = 'self_check_in';
    $settingsJson = mysql_real_escape_string(json_encode($settings));
    if($existingSettings = loadSettings()){//if there are already settings then update
        $id = $existingSettings['id'];
        db_perform('settings', array('site_id' => CURRENT_SITE_ID, 'key_name'=>'self_check_in', 'value' => $settingsJson), 'update', "id = $id");

    } else { //just insert new settings

        db_perform('settings', array('site_id' => CURRENT_SITE_ID, 'key_name'=>'self_check_in', 'value' => $settingsJson));
    }
}

/*
$settings = array('ticketPrinter' => '', 'certificatePrinter' => '',  'simulateCashPayment' => '', 'simulateWeightMeasurement' => '');
scalesCalibrationOffsetKg = '';
saveSettings($settings);

echo '';
*/
