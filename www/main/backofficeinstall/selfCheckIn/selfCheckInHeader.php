<?php
require_once(__DIR__ . "/../includes/application_top.php");

error_reporting(E_ALL);
ini_set('display_errors', 0);

require_once("pageSequence.php");
require_once("functions.php");
$pageSequenceObject = new \SelfCheckIn\PageSequence();
$pageSequence = $pageSequenceObject->getCurrentPageSequence();//default page sequence

$pc_version = true;
?>
<!DOCTYPE html>
<html>
<head>
    <title>Bungy Japan Waiver</title>
    <meta name="apple-mobile-web-app-capable" content="yes">
    <meta name="viewport" content="initial-scale=0.8,user-scalable=no,maximum-scale=1,height=device-height">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">

    <!--<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>-->

    <script src="/js/jquery-1.11.1.min.js"></script>
    <script src="/js/jquery.event.move.js"></script>
    <script src="js/waiver.js"></script>

    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" type="text/css" media="all"/>
    <link rel="stylesheet" href="/css/main_menu.css" type="text/css" media="all"/>
    <link rel="stylesheet" href="/css/selfCheckIn.css" type="text/css" media="all"/>
    <style>
        #step1, #step2, #step3, #step4, #step5, #step6, #step7, {
            margin-left: auto;
            margin-right: auto;
            width: 800px;
            display: table;
        }

        body {
            width: 100%;
        }
    </style>

    <?php require_once(__DIR__ . '/../includes/header_tags.php'); ?>
    <?php
    if ($pc_version) {
        echo "
        <style>
        #step1{
            margin-left: auto;
            margin-right: auto;
            display: inline;
            width: 800px;
        }

        body{
            width: 100%;
        }
        </style>
        ";
    }

    ?>

    <script>
        var pc_version = <?php echo (int)$pc_version; ?>;
        var session = <?php echo json_encode($_SESSION['checkIn'])?>;
        var totalBookingPrice = 0;

        $(document).ready(function () {
            if (session)
                selectLanguage(session['language']);
            else
                selectLanguage('en');

            //TODO why is this here
            document.addEventListener('touchmove', function (e) {
                e.preventDefault();
            });

            //$('input').prop('disabled', 'disabled');//this causes $_POST to be zero. Why is this here???
            $(".back button").bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                document.location = "<?php echo $pageSequenceObject->getPreviousStepUrl($pageName)?>";
            });

            if (pc_version == 0) {
                $('.scrollable').bind('touchmove', scrollmove);
                $('.scrollable').bind('touchend', scrollend);
            }

            if (pc_version) {
                $("body").css('padding-top', '900px');
                $(".back").each(function () {
                    $(this).css("top", 80 + parseInt($(this).css("top").replace('px', '')));
                });

                //This changes the layout depending on if we are on a PC or ipad
                //It would be a good idea to fix all of these in straight css. There is no reason to use JS for layout in
                //this case
                //moved to email.php as it causes an error here as the id cannot be found
                //$("#domains-container").css("top", 55 + parseInt($("#domains-container").css("top").replace('px', '')));
                $("#email-note").css("top", 50);
            }
        });
    </script>
</head>
<!--<body scroll="no"> -->
<body>
