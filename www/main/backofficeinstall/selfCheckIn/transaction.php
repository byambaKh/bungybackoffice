<?
//error_reporting(E_ERROR|E_PARSE);
//error_reporting(E_ALL & ~E_WARNING);
require_once(__DIR__ .'/jcm/config.php');

$pageName = 'transaction';
$amount = calculateRemainingToBePaid();
$hostSettings   = loadHostSettings($host)[0];//load the settings for a particular host
@$transaction   = $mysql->fetchSelectorRow("jcm_transaction", array('id' => $_GET['transaction']));

$bookingId = isset($_SESSION['checkIn']['bookingId']) ? $_SESSION['checkIn']['bookingId'] : 0;
//pr($_SESSION['checkIn']);
//exit();

?>
<?php require_once('selfCheckInHeader.php'); ?>
<script src="js/bookingFunctions.js"></script>
<script>

    var bookingId = <?=$bookingId?>;
    var amountInitial = <?=$amount?>;
    var amountPaid = 0;
    var amountRemaining = amountInitial - amountPaid;

    var machineName = "<?=$host?>";

    function onConnectFunction(){
        console.log("transaction submit");
        newCashPayment(amountInitial);
    }

    function onCompletion() {
        if(bookingId) {
            setBookingPaid(bookingId);
            unlockBooking(bookingId);
        }
        submitForm();
    }

</script>
<script src="js/websocketPage.js"></script>
<script>
    var pageName = "transaction";
    init();//start the web socket
    setTimeout(function () {
        transactionStarted();
    },  4000);

    $(document).ready(function () {
        $('.cancel-payment-button').bind('click touchend MSPointerUp pointerup', function (e) {
            transactionCancel();
            unlockBooking(bookingId);
            //document.location.href = '02_buyPhotos.php';
            window.history.go(-1);
            //e.preventDefault();
            //document.location.href = 'transaction.php?host=<?= $host ?>&transaction=<?= $transaction['id'] ?>&cancel=1';
        });

        recalculateBalance(0);
        transactionNew();
    });
</script>
<style>

    body {
        background: black;
        color: white;
        text-align: center;
        font-family: sans-serif;
    }

    button {
        font-size: 40px;
        background-color: #c00;
        color: white;
        width: 300px;
    }

    h2 {
        color: lime;
    }

    .hidden{
        display: none;
    }
</style>
<form action="" name="waiverForm" method="POST">
    <input type="hidden" name="pageName" value="<?php echo $pageName?>">
    <h1 class="lang_ja">合計金額</h1>
    <h1 class="lang_en">Amount To Pay</h1>
    <h2>&yen; <span class="transaction-amount-initial">0</span></h2>
    <!-- New Transaction -->

    <div id="transaction-new" class="hidden">

        <h1 style="color: yellow;">Please Wait...</h1>

    <!-- Started-->
    </div>
    <div id="transaction-started" class="hidden">
        <h1 class="lang_ja">未入金額</h1>
        <h1 class="lang_en">Amount Remaining</h1>
        <h2 style="color: yellow;">&yen; <span class="transaction-amount-remaining">0</span></h2>
        <br><br>
        <button type="button" class="cancel-payment-button"> CANCEL </button>
    </div>
    <div id="transaction-change" class="hidden">
    <!-- CHANGE-->
        <h1>Returning Change</h1>
        <h2 style="color: yellow;">&yen; <span class="transaction-amount-remaining">0</span></h2>
    </div>
    <div id="transaction-cancel" class="hidden">
            <h2 style="color: yellow;">Cancelling. Returning: &yen; <span class="transaction-amount-paid">0</span></h2>
            <h2 style="color: yellow;">Cancelling. Please Wait...</h2>
    </div>
    <!-- CANCEL-->
    <div id="transaction-cancelled" class="hidden">

    <h2 style="color: yellow;">Cancelling. Please Wait...</h2>

    </div>
    <!-- CANCELLED-->
    <div id="transaction-cancelled" class="hidden">

        <h2 style="color: yellow;">Cancelling. Going back...</h2>
    </div>
    <div id="transaction-completed" class="hidden">
        <h1>Completed</h1>
        <h2 style="color: yellow;">Thank you!</h2>
    </div>
</form>
<img src="img/insert_money_jpn.png" style="float: right; margin-top: -165px;" class="lang_ja">
<img src="img/insert_money_eng.png" style="float: right; margin-top: -165px;" class="lang_en">
<?php require_once('selfCheckInFooter.php'); ?>
