<?php
$pageName = "yearOfBirth";
require_once('selfCheckInHeader.php')
?>
<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">

    <div id="step5">
        <div class="header-container">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en">please enter the year you were born</span>
                    <span class="lang_ja" style="font-size: 60px;">生まれた年を西暦でご入力ください。</span>
                </h1>
            </div>
        </div>
        <div id="content-container">
            <div id="photo-number" style="margin-left:170px;"><input name="birth_year"></div>
        </div>
        <div id="photo-keyboard-container">
            <div class="photo-keyboard">
                <div class="keyboard-button">
                    <button>1</button>
                </div>
                <div class="keyboard-button">
                    <button>2</button>
                </div>
                <div class="keyboard-button">
                    <button>3</button>
                </div>
                <div class="keyboard-button">
                    <button>4</button>
                </div>
                <div class="keyboard-button">
                    <button>5</button>
                </div>
                <div class="keyboard-button">
                    <button>6</button>
                </div>
                <div class="keyboard-button">
                    <button>7</button>
                </div>
                <div class="keyboard-button">
                    <button>8</button>
                </div>
                <div class="keyboard-button">
                    <button>9</button>
                </div>
                <div class="keyboard-button">
                    <button>0</button>
                </div>
            </div>
            <div id="enter-del-container">
                <div id="del">
                    <button><span class="lang_en">DELETE</span><span class="lang_ja">訂正</span></button>
                </div>
                <div id="enter">
                    <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
                </div>
            </div>
        </div>
        <div class="back">
            <button>
                <span class="lang_en">&lt;&lt;BACK</span>
                <span class="lang_ja">&lt;&lt;戻る</span>
            </button>
        </div>
    </div>
</form>

<script>
    var pageName = "yearOfBirth";

    $(document).ready(function () {
        $('#step5 .keyboard-button button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            if ($('#step5 #photo-number input').val().length == 4) return;
            $('#step5 #photo-number input').val('' + $('#step5 #photo-number input').val() + $(this).html());
        });

        $('#step5 #del  button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            var val = $('#step5 #photo-number input').val();
            if (val.length > 0) {
                $('#step5 #photo-number input').val(val.substr(0, val.length - 1));
            }
        });

        $('#step5 #enter  button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            var val = $('#step5 #photo-number input').val();
            if (val.length > 0) {
                if (val > 1900) {
                    //saveWaiverDataToSessionAndGoToNextStep({birth_year: val});
                    submitForm();
                } else {
                    show_info("yeargreater");
                }
            } else {
                show_info("validyear");
            }
        });
    });
</script>
<?php require_once('selfCheckInFooter.php'); ?>
