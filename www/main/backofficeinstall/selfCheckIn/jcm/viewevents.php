<?
require_once('config.php');

$i = new MySQLIterator($mysql->selectionQuery("jcm_event", array('transaction' => $_GET['transaction'])));

?>
<!DOCTYPE html>
<html>
<head>
    <style>
        body {
            background: black;
            color: white;
            text-align: center;
            font-family: sans-serif;
        }

        button {
            font-size: 40px;
            background-color: #c00;
            color: white;
            width: 300px;
        }

        h2 {
            color: lime;
        }

        a {
            color: #c00;
        }

        td {
            padding: 3px 10px;
        }
    </style>
</head>
<body>
<img src="http://test.bungyjapan.com/Waiver/img/wheader.png"/><br/><br/><br/>

<table style="margin: 0 auto;">
    <tr>
        <th>ID</th>
        <th>Created</th>
        <th>Type</th>
        <th>Data / Value</th>
    </tr>
    <? while ($t = $i->getNext()) : ?>
        <tr>
            <td><?= $t['id'] ?></td>
            <td><?= $t['created'] ?></td>
            <td><?= $t['type'] ?></td>
            <td><?= $t['data'] ?></td>
        </tr>
    <? endwhile ?>
</table>

<br/><br/>
<button onclick="javascript:history.go(-1);">BACK</button>
</body>
</html>
