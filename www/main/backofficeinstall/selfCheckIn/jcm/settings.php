<?
require_once('config.php');

if ($_POST) {
    //$_POST['settings'] = json_encode($_POST['settings']);
    foreach ($_POST['settings'] as $hostId => $settings) {
        if (!$hostId) continue;
        //$mysql->updateRows("jcm_host", array($field => $value), array('id' => $hid));
        $scalesWeightOffsetKg = trim($settings['scalesWeightOffsetKg']);

        if(!is_numeric($scalesWeightOffsetKg)) {
            //if there is a non numeric weight offset, set it to zero
           $scalesWeightOffsetKg = 0;

        } else if ($scalesWeightOffsetKg < 0){
            //Only allow additions to the weight, not subtractions as subtractions could be dangerous
            $scalesWeightOffsetKg = 0;

        }

        $settingsToSerialize = array(
            'payments'               => $settings['payments'],
            'scales'                 => $settings['scales'],
            'qrReader'               => $settings['qrReader'],
            'webcam'                 => $settings['webcam'],
            'ticketPrinter'          => $settings['ticketPrinter'],
            'ticketWristBandPrinter' => $settings['ticketWristBandPrinter'],
            'scalesWeightOffsetKg'   => $scalesWeightOffsetKg
        );

        unset($scalesWeightOffsetKg);

        $updatedArray = array(
            //'site'     => 0,//zero for now
            //these items are not set
            //'hostname'  => '',
            //'box1'      => '',
            //'box2'      => '',
            //'cash'      => '',
            //'state'     => '',
            'port'     => $settings['port'],
            'settings' => json_encode($settingsToSerialize)

        );
        $mysql->updateRows("jcm_host", $updatedArray, array('id' => $hostId));
    }
}

$hosts = loadHostSettings();
//header("Location: index.php");
//exit;
?>
<!DOCTYPE html>
<html>
<head>
    <style>
        body {
            background: black;
            color: white;
            text-align: center;
            font-family: sans-serif;
        }

        button {
            font-size: 40px;
            background-color: #c00;
            color: white;
            width: 300px;
        }

        h2 {
            color: lime;
        }

        a {
            color: #c00;
        }
    </style>
</head>
<body>
<img src="/Waiver/img/wheader.png"/><br/><br/><br/>

<form method="post" action="settings.php" style="width: 700px; margin: 0 auto;">
    <? foreach ($hosts as $host) : ?>
        <?
        $payments = $host['settings']['payments'];
        $scales = $host['settings']['scales'];
        $qrReader = $host['settings']['qrReader'];
        $webcam = $host['settings']['webcam'];
        $ticketPrinter = $host['settings']['ticketPrinter'];
        $ticketWristBandPrinter = $host['settings']['ticketWristBandPrinter'];
        $scalesWeightOffsetKg = $host['settings']['scalesWeightOffsetKg'];
        ?>
        <table cellspacing="15" width="1"
               style="border: 1px solid #c00; background-color: #200; padding: 20px; margin: 10px; float: left;">
            <tr>
                <th colspan="2"><?= $host['hostname'] ?></th>
            </tr>
            <tr>
                <td>COM&nbsp;Port</td>
                <td><input type="text" name="<?= "settings[{$host['id']}][port]" ?>" value="<?= $host['port'] ?>"/></td>
            </tr>
            <tr>
                <td>Box&nbsp;#1</td>
                <td align="right">&yen; <?= 1000 * $host['box1'] ?></td>
            </tr>
            <tr>
                <td>Box&nbsp;#2</td>
                <td align="right">&yen; <?= 5000 * $host['box2'] ?></td>
            </tr>
            <tr>
                <td>Payments</td>
                <td align="right">
                    <?= Form::hidden("settings[{$host['id']}][payments]", 0); ?>
                    <?= Form::radioButton("settings[{$host['id']}][payments]", "0", "Off", array('checkedForValue' => $payments)); ?>
                    <?= Form::radioButton("settings[{$host['id']}][payments]", "1", "On", array('checkedForValue' => $payments)); ?>
                </td>
            </tr>
            <tr>
                <td>Scales</td>
                <td align="right">
                    <?= Form::hidden("settings[{$host['id']}][scales]", 0); ?>
                    <?= Form::radioButton("settings[{$host['id']}][scales]", "0", "Off", array('checkedForValue' => $scales)); ?>
                    <?= Form::radioButton("settings[{$host['id']}][scales]", "1", "On", array('checkedForValue' => $scales)); ?>
                </td>
            </tr>
            <tr>
                <td>QR Code</td>
                <td align="right">
                    <?= Form::hidden("settings[{$host['id']}][qrReader]", 0); ?>
                    <?= Form::radioButton("settings[{$host['id']}][qrReader]", "0", "Off", array('checkedForValue' => $qrReader)); ?>
                    <?= Form::radioButton("settings[{$host['id']}][qrReader]", "1", "On", array('checkedForValue' => $qrReader)); ?>
                </td>
            </tr>
            <tr>
                <td>Web Cam</td>
                <td align="right">
                    <?= Form::hidden("settings[{$host['id']}][webcam]", 0); ?>
                    <?= Form::radioButton("settings[{$host['id']}][webcam]", "0", "Off", array('checkedForValue' => $webcam)); ?>
                    <?= Form::radioButton("settings[{$host['id']}][webcam]", "1", "On", array('checkedForValue' => $webcam)); ?>
                </td>
            </tr>
            <tr>
                <td>Ticket Printer</td>
                <td align="right">
                    <?= Form::hidden("settings[{$host['id']}][ticketPrinter]", 0); ?>
                    <?= Form::radioButton("settings[{$host['id']}][ticketPrinter]", "0", "Off", array('checkedForValue' => $ticketPrinter)); ?>
                    <?= Form::radioButton("settings[{$host['id']}][ticketPrinter]", "1", "On", array('checkedForValue' => $ticketPrinter)); ?>
                </td>
            </tr>
            <tr>
                <td>Wrist Band Printer</td>
                <td align="right">
                    <?= Form::hidden("settings[{$host['id']}][ticketWristBandPrinter]", 0); ?>
                    <?= Form::radioButton("settings[{$host['id']}][ticketWristBandPrinter]", "0", "Off", array('checkedForValue' => $ticketWristBandPrinter)); ?>
                    <?= Form::radioButton("settings[{$host['id']}][ticketWristBandPrinter]", "1", "On", array('checkedForValue' => $ticketWristBandPrinter)); ?>
                </td>
            </tr>
            <tr>
                <td>Scales Weight Offset (Kg)</td>
                <td><input type="text" name="<?= "settings[{$host['id']}][scalesWeightOffsetKg]" ?>" value="<?= $scalesWeightOffsetKg ?>"/></td>
            </tr>
            <tr>
                <td colspan="2"><a href="viewtransactions.php?host=<?= $host['id'] ?>">View Transactions</a></td>
            </tr>
        </table>
    <? endforeach ?>
    <div style="clear: both;"></div>
    <br/><br/>
    <button type="submit">SAVE ALL</button>
</form>
</body>
</html>
