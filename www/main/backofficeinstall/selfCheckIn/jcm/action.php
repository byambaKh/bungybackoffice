<?php
require_once('config.php');
$amount = $_GET['amount'];

//stub payment code
$hostSettings   = loadHostSettings($host)[0];//load the settings for a particular host
if(!$hostSettings['settings']['payments']) {
    header("Location: transaction.php?host=$host&transaction=");
}
//TODO this code could be merged in to the top of transaction.php and we can get rid of this file
if ($_GET['create'] == "cash") {
    list ($status, $transaction) = explode(",", shell_exec("php /home/bungyjapan/private/jcm/jcm.php $host transaction_create_cash " . $amount));
    header("Location: transaction.php?host=$host&transaction=" . $transaction);
    exit;
}
if ($_GET['create'] == "coupon") {
    list ($status, $transaction) = explode(",", shell_exec("php /home/bungyjapan/private/jcm/jcm.php $host transaction_create_coupon"));
    header("Location: transaction.php?host=$host&transaction=" . $transaction);
    exit;
}
if ($_GET['create'] == "payout") {
    list ($status, $transaction) = explode(",", shell_exec("php /home/bungyjapan/private/jcm/jcm.php $host transaction_create_payout -" . $amount)); // <- NOTE: This is a negative amount!
    header("Location: transaction.php?host=$host&transaction=" . $transaction);
    exit;
}

?>
<!DOCTYPE html>
<html>
<head>
    <style>
        body {
            background: black;
            color: white;
            text-align: center;
            font-family: sans-serif;
        }

        button {
            font-size: 40px;
            background-color: #c00;
            color: white;
            width: 300px;
        }

        h2 {
            color: lime;
        }
    </style>
</head>
<body>
<img src="http://test.bungyjapan.com/Waiver/img/wheader.png"/><br/><br/><br/>
<? foreach (range(1000, 15000, 1000) as $i) : ?>
    <button onclick="document.location.href='action.php?host=<?= $host ?>&create=cash&amount=<?= $i ?>';">
        Pay &yen; <?= $i ?></button>
    <? if (!($i % 3000)) : ?>
        <br/>
    <? endif ?>
<? endforeach ?>
<button style="width: 910px; height: 80px;"
        onclick="document.location.href='action.php?host=<?= $host ?>&create=coupon';">Use Gift Certificate
</button>
<br/>
<? foreach (range(1000, 15000, 1000) as $i) : ?>
    <button onclick="document.location.href='action.php?host=<?= $host ?>&create=payout&amount=<?= $i ?>';">
        Payout &yen; <?= $i ?></button>
    <? if (!($i % 3000)) : ?>
        <br/>
    <? endif ?>
<? endforeach ?>
<br/><br/>
<a href="index.php" style="color: white; display: inline-block; border: 1px solid white; padding: 5px;">Change Running
    Host</a>
</body>
</html>
