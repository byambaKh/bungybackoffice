<?
//error_reporting(E_ERROR|E_PARSE);
//error_reporting(E_ALL & ~E_WARNING);
require_once('config.php');

$pageName = 'transaction';

$hostSettings   = loadHostSettings($host)[0];//load the settings for a particular host
@$transaction   = $mysql->fetchSelectorRow("jcm_transaction", array('id' => $_GET['transaction']));

//Payment simulation code
if(!$hostSettings['settings']['payments']) {//check if the settings
    $transaction['state']   = "COMPLETED";
    $transaction['type']    = "CASH";
    $transaction['total']   = 1000;
    $transaction['balance'] = $_GET['amount'];
    $_GET['cancel'] = null;
}

if ($_GET['cancel']) {
    shell_exec("php /home/bungyjapan/private/jcm/jcm.php $host transaction_cancel $transaction[id]");
    header("Location: transaction.php?host=$host&transaction=$transaction[id]");
}

//$transaction['state'] = 'STARTED';

?>
<?php require_once('../selfCheckInHeader.php'); ?>
<script>
    var pageName = "transaction";

    $(document).ready(function () {
        $('.cancel-payment-button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            document.location.href = 'transaction.php?host=<?= $host ?>&transaction=<?= $transaction['id'] ?>&cancel=1';
        });
    });
</script>
<style>
    body {
        background: black;
        color: white;
        text-align: center;
        font-family: sans-serif;
    }

    button {
        font-size: 40px;
        background-color: #c00;
        color: white;
        width: 300px;
    }

    h2 {
        color: lime;
    }
</style>
<!--<img src="http://test.bungyjapan.com/Waiver/img/wheader.png"/><br/><br/><br/>-->
<!--
<h2 style="color: yellow;" class="lang_ja">写真を購入された方は、受付カウンターで<br>５００円の返金が受けられます。</h2>
<h2 style="color: yellow;" class="lang_en">For those who purchased photos, <br> the extra ￥500 will be returned to you <br> at the reception counter.</h2>
-->
<? if ($transaction['type'] == "CASH") : ?>
<form action="" name="waiverForm" method="POST">
    <input type="hidden" name="pageName" value="<?php echo $pageName?>">
    <h1 class="lang_ja">合計金額</h1>
    <h1 class="lang_en">Amount To Pay</h1>
    <h2>&yen; <?= $transaction['total'] ?></h2>

    <? if ($transaction['state'] == "NEW") : ?>

        <h1 style="color: yellow;">Please Wait...</h1>
        <script>setTimeout(function () {
                document.location.href = document.location.href;
            }, 1000);</script>

    <? elseif ($transaction['state'] == "STARTED") : ?>

        <h1 class="lang_ja">未入金額</h1>
        <h1 class="lang_en">Amount Remaining</h1>
        <h2 style="color: yellow;">&yen; <?= ($transaction['total'] - $transaction['balance']) ?></h2>
        <br><br>
        <button class="cancel-payment-button"> CANCEL </button>
        <!--<button
            onclick="document.location.href = 'transaction.php?host=<?= $host ?>&transaction=<?= $transaction['id'] ?>&cancel=1';">
            CANCEL
        </button>-->
        <script>setTimeout(function () {
                document.location.href = document.location.href;
            }, 1000);</script>

    <?
    elseif ($transaction['state'] == "CHANGE") : ?>

        <h1>Returning Change</h1>
        <h2 style="color: yellow;">&yen; <?= ($transaction['balance'] - $transaction['total']) ?></h2>
        <script>setTimeout(function () {
                document.location.href = document.location.href;
            }, 1000);</script>

    <?
    elseif ($transaction['state'] == "CANCEL") : ?>

        <? if ($transaction['balance']) : ?>
            <h2 style="color: yellow;">Cancelling. Returning: &yen; <?= ($transaction['balance']) ?></h2>
        <? else : ?>
            <h2 style="color: yellow;">Cancelling. Please Wait...</h2>
        <? endif ?>
        <script>setTimeout(function () {
                document.location.href = document.location.href;
            }, 1000);</script>

    <?
    elseif ($transaction['state'] == "CANCELLED") : ?>

        <h2 style="color: yellow;">Cancelling. Going back...</h2>
        <script>setTimeout(function () {
                //document.location.href = "action.php?host=<?= $host ?>";
                document.location.href = "/selfCheckIn/02_buyPhotos.php";
            }, 5000);</script>

    <?
    elseif ($transaction['state'] == "COMPLETED") : ?>
        <h1>Completed</h1>
        <h2 style="color: yellow;">Thank you!</h2>

        <script>

            //var nextStep = getNextStep();
            //removePaymentPagesFromPageSequence(pageSequence);
            //saveWaiverDataToSession({pageSequence: pageSequence});

            setTimeout(function () {

                submitForm('action=completedPayment');
            }, 5000);</script>

    <? endif ?>

<? elseif ($transaction['type'] == "COUPON") : ?>

    <? if ($transaction['state'] == "NEW") : ?>

        <h1 style="color: yellow;">Please Wait...</h1>
        <script>setTimeout(function () {
                document.location.href = document.location.href;
            }, 1000);</script>

    <? elseif ($transaction['state'] == "STARTED") : ?>

        <h1>Please Insert your Gift Certificate</h1>
        <br><br>
        <button class="cancel-payment-button"> CANCEL </button>
        <script>setTimeout(function () {
                document.location.href = document.location.href;
            }, 1000);</script>

    <?
    elseif ($transaction['state'] == "CANCEL") : ?>

        <h2 style="color: yellow;">Cancelling. Please Wait...</h2>
        <script>setTimeout(function () {
                document.location.href = document.location.href;
            }, 1000);</script>

    <?
    elseif ($transaction['state'] == "CANCELLED") : ?>

        <h2 style="color: yellow;">Cancelling. Going back...</h2>
        <script>setTimeout(function () {
                document.location.href = "action.php?host=<?= $host ?>";
            }, 5000);</script>

    <?
    elseif ($transaction['state'] == "COMPLETED") : ?>

        <? $event = $mysql->fetchSelectorRow("jcm_event", array('type' => "ACCEPT_COUPON", 'transaction' => $transaction['id'])) ?>
        <h2 style="color: yellow;">Coupon Received: <?= $event['data'] ?></h2>
        <script>setTimeout(function () {
                document.location.href = "action.php?host=<?= $host ?>";
            }, 5000);</script>

    <? endif ?>

<?
elseif ($transaction['type'] == "PAYOUT") : ?>

    <h1>Total Amount To Payout</h1>
    <h2>&yen; <?= abs($transaction['total']) ?></h2>
    <h1>Paid</h1>
    <h2>&yen; <?= abs($transaction['balance']) ?></h2>

    <? if ($transaction['state'] == "NEW") : ?>

        <h1 style="color: yellow;">Please Wait...</h1>
        <script>setTimeout(function () {
                document.location.href = document.location.href;
            }, 1000);</script>

    <? elseif ($transaction['state'] == "STARTED") : ?>

        <h1>Remaining Amount</h1>
        <h2 style="color: yellow;">&yen; <?= ($transaction['balance'] - $transaction['total']) ?></h2>
        <script>setTimeout(function () {
                document.location.href = document.location.href;
            }, 1000);</script>

    <?
    elseif ($transaction['state'] == "COMPLETED") : ?>

        <h2 style="color: yellow;">Thank you!</h2>
        <script>
            setTimeout(function () {
                document.location.href = "<?php echo goToNextStep().'&action=completedPayment'?>";
            }, 1000);
        </script>

    <? endif ?>

<? endif ?>
</form>

<?if($transaction['state'] != "COMPLETED") :?>
    <img src="../img/insert_money_jpn.png" style="float: right; margin-top: -165px;" class="lang_ja">
    <img src="../img/insert_money_eng.png" style="float: right; margin-top: -165px;" class="lang_en">
<? endif ?>
<?php require_once('../selfCheckInFooter.php'); ?>
