<?
require_once('config.php');

$host = $mysql->fetchSelectorRow("jcm_host", array('id' => $_GET['host']));
$i = new MySQLIterator($mysql->selectionQuery("jcm_transaction", array('host' => $_GET['host'])));
?>
<!DOCTYPE html>
<html>
<head>
    <style>
        body {
            background: black;
            color: white;
            text-align: center;
            font-family: sans-serif;
        }

        button {
            font-size: 40px;
            background-color: #c00;
            color: white;
            width: 300px;
        }

        h2 {
            color: lime;
        }

        a {
            color: #c00;
        }

        td {
            padding: 3px 10px;
        }
    </style>
</head>
<body>
<img src="http://test.bungyjapan.com/Waiver/img/wheader.png"/><br/><br/><br/>

<table style="margin: 0 auto;">
    <tr>
        <th>ID</th>
        <th>Created</th>
        <th>Host</th>
        <th>Type</th>
        <th>State</th>
        <th>Total</th>
        <th>Balance</th>
        <th></th>
    </tr>
    <? while ($t = $i->getNext()) : ?>
        <tr>
            <td><?= $t['id'] ?></td>
            <td><?= $t['created'] ?></td>
            <td><?= $host['hostname'] ?></td>
            <td><?= $t['type'] ?></td>
            <td><?= $t['state'] ?></td>
            <td><?= $t['total'] ?></td>
            <td><?= $t['balance'] ?></td>
            <td><a href="viewevents.php?transaction=<?= $t['id'] ?>">View Events</a></td>
        </tr>
    <? endwhile ?>
</table>

<br/><br/>
<button onclick="javascript:history.go(-1);">BACK</button>
</body>
</html>
