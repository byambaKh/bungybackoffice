<?

function now()
{
    return date("Y-m-d H:i:s");
}

function output($status, $data = null)
{
    $o = array();
    $o[] = $status;
    if (is_array($data)) {
        $o = array_merge($o, $data);
    } else if (strlen($data)) {
        $o[] = $data;
    }
    echo implode(",", $o);
    file_put_contents("/home/bungyjapan/private/jcm/jcm.log", now() . " < " . implode(",", $o) . "\n", FILE_APPEND);
}

function event($trans, $type, $data = null)
{
    global $mysql, $host;
    return $mysql->insertRow("jcm_event", array('created' => now(), 'host' => $host['id'], 'transaction' => $trans['id'], 'type' => $type, 'data' => $data));
}

function transaction($id)
{
    global $mysql, $host;
    return $mysql->fetchSelectorRow("jcm_transaction", array('id' => $id, 'host' => $host));
}

function host_config()
{
    global $mysql, $host;
    output("OK", array($host['port']));
}

function host_recycler_setcount($args)
{
    // [0] = Count of bills in Box 1
    // [1] = Count of bills in Box 2
    global $mysql, $host;
    $mysql->updateRows("jcm_host", array('box1' => $args[0], 'box2' => $args[1]), array('id' => $host['id']));
    output("OK");
}

function transaction_create_cash($args)
{
    // [0] = Total amount of transaction
    global $mysql, $host;
    $mysql->executeQuery("UPDATE `jcm_transaction` SET `state` = 'FAILED' WHERE `host` = '$host[id]' AND `state` NOT IN ('COMPLETED','CANCELLED')");
    $id = $mysql->insertRow("jcm_transaction", array('created' => now(), 'host' => $host['id'], 'state' => "NEW", 'type' => "CASH", 'total' => $args[0]));
    output("OK", $id);
}

function transaction_create_coupon()
{
    global $mysql, $host;
    $mysql->executeQuery("UPDATE `jcm_transaction` SET `state` = 'FAILED' WHERE `host` = '$host[id]' AND `state` NOT IN ('COMPLETED','CANCELLED')");
    $id = $mysql->insertRow("jcm_transaction", array('created' => now(), 'host' => $host['id'], 'state' => "NEW", 'type' => "COUPON"));
    output("OK", $id);
}

function transaction_create_payout($args)
{
    // [0] = Total amount of payout (NOTE: this should be negative)
    global $mysql, $host;
    $mysql->executeQuery("UPDATE `jcm_transaction` SET `state` = 'FAILED' WHERE `host` = '$host[id]' AND `state` NOT IN ('COMPLETED','CANCELLED')");
    $id = $mysql->insertRow("jcm_transaction", array('created' => now(), 'host' => $host['id'], 'state' => "NEW", 'type' => "PAYOUT", 'total' => $args[0]));
    output("OK", $id);
}

function transaction_listen_new()
{
    global $mysql, $host;
    while (true) {
        $trans = $mysql->fetchSelectorRow("jcm_transaction", array('host' => $host['id'], 'state' => "NEW"));
        if ($trans['id']) {
            output("OK", $trans);
            $mysql->updateRows("jcm_transaction", array('state' => "STARTED"), array('id' => $trans['id']));
            break;
        }
        sleep(1); // TODO: Change this to notify events instead.
    }
}

function transaction_check_balance($args)
{
    // [0] = Transaction ID
    global $mysql, $host;
    $trans = transaction($args[0]);
    output("OK", $trans['balance']);
}

function transaction_check_remaining($args)
{
    // [0] = Transaction ID
    global $mysql, $host;
    $trans = transaction($args[0]);
    output("OK", $trans['total'] - $trans['balance']);
}

function transaction_check_cancel($args)
{
    // [0] = Transaction ID
    global $mysql, $host;
    $trans = transaction($args[0]);
    if ($trans['state'] == "CANCEL") {
        output("CANCEL");
    } else {
        output("OK");
    }
}

function transaction_cancel($args)
{
    // [0] = Transaction ID
    global $mysql, $host;
    $mysql->updateRows("jcm_transaction", array('state' => "CANCEL"), array('id' => $args[0]));
    output("OK");
}

function event_cancelled($args)
{
    // [0] = Transaction ID
    global $mysql, $host;
    $trans = transaction($args[0]);
    event($trans, "CANCELLED");
    $mysql->updateRows("jcm_transaction", array('state' => "CANCELLED"), array('id' => $trans['id']));
    output("OK");
}

function event_accept_cash($args)
{
    // [0] = Transaction ID
    // [1] = Bill Amount
    global $mysql, $host;
    $trans = transaction($args[0]);
    $bill = $args[1];
    event($trans, "ACCEPT_CASH", $bill);
    if ($trans['balance'] < $trans['total']) {
        $mysql->updateRows("jcm_transaction", array('balance' => $trans['balance'] + $bill), array('id' => $trans['id']));
        $trans = transaction($trans['id']);
        if ($trans['balance'] < $trans['total']) {
            output("CONTINUE");
        } elseif ($trans['balance'] == $trans['total']) {
            $mysql->updateRows("jcm_transaction", array('state' => "COMPLETED"), array('id' => $trans['id']));
            output("STOP");
        } elseif ($trans['balance'] > $trans['total']) {
            $mysql->updateRows("jcm_transaction", array('state' => "CHANGE"), array('id' => $trans['id']));
            output("PAYOUT", array($trans['balance'] - $trans['total']));
        }
    } else {
        output("REJECT");
    }
}

function event_payout($args)
{
    // [0] = Transaction ID
    // [1] = Payout Bill
    global $mysql, $host;
    $trans = transaction($args[0]);
    $bill = $args[1];
    event($trans, "PAYOUT", $bill);
    $mysql->updateRows("jcm_transaction", array('balance' => $trans['balance'] - $bill), array('id' => $trans['id']));
    $trans = transaction($trans['id']);
    if ($trans['type'] == "CASH" && $trans['state'] == "CHANGE" && $trans['balance'] == $trans['total']) {
        $mysql->updateRows("jcm_transaction", array('state' => "COMPLETED"), array('id' => $trans['id']));
    } else if ($trans['type'] == "PAYOUT" && $trans['state'] == "STARTED" && $trans['balance'] == $trans['total']) {
        $mysql->updateRows("jcm_transaction", array('state' => "COMPLETED"), array('id' => $trans['id']));
    }
    output("OK");
}

function event_accept_coupon($args)
{
    // [0] = Transaction ID
    // [1] = Coupon Code
    global $mysql, $host;
    $trans = transaction($args[0]);
    $coupon = $args[1];
    event($trans, "ACCEPT_COUPON", $coupon);
    if (true) { // TODO: Check here for coupon validity
        output("IDLE");
    } else {
        output("REJECT");
    }
}

date_default_timezone_set("Asia/Tokyo");

require "MySQL.php";
require "MySQLIterator.php";
$mysql = new MySQL("bungyjapan_book_main", "bookmain", "mainstar99", "localhost:3307");

array_shift($argv);

file_put_contents("/home/bungyjapan/private/jcm/jcm.log", now() . " > " . implode(" ", $argv) . "\n", FILE_APPEND);

$hostname = array_shift($argv);
$host = $mysql->fetchSelectorRow("jcm_host", array('hostname' => $hostname));
$command = array_shift($argv);

if (function_exists($command)) {
    call_user_func($command, $argv);
}

?>
