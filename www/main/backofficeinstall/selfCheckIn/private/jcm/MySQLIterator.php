<?php

// v.1.0

class MySQLIterator
{
    private $result;
    private $index;
    private $row;

    private $rowCount;
    private $fieldCount;
    private $fieldMeta;

    private $fullColumnNames;

    /**
     * MySQL Iterator Class
     *
     * Example Usage:
     * $MySQL = new MySQL('database');
     * $Iterator = new MySQLIterator($MySQL->executeQuery("SELECT * FROM ..."));
     * while ($Iterator->hasNext()) {
     *     $row = $Iterator->getNext();
     * }
     *
     * @param resource $result
     * @return MySQLIterator
     */
    public function __construct($result)
    {
        if (is_resource($result)) {
            $this->result = $result;
        } else {
            $this->error("Iterator result is not a valid MySQL resource.");
        }
        $this->index = -1;
        $this->rowCount = -1;
        $this->fieldCount = -1;
        $this->fieldMeta = null;
        $this->fullColumnNames = false;
    }

    private function error($error_type)
    {
        $msg = 'Unable to complete MySQLIterator operation: ' . $error_type;
        trigger_error($msg, E_USER_WARNING);
    }

    /**
     * Check if there are additional rows to fetch.
     *
     * @return boolean
     */
    public function hasNext()
    {
        return (($this->index + 1) < $this->getRowCount());
    }

    /**
     * Fetch the next row for the iterator result.
     *
     * @param int $flags - Type of array to return. (Default: MYSQL_ASSOC).
     * @return array - Associative row array.
     */
    public function getNext()
    {
        if ($this->hasNext()) {
            if ($this->fullColumnNames) {
                $row = mysql_fetch_array($this->result, MYSQL_NUM);
                for ($i = 0; $i < $this->getFieldCount(); $i++) {
                    $meta = $this->getFieldMeta($i);
                    $this->row[$meta->table . '.' . $meta->name] = $row[$i];
                }
            } else {
                $this->row = mysql_fetch_array($this->result, MYSQL_ASSOC);
            }
            if (!$this->row) {
                $this->error('Could not get next row');
            }
            $this->index++;
            return is_array($this->row) ? $this->row : null;
        } else {
            return null;
        }
    }

    /**
     * Get a particular field for the current row. Must have called getNext() previously.
     *
     * @param string $field - Field name.
     * @return string - Value.
     */
    public function getField($field)
    {
        if (is_array($this->row) && isset($this->row[$field])) {
            return $this->row[$field];
        }
        return null;
    }

    /**
     * Enable/disable full column names on the rows retrieved.
     * (table_name.column_name).
     *
     * @param bool $enabled - Enabled. (Default: true).
     */
    public function setFullColumnNames($enabled = true)
    {
        $this->fullColumnNames = $enabled;
    }

    /**
     * Move the pointer to the row specified. Row can range
     * between 0 and total-1. The following getNext() call
     * will retrieve the row with this number.
     *
     * @param int $row - Row to go to
     * @return bool -
     */
    public function goToRow($row)
    {
        if ($row >= 0 && $row < $this->getRowCount()) {
            if (mysql_data_seek($this->result, $row)) {
                $this->index = $row - 1;
                $this->row = array();
                return true;
            }
        }
        return false;
    }

    /**
     * Get the total number of rows available for this result.
     *
     * @return int
     */
    public function getRowCount()
    {
        if ($this->rowCount >= 0) {
            return $this->rowCount;
        }
        $num = mysql_num_rows($this->result);
        if ($num === false) {
            $this->error("Could not get row count.");
        } else {
            $this->rowCount = $num;
            return $num;
        }
    }

    /**
     * Get the total number of fields/columns available for this result.
     *
     * @return int
     */
    public function getFieldCount()
    {
        if ($this->fieldCount >= 0) {
            return $this->fieldCount;
        }
        $num = mysql_num_fields($this->result);
        if ($num === false) {
            $this->error("Could not get field count.");
        } else {
            $this->fieldCount = $num;
            return $num;
        }
    }

    /**
     * Get the field meta for a column.
     *
     * @param mixed $field - Field offset or name to get meta for.
     * @return int
     */
    public function getFieldMeta($field)
    {
        if (is_null($this->fieldMeta)) {
            // Build it.
            for ($i = 0; $i < $this->getFieldCount(); $i++) {
                $meta = mysql_fetch_field($this->result, $i) OR
                $this->error('Could not get field meta.');
                $this->fieldMeta[$i] = $meta;
            }
        }
        if (is_int($field)) {
            // If it's a numeric field, the data is readily available.
            return $this->fieldMeta[$field];
        }
        foreach ($this->fieldMeta as $meta) {
            // If it's a string, search for the field with that name.
            if ($meta->name == $field || ($meta->table . '.' . $meta->name) == $field) {
                return $meta;
            }
        }
        return null;
    }
}

?>