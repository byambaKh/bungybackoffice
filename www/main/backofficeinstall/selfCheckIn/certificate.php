<?php
$fullName = urldecode($_GET['fullName']);
$date = urldecode($_GET['date']);
$jumpNumber = urldecode($_GET['jumpNumber']);
$printer = urldecode($_GET['printer']);
echo '';

?>
<!DOCTYPE html>
<html>
<head>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
    <script>
        $(document).ready(function () {
            var printer = '<?=$printer?>';

            setTimeout(function() {
                jsPrintSetup.setPrinter(printer);

				// set page header
				jsPrintSetup.setOption('headerStrLeft', '');
				jsPrintSetup.setOption('headerStrCenter', '');
				jsPrintSetup.setOption('headerStrRight', '');
				// set empty page footer
				jsPrintSetup.setOption('footerStrLeft', '');
				jsPrintSetup.setOption('footerStrCenter', '');
				jsPrintSetup.setOption('footerStrRight', '');

                jsPrintSetup.setSilentPrint(true);
                jsPrintSetup.printWindow(window);
            }, 9000);
        });
    </script>
    <style>
        div{
            display: block;
            font-family: "Arial", "Sans-Serif", "Helvetica";
            font-weight: normal;
            margin-left: 33px;
        }

        #fullName{
            margin-top: 345px;
            font-size: 28px;

        }

        #jumpNumber{
            margin-top: 255px;
            font-size: 20px;
        }

        #date{
            margin-top: 55px;
            font-size: 20px;
        }

        #signature{
            margin-top: 22px;
            margin-left: 37px;
            width: 150px;
            height: 60px;
        }
    </style>
</head>
<body>
<div id="fullName"><?php echo $fullName ?></div>
<div id="jumpNumber"><?php echo $jumpNumber ?></div>
<div id="date"><?php echo $date ?></div>
<img id="signature" src="/img/signature.jpg">

</body>
<!--<img src="../Signatures/index.php?imageName=31167_32467.jpg" id="signature">-->
</html>
