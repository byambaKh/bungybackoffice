<?php
$pageName = "exit";
require_once('selfCheckInHeader.php')
?>
<script>
    //setTimeout(function () { submitForm("action=finishWaiver"); }, 10000);
</script>
<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">

    <div id="step17">
        <div class="header-container">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en">Age Rejected</span>
                    <span class="lang_ja">?????????????</span>
                </h1>

                <h2>
                    <span class="lang_en">Unfortunately your age is outside of the acceptable range for us to allow you to jump. Please speak to a member of staff and collect a refund.</span>
					<span class="lang_ja"></span>
                </h2>
            </div>
        </div>
    </div>
</form>
<script>
    var pageName = "exit";
    $(document).ready(function () {
    });
</script>
<script src="/js/reception.js" type="text/javascript"></script>

<?php require_once('selfCheckInFooter.php'); ?>

