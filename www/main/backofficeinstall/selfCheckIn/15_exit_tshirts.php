<?php
$pageName = "exit";
require_once('selfCheckInHeader.php');

//load the printer names
$loadedResults = mysql_query('SELECT * FROM settings WHERE site_id = ' . CURRENT_SITE_ID . ' AND key_name = "printerTicketAndCertificate"');
$settingsRow = mysql_fetch_assoc($loadedResults);
$savedPrintersArray = explode("\n", $settingsRow['value']);

$certificatePrinter = $ticketPrinter = '';//set them to an empty string if there are no results

$findPrintersResultsCount = mysql_num_rows($loadedResults);

if ($findPrintersResultsCount) {
    $ticketPrinter = trim($savedPrintersArray[1]);
}

$date = urlencode(date("Y-m-d"));
$signatureUrl = urlencode('');

$paid = $_SESSION['totalPayment'];
$ticketPrinter = urlencode($ticketPrinter);
$host = getRemoteHostName();

$tshirtFrameUrl= "../selfCheckIn/ticketLayoutTshirt.php?printer=$ticketPrinter&host=$host&paid=$paid";
?>

<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">

    <div id="step15">
        <div class="header-container">
            <div class="head-image-container">
                <h2>
                    <span class="lang_en">Almost done... Please take the T-shirt ticket to the reception counter.</span>
                    <span class="lang_ja">あともう少しです….Tシヤーツ券を<br/>受付までお持ちください。</span>
                </h2>
            </div>
        </div>
        <div class="restart-container">
            <div id="step15progress"></div>
            <iframe style="display:none" name="ticketmini" id="ticketmini" src="<?php echo $tshirtFrameUrl?>"></iframe>
        </div>
    </div>
</form>
<img src="img/get_ticket_eng.png" class="lang_en">
<img src="img/get_ticket_jpn.png" class="lang_ja">
<script>
    var pageName = "exit";
    setTimeout(function () { submitForm("action=finishWaiver"); }, 10000);
    /*
     $(document).ready(function () {
     printTicket('<?php //echo $ticketPrinter;?>', '<?php //echo $certificatePrinter;?>');
     //printForDemo();
     //clearSessionWaiverData();
     });
     */
</script>
<script src="/js/reception.js" type="text/javascript"></script>

<?php require_once('selfCheckInFooter.php'); ?>
