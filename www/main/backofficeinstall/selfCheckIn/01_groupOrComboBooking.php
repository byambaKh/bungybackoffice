<?php
$pageName = "groupOrComboBooking";
require_once('selfCheckInHeader.php')
?>
<script>
    var machineName = "<?= $host ?>";
    var bookingId = <?= json_encode($_SESSION['data']['bookingId']) ?>;
</script>
<script src="js/bookingFunctions.js"></script>
<script src="js/groupOrComboBooking.js"></script>
<style>
    #del {
        float: left;
        margin-left: 0;
        text-align: right;
        width: 542px;
    }

    #yes button {
        background-color: #0f0;
        border: 2px solid white;
        color: black;
        font-family: "pussycat_snickers";
        font-size: 90px;
        height: 90px;
        line-height: 100px;
        text-align: center;
        width: 465px;
        clear: none;
    }

    #no button {
        background-color: red;
        border: 2px solid white;
        color: white;
        font-family: "pussycat_snickers";
        font-size: 90px;
        height: 90px;
        line-height: 100px;
        text-align: center;
        width: 465px;
        clear: none;
    }

    .header-container{
        margin-bottom: 0px;
        margin-top: 10px;
    }

    #no{
        display: inline;
        width:140px;
    }

    #no button{
        width:200px;
    }

    #yes{
        display: inline;
        width:140px
    }

    #yes button{
        width:200px;
    }
    .payment-in-progress, .payment-complete{
        display:none;
    }
</style>
<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?= $pageName ?>">

    <div id="step17">
        <div class="pay-here-question screen-message">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en">This booking must be paid for at one machine. Would you like to pay here?</span>
					<span class="lang_ja">お支払いはグループの合計金額を投入してください。</span>
                </h1>
            </div>
        </div>

        <div class="payment-in-progress screen-message">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en">This booking is currently being paid for at another machine. Check-in will continue once payment is complete.</span>
                    <span class="lang_ja">こちらの予約は他の機械でお支払い中です。お支払い完了後、チェックインはこちらで続けてできます。</span>
                </h1>
            </div>
        </div>

        <div class="payment-complete screen-message">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en">Payment complete.</span>
                    <span class="lang_ja">お支払い完了</span>
                </h1>
            </div>
        </div>

        <div id="yes">
            <button>
                <span class="lang_en">YES</span>
                <span class="lang_ja">はい</span>
            </button>
        </div>

        <div id="no">
            <button style="width:300px">
                <span class="lang_en">NO</span>
                <span class="lang_ja" >いいえ</span>
            </button>
        </div>

        <div id="enter">
            <button type="button" onclick="document.location.href='13_4_weight_websocket.php';">
                <span style="width: 100%;" class="lang_en" style="font-size: 70px;">&lt;&lt;Back</span>
                <span style="width: 100%;" class="lang_ja" style="font-size: 70px;">&lt;&lt;戻る</span>
            </button>
        </div>

    </div>
</form>
<script>init();
    var pageName = "groupOrComboBooking";
    $(document).ready(function() {

    });

    function onCompletion() {
        console.log("performing completion");
        submitForm();
    }
</script>
<script src="/js/reception.js" type="text/javascript"></script>

<?php require_once('selfCheckInFooter.php'); ?>

