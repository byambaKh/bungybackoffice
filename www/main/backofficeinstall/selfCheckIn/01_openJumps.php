<?php
$pageName = "openJumps";
require_once('selfCheckInHeader.php');
//require '../includes/application_top.php';
//echo Html::head("Self Check-In", array("reception.css", '/Waiver/stylesheet.css', 'selfCheckIn.css'), array());

//Todo This needs to grab the booking data from the database and check if it has been paid
?>
<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">
    <input type="hidden" name="bookingDateAndTime" value="">
    <style>
        .openSlots{
            width:100%;
            font-size:60px;
        }

        select{
            background-color:white;
        }

        option{
            text-align: center;
            color: black;
        }

        option[class='unavailableJumps']{
            background-color:grey;
            color: white;
        }
    </style>

    <div id="step2">
        <div class="header-container">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en"><?php echo SYSTEM_SUBDOMAIN_REAL?> Availability</span>
                    <span class="lang_ja">空き状況</span>
                </h1>

                <h2>
                    <span class="lang_en">Please Select A Time and Press [Continue]</span>
                    <span class="lang_ja">時間を選択して（次へ）をクリックしてください</span>
                </h2>
            </div>
            <div class="refresh">
                <button>
                    <span class="lang_en">refresh</span>
                    <span class="lang_ja">更新</span>
                </button>
            </div>
        </div>
            <?php

            error_reporting(E_ALL);
            //$timeSlot = DateTime::createFromFormat("Y:m:d H:i:s A", "2014-11-28 09:30 AM");
            //$currentTime = DateTime::createFromFormat("Y-m-d H:i", "2015-01-03 09:00");//including AM or PM at the end causes unexpected results
            $currentTime = new DateTime('now');
            //$openJumps = getNextTimeSlots($timeSlot, 5);
            //$jumpsAndSlots = getOpenJumpsForNextNumberOfSlots($timeSlot, 10);
            $jumpsAndSlots = getOpenJumpsForNextNumberOfSlotsOriginal($currentTime, 10);
            //for each open slots and jumps echo a row in a table that with ...

            //no free slots today
            //$jumpsAndSlots = array();
            $slotCount = count($jumpsAndSlots);
            if($slotCount >= 2){
                //test slot 1 & 2
                if(($jumpsAndSlots[0]['openJumps'] > 0) || ($jumpsAndSlots[1]['openJumps'] > 0)) {

                }

            } else if ($slotCount == 1) {
               if(($jumpsAndSlots[0]['openJumps'] > 0)) { //only test slot 1
               }

                //redirect
            } else {//no slots
                //please speak to reception
            }
            if(count($jumpsAndSlots)){
                echo "<div id='names-container'>";
                echo "<div id='names-container-child' class='scrollable'>";
                echo "<select multiple='multiple' id='booking-list'>";
                foreach($jumpsAndSlots as $jumpsAndSlot) {
                    if($jumpsAndSlot['openJumps'] <= 0) {
                        continue;
                        //Disable showing Full Slots
                        //$slotStyle = "unavailableJumps";
                        //$disabled = "disabled='disabled'";
                    }  else {
                        $slotStyle = "availableJumps";
                        $disabled = "";
                    }

                    $bookingDateAndTimeString   = $jumpsAndSlot['bookingDateAndTime']->format('Y-m-d H:i A');
                    $bookingTimeString          = $jumpsAndSlot['bookingDateAndTime']->format('H:i');
                    $openJumpCount              = $jumpsAndSlot['openJumps'];
                    $jumpString                 = ($jumpsAndSlot['openJumps'] == 1) ? 'Jump&nbsp' : 'Jumps';
                    echo "<option $disabled class='$slotStyle' value='$bookingDateAndTimeString'>$bookingTimeString - $openJumpCount $jumpString</option>\n";
                }
                echo '</select>';
                echo '</div>';
                echo '</div>';
                ?>
                <div id="bottom-container">
                    <button>
                        <span class="lang_en">continue</span>
                        <span class="lang_ja">次へ</span>
                    </button>
                </div>
               <?php
            } else {
                ?>
                <h1>
                    <span class="lang_en">No free slots today!</span>
                    <span class="lang_ja">空き状況</span>
                </h1>
                <?php
            }
            //$openJump = getOpenJumpsForSlot($timeSlot);
            //$currentSlot = roundTimeToSlot($currentTime);

            //get time slots for now and the next 30 min interval


            ?>
        <div class="back">
            <button>
                <span class="lang_en">&lt;&lt;BACK</span>
                <span class="lang_ja">&lt;&lt;戻る</span>
            </button>
        </div>
    </div>
</form>


<script>
    var pageName = "bookingName";

    $(document).ready(function () {
        $('#step2 .refresh button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            return fill_names();
        });
        $('#step2 #bottom-container button').bind('click touchend MSPointerUp pointerup', function (e) {
            //if the item is disabled, do nothing
            //else set the form element to be nothing
            //
            e.preventDefault();
            var selected = false;
            if (pc_version == 0) {
                $('#step2 li').each(function () {
                    if ($(this).hasClass('selected')) selected = $(this).prop('id');
                });
            } else {
                selected = $("#step2 select").val();
                if (typeof selected == 'undefined') {
                    selected = false;
                }
                console.log(selected)
            }
            ;
            if (selected) {
                //save_var('booking_id', selected);
                document.waiverForm.bookingDateAndTime.value = selected;
                submitForm();
                //saveWaiverDataToSessionAndGoToNextStep({booking_id: selected});
            } else {
                show_info("openslot");//TODO this needs a new message
                save_var('booking_id', 'null');
            }
            ;
        });
    });
</script>
<? require_once('selfCheckInFooter.php'); ?>
