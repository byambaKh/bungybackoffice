<?php
namespace SelfCheckIn;
class PageSequence
{
    private $initialSteps = [
        ["pageName" => "selfCheckIn", "pageUrl" => "selfCheckIn.php", "parameters" => "", "hidden" => false],
        ["pageName" => "bookOrWalk", "pageUrl" => "00_bookOrWalk.php", "parameters" => "", "hidden" => false],
        ["pageName" => "weight", "pageUrl" => "13_4_weight_websocket.php", "parameters" => "", "hidden" => false],
        ["pageName" => "waiver", "pageUrl" => "05_waiver.php", "parameters" => "", "hidden" => false],
    ];

    private $walkInBookingPageSequence = [
        ["pageName" => "selfCheckIn", "pageUrl" => "selfCheckIn.php", "parameters" => "", "hidden" => false],
        ["pageName" => "bookOrWalk", "pageUrl" => "00_bookOrWalk.php", "parameters" => "", "hidden" => false],
        ["pageName" => "weight", "pageUrl" => "13_4_weight_websocket.php", "parameters" => "", "hidden" => false],
        ["pageName" => "waiver", "pageUrl" => "05_waiver.php", "parameters" => "", "hidden" => false],
        //remove as we are allowing walk in jumps to just happen as the customer asks the reception staff
        //before checking in so the just must already be possible
        //["pageName" => "noOpenJumps", "pageUrl" => "01_noOpenJumps.php", "parameters" => "", "hidden" => false],
        ["pageName" => "yearOfBirth", "pageUrl" => "06_yearOfBirth.php", "parameters" => "", "hidden" => false],
        ["pageName" => "monthAndDateOfBirth", "pageUrl" => "07_monthAndDateOfBirth.php", "parameters" => "", "hidden" => false],
        ["pageName" => "lastName", "pageUrl" => "09_lastName.php", "parameters" => "", "hidden" => false],
        ["pageName" => "firstName", "pageUrl" => "10_firstName.php", "parameters" => "", "hidden" => false],
        ["pageName" => "prefecture", "pageUrl" => "11_prefecture.php", "parameters" => "", "hidden" => false],
        ["pageName" => "phoneNumber", "pageUrl" => "12_phoneNumber.php", "parameters" => "", "hidden" => false],
        ["pageName" => "email", "pageUrl" => "13_email.php", "parameters" => "", "hidden" => false],
        ["pageName" => "sex", "pageUrl" => "08_sex.php", "parameters" => "", "hidden" => false],
        ["pageName" => "jumpType", "pageUrl" => "03_jumpType.php", "parameters" => "", "hidden" => false],
        ["pageName" => "buyPhotos", "pageUrl" => "02_buyPhotos.php", "parameters" => "", "hidden" => false],
        ["pageName" => "transaction", "pageUrl" => "transaction.php", "parameters" => "", "hidden" => false, "meta" => "regular"],
        ["pageName" => "signature", "pageUrl" => "14_signature.php", "parameters" => "", "hidden" => false],
        ["pageName" => "exit", "pageUrl" => "15_exit.php", "parameters" => "", "hidden" => false]
    ];

    private $jcmDebugPageSequence = [
        ["pageName" => "selfCheckIn", "pageUrl" => "selfCheckIn.php", "parameters" => "", "hidden" => false],
        ["pageName" => "weight", "pageUrl" => "13_4_weight_websocket.php", "parameters" => "", "hidden" => false],
        ["pageName" => "waiver", "pageUrl" => "05_waiver.php", "parameters" => "", "hidden" => false],
        ["pageName" => "jumpType", "pageUrl" => "03_jumpType.php", "parameters" => "", "hidden" => false],
        ["pageName" => "buyPhotos", "pageUrl" => "02_buyPhotos.php", "parameters" => "", "hidden" => false],
        ["pageName" => "transaction", "pageUrl" => "transaction.php", "parameters" => "", "hidden" => false],
        ["pageName" => "signature", "pageUrl" => "14_signature.php", "parameters" => "", "hidden" => false],
    ];

    private $preBookingPageSequence = [
        ["pageName" => "selfCheckIn", "pageUrl" => "selfCheckIn.php", "parameters" => "", "hidden" => false],
        ["pageName" => "bookOrWalk", "pageUrl" => "00_bookOrWalk.php", "parameters" => "", "hidden" => false],
        ["pageName" => "bookingName", "pageUrl" => "01_bookingName.php", "parameters" => "", "hidden" => false],
        ["pageName" => "weight", "pageUrl" => "13_4_weight_websocket.php", "parameters" => "", "hidden" => false],
        ["pageName" => "waiver", "pageUrl" => "05_waiver.php", "parameters" => "", "hidden" => false],
        ["pageName" => "yearOfBirth", "pageUrl" => "06_yearOfBirth.php", "parameters" => "", "hidden" => false],
        ["pageName" => "monthAndDateOfBirth", "pageUrl" => "07_monthAndDateOfBirth.php", "parameters" => "", "hidden" => false],
        ["pageName" => "lastName", "pageUrl" => "09_lastName.php", "parameters" => "", "hidden" => false],
        ["pageName" => "firstName", "pageUrl" => "10_firstName.php", "parameters" => "", "hidden" => false],
        ["pageName" => "prefecture", "pageUrl" => "11_prefecture.php", "parameters" => "", "hidden" => false],
        ["pageName" => "phoneNumber", "pageUrl" => "12_phoneNumber.php", "parameters" => "", "hidden" => false],
        ["pageName" => "email", "pageUrl" => "13_email.php", "parameters" => "", "hidden" => false],
        ["pageName" => "sex", "pageUrl" => "08_sex.php", "parameters" => "", "hidden" => false],
        ["pageName" => "jumpType", "pageUrl" => "03_jumpType.php", "parameters" => "", "hidden" => false],
        ["pageName" => "buyPhotos", "pageUrl" => "02_buyPhotos.php", "parameters" => "", "hidden" => false],
        ["pageName" => "transaction", "pageUrl" => "transaction.php", "parameters" => "", "hidden" => false, "meta" => "regular"],
        ["pageName" => "signature", "pageUrl" => "14_signature.php", "parameters" => "", "hidden" => false],
        ["pageName" => "exit", "pageUrl" => "15_exit.php", "parameters" => "", "hidden" => false]
    ];

    private $preBookingGroupOrComboPageSequence = [
        ["pageName" => "selfCheckIn", "pageUrl" => "selfCheckIn.php", "parameters" => "", "hidden" => false],
        ["pageName" => "bookOrWalk", "pageUrl" => "00_bookOrWalk.php", "parameters" => "", "hidden" => false],
        ["pageName" => "bookingName", "pageUrl" => "01_bookingName.php", "parameters" => "", "hidden" => false],
        ["pageName" => "paymentInProcess", "pageUrl" => "01_paymentInProcess.php", "parameters" => "", "hidden" => false],
        ["pageName" => "transactionGroupCombo", "pageUrl" => "transactionGroupCombo.php", "parameters" => "", "hidden" => false, "meta" => "groupCombo"],
        ["pageName" => "weight", "pageUrl" => "13_4_weight_websocket.php", "parameters" => "", "hidden" => false],
        ["pageName" => "waiver", "pageUrl" => "05_waiver.php", "parameters" => "", "hidden" => false],
        ["pageName" => "yearOfBirth", "pageUrl" => "06_yearOfBirth.php", "parameters" => "", "hidden" => false],
        ["pageName" => "monthAndDateOfBirth", "pageUrl" => "07_monthAndDateOfBirth.php", "parameters" => "", "hidden" => false],
        ["pageName" => "lastName", "pageUrl" => "09_lastName.php", "parameters" => "", "hidden" => false],
        ["pageName" => "firstName", "pageUrl" => "10_firstName.php", "parameters" => "", "hidden" => false],
        ["pageName" => "prefecture", "pageUrl" => "11_prefecture.php", "parameters" => "", "hidden" => false],
        ["pageName" => "phoneNumber", "pageUrl" => "12_phoneNumber.php", "parameters" => "", "hidden" => false],
        ["pageName" => "email", "pageUrl" => "13_email.php", "parameters" => "", "hidden" => false],
        ["pageName" => "sex", "pageUrl" => "08_sex.php", "parameters" => "", "hidden" => false],
        ["pageName" => "buyPhotos", "pageUrl" => "02_buyPhotos.php", "parameters" => "", "hidden" => false],
        ["pageName" => "transaction", "pageUrl" => "transaction.php", "parameters" => "", "hidden" => false, "meta" => "regular"],
        ["pageName" => "signature", "pageUrl" => "14_signature.php", "parameters" => "", "hidden" => false],
        ["pageName" => "exit", "pageUrl" => "15_exit.php", "parameters" => "", "hidden" => false]
    ];

    private $tShirtsOnlyPageSequence = [
        ["pageName" => "selfCheckIn", "pageUrl" => "selfCheckIn.php", "parameters" => "", "hidden" => false],
        ["pageName" => "bookOrWalk", "pageUrl" => "00_bookOrWalk.php", "parameters" => "", "hidden" => false],
        ["pageName" => "buyTShirts", "pageUrl" => "02_buyTShirts.php", "parameters" => "", "hidden" => false],
        ["pageName" => "transaction", "pageUrl" => "transaction.php", "parameters" => "", "hidden" => false, "meta" => "regular"],
        ["pageName" => "exit", "pageUrl" => "15_exit_tshirts.php", "parameters" => "", "hidden" => false]
    ];

    function getCurrentPageSequence()
    {
        //if there already is a page sequence, return that or else return
        if(isset($_SESSION['checkIn']['pageSequence'])) {
            return $_SESSION['checkIn']['pageSequence'];
        } else {
            return $this->initialSteps;

        }
    }

    function getPageSequenceForCheckInType($checkInType = '')
    {
        $pageSequence = $this->initialSteps;

        /*
        //TODO replace below with this
        $checkInPageSequence = $checkInType.'PageSequence';
        if(property_exists($this, $checkInPageSequence)) {
            $pageSequence = $$checkInPageSequence;
        }
        */

        if ($checkInType == 'preBooking') {
            $pageSequence = $this->preBookingPageSequence;

        } else if ($checkInType == 'walkInBooking') {
            $pageSequence = $this->walkInBookingPageSequence;

        } else if ($checkInType == 'tShirtsOnly') {
            $pageSequence = $this->tShirtsOnlyPageSequence;

        } else if ($checkInType == 'jcmDebug') {
            //SET Booking information here
            $pageSequence = $this->jcmDebugPageSequence;

        } else if ($checkInType == 'preBookingGroupOrCombo') {

            $pageSequence = $this->preBookingGroupOrComboPageSequence;
        }

        return $pageSequence;
    }

    function setPageSequenceForCheckInType($checkInType = '')
    {
        $_SESSION['checkIn']['pageSequence'] = $this->getPageSequenceForCheckInType($checkInType);
    }

    function indexOfPageInPageSequence($pageName, $meta = NULL) {
        $pageSequence = $_SESSION['checkIn']['pageSequence'];

        for ($index = 0; $index < count($pageSequence); $index++) {
            if($meta == NULL) {
                if ($pageSequence[$index]['pageName'] == $pageName) {
                    return $index;
                }
            } else {
                //there are two transaction pages. To prevent removing both of them at the same time
                //they are given 'meta' information. We only remove the ones with the same meta information
                //as supplied in the function argument
                if(array_key_exists("meta", $pageSequence[$index])) {
                    if($pageSequence[$index]['meta'] == $meta) {
                        $pageSequence[$index]['hidden'] = true;
                        return $index;
                    }
                }

            }
        }
        return false;
    }


    function getNextStep($pageName) {
        $pageSequence = $_SESSION['checkIn']['pageSequence'];
        $nextPageIndex = $this->indexOfPageInPageSequence($pageName) + 1;

        $nextPage = &$pageSequence[$nextPageIndex];

        if ($nextPageIndex >= (count($pageSequence)) || ($nextPage == null)) {//if the page is out of bounds return the first page
            return $pageSequence[0];

        } else if ($nextPage['hidden']) {
            return $this->getNextStep($nextPage['pageName']);//recursion

        } else return $pageSequence[$nextPageIndex];
    }

    function getNextStepUrl($pageName) {
        $nextStep = $this->getNextStep($pageName);
        return $nextStep['pageUrl'];
    }

    function getPreviousStep($pageName) {
        $pageSequence = $_SESSION['checkIn']['pageSequence'];
        $previousPageIndex = $this->indexOfPageInPageSequence($pageName) - 1;

        $previousPage = &$pageSequence[$previousPageIndex];

        if ($previousPageIndex < 0) {
            return $pageSequence[0];

        } else if ($previousPage['hidden']) {
            return $this->getPreviousStep($previousPage['pageName']);//recursion

        } else return $pageSequence[$previousPageIndex];
    }

    function getPreviousStepUrl($pageName) {
        $previousStep = $this->getPreviousStep($pageName);
        return $previousStep['pageUrl'];
    }

    function goToPageWithUrl($url) {
        header("Location: $url");//include next and previous buttons in get?
        exit();
    }

    function goToNextStep($pageName) {
        $this->goToPageWithUrl($this->getNextStepUrl($pageName));
    }

    function goToPreviousStep($pageName) {
        $this->goToPageWithUrl($this->getPreviousStepUrl($pageName));
    }

    function hidePageFromPageSequence($page, $meta = NULL) {
        $pageSequence = &$_SESSION['checkIn']['pageSequence'];

        $index = $this->indexOfPageInPageSequence($page, $meta);
        if($index !== false) {
            if($pageSequence[$index]['hidden'] == false) {
                $pageSequence[$index]['hidden'] = true;
                return true;
            }
            else return false;//the page was already hidden
        }
        return false;
    }

    function pageIsViewable($page, $meta = NULL){
        $pageSequence = &$_SESSION['checkIn']['pageSequence'];

        $index = $this->indexOfPageInPageSequence($page, $meta);
        if($index !== false)
            return $pageSequence[$index]['hidden'];
        else return false; //if the page does not exist also return false

    }

    function unHidePagesFromPageSequence($pages, $meta = NULL) {
        $pageSequence = &$_SESSION['checkIn']['pageSequence'];

        foreach ($pages as $page) {
            $index = $this->indexOfPageInPageSequence($page, $meta);
            $pageSequence[$index]['hidden'] = false;
        }
    }

    function __construct(){
        if (!isset($_SESSION['checkIn']['pageSequence'])) $_SESSION['checkIn']['pageSequence'] = $this->initialSteps;
        $pageSequence = &$_SESSION['checkIn']['pageSequence'];

        if ($pageSequence == null) $pageSequence = $this->initialSteps;
    }

}

/*
$pageSequence = new \SelfCheckIn\PageSequence();

$_SESSION['checkIn']['pageSequence'] = $pageSequence->getPageSequenceForCheckInType('preBookingGroupOrCombo');

$pageSequence->hidePageFromPageSequence('transaction', 'regular');
$pageSequence->hidePageFromPageSequence('transaction', 'groupCombo');

echo '';
*/


