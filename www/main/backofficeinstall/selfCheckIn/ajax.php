<?php
require_once('../includes/application_top.php');
mysql_query("SET NAMES UTF8;");
$result = array();
switch (TRUE) {
    case (isset($_GET['action']) && $_GET['action'] == 'save-waiver'):
    
        $_POST = $_SESSION['bj_waiver'];
        if (is_array($_POST['booking_id'])) {
            $_POST['booking_id'] = $_POST['booking_id'][0];
        };
        //if this is a walkin booking then save the booking first, then save the waiver data
        //insert into the database
        //get the registration id
        //use bid for waiver data for waiver data
        $firstName = strtoupper($_POST['first_name']);
        $lastName = strtoupper($_POST['last_name']);
        $date = date("Y-m-d");//Year Month Day 2014-01-21
        $time = date("H:i:s A");//Hours Mins Seconds Meridian 13:12 PM

        $walkInBookingArray = array(
            'site_id' => CURRENT_SITE_ID,
            'BookingDate' => $date,//today
            'BookingTime' => '',//What times?
            'NoOfJump' => 1,
            /*
            'RomajiName' 		=> $lastName." ".$firstName,
            'CustomerLastName' 	=> $firstName,
            'CustomerFirstName' 	=> $lastName,
            */
            'RomajiName' => "DEMO TEST",
            'CustomerLastName' => "TEST",
            'CustomerFirstName' => "DEMO",

            'ContactNo' => $_POST['phone_number'],
            'CustomerEmail' => $_POST['email'],
            'BookingType' => 'walkIn',
            'Rate' => getFirstJumpRate(CURRENT_SITE_ID, date("Y-m-d")),//get from site defaults
            'bookingreceived' => $date . " " . $time,//now
            'collectpay' => 'onsite',
            'ratetopay' => 0,
            'ratetopayqty' => 1,
            'checked' => 1,
            'splitname1' => $lastname . " " . $firstname,
            'splitname2' => $lastname . " " . $firstname,
            'splitname3' => $lastname . " " . $firstname,
            'photos_qty' => $_post['photo_number'],
            'username' => "bungy",
            'place_id' => current_site_id
        );


        if ($_post['bookingtype'] == 'walkinbooking') {
            db_perform('customerregs1', $walkinbookingarray);
        }

        $previousinsertid = mysql_insert_id();

        $data = array(
            'bid' => $_post['booking_id'],
            'photo_number' => $_post['photo_number'],
            'agree' => $_post['agree'],
            'birth_year' => $_post['birth_year'],
            'birth_month' => date("m", mktime(0, 0, 0, preg_replace("/([^\d]+)/", "", $_post['birth_month']), 1, 2000)),
            'birth_day' => $_post['birth_day'],
            'sex' => $_post['sex'],
            //'lastname' 		=> strtoupper($_post['last_name']),
            //'firstname' 	=> strtoupper($_post['first_name']),
            'lastname' => "TEST",
            'firstname' => "DEMO",
            'prefecture' => mb_strtoupper(preg_replace("/([\P{Latin}]+)/u", "", $_POST['prefecture'])),
            'phone_number' => $_POST['phone_number'],
            'email' => strtoupper($_POST['email']),
            'sig_img' => $_POST['sig_img'],
            //'weight'            => $_POST['weight'];
            'weight' => $_POST['weight']
        );


        if ($_POST['bookingType'] == 'walkInBooking') {
            $data['bid'] = $previousInsertId;
            $data['site_id'] = CURRENT_SITE_ID;
        }

        db_perform('waivers', $data);

        $result['success'] = true;
        break;

    case (isset($_GET['action']) && $_GET['action'] == 'get-names'):
        $i = 0;
        while ($i++ < 10) {
            $result['data'][] = array(
                'id' => 1,
                'name' => 'Test customer',
            );
        };
        /*
        if you run :

        SELECT cr.CustomerRegID, cr.CustomerLastName, cr.CustomerFirstName, cr.NoOfJump, count(w.id) as total_w
        FROM customerregs1 cr
        LEFT JOIN waivers w on (cr.customerregid = w.bid)
        WHERE
        cr.site_id = 1
            AND cr.BookingDate = '2014-09-17'
            and cr.Checked = 1
            background:UP BY cr.CustomerRegID;

        as you complete waivers, the total_w increases. This is the total number of waivers the application logic
        only takes people off of the list of waivers if NoJump < total_w. ie: they only disappear if they have
        completed the waiver system the number of time they are jumping.

        */

        $result = array();
        $bDate = date('Y-m-d');

        $siteId = CURRENT_SITE_ID;
        $sql = "
        SELECT cr.CustomerRegID,
            cr.CustomerLastName,
            cr.CustomerFirstName,
            SUM(cr.NoOfJump) AS NoOfJump,
            count(w.id) as total_w,
            @master_id:= IF(GroupBooking <= 1, CustomerRegId, GroupBooking) AS master_id #Master ID is the id of the either customerregs1 or groupbooking (if it is a vairable booking child booking)
		FROM customerregs1 cr
		LEFT JOIN waivers w on (CustomerRegId = w.bid)
		WHERE
			cr.site_id = $siteId
			AND cr.BookingDate = '$bDate'
		GROUP BY master_id;
        ";

        /*
        $sql = "SELECT cr.CustomerRegID, cr.CustomerLastName, cr.CustomerFirstName, cr.NoOfJump, count(w.id) as total_w
		FROM customerregs1 cr
		LEFT JOIN waivers w on (cr.customerregid = w.bid)
		WHERE
			cr.site_id = '" . CURRENT_SITE_ID . "'
			AND cr.BookingDate = '" . $bDate . "'
		GROUP BY cr.CustomerRegID;";
        */

        $res = mysql_query($sql) or die(json_encode(array('error' => 'some issues with DB')));
        while ($row = mysql_fetch_assoc($res)) {
            if ($row['total_w'] < $row['NoOfJump']) {
                $result['data'][] = array(
                    'id' => $row['CustomerRegID'],
                    'name' => strtoupper(htmlentities(mb_convert_encoding($row['CustomerLastName'] . ' ' . $row['CustomerFirstName'], 'UTF-8', 'SJIS')))
                );
            };
        };
        $result['success'] = true;
        break;

    case (isset($_GET['action']) && $_GET['action'] == 'getFreeSlots'):
        break;

    case (isset($_GET['action']) && $_GET['action'] == 'saveWaiverDataToSession'):
        //append the waiver data to $_SESSION['bj_waiver'];
        //On the AWS instance running array merge on a non array returns nothing
        if (!is_array($_SESSION['bj_waiver'])) $_SESSION['bj_waiver'] = array();
        //Add new post data to the $_SESSION and write if $_POST and $_SESSION have the same
        //key over write it with the value of the key in $_POST
        if (!isset($_POST)) $_POST = array();

        $fp = fopen('sessionDebug.log', "w");

        $_SESSION['bj_waiver'] = array_merge($_SESSION['bj_waiver'], $_POST);

        fputs($fp, print_r("Get Array", true));
        fputs($fp, print_r($_GET, true));
        fputs($fp, print_r("Session Array", true));
        fputs($fp, print_r($_SESSION, true));
        fputs($fp, print_r("Post Array", true));
        fputs($fp, print_r($_POST, true));
        fclose($fp);

        break;

    case (isset($_GET['action']) && $_GET['action'] == 'clearSessionWaiverData'):
        unset($_SESSION['bj_waiver']);
        break;

    case (isset($_GET['action']) && $_GET['action'] == 'use_one_time_pin'):
        $success = oneTimePin::useOneTimePin($_GET['pin'], CURRENT_SITE_ID);
        $result['success'] = ($success ? true : false);
        break;
}

Header("Content-type: application/json; encoding: utf-8");
echo json_encode($result);
