<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php
require_once("../includes/application_top.php");
$time = urldecode($_GET['time']);
$weight = urldecode($_GET['weight']);
$bookingType = urldecode($_GET['bookingType']);
$jumpCount = urldecode($_GET['jumpCount']);
$photoNumber = urldecode($_GET['photoNumber']);
$cordColor = urldecode($_GET['cordColor']);
//we could run a check to see if cord color matches the weight
$groupOfNumber = urldecode($_GET['groupOfNumber']);
$firstName = urldecode($_GET['firstName']);
$lastName = urldecode($_GET['lastName']);
$printer = urldecode($_GET['printer']);
$host = urldecode($_GET['host']);
$paid = urldecode($_GET['paid']);

switch($cordColor){
    case ("yellow"):
        $yellowHl = "highlight-cord";
    break;
    case ("red"):
        $redHl = "highlight-cord";
    break;
    case ("blue"):
        $blueHl = "highlight-cord";
    break;
    case ("black"):
        $blackHl = "highlight-cord";
    break;
}
/*
$time = "8:00";
$weight = "66";
$bookingType = "Walkin";
$jumpNumber = "5";
$photoNumber = "5";
$cordColor = "Yellow";
//we could run a check to see if cord color matches the weight
$groupOfNumber = "3";
$firstName = "TEST";
$lastName = "TEST";
$printer = urldecode($_GET['printer']);
*/

?>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <title>Ticket</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
    <style type="text/css">
        @media print, screen {
            body{
                font-family: "Arial", "Helvetica", "sans-serif";
            }
            @font-face {
                font-family: 'pussycat_snickers';
                src: url('../fonts/pusss___-webfont.eot');
                src: url('../fonts/pusss___-webfont.eot?#iefix') format('embedded-opentype'),
                url('../fonts/pusss___-webfont.woff') format('woff'),
                url('../fonts/pusss___-webfont.ttf') format('truetype'),
                url('../fonts/pusss___-webfont.svg#pussycat_snickers') format('svg');
                font-weight: normal;
                font-style: normal;
            }
            .cellContent {
                font-size: 20px;
            }

            td {
                border: 1px;
                border-style: solid;
            }
            .bigText{
                font-size: 60px;
                font-weight: 700;
                line-height: 50px;
            }

            .bigHeader {
                font-size: 40px;
            }
            .bigNameText{
                font-size:35px;
                line-height: 30px;
            }

            .smallNameText {
                font-size: 17px;
            }

            .mediumText {
                font-size: 25px;
            }

            .mediumLargeText {
                font-size: 37px;
            }

            table {
                border-collapse: collapse;
                width: 260px;
                height: 370px;
            }

            .blackOnWhite {
                font-family: 'pussycat_snickers';
                background-color: black;
                color: white;
            }

            .centerText {
                text-align: center;
            }

            .cellHeader {
                display: block;
                font-size: 18px;
                text-align: left;
            }
            .cellHeaderRight{
                float: left;
                font-size: 15px;
                text-align: right;
                width: 50%;
            }
            .cellHeaderLeft{
                float: left;
                width: 50%;
                font-size: 18px;
                text-align: left;
            }
            .time{
                float: left;
                width: 100%;
            }
            .highlight-cord{
               text-decoration: underline;
            }
        }
    </style>

    <script>
    $(document).ready(function () {
        var printer = '<?=$printer?>';

        setTimeout(function() {
            jsPrintSetup.setPrinter(printer);
            jsPrintSetup.setSilentPrint(true);

            jsPrintSetup.printWindow(window);
        }, 1000);
    });

    </script>
</head>
<body>

<table width="200px">
    <tr class="blackOnWhite centerText bigHeader">
        <td colspan=2>BUNGY JAPAN</td>
    </tr>
    <tr>
        <td colspan=2>
            <div class="cellHeader">Time:<?php echo date("Y-m-d H:i:s"); ?></div>
        </td>
    </tr>
    <tr>
        <td colspan=2>
            <div class="centerText bigNameText time"><?php echo "1 Bungy T-Shirt"; //TODO this should only appear for walkin bookings or not at all ?></div>
        </td>
    </tr>
    <tr>
        <td colspan=2>
            <div class="bigNameText time">Size: </div>
        </td>
    </tr>
    <tr>
        <td colspan=2 class="blackOnWhite centerText mediumText">Japan’s Only Bungy Professional</td>
    </tr>
    <tr>
        <td colspan=2 class="blackOnWhite centerText mediumText"><?="{$host}#".date('ymd')."#$paid" ?></td>
    </tr>
</table>

</body>
</html>
