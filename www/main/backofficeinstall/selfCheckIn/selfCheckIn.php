<?php
$pageName = "selfCheckIn";
require_once('selfCheckInHeader.php');
$_SESSION['checkIn'] = null;//clear the session on restart
cancelIncompletePayment(getRemoteHostName());//cancel any payments that are not complete for this host

?>
<script>
    var machineName = 'MINA01';
</script>
<script src="js/poll_client.js"></script>
<form action="controller.php" id="waiverForm" name="waiverForm" method="POST">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">
    <input type="hidden" name="language" value="x">     
        <a href ="cordreport.php" style="float: right; display:inline; margin-top:-1000px; color: #ccc; font-size: 16px;">C</a>
        <a href ="scmreport.php" style="float: right; display:inline;margin-right:15px ;margin-top:-1000px; color: #ccc; font-size: 16px;">-R-</a>
        <a href ="dailySalesReportSelfCheckIn.php" style="margin-left: 10px; margin-right: 5px; float: left; display:inline; margin-top:-1000px; color: #ccc; font-size: 16px;">S</a>
        <a href ="cash_out.php" style="margin-left: 30px; float: left; display:inline; margin-top:-1000px; color: #ccc; font-size: 16px;">&yen;</a>
    <div id="step1">
        <div id="waiver-container">
            <div class="header-container"><h1 style="font-size: 100px;">Welcome to Bungy Japan <br> Self Check-In</h1> </div>
            <div id="language-container">
                <div id="english-container" class="half">
                    <button><img src="img/wenglish.png"></button>
                </div>
                <div id="japanese-container" class="half">
                    <button><img src="img/wjapanese.png"></button>
                </div>

            </div>
        </div>
        <div id="step1-add-info">
            <nobr>文字入力や各ボタンの選択が認識されるまで、</nobr>
            <br/>
            多少時間がかかります。ひとつずつ、ゆっくりめにご入力ください。
        </div>
    </div>
</form>
<script>
    var pageName = "selfCheckIn";

    $(document).ready(function () {

            $('#english-container button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                document.waiverForm.language.value = "en";
                submitForm();
            });

            $('#japanese-container button').bind('click touchend MSPointerUp pointerup', function (e) {
                e.preventDefault();
                document.waiverForm.language.value = "ja";
                submitForm();
            });
        }
    )
</script>

<?php require_once('selfCheckInFooter.php'); ?>
