<?php
$pageName = "photosInfo";
require_once('selfCheckInHeader.php') ?>
<style>
    #del {
        float: left;
        margin-left: 0;
        text-align: right;
        width: 542px;
    }

    #yes button {
        background-color: #0f0;
        border: 2px solid white;
        color: black;
        font-family: "pussycat_snickers";
        font-size: 90px;
        height: 90px;
        line-height: 100px;
        text-align: center;
        width: 465px;
        clear: none;
    }

    #no button {
        background-color: red;
        border: 2px solid white;
        color: white;
        font-family: "pussycat_snickers";
        font-size: 90px;
        height: 90px;
        line-height: 100px;
        text-align: center;
        width: 465px;
        clear: none;
    }

    .photo-options{
        display: flex;
        justify-content: space-between;
        width: 900px;
        margin: 0 auto;
    }

    .option{
        background-color: #111111;
        width: 290px;
        margin: 0px 20px 0 2px;
        height: 720px;
    }
    .option-button-no{
        height: 170px;
        width: 100%;
        background-color: red;
        border: 2px solid white;
        color: white;
        font-family: "pussycat_snickers";
        font-size: 90px;
        line-height: 100px;
        text-align: center;
        clear: none;
    }

    .option-button-yes{
        height: 170px;
        width: 100%;
        background-color: #00ff00;
        border: 2px solid white;
        color: white;
        font-family: "pussycat_snickers";
        font-size: 100px;
        line-height: 100px;
        text-align: center;
        clear: none;
    }


    .video-preview{
        border: 12px solid white;
        border-radius: 5px;
        transform: rotate(12deg);
        margin-top: -50px;
        -webkit-box-shadow: 10px 10px 28px 2px rgba(0,0,0,0.75);
        -moz-box-shadow: 10px 10px 28px 2px rgba(0,0,0,0.75);
        box-shadow: 10px 10px 28px 2px rgba(0,0,0,0.75);
    }
    #photos-image{
        margin-top: 30px;
        margin-bottom: 28px;
    }
    #sad-guy{
        margin-top: 30px;
        margin-bottom: 72px;
    }

    .yen{font-family: sans-serif}
</style>


<form action="" name="waiverForm" method="post">
    <input type="hidden" name="photosInfo" value="<?php echo $pageName ?>">
    <input type="hidden" name="includePhotos" value="">

    <div id="step3">
        <div class="header-container">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en">Would you like to buy photos?</span>
                    <span class="lang_ja">お写真又はビデオをご購入された方は左手首の番号をご入力ください。</span>
                </h1>

            </div>
        </div>

        <div class="photo-options">
            <div class="option">
                <img src="img/sad_guy.png" alt="" id="sad-guy">
                <button class="option-button-no">なし</button>
            </div>
            <div class="option">
                <img src="img/gopro_photos.png" id="photos-image">
                <button class="option-button-yes"><span class="yen">&yen;</span>3000</button>
            </div>
            <div class="option">
                <img src="img/gopro_hand_text.png">
                <div></div>
                <video class="video-preview" width="275" height="165" autoplay loop>
                    <source src="videos/gopro.mp4" type="video/mp4">
                </video>
                <button class="option-button-yes"><span class="yen">&yen;</span>5000</button>
            </div>
        </div>

        <div class="back">
            <button>
                <span class="lang_en">&lt;&lt;BACK</span>
                <span class="lang_ja">&lt;&lt;戻る</span>
            </button>
        </div>
    </div>
</form>

<script>
    var pageName = "buyPhotos";

    $(document).ready(function () {
        $('#step3 #yes button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            document.waiverForm.includePhotos.value = 1;//true and false are getting sent as strings
            submitForm();
        });

        $('#step3 #no button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            document.waiverForm.includePhotos.value = 0;
            submitForm();
        });
    });
</script>
<?php require_once('selfCheckInFooter.php'); ?>
