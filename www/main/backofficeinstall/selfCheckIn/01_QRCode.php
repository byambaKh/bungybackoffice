<?php
$pageName = "QRCode";
require_once('selfCheckInHeader.php')
?>
<script>
    var machineName = "<?=$host?>";
    var bookingId = <?=json_encode($_SESSION['data']['bookingId'])?>;

    var bookings = [
        {"time":"09:00 AM", "name":"NAKAMURA", "id":"50404"},
        {"time":"09:30 AM", "name":"NAKAMURA", "id":"50405"}
    ];

    function renderBookings(bookings){
        //truncate the table
        var output = '';
        bookings.forEach(function(booking) {
            //add booking to the table
            output = output + "<li>" + booking.time + " " + booking.name + "</li>";
        });

        return output;
    }
    console.log(renderBookings(bookings));

</script>
<script src="js/bookingFunctions.js"></script>
<script src="js/groupOrComboBooking.js"></script>
<style>
    #del {
        float: left;
        margin-left: 0;
        text-align: right;
        width: 542px;
    }

    #yes button {
        background-color: #0f0;
        border: 2px solid white;
        color: black;
        font-family: "pussycat_snickers";
        font-size: 90px;
        height: 90px;
        line-height: 100px;
        text-align: center;
        width: 465px;
        clear: none;
    }

    #no button {
        background-color: red;
        border: 2px solid white;
        color: white;
        font-family: "pussycat_snickers";
        font-size: 90px;
        height: 90px;
        line-height: 100px;
        text-align: center;
        width: 465px;
        clear: none;
    }

    .header-container{
        margin-bottom: 0px;
        margin-top: 10px;
    }

    #no{
        display: inline;
        width:140px;
    }

    #no button{
        width:200px;
    }

    #yes{
        display: inline;
        width:140px
    }

    #yes button{
        width:200px;
        bottom: 0;
        left: 0;
        position: absolute !important;
        display: none;
    }

    #enter{
        bottom: 0;
        left: 0;
        position: absolute !important;
        margin: 0;
        width: 180px;
    }

    #enter button{
        background-color: red;
        color: white;
        border-color: white;
        width: 180px;
    }

    .payment-in-progress, .payment-complete{
        display:none;
    }

    #step17{
        position: relative;
        border: 1px red solid;
        height: 600px;
    }

    .hidden{
        display: none;
    }

    #qr-scan-image{
        position: absolute;
        top: 130px;
        left: 270px;
    }

    #qr-arrow{
        position: absolute;
        right: 30px;
        bottom: 10px;
    }
</style>
<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?= $pageName ?>">


    <div id="step17">

        <div id="scan-qr" class="header-container hidden">
            <div class="header-container">
            <h1>
                <span class="lang_en">Scan Booking QR Code</span>
                <span class="lang_ja">XXXXXXXXXX</span>
            </h1>
            </div>
            <img src="img/qr-scan-image@2x.png" id="qr-scan-image">
            <img src="img/qr-arrow@2x.png" id="qr-arrow">
        </div>

        <div id="results" class="">
            <ul></ul>
            Show returned bookings.
        </div>

        <div id="no-results" class="hidden">
            Booking Not Found.
        </div>
        <div class="pay-here-question screen-message hidden">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en">Please scan your QR Code Here</span>
					<span class="lang_ja">お支払いはグループの合計金額を投入してください。</span>
                </h1>
            </div>
            <div id="yes">
                <button>
                    <span class="lang_en">YES</span>
                    <span class="lang_ja">はい</span>
                </button>
            </div>

            <div id="no">
                <button style="width:300px">
                    <span class="lang_en">NO</span>
                    <span class="lang_ja" >いいえ</span>
                </button>
            </div>

            <img src="img/get_ticket.png" >

        </div>

        <div id="enter">
            <button type="button" onclick="document.location.href='01_bookingName.php';">
                <span style="width: 100%;" class="lang_en" style="font-size: 70px;">&lt;&lt;back</span>
                <span style="width: 100%;" class="lang_ja" style="font-size: 70px;">&lt;&lt;戻る</span>
            </button>
        </div>

    </div>
</form>
<script>init();
    var pageName = "QRCode";
    $(document).ready(function() {

    });

    function onCompletion() {
        console.log("performing completion");
        submitForm();
    }
</script>
<script src="/js/reception.js" type="text/javascript"></script>
<?php


function getBookingIdForTimeAndTelephone($time, $telephone, $date) {
    $query = "SELECT * FROM CustomerRegs1 WHERE BookingDate = '$date' 
		AND BookingTime = '$time' 
		ContactNo = '$telephone'";

	$results = queryForRows($query);

	if(count($results) == 0) {
        return [];
    }

    else
        return $results; //return all of the matching bookings and display them
}

?>
<?php require_once('selfCheckInFooter.php'); ?>

