<?php
$pageName = "jumpType";
require_once('selfCheckInHeader.php');
require_once('jcm/config.php');
//echo "$host";
$firstJumpRate = getFirstJumpRate(CURRENT_SITE_ID, date("Y-m-d"));
$regular = $firstJumpRate;
$second = $config['second_jump_rate'];
$repeater = $config['repeater_rate'];
$otherSiteDiscount = $firstJumpRate - 1000;
?>
<style>
    .numpad-button {
        display: inline;
    }

    .numpad-button button {
        color: white;
        font-size: 70px;
        font-weight: bold;
        border: 2px solid;
        border-color: #fff #eee #fff #eee;
        padding-left: 20px;
        padding-right: 20px;
        height: 100px;
        width: 100px;
        display: inline;
    }

    #numpad-container {
        width: 320px;
    }

    #cancel {
        float: left;
        margin-left: 0px;
        width: 500px;
        text-align: right;
    }

    #cancel button {
        font-family: "pussycat_snickers";
        font-size: 90px;
        line-height: 100px;
        text-align: center;
        color: #FFF;
        background-color: #F00;
        width: 300px;
        height: 90px;
        border: 2px solid #FFF;
    }

    #numpad-display input {
        width: 300px;
        height: 100px;
        font-size: 90px;
    }


    #enter-del-container #enter {
        margin: 0;
        display: block;
        width: 100%;
    }

    #enter-del-container #del {
        margin: 0;
        display: block;
        width: 100%;
    }

    #enter-del-container #cancel {
        margin: 0;
        display: block;
        width: 100%;
    }

    #numpad-container {
        width: 320px;
        float: left;
        display: none;

    }

    #jump-types{
        float: left;
        margin-left: auto;
        margin-right: auto;
        clear: none;
        width: 700px;
        display: inline;
    }

	 .jumpOption button {
		//* font-family: "pussycat_snickers"; */
		/* font-size: 90px; */
		/* line-height: 100px; */
		text-align: center;
		color: #000;
		background-color: #F00;
		width: 700px;
		/* height: 200px; */
		border: 2px solid #FFF;
	}

    .jumpOption{
        width: 100%;
    }
	/*
    .jumpOption button{
        height: 60px;
        width: 700px;
    }
	*/

    .jumpOption button .lang_en {
        font-family: "Arial", "Sans-Serif", "Helvetica";
        font-size: 20px;
    }
</style>
<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">
    <input type="hidden" name="jumpType" value="x">

    <div id="step00">
        <div class="header-container">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en">Jump Rate</span>
                    <span class="lang_ja">ジャンプ料金</span>
                </h1>
                <!--<h2>
                  <span class="lang_en">Please select the name in which your booking was made and then press [Continue].</span>
                  <span class="lang_ja">をタッチして、「次へ」お押ししてください。</span>
                </h2>-->
            </div>
        </div>
        <div id="jump-types">
            <div id="regular" class="jumpOption">
                <button>
                    <span class="lang_en" style="font-size: 60px;"> Regular Jump (&yen;<?=$regular?>) </span>
                    <span class="lang_ja" style="font-size: 60px;">一般料金 (&yen;<?=$regular?>)<br></span>
                </button>
            </div>
            <br>

            <div id="second" class="jumpOption">
                <button>
                    <span class="lang_en" style="font-size: 60px;">Second Jump (&yen;<?=$second?>) </span>
                    <span class="lang_ja" style="font-size: 60px;">同日2回目 (&yen;<?=$second?>)<br></span>
                </button>
            </div>
            <br>


            <div id="repeater" class="jumpOption">
                <button >
                    <span class="lang_en" style="font-size: 60px;">Repeater (&yen;<?=$repeater?>)<br> (Certificate Required) </span>
                    <span class="lang_ja" style="font-size: 60px;">リピーター (&yen;<?=$repeater?>)<br>(バンジーの認定証必)</span>
                </button>
            </div>
            <br>

            <div id="otherSiteDiscount" class="jumpOption">
                <button>
                    <span class="lang_en" style="font-size: 60px;"> Other Site (&yen;<?=$otherSiteDiscount?>) (Certificate Required) </span>
                    <span class="lang_ja" style="font-size: 60px;">サイト割引 (&yen;<?=$otherSiteDiscount?>)<br>(他のサイトの認定証必)</span>
                </button>
            </div>
        </div>
        <br>
        <!--one time pin auth keypad-->
        <div id="numpad-container">
            <div id="numpad-display"><input name="pin"></div>
            <div class="photo-numpad">
                <div class="numpad-button">
                    <button>1</button>
                </div>
                <div class="numpad-button">
                    <button>2</button>
                </div>
                <div class="numpad-button">
                    <button>3</button>
                </div>
                <div class="numpad-button">
                    <button>4</button>
                </div>
                <div class="numpad-button">
                    <button>5</button>
                </div>
                <div class="numpad-button">
                    <button>6</button>
                </div>
                <div class="numpad-button">
                    <button>7</button>
                </div>
                <div class="numpad-button">
                    <button>8</button>
                </div>
                <div class="numpad-button">
                    <button>9</button>
                </div>
                <div class="numpad-button">
                    <button>0</button>
                </div>
            </div>
            <div id="enter-del-container">
                <div id="enter">
                    <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
                </div>
                <div id="del">
                    <button><span class="lang_en">DELETE</span><span class="lang_ja">訂正</span></button>
                </div>
                <div id="cancel">
                    <button><span class="lang_en">CANCEL</span><span class="lang_ja">CANCEL</span></button>
                </div>
            </div>
        </div>
        <!-- end one time pin auth keypad-->


        <div class="back">
            <button>
                <span class="lang_en">&lt;&lt;BACK</span>
                <span class="lang_ja">&lt;&lt;戻る</span>
            </button>
        </div>
    </div>
</form>
<script>
    var pageName = "bookOrWalk";

    $(document).ready(function () {

        $('.numpad-button button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            if ($('#numpad-display input').val().length == 4) return;
            $('#numpad-display input').val('' + $('#numpad-display input').val() + $(this).html());
        });

        $('#del button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            var val = $('#numpad-display input').val();
            if (val.length > 0) {
                $('#numpad-display input').val(val.substr(0, val.length - 1));
            }
        });

        $('#cancel button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            $('#numpad-container').hide();
            //hide the numerical input
            //set the value to zero
        });

        $('#enter button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            var pin = $('#numpad-display input').val();
            //console.log('enter button pressed');
            return $.ajax(
                'ajax.php?action=use_one_time_pin&pin=' + pin,
                {
                    dataType: 'json',
                    type: 'POST',
                    async: false,//try async true
                    cache: false,
                    success: function (data) {
                        if (data['success'] == true) {
                            submitForm();
                        } else {
                            //show error message
                        }
                        //submit the form and go to the next page
                    },
                    error: function () {

                    }
                }
            );
            //perform an ajax request using use_one_time_pin
        });

        $('#step00 #repeater button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            document.waiverForm.jumpType.value = "repeater";
            $('#numpad-container').show();
            //show authentication window
        });

        $('#step00 #second button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            document.waiverForm.jumpType.value = "second";
            $('#numpad-container').show();
            //show authentication window
        });
        //book or walk page walkInBooking Button
        $('#step00 #regular button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            document.waiverForm.jumpType.value = "regular";
            submitForm();
        });

        $('#step00 #otherSiteDiscount button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            $('#numpad-container').show();
            document.waiverForm.jumpType.value = "otherSiteDiscount";
            //show authentication window
        });
    });
</script>

<?php require_once('selfCheckInFooter.php'); ?>
