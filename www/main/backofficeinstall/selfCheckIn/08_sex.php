<?php
$pageName = "sex";
require_once('selfCheckInHeader.php')
?>
<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">
    <input type="hidden" name="sex" value="<?php echo $pageName ?>">

    <div id="step7">
        <div class="header-container">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en">please select your sex</span>
                    <span class="lang_ja">性別をお選びください。</span>
                </h1>
            </div>
        </div>
        <div id="male-female-container">
            <div id="male-button">
                <button>
                    <span class="lang_en">MALE</span>
                    <span class="lang_ja">男性</span>
                </button>
            </div>
            <div id="female-button">
                <button>
                    <span class="lang_en">FEMALE</span>
                    <span class="lang_ja">女性</span>
                </button>
            </div>
        </div>
        <div class="back">
            <button>
                <span class="lang_en">&lt;&lt;BACK</span>
                <span class="lang_ja">&lt;&lt;戻る</span>
            </button>
        </div>
    </div>
</form>
<script>
    var pageName = "sex";

    $(document).ready(function () {
        $('#step7 #male-button button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            document.waiverForm.sex.value = "male";
            submitForm();
            //saveWaiverDataToSessionAndGoToNextStep({sex: "male"});
        });
        $('#step7 #female-button button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            document.waiverForm.sex.value = "female";
            submitForm();
            //saveWaiverDataToSessionAndGoToNextStep({sex: "female"});
        });
    });
</script>
<?php require_once('selfCheckInFooter.php'); ?>
