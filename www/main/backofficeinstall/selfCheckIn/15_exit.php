<?php
$pageName = "exit";
require_once('selfCheckInHeader.php');

//load the printer names
$loadedResults = mysql_query('SELECT * FROM settings WHERE site_id = ' . CURRENT_SITE_ID . ' AND key_name = "printerTicketAndCertificate"');
$settingsRow = mysql_fetch_assoc($loadedResults);
$savedPrintersArray = explode("\n", $settingsRow['value']);

$certificatePrinter = $ticketPrinter = '';//set them to an empty string if there are not results

$findPrintersResultsCount = mysql_num_rows($loadedResults);

if ($findPrintersResultsCount) {
    $ticketPrinter = trim($savedPrintersArray[0]);
    $certificatePrinter = trim($savedPrintersArray[1]);
}

$date = urlencode(date("Y-m-d"));
$signatureUrl = urlencode('');

$bookingType = urlencode($_SESSION['checkIn']['bookingType']);
$jumpNumber = urlencode($_SESSION['checkIn']['jump_number']);//need to get from database
//$photoNumber = urlencode($_SESSION['checkIn']['photo_number']);//need to get from database
$cordColor = getCordColorForWeightKg(str_replace("kg", "", $_SESSION['checkIn']['weight']), date("Y-m-d"), CURRENT_SITE_ID);
$groupOfNumber = urlencode($_SESSION['checkIn']['groupOfNumber']);//need to get from database
$firstName = urlencode($_SESSION['checkIn']['firstname']);
$lastName = urlencode($_SESSION['checkIn']['lastname']);
$fullName = urlencode($firstName . " " . $lastName);
$time = urlencode($_SESSION['checkIn']['BookingTime']);
$paid = $_SESSION['totalPayment'];
$certificatePrinter = urlencode($certificatePrinter);
$ticketPrinter = urlencode($ticketPrinter);
$host = getRemoteHostName();

$yearJumpNumber = getYearJumpNumber(date("Y"), CURRENT_SITE_ID);

//TODO temp placeholder variables that I must get
$weight = urlencode($_SESSION['checkIn']['weight']);
$jumpCount = urlencode(getDayJumpNumber(date("Y-m-d"), CURRENT_SITE_ID));
$jumpCode = urlencode(getJumpCode($_SESSION['checkIn'], date("Y-m-d"), CURRENT_SITE_ID));

if($_SESSION['checkIn']['includePhotos']) {
    $photoNumber = getDayPhotoNumber(date("Y-m-d"), CURRENT_SITE_ID);
} else {
    $photoNumber = " ";
}

$certificateFrameUrl  = "../selfCheckIn/certificate.php?fullName=$fullName&date=$date&printer=$ticketPrinter&jumpNumber=$jumpCode";
$ticketFrameUrl= "../selfCheckIn/ticketLayout.php?time=$time&weight=$weight&bookingType=$bookingType&photoNumber=$photoNumber&jumpCount=$jumpCount&groupOfNumber=$groupOfNumber&firstName=$firstName&lastName=$lastName&cordColor=$cordColor&printer=$certificatePrinter&host=$host&paid=$paid";

?>

<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">

    <div id="step15">
        <div class="header-container">
            <div class="head-image-container">
                <h2>
                    <span class="lang_en">Almost done... Please take the jump ticket to the reception counter.</span>
                    <span class="lang_ja">あともう少しです….ジャンプ券を<br/>受付までお持ちください。</span>
                </h2>
            </div>
        </div>
        <div class="restart-container">
            <div id="step15progress"></div>
            <iframe style="display:none" class="hiddenCertificateFrame" name="certificate" src="<?php echo $certificateFrameUrl ?>"></iframe>
            <iframe style="display:none" name="ticketmini" id="ticketmini" src="<?php echo $ticketFrameUrl ?>"></iframe>
        </div>
    </div>
</form>
<img src="img/get_ticket_eng.png" class="lang_en">
<img src="img/get_ticket_jpn.png" class="lang_ja">
<script>
    var pageName = "exit";
    setTimeout(function () { submitForm("action=finishWaiver"); }, 12000);
</script>
<script src="/js/reception.js" type="text/javascript"></script>

<?php require_once('selfCheckInFooter.php'); ?>
