<?php
$pageName = "photosInfo";
require_once('selfCheckInHeader.php') ?>
<style>
    #del {
        float: left;
        margin-left: 0;
        text-align: right;
        width: 542px;
    }

    #yes button {
        background-color: #0f0;
        border: 2px solid white;
        color: black;
        font-family: "pussycat_snickers";
        font-size: 90px;
        height: 90px;
        line-height: 100px;
        text-align: center;
        width: 465px;
        clear: none;
    }

    #no button {
        background-color: red;
        border: 2px solid white;
        color: white;
        font-family: "pussycat_snickers";
        font-size: 90px;
        height: 90px;
        line-height: 100px;
        text-align: center;
        width: 465px;
        clear: none;
    }
</style>


<form action="" name="waiverForm" method="post">
    <input type="hidden" name="photosInfo" value="<?php echo $pageName ?>">
    <input type="hidden" name="includePhotos" value="">

    <div id="step3">
        <div class="header-container">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en">Would you like to buy photos?</span>
                    <span class="lang_ja">お写真又はビデオをご購入された方は左手首の番号をご入力ください。</span>
                </h1>

            </div>
        </div>
        <div id="yes">
            <button><span class="lang_en">YES</span><span class="lang_ja">次へ</span> </button>
        </div>

        <br>
        <br>

        <div id="no">
            <button><span class="lang_en">NO</span><span class="lang_ja">訂正</span> </button>
        </div>
        <br>

        <div class="back">
            <button>
                <span class="lang_en">&lt;&lt;BACK</span>
                <span class="lang_ja">&lt;&lt;戻る</span>
            </button>
        </div>
    </div>
</form>

<script>
    var pageName = "buyPhotos";

    $(document).ready(function () {
        $('#step3 #yes button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            document.waiverForm.includePhotos.value = 1;//true and false are getting sent as strings
            submitForm();
        });

        $('#step3 #no button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            document.waiverForm.includePhotos.value = 0;
            submitForm();
        });
    });
</script>
<?php require_once('selfCheckInFooter.php'); ?>
