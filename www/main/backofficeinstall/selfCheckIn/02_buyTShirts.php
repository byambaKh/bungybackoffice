<?php
$pageName = "buyTShirts";
require_once('selfCheckInHeader.php');

$tShirtPrice = 2000;
$photoPath = 'img/buy_tshirts.jpg';
?>
<style>
    #del {
        float: left;
        margin-left: 0;
        text-align: right;
        width: 542px;
    }

    #yes button {
        background-color: #0f0;
        border: 2px solid white;
        color: black;
        font-family: "pussycat_snickers";
        font-size: 90px;
        height: 90px;
        line-height: 100px;
        text-align: center;
        width: 465px;
        clear: none;
    }

    #no button {
        background-color: red;
        border: 2px solid white;
        color: white;
        font-family: "pussycat_snickers";
        font-size: 90px;
        height: 90px;
        line-height: 100px;
        text-align: center;
        width: 465px;
        clear: none;
    }

    .header-container{
        margin-bottom: 0px;
        margin-top: 10px;
    }

    #no{
        display: inline;
        width:140px;
    }

    #no button{
        width:200px;
    }

    #yes{
        display: inline;
        width:140px
    }

    #yes button{
        width:200px;
    }
</style>


<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">
    <input type="hidden" name="includeTShirt" value="">
    <input type="hidden" name="includeTShirtPrice" value="<?php echo $tShirtPrice?>">

    <div id="step3">
        <div class="header-container">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en" style='font-family: "Arial", "Sans-Serif", "Helvetica"; font-size:50px;'>Would you like to buy a Bungy t-shirt? (&yen;<?php echo $tShirtPrice?>)</span>
                    <span class="lang_ja" style='font-family: "Arial", "Sans-Serif", "Helvetica"'>バンジーTシャツをご注文しますか？ (&yen;<?php echo $tShirtPrice?>)</span>
                </h1>
            <img src='<?php echo $photoPath?>'>
            </div>
        </div>

        <div id="no">
            <button style="width:300px">
                <span class="lang_en">NO</span>
                <span class="lang_ja" >いいえ</span>
            </button>
        </div>

        <div id="yes">
            <button>
                <span class="lang_en">YES</span>
                <span class="lang_ja">はい</span>
            </button>
        </div>

        <div class="back">
            <button>
                <span class="lang_en">&lt;&lt;BACK</span>
                <span class="lang_ja">&lt;&lt;戻る</span>
            </button>
        </div>
    </div>
</form>

<script>
    var pageName = "buyTShirts";

    $(document).ready(function () {
        $('#step3 #yes button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            document.waiverForm.includeTShirt.value = 1;//true and false are getting sent as strings
            submitForm();
        });

        $('#step3 #no button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            document.waiverForm.includeTShirt.value = 0;
            submitForm();
        });
    });
</script>
<?php require_once('selfCheckInFooter.php'); ?>

