<?php
$pageName = "weight";
require_once('settings.php'); //Get the printer settings
require_once('jcm/config.php');//Get JCM settings
$hostSettings = loadHostSettings($host)[0];//load the settings for a particular host
$enableScales = is_array($hostSettings['settings']) && ($hostSettings['settings']['scales'] == 1);
$action = isset($_GET['action']) ? $_GET['action'] : '';

$scalesWeightOffsetKg = 0;
if(isset($hostSettings['settings']['scalesWeightOffsetKg']) && is_numeric($hostSettings['settings']['scalesWeightOffsetKg'])) {
    $scalesWeightOffsetKg = $hostSettings['settings']['scalesWeightOffsetKg'];
}

//error_reporting(E_ALL);
//ini_set('display_errors', 'On');

require_once('selfCheckInHeader.php');

?>
<script>
    var machineName = "<?=$host?>";
    var scalesWeightOffsetKg = <?=$scalesWeightOffsetKg?>;
    function onConnectFunction(){
        //do nothing on connecting
        //this is used in the transaction.php
    }
</script>
<script src="js/poll_client.js"></script>
<script>

    function onCompletion() {
        console.log("performing completion");
        submitForm();
    }
</script>
<style>
    .readable_text {
        font-family: "Helvetica";
        color: white;
        text-align: left;
        font-size: 40px;
    }

    .centeredCleared {
        clear: both;
        margin-left: auto;
        margin-right: auto;
    }

    #first-weigh {
        width: 800px;
    }

    #refresh_weight {
        font-family: "pussycat_snickers";
        font-size: 90px;
        line-height: 100px;
        text-align: center;
        color: #000;
        background-color: #F00;
        width: 700px;
        height: 200px;
        border: 2px solid #FFF;
    }

    #get-ready {
        width: 700px;
        margin-left: auto;
        margin-right: auto;

    }

    .are-you-ready{
        font-size: 90px;
        text-align: center;

    }
    #step-on-the-scale{
        font-family: "pussycat_snickers";
        color: #F00;
        line-height: 1em;
        font-weight: normal;
        text-transform: uppercase;
        font-size: 80px;
    }

    .header-container .lang_ja{
        font-size: 70px;
    }
    #step-on-the-scale .lang_ja{
        font-size: 60px;

    }

    .are-you-ready .lang_ja{
        font-size: 65px;
    }
    #loading-spinner{
        display: none;
    }

    .hidden{
        display: none;
    }

    #weightMessageString{
        font-size: 16px;
        color: #110;
    }
</style>

<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">
    <input type="hidden" name="weight" value="">
    <input type="hidden" name="weightMeasured" value="">

    <div id="step00">
        <div class="header-container">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en">Do Not Close</span>
                    <span class="lang_ja">Do Not Close</span>
                </h1>
            </div>
        </div>
    </div>

    <a href="13_2_weight.php" style="font-size: 20px; color: #444;">*</a>
</form>

<div class="back">
    <button>
        <span class="lang_en">&lt;&lt;BACK</span>
        <span class="lang_ja">&lt;&lt;戻る</span>
    </button>
</div>

<script>
    var pageName = "weight";
    $(document).ready(function () {
        init();//start the websocket client
        //when websocket status is 1 enable button pushing

        $("#refresh_weight").bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();

            requestWeight();//request the weight from the server
        });
    });

</script>
<?php require_once('selfCheckInFooter.php'); ?>
