<?php
include ('../includes/application_top.php');
include ('../Reception/checkin_config.php');

/**
 * @return bool|string
 */
function getDate_()
{
    $cDate = date("Y-m-d");
    if (isset($_GET['date'])) {
        $cDate = $_GET['date'];

        return $cDate;
    }

    return $cDate;
}

/**
 * @param $machineName
 * @param $date
 *
 * @return array
 */
function cashOut($machineName, $date)
{
    //get machine balance in notes

    $machineBalance  = getMachineBalance($machineName, $date); //[10000 => 5, 5000 => 8, 1000 => 20];

    //get machine template
    $machineTemplate = getMachineTemplate($machineName);
    //subtract

    $cashOut = subtractCashOutArrays($machineTemplate, $machineBalance);
    //OR we could just get what has come in and gone out of the machine

    return [$cashOut, $machineBalance];
}

/**
 * @param $array1
 * @param $array2
 *
 * @return array
 */
function subtractCashOutArrays($array1, $array2)
{
    $result = [];
    foreach ($array1 as $key => $noteCount) {
        $result[$key] = $array1[$key] - $array2[$key];
    }

    return $result;
}

/**
 * @param $cashOutArray
 * @param $machineName
 *
 * @return string
 */
function showCashOut($cashOutArray, $machineBalance, $machineName)
{
    $showTitle = true;

    $output = "<table class='cash-out'>\n";
    $output .= "<tr><th class='machine'>$machineName</th></tr>\n";
    $output .= "<tr><td>\n";

    $output .= "<table class='take-put-table'>\n";
    $output .= "<tr><th colspan='4' class='take-put'>BALANCE</th></tr>\n";
    foreach($machineBalance as $denomination => $noteCount)
    {
        $noteCount = $noteCount * 1;//make it positive;
        $output .= "<tr><td class='note'>&yen;$denomination</td><td>x</td><td>$noteCount</td><td>[&nbsp;&nbsp;&nbsp;]</td></tr>\n";
    }
    $output .= "</table>\n";

    $output .= "<table class='take-put-table'>\n";
    $output .= "<tr><th colspan='4' class='take-put'>TAKE OUT</th></tr>\n";
    foreach($cashOutArray as $denomination => $noteCount)
    {
        if($noteCount >= 0) continue;


        $noteCount = $noteCount * -1;//make it positive;
        $output .= "<tr><td class='note'>&yen;$denomination</td><td>x</td><td>$noteCount</td><td>[&nbsp;&nbsp;&nbsp;]</td></tr>\n";
    }
    $output .= "</table>\n";
    $output .= "<!--End Table-->\n";

    $output .= "<table class='take-put-table'>\n";
    $output .= "<tr><th colspan='4' class='take-put'>PUT IN</th></tr>\n";
    foreach($cashOutArray as $denomination => $noteCount)
    {
        if($noteCount <= 0) continue;

        $output .= "<tr><td class='note'>&yen;$denomination</td><td>x</td><td>$noteCount</td><td>[&nbsp;&nbsp;&nbsp;]</td></tr>\n";
    }
    $output .= "</table>\n";

    $output .= "</td></tr>\n";
    $output .= "</table>\n";

    return $output;
}

/**
 * @param $machineName
 *
 * @return array
 */
function getMachineTemplate($machineName) {
    //For now just return an array, we can store templates in the database later
    return [1000 => 50, 5000 => 10,10000 => 0] ;
}

/**
 * @param $machineName
 * @param $date
 *
 * @return array
 */
function getMachineBalance($machineName, $date)
{
    $query = "
        SELECT 
            SUM(money_in) AS money_in,
            SUM(money_out) AS money_out,
            SUM(money_in) - SUM(money_out) AS balance,
            CAST(`denomination` AS UNSIGNED) AS denomination
            FROM 
            (
                (
                    SELECT 
                        COUNT(IF(`type`='ACCEPT_CASH', `data`, NULL)) AS `money_in`,
                        COUNT(IF(`type`='PAYOUT', `data`, NULL)) AS `money_out`,
                        `data` AS denomination
                    FROM jcm_event je
                        LEFT JOIN jcm_host jh ON (jh.id = je.host)
                        
                    WHERE 
                        jh.hostname = '$machineName' AND 
                        DATE(created) = '$date' 
                        AND `type` IN ('ACCEPT_CASH', 'PAYOUT')
                    GROUP BY `data`
                    ORDER BY CAST(`data` AS UNSIGNED) ASC
                )		
                UNION ALL
                
                (
                    SELECT 
                        IF(box1, box1, 0) AS money_in,
                        0 AS money_out,
                        IF(box1, 1000, 0) AS denomination
                    FROM jcm_host WHERE hostname = '$machineName' 
                )
                
                UNION ALL
                
                (
                    SELECT 
                        IF(box2, box2, 0) AS money_in, 
                        0 AS money_out,
                        IF(box2, 5000, 0) AS denomination
                    FROM jcm_host WHERE hostname = '$machineName'
                )
            ) AS t
        GROUP BY denomination
        ORDER BY denomination;
    ";

    $results = queryForRows($query);

    $output = [];
    foreach($results as $key => $result)
    {
        $denomination = (int)$result['denomination'];
        $balance = (int)$result['balance'];

        $output[$denomination] = $balance;
    }

    //there might not be a 10000 yen in the list of transactions
    if(!array_key_exists(10000, $output)){
        $output[10000] = 0;
    }

    return $output;
}

//get each machine's cash out value
/**
 * @param $siteId
 *
 * @return array
 */
function getMachines($siteId = CURRENT_SITE_ID)
{
    $query = "SELECT * FROM jcm_host WHERE site = $siteId";
    $machines = queryForRows($query);

    return $machines;

}
$date = getDate_();

$machines = getMachines();
$output = '';

foreach($machines as $machine)
{
    list($cashOut, $machineBalance) = cashOut($machine['hostname'], $date);
    $output .= showCashOut($cashOut, $machineBalance, $machine['hostname']);
}

?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo SYSTEM_SUBDOMAIN; ?> - Daily Sales Report</title>
	<?php include "../includes/head_scripts.php"; ?>
<style>
@media print { .not-printed, .not-printed * { display: none !important; } }
    .cash-out{
        width: 250px;
        font-family: Arial, sans-serif;
        margin-bottom: 10px;
        margin-left: 4px;
        border-collapse:collapse;
        display: block;
    }

    .buttons{
        width: 250px;
        display: block;
    }

    .take-put-table{
        width: 250px;
        margin: 0;
        padding: 0;
    }

    .note{
        width: 170px;
    }

    .machine{
        background-color: black;
        color: white;
        font-size: 20px;
    }

    table{
        border: 1px solid black;
    }
    td, th, tr{
        padding: 0;
    }
</style>
<script>

function printPage(ticketPrinter){
    jsPrintSetup.setPrinter(ticketPrinter);
    jsPrintSetup.setOption('marginTop', 1);
    jsPrintSetup.setOption('marginBottom', 1);
    jsPrintSetup.setOption('marginLeft', 1);
    jsPrintSetup.setOption('marginRight', 1);

    jsPrintSetup.setOption('headerStrLeft', '');
    jsPrintSetup.setOption('headerStrCenter', '');
    jsPrintSetup.setOption('headerStrRight', '');
    jsPrintSetup.setOption('footerStrLeft', '');
    jsPrintSetup.setOption('footerStrCenter', '');
    jsPrintSetup.setOption('footerStrRight', '');

    jsPrintSetup.setSilentPrint(true);
    jsPrintSetup.print();
    jsPrintSetup.setSilentPrint(false);
}

</script>
</head>
<body>
<?= $output ?>

<div class="not-printed buttons">
    <button type="button" onClick="document.location = 'selfCheckIn.php';" id="back">Back</button>
    <button type="button" onClick="printPage('Ticket')" style="float: right;">Print</button>
</div>
</body>
</html>
