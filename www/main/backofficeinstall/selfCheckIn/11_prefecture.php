<?php
$pageName = "prefecture";
require_once('selfCheckInHeader.php')
?>
<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">
    <input type="hidden" name="prefecture" value="">

    <div id="step10">
        <div class="header-container">
            <div class="head-image-container"><h1>
                    <span class="lang_en">Where do you live?</span>
                    <span class="lang_ja">お住まいの都道府県を選択してください。</span></h1></div>
        </div>
        <div id="place-container">
            <?php
            $perfs = array(
                array(
                    "北海道・Hokkaido",
                    "",
                    array(
                        "type" => 'title',
                        "text" => "東北・Tohoku",
                    ),
                    "青森県・Aomori",
                    "岩手県・Iwate",
                    "宮城県・Miyagi",
                    "秋田県・Akita",
                    "山形県・Yamagata",
                    "福島県・Fukushima"
                ),
                array(
                    array(
                        "type" => 'title',
                        "text" => "関東・Kanto",
                    ),
                    "茨城県・Ibaraki",
                    "栃木県・Tochigi",
                    "群馬県・Gunma",
                    "埼玉県・Saitama",
                    "千葉県・Chiba",
                    "東京都・Tōkyō",
                    "神奈川県・Kanagawa",
                    "",
                    "",
                    "",
                    array(
                        "cols" => 3,
                        "text" => "外国・I am a visitor from overseas",
                    )
                ),
                array(
                    array(
                        "type" => 'title',
                        "text" => "中部・Chūbu",
                    ),
                    "新潟県・Niigata",
                    "富山県・Toyama",
                    "石川県・Ishikawa",
                    "福井県・Fukui",
                    "山梨県・Yamanashi",
                    "長野県・Nagano",
                    "岐阜県・Gifu",
                    "静岡県・Shizuoka",
                    "愛知県・Aichi"
                ),
                array(
                    array(
                        "type" => 'title',
                        "text" => "関西・Kansai",
                    ),
                    "三重県・Mie",
                    "滋賀県・Shiga",
                    "京都府・Kyōto",
                    "大阪府・Ōsaka",
                    "兵庫県・Hyōgo",
                    "奈良県・Nara",
                    "和歌山県・Wakayama"
                ),
                array(
                    array(
                        "type" => 'title',
                        "text" => "中国・Chūgoku",
                    ),
                    "鳥取県・Tottori",
                    "島根県・Shimane",
                    "岡山県・Okayama",
                    "広島県・Hiroshima",
                    "山口県・Yamaguchi",
                    "",
                    array(
                        "type" => 'title',
                        "text" => "四国・Shikoku",
                    ),
                    "徳島県・Tokushima",
                    "香川県・Kagawa",
                    "愛媛県・Ehime",
                    "高知県・Kōchi"
                ),
                array(
                    array(
                        "type" => 'title',
                        "text" => "九州・Kyushu",
                    ),
                    "福岡県・Fukuoka",
                    "佐賀県・Saga",
                    "長崎県・Nagasaki",
                    "熊本県・Kumamoto",
                    "大分県・Ōita",
                    "宮崎県・Miyazaki",
                    "鹿児島県・Kagoshima",
                    "",
                    "沖縄県・Okinawa"
                )
            );
            foreach ($perfs as $col) {
                echo '<div class="perfs-col">';
                foreach ($col as $perf) {
                    switch (TRUE) {
                        case(is_array($perf) && array_key_exists('type', $perf)):
                            echo "<div class=\"perfs-cell\"><div class=\"perfs-title\">" . $perf['text'] . "</div></div>";
                            break;
                        case(is_array($perf) && array_key_exists('cols', $perf)):
                            $margin = 34;
                            $col_width = 170;
                            $cell_width = $perf['cols'] * $col_width + ($perf['cols'] - 1) * $margin;
                            echo "<div class=\"perfs-cell\" style=\"width: {$cell_width}px;\"><button>" . $perf['text'] . "</button></div>";
                            break;
                        case (empty($perf)):
                            echo "<div class=\"perfs-cell\">" . $perf . "</div>";
                            break;
                        default:
                            echo "<div class=\"perfs-cell\"><button>" . $perf . "</button></div>";
                            break;
                    };
                };
                echo "</div>";
            };
            ?>
        </div>
        <div class="back">
            <button>
                <span class="lang_en">&lt;&lt;BACK</span>
                <span class="lang_ja">&lt;&lt;戻る</span>
            </button>
        </div>
    </div>
</form>
<script>
    var pageName = "prefecture";

    $(document).ready(function () {
        $('#step10 #place-container button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            document.waiverForm.prefecture.value = $(this).html();
            submitForm();
            //saveWaiverDataToSessionAndGoToNextStep({prefecture: });
        });
    });
</script>
<?php require_once('selfCheckInFooter.php'); ?>
