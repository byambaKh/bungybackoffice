<?php
$pageName = "firstName";
require_once('selfCheckInHeader.php')
?>
<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">

    <div id="step9">
        <div class="header-container">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en"><span style="font-size: 80px;">please enter your</span> first name</span>
                    <span style="font-size:60px;" class="lang_ja">ローマ字で下の名前をご入力ください。</span>
                </h1>
            </div>
        </div>
        <div id="name-container">
            <div id="name-input-container"><input name="firstname"></div>
            <div id="del-button">
                <button><span class="lang_en">DEL</span><span class="lang_ja">訂正</span></button>
            </div>
            <div id="enter-button">
                <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
            </div>
        </div>
        <div id="keyboard-container">
            <?php
            $keyboard = array(
                array('q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'),
                array('a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l'),
                array('z', 'x', 'c', 'v', 'b', 'n', 'm')
            );
            foreach ($keyboard as $line) {
                echo '<div class="keyboard-line">';
                foreach ($line as $letter) {
                    echo "<div class=\"keyboard-button\"><button>" . strtoupper($letter) . "</button></div>";
                };
                echo "</div>";
            };
            ?>
            <div class="keyboard-line">
                <div class="keyboard-button lang_en" id="space">
                    <button>SPACE</button>
                </div>
                <div class="keyboard-button lang_ja" id="space">
                    <button>スペース</button>
                </div>
            </div>
        </div>
        <div class="back">
            <button>
                <span class="lang_en">&lt;&lt;BACK</span>
                <span class="lang_ja">&lt;&lt;戻る</span>
            </button>
        </div>
    </div>
</form>

<script>
    var pageName = "firstName";

    $(document).ready(function () {
        $('#step9 .keyboard-button button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            $('#step9 #name-input-container input').val('' + $('#step9 #name-input-container input').val() + $(this).html().replace('SPACE', ' ').replace('スペース', ' '));
            var element = $('#step9 #name-input-container input')[0];
            if (element.setSelectionRange) {
                element.setSelectionRange(element.value.length, element.value.length); // scrolls to end
                element.focus(); // makes caret visible
            }
            ;
        });
        $('#step9 #del-button  button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            var val = $('#step9 #name-input-container input').val();
            if (val.length > 0) {
                $('#step9 #name-input-container input').val(val.substr(0, val.length - 1));
            }
            ;
            var element = $('#step9 #name-input-container input')[0];
            if (element.setSelectionRange) {
                element.setSelectionRange(element.value.length, element.value.length); // scrolls to end
                element.focus(); // makes caret visible
            }
            ;
        });
        $('#step9 #enter-button  button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            var val = $('#step9 #name-input-container input').val();
            if (val.length > 0) {
                submitForm();
                //saveWaiverDataToSessionAndGoToNextStep({first_name: val});
            } else {
                show_info("validfirst");
            }
            ;
        });
    });
</script>
<?php require_once('selfCheckInFooter.php'); ?>
