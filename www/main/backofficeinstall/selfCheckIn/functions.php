<?php
require_once('includes/application_top.php');

function indexOfPageInPageSequence($pageName) {
    $pageSequence = $_SESSION['data']['pageSequence'];

    for ($i = 0; $i < count($pageSequence); $i++) {
        if ($pageSequence[$i]['pageName'] == $pageName) {
            return $i;
        }
    }
    return null;
}

function getNextStep($pageName) {
    $pageSequence = $_SESSION['data']['pageSequence'];
    $nextPageIndex = indexOfPageInPageSequence($pageName) + 1;

    $nextPage = &$pageSequence[$nextPageIndex];

    if ($nextPageIndex >= (count($pageSequence)) || ($nextPage == null)) {//if the page is out of bounds return the first page
        return $pageSequence[0];

    } else if ($nextPage['hidden']) {
        return getNextStep($nextPage['pageName']);//recursion

    } else return $pageSequence[$nextPageIndex];
}

function getNextStepUrl($pageName) {
    $nextStep = getNextStep($pageName);
    return $nextStep['pageUrl'];
}

function getPreviousStep($pageName) {
    $pageSequence = $_SESSION['data']['pageSequence'];
    $previousPageIndex = indexOfPageInPageSequence($pageName) - 1;

    $previousPage = &$pageSequence[$previousPageIndex];

    if ($previousPageIndex < 0) {
        return $pageSequence[0];

    } else if ($previousPage['hidden']) {
        return getPreviousStep($previousPage['pageName']);//recursion

    } else return $pageSequence[$previousPageIndex];
}

function getPreviousStepUrl($pageName) {
    $previousStep = getPreviousStep($pageName);
    return $previousStep['pageUrl'];
}

function goToPageWithUrl($url) {
    header("Location: $url");//include next and previous buttons in get?
}

function goToNextStep() {
    global $pageName;
    goToPageWithUrl(getNextStepUrl($pageName));
}

function goToPreviousStep() {
    global $pageName;
    goToPageWithUrl(getPreviousStepUrl($pageName));
}

function hidePagesFromPageSequence($pages) {
    $pageSequence = &$_SESSION['data']['pageSequence'];

    //$pages = array('transaction', 'action');
    foreach ($pages as $page) {
        $index = indexOfPageInPageSequence($page);
        if($index) {
            $pageSequence[$index]['hidden'] = true;
        }
    }
}

function unHidePagesFromPageSequence($pages) {
    $pageSequence = &$_SESSION['data']['pageSequence'];

    //$pages = array('transaction', 'action');
    foreach ($pages as $page) {
        $index = indexOfPageInPageSequence($page);
        $pageSequence[$index]['hidden'] = false;
    }
}

function totalPriceForJumps($bookingId) {
    $booking = getBookingInfo($bookingId);
    $total = 0;

    foreach ($booking as $b) {
        if($b['selfCheckInPaid'] != 0) return 0;//if this was already paid at self checkin the price is zero
        if ($b['NoOfJump'] == 0) continue;
        if ($b['CollectPay'] == 'Onsite') {
            $total += $b['NoOfJump'] * $b['Rate'];
            $total += $b['RateToPay'] * $b['RateToPayQty'];
            $total += $b['photos'] * $b['photos_qty'];
        }
    }

    return $total;
}

/**
 * @param @bookingId int The id of the booking
 * @return array An array of the
 */
function getBookingBreakDown ($bookingId)
{
    $bookingDetails = array (
        'name'                  => '',
        'site_id'               =>  0,
        'items'            =>  array(),
    );

    $jump = array(
        'price' => '',
        'quantity'      => '',
        'time'      => ''

    );

    $bookings = getBookingInfo($bookingId);
    $bookingDetails['name'] = $bookings[0]['RomajiName'];

    $places = queryForRows("SELECT * FROM sites WHERE hidden = 0 ORDER BY id;");
    $placesById = array();
    foreach($places as $place) {
        $placesById[$place['id']] = $place['display_name'];
    }

    foreach($bookings as $booking) {
        if(($booking['NoOfJump'] != 0) && ($booking['DeleteStatus'] != 1)) {
            $placeName = $placesById[$booking['site_id']];

            $bookingDetails['items'][] = array(
                'name'       => "$placeName Jump",
                'quantity'   => $booking['NoOfJump'],
                'price'      => $booking['Rate']
            );

            if($booking['2nd_jqty']) {
                $bookingDetails['items'][] = array(
                    'name'       => "$placeName 2nd Jump",
                    'quantity'   => $booking['2ndj_qty'],
                    'price'      => $booking['2ndj']
                );
            }
            //todo how to handle payments to agents (rate to pay)
        }
    }

    return $bookingDetails;
}

//TODO move the timezone in to a separate file
//http://php.net/manual/en/timezones.asia.php

function calculateAge($year, $month, $day) {
    //the diff function will not include this year if it is your birthday. So you must specify the hours and seconds
    date_default_timezone_set("Asia/Tokyo");
    $dateTimeBirth = DateTime::createFromFormat("Y-M-j H:i:s", "$year-$month-$day 00:00:00", new DateTimeZone("Asia/Tokyo"));
    $dateTimeNow = new DateTime(date('Y-m-d H:i:s'), new DateTimeZone("Asia/Tokyo"));

    $age = $dateTimeBirth->diff($dateTimeNow);
    return $age->y;

}

function roundTimeToSlot($currentTime){
    //rounds time to the nearest half hour

    $currentMinutes = $currentTime->format("i");

    //if the time is between 0 and 15, then round to 0
    if($currentMinutes <  15) $currentTime->setTime($currentTime->format("H"), 0, 0);
    //if the time is between 15 and 30, then round to 30
    else if($currentMinutes <  30) $currentTime->setTime($currentTime->format("H"), 30, 0);
    //if the time is between 30 and 45, then round to 30
    else if($currentMinutes <  45) $currentTime->setTime($currentTime->format("H"), 30, 0);
    //if the time is between 45 and 59, go to the next hour
    else if($currentMinutes <=  59) {
        $currentTime->setTime($currentTime->format("H") + 1, 0, 0);
    }

    return $currentTime;
}

function getNextTimeSlots($currentTimeSlot, $numberOfNextSlots){
    //go to the timeMaster table and get next n timeslots
    $sql = "SELECT id FROM TimeMaster WHERE bookingTime ='".$currentTimeSlot->format("H:i A")."';";
    $result = mysql_query($sql);
    while(($ids[] = mysql_fetch_assoc($result)) || array_pop($ids));
    $timeSlotId = $ids[0]['id'];

    //Decrement by 1 so that the LIMIT function includes the current time
    $startingLimit = $timeSlotId;
    $startingLimit--;

    //note you cannot order by id as some bozo has made the id field a varchar instead of an int. The order comes out wrong
    $sql = "SELECT * FROM TimeMaster ORDER BY bookingTime ASC LIMIT $startingLimit , $numberOfNextSlots;";
    $results     = mysql_query($sql);
    while(($slotsResults[] = mysql_fetch_assoc($results)) || array_pop($slotsResults));

    $slots = array();
    foreach($slotsResults as $key => $slotsResult){
        //get rid of AM or PM in the string as createFromFormat goes crazy with 24hour time and AM or PM in the string
        $bookingTime = preg_replace(array("/ AM/i", "/ PM/i"), "", $slotsResult['bookingTime']);
        $dateTimeString = $currentTimeSlot->format("Y-m-d")." ".$bookingTime;
        $slots[] = DateTime::createFromFormat("Y-m-d H:i", $dateTimeString);
    }

    return $slots;
}

function getOpenJumpsForNextNumberOfSlots($currentTimeSlot, $numberOfNextSlots){
    //take the current slot
    //find the next n slots
    $openSlots = getNextTimeSlots($currentTimeSlot, $numberOfNextSlots);
    return getOpenJumpsForSlots($openSlots);
}

//This uses the original code to find the number of open slots as my calculation differs
function getOpenJumpsForNextNumberOfSlotsOriginal($dateTime, $numberOfNextSlots){
    $dateTime = roundTimeToSlot($dateTime);

    $date = $dateTime->format('Y-m-d');
    $schedules = getBookTimes($date, 0, false, CURRENT_SITE_ID);

    $slotsAndJumps = array();
    foreach($schedules['options'] as $index => $openSlot){
        //We remove AM and PM because using those with 24 hour time causes unpredictable results
        $openSlotTime = preg_replace(array('/ PM/', '/ AM/'), '', $openSlot['value']);
        $jumpTimeSlot = DateTime::createFromFormat('Y-m-d H:i', "$date $openSlotTime");

        //only consider timeSlots after now
        if($jumpTimeSlot >= $dateTime) {
            $slotsAndJumps[] = array(
                'bookingDateAndTime' => $jumpTimeSlot,
                'openJumps' => $openSlot['avail']
            );
        }
    }

    return $slotsAndJumps;
}

function getOpenJumpsForSlot($dateTime){
    //This code is from search.php, it was not working and the function it calls is too complicated
    $date = $dateTime->format('Y-m-d');
    $timeSlot = $dateTime->format('H:i A');

    /*
    $schedules = getBookTimes($date, 0, false, CURRENT_SITE_ID);
    $i = 0;
    foreach($schedules['options'] as $schedule) {
        if ($schedule['value'] == $timeSlot && $schedule['open'] == 1) {
            $i = $schedule['avail'];
        };
    };
    if ($i < 1) {
        $i = 0;
    };
    */

    //return $i;

    global $config;

    $siteId = CURRENT_SITE_ID;

    $sql = "SELECT BookingTime, SUM(NoOfJump) AS jumps FROM customerregs1 WHERE BookingDate = '$date' AND site_id = $siteId AND BookingTime = '$timeSlot' GROUP BY BookingTime;";
    //$sql ="SELECT BookingTime, SUM(NoOfJump) AS jumps FROM customerregs1 WHERE BookingDate = '2014-11-28' AND site_id = 3 AND BookingTime = '14:00 PM' GROUP BY BookingTime;";

    $openJumps = queryForRows($sql);
    //config['online_slots'] holds the number of jumps available in a single slot. We subtract the jumps booked from it
    //to find out the number of free jumps
    if(is_array($openJumps)) {
        if(isset($openJumps[0])&&is_array($openJumps[0]))
            return $config['online_slots'] - $openJumps[0]['jumps'];
    }
    else return 0;
}

function getOpenJumpsForSlots($openSlots){

    $slotsAndJumps =  array();
    foreach($openSlots as $index => $openSlot){
        $slotsAndJumps[] = array(
            'bookingDateAndTime' => $openSlot,
            'openJumps' => getOpenJumpsForSlot($openSlot)
        );
    }

    return $slotsAndJumps;
}

/**
 * A function to cancel any payments that are left open
 * @param string $hostname Name of the host to perform the cancellation on
 */
function cancelIncompletePayment($hostname) {
    $sql = "
   UPDATE jcm_transaction SET jcm_transaction.state = 'CANCEL' WHERE jcm_transaction.id =
   (SELECT jt.id FROM (SELECT * FROM jcm_transaction) as jt
        LEFT JOIN jcm_host
            ON (jcm_host.id = jt.host)
    WHERE jcm_host.hostname = '$hostname'
    AND jt.state <> 'COMPLETED'
    ORDER BY jt.id DESC
    LIMIT 1);
   ";

    queryForRows($sql);
}

function saveWaiverData($waiverData){
    $waiver['site_id']		= CURRENT_SITE_ID;
    $waiver['bid']			= $waiverData['bookingId'];
    $waiver['photo_number'] = $waiverData['includePhotos'];
    $waiver['agree']		= $waiverData['agree'];
    $waiver['birth_year']	= $waiverData['birth_year'];
    $waiver['birth_month']	= date("M", mktime(0, 0, 0, preg_replace("/([^\d]+)/", "", $waiverData['birth_month']), 1, 2000));
    $waiver['birth_day']	= $waiverData['birth_day'];
    $waiver['sex']			= $waiverData['sex'];
    $waiver['lastname']		= $waiverData['lastname'];
    $waiver['firstname']	= $waiverData['firstname'];

    $waiver['prefecture']	= mb_strtoupper(preg_replace("/([\P{Latin}]+)/u", "", $waiverData['prefecture']));
    $waiver['phone_number'] = $waiverData['phone_number'];
    $waiver['weight_kg']    = str_replace("kg", "", $waiverData['weight']);
    $waiver['email']		= $waiverData['email'];
    //we add 1 since the waiver has not been saved yet
    $waiver['jump_number']	= getDayJumpNumber($waiverData['BookingDate'], CURRENT_SITE_ID) + 1;


    mysql_query("set names utf8;");
    db_perform("waivers", $waiver);

    return mysql_insert_id();//This will be zero if the previous query failed
}

function savePhotoPurchase($waiverData){
    //save the photo payment to the database
    //insert in to merchandise_sales and possibly merchandise_sales_items
    global $config;
    $pricePhoto                     = $config['price_photo'];
    $photoQuantity                  = $waiverData['includePhotos'];
    $saleTotal                      = $pricePhoto * $photoQuantity;

    $data['sale_time']              = date("Y-m-d H:i:s");
    $data['sale_total_photo']       = $saleTotal;
    $data['sale_total']             = $saleTotal;
    $data['sale_total_qty']         = $photoQuantity;
    $data['sale_total_qty_photo']   = $photoQuantity;
    $data['site_id'] = CURRENT_SITE_ID;
    db_perform('merchandise_sales', $data);
    return mysql_insert_id();
}

function saveBookingDataFromWaiverData($data){
    global $config;
    $fullName = $data['lastname']." ".$data['firstname'];
    $timeNow  = new datetime("now");
    $timeNowString = $timeNow->format('y-m-d h:i:s');

    $booking['site_id']			  = CURRENT_SITE_ID;
    $booking['BookingDate']		  = $data['BookingDate'];
    $booking['BookingTime']		  = $data['BookingTime'];
    $booking['NoOfJump']		      = 1;
    $booking['RomajiName']		  = $fullName;
    $booking['CustomerLastName']   = $data['lastname'];
    $booking['CustomerFirstName']  = $data['firstname'];
    $booking['Prefecture']		  = $data['prefecture'];
    $booking['ContactNo']		  = $data['phone_number'];
    $booking['CustomerEmail']	      = $data['email'];
    $booking['DeleteStatus']	      = 0;
    $booking['DayStatus']		  = 0;
    $booking['TimeStatus']		  = 0;
    $booking['BookingType']		  = "walk-in";
    //$booking['Rate']			      = $config['first_jump_rate'];
    $booking['Rate']			      = getFirstJumpRate(CURRENT_SITE_ID, $booking['BookingDate']);
    $booking['BookingReceived']	  = $timeNowString;
    $booking['CollectPay']		  = "Onsite";
    $booking['RateToPay']		  = 0;
    $booking['RateToPayQTY']	  = 0;
    $booking['Checked']			  = 1;
    $booking['SplitName1']		  = $fullName;
    $booking['SplitName2']		  = $fullName;
    $booking['SplitName3']		  = $fullName;
    $booking['SplitTime1']		  = $data['BookingTime'];
    $booking['SplitTime2']		  = '';//$data[''];
    $booking['SplitTime3']		  = ''; //$data[''];
    $booking['SplitJump1']		  = ''; //$data[''];
    $booking['SplitJump2']		  = ''; //$data[''];
    $booking['SplitJump3']		  = ''; //$data[''];
    $booking['Notes']			  = "Walk In (".date('Y-m-d H:i').")";
    //Photos are added to the booking on at the end of signing
    //$booking['photos_qty']		  = $data['includePhotos'];
    //the daily view looks at a photo price as a purchase...
    //$booking['photos']			  = ($data['includePhotos'] ? $data['includePhotosPrice'] : 0);//photos are saved
    $booking['video_qty']		  = 0;
    $booking['video']			  = 0;
    $booking['gopro_qty']		  = 0;
    $booking['gopro']			  = 0;
    $booking['2ndj_qty']		      = 0;
    $booking['2ndj']			      = 0;
    $booking['tshirt_qty']		  = 0;
    $booking['tshirt']			  = 0;
    $booking['other_qty']		  = 0;
    $booking['other']			  = 0;
    $booking['Merchandise']		  = 0;
    $booking['Agent']			  = "none";
    $booking['UserName']		      = "bungy";
    $booking['CancelFee']		  = 0;
    $booking['CancelFeeQTY']	  = 0;
    $booking['CancelFeeCollect']  = 0;
    $booking['GroupBooking']	  = 0;
    $booking['place_id']		  = CURRENT_SITE_ID;
    $booking['foc']				  = 0;

    mysql_query("set names utf8;");
    db_perform("customerregs1", $booking);

    return mysql_insert_id();//This will be zero if the previous query failed
}

//get current number of jumps on this day
function getDayJumpNumber($bookingDate, $site_id) {
    //if we get the number of completed waivers for the current day that is the total number of jumps completed
    //at present
    $sql = "SELECT IFNULL(COUNT(ws.id), 0) AS JumpCount FROM customerregs1 AS cr
        INNER JOIN waivers AS ws ON (cr.CustomerRegId = ws.bid)
        WHERE cr.BookingDate = '$bookingDate'
        AND cr.site_id = $site_id
        AND cr.DeleteStatus <> 1
        AND `CancelFeeQTY` = 0
    GROUP BY cr.BookingDate;";
    $jumpCount = queryForRows($sql)[0];

    //todo the plus 1 needs to be moved out of this function as it should return the current photo count
    return ($jumpCount['JumpCount'] ? $jumpCount['JumpCount'] : 0) + 1;
}

//get the number for the ticket
function getDayPhotoNumber($bookingDate, $site_id) {
    //count the number of checked in bookings with waivers that have photos and add 1 as that is the current photo number
    $sql = "
    SELECT count(ws.id) AS photoCount, RomajiName, photos_qty FROM customerregs1 AS cr
        INNER JOIN waivers AS ws ON (cr.CustomerRegId = ws.bid)
        WHERE cr.BookingDate = '$bookingDate'
        AND cr.site_id = $site_id
        AND cr.DeleteStatus <> 1
        AND `CancelFeeQTY` = 0
        AND photos_qty > 0
        AND checked = 1
    GROUP BY cr.BookingDate;";

    $results = queryForRows($sql);
    //todo the plus 1 needs to be moved out of this function as it should return the current jump count
    return ($results[0]['photoCount'] ? $results[0]['photoCount'] : 0) + 1;
}

function getYearJumpNumber($year, $site_id) {
    $sql = "SELECT COUNT(ws.id) AS JumpCount FROM customerregs1 AS cr
        INNER JOIN waivers AS ws ON (cr.CustomerRegId = ws.bid)
        WHERE cr.BookingDate LIKE '{$year}-%'
        AND cr.site_id = $site_id
        AND cr.DeleteStatus <> 1
        AND `CancelFeeQTY` = 0;";
    $jumpCount = queryForRows($sql)[0];

    return ($jumpCount['JumpCount'] ? $jumpCount['JumpCount']  : 0) + 1;
}

//get the jump code for the certificate
function getJumpCode($bookingData, $bookingDate, $siteId) {
    //BookingInitials
    $initials = $bookingData['firstname'][0].$bookingData['lastname'][0];
    //Jump Number For The Day
    $dayJumpNumber = getDayJumpNumber($bookingDate, $siteId);
    //Jump Number For The Year

    $yearJumpNumber = getYearJumpNumber(date("Y", strtotime($bookingDate)), $siteId);
    return "$initials  $dayJumpNumber.$yearJumpNumber";
}

function loadHostSettings($hostName = null)
{
    global $mysql;
    $hosts = array();

    if ($hostName) {//query for that host and put it in an array
        $sql = "SELECT * FROM jcm_host WHERE hostname = '$hostName';";
        $hosts = queryForRows($sql);
    } else {
        $sql = "SELECT * FROM jcm_host WHERE site = ".CURRENT_SITE_ID.";";
        $hosts = queryForRows($sql);
    }

    foreach ($hosts as $hostId => $host) {
        $hosts[$hostId]['settings'] = json_decode($host['settings'], true);
    }

    return $hosts;
}

function getRemoteHostName()
{
    $hostName = '';

    if($hostArray = json_decode($_SERVER['HTTP_USER_AGENT'], true)) {
        //use the hostname created by the get-host-name extension by extracting it from the json
        if($hostArray['extension'] == 'get-machine-name.xpi')
            $hostName = $hostArray['host'];
    } else if (isset($_GET['host'])) {//if there is a host set in the URL use that if the
        $hostName = $_GET['host'];
    } else {
        //die('No Host Name Found');
        $hostName = "DEV01";
    }

    return $hostName;
}

function setBookingPaid($bookingId)
{
    //assume that a booking completed has been fully paid for (includes variable bookings)
    mysql_query("UPDATE customerregs1 SET selfCheckInPaid = 1 WHERE CustomerRegID = $bookingId OR GroupBooking = $bookingId");
}

function cordBounds($date = NULL, $site_id = NULL)
{
    $query = "SELECT * FROM daily_reports WHERE site_id = $site_id AND date = '$date';";
    $results = queryForRows($query);

    if(count($results) == 1)
        $cordBounds = $results[0];
    else
        (die("No cord bounds for the day"));

    //get the weight from the daily report
    $minWeight			 = 40;

    $bounds = array();

    $bounds['yellowMin'] = (int)$minWeight;
    $bounds['yellowMax'] = (int)$cordBounds['eq_cord_tw_yellow'];

    $bounds['redMin']	 = (int)$bounds['yellowMax'] + 1;
    $bounds['redMax']	 = (int)$cordBounds['eq_cord_tw_red'];

    $bounds['blueMin']   = (int)$bounds['redMax'] + 1;
    $bounds['blueMax']   = (int)$cordBounds['eq_cord_tw_blue'];

    $bounds['blackMin']  = (int)$bounds['blueMax'] + 1;
    $bounds['blackMax']  = (int)$cordBounds['eq_cord_tw_black'];

    return $bounds;
}

/**
 * @param      $weight the weight of the jumper, must be in kg as a floating point string or number. No letters should be appended
 * @param null $date the date of the jump, will default to today if not set
 * @param null $site_id the site of the jump, will default to this site if not set
 *
 * @return string
 */
function getCordColorForWeightKg($weight, $date = NULL, $site_id = NULL)
{
    if(!is_numeric($weight)) die('Non Numeric Weight Encountered. Cannot supply a cord color.');
    //the boundaries are stored as integers so weight must be rounded up
    $weight = round($weight);

    $cordBounds = cordBounds($date, $site_id);

    $yellowMin = $cordBounds['yellowMin'];
    $yellowMax = $cordBounds['yellowMax'];

    $redMin	   = $cordBounds['redMin'];
    $redMax	   = $cordBounds['redMax'];

    $blueMin   = $cordBounds['blueMin'];
    $blueMax   = $cordBounds['blueMax'];

    $blackMin  = $cordBounds['blackMin'];
    $blackMax  = $cordBounds['blackMax'];

    if($weight < $yellowMin) {
        $output = "underweight";

    } else if (($weight >= $yellowMin) && ($weight <= $yellowMax)) {
        $output ="yellow";

    } else if (($weight >= $redMin) && ($weight <= $redMax)) {
        $output ="red";

    } else if (($weight >= $blueMin) && ($weight <= $blueMax)) {
        $output ="blue";

    } else if (($weight >= $blackMin) && ($weight <= $blackMax)) {
        $output ="black";

    } else {
        $output = "overweight";
    }

    return "$output";
}

function getTotalBookingPrice($bookingId) {
    $query = "
            SELECT
                IF(selfCheckInPaid, 0, SUM(NoOfJump * Rate + `RateToPay` * RateToPayQty + photos)) AS total
                    FROM customerregs1
                WHERE (GroupBooking = $bookingId OR CustomerRegId = $bookingId);
            ";
    return queryForRows($query)[0]['total'];
}

function getJumpRate($bookingData) {
    global $config;

    $jumpRate = 0;
    $firstJumpRate = getFirstJumpRate(CURRENT_SITE_ID, date("Y-m-d"));

    if ($bookingData['bookingType'] == "preBooking") {

        $bookingRecord = getBookingInfo($bookingData['bookingId'])[0];

        if ($bookingRecord['CollectPay'] == "Onsite") {//only calculate a price if the payment is onsite
            if ($bookingData['jumpType'] == 'regular') {
                //$jumpRate = $config['first_jump_rate'];
                $jumpRate = $firstJumpRate;

            } else if ($bookingData['jumpType'] == 'second') {
                $jumpRate = $config['second_jump_rate'];

            } else if ($bookingData['jumpType'] == 'repeater') {
                $jumpRate = $config['repeater_rate'];

            } else if ($bookingData['jumpType'] == 'otherSiteDiscount') {
                $jumpRate = $firstJumpRate - 1000;

            }
        }
    } else if ($bookingData['bookingType'] == 'walkInBooking') {
        if ($bookingData['jumpType'] == 'regular') {
            $jumpRate = $firstJumpRate;

        } else if ($bookingData['jumpType'] == 'second') {
            $jumpRate = $config['second_jump_rate'];

        } else if ($bookingData['jumpType'] == 'repeater') {
            $jumpRate = $config['repeater_rate'];

        } else if ($bookingData['jumpType'] == 'otherSiteDiscount') {
            $jumpRate = $firstJumpRate - 1000;

        }
    }

    return $jumpRate;
}


function getPhotoPrice($bookingData){
    global $config;
    $totalPhotoPrice = $config['price_photo'] * $bookingData['includePhotos'];
    return $totalPhotoPrice;

}

function isStandardBooking($bookingId) {
    $query = "
        SELECT
            @is_group :=IF(GroupBooking >= 1, TRUE, FALSE) AS is_group,
            @is_combo :=IF(Notes LIKE '%combo%', TRUE, FALSE) AS is_combo,
            IF(NOT(IF(GroupBooking >= 1, TRUE, FALSE) OR IF(Notes LIKE '%combo%', TRUE, FALSE)), 1, 0) AS is_standard #NOT(A OR B) = A NOR B
		FROM
            customerregs1
        WHERE
            (GroupBooking = $bookingId OR CustomerRegId = $bookingId)
                ";

    $results = queryForRows($query);
    return $results[0]['is_standard'];
}

function addNewPriceToBooking($bookingId, $rate)
{   //the booking becomes a group booking
    query("DROP TEMPORARY TABLE IF EXISTS temp_booking;");

    /*Make the booking a group booking, and decrement a jump from it*/
    query("UPDATE customerregs1 SET GroupBooking = 1,  NoOfJump = NoOfJump - 1, is_scm_created_group_booking = 1  WHERE CustomerRegID = {$bookingId};");

    /*Insert row in to a temp table*/
    query("CREATE TEMPORARY TABLE temp_booking SELECT * FROM customerregs1 WHERE CustomerRegID = {$bookingId};");

    /*Set group booking id, create a single jump booking*/
    query("UPDATE temp_booking SET CustomerRegID = NULL, GroupBooking = {$bookingId}, NoOfJump = 1, Rate = {$rate};");

    /*Copy back to the booking table*/
    query("INSERT INTO customerregs1 SELECT * FROM temp_booking;");

    /*Drop the table*/
    query("DROP TEMPORARY TABLE IF EXISTS temp_booking;");
}

function adjustPriceOfBookingForGroupJumper($bookingId, $rate) {

    if(isStandardBooking($bookingId)) {
        $noOfJump = '?';// get this from the booking
        //if standard booking
        if($noOfJump > 1 ) {
            addNewPriceToBooking($bookingId, $rate);

        } else {
            //single jump so just update the rate
            $query = "UPDATE customerregs1 SET Rate = {$rate} WHERE CustomerRegID = {$bookingId}";
            mysql_query($query);

        }

    } else {
        //if it is a group booking, the price is paid all at once for the entire group
        //this code to edit the price for a group is useless.
        //
        //if group booking ie: has multiple rows
                //check if that price already exists in the booking
                    //if yes
                        //decrement original price row
                        //increment the new price row
                    //if no
                        //create duplicate line
                        //decrement the row duplicated row

    }

}

function getBookingMainId($bookingId) {
    $query = "SELECT
                IF(GroupBooking <= 1, CustomerRegId, GroupBooking) AS master_id
                FROM customerregs1
            WHERE CustomerRegId = $bookingId";

    $results = queryForRows($query);
    return $results[0]['master_id'];

}

function calculateRemainingToBePaid() {
    //for group or combo booking calculates everything
    $payments = &$_SESSION['checkIn']['payments'];

    $payments['total'] = $payments['photos'] + $payments['booking'] + $payments['merchandise'];
    return $payments['total'];
    //when the transaction is complete, set what ever was paid to zero

}

function getJumpRateByJumpType($jumpType){
    global $config;
    $firstJumpRate = getFirstJumpRate(CURRENT_SITE_ID, date("Y-m-d"));

    $jumpPrice = 0;
    switch ($jumpType) {
        case "regular":
            $jumpPrice = $firstJumpRate;
            break;

        case "second":
            $jumpPrice = $firstJumpRate;
            break;

        case "repeater":
            $jumpPrice = $config['repeater_rate'];
            break;

        case "otherSiteDiscount":
            $jumpPrice = $firstJumpRate - 1000;
            break;
    }

    return $jumpPrice;
}

function changeBookingPrice($bookingId, $rate)
{
    $query = "UPDATE customerregs1 SET Rate = $rate WHERE CustomerRegID = $bookingId;";

    if(in_array($rate[0], ['+', '-'])){//increment or decrement
        $query = "UPDATE customerregs1 SET Rate = Rate $rate WHERE CustomerRegID = $bookingId;";
    } else {
        $query = "UPDATE customerregs1 SET Rate = $rate WHERE CustomerRegID = $bookingId;";
    }

    query($query);
}

function getBookingType($bookingId)
{
    $query = "
        SELECT
            @is_group :=IF(GroupBooking >= 1, TRUE, FALSE) AS is_group,
            @is_combo :=IF(Notes LIKE '%combo%', TRUE, FALSE) AS is_combo,
            IF(NOT(@is_group OR @is_combo), 1, 0) AS is_standard #NOT(A OR B) = A NOR B
            FROM
                customerregs1
            WHERE
                (GroupBooking = $bookingId OR CustomerRegId = $bookingId)
    ";

    return queryForRows($query)[0];

}

function setJumpNumber($bookingId, $jumpNumber)
{
    if(in_array($jumpNumber[0], ['+', '-'])){//increment or decrement
        $query = "UPDATE customerregs1 SET NoOfJump = NoOfJump $jumpNumber WHERE CustomerRegID = $bookingId;";
    } else {
        $query = "UPDATE customerregs1 SET NoOfJump = $jumpNumber WHERE CustomerRegID = $bookingId;";
    }

    query($query);

}

function setGroupBookingId($bookingId, $groupBookingId)
{
    $query = "UPDATE customerregs1 SET GroupBooking = $groupBookingId WHERE CustomerRegID = $bookingId;";
    query($query);
}

function createRepeaterBooking($bookingId, $rate)
{
    //if it is a single booking of one row
    $duplicateBookingId = NULL;
    $bookingType = getBookingType($bookingId);

    if($bookingType['is_standard']) {
        $duplicateBookingId = duplicateBooking($bookingId);
        changeBookingPrice($duplicateBookingId, $rate);//change the booking price to the new rate
        setJumpNumber($bookingId, '- 1');//decrement the jump number of the original booking
        setGroupBookingId($bookingId, 1);//make group booking parent
        setJumpNumber($duplicateBookingId, '1');//set the jump number of the new booking to 1
        setGroupBookingId($duplicateBookingId, $bookingId);//make group booking child

    } else {
        //todo implement for groups
        //Which booking do I choose from a group booking
        //A repeater is a discounted regular jump
        //ie:
        //choose the cheapest? does it matter?
        //you are still subtracting
        //booking of
        // [8000] x 4, [7000] x 4 = 32 + 28 = 60,000
        // -> [8000] x 3, [7000] x 4 + [7000] x 1 = 59,000
        // -> [8000] x 4, [7000] x 3 + [7000] x 1 = 60,000 //no difference
        // -> [8000] x 4, [7000] x 3 + [6000] x 1 = 59,000 //difference

        //As long as you subtract 1000 from a jump it does not matter where it comes from
        //if group booking
        //get all rows
        //if a row has 1 jump, discount rate by discount
        //if not
        //find the booking with the maximum rate
        //duplicate row
        //set rate to new lower rate
        //set jump number to one
        //set rate to pay to 1
        //what happens with photos?


    }

    return $duplicateBookingId;
}

function query($query)
{
    mysql_query($query);
}

function getLastInsertId()
{
    return mysql_insert_id();
}

function duplicateBooking($sourceId)
{
    $duplicateId = null;

    query("DROP TEMPORARY TABLE IF EXISTS _tmptable_1;");
    query("CREATE TEMPORARY TABLE _tmptable_1 SELECT * FROM customerregs1 WHERE CustomerRegID = $sourceId;");
    query("UPDATE _tmptable_1 SET CustomerRegID = NULL;");
    query("INSERT INTO CustomerRegs1 SELECT * FROM _tmptable_1;");

    $duplicateId = getLastInsertId();

    query("DROP TEMPORARY TABLE IF EXISTS _tmptable_1;");

    return $duplicateId;
}
