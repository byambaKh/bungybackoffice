<?php
//This is an effort to reduce the number of calls made to the server
require_once('../includes/application_top.php');
require_once('pageSequence.php');
require_once('functions.php');
require_once('jcm/config.php');

//error_reporting(E_ALL);
//ini_set('display_errors', 0);

class CheckInSequence
{
    private $pageName;
    private $pageSequenceObject;
    private $data;

    /*
    function startPaymentWithTransactionAmount($transactionAmount)
    {
        $this->setPaymentTransactionAmount($transactionAmount);
        header("Location: transaction.php");
        exit();
    }

    function setPaymentTransactionAmount($transactionAmount)
    {
        $this->data['transaction']['amount'] = $transactionAmount;
    }
    */

    function performAction($action)
    {
        if ($action == "cancelWaiver") {
            $_SESSION['checkIn'] = null;

            $this->pageSequenceObject->goToPageWithUrl("02_buyPhotos.php");
            exit();

        } else if ($action == "finishWaiver") {
            //save the this->data and return to the start
            //save waiver this->data to the database
            //save signature image to a folder
            $_SESSION['checkIn'] = null;
            header("Location: selfCheckIn.php");//restart
            exit();

        } else if ($action == 'completedPayment') {
            //hidePageFromPageSequence(array('transaction', 'action', 'buyPhotos'));
        }
    }

    function bookOrWalk()
    {
        $this->pageSequenceObject->setPageSequenceForCheckInType($_POST['bookingType']);

        if($_POST['bookingType'] == 'walkInBooking') {
            $currentTime = new DateTime('now');
            $jumpTime = roundTimeToSlot($currentTime);
            //$openJumps = getNextTimeSlots($timeSlot, 5);
            //$jumpsAndSlots = getOpenJumpsForNextNumberOfSlots($timeSlot, 10);
            $this->data['BookingDate'] = $jumpTime->format('Y-m-d');
            $this->data['BookingTime'] = $jumpTime->format('H:i A');
        }
        //$this->getNextFreeSlot($currentTime); //Dont use this for now, just get the next open slot

        $this->pageSequenceObject->goToNextStep($this->pageName);
    }

    function bookingName()
    {
        //if this is for group bookings where we want the main id of the booking that
        //is in CustomerRegId (of the first booking) of Groupbooking of the child bookings
        $this->data['bookingId'] = getBookingMainId($this->data['bookingId']);
        $bookingData = getBookingInfo($this->data['bookingId']);

        if(isStandardBooking($this->data['bookingId'])) {
            //hide jumpType page for offsite bookings
            //This will not work if there is a group of 5 and one of the jumps is free
            if (($bookingData[0]['CollectPay'] == "Offsite") || $bookingData[0]['foc']) {//foc can be '' or 0 for non free
                $this->pageSequenceObject->hidePageFromPageSequence('jumpType');
            }
        } else {
            //If it is an offsite Group or Combo booking hide all payment related pages
            $this->pageSequenceObject->setPageSequenceForCheckInType('preBookingGroupOrCombo');

            if ($bookingData[0]['CollectPay'] == "Offsite") {
                $this->pageSequenceObject->hidePageFromPageSequence('paymentInProcess');
                $this->pageSequenceObject->hidePageFromPageSequence('transactionGroupCombo');
                $this->pageSequenceObject->hidePageFromPageSequence('jumpType');

            }
        }
    }

    /**
     * This function will get the price
     */
    function buyPhotos()
    {
        global $config;
        //round payment up
        //we are not going use the total booking price that will include photos entered by reception
        //$totalPayment = getJumpRate($this->data);

        //if (($totalPayment / 500) % 2) $totalPayment += 500;//if the payment is has 500 at the end, add 500 so that the machine does not get confused
        //if this has been paid booking will be zero

        //get the full price of the jump is
        if($_SESSION['checkIn']['bookingType'] == 'preBookingGroupOrCombo') {
            $_SESSION['checkIn']['payments']['booking'] = getTotalBookingPrice($this->data['bookingId']);

        } else {
            $_SESSION['checkIn']['payments']['booking'] = getJumpRate($this->data);

        }

        //
        $_SESSION['checkIn']['payments']['photos'] = getPhotoPrice($this->data) * $this->data['includePhotos'];

        $totalPayment = calculateRemainingToBePaid();

        if ($totalPayment == 0) { //skip payment
            $this->pageSequenceObject->hidePageFromPageSequence('transaction', 'regular');
            $this->pageSequenceObject->goToNextStep("transaction");

        } else {

            //$this->pageSequenceObject->unHidePageFromPageSequence('transaction', 'regular');
            //$this->setPaymentTransactionAmount($totalPayment);
            //header("Location: transaction.php");
            //exit();
        }
    }

    function updatePhotos()
    {
        global $config;

        if ($this->data['includePhotos'] && isset($this->data['bookingId'])) {
            //add a photos purchase to the booking...
            $photos_qty = $this->data['includePhotos'];
            $photosTotalPrice = $config['price_photo'] * $photos_qty;
            mysql_query("
                    UPDATE customerregs1
                        SET photos_qty = photos_qty + {$photos_qty},
                        photos = photos + {$photosTotalPrice}
                    WHERE CustomerRegID = {$this->data['bookingId']}
                ");
            //TODO what happens if there is no booking? Make a merchandise payment
        }
    }

    function buyTshirts()
    {
        if ($_POST['includeTShirt']) {
            $_SESSION['checkIn']['payments']['merchandise'] = $_POST['includeTShirtPrice'];

        } else {
            header("Location: selfCheckIn.php");//restart
            exit();

        }
    }

    function signature()
    {
		global $config;
        //if this is a walkin booking, save it to the customer bookings table as it will not have been saved yet
        if ($this->data['bookingType'] == 'walkInBooking') {
            $this->data['NoOfJump'] = 1;
            $this->data['bookingId'] = saveBookingDataFromWaiverData($this->data);
        } else {
            $preBookingData = getBookingInfo($this->data['bookingId']);
            $this->data['BookingTime'] = $preBookingData[0]['BookingTime'];
            $this->data['BookingDate'] = $preBookingData[0]['BookingDate'];

            //check the customer in
            $query = "UPDATE customerregs1 SET Checked = 1 WHERE CustomerRegID = {$this->data['bookingId']}";
            mysql_query($query);

        }

        setBookingPaid($this->data['bookingId']);//record booking as paid

        $waiverInsertId = saveWaiverData($this->data);
        $fileName = $waiverInsertId;
        $path = "../uploads/signatures/$fileName.jpg";

        $waiverImageFileHandle = fopen($path, "w");
        $image = str_replace("data:image/jpeg;base64,", "", $this->data['img']);//get rid of mime data as this invalidates the file
        $image = base64_decode($image);
        fputs($waiverImageFileHandle, $image);
        fclose($waiverImageFileHandle);

        unset($waiverImageFileHandle);
    }

    function photoPurchaseCompleted()
    {
        savePhotoPurchase($this->data);
        $_SESSION['checkIn'] = null;
    }

    function openJumps()//no longer used as we are not allowing users to select their jump slot
    {
        $_POST['bookingDateAndTime'] = substr($_POST['bookingDateAndTime'], 0, -3);
        $bookingDateAndTime = DateTime::createFromFormat("Y-m-d H:i", ($_POST['bookingDateAndTime']));
        $this->data['BookingDate'] = $bookingDateAndTime->format("Y-m-d");
        $this->data['BookingTime'] = $bookingDateAndTime->format("H:i A");
    }

    function weight()
    {

    }

    function transactions()
    {
        //delete the transaction page from the sequence
        //array('transaction', 'buyPhotos', 'buyTShirts', 'jumpType');
        if($this->pageSequenceObject->pageIsViewable('transaction', 'groupCombo')) {
            $this->pageSequenceObject->hidePageFromPageSequence('transaction', 'groupCombo');
            $this->pageSequenceObject->hidePageFromPageSequence('transaction', 'regular');
           //do nothing as only this page is removed
        } else if($this->pageSequenceObject->pageIsViewable('transaction', 'regular')) {
            $this->pageSequenceObject->hidePageFromPageSequence('transaction', 'regular');
            $this->pageSequenceObject->hidePageFromPageSequence('buyPhotos');
            $this->pageSequenceObject->hidePageFromPageSequence('jumpType');

        }

    }

    function verifyAge($minimumJumpAge)
    {
        if (isset($this->data['birth_year']) && isset($data['birth_month']) && isset($data['birth_day'])) {
            $age = calculateAge($this->data['birth_year'], $data['birth_month'], $data['birth_day']);
            if ($age < $minimumJumpAge) {
                $_SESSION['checkIn'] = null;
                header("Location: 18_ageRejected.php");//go back to the beginning
                //echo "You are $age. The miniumum age for bungy jumping is $minimumJumpAge.";
                die();
            }
        }
    }

    function verifyWeight($minimumWeight, $maximumWeight)
    {
        if (isset($this->data['weight'])) {
            if (($this->data['weight'] > $maximumWeight) || ($this->data['weight'] < $minimumWeight)) {
                header("Location: 17_weightRejected.php");//go back to the beginning
                exit();
            }
        }
    }

    function mergePostDataIntoSession()
    {
        if (!is_array($_POST)) $_POST = array();
        $_SESSION['checkIn'] = array_merge($_SESSION['checkIn'], $_POST);
    }

    //If a booking is discounted the rate changes. The problem is that we store them as rate * noOfJump
    //If only 1 is discounted, rate * noOfJump cannot work. Instead we need to put a new booking in to
    //the system with the correct price and reduce the current price of the booking
    function setJumpRateOnBooking($rate)
    {
        //if this booking has 1 jump, change the rate and return
        //if it has more than one jump
            //write a new booking to the system with same info
        //make this->booking = new booking

    }

    function getRateForJumpType($jumpType)
    {

    }

    function jumpType()
    {
        /*
        $jumpType = $_POST['jumpType'];
        createRepeaterBooking($this->data['bookingId'], 5000);
        */


    }

    function __construct($pageSequenceObject)
    {
        /***********************************************************************************************************************/
        /*************************************************GENERAL LOGIC*********************************************************/
        /***********************************************************************************************************************/

        $this->pageSequenceObject = $pageSequenceObject;
        $this->pageName = $_POST['pageName'];//identifies the page that sent the request
        $action = isset($_GET['action']) ? $_GET['action'] : null;


        $this->data = &$_SESSION['checkIn'];

        //GET is used for actions not specific to a particular page
        //ie we could finish the Waiver at any time or cancel at any time
        //In contrast with post, those actions are tied to the particular page
        $minimumJumpAge = 13;
        $minimumWeight = 40;//weight in Kg
        $maximumWeight = 105;//weight in Kg

        $this->mergePostDataIntoSession();
        $this->verifyAge($minimumJumpAge);
        $this->verifyWeight($minimumWeight, $maximumWeight);

        /**************************************************************************************************************/
        /*************************************************ACTION LOGIC*************************************************/
        /**************************************************************************************************************/

        if (isset($action)) {
            $this->performAction($action);
        }

        /**************************************************************************************************************/
        /**************************************************PAGE LOGIC**************************************************/
        /*Logic that runs after page $pageName is submitted************************************************************/
        /**************************************************************************************************************/

        $payments = &$_SESSION['checkIn']['payments'];

        //we use POST parameters when we are performing actions specific to a particular page
        //we use GET when there is an action that could be called from any page such as cancelling
        if ($this->pageName == "bookOrWalk") {
            $payments['photos']         = 0;
            $payments['booking']        = 0;
            $payments['merchandise']    = 0;

            $this->bookOrWalk();
        } else if ($this->pageName == "bookingName") {
            $this->bookingName();

        } else if ($this->pageName == "jumpType") {
            if($_POST['jumpType'] === "walkIn") {
                $jumpRate = getJumpRateByJumpType($_POST['jumpType']);
                //$this->setPaymentTransactionAmount($jumpRate);

                if(isStandardBooking($this->data['CustomerRegID'])||($this->data['is_scm_created_group_booking'])){
                    //$this->setJumpRateOnBooking($jumpRate);
                    addNewPriceToBooking($this->data['CustomerRegID'], $jumpRate);
                }
            } else {//regular booking of 1 or more
                $_SESSION['checkIn']['payments']['booking'] = getJumpRate($this->data);
            }

        } else if ($this->pageName == "buyPhotos") {
            $this->buyPhotos();

        } else if ($this->pageName == "buyTShirts") {
            $this->buyTshirts();

        } else if ($this->pageName == "photosPurchaseCompleted") {
            $this->photoPurchaseCompleted();

        } else if ($this->pageName == "paymentInProgress") {

        }
        /* else if ($this->pageName == 'openJumps') { $this->openJumps(); }*/
        else if ($this->pageName == "weight") {
            $this->weight();

        } else if ($this->pageName == 'transaction') {
            //delete the transaction page from the sequence
            //$this->pageSequenceObject->hidePageFromPageSequence(array('transaction', 'buyPhotos', 'buyTShirts', 'jumpType'));
            //Booking is unlocked in the javascript once payment is complete
            //booking is set as paid
            $payments['photos'] = 0;
            $payments['booking'] = 0;
            $payments['merchandise'] = 0;

        } else if ($this->pageName == "signature") {
            $this->signature();
            $this->updatePhotos();//booking id is set in signature

        }

        /**********************************************************************************************************************/
        /*********************Default code that runs when the page logic does not direct to new page***************************/
        /**********************************************************************************************************************/

        //if we specify a next page url, go to it, or else go to the next page in the sequence
        if (isset($_GET['nextPageUrl'])) {
            $this->pageSequenceObject->goToPageWithUrl($this->pageSequenceObject->getNextStepUrl($this->pageName));

        } else $this->pageSequenceObject->goToNextStep($this->pageName);

    }

    /**
     * @param $currentTime
     */
    public function getNextFreeSlot($currentTime)
    {
        $jumpsAndSlots = getOpenJumpsForNextNumberOfSlotsOriginal($currentTime, 10);
        $slotCount = count($jumpsAndSlots);
        //this could all be moved in to a function getNextFreeSlotIn($currentTime, $numberOfslots, ...)
        if ($slotCount >= 2) {
            //test slot 1 & 2
            if (($jumpsAndSlots[0]['openJumps'] > 0) || ($jumpsAndSlots[1]['openJumps'] > 0)) {
                //redirect
                //get the first open slot
                $openSlot = ($jumpsAndSlots[0]['openJumps'] ? $jumpsAndSlots[0] : $jumpsAndSlots[1]);
                $this->data['BookingDate'] = $openSlot['bookingDateAndTime']->format('Y-m-d');
                $this->data['BookingTime'] = $openSlot['bookingDateAndTime']->format('H:i A');
                $this->pageSequenceObject->hidePageFromPageSequence('noOpenJumps');
                $this->pageSequenceObject->goToNextStep($this->pageName);
            }

        } else if ($slotCount == 1) {
            if (($jumpsAndSlots[0]['openJumps'] > 0)) { //only test slot 1
                //redirect
                //get the first open slot
                $this->data['BookingDate'] = $jumpsAndSlots[0]['bookingDateAndTime']->format('Y-m-d');
                $this->data['BookingTime'] = $jumpsAndSlots[0]['bookingDateAndTime']->format('H:i A');
                $this->pageSequenceObject->hidePageFromPageSequence('noOpenJumps');
                $this->pageSequenceObject->goToNextStep($this->pageName);
            }
        }
    }
}

$pageSequence = new \SelfCheckIn\PageSequence();
$checkIn = new CheckInSequence($pageSequence);


