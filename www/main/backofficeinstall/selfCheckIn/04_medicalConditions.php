<?php
$pageName = "waiver";
require_once('selfCheckInHeader.php');
?>
<style>
	.readable_text{
		font-family: "Helvetica";
		color: white;
	}

	#en_waiver_table tr td{
		padding-bottom: 10px;
		vertical-align: top;
		font-size: 16px;
		line-height: 19px;
		font-weight: 100;
	}

	.en_waiver_table_number{
		font-size: 15px;
		line-height: 8px;
	}

	#en_waiver_header p{
		font-size: 17px !important;
		line-height: 17px !important;
		font-weight: 900 !important;
	}

	#ja_waiver_table tr td{
		padding-bottom: 10px;
		vertical-align: top;
		font-size: 16px;
		line-height: 22px;
		font-weight: 100;
        color: white;
		font-weight: 100;
	}

	.ja_waiver_table_number{
		font-size: 20px;
		line-height: 8px;
		font-weight: 100;
	}
</style>
  <div id="step4">
    <div class="header-container" style="margin-top: 10px; margin-bottom: 10px;">
      <div class="head-image-container">
        <h1>
          <span class="lang_en">waiver of liability and indemnity agreement</span>
          <span class="lang_ja" style="font-size:45px;">バンジージャンプ確認書兼保険申込</span>
        </h1>
      </div>
    </div>
    
	<div id="content-container">
<!--since I am going to rewrite this entire file I am leaving these styles here-->
	<span class="lang_en">
	<span id="en_waiver_header" class="lang_en en_waiver_header readable_text">
		<p>READ AND FULLY UNDERSTAND EACH PROVISION OF THIS AGREEMENT AND INDICATE YOUR ACKNOWLEDGEMENT BY TOUCHING "I AGREE".</p>
		<p>IN CONSIDERATION of the organizers allowing you (hereinafter referred to as "the participant") to utilize the facilities and equipment and to participate in bungy jumping and its associated activities, it is agreed that:</p>
	</span>

	<table id="en_waiver_table" class="readable_text">
		<tr>
			<td class="en_waiver_table_number">1.<td>I understand &amp; acknowledge that participating in bungy jumping has inherent risks, including but not limited to physical injury &amp; even death.</td>
		</tr>
		<tr>
			<td class="en_waiver_table_number">2.<td>I confirm that I understand and am in accordance with all applicable participation requirements. I assert that I have no injury or disease that would affect my ability to safely participate as such. I also agree to refrain from participating in bungy jumping when I am pregnant or under the influence of alcohol. I also understand and acknowledge that if I conceal an illness or injury, when participating in bungy jumping, the insurance policy as stipulated in article 3 shall not apply to me and that I shall be liable for my own acts (and omissions), accordingly.</td>
		</tr>
		<tr>
			<td class="en_waiver_table_number">3.<td>I understand that SM provides the following insurance cover for participating in bungy jumping: Death or residual disability: 10 million JPY Hospital confinement: 4,000 JPY per day (up to 180 days) Hospital visits: 2,000 JPY per day (up to 90 days). <br>I also understand and acknowledge that this insurance policy does not include the costs of medical care, transportation or property damage.</td>
		</tr>
		<tr>
			<td class="en_waiver_table_number">4.<td>I agree to observe and obey all posted rules, to follow any instructions or directions given by SM through its staff, and to be fully aware of all safety matters. I also understand and acknowledge that if I do not observe and obey the said rules and instructions given by SM, SM has the right to cancel my participation in bungy jumping, and that I shall be liable for my own actions (and omissions), accordingly.</td>
		</tr>
		<tr>
			<td class="en_waiver_table_number">5.<td>I grant a complete and unconditional release of all liability for SM and its staff due to act of god (including unpredicted natural phenomenon).</td>
		</tr>
		<tr>
			<td class="en_waiver_table_number">6.<td>I also understand and acknowledge that I shall be liable to pay the repair and/or replacement costs for any damage that is caused to SM property, in whole or part, by my intentional acts or omissions, or my failure to observe the rules and instructions given by SM and its staff to any facilities or equipment operated and managed by SM.</td>
		</tr>
		<tr>
			<td class="en_waiver_table_number">7.<td>I consent to medical care and transportation as SM or medical professions may deem appropriate in order to obtain treatment in the event of my falling ill or become injured during my participation in the bungy jumping.</td>
		</tr>
		<tr>
			<td class="en_waiver_table_number">8.<td>I consent to the SM’s use of my image at no cost in photographs, motion pictures or recordings taken at any SM’s bungy jumping sites for use in SM’s advertising, marketing or promotion throughout the world.</td>
		</tr>
		<tr>
			<td class="en_waiver_table_number">9.<td>I agree that SM reserves the right to cancel bungy jumping in the event of inclement weather, natural or man-made disasters, acts of god or for any other reason that SM deems, at its discretion, necessary for safety and security of participants or for difficulty of operating and managing bungy jumping.</td>
		</tr>
	</table>
</span>

<span class="lang_ja">
	<p style="font-weight:bold; color: white; font-size: 20px;"> 私は、スタンダード・ムーブ株式会社（以下「主催者」といいます）が主催・催行するバンジージャンプを行うにあたり、この確認書に記載されている条項（１－９）をよく読み、保険の内容やバンジージャンプの危険性、主催者の権利などを理解し、了承し、同意した上で、自筆の署名をいたします。 </p>
	<table id="ja_waiver_table">
		<tr>
			<td class="ja_waiver_table_number">１.<td>私が参加する<nobr>バンジージャンプ</nobr><nobr>は</nobr>、<nobr>その性質上、</nobr><nobr>死傷する危険</nobr>があることを<nobr>十分に</nobr><nobr>認識しています</nobr>。</td>
		</tr>
		<tr>
			<td class="ja_waiver_table_number">２.</td><td>私は、主催者が定めた参加条件に<nobr>反しておりません</nobr>。私は、<nobr>バンジージャンプが</nobr><br><nobr>影響</nobr>を及ぼす可能性のある症状や怪我を<nobr>有しておりません</nobr>。私は、<nobr>飲酒、</nobr><nobr>酒気帯び、</nobr><nobr>妊娠もしておりません。</nobr>私は、私が<nobr>上記</nobr>の<nobr>症状や怪我を隠ぺいして</nobr><nobr>バンジージャンプ</nobr><nobr>を行った</nobr><nobr>場合には、</nobr><nobr>応分の責任</nobr>を負<nobr>わねばならず、</nobr><nobr>保険は適用されない</nobr>ことを<nobr>理解しています。</nobr></td>
		</tr>
		<tr>
			<td class="ja_waiver_table_number">３.</td><td>私は、主催者が加入している保険が以下の内容（補償金額）であることを理解しています。<br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;死亡・後遺障害1,000万円　入院4,000円（180日限度）通院2,000円（90日限度）<br>私は、保険には実際に支払った医療費や交通費等に<nobr>対する</nobr><nobr>補償、</nobr><nobr>また物損に対する</nobr><nobr>補償がないことも</nobr><nobr>理解しています。</nobr></td>
		</tr>
		<tr>
			<td class="ja_waiver_table_number">４.</td><td>私は、主催者が設けた規則やそのスタッフの指示に従い、安全を心がけなければならないということを十分に理解しています。私は、主催者が設けた規則や指示を無視して行った行動に対しては、応分の責任を負わなければならず、主催者が、規則や指示に従わない参加者に対し、<nobr>バンジージャンプ</nobr>への<nobr>参加</nobr>を取り消す権利を有していることも、<nobr>理解しています。</nobr></td>
		</tr>
		<tr>
			<td class="ja_waiver_table_number">５.</td><td>私は、予期し得ぬ自然現象など、不可避的な事象によって引き起こされた事故については、主催者とそのスタッフに対して、<nobr>責任を問うことはしません。</nobr></td>
		</tr>
		<tr>
			<td class="ja_waiver_table_number">６.</td><td>私は、故意により、または主催者が設けた規則もしくは指示に反した行動により、主催者の管理する施設や備品などに損傷を与えた場合、その修理費を負担することを了承します。</td>
		</tr>
		<tr>
			<td class="ja_waiver_table_number">７.</td><td>私は、バンジージャンプへ参加した結果、怪我や病気になった場合、主催者の判断により医療機関での治療を受け、<nobr>緊急</nobr>の<nobr>場合</nobr>には<nobr>主催者また</nobr>は<nobr>医師</nobr>が<nobr>応急処置</nobr>を施すことに同意いたします。</td>
		</tr>
		<tr>
			<td class="ja_waiver_table_number">８.</td><td>私は、バンジージャンプへの参加にあたり撮影された私の写真や映像の肖像権を放棄し、それらを全世界において広告宣伝目的、<nobr>プロモーション</nobr><nobr>活動</nobr>などで<nobr>使用する</nobr><nobr>権利が主催者</nobr>に無償で<nobr>帰属する</nobr><nobr>ことに</nobr><nobr><nobr>同意いたします。</nobr></nobr></td>
		</tr>
		<tr>
			<td class="ja_waiver_table_number">９.</td><td>私は、天災、<nobr>天候不良等不可抗力の理由</nobr><nobr>により、</nobr><nobr>主催者</nobr>が<nobr>バンジージャンプ</nobr>の<nobr>実施もしく</nobr>は<nobr>運営に支障があり、</nobr>又は<nobr>安全</nobr><nobr>確保に</nobr><nobr>支障がある</nobr>と<nobr>判断した場合、</nobr><nobr>バンジージャンプ</nobr>の<nobr>実施が中止となる</nobr><nobr>ことに</nobr><nobr>同意いたします。</nobr></td>
		</tr>
	</table>
</span>
</div>
<form action="" name="waiverForm" method="post">
	<input type="hidden" name="pageName" value="<?php echo $pageName ?>">
	<input type="hidden" name="agree" value="">
	<div id="agree-exit-container">
		<div id="agree" style="padding-top: 10px;">
			<button>
				<span class="lang_en" >I AGREE</span>
				<span class="lang_ja">同意する</span>
			</button>
		</div>
		<div id="exit" style="padding-top: 10px;">
			<button>
				<span class="lang_en">EXIT</span>
				<span class="lang_ja">同意しない</span>
			</button>
		</div>
	</div>
	<div class="back">
		<button>
			<span class="lang_en">&lt;&lt;BACK</span>
			<span class="lang_ja">&lt;&lt;戻る</span>
		</button>
	</div>
</form>


<script>
var pageName = "waiver";

$(document).ready(function () {
		$('#step4 #agree  button').bind('click touchend MSPointerUp pointerup', function (e) {
			e.preventDefault();
			//save_var('agree', 1);
			document.waiverForm.agree.value = 1;
			submitForm();
			//saveWaiverDataToSessionAndGoToNextStep);
		});
$('#step4 #exit  button').bind('click touchend MSPointerUp pointerup', function (e) {
		e.preventDefault();
		//TODO go to the controller with action = clearWaiverData and start again

		document.waiverForm.agree.value = 0;
		submitForm("action=cancelWaiver");

		//clearSessionWaiverData();
		//document.bj_waiver = null;
		//goToStepWithUrl("selfCheckIn.php");
		});
});
</script>
<?php require_once('selfCheckInFooter.php'); ?>
