<?php
include "../includes/application_top.php";
$max_steps = 15;
$site_id = (int)CURRENT_SITE_ID;

if (!isset($_GET['step'])) {
    $_GET['step'] = 1;
};
if ($_GET['step'] < 1 || $_GET['step'] > $max_steps) {
    $_GET['step'] = 1;
};
$lang = 'en';
if (array_key_exists('lang', $_GET) && in_array($_GET['lang'], array('en', 'ja'))) {
    $lang = $_GET['lang'];
};
$pc_version = true;
if (preg_match("/iPad/i", $_SERVER['HTTP_USER_AGENT'])) {
    $pc_version = false;
};
?>

<!DOCTYPE html>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<title>Bungy Japan Waiver</title>
<meta name="apple-mobile-web-app-capable" content="yes">
<meta name="viewport" content="initial-scale=0.8,user-scalable=no,maximum-scale=1,height=device-height">
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">

<?php include "../includes/head_scripts.php"; ?>

<script src="js/jquery.event.move.js"></script>
<script src="waiver.js"></script>
<link rel="stylesheet" href="/Waiver/css/stylesheet.css" type="text/css" media="all"/>
<script>
var pc_version = <?php echo (int)$pc_version; ?>;
function scrollend(e) {
    document.lastScroll = null;
}
function scrollmove(e) {
    var targetEvent = e.originalEvent;
    if (typeof document.lastScroll == 'undefined' || document.lastScroll === null) {
        document.lastScroll = targetEvent.pageY;
    }
    ;
    var el = $(this).children();
    var old_scroll = $(el).prop('scroll');
    if (typeof old_scroll == 'undefined') {
        old_scroll = 0;
    }
    ;
    var scroll = old_scroll - targetEvent.pageY + document.lastScroll;
    if (scroll < 0) {
        scroll = 0;
    }
//alert($(el).height() + ' ' + $(el).parent().height() + ' ' + scroll);
    if (scroll > ($(el).height() - $(el).parent().height())) {
        scroll = $(el).height() - $(el).parent().height();
    }
    ;
    $(el).css('webkitTransform', 'translate(0px,-' + scroll + 'px)');
    $(el).prop('scroll', scroll);
    document.lastScroll = targetEvent.pageY;
}
// scrolling disable
document.addEventListener('touchmove', function (e) {
    e.preventDefault();
});
function select_language(lang) {
    document.bungywaiver_lang = lang;
    $('.lang_en, .lang_ja').css('display', 'none');
    $('.lang_' + lang).css('display', 'block');
}

$(document).ready(function () {
    if (pc_version == 0) {
        $('.scrollable').bind('touchmove', scrollmove);
        $('.scrollable').bind('touchend', scrollend);
    }
    ;
    $('input').prop('disabled', 'disabled');
    goto_step(<?php echo (int)$_GET['step']; ?>);
    select_language('<?php echo $lang; ?>');
    $(".back button").bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        document.current_step--;
        if (document.current_step == 11) {
            document.current_step--;
        }
        ;
        goto_step(document.current_step);
    });
    $('#english-container button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        select_language('en');
        goto_step(2);
    });
    $('#japanese-container button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        select_language('ja');
        goto_step(2);
    });
    $('#step2 .refresh button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        return fill_names();
    });
    $('#step2 #bottom-container button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var selected = false;
        if (pc_version == 0) {
            $('#step2 li').each(function () {
                if ($(this).hasClass('selected')) selected = $(this).prop('id');
            });
        } else {
            selected = $("#step2 select").val();
            if (typeof selected == 'undefined') {
                selected = false;
            }
        }
        ;
        if (selected) {
            save_var('booking_id', selected);
            goto_step(3);
        } else {
            show_info("bookingname");
            save_var('booking_id', 'null');
        }
        ;
    });
    $('#step3 .keyboard-button button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        if ($('#step3 #photo-number input').val().length == 3) return;
        $('#step3 #photo-number input').val('' + $('#step3 #photo-number input').val() + $(this).html());
    });
    $('#step3 #del  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step3 #photo-number input').val();
        if (val.length > 0) {
            $('#step3 #photo-number input').val(val.substr(0, val.length - 1));
        }
        ;
    });
    $('#step3 #photo-dho  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        save_var('photo_number', null);
        goto_step(4);
    });
    $('#step3 #enter  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step3 #photo-number input').val();
        if (val.length > 0) {
            save_var('photo_number', val);
            goto_step(4);
        } else {
            show_info("validphoto");
        }
        ;
    });
    $('#step4 #agree  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        save_var('agree', 1);
        goto_step(5);
    });
    $('#step4 #exit  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        document.bj_waiver = null;
        goto_step(1);
    });
    $('#step5 .keyboard-button button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        if ($('#step5 #photo-number input').val().length == 4) return;
        $('#step5 #photo-number input').val('' + $('#step5 #photo-number input').val() + $(this).html());
    });
    $('#step5 #del  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step5 #photo-number input').val();
        if (val.length > 0) {
            $('#step5 #photo-number input').val(val.substr(0, val.length - 1));
        }
        ;
    });
    $('#step5 #enter  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step5 #photo-number input').val();
        if (val.length > 0) {
            if (val > 1900) {
                save_var('birth_year', val);
                goto_step(6);
            } else {
                show_info("yeargreater");
            }
        } else {
            show_info("validyear");
        }
        ;
    });
    $('#step6 #birth-month-container button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        $('#step6 #birth-month input').val($(this).html());
    });
    $('#step6 #birth-day-container .keyboard-container button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        if ($('#step6 #birth-day input').val().length == 2) return;
        $('#step6 #birth-day input').val('' + $('#step6 #birth-day input').val() + $(this).html());
    });
    $('#step6 #birth-day-del-button button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step6 #birth-day input').val();
        if (val.length > 0) {
            $('#step6 #birth-day input').val(val.substr(0, val.length - 1));
        }
        ;
    });
    $('#step6 #enter button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var bmonth = $('#step6 #birth-month input').val();
        var bday = $('#step6 #birth-day input').val();
        var error = false;
        if (bmonth.length == 0) {
            show_info("validmonth");
            error = true;
        }
        ;
        if (!error && bday.length == 0) {
            show_info("validday");
            error = true;
        }
        ;
        if (!error && bday > 31) {
            show_info("validday31");
            error = true;
        }
        ;
        if (!error) {
            save_var('birth_month', bmonth);
            save_var('birth_day', bday);
            goto_step(7);
        }
        ;
    });
    $('#step7 #male-button button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        save_var('sex', 'male');
        goto_step(8);
    });
    $('#step7 #female-button button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        save_var('sex', 'female');
        goto_step(8);
    });
    $('#step8 .keyboard-button button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        $('#step8 #name-input-container input').val('' + $('#step8 #name-input-container input').val() + $(this).html().replace('SPACE', ' ').replace('スペース', ' '));
        var element = $('#step8 #name-input-container input')[0];
        if (element.setSelectionRange) {
            element.setSelectionRange(element.value.length, element.value.length); // scrolls to end
            element.focus(); // makes caret visible
        }
        ;
    });
    $('#step8 #del-button  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step8 #name-input-container input').val();
        if (val.length > 0) {
            $('#step8 #name-input-container input').val(val.substr(0, val.length - 1));
        }
        ;
        var element = $('#step8 #name-input-container input')[0];
        if (element.setSelectionRange) {
            element.setSelectionRange(element.value.length, element.value.length); // scrolls to end
            element.focus(); // makes caret visible
        }
        ;
    });
    $('#step8 #enter-button  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step8 #name-input-container input').val();
        if (val.length > 0) {
            save_var('last_name', val);
            goto_step(9);
        } else {
            show_info("validlast");
        }
        ;
    });
    $('#step9 .keyboard-button button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        $('#step9 #name-input-container input').val('' + $('#step9 #name-input-container input').val() + $(this).html().replace('SPACE', ' ').replace('スペース', ' '));
        var element = $('#step9 #name-input-container input')[0];
        if (element.setSelectionRange) {
            element.setSelectionRange(element.value.length, element.value.length); // scrolls to end
            element.focus(); // makes caret visible
        }
        ;
    });
    $('#step9 #del-button  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step9 #name-input-container input').val();
        if (val.length > 0) {
            $('#step9 #name-input-container input').val(val.substr(0, val.length - 1));
        }
        ;
        var element = $('#step9 #name-input-container input')[0];
        if (element.setSelectionRange) {
            element.setSelectionRange(element.value.length, element.value.length); // scrolls to end
            element.focus(); // makes caret visible
        }
        ;
    });
    $('#step9 #enter-button  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step9 #name-input-container input').val();
        if (val.length > 0) {
            save_var('first_name', val);
            goto_step(10);
        } else {
            show_info("validfirst");
        }
        ;
    });
    $('#step10 #place-container button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        save_var('prefecture', $(this).html());
        goto_step(11);
    });
    $('#step11 .keyboard-button button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        $('#step11 #street1-input-container input').val('' + $('#step11 #street1-input-container input').val() + $(this).html().replace('SPACE', ' ').replace('スペース', ' '));
        var element = $('#step11 #street1-input-container input')[0];
        if (element.setSelectionRange) {
            element.setSelectionRange(element.value.length, element.value.length); // scrolls to end
            element.focus(); // makes caret visible
        }
        ;
    });
    $('#step11 #del  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step11 #street1-input-container input').val();
        if (val.length > 0) {
            $('#step11 #street1-input-container input').val(val.substr(0, val.length - 1));
        }
        ;
        var element = $('#step11 #street1-input-container input')[0];
        if (element.setSelectionRange) {
            element.setSelectionRange(element.value.length, element.value.length); // scrolls to end
            element.focus(); // makes caret visible
        }
        ;
    });

    $('#step12 .keyboard-button button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        if ($('#step12 #phone-number input').val().length == 11) return;
        $('#step12 #phone-number input').val('' + $('#step12 #phone-number input').val() + $(this).html());
    });
    $('#step12 #del  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step12 #phone-number input').val();
        if (val.length > 0) {
            $('#step12 #phone-number input').val(val.substr(0, val.length - 1));
        }
        ;
    });
    $('#step12 #enter  button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step12 #phone-number input').val();
        if (val.length > 8 && val.length < 12) {
            save_var('phone_number', val);
// ignore medical form screen
            save_var('med_form', 0);
            goto_step(13);
        } else {
            show_info("validphone");
        }
        ;
    });
    /*
     $('#step13 #rather-not button').click(function () {
     $('#step13 input').val('');
     $('#step13 #enter button').trigger('click');
     });
     */
    $('#step13 .keyboard-button button, #step13 #domains-container button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        $('#step13 input').val('' + $('#step13 input').val() + $(this).html().toLowerCase());
    });
    $('#step13 #del button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var val = $('#step13 input').val();
        if (val.length > 0) {
            $('#step13 input').val(val.substr(0, val.length - 1));
        }
        ;
    });
    $('#step13 #enter button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        if (!/(.+)@(.+\..+)/.test($("#step13 input").val())) {
            show_info('validemail');
            return;
        }
        ;
        save_var('email', $("#step13 input").val());
        goto_step(14);
    });

    $('#waiver-signature').bind('movestart MSPointerDown pointerdown', bjmovestart);
    $('#waiver-signature').bind('move MSPointerMove pointermove', bjmove);
    $('#waiver-signature').bind('moveend MSPointerUp pointerup', bjmoveend);
    //$('#waiver-signature').bind('click', bjmovestart);
    $('#step14 #del-button button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        var c = $('#waiver-signature')[0];
        var ctx = c.getContext('2d');
        var rect = document.getElementById('waiver-signature').getBoundingClientRect();
        ctx.fillStyle = 'white';
        ctx.beginPath();
        ctx.fillRect(0, 0, rect.width, rect.height);
        ctx.stroke();
        document.bj_signature = null;
    });
    $('#step14 #enter-button button').bind('click touchend MSPointerUp pointerup', function (e) {
        e.preventDefault();
        if (typeof document.bj_signature == 'undefined' || document.bj_signature === null) {
            alert('Please draw your signature');
            return false;
        }
        var c = $('#waiver-signature')[0];
        var img = c.toDataURL('image/jpeg', 0.8);
        save_var('sig_img', img);
        goto_step(15);
    });

});
function bjmovestart(e) {
    document.bj_last_coords = e;
    bjmove(e);
}
function bjmove(e) {
    if (typeof document.bj_last_coords == 'undefined' || document.bj_last_coords === null) {
        document.bj_last_coords = e;
    }
    var c = $('#waiver-signature')[0];
    var ctx = c.getContext('2d');
    var rect = document.getElementById('waiver-signature').getBoundingClientRect();
    ctx.beginPath();
    ctx.moveTo(document.bj_last_coords.pageX - rect.left - window.scrollX, document.bj_last_coords.pageY - rect.top - window.scrollY);
    ctx.lineWidth = 10;
    ctx.lineCap = 'round';
    ctx.strokeStyle = 'blue';
    ctx.lineTo(e.pageX - rect.left - window.scrollX, e.pageY - rect.top - window.scrollY);
    ctx.stroke();
    document.bj_last_coords = e;
    document.bj_signature = true;
}
function bjmoveend(e) {
    var c = $('#waiver-signature')[0];
    var ctx = c.getContext('2d');
    document.bj_last_coords = null;
}
function save_var(name, value) {
    if (typeof document.bj_waiver == 'undefined' || document.bj_waiver == null) {
        document.bj_waiver = new Object();
    }
    ;
    document.bj_waiver[name] = value;
}
function goto_step(step) {
    if (step == 2) {
        fill_names();
    }
    ;
    if (step == 11) {
        step = 12;
    }
    ;
    document.current_step = step;
    for (var i = 1; i <= <?php echo $max_steps; ?>; i++) {
        if ($('#step' + i).length) {
            $('#step' + i).css('display', 'none');
            if (i == step) {
                $('#step' + i).css('display', 'block');
            }
            ;
        }
        ;
    }
    ;
    if (step == 14) {
        $('#step14 #del-button button').trigger('touchend');
    }
    ;
    // save data, restart form
    if (step == 15) {
        $("#step15progress").css({
            width: 800,
            height: 30,
            "background-color": 'black',
            border: "2px solid white",
            'margin-top': '50px',
            'margin-right': 'auto',
            'margin-left': 'auto'
        }).html("<div></div>");
        $("#step15progress div").css({
            width: 0,
            height: 26,
            "background-color": 'red'
        });
        var timer_sec = 5;
        document.reload_timer = setInterval(function () {
            timer_sec--;
            //$("#restart-in").html(timer_sec);
            var width = $("#step15progress div").css('width').replace('px', '');
            if (width == 0) width -= 2;
            if (width > 600) width -= 2;
            $("#step15progress div").css('width', parseInt(width) + 160);
            if (width > 600) {
                clearInterval(document.reload_timer);
                document.location = '/Waiver/';
            }
            ;
        }, 1000);
    }
    if (step == 15) {
        // save all data
        if (!save_data()) {
            show_info("errorsaving");
            goto_step(14);
            return;
        }
        ;
    }
    ;
}
function save_data() {
    return $.ajax(
        'ajax.php?action=save-waiver',
        {
            data: document.bj_waiver,
            dataType: 'json',
            type: 'POST',
            async: false,
            cache: false,
            success: function (data) {
                return true;
            }
        }
    );
}
function fill_names() {
    $.ajax(
        'ajax.php?action=get-names',
        {
            dataType: 'json',
            cache: false,
            success: function (data) {
                if (data['error']) {
                    show_info(data['error']);
                } else {
                    if (pc_version == 0) {
                        $('#step2 ul > li').remove();
                    } else {
                        $('#step2 select > option').remove();
                    }
                    ;
                    //$('#step2 select').prop('multiple', false);
                    if (data['data'] && data['data'].length) {
                        for (var i in data['data']) {
                            if (!data['data'].hasOwnProperty(i)) continue;
                            if (pc_version == 0) {
                                var o = $('<li></li>');
                                o.prop('id', data['data'][i].id);
                                o.html(data['data'][i].name);
                                o.bind('click touchend MSPointerUp pointerup', function (e) {
                                    e.preventDefault();
                                    $('#step2 li').removeClass('selected');
                                    $(this).addClass('selected');
                                });
                                $('#step2 ul').append(
                                    o
                                );
                            } else {
                                var o = $('<option></option>')
                                $('#step2 select').append(
                                    o.prop('value', data['data'][i].id).prop('text', data['data'][i].name)
                                );
                            }
                            ;
                        }
                    }
                    ;
                    //$('#step2 select').prop('multiple', true);
                }
            },
        }
    );
}
function show_info(msg) {
    var lang = {
        en: {
            "bookingname": "You must select the name under which your booking was made.",
            "validphoto": "You must enter a valid <br /><b>photo number</b><br /> or click<br /> the <b>\"No photos purchased\"</b> button.",
            "yeargreater": "The year must be greater than 1900.",
            "validyear": "The year must be greater than 1900.",
            "validmonth": "Please enter your month of birth.",
            "validday": "Please enter your day of birth.",
            "validday31": "Please enter your day of birth.",
            "validlast": "You must enter your last name to continue.",
            "validfirst": "You must enter your first name to continue.",
            "validphone": "Your phone number must contain from 9 to 11 digits.",
            "validemail": "Pleae enter valid email",
            "errorsaving": 'some issues saving you credentials'
        },
        ja: {
            "bookingname": "代表者様のお名前を<br />選択してください。",
            "validphoto": "お写真の番号を入力するか「ご購入されない方はこちら」のボタンを押してください。",
            "yeargreater": "１９００年以降でご入力ください。",
            "validyear": "１９００年以降でご入力ください。",
            "validmonth": "誕生月を入力してください。",
            "validday": "誕生日を入力してください。",
            "validday31": "誕生日を入力してください。",
            "validlast": "姓字を入力してください。",
            "validfirst": "名前を入力してください。",
            "validphone": "9文字～１１文字でご入力ください。",
            "validemail": 'Pleae enter valid email',
            "errorsaving": 'some issues saving you credentials'
        }
    }
    if (lang.hasOwnProperty(document.bungywaiver_lang) && lang[document.bungywaiver_lang].hasOwnProperty(msg)) {
        msg = lang[document.bungywaiver_lang][msg];
    }
    ;
    $('#info-dialog').html(msg);
    $('#info-dialog').dialog({
        buttons: [
            {
                text: (document.bungywaiver_lang == 'en') ? "Close" : "OK",
                width: 650,
                MSPointerUP: function () {
                    $('#info-dialog').dialog("close");
                },
                pointerup: function () {
                    $('#info-dialog').dialog("close");
                },
                click: function () {
                    $('#info-dialog').dialog("close");
                },
                touchend: function () {
                    $('#info-dialog').dialog("close");
                }
            }
        ],
        dialogClass: "alert",
        draggable: false,
        modal: true,
        position: {my: "center", at: "center", of: window},
        resizable: false,
        minWidth: 800,
        maxWidth: 1200,
        //html : msg,
        title: (document.bungywaiver_lang == 'en') ? "Information" : "注意"
    });
}
</script>
</head>
<body scroll="no">
<?php
if ($pc_version) {
    ?>
    <script>
        $(document).ready(function () {
            $("body").css('padding-top', '40px');
            $(".back").each(function () {
                $(this).css("top", 80 + parseInt($(this).css("top").replace('px', '')));
            });
            $("#domains-container").css("top", 55 + parseInt($("#domains-container").css("top").replace('px', '')));
            //$("#rather-not").css("top", 35 + parseInt($("#rather-not").css("top").replace('px', '')));
            $("#email-note").css("top", 50);
        });
    </script>
<?php
};
?>
<div>
<div id="step1" style="display: none;">
    <div id="waiver-container">
        <div class="header-container"><img alt="Step 1: Language selection" src="img/wheader.png"></div>
        <div id="language-container">
            <div id="english-container" class="half">
                <button><img src="img/wenglish.png"></button>
            </div>
            <div id="japanese-container" class="half">
                <button><img src="img/wjapanese.png"></button>
            </div>
        </div>
    </div>
    <div id="step1-add-info">
        <nobr>文字入力や各ボタンの選択が認識されるまで、</nobr>
        <br/>
        多少時間がかかります。ひとつずつ、ゆっくりめにご入力ください。
    </div>
</div>
<div id="step2" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">NAME OF BOOKING</span>
                <span class="lang_ja">代表者様のお名前</span>
            </h1>

            <h2>
                <span
                    class="lang_en">Please select the name in which your booking was made and then press [Continue].</span>
                <span class="lang_ja">をタッチして、「次へ」お押ししてください。</span>
            </h2>
        </div>
        <div class="refresh">
            <button>
                <span class="lang_en">refresh</span>
                <span class="lang_ja">更新</span>
            </button>
        </div>
    </div>
    <div id="names-container">
        <div id="names-container-child" class="scrollable">
            <?php if ($pc_version == 0) { ?>
                <ul id="booking-list">
                </ul>
            <?php } else { ?>
                <select id="booking-list" multiple="multiple"></select>
            <?php }; ?>
        </div>
        <!--select name="bid" size="6" id="booking-id" style="width: 850px; height: 530px;">
          <option>test1</option>
          <option>test 2</option>
        </select-->
    </div>
    <div id="bottom-container">
        <button>
            <span class="lang_en">continue</span>
            <span class="lang_ja">次へ</span>
        </button>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step3" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">please enter your photo number</span>
                <span class="lang_ja">お写真又はビデオをご購入された方は左手首の番号をご入力ください。</span>
            </h1>

            <h2>
                <span class="lang_en">if you purchased them and have a number on your left wrist</span>
                <span class="lang_ja"></span>
            </h2>
        </div>
    </div>
    <div id="content-container">
        <div id="photo-example"><img src="img/step3_hand.png" alt="Hand example"/></div>
        <div id="photo-number"><input name="photo_number"></div>
        <div id="photo-dho">
            <button>
                <span class="lang_en">No photos purchased.</span>
                <span class="lang_ja">ご購入されない方はこちら</span>
            </button>
        </div>
    </div>
    <div id="photo-keyboard-container">
        <div class="photo-keyboard">
            <div class="keyboard-button">
                <button>1</button>
            </div>
            <div class="keyboard-button">
                <button>2</button>
            </div>
            <div class="keyboard-button">
                <button>3</button>
            </div>
            <div class="keyboard-button">
                <button>4</button>
            </div>
            <div class="keyboard-button">
                <button>5</button>
            </div>
            <div class="keyboard-button">
                <button>6</button>
            </div>
            <div class="keyboard-button">
                <button>7</button>
            </div>
            <div class="keyboard-button">
                <button>8</button>
            </div>
            <div class="keyboard-button">
                <button>9</button>
            </div>
            <div class="keyboard-button">
                <button>0</button>
            </div>
        </div>
        <div id="enter-del-container">
            <div id="del">
                <button><span class="lang_en">DELETE</span><span class="lang_ja">訂正</span></button>
            </div>
            <div id="enter">
                <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
            </div>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step4" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">waiver of liability and indemnity agreement</span>
                <span class="lang_ja">バンジージャンプ確認書兼保険申込書</span>
            </h1>
        </div>
    </div>
    <div id="content-container">
      <span class="lang_en">
<p>READ AND FULLY UNDERSTAND EACH PROVISION OF THIS AGREEMENT AND INDICATE YOUR ACKNOWLEDGEMENT BY TOUCHING "I
    AGREE".</p>

<p>IN CONSIDERATION of the organizers allowing you (hereinafter referred to as "the participant") to utilize the
    facilities and equipment and to participate in bungy jumping and its associated activities, it is agreed that:</p>

<p class="numerable">1. PARTIES INCLUDED: The participant understands that this agreement includes the organizers and
    their partners, employees, instructors, agents, the owners of the land, bridges utilized for bungy jumping, or its
    employees, designers and engineers of equipment or systems used.</p>

<p class="numerable">2. ASSUMPTION OF RISK: The participant is fully aware that bungy jumping and all associated
    activity contain inherent risks and dangers. The participant understands the scope, nature, and extent of the risks
    involved and voluntarily and freely chooses to incur any and all such risks and dangers.</p>
    <p>*Customers who have low bone density, weak bones and/or osteoporosis will not be allowed to participate in bungy jump activities.
    There is a risk that you could break and/or injure your bones due to the strong forces involved in a bungy jump.
    If a claim is made that relates to any broken bones the customer must submit to a Bone Density or DXA (noninvasive) Test.</p>


<p class="numerable">3. EXEMPTION FROM LIABILITY: The participant hereby fully and forever discharges and releases the
    organizers from any and all liability, claims, demands, actions, and causes of action whatsoever arising out of any
    damages sustained by the participant. Exemption from liability includes loss, damage, or injury resulting from the
    negligence of the organizers or from any other cause or causes.</p>

<p class="numerable">4. COVENANT NOT TO SUE: The participant agrees, for him/herself and his/her heirs, executors or
    administrators, or assigns to indemnify and hold harmless the organizers nor to initiate or assist the prosecution
    of any claim for damages.</p>

<p class="numerable">5. INDEMNITY AGREEMENT: The participant agrees for him/herself and his/her heirs, executors,
    assigns or administrators to hold harmless the organizers from any and all losses, claims, actions or proceedings of
    any kind which may be initiated by the participant and/or any other person or organization. This includes
    reimbursement of all legal costs and reasonable counsel fees incurred by the organizers. The agreement shall be
    effective not only for the participant.s first bungy jump or related activity but for any subsequent bungy
    jumps.</p>

<p class="numerable">6. INSURANCE DISCLAIMER: I understand that the organizers provide medical and liability insurance
    for incidents or accidents which may arise as a result of my participation in any phase of Bungy jumping to cover
    medical treatment / hospitalization and death.</p>

<p class="numerable">7. MEDICAL DISCLAIMER: I certify that I do not suffer from any conditions which may endanger the
    safety of myself or anyone else, by participating in bungy jumping. I further certify that I do not suffer from any
    of the following conditions: extreme asthma, epilepsy, a cardio/respiratory disorder, hypertension, any skeletal or
    joint or ligament problem or conditions. I further certify that I am neither pregnant nor in an intoxicated
    state.</p>

<p>I hereby expressly recognize that this agreement is a contract pursuant to which I have released any and all claims
    against the organizers resulting from my participation in bungy jumping including any claims caused by the
    negligence of the organizers. I have read this agreement carefully and fully understand its contents and sign it of
    my own free will. I further certify that I am eighteen (18) years of age or older or I am 13 years or older and I am
    obtaining parental acceptance.</p>
      </span>
      <span class="lang_ja">
<br/>
    <?php if ($site_id > 2): ?>
    <p>
        私は、スタンダード・ムーブ（株）が業務委託を受け催行するバンジージャンプを<br/>
        行うにあたり、最終確認をいたします。保険の内容やバンジージャンプの危険性、主催者の権利などを理解した上で、この確認書に記載されている条項（１－８）を全て読み、理解し、了承し同意した上で、自筆の署名をいたします。
    </p>
    <?php else: ?>
    <p>
        私は、みなかみ町観光まちづくり協会（以下主催者と記載）が主催し、スタンダード・ムーブ（株）が業務委託を受け催行するバンジージャンプを<br/>
        行うにあたり、最終確認をいたします。保険の内容やバンジージャンプの危険性、主催者の権利などを理解した上で、この確認書に記載されている条項（１－８）を全て読み、理解し、了承し同意した上で、自筆の署名をいたします。
    </p>
    <?php endif; ?>

<p>
    １．私が参加するバンジージャンプは、性質上、死傷する危険があることを十分に認識しています。
</p>

<p>
    ２．私は主催者が定めた参加条件（年齢１３歳以上、体重４０～１０５ｋｇで健康である事）に反しておりません。バンジージャンプが影響を<br/>
    　　及ぼす可能性のある症状や怪我はありません。飲酒、酒気帯び、妊娠もしておりません。隠ぺいして行った場合には応分の責任を<br/>
    　　負わねばならず、保険は適応されないことを理解しています。
</p>
<p>
-骨密度が低い方、骨が弱い・もろい方及び骨粗鬆症の方は参加できません。強い圧力が加わることで、骨折及び骨に関する怪我を負う可能性があります。
骨折及び骨に関する怪我について保険請求をされる場合は骨密度検査もしくは DXA 検査 (非侵襲的) を受け、検査結果を提出しなければなりません。
</p>
<?php if ($site_id > 2): ?>
<p>
    ３．私は主催者が加入している保険の内容（補償金額）を理解しています。死亡・後遺障害１０００万円<br/>
    　　実際に支払った医療費や交通費等に対する補償、また物損に対する保証はありません）。
</p>
<?php else: ?>
<p>
    ３．私は主催者が加入している保険の内容（補償金額）を理解しています。死亡・後遺障害１０００万円,入院４０００円（１８０日限度）,<br/>
    　　通院２ ０００円（９０日限度）（実際に支払った医療費や交通費等に対する補償、また物損に対する保証はありません）。
</p>
<?php endif; ?>
<p>
    ４．私は主催者が設けた規則やその委託するスタッフの指示に従い、安全に心がけなければならないということを十分に理解しています。<br/>
    　　正当な指示を無視して行った行動に対しては、応分の責任を負わなければならず、主催者側は指示に従わない参加者に対し、バンジー　<br/>
    　　ジャンプへの参加を取り消す権利を有していることも、理解しています。
</p>

<p>
    ５．私は予期し得ぬ自然現象など、不可避的な事象によって引き起こされた事故については、主催者とその委託するスタッフに対して無制限<br/>
    　　の責任は問いません。３者間（主催者・スタッフ・私参加者）で責任配分の合意に至らぬ場合は、法的な第３者の裁定に従うことを<br/>
    　　了承します。
</p>

<p>
    ６．私は故意または指示に反した行動により、主催者側の管理する施設や備品などに損傷を与えた場合、その修理費を負担することを<br/>
    　　了承します。
</p>

<p>
    ７．私はバンジージャンプを行うにあたり怪我や病気になった場合、主催者側の判断により医療機関での治療を受け、緊急の場合には主催者<br/>
    　　側及び医師が応急処置を施すことに同意いたします。
</p>

<p>
    ８．私はバンジージャンプにおいて撮影した写真や映像の肖像権を放棄し、それらを広告宣伝目的などで使用する権利は主催者側に帰属する<br/>
    　　ことに同意いたします。
</p>
      </span>
    </div>
    <div id="agree-exit-container">
        <div id="agree">
            <button>
                <span class="lang_en">I AGREE</span>
                <span class="lang_ja">同意する</span>
            </button>
        </div>
        <div id="exit">
            <button>
                <span class="lang_en">EXIT</span>
                <span class="lang_ja">同意しない</span>
            </button>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step5" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">please enter the year you were born</span>
                <span class="lang_ja">生まれた年を西暦でご入力ください。</span>
            </h1>
        </div>
    </div>
    <div id="content-container">
        <div id="photo-number" style="margin-left:320px;"><input name="born_year"></div>
    </div>
    <div id="photo-keyboard-container">
        <div class="photo-keyboard">
            <div class="keyboard-button">
                <button>1</button>
            </div>
            <div class="keyboard-button">
                <button>2</button>
            </div>
            <div class="keyboard-button">
                <button>3</button>
            </div>
            <div class="keyboard-button">
                <button>4</button>
            </div>
            <div class="keyboard-button">
                <button>5</button>
            </div>
            <div class="keyboard-button">
                <button>6</button>
            </div>
            <div class="keyboard-button">
                <button>7</button>
            </div>
            <div class="keyboard-button">
                <button>8</button>
            </div>
            <div class="keyboard-button">
                <button>9</button>
            </div>
            <div class="keyboard-button">
                <button>0</button>
            </div>
        </div>
        <div id="enter-del-container">
            <div id="del">
                <button><span class="lang_en">DELETE</span><span class="lang_ja">訂正</span></button>
            </div>
            <div id="enter">
                <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
            </div>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step6" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">please enter your birthday</span>
                <span class="lang_ja">誕生日をご入力ください。</h1>
        </div>
    </div>
    <div id="birth-month-container">
        <div id="birth-month">
            <div id="birth-month-value"><input name="birth_month"></div>
        </div>
        <div class="keyboard-container">
            <div class="keyboard-button lang_en">
                <button>JAN</button>
            </div>
            <div class="keyboard-button lang_en">
                <button>FEB</button>
            </div>
            <div class="keyboard-button lang_en">
                <button>MAR</button>
            </div>
            <div class="keyboard-button lang_en">
                <button>APR</button>
            </div>
            <div class="keyboard-button lang_en">
                <button>MAY</button>
            </div>
            <div class="keyboard-button lang_en">
                <button>JUN</button>
            </div>
            <div class="keyboard-button lang_en">
                <button>JUL</button>
            </div>
            <div class="keyboard-button lang_en">
                <button>AUG</button>
            </div>
            <div class="keyboard-button lang_en">
                <button>SEP</button>
            </div>
            <div class="keyboard-button lang_en">
                <button>OCT</button>
            </div>
            <div class="keyboard-button lang_en">
                <button>NOV</button>
            </div>
            <div class="keyboard-button lang_en">
                <button>DEC</button>
            </div>
            <div class="keyboard-button lang_ja">
                <button>1月</button>
            </div>
            <div class="keyboard-button lang_ja">
                <button>2月</button>
            </div>
            <div class="keyboard-button lang_ja">
                <button>3月</button>
            </div>
            <div class="keyboard-button lang_ja">
                <button>4月</button>
            </div>
            <div class="keyboard-button lang_ja">
                <button>5月</button>
            </div>
            <div class="keyboard-button lang_ja">
                <button>6月</button>
            </div>
            <div class="keyboard-button lang_ja">
                <button>7月</button>
            </div>
            <div class="keyboard-button lang_ja">
                <button>8月</button>
            </div>
            <div class="keyboard-button lang_ja">
                <button>9月</button>
            </div>
            <div class="keyboard-button lang_ja">
                <button>10月</button>
            </div>
            <div class="keyboard-button lang_ja">
                <button>11月</button>
            </div>
            <div class="keyboard-button lang_ja">
                <button>12月</button>
            </div>
        </div>
    </div>
    <div id="birth-day-container">
        <div id="birth-day">
            <div id="bidth-day-value"><input name="birth_day"></div>
            <div id="birth-day-del-button">
                <button><span class="lang_en">DEL</span><span class="lang_ja">訂正</span></button>
            </div>
        </div>
        <div class="keyboard-container">
            <div class="keyboard-button">
                <button>1</button>
            </div>
            <div class="keyboard-button">
                <button>2</button>
            </div>
            <div class="keyboard-button">
                <button>3</button>
            </div>
            <div class="keyboard-button">
                <button>4</button>
            </div>
            <div class="keyboard-button">
                <button>5</button>
            </div>
            <div class="keyboard-button">
                <button>6</button>
            </div>
            <div class="keyboard-button">
                <button>7</button>
            </div>
            <div class="keyboard-button">
                <button>8</button>
            </div>
            <div class="keyboard-button">
                <button>9</button>
            </div>
            <div class="keyboard-button">
                <button>0</button>
            </div>
        </div>
    </div>
    <div id="enter-container">
        <div id="enter">
            <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step7" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">please select your sex</span>
                <span class="lang_ja">性別をお選びください。</span>
            </h1>
        </div>
    </div>
    <div id="male-female-container">
        <div id="male-button">
            <button>
                <span class="lang_en">MALE</span>
                <span class="lang_ja">男性</span>
            </button>
        </div>
        <div id="female-button">
            <button>
                <span class="lang_en">FEMALE</span>
                <span class="lang_ja">女性</span>
            </button>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step8" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en"><span style="font-size: 80px;">please enter your</span> last name</span>
                <span class="lang_ja">ローマ字で姓字をご入力ください。</span>
            </h1>
        </div>
    </div>
    <div id="name-container">
        <div id="name-input-container"><input name="lastname"></div>
        <div id="del-button">
            <button><span class="lang_en">DEL</span><span class="lang_ja">訂正</span></button>
        </div>
        <div id="enter-button">
            <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
        </div>
    </div>
    <div id="keyboard-container">
        <?php
        $keyboard = array(
            array('q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'),
            array('a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l'),
            array('z', 'x', 'c', 'v', 'b', 'n', 'm')
        );
        foreach ($keyboard as $line) {
            echo '<div class="keyboard-line">';
            foreach ($line as $letter) {
                echo "<div class=\"keyboard-button\"><button>" . strtoupper($letter) . "</button></div>";
            };
            echo "</div>";
        };
        ?>
        <div class="keyboard-line">
            <div class="keyboard-button lang_en" id="space">
                <button>SPACE</button>
            </div>
            <div class="keyboard-button lang_ja" id="space">
                <button>スペース</button>
            </div>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step9" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en"><span style="font-size: 80px;">please enter your</span> first name</span>
                <span class="lang_ja">ローマ字で下の名前をご入力ください。</span>
            </h1>
        </div>
    </div>
    <div id="name-container">
        <div id="name-input-container"><input name="firstname"></div>
        <div id="del-button">
            <button><span class="lang_en">DEL</span><span class="lang_ja">訂正</span></button>
        </div>
        <div id="enter-button">
            <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
        </div>
    </div>
    <div id="keyboard-container">
        <?php
        $keyboard = array(
            array('q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'),
            array('a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l'),
            array('z', 'x', 'c', 'v', 'b', 'n', 'm')
        );
        foreach ($keyboard as $line) {
            echo '<div class="keyboard-line">';
            foreach ($line as $letter) {
                echo "<div class=\"keyboard-button\"><button>" . strtoupper($letter) . "</button></div>";
            };
            echo "</div>";
        };
        ?>
        <div class="keyboard-line">
            <div class="keyboard-button lang_en" id="space">
                <button>SPACE</button>
            </div>
            <div class="keyboard-button lang_ja" id="space">
                <button>スペース</button>
            </div>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step10" style="display: none;">
    <div class="header-container">
        <div class="head-image-container"><h1>
                <span class="lang_en">Where do you live?</span>
                <span class="lang_ja">お住まいの都道府県を選択してください。</span></h1></div>
    </div>
    <div id="place-container">
        <?php
        $perfs = array(
            array(
                "北海道・Hokkaido",
                "",
                array(
                    "type" => 'title',
                    "text" => "東北・Tohoku",
                ),
                "青森県・Aomori",
                "岩手県・Iwate",
                "宮城県・Miyagi",
                "秋田県・Akita",
                "山形県・Yamagata",
                "福島県・Fukushima"
            ),
            array(
                array(
                    "type" => 'title',
                    "text" => "関東・Kanto",
                ),
                "茨城県・Ibaraki",
                "栃木県・Tochigi",
                "群馬県・Gunma",
                "埼玉県・Saitama",
                "千葉県・Chiba",
                "東京都・Tōkyō",
                "神奈川県・
            Kanagawa",
                "",
                "",
                "",
                array(
                    "cols" => 3,
                    "text" => "外国・I am a visitor from overseas",
                )
            ),
            array(
                array(
                    "type" => 'title',
                    "text" => "中部・Chūbu",
                ),
                "新潟県・Niigata",
                "富山県・Toyama",
                "石川県・Ishikawa",
                "福井県・Fukui",
                "山梨県・Yamanashi",
                "長野県・Nagano",
                "岐阜県・Gifu",
                "静岡県・Shizuoka",
                "愛知県・Aichi"
            ),
            array(
                array(
                    "type" => 'title',
                    "text" => "関西・Kansai",
                ),
                "三重県・Mie",
                "滋賀県・Shiga",
                "京都府・Kyōto",
                "大阪府・Ōsaka",
                "兵庫県・Hyōgo",
                "奈良県・Nara",
                "和歌山県・
            Wakayama"
            ),
            array(
                array(
                    "type" => 'title',
                    "text" => "中国・Chūgoku",
                ),
                "鳥取県・Tottori",
                "島根県・Shimane",
                "岡山県・Okayama",
                "広島県・Hiroshima",
                "山口県・
            Yamaguchi",
                "",
                array(
                    "type" => 'title',
                    "text" => "四国・Shikoku",
                ),
                "徳島県・
            Tokushima",
                "香川県・Kagawa",
                "愛媛県・Ehime",
                "高知県・Kōchi"
            ),
            array(
                array(
                    "type" => 'title',
                    "text" => "九州・Kyushu",
                ),
                "福岡県・Fukuoka",
                "佐賀県・Saga",
                "長崎県・Nagasaki",
                "熊本県・
            Kumamoto",
                "大分県・Ōita",
                "宮崎県・Miyazaki",
                "鹿児島県・
            Kagoshima",
                "",
                "沖縄県・Okinawa"
            )
        );
        foreach ($perfs as $col) {
            echo '<div class="perfs-col">';
            foreach ($col as $perf) {
                switch (TRUE) {
                    case(is_array($perf) && array_key_exists('type', $perf)):
                        echo "<div class=\"perfs-cell\"><div class=\"perfs-title\">" . $perf['text'] . "</div></div>";
                        break;
                    case(is_array($perf) && array_key_exists('cols', $perf)):
                        $margin = 34;
                        $col_width = 170;
                        $cell_width = $perf['cols'] * $col_width + ($perf['cols'] - 1) * $margin;
                        echo "<div class=\"perfs-cell\" style=\"width: {$cell_width}px;\"><button>" . $perf['text'] . "</button></div>";
                        break;
                    case (empty($perf)):
                        echo "<div class=\"perfs-cell\">" . $perf . "</div>";
                        break;
                    default:
                        echo "<div class=\"perfs-cell\"><button>" . $perf . "</button></div>";
                        break;
                };
            };
            echo "</div>";
        };
        ?>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step11" style="display: none;">
    <div class="header-container">
        <div class="head-image-container"><img src="img/step11_header.jpg"/></div>
    </div>
    <div id="street-container">
        <div id="street1-input-container"><input name="street1"></div>
        <div id="street2-input-container"><input name="street2"></div>
        <div id="street3-input-container"><input name="street3"></div>
        <div id="street4-input-container"><input name="street4"></div>
    </div>
    <div id="keyboard-container">
        <?php
        $keyboard = array(
            array('1', '2', '3', '4', '5', '6', '7', '8', '9', '0'),
            array('q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'),
            array('a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', '-'),
            array('z', 'x', 'c', 'v', 'b', 'n', 'm', '.')
        );
        $i = 0;
        foreach ($keyboard as $line) {
            echo '<div class="keyboard-line' . $i . '">';
            foreach ($line as $letter) {
                echo "<div class=\"keyboard-button\"><button>" . strtoupper($letter) . "</button></div>";
            };
            echo "</div>";
            $i++;
        };
        ?>
        <div class="keyboard-line">
            <div class="keyboard-button lang_en" id="space">
                <button>SPACE</button>
            </div>
            <div class="keyboard-button lang_ja" id="space">
                <button>スペース</button>
            </div>
        </div>
    </div>
    <div id="enter-del-container">
        <div id="enter">
            <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
        </div>
        <div id="del">
            <button><span class="lang_en">DELETE</span><span class="lang_ja">訂正</span></button>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step12" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en"><span style="font-size: 80px;">please enter your</span> phone number</span>
                <span class="lang_ja">電話番号をご入力ください。</span>
            </h1>
        </div>
    </div>
    <div id="content-container">
        <div id="phone-number"><input name="phone_number"></div>
    </div>
    <div id="keyboard-container">
        <?php
        $keyboard = array(
            array('1', '2', '3', '4', '5'),
            array('6', '7', '8', '9', '0')
        );
        foreach ($keyboard as $line) {
            echo '<div class="keyboard-line">';
            foreach ($line as $letter) {
                echo "<div class=\"keyboard-button\"><button>" . strtoupper($letter) . "</button></div>";
            };
            echo "</div>";
        };
        ?>
        <div id="enter-del-container">
            <div id="del">
                <button><span class="lang_en">DELETE</span><span class="lang_ja">訂正</span></button>
            </div>
            <div id="enter">
                <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
            </div>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step13" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1><span class="lang_en">Please enter your email address</span><span
                    class="lang_ja">メールアドレスをご入力ください。</span></h1></div>
        <div id="email-note">
            <span class="lang_en">A customer survey will be sent to the email address provided and those that complete it will be entered in a monthly draw for 2 free jump tickets!</span>
            <span class="lang_ja">アンケートをメールにて送信しますので、ご協力をお願いいたします。お答えいただいた方に毎月抽選で無料ジャンプ券2枚プレゼント！</span>
        </div>
    </div>
    <!--div id="rather-not">
      <button><span class="lang_en">No, thank you</span><span class="lang_ja">入力不要</span></button>
    </div-->
    <div id="email-container">
        <div id="email-input-container"><input name="email"></div>
    </div>
    <div id="keyboard-container">
        <?php
        $keyboard = array(
            array('1', '2', '3', '4', '5', '6', '7', '8', '9', '0'),
            array('q', 'w', 'e', 'r', 't', 'y', 'u', 'i', 'o', 'p'),
            array('a', 's', 'd', 'f', 'g', 'h', 'j', 'k', 'l', '-'),
            array('z', 'x', 'c', 'v', 'b', 'n', 'm', '.', '@', '_')
        );
        $i = 0;
        foreach ($keyboard as $line) {
            echo '<div class="keyboard-line' . $i . '">';
            foreach ($line as $letter) {
                echo "<div class=\"keyboard-button\"><button>" . strtoupper($letter) . "</button></div>";
            };
            echo "</div>";
            $i = '';
        };
        ?>
    </div>
    <div id="enter-del-container">
        <div id="enter">
            <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
        </div>
        <div id="del">
            <button><span class="lang_en">DELETE</span><span class="lang_ja">訂正</span></button>
        </div>
    </div>
    <div id="domains-container">
        <div class="domains-row">
            <div class="domains-cell first-cell">
                <button>.com</button>
            </div>
            <div class="domains-cell second-cell">
                <button>@i.softbank.jp</button>
            </div>
            <div class="domains-cell third-cell">
                <button>@yahoo.co.jp</button>
            </div>
        </div>
        <div class="domains-row">
            <div class="domains-cell first-cell">
                <button>.ne.jp</button>
            </div>
            <div class="domains-cell second-cell">
                <button>@gmail.com</button>
            </div>
            <div class="domains-cell third-cell">
                <button>@hotmail.ne.jp</button>
            </div>
        </div>
        <div class="domains-row">
            <div class="domains-cell first-cell">
                <button>.co.jp</button>
            </div>
            <div class="domains-cell second-cell">
                <button>@ezweb.ne.jp</button>
            </div>
            <div class="domains-cell third-cell">
                <button>@docomo.ne.jp</button>
            </div>
        </div>
    </div>
    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step14" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">Please sign your name using your finger</span>
                <span class="lang_ja">画面に直接ご署名ください。</span>
            </h1>
        </div>
    </div>
    <div id="sig-container">
        <canvas id="waiver-signature" height="600" width="1000"/>
    </div>
    <div id="actions-container">
        <div id="del-button">
            <button><span class="lang_en">DELETE</span><span class="lang_ja">訂正</span></button>
        </div>
        <div id="enter-button">
            <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ<//span></button>
        </div>
    </div>

    <div class="back">
        <button>
            <span class="lang_en">&lt;&lt;BACK</span>
            <span class="lang_ja">&lt;&lt;戻る</span>
        </button>
    </div>
</div>
<div id="step15" style="display: none;">
    <div class="header-container">
        <div class="head-image-container">
            <h1>
                <span class="lang_en">Thank you. Registration complete!</span>
                <span class="lang_ja">登録が完了しました。</span>
            </h1>

            <h2>
                <span class="lang_en">Please give your name to the reception staff.</span>
          <span class="lang_ja">お客様の氏名をスタッフまで<br/>
お申し付けください。</span>
            </h2>
        </div>
    </div>
    <div class="restart-container">
        <!--div>This form will restart in <span id="restart-in">5</span> seconds</div-->
        <div id="step15progress"></div>
    </div>
</div>
</div>
<div id="info-dialog" style="display: none;"></div>
</body>
</html>
