<?php
$pageName = "weight";
require_once('settings.php'); //Get the printer settings
require_once('jcm/config.php');//Get JCM settings
$hostSettings = loadHostSettings($host)[0];//load the settings for a particular host
$enableScales = is_array($hostSettings['settings']) && ($hostSettings['settings']['scales'] == 1);
$action = isset($_GET['action']) ? $_GET['action'] : '';

//error_reporting(E_ALL);
//ini_set('display_errors', 'On');
//load the page
//onload, the page should ...
require_once('selfCheckInHeader.php');

?>
<script>
    var machineName = "<?=$host?>";
    $(document).ready(function () {
        init();//start the websocket client
        //when websocket status is 1 enable button pushing

         $("#refresh-weight").bind('click touchend MSPointerUp pointerup', function (e) {
             e.preventDefault();

             console.log("Event Happened");
             var weight = requestWeight();//request the weight from the server
             console.log("Weight set" + weight);
             //if the weight is null or invalid
             //keep checking
         });
        /*
        $("#refresh-weight").on('click', function (e) {
            e.preventDefault();

            console.log("Event Happened");
            var weight = requestWeight();//request the weight from the server
            console.log("Weight set" + weight);
            //if the weight is null or invalid
            //keep checking
        });
        */
    });
</script>
<!--<script src="js/websocketPage.js"></script>-->
<style>
    .readable_text {
        font-family: "Helvetica";
        color: white;
        text-align: left;
        font-size: 40px;
    }

    .centeredCleared {
        clear: both;
        margin-left: auto;
        margin-right: auto;
    }

    #first-weigh {
        width: 800px;
    }

    #refresh_weight {
        font-family: "pussycat_snickers";
        font-size: 90px;
        line-height: 100px;
        text-align: center;
        color: #000;
        background-color: #F00;
        width: 700px;
        height: 200px;
        border: 2px solid #FFF;
    }

    #get-ready {
        width: 700px;
        margin-left: auto;
        margin-right: auto;

    }

    .are-you-ready{
        font-size: 90px;
        text-align: center;

    }
    #step-on-the-scale{
        font-family: "pussycat_snickers";
        color: #F00;
        line-height: 1em;
        font-weight: normal;
        text-transform: uppercase;
        font-size: 80px;
    }

    .header-container .lang_ja{
        font-size: 70px;
    }
    #step-on-the-scale .lang_ja{
        font-size: 60px;

    }

    .are-you-ready .lang_ja{
        font-size: 65px;
    }
    #loading-spinner{
        display: none;
    }
</style>

<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">
    <input type="hidden" name="weight" value="">
    <input type="hidden" name="weightMeasured" value="">

    <div id="step00">
        <div class="header-container">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en">Let's get ready....</span>
                    <span class="lang_ja">次の準備をしましょう....</span>
                </h1>
            </div>
        </div>
        <div id="get-ready">
            <h2 class="readable_text">
            <span class="lang_en get-ready-list">
                <ol>
                    <li>Place bags/purses onto the table,</li>
                    <li>Empty your pockets,</li>
                    <li>Step onto the scale with your shoes on.</li>
                </ol>
            </span>
            <span class="lang_ja">
                <ol>
                    <li>テーブルの上に手荷物を置いて、</li>
                    <li>ポケットの中を空にして、</li>
                    <li>靴を履いたままで体重計に乗って下さい。</li>
                </ol>
            </span>
            </h2>
            <? if($reminderMessage):?>
                <div id="step-on-the-scale">
                        <span class="lang_en">Please Step on The Scale</span>
                        <span class="lang_ja">体重計に乗って下さい。</span>
                </div>
            <? endif ?>
            <h2 class="readable_text are-you-ready">
                <span class="lang_en">Are you ready?</span>
                <span class="lang_ja">準備は出来ましたか？</span>
            </h2>
        </div>

        <div id="loading-spinner">
                <img src="img/weight_loading_spinner.gif">
        </div>
    </div>

    <a href="13_2_weight.php" style="font-size: 20px; color: #444;">*</a>
    </div>
</form>
<button id="refresh_weight" class="centeredCleared" style="background-color: red; height: 120px; width: 650px; color: white;">
    <span class="lang_en">Yes</span>
    <span class="lang_ja">はい</span>
</button>

<div class="back">
    <button>
        <span class="lang_en">&lt;&lt;BACK</span>
        <span class="lang_ja">&lt;&lt;戻る</span>
    </button>
</div>

<script>
    var pageName = "weight";

</script>
<?php require_once('selfCheckInFooter.php'); ?>
