<?php
//This is an effort to reduce the number of calls made to the server
require_once('../includes/application_top.php');
require_once('pageSequence.php');
require_once('functions.php');
require_once('jcm/config.php');

error_reporting(E_ALL);
ini_set('display_errors', 0);

$pageName = $_POST['pageName'];//identifies the page that sent the request
$action = isset($_GET['action']) ? $_GET['action'] : null;

//TODO: tidy this up
if (!isset($_SESSION['data']['pageSequence'])) $_SESSION['data']['pageSequence'] = $initialSteps;
$pageSequence = &$_SESSION['data']['pageSequence'];
if ($pageSequence == null) $pageSequence = $initialSteps;

$data = &$_SESSION['data'];

//GET is used for actions not specific to a particular page
//ie we could finish the Waiver at any time or cancel at any time
//In contrast with post, those actions are tied to the particular page
$minimumJumpAge = 13;
$minimumWeight = 40;//weight in Kg
$maximumWeight = 105;//weight in Kg

if (isset($action)) {
    if ($action == "cancelWaiver") {
        $_SESSION['data'] = null;
        $startingPage = $initialSteps[0]['pageUrl'];
        //header("Location: $startingPage");//go back to the beginning
        //header("Location: {$initialSteps[0]['pageUrl']}");//go back to the beginning
        goToPageWithUrl("02_buyPhotos.php");
        die();

    } else if ($action == "finishWaiver") {
        //save the data and return to the start
        //save waiver data to the database
        //save signature image to a folder
        $_SESSION['data'] = null;
        header("Location: {$initialSteps[0]['pageUrl']}");//go back to the beginning
        die();

    } else if ($action == 'completedPayment') {
        //hidePagesFromPageSequence(array('transaction', 'action', 'buyPhotos'));
    }
}

//merge $_POST data in to the sessions waiver data
if (!is_array($_POST)) $_POST = array();
$_SESSION['data'] = array_merge($_SESSION['data'], $_POST);

//throw these in to the pages conditionals below
if (isset($data['birth_year']) && isset($data['birth_month']) && isset($data['birth_day'])) {
    $age = calculateAge($data['birth_year'], $data['birth_month'], $data['birth_day']);
    if ($age < $minimumJumpAge) {
        $_SESSION['data'] = null;
        header("Location: 18_ageRejected.php");//go back to the beginning
        //echo "You are $age. The miniumum age for bungy jumping is $minimumJumpAge.";
        //TODO Redirect to a page of reasons for rejecting people. sickness, etc
        die();
    }
}

//TODO throw these in to the pages conditionals below
//Check weight;
if (isset($data['weight'])) {
    if (($data['weight'] > $maximumWeight) || ($data['weight'] < $minimumWeight)) {
        header("Location: 17_weightRejected.php");//go back to the beginning
        die();
    }
}

//we use POST parameters when we are performing actions specific to a particular page
//we use GET when there is an action that could be called from any page such as cancelling
if ($pageName == "bookOrWalk") {
    if ($_POST['bookingType'] == 'preBooking') {
        $pageSequence = $preBookingPageSequence;
    } else if ($_POST['bookingType'] == 'walkInBooking') {
        $pageSequence = $walkInBookingPageSequence;
    } else if ($_POST['bookingType'] == 'tShirtsOnly') {
        $pageSequence = $tShirtsOnlyPageSequence;
    } else if ($_POST['bookingType'] == 'jcmDebug') {
        //SET Booking information here
        $pageSequence = $jcmDebugPageSequence;
    }

    $currentTime = new DateTime('now');
    //$openJumps = getNextTimeSlots($timeSlot, 5);
    //$jumpsAndSlots = getOpenJumpsForNextNumberOfSlots($timeSlot, 10);
    $jumpsAndSlots = getOpenJumpsForNextNumberOfSlotsOriginal($currentTime, 10);
    $slotCount = count($jumpsAndSlots);
    //this could all be moved in to a function getNextFreeSlotIn($currentTime, $numberOfslots, ...)
    if($slotCount >= 2){
        //test slot 1 & 2
        if(($jumpsAndSlots[0]['openJumps'] > 0) || ($jumpsAndSlots[1]['openJumps'] > 0)) {
            //
            //redirect
            //get the first open slot
            $openSlot = ($jumpsAndSlots[0]['openJumps'] ? $jumpsAndSlots[0] : $jumpsAndSlots[1]);
            $data['BookingDate'] = $openSlot['bookingDateAndTime']->format('Y-m-d');
            $data['BookingTime'] = $openSlot['bookingDateAndTime']->format('H:i A');
            hidePagesFromPageSequence(array('noOpenJumps'));
            goToNextStep();
        }


    } else if ($slotCount == 1) {
        if(($jumpsAndSlots[0]['openJumps'] > 0)) { //only test slot 1
            //redirect
            //get the first open slot
            $data['BookingDate'] = $jumpsAndSlots[0]['bookingDateAndTime']->format('Y-m-d');
            $data['BookingTime'] = $jumpsAndSlots[0]['bookingDateAndTime']->format('H:i A');
            hidePagesFromPageSequence(array('noOpenJumps'));
            goToNextStep();
        }
    }

    goToNextStep();
    //goTo

} else if($pageName == "bookingName") {
    //pr($data);
    $bookingData = getBookingInfo($data['bookingId']);
    //hide jumpType page for offsite bookings
    if($bookingData[0]['CollectPay'] == "Offsite") {
        hidePagesFromPageSequence(array('jumpType'));
    }

    //This will not work if there is a group of 5 and one of the jumps is free
    if($bookingData[0]['foc'] != "") {//hide the price from free of charge jumps
        hidePagesFromPageSequence(array('jumpType'));
    }

}  else if ($pageName == "buyPhotos") {
    /*
    $totalPhotoPrice = $config['price_photo'] * $data['includePhotos'];
    $totalBookingPrice = 0;

    if ($data['bookingType'] == "preBooking") {
		//echo 'pre booking';
        $totalBookingPrice = totalPriceForJumps($data['bookingId']);

    } else if ($data['bookingType'] == "walkInBooking") {
        $totalBookingPrice = $config['first_jump_rate'];

    } else if ($data['bookingType'] == "photosOnly") {
        if ($data['includePhotos'] == 0) { //the user decided not to buy photos in the photosOnly sequence
            $_SESSION['data'] = null;
            header("Location: {$initialSteps[0]['pageUrl']}");//go back to the beginning
            die();
        }
    }
    */
    $totalBookingPrice = 0;
    if ($data['bookingType'] == "preBooking") {
        $bookingData = getBookingInfo($data['bookingId']);
        if ($bookingData[0]['CollectPay'] == "Onsite") {//only calculate a price if the payment is onsite
            if ($data['jumpType'] == 'regular') {
                $totalBookingPrice = $config['first_jump_rate'];

            } else if ($data['jumpType'] == 'second') {
                $totalBookingPrice = $config['second_jump_rate'];

            } else if ($data['jumpType'] == 'repeater') {
                $totalBookingPrice = $config['repeater_rate'];

            } else if ($data['jumpType'] == 'otherSiteDiscount') {
                $totalBookingPrice = $config['first_jump_rate'] - 1000;

            }
        }
    } else if ($data['bookingType'] == 'walkInBooking') {
        if ($data['jumpType'] == 'regular') {
            $totalBookingPrice = $config['first_jump_rate'];

        } else if ($data['jumpType'] == 'second') {
            $totalBookingPrice = $config['second_jump_rate'];

        } else if ($data['jumpType'] == 'repeater') {
            $totalBookingPrice = $config['repeater_rate'];

        } else if ($data['jumpType'] == 'otherSiteDiscount') {
            $totalBookingPrice = $config['first_jump_rate'] - 1000;

        }
    }

    $totalPhotoPrice = $config['price_photo'] * $data['includePhotos'];

    //round payment up
    //we are not going use the total booking price that will include photos entered by reception
    $totalPayment = $totalBookingPrice + $totalPhotoPrice;

    if (($totalPayment / 500) % 2) $totalPayment += 500;//if the payment is has 500 at the end, add 500 so that the machine does not get confused
    $_SESSION['totalPayment'] = $totalPayment;

    $data['selfCheckInPaidAmount'] = $totalPayment;
    if ($totalPayment == 0) { //skip payment
        hidePagesFromPageSequence(array('transaction'));

    } else {
        //TODO reenable when adding payments back
        if ($data['includePhotos'] && isset($data['bookingId'])) {
            //add a photos purchase to the booking...
            //TODO It would makes sense to move this to last step as if the person changes their mind, this will not update
            mysql_query("UPDATE customerregs1 SET photos_qty = photos_qty + {$data['includePhotos']}, photos = {$config['price_photo']} WHERE CustomerRegID = {$data['bookingId']}");
        }
        $paymentUrl = "transaction.php?amount=$totalPayment";
        header("Location: $paymentUrl");
        die();

    }

} else if ($pageName == "buyTShirts") {
    $totalPrice = 0;

    if ($_POST['includeTShirt']) {
        $_SESSION['totalPayment'] = $_POST['includeTShirtPrice'];
        header("Location: transaction.php?amount={$_POST['includeTShirtPrice']}");
        die();

    } else {
        header("Location: selfCheckIn.php");//restart
        die();

    }

} else if ($pageName == "signature") {
    //if this is a walkin booking, save it to the customer bookings table as it will not have been saved yet
    if ($data['bookingType'] == 'walkInBooking') {
        $data['NoOfJump'] = 1;
        $data['bookingId'] = saveBookingDataFromWaiverData($data);
    } else {
        $preBookingData = getBookingInfo($data['bookingId']);
        $data['BookingTime'] = $preBookingData[0]['BookingTime'];
        $data['BookingDate'] = $preBookingData[0]['BookingDate'];

        //check the customer in
        $query = "UPDATE customerregs1 SET Checked = 1 WHERE CustomerRegID = {$data['bookingId']}";
        mysql_query($query);

        if($data['includePhotos']) {
            //add a photos purchase to the booking...
            //daily view thinks a 0 photo quantity with a price is still a purchase so set the price to zero if there is no purchase
            $photoPrice = ($data['includePhotos'] ? $data['price_photo'] : 0 );
            $query = "UPDATE customerregs1 SET photos_qty = photos_qty + {$data['includePhotos']}, photos = photos + $photoPrice WHERE CustomerRegID = {$data['bookingId']}";
            mysql_query($query);
        }
    }

    setBookingPaid($data['bookingId']);//record booking as paid

    //Assume that the total price paid is the complete booking price
    //TODO if waiverInsertId is null the image will not save properly
    $waiverInsertId = saveWaiverData($data);

    //TODO consider writing the raw image data to a file just in case there is an issue saving
    //TODO send out an email if the image write fails
    $fileName = $waiverInsertId;

    //TODO we could insert the filename in to the database as sig_img
    //mysql_query("SET NAMES sjis;");
    //$query =  "UPDATE waivers SET sig_img= \"$fileName.jpeg\" WHERE bid=$waiverInsertId";
    //mysql_query($query);

    $path = "../uploads/signatures/$fileName.jpg";
    //$path = $_SERVER['DOCUMENT_ROOT'] . "/../private/waiverSignatures/$fileName.jpg";
    $waiverImageFileHandle = fopen($path, "w");
    $image = str_replace("data:image/jpeg;base64,", "", $data['img']);//get rid of mime data as this invalidates the file
    $image = base64_decode($image);
    fputs($waiverImageFileHandle, $image);
    fclose($waiverImageFileHandle);

    unset($waiverImageFileHandle);
} else if ($pageName == "photosPurchaseCompleted") {
    savePhotoPurchase($data);
    $_SESSION['data'] = null;
} else if ($pageName == 'openJumps') {
    $_POST['bookingDateAndTime'] = substr($_POST['bookingDateAndTime'], 0, -3);
    $bookingDateAndTime = DateTime::createFromFormat("Y-m-d H:i", ($_POST['bookingDateAndTime']));
    $data['BookingDate'] = $bookingDateAndTime->format("Y-m-d");
    $data['BookingTime'] = $bookingDateAndTime->format("H:i A");
} else if ($pageName == "weight") {
    /*
    if ($action == "refresh_weight") {
        header("Location: 13_3_ready.php?action=refresh_weight");//
        die();
        //call the script to end/start the fast polling of weights

    }
    */
} else if ($pageName == 'transaction') {
    //delete the transaction page from the sequence
    hidePagesFromPageSequence(array('transaction', 'buyPhotos', 'buyTShirts','jumpType'));
}

//TODO this should be the else statement
//if we specify a next page url, go to it, or else go to the next page in the sequence
if (isset($_GET['nextPageUrl'])) {
    goToPageWithUrl(getNextStepUrl($pageName));
} else goToNextStep();

