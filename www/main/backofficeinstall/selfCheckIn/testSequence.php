<?php
require_once("../includes/application_top.php");
//set the page sequence to scales, jumptype payment, finish
//set up the users information to be something test
//set up a test page sequence
$_SESSION['data']['pageSequence'] = [
    ["pageName" => "selfCheckIn", "pageUrl" => "selfCheckIn.php", "parameters" => "", "hidden" => false],
    ["pageName" => "jumpType", "pageUrl" => "03_jumpType.php", "parameters" => "", "hidden" => false],
    ["pageName" => "buyPhotos", "pageUrl" => "02_buyPhotos.php", "parameters" => "", "hidden" => false],
    ["pageName" => "transaction", "pageUrl" => "transaction.php", "parameters" => "", "hidden" => false],
    ["pageName" => "signature", "pageUrl" => "14_signature.php", "parameters" => "", "hidden" => false],
    ["pageName" => "exit", "pageUrl" => "15_exit.php", "parameters" => "", "hidden" => false]
];

//set initial user data
$_SESSION['data']['BookingDate']    = date("Y-m-d");
$_SESSION['data']['BookingTime']    = "16:30";
$_SESSION['data']['bookingType']    = "walkInBooking";
$_SESSION['data']['lastname']       = "TEST";
$_SESSION['data']['firstname']      = "TEST";
$_SESSION['data']['prefecture']     = "TEST";
$_SESSION['data']['phone_number']   = "123234345";
$_SESSION['data']['email']          = "test@bungyjapan.com";

header("location: 03_jumpType.php");//skip the start page for now
