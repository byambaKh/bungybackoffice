<?php
$pageName = "exit";
require_once('selfCheckInHeader.php')
?>
<script>
    //setTimeout(function () { submitForm("action=finishWaiver"); }, 10000);
</script>
<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">

    <div id="step17">
        <div class="header-container">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en">Your weight falls outside of the accepted range. Please speak to reception staff.</span>
					<span class="lang_ja">体重制限外です。スタッフをお呼びください。</span>
                </h1>
            </div>
        </div>
        <div id="enter">
            <button type="button" onclick="document.location.href='13_4_weight_websocket.php';">
                <span style="width: 100%;" class="lang_en" style="font-size: 70px;">&lt;&lt;Back</span>
                <span style="width: 100%;" class="lang_ja" style="font-size: 70px;">&lt;&lt;戻る</span>
            </button>
        </div>
    </div>
</form>
<script>
    var pageName = "exit";
    $(document).ready(function() {});
</script>
<script src="/js/reception.js" type="text/javascript"></script>

<?php require_once('selfCheckInFooter.php'); ?>

