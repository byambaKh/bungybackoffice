<?
/*
Code to manipulate bookings
A group booking can be represented as a series of rows of bookings

More simply, it can be represented as a the first row of a booking
and an array of the Rates, NoOfJumps, etc in the other rows that are part of the group booking

*/
class BookingCollection
{
	private $bookingRows;
	private $bookingId;

	function __construct($id)
	{
		$this->$id = $id;
		$this->getBookingForId($id);
	}

	function getBookingForId($id)
	{
		$query = "SELECT * FROM customerregs1 WHERE CustomerRegID = $id"; 
		//query for rows
		$result = queryForRows($query);
		$this->bookingRows = $result;
	}

	//from functions getTotalBookingPrice
	function getBookingTotalPrice()
	{
		$bookingId = $this->bookingId;
		$query = "
				SELECT
					IF(selfCheckInPaid, 0, SUM(NoOfJump * Rate + `RateToPay` * RateToPayQty + photos)) AS total
						FROM customerregs1
					WHERE (GroupBooking = $bookingId OR CustomerRegId = $bookingId);
				";
		return queryForRows($query)[0]['total'];
	}

	function getDefaultPrice()
	{
		global $config;
		return $config['first_jump_rate'];
	}

	function getBookingPrices()
	{
		$query = 
			"SELECT Rate
				FROM customerregs1 
			WHERE GroupBooking = {$this->bookingId} 
				OR CustomerRegID = {$this->bookingId}
			GROUP BY Rate";
		$results = queryForRow($query);
		return $results;
	}

	function convertToGroupBooking()
	{
	}

	function addPriceToRegularBooking($rate)
	{
		$bookingId = $this->bookingId;

		query("DROP TEMPORARY TABLE IF EXISTS temp_booking;");

		/*Make the booking a group booking, and decrement a jump from it*/
		query("UPDATE customerregs1 SET GroupBooking = 1,  NoOfJump = NoOfJump - 1, is_scm_created_group_booking = 1  WHERE CustomerRegID = {$bookingId};");

		/*Insert row in to a temp table*/
		query("CREATE TEMPORARY TABLE temp_booking SELECT * FROM customerregs1 WHERE CustomerRegID = {$bookingId};");

		/*Set group booking id, create a single jump booking*/
		query("UPDATE temp_booking SET CustomerRegID = NULL, GroupBooking = {$bookingId}, NoOfJump = 1, Rate = {$rate};");

		/*Copy back to the booking table*/
		query("INSERT INTO customerregs1 SELECT * FROM temp_booking;");

		/*Drop the table*/
		query("DROP TEMPORARY TABLE IF EXISTS temp_booking;");
	}

	function findFirstRowWithPriceAndTwoOrMoreJumps($rate)
	{
		$query = "
			SELECT CustomerRegId 
				FROM customerregs1 
				WHERE GroupBooking = {$this->bookingId} 
				OR CustomerRegId = {$this->bookingId}";
	}

	function addPriceToGroupBooking()
	{
		//look at the booking
		$regularPrice = $this->getDefaultPrice();
		//find the first regular priced rows
        //????
        $regularId = NULL;
		//find the first row that is non-regular priced
        //????
        $nonRegularId = NULL;
			//if there is one, 
				//decrement jumps in the regular row
				//increment jumps in the non-regular row
			//if there is none
				//decrement the regular priced row
				//insert a new row of non regular price


		//Alternative idea, how about splitting a group booking in to single rows of 1 jump
		//change the price as needed
	}

	function addPriceToBooking($price)
	{
		//if regular booking 
			//$this->addPriceToRegularBooking($price)
		//if group
			//addPriceToGroup
	}

	function reloadBooking()
	{
	}
}
