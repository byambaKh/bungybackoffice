<?php
$pageName = "bookingName";
require_once('selfCheckInHeader.php');
//require '../includes/application_top.php';
//echo Html::head("Self Check-In", array("reception.css", '/Waiver/stylesheet.css', 'selfCheckIn.css'), array());

?>
<script>
    var machineName = "<?= getRemoteHostName() ?>";
</script>
<!--<script src="js/qr_code.js"></script>-->
<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">
    <input type="hidden" name="bookingId" value="<?php echo $pageName ?>">

    <div id="step2">
        <div class="header-container">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en">NAME OF BOOKING</span>
                    <span class="lang_ja">代表者様のお名前</span>
                </h1>

                <h2>
                    <span
                        class="lang_en">Please select the name in which your booking was made and then press [Continue].</span>
                    <span class="lang_ja">をタッチして、「次へ」お押ししてください。</span>
                </h2>
            </div>
            <div class="refresh">
                <button>
                    <span class="lang_en">refresh</span>
                    <span class="lang_ja">更新</span>
                </button>
            </div>
        </div>
        <div id="names-container">
            <div id="names-container-child" class="scrollable">
                <?php if ($pc_version == 0) { ?>
                    <ul id="booking-list">
                    </ul>
                <?php } else { ?>
                    <select id="booking-list" multiple="multiple"></select>
                <?php }; ?>
            </div>
            <!--select name="bid" size="6" id="booking-id" style="width: 850px; height: 530px;">
              <option>test1</option>
              <option>test 2</option>
            </select-->
        </div>
        <div id="bottom-container">
            <button>
                <span class="lang_en">continue</span>
                <span class="lang_ja">次へ</span>
            </button>
        </div>
        <div class="back">
            <button>
                <span class="lang_en">&lt;&lt;BACK</span>
                <span class="lang_ja">&lt;&lt;戻る</span>
            </button>
        </div>
    </div>
</form>

<script>
    var pageName = "bookingName";

    $(document).ready(function () {
        fill_names();
        $('#step2 .refresh button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            return fill_names();
        });
        $('#step2 #bottom-container button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            var selected = false;
            if (pc_version == 0) {
                $('#step2 li').each(function () {
                    if ($(this).hasClass('selected')) selected = $(this).prop('id');
                    console.log("PC Version");
                });
            } else {
                selected = $("#step2 select").val();
                if (typeof selected == 'undefined') {
                    selected = false;
                }
                console.log(selected);
            }
            ;
            if (selected) {
                //save_var('booking_id', selected);
                document.waiverForm.bookingId.value = selected;
                submitForm();
                //saveWaiverDataToSessionAndGoToNextStep({booking_id: selected});
            } else {
                show_info("bookingname");
                save_var('booking_id', 'null');
            };
        });


        /*
        setInterval(function() {
           hideNonQrBookings(90344);
        }, 1000);
        */

    });
</script>
<? require_once('selfCheckInFooter.php'); ?>
