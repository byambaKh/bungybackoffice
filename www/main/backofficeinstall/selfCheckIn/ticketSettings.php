<?php
require '../includes/application_top.php';
echo Html::head("Bungy Japan - Ticket Panel Settings", array("/Waiver/css/stylesheet.css", "reception.css"), array("reception.js", "jquery-1.11.1.js"));

$saveSettings = $_GET['saveSettings'];

//Load settings from the database if they exist
$loadedResults = mysql_query('SELECT * FROM settings WHERE site_id = ' . CURRENT_SITE_ID . ' AND key_name = "printerTicketAndCertificate"');
$settingsRow = mysql_fetch_assoc($loadedResults);
$savedPrintersArray = explode("\n", $settingsRow['value']);

$certificatePrinter = $ticketPrinter = '';//set them to an empty string if there are not results

$findPrintersResultsCount = mysql_num_rows($loadedResults);

if ($findPrintersResultsCount) {
    $certificatePrinter = trim($savedPrintersArray[0]);
    $ticketPrinter = trim($savedPrintersArray[1]);
}

//Save new settings if they have been submitted
if ($saveSettings && isset($_POST['certificatePrinter']) && $_POST['ticketPrinter']) {
    $saveCertificatePrinter = $_POST['certificatePrinter'];
    $saveTicketPrinter = $_POST['ticketPrinter'];
    //save the settings

    //if setting exists..
    if ($findPrintersResultsCount == 1) { //update the entry if a key already exists
        $query = 'UPDATE settings SET value = "' . mysql_escape_string($saveCertificatePrinter) . "\n" . mysql_escape_string($saveTicketPrinter) . '" WHERE site_id = ' . CURRENT_SITE_ID . ' AND key_name = "printerTicketAndCertificate"';
        mysql_query($query);
    } else if ($findPrintersResultsCount == 0) {//nothing returned from the database so save a new setting
        $query = 'INSERT INTO settings (value, site_id, key_name) VALUES ("' . mysql_escape_string($saveCertificatePrinter) . "\n" . mysql_escape_string($saveTicketPrinter) . '", ' . CURRENT_SITE_ID . ', "printerTicketAndCertificate")';
        mysql_query($query);
    }

    $certificatePrinter = $saveCertificatePrinter;
    $ticketPrinter = $saveTicketPrinter;
}

?>
<h3><a href="selfCheckIn.php">&lt;Back</a></h3>
<form action="ticketSettings.php?saveSettings=1" method="post">
    <h2>Certificate Printer</h2>
    <select id="certificatePrinterSelect" name="certificatePrinter"> </select>

    <h2>Ticket Printer</h2>
    <select id="ticketPrinterSelect" name="ticketPrinter"> </select> <br><br>
    <?php echo Html::inputPageButton("testPrint", "", "300px", "100px", "redButton", "testPrint"); ?>
    <?php echo Html::inputPageButton("Save Settings", "submit();", "300px", "100px", "redButton newLine"); ?>
</form>


<script>
    function createPrinterDropDown(savedPrinter) {
        var printers = jsPrintSetup.getPrintersList();
        //console.log(printers);

        var printerArray = printers.split(",");
        //console.log(printerArray);

        var selectHtmlString = '';

        for (var printerIndex = 0; printerIndex < printerArray.length; printerIndex++) {
            //(redirected 2) seems to indicate that the printer is installed but not connected
            //so we filter those out as they are not options
            var printerString = printerArray[printerIndex].trim();
            if (printerString.indexOf("(redirected 2)") == -1)
                if (printerString == savedPrinter)//select the selected option
                    selectHtmlString = selectHtmlString + "<option value='" + printerString + "'selected>" + printerString + "</option>\n";
                else
                    selectHtmlString = selectHtmlString + "<option value='" + printerString + "'>" + printerString + "</option>\n";
        }

        return selectHtmlString;
    }

    $(document).ready(function () {
        var ticketPrinter = '<?php echo $ticketPrinter?>';
        var certificatePrinter = '<?php echo $certificatePrinter?>';

        document.getElementById('certificatePrinterSelect').innerHTML = createPrinterDropDown("<?php echo $certificatePrinter;?>");
        document.getElementById('ticketPrinterSelect').innerHTML = createPrinterDropDown("<?php echo $ticketPrinter;?>");

        $("#testPrint").click(function () {
            testPrint(ticketPrinter, certificatePrinter);
            console.log('TEST' + ' ' + ticketPrinter + ' ' + certificatePrinter);
        });
        //populate the dropdown with printer names
        //select the one that matches what is stored in the database
    });

</script>

<?php
require_once("../includes/pageParts/standardFooter.php");
?>
