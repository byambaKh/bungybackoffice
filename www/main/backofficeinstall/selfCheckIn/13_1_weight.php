<?php
$pageName = "weight";
require_once('settings.php'); //Get the printer settings
require_once('jcm/config.php');//Get JCM settings
$hostSettings = loadHostSettings($host)[0];//load the settings for a particular host
$enableScales = is_array($hostSettings['settings']) && ($hostSettings['settings']['scales'] == 1);
$action = isset($_GET['action']) ? $_GET['action'] : '';
require_once('selfCheckInHeader.php');

error_reporting(E_ALL);
ini_set('display_errors', 'On');

$enableScales = true;
//$_SESSION['data']['wasWeighed'] = false;
if ($enableScales) {
    //Is there a new scales measurement? if action == refresh-weight
    //require the scaletest with new_weight
    //How do we know?
    //The user presses the get weight button
    //scalesjar puts the weight in
    //if action = refresh-weight
    //get the weight again
    if ($action == '') {//no action so set a new weight, called on first page load
        //set new weight to the file so that the scalereader.jar starts fast polling
        $action = "new_weight";
        $silent = true;
        require_once("../scaletest.php");//TODO set hostname

    } else if ($action == 'refresh_weight') {
        //echo "Refresh Weight";
        $action = "new_weight";
        $silent = true;
        require_once("../scaletest.php");//TODO set hostname
        //we don't know how long it takes to write the file to the server
        //this file could return before the file is the file is ready
        //if the filesystem takes longer than it takes to write the time it takes to read the file
        //$fileContents will be null

        $i = 0;
        $didLoop = "Loop Failed";
        $timestart = microtime(true);
        $fileContents = file_get_contents("/home/bungyjapan/private/weight.dat");
        if($fileContents == "new_weight") {
            while($fileContents == "new_weight") {
                $fileContents = file_get_contents("/home/bungyjapan/private/weight.dat");
                usleep(100000);
                $i++;
                $didLoop = "Loop Success";
            }
        } else {
            die("Weight sync error.");
        }
        $timeend = microtime(true);
        $delta = $timeend - $timestart;
        //echo "Took $delta seconds to complete and looped $i times. $didLoop";
        /*
        echo file_get_contents("/home/bungyjapan/private/weight.dat")."<br>";
        $i = 0;
        $timestart = microtime(true);
        do {
            usleep(1000);//it fails if we dont do this... why?
            $fileContents = file_get_contents("/home/bungyjapan/private/weight.dat");
            $i++;
        } while ($fileContents == 'new_weight');
        $timeend = microtime(true);

        $delta = $timeend - $timestart;
        echo "Took $delta seconds to complete and looped $i times.";
        //I have seen this take up to 4 seconds

        //after the file has been used, remove it.
        //This rm does not this does not work for unknown reasons
        echo unlink("/home/bungyjapan/private/weight.dat");
        */

        //echo $fileContents;
        if (($fileContents != 'new_weight')) {//This is
            //echo "Weight Set";
            $_SESSION['data']['weight'] = $fileContents;
            $_SESSION['data']['wasWeighed'] = true;

        } else {//No weight has been set so start again
            //header("Location: 13_1_weight.php?action=new_weight");
            //echo "die";
            //echo $fileContents; die();

        }

        /*
        //TODO check to make sure the weight is current and not old
        $updated = filemtime("/home/bungyjapan/private/weight.dat");
        $current = ($updated >= time() - 4);
        */

    } else {

    }
}
$weight = isset($_SESSION['data']['weight']) ? $_SESSION['data']['weight'] : 0;
//if present a video of the weight page
//use a dummy for now
//Once the user has stepped on and press "weight" get weight
//hide button
//hide video
//ask to try again
//display continue button
?>

<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">
    <input type="hidden" name="weight" value="">
    <input type="hidden" name="weightMeasured" value="">

    <div id="step00">
        <div class="header-container">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en">Weight</span>
                    <span class="lang_ja">体重</span>
                </h1>

                <h2>
                    <span class="lang_en">Please step on the scale.</span>
                    <span class="lang_ja">体重計に乗って下さい</span>
                </h2>
            </div>
        </div>

        <div id="first-weigh" class="">
            <!--<img src="img/footprint.png" onclick="javascript:window.open('getweight.php', 'getweight', 'height=200,width=400');">-->
            <!--<button style="width:415px; height:415px;"onclick="javascript:window.open('getweight.php', 'getweight', 'height=200,width=400');">
                <img src="img/footprintBlack.png">
            </button>-->

            <!-- <iframe src="getweight.php?PHPSESSID=<?php //echo urlencode(session_id())?>" height="400" width="400">

		</iframe>-->
            <style>
                .centeredCleared {
                    clear: both;
                    margin-left: auto;
                    margin-right: auto;
                }
            </style>
            <?php if (!isset($_SESSION['data']['wasWeighed']) || !$_SESSION['data']['wasWeighed']): ?>
                <video class="centeredCleared" id="scaleInstructions" width="600" height="400" autoplay loop>
                    <source src="/videos/cars.mp4" type="video/mp4">
                    <source src="/videos/cars.webm" type="video/webm">
                    Your video does not support the video tag.
                </video>
                <br>

                <button id="refresh_weight" class="centeredCleared" style="height: 120px; width: 650px; color: white;">
                    <span class="lang_en">Weigh Now</span>
                    <span class="lang_ja">次へ</span>
                </button>
            <?php endif ?>
        </div>

        <div class="continue" id="walkinBooking">
            <?php if (isset($_SESSION['data']['wasWeighed']) && $_SESSION['data']['wasWeighed']): ?>
                <button class="centeredCleared" id="weightNow" style="height: 360px; width: 350px; color: white;" onclick="window.location.assign('selfCheckInController.php?action=refresh_weight');"><?php echo "-$weight -" ?>
                    <br>
                    <span class="lang_en">Weigh Again?</span>
                    <span class="lang_ja" style="font-size: 65px; line-height: 20px;">もう一度 計りますか？</span>
                </button>

                <br>

                <button class="centeredCleared" id="continue" style="clear:both; margin-left: auto; margin-right: auto;">
                    <span class="lang_en">Continue</span>
                    <span class="lang_ja">次へ</span>
                </button>
            <?php endif ?>
        </div>
        <a href="13_2_weight.php" style="font-size: 20px; color: #444;">*</a>
    </div>
</form>

<script>
    var pageName = "weight";
    $(document).ready(function () {
        $('#step00 #walkinBooking #continue').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            document.waiverForm.weight.value = '<?php echo $weight?>';
            submitForm();
        });

        $('#step00 #walkinBooking #weightNow').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            document.waiverForm.weight.value = '<?php echo $weight?>';
            document.waiverForm.weightMeasured.value = 1;
            document.waiverForm.action = 'controllerSelfCheckIn.php';
            submitForm("action=refresh_weight");
        });

        $('#refresh_weight').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            submitForm("action=refresh_weight");
        });
    });

    <?php
    if(!$enableScales){
        $_SESSION['data']['weight'] = 70;
        $_SESSION['data']['wasWeighed'] = true;
    }
    ?>
</script>
<?php require_once('selfCheckInFooter.php'); ?>
