<?php
$pageName = "bookOrWalk";
require_once('selfCheckInHeader.php');
require_once('jcm/config.php');
//echo "$host";
?>
<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">
    <input type="hidden" name="bookingType" value="x">

    <div id="step00">
        <div class="header-container">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en">Booking Type</span>
                    <span class="lang_ja">ご予約の方？</span>
                </h1>
                <!--<h2>
                  <span class="lang_en">Please select the name in which your booking was made and then press [Continue].</span>
                  <span class="lang_ja">をタッチして、「次へ」お押ししてください。</span>
                </h2>-->
            </div>
        </div>

        <div id="preBooking" class="bookingOption">
            <button>
                <span class="lang_en">I have a booking</span>
                <span class="lang_ja">予約をしている</span>
            </button>
        </div>

        <br>

        <div id="walkInBooking" class="bookingOption">
            <button>
                <span class="lang_en">I don't have a booking</span>
                <span class="lang_ja">予約をしていない</span>
            </button>
        </div>

        <br>

        <div id="tShirtsOnly" class="bookingOption">
            <button>
                <span class="lang_en">Bungy T-Shirt</span>
                <span class="lang_ja">バンジーTシャツ</span>
            </button>
        </div>
        <br>
        <div class="back">
            <button>
                <span class="lang_en">&lt;&lt;BACK</span>
                <span class="lang_ja">&lt;&lt;戻る</span>
            </button>
        </div>
    </div>
</form>
<script>
    var pageName = "bookOrWalk";

    $(document).ready(function () {
        e.preventDefault();
        document.waiverForm.bookingType.value = "jcmDebug";
        submitForm();
    });
</script>

<?php require_once('selfCheckInFooter.php'); ?>
