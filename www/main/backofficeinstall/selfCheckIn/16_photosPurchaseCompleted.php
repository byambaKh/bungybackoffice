<?php
$pageName = "photosPurchaseCompleted";
require_once('selfCheckInHeader.php')
?>
<script>
    setTimeout(function () {
        submitForm();
    }, 10000);
</script>
<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">

    <div id="step15">
        <div class="header-container">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en">Thank you. Photos purchase complete!</span>
                    <span class="lang_ja">ありがとうございました。写真購入は完了しました！</span>
                </h1>

                <h2>
                    <span class="lang_en">Please collect your photo ticket.</span>
					<span class="lang_ja">写真券を集めてください</span>
                </h2>
            </div>
        </div>
        <div class="restart-container">
            <!--div>This form will restart in <span id="restart-in">5</span> seconds</div-->
            <div id="step15progress"></div>
            <?php

            //TODO save the photos to the database
            //
            ?>
            <iframe style="display:none" name="photoTicket" id="photoTicket" src="<?php  ?>"></iframe>
        </div>
    </div>
</form>
<script>
    var pageName = "exit";
    $(document).ready(function () {
        //print the tickets
        //printTicket('<?php //echo $ticketPrinter;?>', '<?php //echo $certificatePrinter;?>');
        //printForDemo();
        //clearSessionWaiverData();
    });
</script>
<script src="/js/reception.js" type="text/javascript"></script>

<?php require_once('selfCheckInFooter.php'); ?>
