<?php
$pageName = "exit";
require_once('selfCheckInHeader.php');

//load the printer names
$loadedResults = mysql_query('SELECT * FROM settings WHERE site_id = ' . CURRENT_SITE_ID . ' AND key_name = "printerTicketAndCertificate"');
$settingsRow = mysql_fetch_assoc($loadedResults);
$savedPrintersArray = explode("\n", $settingsRow['value']);

$certificatePrinter = $ticketPrinter = '';//set them to an empty string if there are not results

$findPrintersResultsCount = mysql_num_rows($loadedResults);

if ($findPrintersResultsCount) {
    $certificatePrinter = trim($savedPrintersArray[0]);
    $ticketPrinter = trim($savedPrintersArray[1]);
}
?>
<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">

    <div id="step15">
        <div class="header-container">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en">Thank you. Registration complete!</span>
                    <span class="lang_ja">登録が完了しました。</span>
                </h1>

                <h2>
                    <span class="lang_en">Please give your name to the reception staff.</span>
		<span class="lang_ja">お客様の氏名をスタッフまで<br/>
お申し付けください。</span>
                </h2>
            </div>
        </div>
        <div class="restart-container">
            <!--div>This form will restart in <span id="restart-in">5</span> seconds</div-->
            <div id="step15progress"></div>
            <?php
            $date = urlencode(date("Y-m-d"));
            $signatureUrl = urlencode('');

            //$time 	  urlencode(      = $_SESSION['data']['time']);//for a walkin is time know, it is known for a preBooking
            //$weight 	= urlencode($_SESSION['data']['weight']);//need to get from scales
            $bookingType = "WalkIn";
            $jumpNumber = "34";//need to get from database
            $photoNumber = "12";
            $cordColor = "Yellow";
            $groupOfNumber = "7";
            $firstName = "Tim";
            $lastName = "Smith";
            $fullName = urlencode($firstName . " " . $lastName);

            //TODO temp placeholder variables that I must get
            $time = "13:30";
            $weight = "70";
            $cordColor = "Yellow";
            $groupOfNumber = 4;
            $jumpNumber = 47;

            $certificatePrinter = urlencode($certificatePrinter);
            $ticketPrinter = urlencode($ticketPrinter);
            ?>
            <iframe style="display:none" class="hiddenCertificateFrame" name="certificate"
                    src="<?php echo "../selfCheckIn/certificate.php?fullName=$fullName&date=$date&signatureUrl=$signatureUrl&printer=$certificatePrinter" ?>"></iframe>
            <iframe style="display:none" name="ticketmini" id="ticketmini"
                    src="<?php echo "../selfCheckIn/ticketLayout.php?time=$time&weight=$weight&bookingType=$bookingType&photoNumber=$photoNumber&jumpNumber=$jumpNumber&groupOfNumber=$groupOfNumber&firstName=$firstName&lastName=$lastName&cordColor=$cordColor&printer=$ticketPrinter" ?>"></iframe>

        </div>
    </div>
</form>
<script>
    var pageName = "exit";
    $(document).ready(function () {
        //printTicket('<?php //echo $ticketPrinter;?>', '<?php //echo $certificatePrinter;?>');
        //printForDemo();
        //clearSessionWaiverData();

    });
</script>
<script src="/js/reception.js" type="text/javascript"></script>

<?php require_once('selfCheckInFooter.php'); ?>
