<?php
$pageName = "signature";
require_once('selfCheckInHeader.php')
?>
<form action="" name="waiverForm" method="post">
    <input type="hidden" name="pageName" value="<?php echo $pageName ?>">
    <input type="hidden" name="img" value="">

    <div id="step14">
        <div class="header-container">
            <div class="head-image-container">
                <h1>
                    <span class="lang_en">Please sign your name using your finger</span>
                    <span class="lang_ja">画面に直接ご署名ください。</span>
                </h1>
            </div>
        </div>
        <div id="sig-container">
            <canvas id="waiver-signature" height="600" width="1000"/>
        </div>
        <div id="actions-container">
            <div id="del-button">
                <button><span class="lang_en">DELETE</span><span class="lang_ja">訂正</span></button>
            </div>
            <div id="enter-button">
                <button><span class="lang_en">ENTER</span><span class="lang_ja">次へ</span></button>
            </div>
        </div>

        <div class="back">
            <button>
                <span class="lang_en">&lt;&lt;BACK</span>
                <span class="lang_ja">&lt;&lt;戻る</span>
            </button>
        </div>
    </div>
</form>

<!--<script src="/js/reception.js" type="text/javascript"></script>--><!--This is not used-->
<script>
    var pageName = "signature";
    function clearCanvas(){
        var c = $('#waiver-signature')[0];
        var ctx = c.getContext('2d');
        var rect = document.getElementById('waiver-signature').getBoundingClientRect();
        ctx.fillStyle = 'white';
        ctx.beginPath();
        ctx.fillRect(0, 0, rect.width, rect.height);
        ctx.stroke();
        document.bj_signature = null;
    }

    $(document).ready(function () {
        //print the tickets
        //printTicket('<?php //echo $ticketPrinter;?>', '<?php //echo $certificatePrinter;?>');
        //printForDemo();

        $('#waiver-signature').bind('movestart MSPointerDown pointerdown', bjmovestart);
        $('#waiver-signature').bind('move MSPointerMove pointermove', bjmove);
        $('#waiver-signature').bind('moveend MSPointerUp pointerup', bjmoveend);

        clearCanvas();//if the canvas is not cleared, the background is black

        $('#step14 #del-button button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            clearCanvas();
        });

        $('#step14 #enter-button button').bind('click touchend MSPointerUp pointerup', function (e) {
            e.preventDefault();
            if (typeof document.bj_signature == 'undefined' || document.bj_signature === null) {
                show_info('validsignature');
                return false;
            }
            var c = $('#waiver-signature')[0];
            var img = c.toDataURL('image/jpeg', 0.8);
            document.waiverForm.img.value = img;
            $('#step14 #enter-button button').prop('disabled', true);
            submitForm();
            //saveWaiverDataToSession({sig_img: img});
            //save_data();
            //goToNextStep();
        });

    });
</script>
<?php require_once('selfCheckInFooter.php'); ?>
