<?php
include "../includes/application_top.php";

$cDate = date("Y-m-d");

$_SESSION['jumpDate'] = $_POST['jumpDate'];


function display_db_query($sql_select, $conn, $header_bool, $table_params)
{
    $result_select = mysql_query($sql_select, $conn)
    or die("display_db_query:" . mysql_error());
    // find out the number of columns in result
    $column_count = mysql_num_fields($result_select)
    or die("display_db_query:" . mysql_error());

    // Here the table attributes from the $table_params variable are added
    print("<TABLE $table_params align='center' width='400px'>");
    // optionally print a bold header at top of table
    if ($header_bool) {

        print("<TR>");
        for ($column_num = 0; $column_num < $column_count; $column_num++) {
            $field_name = mysql_field_name($result_select, $column_num);
            print("<TH bgcolor='#87CEFA'>$field_name</TH>");
        }
        print("</TR>");
    }
    //Check ROW Count
    $num_rows = mysql_num_rows($result_select);
    if ($num_rows != 0) {


        while ($row = mysql_fetch_row($result_select)) {
            $fd = $row[2];
            //print $fd;
            print("<TR ALIGN=CENTER VALIGN=TOP>");
            for ($column_num = 0; $column_num < 2; $column_num++) {
                print("<TD style=valign='middle' >$row[$column_num]</TD>");
            }
            print("<TD style=valign='middle' >$fd</TD>");
            for ($column_num = 3; $column_num < $column_count; $column_num++) {
                print("<TD style=valign='middle' >$row[$column_num]</TD>");
            }
            print("</TR>");
        }

    }
    print("</TABLE>");

}

function display_db_table($tablename, $conn, $header_bool, $table_params)
{

    $chk = 1;
    if (is_null($_SESSION[jumpDate])) {
        $cDate = date("Y-m-d");
        //$sql_select_def = "SELECT customerlastname as 'Last Name', customerfirstname as 'First Name',
        //noofjump as 'Jumps', contactno as 'Contact No'".
        //" FROM customerregs1 ".
        //" WHERE bookingdate = '".$cDate."'".
        //" and Checked = '".$chk."'";

        $sql_select_def = "SELECT * " .
            " FROM customerregs1 " .
            " WHERE site_id = '" . CURRENT_SITE_ID . "' AND bookingdate = '" . $cDate . "'" .
            " and Checked = '" . $chk . "'";

        //echo $sql_select_def;
        display_db_query($sql_select_def, $conn, $header_bool, $table_params);
    } else {
        /*$sql_select = "SELECT customerlastname as 'Last Name', customerfirstname as 'First Name',
                  noofjump as 'Jumps', contactno as 'Contact No'".
                  " FROM customerregs1 ".
                  " WHERE bookingdate = '".$_SESSION[jumpDate]."'".
                  " and Checked = '".$chk."'";
        //echo $sql_select;
        display_db_query($sql_select, $conn, $header_bool, $table_params);*/
    }

}

?>
<html>
<head>
    <?php include 'includes/header_tags.php'; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
    <meta http-equiv="X-UA-Compatible" content="IE=8">

    <?php include '../includes/head_scripts.php'; ?>
    <script src="js/functions.js" type="text/javascript"></script>

    <title>Checked-IN Data View</title>
    <script type="text/javascript" charset=Shift_JIS>

        function OnSubmitForm() {
            if (document.getElementById('jumpDate')) {
                document.dailyForm.submit();
            }
        }
        function procBack() {
            if (document.getElementById('bck')) {
                //document.dailyForm.action = "index.php";
                //document.dailyForm.submit();
                //return true;
            }
        }

        $(function () {
            $mydate1 = $('#jumpDate').datepicker({
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd',
                altField: '#alternate',
                altFormat: 'yy-mm-dd',
                minDate: '0d'
            });
        });

    </script>
</head>
<body>
<?php include '../includes/main_menu.php'; ?>
<br>

<form name="dailyForm" method="post">
    <table align="center" width="400px">
        <tbody>
        <tr>
            <td>
                <?php
                $table = "table1";
                display_db_table($table, $conn, TRUE, "border='1'");
                ?>

            </td>
        </tr>
        <!--
        <tr>
          <td colspan="4">
            <input type="button" name="bck" id="bck" value="BACK" style="background-color:#FFFF66;border-style:ridge;border-color:#FF0000;" onclick="procBack();">
          </td>
        </tr>
         -->
        </tbody>
    </table>
</form>
</body>
</html>
