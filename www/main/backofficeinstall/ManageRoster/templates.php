<?php
	include ("../includes/application_top.php");

	$current_uid = '';
	$action_result = "";
	$record_type = 'roster_daily_templates';
	$record_template = array(
		array(
			'name'	=> 'id',
			'type'	=> 'primary',
			'title'	=> 'Template ID'
		),
		array(
			'name'	=> 'template_name',
			'type'	=> 'input',
			'title'	=> 'Template Name'
		),
		array(
			'name'	=> 'min_jumps',
			'type'	=> 'input',
			'title'	=> 'Min Jumps'
		),
		array(
			'name'	=> 'max_jumps',
			'type'	=> 'input',
			'title'	=> 'Max Jumps'
		),
		array(
			'name'	=> 'start_time',
			'type'	=> 'input',
			'title'	=> 'Start Time'
		),
		array(
			'name'	=> 'end_time',
			'type'	=> 'input',
			'title'	=> 'End Time'
		),
		array(
			'name'	=> 'template_color',
			'type'	=> 'input',
			'title'	=> 'Template Color'
		),
		array(
			'name'	=> 'person',
			'type'	=> 'input',
			'title'	=> 'Required Staff'
		),
	);
	$places_drop_down = BJHelper::getPlacesDropDown();
	$site_id = 0;
	if (CURRENT_SITE_ID > 0) {
		$site_id = CURRENT_SITE_ID;
	};
	if (isset($_GET['site_id']) && SYSTEM_SUBDOMAIN_REAL == 'MAIN') {
		$site_id = $_GET['site_id'];
	};
	if (isset($_POST['site_id']) && SYSTEM_SUBDOMAIN_REAL == 'MAIN') {
		$site_id = $_POST['site_id'];
	};

	if (!empty($_POST)) {
		switch (TRUE) {
			case isset($_POST['update']): 
			case isset($_POST['add']): 
				$type = 'insert';
				$parameters = '';
				if (!empty($_POST['id'])) {
					$type = 'update';
					$parameters = 'id=' . (int)$_POST['id'];
				};
				$action_result = $type;
				$data = $_POST['record'];
				$data['site_id'] = $site_id;
				db_perform($record_type, $data, $type, $parameters);
				break;
		};
	} else {
		switch (TRUE) {
			case array_key_exists('action', $_GET):
				if ($_GET['action'] == 'delete') {
					$id = (int)$_GET['id'];
					$sql = "DELETE FROM `$record_type` WHERE id = '$id'";
					mysql_query($sql);
					$id = '';
					$action_result = 'delete';
				} elseif ($_GET['action'] == 'edit') {
					$id = (int)$_GET['id'];
				};
				break;
		};
	};
	if (!empty($action_result)) {
		$_SESSION['result_message'] = $action_result;
		$params = '';
		if ($site_id > 0) {
			$params = '?site_id=' . $site_id;
		};
		Header("Location: " . $PHP_SELF . $params);
		die();
	};
	$info = array();
	if (!empty($id)) {
		$sql = "select * from `$record_type` WHERE id = ". (int)$id;
		$res = mysql_query($sql);
		if ($res) {
			$row = mysql_fetch_assoc($res);
			$info = $row;
			$site_id = $row['site_id'];
		};
	};


?>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">
<?php include '../includes/head_scripts.php'; ?>
<style>
.table-row1 {
	background-color: #CCCCFF;
}
.table-row2 {
	background-color: #CCFFCC;
}
#system-message {
	background-color: #55FF55;
	text-align: center;
	line-height: 150%;
}
.users-table-header {
	background-color: #fdd;
}
.data {
	text-align: center;
}
.table-actions {
	text-align: center;
	width: 100px;
}
</style>

<title>Bungy Japan :: Manage Roster :: Templates Managment</title>

</head>
<script type="text/javascript">
function deleteConfirm(id, name) {
	var answer = window.confirm('Are you sure to delete "'+name+'"?') 
	if (answer == true) {
		document.location = "<?php echo $PHP_SELF; ?>?action=delete&id=" + id;
	};
	return answer;
}
$(document).ready(function () {
	$('#site-id').change(function () {
		document.location = 'templates.php?site_id=' + $(this).val();
	});
});
</script>
<body>
<?php include '../includes/main_menu.php'; ?>
<br />
<?php
 if (array_key_exists('result_message', $_SESSION) && !empty($_SESSION['result_message'])) {
	echo "<div id='system-message'>";
	switch ($_SESSION['result_message']) {
		case "insert":
			$message = "Template added";
			break;
		case "update":
			$message = "Template updated";
			break;
		case "delete":
			$message = "Template deleted";
			break;
	};
	echo $message;
	echo "</div>";
	unset($_SESSION['result_message']);
 };
?>
<form method="post" name="uForm">
<?php include 'management_menu.php'; ?>
<table align="center" width="480px" bgcolor="#D3D3D3">
	<tbody>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500">Please select site:</td>
			<td align="left" bgcolor="#FFA500"><?php 
	echo draw_pull_down_menu('site_id', $places_drop_down, $site_id, 'id="site-id"');
?>
			</td>
		</tr>
	
		<tr>
			<td colspan="8" align="center" bgcolor="#FFA500"><b>Templates Management</b>
			</td>
		</tr>
<?php
	foreach ($record_template as $field) {
?>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500"><?php echo $field['title']; ?>:</td>
			<td align="left" bgcolor="#FFA500">
<?php
		$text = '';
		$value = (array_key_exists($field['name'], $info)) ? $info[$field['name']] : '';
		switch ($field['type']) {
			case 'primary':
				$text .= $value . '<input type="hidden" name="' . $field['name'] . '" value="' . $value . '">';
				break;
			default:
				$text .= '<input type="input" name="record[' . $field['name'] . ']" value="' . $value . '">';
		};
		echo $text;
?>
			</td>
		</tr>
<?php
	};
?>
		<tr>
			<td colspan="8" align="right" bgcolor="#FFA500">
<?php if ($info['id'] != '') { ?>
				<input type="submit" name="update" id="update" value="Update">
<?php } else { ?>
				<input type="submit" name="add" id="add" value="Add">
<?php }; ?>
				<input type="button" name="back" id="back" value="Back" onclick="document.location = '/';">
			</td>

			
		</tr>
	</tbody>
</table>

<table align="center" width="600" id="users-table">
<tr>
	<th class="user-access-type" colspan="7"><h2>Templates</h2></th>
</tr>
<tr class="users-table-header">
<?php foreach ($record_template as $field) { ?>
	<th class=""><?php echo $field['title']; ?></th>
<?php }; ?>
	<th class="users-table-actions">Manage</th>
</tr>

<?php
	$sql = "select * from $record_type WHERE site_id =' $site_id' order by max_jumps DESC, id asc";
	$res = mysql_query($sql);
	$i = 1;
	$at = '';
	while($row = mysql_fetch_assoc($res)) {
		$i = !$i;
?>
	<tr class="table-row<?php echo ($i+1); ?>">
<?php foreach ($record_template as $field) { 
		$s = array();
		$s[] = ($field['name'] == 'template_name' && array_key_exists('template_color', $row) && !empty($row['template_color'])) ? 'background-color: '.$row['template_color'].' !important;' : '';
		$s = (empty($s)) ? '' : 'style="' . implode(' ', $s) . '"';
?>
		<td class="data"<?php echo $s; ?>><?php echo $row[$field['name']]; ?></td>
<?php }; ?>
		<td class="table-actions">
			<a href="?action=edit&id=<?php echo $row['id']; ?>">Edit</a> | 
			<a class="delete" href="<?php echo $PHP_SELF; ?>?action=delete&id=<?php echo $row['id']; ?>" custom-id="<?php echo $row['id']; ?>" custom-name="<?php echo $row['name']; ?>">Delete</a><br>
			<a href="/Roster/daily_roster.php?mode=template&template_id=<?php echo $row['id']; ?>">Edit Template</a>
		</td>
	</tr>
<?php
	};
?>
</table>
<script>
$(document).ready(function (){
	$('.delete').click(function (event){
		event.preventDefault();
		if (deleteConfirm($(this).attr('custom-id'), $(this).attr('custom-name'))) {
			document.location = '<?php echo $PHP_SELF; ?>?action=delete&id=' + $(this).attr('custom-id');
		};
		return false;
	});
});
</script>

</form>
</body>
</html>
