<?php
	$user = new BJUser();
?>
<div style="width: 500px; margin-right: auto; margin-left: auto; text-align: center; margin-bottom: 20px;">
<?php echo showLink("index.php", 'Positions'); ?> | 
<?php echo showLink("functions.php", 'Functions'); ?> |
<?php echo showLink("templates.php", 'Templates'); ?> 
</div>
<?php 
	function showLink($url, $name) {
		$urlself = ($url == basename($_SERVER['PHP_SELF']));
		if (!$urlself) {
			echo "<a href='$url'>";
		};
		echo $name;
		if (!$urlself) {
			echo "</a>";
		};
	}
?>
