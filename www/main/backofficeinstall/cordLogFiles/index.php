<?
//require '../includes/application_top.php';
require '../includes/application_top_noAuth.php';
//require_once("../includes/pageParts/standardHeader.php");
echo Html::head(SYSTEM_SUBDOMAIN." - CordLog Files", array("main_menu.css", "companyCharter.css"));
?>
<title><?php echo SYSTEM_SUBDOMAIN; ?> CordLog Files</title>
<div>
	<img src="../img/index.jpg">
	<h1><?php echo SYSTEM_SUBDOMAIN; ?><br> CordLog Excel files</h1>

	<?
	echo Html::menuPageLink("Download Latest", "downloadFile.php?latest=true");
    echo Html::menuPageLink("Older Versions", "oldVersions.php");
    echo Html::menuPageLink("-", "#");
	echo Form::menuPageUpload("Upload", "uploadFile.php");
	echo Html::menuPageLink("Log Out", "/?logout=true");
	echo Html::menuPageLink("Back", "/") ;
	?>
</div>
<? require_once("../includes/pageParts/standardFooter.php");?>
