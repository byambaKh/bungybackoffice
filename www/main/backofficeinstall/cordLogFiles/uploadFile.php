<?
require_once('../includes/application_top.php');
?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="stylesheet.css">
</head>
<body>
<?
$temp = explode(".", $_FILES["file"]["name"]);
$originalName = $_FILES["file"]["name"];
$extension = end($temp);

$uploadDirectory = $_SERVER['DOCUMENT_ROOT'].'/uploads/cordlogs/';

if ($extension == "xls" || $extension == "xlsx") {
	if ($_FILES["file"]["error"] > 0) {
		//echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
	} else {
        $storageName = $user->getID()."_".date('YmdHis').".xls";//A unique name for storing the file
		move_uploaded_file($_FILES["file"]["tmp_name"], $uploadDirectory . $storageName);
        $user = new BJUser();

        $data = array(
            'originalName' => $originalName,
            'storageName' => $storageName,
            'filename' => 'CordLog_'.SYSTEM_SUBDOMAIN.'.'.$extension,
            'userId' => $user->getID(),
            'dateTime' => date('Y-m-d H:i:s'),
            'site_id' => CURRENT_SITE_ID,
            'category' => 'cordlog'
        );

        //$mail = new PHPMailer;
        //$mail->From = 'noreply@bungyjapan.com';
        //$mail->FromName = 'Company Charter';

        $recipients = [
            "beau@bungyjapan.com",            
            "dez@bungyjapan.com", 
            "byamba@bungyjapan.com",
        ];

        //foreach ($recipients as $recipient) {
            //$mail->addAddress($recipient); // Site Manager
        //}

        //$mail->addReplyTo('noreply@bungyjapan.com', 'Charter Update');
        //$mail->isHTML(true); // Set email format to HTML
        //$mail->CharSet = 'UTF-8';

        //$mail->Subject = "Updated Company Charter";
        //$mail->Body = "The Company charter has been updated. Please go to
        //<a href='http://main.bungyjapan.com/companyCharter/index.php'>Company Charter Page</a> and Download The Latest Version.";

        //$mail->send();

        db_perform('uploads', $data);

		echo "CordLog File Successfully Uploaded.<br> ";
		echo "<a href = 'index.php'>&lt;&lt; Back</a> ";
	}

} else {
	echo "Please upload only xls file. <br>";
    echo "<a href = 'index.php'>&lt;&lt; Back</a> ";
}
?> 
</body>
</html>

