<?php
include '../includes/application_top.php';
$record_type = 'invoices';
$current_time = time();
$d = date('Y-m', $current_time);
if (isset($_GET['date'])) {
    $current_time = mktime(0, 0, 0, substr($_GET['date'], 5, 2), 15, substr($_GET['date'], 0, 4));
} else {
    $current_time = mktime(0, 0, 0, substr($d, 5, 2), 15, substr($d, 0, 4));
};
if (date("m", $current_time) == "12" && date("m") != "12" && substr($_GET['date'], 0, 4) == date("Y")) {
    $current_time -= 365 * 24 * 60 * 60;
};
$d = date('Y-m', $current_time);
$d_last = date('Y-m', $current_time - 30 * 24 * 60 * 60);
$d_last = date('Y-', $current_time) . '01';
$agent = $_GET['agent'];
if (empty($agent)) {
    $agent = $_SESSION['myusername'];
};

$all_data = array();
$invoices = array();
$payments = array();
$places = BJHelper::getPlaces();
$total_fields = array('NoOfJump', 'TotalToPay', 'TotalToCollect', 'total_cancel', 'CancelFeeQTY');
$totals = array();
$totals_last = array();
foreach ($total_fields as $field) {
    $totals[$field] = 0;
    $totals_last[$field] = 0;
};
$total_payment = 0;
$all_invoices = array();
$all_payments = array();
foreach ($places as $place) {
    if (in_array($place['subdomain'], array('test'))) continue;

    $sql = "SELECT
            '" . ucfirst($place['subdomain']) . "' as site,
            CustomerRegID,
            BookingDate,
            RomajiName,
            IF(CollectPay = 'Offsite', NoOfJump, RateToPayQTY) as NoOfJump,
            IF(CollectPay = 'Offsite', Rate, RateToPay) as 'UnitCost',
            IF(CollectPay = 'Offsite', 0, RateToPay * RateToPayQTY) as 'TotalToPay',
            IF(CollectPay = 'Offsite', Rate * NoOfJump, 0) as 'TotalToCollect',
            IF(CancelFeeCollect = 'Offsite', CancelFee, 0) as CancelFee,
            IF(CancelFeeCollect = 'Offsite', CancelFeeQTY, 0) as CancelFeeQTY,
            IF(CancelFeeCollect = 'Offsite', CancelFeeCollect, 0) as CancelFeeCollect,
            IF(CancelFeeCollect = 'Offsite', CancelFee * CancelFeeQTY, 0) as total_cancel
            from customerregs1 as cr
            WHERE 
				BookingDate like '$d-%'
                AND site_id = '" . $place['id'] . "'
                AND Agent = '{$agent}'
			 AND (
                    (
                        /*Bookings made offsite that were completed via an agent*/
                        DeleteStatus = 0
                        AND Checked = 1
                        AND CollectPay = 'Offsite'
                        AND NoOfJump > 0
                    ) OR (
                        /*Bookings made Onsite that were completed via an agent*/
                        DeleteStatus = 0
                        AND Checked = 1
                        AND RateToPay > 0
                        AND RateToPayQTY > 0
                        AND CollectPay = 'Onsite'
                        AND NoOfJump > 0
                    ) OR (
                        /*partial cancellations through setting noOfJumps to zero in from dailyview checkin*/
                        CancelFeeCollect = 'Offsite'
                        AND NoOfJump = 0
                    )
                )
                
            ORDER BY BookingDate ASC, RomajiName ASC;";
    //echo $sql."<br>";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        $key = $row['bookingdate'] . ' ' . $domain . ' ' . $row['bookingtime'];
        if (!array_key_exists($key, $all_data)) {
            $all_data[$key] = array();
        };
        $all_data[$key][] = $row;
        foreach ($row as $field => $value) {
            if (in_array($field, $total_fields)) {
                if (!array_key_exists($field, $totals)) {
                    $totals[$field] = 0;
                };
                $totals[$field] += $value;
            };
        };
    };

    // get full invoice history old query that returned wrong results
    /*
    $sql = "SELECT
            site_id,
            DATE_FORMAT(bookingdate, '%Y-%m') as invoice_date,
            SUM(IF(CollectPay = 'Offsite', 0, RateToPay * RateToPayQTY)) as 'TotalToPay',
            SUM(IF(CollectPay = 'Offsite', Rate * NoOfJump, 0)) as 'TotalToCollect',
            SUM(IF(CancelFeeCollect = 'Offsite', CancelFeeCollect, 0)) as CancelFeeCollect,
            SUM(IF(CancelFeeCollect = 'Offsite', CancelFee * CancelFeeQTY, 0)) as total_cancel
            from customerregs1 as cr
            WHERE
                BookingDate >= '" . $d_last . "-01'
				AND BookingDate < '" . $d . "-01'
                AND site_id = '" . $place['id'] . "'
                and DeleteStatus = 0
                and Checked = 1
                and Agent = '{$agent}'
                and (
                    (CollectPay = 'Offsite' and NoOfJump > 0)
                    or (
                        RateToPay > 0
                        and RateToPayQTY > 0
                        and CollectPay = 'Onsite'
                    )
                )
            GROUP BY invoice_date
            ORDER BY invoice_date ASC;
        ";
    */

    $sql = "SELECT
            site_id,
            DATE_FORMAT(bookingdate, '%Y-%m') as invoice_date,
            SUM(IF(CollectPay = 'Offsite', 0, RateToPay * RateToPayQTY)) as 'TotalToPay',
            SUM(IF(CollectPay = 'Offsite', Rate * NoOfJump, 0)) as 'TotalToCollect',
            SUM(IF(CancelFeeCollect = 'Offsite', CancelFeeCollect, 0)) as CancelFeeCollect,
            SUM(IF(CancelFeeCollect = 'Offsite', CancelFee * CancelFeeQTY, 0)) as total_cancel
            from customerregs1 as cr
            WHERE
                BookingDate >= '" . $d_last . "-01'
				AND BookingDate < '" . $d . "-01'
                AND site_id = '" . $place['id'] . "'
                AND Agent = '{$agent}'
               AND (
                    (
                        /*Bookings made offsite that were completed via an agent*/
                        DeleteStatus = 0
                        AND Checked = 1
                        AND CollectPay = 'Offsite'
                        AND NoOfJump > 0
                    ) OR (
                        /*Bookings made Onsite that were completed via an agent*/
                        DeleteStatus = 0
                        AND Checked = 1
                        AND RateToPay > 0
                        AND RateToPayQTY > 0
                        AND CollectPay = 'Onsite'
                        AND NoOfJump > 0
                    ) OR (
                        /*partial cancellations through setting noOfJumps to zero in from dailyview checkin*/
                        CancelFeeCollect = 'Offsite'
                        AND NoOfJump = 0
                    )
                )
            GROUP BY invoice_date
            ORDER BY invoice_date ASC;
        ";
    //echo $sql."<br>";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        $all_invoices[] = $row;
        foreach ($row as $field => $value) {
            if (in_array($field, $total_fields)) {
                if (!array_key_exists($field, $totals_last)) {
                    $totals_last[$field] = 0;
                };
                $totals_last[$field] += $value;
            };
        };
    };
    //we use the description SALES DEPOSIT FROM Rafting ... because we want to ignore other payments to the company as these are not part of the account
    $sql = "SELECT sum(`in`-`out`) as total, DATE_FORMAT(`date`, '%Y-%m') as payment_date FROM banking WHERE company = '{$agent}' AND description = 'SALES DEPOSIT FROM Rafting Company, Pension ETC For Bungy Sales' AND site_id = '" . $place['id'] . "' and `date` BETWEEN '$d_last-01' AND '$d-" . date("t", $current_time) . "' GROUP BY payment_date;";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        $all_payments[] = $row;
        $total_payment += (int)$row['total'];
    };
};
ksort($all_data);
// last invoice amount
$last_invoice = 0;
foreach ($all_invoices as $i) {
    if ($i['invoice_date'] < $d) {
        $last_invoice += $i['TotalToCollect'] - $i['TotalToPay'] + $i['total_cancel'];
    };
};
$last_month_payment = 0;
foreach ($all_payments as $p) {
    if ($p['payment_date'] < $d) {
        $last_invoice -= $p['total'];
    };
    if ($p['payment_date'] == $d) {
        $last_month_payment += $p['total'];
    };
};

//This month of the total amount
$this_month_owings =
    $totals['TotalToCollect']
    - $totals['TotalToPay']
    + $totals['total_cancel'];

//This amount billed 今回のご請求額
$this_month_outstanding_balance =
    $totals_last['TotalToCollect']
    - $totals_last['TotalToPay']
    + $totals_last['total_cancel']
    - $total_payment
    + $totals['TotalToCollect']
    - $totals['TotalToPay']
    + $totals['total_cancel'];
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Invoice for <?php echo $agent; ?></title>
    <?php include "../includes/head_scripts.php"; ?>
    <style>
        * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -ms-box-sizing: border-box;
        }

        #invoice-container {
            width: 980px;
            margin-right: auto;
            margin-left: auto;
        }

        #invoice-body {
            clear: both;
            display: table;
            width: 100%;
            border-collapse: collapse;
        }

        .invoice-row {
            display: table-row;
            border: 1px solid black;
        }

        .invoice-cell {
            display: table-cell;
            width: auto;
            min-width: 10%;
            max-width: 30%;
            border: 1px solid black;
            padding: 2px;
            font-size: 12px;
        }

        .valign {
            vertical-align: middle;
        }

        .center {
            text-align: center;
        }

        .table-heading {
            text-align: center;
            font-size: 14px;
        }

        .totals {
            height: 52px;
            border-bottom: none;
        }

        .no-border-right-left {
            border-right: none;
            border-left: none;
        }

        .float-left {
            float: left;
        }

        .height100 {
            height: 35px;
            display: block;
        }

        .notes {
            width: 30%;
        }

        .no-wrap {
            white-space: nowrap;
        }

        .big-font {
            font-size: 18px !important;
        }

        #left-header, #right-header {
            float: left;
            width: 50%;
        }

        #right-header {
            display: table;
            border: none;
        }

        #right-header .invoice-row {
            border: none;
        }

        #right-header .invoice-cell {
            border: none;
        }

        #invoice-caption {
            padding: 10px;
            font-size: 30px;
            float: left;
            margin-bottom: 100px;
        }

        #invoice-agent-name {
            width: 60%;
            float: left;
            padding: 10px;
            font-size: 20px;
        }

        #invoice-dear {
            width: 40%;
            float: left;
            padding: 10px;
            font-size: 20px;
        }

        #invoice-particulars {
            float: left;
            padding: 10px;
            width: 100%;
        }

        #invoice-add-info {
            float: left;
            padding: 10px;
            width: 100%;
        }

        .date-title, .date-value, .invoice-num-title, .invoice-num-value {
            float: left;
            width: 30%;
            text-align: right;
            padding: 10px;
            height: 60px;
            padding-top: auto;
            padding-bottom: auto;
        }

        .date-value, .invoice-num-value {
            font-size: 20px;
            line-height: 40px;
        }

        #invoice-stand {
            padding-top: 20px;
            padding-bottom: 20px;
        }

        #month-totals {
            margin-top: 20px;
            width: 500px;
        }

        #month-totals table {
            border-collapse: collapse;
            width: 560px;
        }

        #month-totals table th, #month-totals table td {
            border: 1px solid black;
            text-align: center;
            font-weight: normal;
        }

        #month-totals table th {
            padding: 3px;
            padding-left: 10px;
            padding-right: 10px;
        }

        #month-totals table td {
            padding: 5px;
        }

        #back {
            float: right;
        }
    </style>
</head>
<body>
<?php

// Invoice NO
$invoice_no = strtoupper(substr(preg_replace("/([^a-z]+)/i", "", $agent), 0, 3))
    . date("Ym01", $current_time);
?>
<div id="invoice-container">
    <div id="left-header">
        <div id="invoice-caption">請求書・INVOICE</div>
        <div id="invoice-agent-name"><?php echo $agent; ?></div>
        <div id="invoice-dear">御中</div>
        <div id="invoice-particulars">件名・Particulars:
            <?php echo date("Y", $current_time); ?>年
            <?php echo date("m", $current_time); ?>
            月分
        </div>
        <div id="invoice-add-info">下記の通り、ご請求申し上げます。</div>
    </div>
    <div id="right-header">
        <div class="date-title">
            発行年月日<br/>
            Date of issue
        </div>
        <div class="date-value">
            <?php echo date("y.m.d"); ?>
        </div>
        <br clear="both"/>

        <div class="invoice-num-title">
            請求番号<br/>
            Invoice number
        </div>
        <div class="invoice-num-value"><?php echo $invoice_no; ?></div>
        <br clear="both"/>

        <div id="invoice-stand">STANDARD MOVE ㈱</div>
        <div id="invoice-contacts">
            〒379-1617<br/>
            群馬県利根郡みなかみ町小日向１４３<br/>
            TEL: 0278-72-8133<br/>
            FAX: 0278-25-3099
        </div>
    </div>
    <br clear="both">

    <div id="month-totals">
        <table>
            <tr>
                <th>前回のご請求額</th>
                <th><?php echo (SYSTEM_SUBDOMAIN_REAL == 'AGENT') ? 'お振込み済み金額' : '今回ご入金額'; ?></th>
                <th><?php echo (SYSTEM_SUBDOMAIN_REAL == 'AGENT') ? '今月分合計金額' : '今回合計金額'; ?></th>
                <th>今回のご請求額</th>
            </tr>
            <tr>
                <td><?php echo $last_invoice; ?></td>
                <td><?php echo $last_month_payment; ?></td>
                <td><?php echo $this_month_owings; ?></td>
                <td><?php echo $this_month_outstanding_balance; ?></td>
            </tr>
        </table>
    </div>
    <br clear="both">

    <div id="invoice-body">
        <!--div class="invoice-row">
			<div class="invoice-cell center">
				ご請求金額<br />
				Amount Payable
			</div>
			<div class="invoice-cell valign big-font" style="border: none; overflow: visible;">&nbsp;&yen; <?php echo $totals['TotalToCollect'] - $totals['TotalToPay'] + $totals['total_cancel']; ?></div>
		</div-->
        <div class="invoice-row">
            <div class="invoice-cell table-heading">
                場所<br/>
                Site
            </div>
            <div class="invoice-cell table-heading">
                日付<br/>
                Date
            </div>
            <div class="invoice-cell table-heading">
                お客様名<br/>
                Customer Name
            </div>
            <div class="invoice-cell table-heading">
                人数<br/>
                No. of Jumps
            </div>
            <div class="invoice-cell table-heading">
                単価<br/>
                Unit Cost
            </div>
            <div class="invoice-cell table-heading">
                <?php if (SYSTEM_SUBDOMAIN_REAL != 'AGENT') { ?>
                    御支払金額<br/>
                    Total To Pay
                <?php } else { ?>
                    御受取金額<br/>
                    Total To Collect
                <?php }; ?>
            </div>
            <div class="invoice-cell table-heading">
                <?php if (SYSTEM_SUBDOMAIN_REAL != 'AGENT') { ?>
                    御受取金額<br/>
                    Total To Collect
                <?php } else { ?>
                    御支払金額<br/>
                    Total To Pay
                <?php }; ?>
            </div>
        </div>
        <?php
        $fields = array(
            'site',
            'CustomerRegID',
            'BookingDate',
            'RomajiName',
            'NoOfJump',
            'UnitCost',
            'TotalToPay',
            'TotalToCollect'
        );
        foreach ($all_data as $row_array) {
            foreach ($row_array as $row) {
                if ($row['TotalToPay'] || $row['TotalToCollect']) {
                    echo '<div class="invoice-row">';
                    foreach ($fields as $field) {
                        $value = $row[$field];
                        if ($field == 'CustomerRegID') continue;
                        if ($field == 'RomajiName') $value = strtoupper($value);
                        $add_classes = array();
                        $add_classes['align'] = 'center';
                        if ($field != 'Notes') $add_classes[] = 'no-wrap';
                        if ($field == 'RomajiName') $add_classes['align'] = 'left';
                        if ($field == 'Notes') $add_classes['align'] = 'left';
                        echo '<div class="invoice-cell ' . implode(' ', $add_classes) . '">' . $value . '</div>';
                    };
                    echo '</div>' . "\n";
                };
                if ($row['CancelFee'] && $row['CancelFeeQTY'] && $row['CancelFeeCollect'] == 'Offsite') {
                    echo '<div class="invoice-row">';
                    foreach ($fields as $field) {
                        $value = $row[$field];
                        if ($field == 'CustomerRegID') continue;
                        if ($field == 'RomajiName') $value = strtoupper($value);
                        $add_classes = array();
                        $add_classes['align'] = 'center';
                        if ($field != 'Notes') $add_classes[] = 'no-wrap';
                        if ($field == 'RomajiName') {
                            $add_classes['align'] = 'left';
                            $value .= ' (Cancel)';
                        };
                        if ($field == 'Notes') $add_classes['align'] = 'left';
                        if ($field == 'NoOfJump') {
                            $value = $row['CancelFeeQTY'];
                        };
                        if ($field == 'UnitCost') {
                            $value = $row['CancelFee'];
                        };
                        if ($field == 'TotalToCollect') {
                            $value = $row['CancelFee'] * $row['CancelFeeQTY'];
                        };
                        if ($field == 'TotalToPay') {
                            $value = 0;
                        };
                        echo '<div class="invoice-cell ' . implode(' ', $add_classes) . '">' . $value . '</div>';
                    };
                    echo '</div>' . "\n";
                };
            };
        };
        ?>
        <div class="invoice-row totals">
            <div class="invoice-cell table-heading no-border-right-left">&nbsp;</div>
            <div class="invoice-cell table-heading no-border-right-left">&nbsp;</div>
            <div class="invoice-cell table-heading left valign no-border-right-left"> 合計・TOTALS</div>
            <div class="invoice-cell table-heading valign big-font" style="border-bottom: 3px double black;"><?php echo $totals['NoOfJump'] + $totals['CancelFeeQTY']; ?></div>
            <div class="invoice-cell table-heading" style="border-bottom: none !important;"></div>
            <div class="invoice-cell table-heading valign big-font" style="border-bottom: 3px double black;">&yen; <?php echo $totals['TotalToPay']; ?></div>
            <div class="invoice-cell table-heading valign big-font" style="border-bottom: 3px double black;">&yen; <?php echo $totals['TotalToCollect'] + $totals['total_cancel']; ?></div>
        </div>
        <div class="invoice-row totals" style="border: none !important;">
            <div class="invoice-cell table-heading" style="border: none !important;">&nbsp;</div>
            <div class="invoice-cell table-heading" style="border: none !important;">&nbsp;</div>
            <div class="invoice-cell table-heading" style="border: none !important;">&nbsp;</div>
            <div class="invoice-cell table-heading" style="border: none !important;">&nbsp;</div>
            <div class="invoice-cell table-heading" style="border: none !important;">&nbsp;</div>
            <div class="invoice-cell table-heading valign" style="border: 2px solid black;">
                <!--div class="float-left valign" style="width: 100px;"-->
                <?php if (SYSTEM_SUBDOMAIN_REAL == 'AGENT') { ?>
                    今月分合計金額
                <?php } else { ?>
                    今回合計金額
                <?php }; ?>
                <br/>
                Current Owing
                <!--/div-->
            </div>
            <div class="invoice-cell table-heading valign big-font" style="border: 2px solid black;">
                &yen; <?php echo $totals['TotalToCollect'] - $totals['TotalToPay'] + $totals['total_cancel']; ?>
            </div>
        </div>
    </div>
    <br/>
    お振込みの場合は下記口座へ翌月の１５日までによろしくお願いいたします。<br/>
    <br/>
    ゆうちょ銀行　(〇四八）<br/>
    口座番号：普通　１３９９８１３<br/>
    スタンダードムーブ株式会社<br/>
    <button id="back" onClick="document.location = '/';"><?php echo ($lang == 'en') ? 'Back' : '<< 戻る'; ?></button>
</div>

<br/>
<br/>
<br/>
<br/>
</body>
</html>
