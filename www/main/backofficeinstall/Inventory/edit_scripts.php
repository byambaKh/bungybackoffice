<?php
$headers = array(
    "item_name",
    "item_no",
    "supplier",
    "unit",
    "unit_cost",
    "units_per_order",
    "type",
    "target_level",
    "notes",
);
$edit_fields = array_map(function ($item) {
    return strtolower(
        preg_replace(
            "/(_)$/",
            "",
            preg_replace("([^\w]+)", "_", $item)
        )
    );
}, $headers);
if (CURRENT_SITE_ID != 0) {
    $edit_fields = array('target_level', 'notes');
};
$values = array();
foreach ($edit_fields as $field) {
    $needed_field = $field;
    if ($field == 'supplier') {
        $group = 'general';
        $needed_field = 'company';
    };
    $list = BJHelper::getList($group, $needed_field);
    if (!empty($list)) {
        $values[$field] = $list[$group][$needed_field];
    };
    if ($field == 'who') {
        $values[$field] = BJHelper::getStaffList('StaffListName');
    };
    if ($field == 'type') {
        $values[$field] = BJHelper::getInventoryTypesList();
    };
};
echo '';
?>

<script>
    function row_update() {
        var id = $(this).parent().attr('custom-item-id');
        var post_data = {};
        post_data.id = id;
        $(this).parent().children("td.value").each(function () {//create an post array for updating the database
            var field = $(this).attr('rel');
            post_data[field] = $(this).children('[name=' + field + ']').prop('value');
            if (field == 'shop_name' && $(this).children('[name=' + field + '_new]').prop('value') != '') {
                post_data[field + '_new'] = $(this).children('[name=' + field + '_new]').prop('value');
            }
            ;
            if (field == 'company' && $(this).children('[name=' + field + '_new]').prop('value') != '') {
                post_data[field + '_new'] = $(this).children('[name=' + field + '_new]').prop('value');
            }
            ;
        });
        <? echo ''?>//debug breakpoint
        $.ajax({
            url: 'ajax_handler.php?action=<?php echo (CURRENT_SITE_ID == 0) ? 'update_row' : 'update_level'; ?>&type=<?php echo 'inventory_items'; ?>',
            method: 'POST',
            data: post_data,
            context: $(this)
        }).done(function () {
            $(this).parent().children("td.value").each(function () {
                var field = $(this).attr('rel');
                var value = '';
                if ((field == 'shop_name' || field == 'company') && $(this).children('[name=' + field + '_new]').prop('value') != '') {
                    value = $(this).children('[name=' + field + '_new]').prop('value');
                    fvalues[field].push({
                        'id': $(this).html(),
                        'text': $(this).html()
                    });
                    fvalues[field] = fvalues[field].sort(function (item1, item2) {
                        return (item1.text > item2.text);
                    });
                } else if (field == 'item_name') {
                    value = $(this).children('[name=' + field + ']').prop('value');
                    value = '<a href="#" onclick="window.open(\'supplier_notes.php?id=' + id + '\', \'\', \'width=600, height=700\'); return false;">' + value + '</a>';//make it into a link again
                } else {
                    value = $(this).children('[name=' + field + ']').prop('value');
                }
                if ($(this).children('[name=' + field + ']').is('select')) {
                    for (i in fvalues[field]) {
                        if (fvalues[field].hasOwnProperty(i)) {
                            if (fvalues[field][i].id == value) {
                                value = fvalues[field][i].text;
                            };
                        };
                    };
                };
                <? echo ''?>//debug breakpoint
                $(this).html(value);
            });
            $(this).html('Edit').removeClass('update').addClass('edit').unbind('click').click(row_change);
            $(this).parent().children(".cancel").html('Delete').removeClass('cancel').addClass('delete').unbind('click').click(row_delete);
        });
    }
    function row_cancel() {
        var id = $(this).attr('custom-item-id');
        $(this).parent().children("td.value").each(function () {
            var field = $(this).attr('rel');
            $(this).html($(this).children('input[type=hidden]').prop('value'));
        });
        $(this).parent().children(".update").prop('value', 'Edit').removeClass('update').addClass('edit').unbind('click').click(row_change);
        $(this).html('Delete').removeClass('cancel').addClass('delete').unbind('click').click(row_delete);
    }

    <? echo ''?>//debug breakpoint

    function row_change() {
        var id = $(this).attr('custom-item-id');
        $(this).parent().children("td.value").each(function () {
            var input_field = '';
            //rel stores information about the type of cell it is
            var field = $(this).attr('rel');
            var value = $(this).html();
            if (typeof field != 'undefined' && fvalues[field].length > 0) {
                var value_set = false;
                input_field = $('<select>').prop('name', field);
                //fvalues is an array of predefined values for drop down menus
                //it is defined at the bottom of this file
                for (fv in fvalues[field]) {//create dropdown menus for fields that have fvalues
                    if (fvalues[field].hasOwnProperty(fv)) {
                        input_field.append(
                            $('<option>').prop('value', fvalues[field][fv].id).text(fvalues[field][fv].text)
                        );
                        if (value == fvalues[field][fv].text) {
                            input_field.prop('value', fvalues[field][fv].id);
                            value_set = true;
                        }
                        ;
                    }
                    ;
                }
                ;
                if (!value_set) {
                    input_field.prop('value', $(this).html());
                }
                ;
            } else if (field == 'item_name') {//item names appear in links so we need to do things a bit differently
                var old_item_name = $(this).children('a').text();
                input_field = $('<input>').prop('name', field).prop('value', old_item_name);//edit the name of the item

            } else {
                input_field = $('<input>').prop('name', field).prop('value', $(this).html());//edit the text box for the item
            }
            ;
            $(this).html(input_field);
            $(this).append(
                $('<input>').prop('type', 'hidden').prop('name', field + '_old').prop('value', value)
            );
            if (field == 'shop_name' || field == 'company') {
                $(this).append($('<br />')).append(
                    $('<input>').prop('name', field + '_new')
                );
            }
            ;
        });
        $(this).html('Update').removeClass('edit').addClass('update').unbind('click').click(row_update);
        $(this).parent().children(".delete").html('Cancel').removeClass('delete').addClass('cancel').unbind('click').click(row_cancel);
    };

    <? echo ''?>//debug breakpoint

    $(document).ready(function () {
        $('.edit').unbind('click').click(row_change);
    });
    var fvalues = {};
    <?php
    foreach ($edit_fields as $field) {
        echo "\tfvalues['$field'] = " . json_encode(array_key_exists($field, $values) ? $values[$field] : array()) . ";\n";
    };
    ?>
</script>
