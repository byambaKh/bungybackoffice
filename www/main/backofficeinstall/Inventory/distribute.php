<?php
include '../includes/application_top.php';
require_once('functions.php');
handlePermissions();

$user = new BJUser();

$inventory_site_id = CURRENT_SITE_ID;

// get item info
$id = (int)$_GET['id'];
if (array_key_exists('item_id', $_POST)) {
    $id = (int)$_POST['item_id'];
};
$sql = "SELECT * FROM inventory_items WHERE id = $id";
$res = mysql_query($sql) or die(mysql_error());
if (!($item = mysql_fetch_assoc($res))) {
    echo "No item found.";
    die();
}
mysql_free_result($res);
// get available items
$items = array();
$sql = "SELECT
                iit.id,
                iit.created,
                iit.unit_cost,
                (iit.units + IFNULL(distributed, 0)) as total
        FROM inventory_items_transactions iit
        LEFT JOIN (SELECT sum(iitd.units) as distributed, parent_id FROM inventory_items_transactions iitd GROUP BY parent_id) iid
        on (iit.id = iid.parent_id)
        WHERE iit.item_id = $id and iit.site_id = '" . CURRENT_SITE_ID . "' /*The following was commented out*/AND iit.units > IFNULL(distributed, 0)/*END*/
        ORDER BY iit.created ASC, id ASC;";
$res = mysql_query($sql) or die(mysql_error());
$total = 0;
while ($row = mysql_fetch_assoc($res)) {
    $items[] = $row;
    $total += $row['total'];
};
mysql_free_result($res);

// post processing
$action = '';
if (isset($_POST['action'])) {
    $action = $_POST['action'];
};
switch ($action) {
    case 'save':
    case 'Save':
        $data = array(
            'item_id' => $_POST['item_id'],
            'created' => $_POST['date'],
            'type_id' => 2, // Distribute
            'notes'   => $_POST['notes'],
        );
        $needed = $_POST['units'];
        foreach ($items as $item) {
            if ($needed == 0) break;
            if ($item['total'] == 0) continue;
            $distributed = ($needed < $item['total']) ? $needed : $item['total'];
            $needed -= $distributed;
            $data['parent_id'] = $item['id'];
            $data['units'] = -$distributed;
            $data['unit_cost'] = $item['unit_cost'];
            $data['site_id'] = CURRENT_SITE_ID;
            $data['approved'] = 1;
            $data['user_id'] = $user->getID();
            $data['user_name'] = $user->getUserName();
            $res = db_perform('inventory_items_transactions', $data);
            $fid = mysql_insert_id();

            $data['parent_id'] = 0;
            $data['spare_id'] = $fid;
            $data['units'] = $distributed;
            $data['site_id'] = $_POST['distribute_site'];
            $data['approved'] = 0;
            $data['user_id'] = $user->getID();
            $data['user_name'] = $user->getUserName();
            db_perform('inventory_items_transactions', $data);
            $sid = mysql_insert_id();
            $data2 = array('spare_id' => $sid);
            db_perform('inventory_items_transactions', $data2, 'update', 'id=' . $fid);
        };

        ?>
        <!DOCTYPE html>
        <html>
        <head>
            <?php include "../includes/head_scripts.php"; ?>
            <script>
                $(document).ready(function () {
                    window.opener.location.reload();
                    window.opener.focus();
                    window.close();
                });
            </script>
        </head>
        <body>
        </body>
        </html>
        <?php
        die();
        break;
};
// get all sites info
$sql = "SELECT * FROM sites ORDER BY name ASC;";
$res = mysql_query($sql) or die(mysql_error());
$sites_drop_down = "<select name='distribute_site' id='distibute-site'>";
while ($row = mysql_fetch_assoc($res)) {
    $sites_drop_down .= "<option value='{$row['id']}'>{$row['name']}</option>";
};
$sites_drop_down .= "</select>";
mysql_free_result($res);
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $item['item_name']; ?> Distribute</title>
    <?php include "../includes/head_scripts.php"; ?>
    <style>
        #distribute-container {
            width: 380px;
            overflow: hidden;
            margin-left: auto;
            margin-right: auto;
        }

        #distribute-container label {
            float: left;
            width: 150px;
            text-align: right;
            display: block;
            clear: both;
            height: 25px;
        }

        #distribute-container input, #distribute-container textarea, #distribute-container select {
            float: left;
            margin-left: 5px;
            width: 200px;
        }

        #distribute-date {
            width: 80px !important;
            text-align: center;
        }

        h1 {
            width: 100%;
            text-align: center;
        }

        #buttons-container {
            width: 100%;
        }

        #distribute-save-button {
            float: right;
        }

        #back {
            float: left;
        }
    </style>
    <script>
        $(document).ready(function () {
            $("#distribute-date").datepicker({
                showAnim: 'fade',
                dateFormat: 'yy-mm-dd',
                currentText: 'Today'
            });
            $("#back").on('click', function () {
                window.opener.focus();
                window.close();
            });
            $("#distribute-units").change(function () {
                var result = false;
                try {
                    var val = parseInt($(this).val());
                    if (val > 0 && val <= <?php echo (int)$total; ?>) {
                        //This linebreak goes missing in Firefox
                        result = true;
                    }
                } catch (e) {
                    window.alert('Quantity for distribution must be numeric and less or equal to <?php echo (int)$total; ?>');
                    $(this).focus();
                }
                ;
                if (!result) {
                    window.alert('Quantity for distribution must be numeric and less or equal to <?php echo (int)$total; ?>');
                    $(this).focus();
                }
                $("#distribute-save-button").attr('disabled', !result);
                return result;
            });
        });
    </script>
</head>
<body>
<div id="distribute-container">
    <form method="post">
        <h1><?php echo $item['item_name']; ?></h1>
        <label for="distribute-date">Date:</label>
        <input type="text" name="date" value="<?php echo date("Y-m-d"); ?>" id="distribute-date">
        <label for="distribute-units">Units Distributed:</label>
        <input type="text" name="units" value="" id="distribute-units">
        <label for="distribute-site">Target Site:</label>
        <?php echo $sites_drop_down; ?>
        <label for="distribute-notes">Notes:</label>
        <textarea name="notes" id="distribute-notes"></textarea>
        <div class="buttons-container">
            <button type="button" id="back">Close</button>
            <button type="submit" id="distribute-save-button" name="action" value="save">Save</button>
        </div>
        <input type="hidden" name="item_id" value="<?php echo $item['id']; ?>">
    </form>
</div>
</body>
