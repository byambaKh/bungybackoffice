<?php
include '../includes/application_top.php';
require_once('functions.php');
handlePermissions();

$user = new BJUser();

$user = new BJUser();
if ($user->getUserName() == 'sugai'){
    $readOnly = false;
} else {
    $readOnly = true;
}

mysql_query("SET NAMES UTF8");//prevents json decodes from failing if there are invalid UTF-8 chars
/**
 * @param $user
 *
 * @return array
 */
function handleGET($user)
{
    $action = '';
    if (isset($_POST['action'])) {
        $action = $_POST['action'];
    }

    if (isset($_GET['action'])) {
        $action = $_GET['action'];
    }

    $filter = 0;
    if (isset($_GET['filter'])) {
        $filter = $_GET['filter'];
    }

    switch ($action) {//The user is on main and has used the input at the bottom to add a new item
        case 'add_item':
        case 'Add Item':
            $data = array(
                'item_name'       => $_POST['item_name'],
                'item_no'         => $_POST['item_no'],
                'supplier'        => $_POST['supplier'],
                'unit'            => $_POST['unit'],
                'unit_cost'       => $_POST['unit_cost'],
                'units_per_order' => $_POST['units_per_order'],
                'type_id'         => $_POST['type'],
                'target_level'    => $_POST['target_level'],
                'notes'           => $_POST['notes'],
            );
            db_perform('inventory_items', $data);
            Header("Location: /Inventory/");
            die();
            break;
        case 'deletetrn'://Delete Transaction when on main and you select a check box and press the delete transaction button
            $deleted = 0;
            foreach ($_POST['ids'] as $id) {
                $sql = "DELETE FROM inventory_items_transactions WHERE id = '$id' and site_id = '" . CURRENT_SITE_ID . "'";
                if ($res = mysql_query($sql)) {
                    $deleted += $res;
                }
            }

            //this looks wrong and inventory is spelt wrongly
            $_SESSION['inventroy_message'] = "Totally $deleted records deleted.";
            Header("Location: /Inventory/");
            die();
            break;
        case 'approve'://Approve a distribution transaction using "Show Pending Requests" -> "Confirm distribution"
            $updated = 0;
            foreach ($_POST['ids'] as $id) {
                $sql = "UPDATE inventory_items_transactions SET approved = 1 WHERE id = '$id' and site_id = '" . CURRENT_SITE_ID . "'";
                if ($res = mysql_query($sql)) {
                    $updated += $res;
                }
                // request processing
                if (CURRENT_SITE_ID == 0) {
                    $sql = "SELECT * from inventory_items_transactions WHERE id = '$id';";
                    $res = mysql_query($sql) or die(mysql_error());
                    if (($row = mysql_fetch_assoc($res)) && $row['type_id'] == 2) {//a distribution transaction
                        unset($row['id']);
                        $row['approved'] = 0;
                        $s_site_id = $row['site_id'];
                        $row['site_id'] = $row['d_site_id'];
                        $row['d_site_id'] = $s_site_id;
                        $row['units'] = -$row['units'];
                        $row['spare_id'] = $id;
                        $row['parent_id'] = 0;
                        $row['created'] = date("Y-m-d");
                        $row['user_id'] = $user->getUserName();
                        $row['user_name'] = $user->getID();
                        db_perform('inventory_items_transactions', $row);
                        $new_id = mysql_insert_id();
                        $data = array('spare_id' => $new_id);
                        db_perform('inventory_items_transactions', $data, 'update', 'id=' . $id);
                    }
                }
            }

            $_SESSION['inventory_message'] = "Totally $updated records approved.";
            Header("Location: /Inventory/");
            die();
            break;
        case 'delete'://using the delete button on main, to the right of the screen
            $id = (int)$_GET['id'];
            $sql = "UPDATE inventory_items SET deleted = 1 WHERE id = $id;";
            $res = mysql_query($sql) or die(mysql_error());
            Header("Location: /Inventory/");
            die();
            break;
    }

    return array($action, $filter);
}

/**
 * @param $place_names
 *
 * @return array
 */
function getPlacesData()
{
    // select all possible inventory places
    $places = [];
    $place_names = [];

    $sql = "SELECT * FROM sites;";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        $places[$row['id']] = $row;
        $place_names[$row['id']] = $row['name'];
    }
    $inventory_site_id = CURRENT_SITE_ID;
    $current_place_name = 'Unknown'; // for testing errors
    if (array_key_exists($inventory_site_id, $places)) {
        $current_place_name = $places[$inventory_site_id]['name'];
    }

    return array($place_names, $inventory_site_id, $current_place_name);
}

/**
 * @param $filter
 *
 * @return array
 */
function getAllItemTypes($filter)
{
// get all item types
    $sql = "SELECT * FROM inventory_items_types ORDER BY type_name ASC;";
    $res = mysql_query($sql) or die(mysql_error());
    $item_types = array();
    $filter_found = false;
    while ($row = mysql_fetch_assoc($res)) {
        $item_types[$row['id']] = $row['type_name'];
        if ($filter && $filter == $row['id']) {
            $filter_found = true;
        }
    }
    if (!$filter_found) {
        $filter = 0;
    }
    mysql_free_result($res);

    return array($item_types, $filter);
}

/**
 * @param $inventory_site_id
 * @param $filter
 *
 * @return array
 */
function getExistingItems($inventory_site_id, $filter)
{
// get existing items
//This query seems to get all of the items and the required information to populate the table
    $sql = "SELECT
				ii.*, 
				IFNULL(iil.target_level, ii.target_level) as site_target_level,/*if the target level in inventory_items_levels is not set, used the target level from inventory_items*/
				iit.type_name as type,  
				sum(IFNULL(iitr.units, 0)) as on_hand,/*Sum up all of the inventory items transactions*/
				sum(IFNULL(iitr.units, 0) * cast(iitr.unit_cost as signed)) as total /*Sum inventory items transactions * unit cost as total*/
		FROM inventory_items ii
		LEFT JOIN inventory_items_transactions iitr on (ii.id = iitr.item_id and iitr.site_id = '$inventory_site_id' and approved = 1)/*Get all approved transactions that match the item and site. They are summed*/
		LEFT JOIN inventory_items_types iit on (ii.type_id = iit.id)/*get the transaction type*/
		LEFT JOIN inventory_items_levels iil on (iil.site_id = '" . CURRENT_SITE_ID . "' AND ii.id = iil.item_id)/*Get the levels from iil*/
	" . (($filter != 0) ? " WHERE ii.type_id = '$filter' " : '') . "/*If a filter is set in the dropdown, filter by that.*/
		GROUP BY ii.id
		ORDER BY ii.item_name ASC;";

//echo $sql . "<br><br>";
    $res = mysql_query($sql) or die(mysql_error());
    $items = array();
    while ($row = mysql_fetch_assoc($res)) {
        $items[$row['id']] = $row;
    }
    mysql_free_result($res);

    return $items;
}

/**
 * @return array
 */
function getItemTooltipData()
{
    $detailed_items = [];
    $detailed_summary = [];
    //This query seems to get extra info for the javascript tooltip balloon that appears when hovering over the on hand column
    //The other values are just calculated in the script.
    $sql = "SELECT
                iit.item_id,
                iit.unit_cost,
                sum(iit.units + IFNULL(distributed, 0)) AS total
        FROM inventory_items_transactions iit
        LEFT JOIN (SELECT sum(iitd.units) AS distributed, parent_id FROM inventory_items_transactions iitd GROUP BY parent_id) iid
        ON (iit.id = iid.parent_id)
        WHERE iit.site_id = '" . CURRENT_SITE_ID . "' /*and iit.units > ABS(IFNULL(iid.distributed, 0))*/ AND iit.parent_id = 0
		GROUP BY item_id, unit_cost
        ORDER BY iit.created ASC, id ASC;";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        $detailed_items[$row['item_id']][] = $row;
    }


    //echo $sql . "<br><br>";
    mysql_free_result($res);

    return $detailed_items;
}

/**
 * @return array
 */
function getItemTransfers()
{
    $sql = "SELECT iitt.title AS tname, iit.*, ii.item_name
			FROM inventory_items_transactions iit
			LEFT JOIN inventory_items ii ON (iit.item_id = ii.id)
			LEFT JOIN inventory_items_transaction_types iitt ON (iit.type_id = iitt.id)
			WHERE
				iit.site_id = '" . CURRENT_SITE_ID . "' AND iit.approved = 0
			ORDER BY iit.created ASC;
	";
    //echo $sql."<br><br>";
    //This shows the Pending number of requests
    $transfers = queryForRows($sql);

    return $transfers;
}

$inventory_site_id = CURRENT_SITE_ID;
list($action, $filter) = handleGET($user);
list($place_names, $inventory_site_id, $current_place_name) = getPlacesData();
list($item_types, $filter) = getAllItemTypes($filter);
$items = getExistingItems($inventory_site_id, $filter);
$detailed_items = getItemTooltipData();
$detailed_items_json = json_encode($detailed_items);

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo SYSTEM_SUBDOMAIN; ?> Inventory</title>

    <?php include "../includes/head_scripts.php"; ?>
    <link rel='stylesheet' href="css/index.css" type="text/css">
    <style>
    </style>
    <script>
        var detailed_items = <?= $detailed_items_json ?>;
        $(document).ready(function () {
            $('.purchase').on('click', function () {
                var item_id = $(this).parent().attr('custom-item-id');
                var purchase_window = window.open('purchase.php?id=' + item_id, 'bungyjapan_purchase', 'height=280px,width=500px,location=0,menubar=0,status=0,toolbar=0');
                purchase_window.focus();
            });
            $('.history').on('click', function () {
                var item_id = $(this).parent().attr('custom-item-id');
                var history_window = window.open('history.php?id=' + item_id, 'bungyjapan_history', 'height=500,width=900,location=0,menubar=0,status=0,scrollbars=1,toolbar=0');
                history_window.focus();
            });
            $('#summary').on('click', function () {
                var summary_window = window.open('summary.php', 'bungyjapan_summary', 'height=500,width=900,location=0,menubar=0,status=0,toolbar=0,scrollbars=1');
                summary_window.focus();
            });
            $('.distribute').on('click', function () {
                var item_id = $(this).parent().attr('custom-item-id');
                var distribute_window = window.open('distribute.php?id=' + item_id, 'bungyjapan_distribute', 'height=350,width=900,location=0,menubar=0,status=0,toolbar=0, scrollbars=yes');
                distribute_window.focus();
            });
            $('.request').on('click', function () {
                var item_id = $(this).parent().attr('custom-item-id');
                var request_window = window.open('request.php?id=' + item_id, 'bungyjapan_request', 'height=280px,width=500px,location=0,menubar=0,status=0,toolbar=0');
                request_window.focus();
            });
            $('.adjust').on('click', function () {
                var item_id = $(this).parent().attr('custom-item-id');
                var adjust_window = window.open('adjust.php?id=' + item_id, 'bungyjapan_adjust', 'height=280px,width=500px,location=0,menubar=0,status=0,toolbar=0');
                adjust_window.focus();
            });
            $('.delete').on('click', row_delete);
            $(".on_hand").mouseenter(show_item_hint);
            $(".on_hand").mousemove(move_item_hint);
            $(".on_hand").mouseout(hide_item_hint);
            $('#show-incomplete').click(show_incomplete);
            $('.ids').change(function () {
                if ($(".ids:checked").length > 0) {
                    $("#approve").attr('disabled', false);
                    $("#deletetrn").attr('disabled', false);
                } else {
                    $("#approve").attr('disabled', 'disabled');
                    $("#deletetrn").attr('disabled', 'disabled');
                }

                if ($(".not-available .ids:checked").length == 0 && $(".ids:checked").length > 0) {
                    $("#approve").attr('disabled', false);
                } else {
                    $("#approve").attr('disabled', 'disabled');
                }
            });
            //$('#info-message').sleep(3000).slideUp('slow');
        });
        function row_delete() {
            var item_id = $(this).parent().attr('custom-item-id');
            if (window.confirm('Are you sure that you want to delete this record?')) {
                document.location = '?action=delete&id=' + item_id;
            }
        }

        function move_item_hint(e) {
            if ($("#item-hint").length == 0) return;
            $("#item-hint").css({
                'left': e.pageX + 20,
                'top': e.pageY + 10
            });
        }

        function show_item_hint(e) {
            if (e.target.tagName != 'TD') return;
            $("#item-hint").remove();
            e.preventDefault();
            var id = $(this).parent().attr('custom-item-id');
            var hint = $('<div id="item-hint">');
            var totals = {}
            totals.units = 0;
            totals.total = 0
            //alert(id + detailed_items.hasOwnProperty(id));
            if (detailed_items.hasOwnProperty(id) && typeof detailed_items[id] != 'null') {
                var t = $('<table>').append(
                    $("<tr>").html("<th>On Hand</th><th>Cost</th><th>Total</th>")
                );
                for (i in detailed_items[id]) {
                    if (!detailed_items[id].hasOwnProperty(i)) continue;
                    t.append(
                        $("<tr>").html("<td>" + detailed_items[id][i].total + "</td><td>" + detailed_items[id][i].unit_cost + "</td><td>" + (detailed_items[id][i].total * detailed_items[id][i].unit_cost) + "</td>")
                    );
                    totals.units += parseInt(detailed_items[id][i].total);
                    totals.total += detailed_items[id][i].total * detailed_items[id][i].unit_cost;
                }
                if (totals.units > 0) {
                    totals.unit_cost = Math.ceil(totals.total / totals.units);
                } else {
                    totals.unit_cost = 0;
                }
                t.append(
                    $("<tr class='item-details-total'>").html("<td>" + totals.units + "</td><td>" + totals.unit_cost + "</td><td>" + totals.total + "</td>")
                );
                hint.html(t);
                hint.css({
                    'position': 'absolute',
                    'left': e.pageX + 20,
                    'top': e.pageY + 10
                });
                //alert(hint.html());
                $('body').append(hint);
            }

        }

        if (<?php echo json_encode($readOnly); ?>){
          function hide_item_hint() {
              $("#item-hint").remove();
          }

          function show_incomplete() {
              $('#action-required').slideDown();
              $('#show-incomplete').html('Hide Pending Requests').unbind('click').click(hide_incomplete);
          }

          function hide_incomplete() {
              $('#action-required').slideUp();
              $('#show-incomplete').html('Show Pending Requests').unbind('click').click(show_incomplete);
          }
        }  

    </script>
    <?php include 'edit_scripts.php';//Hiroshi is logged out here ?>
</head>
<body>
<div id="main-container">
    <?php include "../includes/main_menu.php"; ?>
    <h1>Inventory - <?php echo $current_place_name; ?></h1>
    <?php
    if (!empty($_SESSION['inventory_message'])) {
        echo "<div id='info-message'>{$_SESSION['inventory_message']}</div>";
        unset($_SESSION['inventory_message']);
    }
    $transfers = getItemTransfers();
    $fields = array(
        'd_site_id' => 'Site Name',
        'tname'     => 'Transaction Name',
        'item_name' => 'Item Name',
        'units'     => 'Units',
        'unit_cost' => 'Unit Cost',
        'created'   => 'Date',
        'notes'     => 'Notes',
    );
    if (count($transfers) > 0) {
    ?>
    <form method="post">
        <div id="action-required-container">
            <?php
            echo "<button id='show-incomplete' type='button'>Show Pending Requests</button>";
            ?>
            <div id="action-required">
                <br/>
                <table>
                    <?php
                    echo "<tr>";
                    echo "<th class='id'>&nbsp;</th>";
                    foreach ($fields as $fname => $ftitle) {
                        echo "<th>$ftitle</th>";
                    }
                    echo "</tr>";
                    foreach ($transfers as $transfer) {
                        $rowclass = '';
                        $checkbox_disabled = '';
                        //This code does not compare the onhand totals to the request total
                        //Not sure why or if this truly works
                        if ($transfer['parent_id'] == 0 && CURRENT_SITE_ID == 0) {
                            $rowclass = 'class="not-available"';
                            $checkbox_disabled = ' disabled="disabled"';
                        }

                        /*
                        $notEnoughStock = ($items[$transfer['item_id']]['on_hand'] + $transfer['units']) < 0;
                        if($notEnoughStock) {
                            $rowclass = 'class="not-available"';
                            $checkbox_disabled = ' disabled="disabled"';
                        }
                        */

                        echo "<tr $rowclass>";
                        echo "<td class='id'><input type='checkbox' class='ids' name='ids[]' value='{$transfer['id']}'></td>";
                        foreach ($fields as $fname => $ftitle) {
                            $value = $transfer[$fname];
                            if ($fname == 'd_site_id') {
                                $value = $place_names[$value];
                            }
                            echo "<td class='$fname'>$value</td>";
                        }
                        echo "</tr>";
                    }
                    echo "</table><br />";
                    echo "<button id='approve' type='submit' name='action' value='approve' disabled='disabled'>Confirm Distribution</button>";
                    if (CURRENT_SITE_ID == 0) {
                        echo "<button id='deletetrn' type='submit' name='action' value='deletetrn' disabled='disabled'>Delete Request(s)</button>";
                    }
                    echo "</div>";
                    echo "</div>";
                    echo "</form>";
                    }

                    $item_types_drop = array(
                        array(
                            'id'   => 0,
                            'text' => 'All'
                        )
                    );

                    foreach ($item_types as $id => $type) {
                        $item_types_drop[] = array(
                            'id'   => $id,
                            'text' => $type
                        );
                    }
                    ?>
                    <br/>

                    <form>
                        Filter by
                        type: <?php echo draw_pull_down_menu('filter', $item_types_drop, $filter, 'onChange="this.form.submit();"'); ?>
                    </form>
                    <form method="post">
                        <table id="items-table">
                            <?php
                            // fields part
                            $fields = array(
                                'item_name' => 'Item'
                            );
                            if (CURRENT_SITE_ID == 0) {
                                $admin_fields = array(
                                    'item_no'  => 'Item No.',
                                    'supplier' => 'Supplier'
                                );
                                $fields = array_merge($fields, $admin_fields);
                            }
                            $fields = array_merge($fields, array(
                                'unit'            => 'Unit',
                                'unit_cost'       => 'Unit Cost',
                                'units_per_order' => 'Units per Order',
                                'type'            => 'Type',
                                'target_level'    => 'Target Level',
                                'on_hand'         => 'On Hand',
                                'notes'           => 'Notes',
                            ));
                            // actions part
                            $actions = array('history' => 'History');
                            if ($inventory_site_id == 0) {
                                $actions = $actions = array_merge($actions, array(
                                    'purchase'   => 'Purchase',
                                    'distribute' => 'Distribution',
                                    'adjust'     => 'Adjustment',
                                    'edit'       => 'Edit',
                                    'delete'     => 'Delete'
                                ));
                            } else {
                                $actions = $actions = array_merge($actions, array(
                                    'request' => 'Requisition',
                                    //'retire'	=> 'Retirement',
                                    'adjust'  => 'Adjustment',
                                    'edit'    => 'Edit',
                                ));
                            }
                            $actions = array_merge($actions, array());
                            // headers
                            echo "<tr>\n";

                            //Print the headers
                            foreach ($fields as $field_name => $field_text) {
                                echo "\t<th class='$field_name'>$field_text</th>\n";
                            }
                            unset($field_name);

                            echo "\t<th colspan='" . (count($actions) + 1) . "' class='empty-header'>&nbsp;</th>\n";
                            echo "</tr>\n";
                            // eof headers
                            // items section
                            foreach ($items as $item) {
                                if ($item['deleted'] != 0 && $item['on_hand'] == 0) continue;
                                echo "<tr custom-item-id='{$item['id']}'>\n";
                                foreach ($fields as $field_name => $field_text) {
                                    $add_class = "";
                                    $rel = '';
                                    if ($field_name == 'on_hand') {
                                        switch (TRUE) {
                                            case ($item['on_hand'] < 2):
                                                $add_class = " low";
                                                break;
                                            case ($item['on_hand'] < $item['site_target_level']):
                                                $add_class = " medium";
                                                break;
                                            default:
                                                $add_class = " normal";
                                                break;
                                        }
                                    }
                                    if ($field_name == 'unit_cost') {
                                        if ($item['on_hand'] != 0) {
                                            $item[$field_name] = ceil($item['total'] / $item['on_hand']);
                                        }
                                    } elseif (in_array($field_name, $edit_fields)) {
                                        $add_class .= " value";
                                        $rel = " rel='$field_name'";
                                    }
                                    $add_text = '';
                                    if ($field_name == 'notes' && $item['deleted'] == 1) {
                                        $add_text = ' <span class="deleted">Pending Deletion</span>';
                                    }
                                    $value = $item[$field_name];
                                    if ($field_name == 'target_level') {
                                        $value = $item['site_target_level'];
                                    }

                                    if ($field_name == 'item_name') {
                                        $itemId = $item['id'];
                                        $itemName = $item['item_name'];
                                        $value = "<a href='#' onclick=\"window.open('supplier_notes.php?id=$itemId', '', 'width=600, height=700, scrollbars=yes'); return false;\">$itemName</a>";
                                    }

                                    echo "\t<td class='$field_name$add_class'$rel>{$value}$add_text</td>\n";
                                }
                                echo "\t<td class='empty-header'>&nbsp;</td>\n";
                                    if ($readOnly){//don't let sugai (accounting firm) to edit the records
                                    	foreach ($actions as $action => $text) {
                                        	echo "\t<td class=\"action $action\">$text</td>\n";
                                    }
                                    echo "</tr>\n";
                                }
                            }
                            if ($readOnly){

                            // items section
                            // headers
                            echo "<tr>\n";
                            foreach ($fields as $field_name => $field_text) {
                                echo "\t<th class='$field_name'>$field_text</th>\n";
                            }
                            echo "\t<th colspan='" . (count($actions) + 1) . "' class='empty-header'>&nbsp;</th>\n";
                            echo "</tr>\n";
                            // eof headers

                            echo "<tr class='last-row'>\n";
                            foreach ($fields as $field_name => $field_text) {
                                $input = "<input type='text' name='$field_name'>\n";
                                if ($field_name == 'type') {
                                    $input = "<select name='$field_name'>";
                                    foreach ($item_types as $id => $type_name) {
                                        $input .= "<option value='$id'>$type_name</option>\n";
                                    }
                                    $input .= "</select>";
                                }
                                if ($field_name == 'supplier') {
                                    $values = BJHelper::getList('general', 'company');
                                    $input = draw_pull_down_menu($field_name, $values['general']['company']);
                                }
                                if ($field_name == 'on_hand') $input = '';
                                if ($field_name == 'unit_cost') $input = '';
                                $input = ($inventory_site_id == 0) ? $input : '';
                                echo "\t<td class='fields'>$input</td>\n";
                            }
                            echo "\t<td class='empty-header'>&nbsp;</td>\n";
                            $add_item = ($inventory_site_id == 0) ?
                                "<button name='action' value='add_item' id='add-item' type='submit'>Add Item</button>"
                                : '';
                            echo "\t<td colspan='2' class=\"actions\">$add_item</td>\n";
                            echo "\t<td colspan='" . (count($actions) - 2) . "' class=\"actions\">&nbsp;</td>\n";
                            echo "</tr>\n";
                            }
                            ?>
                        </table>
                    </form>
                    <button onClick="javascript: document.location='/'" id="back">Back</button>
                    <?php if (CURRENT_SITE_ID == 0) { ?>
                        <button onClick="javascript: document.location='/Inventory/deleted.php'" id="deleted">Deleted Products
                        </button>
                    <?php } else { ?>
                        <button type="button" id="summary">Distribution Summary</button>
                    <?php } ?>
                    <?php include("ticker.php"); ?>
            </div>
</body>
</html>
