<?php
include '../includes/application_top.php';
require_once('functions.php');

handlePermissions();

/**
 * @return array
 */
function getItemInfo()
{
// get item info
    $id = (int)$_GET['id'];
    $sql = "SELECT * FROM inventory_items WHERE id = $id";
    $res = mysql_query($sql) or die(mysql_error());
    if (!($item = mysql_fetch_assoc($res))) {
        echo "No item found.";
        die();
    }
    mysql_free_result($res);

    return $item;
}

/**
 * @return array
 */
function getItemsForPostId()
{
    $sql = "SELECT
						id, 
						iit.item_id,
                		iit.unit_cost,
                		(iit.units + IFNULL(distributed, 0)) as total,/*Sum of units plus distributed*/
						IF(iit.units + IFNULL(distributed, 0) = 0, 0, 1) as cleared_order
						/*if (Units + Distributed) == 0 return 0, else return 1; If there are items left, it is not a cleared order*/
						/*Cleared order is for when the order has been used up*/
        		FROM inventory_items_transactions iit
        		LEFT JOIN (SELECT sum(iitd.units) as distributed, parent_id FROM inventory_items_transactions iitd GROUP BY parent_id) iid /**/
        		on (iit.id = iid.parent_id)
        		WHERE iit.item_id = '{$_POST['item_id']}' and iit.site_id = '" . CURRENT_SITE_ID . "' /*and iit.units >= ABS(IFNULL(distributed, 0))*/ and iit.parent_id = 0
        		ORDER BY cleared_order DESC, iit.created ASC, id ASC;";
    $res = mysql_query($sql) or die(mysql_error());
    //echo $sql."<br><br>";

    $items = array();
    while ($row = mysql_fetch_assoc($res)) {
        $items[] = $row;
    };
    mysql_free_result($res);

    return $items;
}

/**
 * @param $items
 * @param $user
 */
function distributeItems($items, $user)
{
    $needed = $_POST['order_units'];

    foreach ($items as $item) {
        //needed = the quantity requested for the transaction
        if ($needed == 0) break;//end subtracting from the item once all needed items have been done.
        //It appears as if this code distributes the adjustment across multiple transactions entries in the inventory items transactions table
        //If needed is > 0, ie positive, just add it to a transactions
        //if it is negative,

        if ($needed > 0) {//if needed is greater than zero, then assign that value to the quantity for the transaction we are adding items as an adjustment
            $quantity = $needed;
        } else {
            //abs($needed) of something not greater than zero is gonna be the positive of a negative
            //if we require more items than there are in that particular item total
            //then subtract the whole amount available from that item total
            //or else if not, just use needed (which is negative)
            //else it is needed
            $quantity = (abs($needed) > $item['total']) ? -$item['total'] : $needed;
        };
        $needed -= $quantity;//decrement needed by quantity

        $data = array(
            'parent_id' => $item['id'],//this is the transaction id not the item id
            'item_id'   => $_POST['item_id'],
            'units'     => $quantity,
            'unit_cost' => $item['unit_cost'],
            'site_id'   => CURRENT_SITE_ID,
            'approved'  => 1,
            'created'   => $_POST['order_date'],
            'type_id'   => 3, // Adjust
            'notes'     => $_POST['order_notes'],
            'user_id'   => $user->getID(),
            'user_name' => $user->getUserName()
        );
        //insert a new record of type 3 (adjustment) to and keep doing so until all of the needed or allocated
        db_perform('inventory_items_transactions', $data);
    }
    /*
     * There is no reason to do all of this, an adjustment should just be an adjustment, there is no need to go through
     * previous values and try to match values. If a person changes the total, just do that!
     *
     * parent id seems to keep track of items at they move in to the company, then in to head office.
     *
     * It seems that we also keep track of the average price that the item was ordered at. Should we do this?
     * Should the average price just be a company wide average?
     */
}

$user = new BJUser();
$inventory_site_id = CURRENT_SITE_ID;

$action = '';
if (isset($_POST['action'])) {
    $action = $_POST['action'];
}

switch ($action) {
    case 'save':
    case 'Save':
        $items = getItemsForPostId();
        distributeItems($items, $user);
        ?>
        <!DOCTYPE html>
        <html>
        <head>
            <?php include "../includes/head_scripts.php"; ?>
            <script>
                $(document).ready(function () {
                    window.opener.location.reload();
                    window.opener.focus();
                    window.close();
                });
            </script>
        </head>
        <body>
        </body>
        </html>
        <?php
        die();
        break;
};

$item = getItemInfo();

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?= $item['item_name']; ?> Adjustment</title>
    <?php include "../includes/head_scripts.php"; ?>
    <style>
        #purchase-container {
            width: 380px;
            overflow: hidden;
            margin-left: auto;
            margin-right: auto;
        }

        #purchase-container label {
            float: left;
            width: 150px;
            text-align: right;
            display: block;
            clear: both;
            height: 25px;
        }

        #purchase-container input, #purchase-container textarea {
            float: left;
            margin-left: 5px;
            width: 200px;
        }

        #purchase-date {
            width: 80px !important;
            text-align: center;
        }

        h1 {
            width: 100%;
            text-align: center;
        }

        #buttons-container {
            width: 100%;
        }

        #purchase-save-button {
            float: right;
        }

        #back {
            float: left;
        }
    </style>
    <script>
        $(document).ready(function () {
            $("#purchase-date").datepicker({
                showAnim: 'fade',
                dateFormat: 'yy-mm-dd',
                currentText: 'Today'
            });
            $("#back").on('click', function () {
                window.opener.focus();
                window.close();
            });
        });
    </script>
</head>
<body>
<div id="purchase-container">
    <form method="post">
        <h1><?= $item['item_name']; ?></h1>
        <label for="purchase-date">Date:</label>
        <input type="text" name="order_date" value="<?= date("Y-m-d"); ?>" id="purchase-date">
        <label for="purchase-units">Units Adjusted:</label>
        <input type="text" name="order_units" value="" id="purchase-units">
        <label for="purchase-notes">Notes:</label>
        <textarea name="order_notes" id="purchase-notes"></textarea>

        <div class="buttons-container">
            <button type="button" id="back">Close</button>
            <button type="submit" id="purchase-save-button" name="action" value="save">Save</button>
        </div>
        <input type="hidden" name="item_id" value="<?= $item['id']; ?>">
    </form>
</div>
</body>
