<?php

function handlePermissions()
{
    $user = new BJUser();
    $allowedRole = $user->hasRole(['SysAdmin', 'General Manager', 'Site Manager', 'Price Waterhouse Cooper', 'Financial', 'InventoryManager']);
    $userName = $user->getUserName();
    //$allowedPerson = ($user->getUserName() == 'Hiroshi'|| $user->getUserName() == 'Yu');
	$allowedPerson = in_array($userName, ['Hiroshi', 'Yu', 'noriko', 'sugai', 'Sachiko', 'mike']);

    if (!($allowedRole || $allowedPerson )) {
        access_denied();
        exit();
    }
}
