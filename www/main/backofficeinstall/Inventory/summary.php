<?php
include '../includes/application_top.php';
require_once('functions.php');
handlePermissions();

$user = new BJUser();

$inventory_site_id = CURRENT_SITE_ID;

$places = BJHelper::getPlaces();
$place_names = array();
foreach ($places as $p) {
    $place_names[$p['id']] = $p['name'];
};
$sdate = $edate = '';
if (isset($_GET['sdate'])) {
    $sdate = $_GET['sdate'];
};
if (isset($_GET['edate'])) {
    $edate = $_GET['edate'];
};
$filter = 0;
if (isset($_GET['filter'])) {
    $filter = $_GET['filter'];
};
// get all item types
$sql = "SELECT * FROM inventory_items_types ORDER BY type_name ASC;";
$res = mysql_query($sql) or die(mysql_error());
$item_types = array();
$filter_found = false;
while ($row = mysql_fetch_assoc($res)) {
    $item_types[$row['id']] = $row['type_name'];
    if ($filter && $filter == $row['id']) {
        $filter_found = true;
    };
};
if (!$filter_found) {
    $filter = 0;
};
mysql_free_result($res);
$item_types_drop = array(
    array(
        'id'   => 0,
        'text' => 'All'
    )
);
foreach ($item_types as $id => $type) {
    $item_types_drop[] = array(
        'id'   => $id,
        'text' => $type
    );
};

$add_where = '';
$history = null;
$add_title = '';
if (!empty($sdate) && !empty($edate)) {
    $add_where = "and iit.created >= '$sdate' and iit.created <= '$edate'";
    $add_title = ' : ' . $sdate . ' to ' . $edate;
    if ($filter > 0) {
        $add_where .= ' and ii.type_id = ' . $filter;
    };
    // get item info
    // get history entries
    $history = array();
    $sql = "SELECT 
				tn.title as transaction_type, ii.item_name, iit.*, iit.units * iit.unit_cost as total_cost 
			FROM inventory_items_transactions iit LEFT JOIN inventory_items ii on (iit.item_id = ii.id), inventory_items_transaction_types tn
			WHERE
				iit.site_id = '$inventory_site_id' 
				and iit.type_id = 2
				and iit.approved = 1
				$add_where
				and iit.type_id = tn.id
			ORDER BY created ASC, id ASC;";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        $history[] = $row;
    };
} else {
    $sdate = $edate = date("Y-m-d");
};
$fields = array(
    'created'    => 'Date',
    'item_name'  => 'Item Name',
    'units'      => 'Units',
    'unit_cost'  => 'Cost per Unit',
    'total_cost' => 'Total Cost',
    'notes'      => 'Notes'
);
// income records
$action = $_POST['action'];
if (empty($action)) {
    $action = $_GET['action'];
};
switch (TRUE) {
    case ($action == 'download'):
        $d = str_replace("-", "", $sdate) . '-' . str_replace("-", "", $edate);
        $d .= '-';
        $d .= ($filter > 0) ? $item_types[$filter] : 'All';
        Header('Content-Type: text/csv; name="summary-' . $d . '.csv"');
        Header('Content-Disposition: attachment; filename="summary-' . $d . '.csv"');
        echo '"' . implode('","', array_values($fields)) . '"' . "\r\n";
        foreach ($history as $row) {
            $record = array();
            foreach ($fields as $f => $t) {
                $record[] = str_replace(array("\n", "\r"), " ", $row[$f]);
            };
            echo '"' . implode('","', $record) . '"' . "\r\n";
        };
        die();
        break;
};

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo strtoupper(SYSTEM_DATABASE_INFO); ?> - Distribution Summary <?php echo $add_title; ?></title>
    <?php include "../includes/head_scripts.php"; ?>
    <style>
        body * {
            box-sizing: border-box;
            -ms-box-sizing: border-box;
            -moz-box-sizing: border-box;
        }

        #history-container {
            width: 700px;
            margin-left: auto;
            margin-right: auto;

        }

        h1 {
            width: 100%;
            text-align: center;
            background-color: red;
            padding: 5px;
            border: 2px solid black;
        }

        .buttons-container {
            width: 100%;
        }

        #back {
            float: left;
        }

        #history-table {
            margin-top: 22px;
            border-collapse: collapse;
            width: 100%;
        }

        #history-table th {
            background-color: black;
            color: white;
            border: 1px solid black;
        }

        #history-table td {
            text-align: center;
            border: 1px solid black;
            padding: 3px;
        }

        #create, #download, #close {
            background-color: yellow;
            border: 1px solid black;
            cursor: pointer;
            height: 22px;
        }

        #create, #download {
            float: right;
        }

        label {
            margin-left: 30px;
            width: 200px;
            min-width: 200px;
            text-align: right;
        }

        input.datepicker {
            width: 90px;
            text-align: center;
        }

        td.notes {
            text-align: left !important;
        }

        td.total-owing {
            background-color: red;
            text-align: left !important;
        }

        form {
            display: inline;
        }
    </style>
    <script>
        $(document).ready(function () {
            $(".datepicker").datepicker({
                showAnim: 'fade',
                dateFormat: 'yy-mm-dd',
                currentText: 'Today'
            });
            $("input[name=sdate]").change(function () {
                $("input[name=edate]").val($(this).val());
            });
            $("#close").on('click', function () {
                window.opener.focus();
                window.close();
            });
        });
    </script>
</head>
<body>
<div id="history-container">
    <h1><?php echo strtoupper(SYSTEM_DATABASE_INFO); ?> Distribution Summary</h1>
    <div class="buttons-container">
        <form>
            <label for='sdate'>Start Date: </label>
            <input class="datepicker" name='sdate' id='sdate' value="<?php echo $sdate; ?>">
            <label for='edate'>End Date: </label>
            <input class="datepicker" name='edate' id='edate' value="<?php echo $edate; ?>">
            <label for='filter'>Type: </label>
            <?php echo draw_pull_down_menu('filter', $item_types_drop, $filter, 'id="filter"'); ?>
            <button type="submit" id="create">Create Report</button>
        </form>
    </div>
    <?php if (!is_null($history)) { ?>
        <table id="history-table">
            <?php
            echo "<tr>\n";
            $total = 0;
            foreach ($fields as $field_name => $field_text) {
                echo "\t<th class='$field_name'>$field_text</th>\n";
            };
            echo "</tr>\n";
            // history section
            foreach ($history as $entry) {
                echo "<tr>\n";
                foreach ($fields as $field_name => $field_text) {
                    $value = $entry[$field_name];
                    if ($field_name == 'total_cost') {
                        $total += $value;
                    };
                    echo "\t<td class='$field_name'>{$value}</td>\n";
                };
                echo "</tr>\n";
            };
            // history section
            if ($total > 0) {
                ?>
                <tr>
                    <td style="border: none !important;" colspan="<?php echo count($fields) - 2; ?>">&nbsp;</td>
                    <td><?php echo $total; ?></td>
                    <td class="total-owing">TOTAL Owing</td>
                </tr>
                <?php
            };
            ?>
        </table>
    <?php }; ?>
    <br/>
    <div class="buttons-container">
        <button type="button" id="close">Close</button>
        <?php if (!is_null($history)) { ?>
            <form>
                <input name=sdate type="hidden" value="<?php echo $sdate; ?>">
                <input name=edate type="hidden" value="<?php echo $edate; ?>">
                <input name=filter type="hidden" value="<?php echo $filter; ?>">
                <button type="submit" id="download" name=action value="download">Download Invoice</button>
            </form>
        <?php }; ?>
    </div>
    <br/>
    <br/>
    <br/>
</div>
</body>
