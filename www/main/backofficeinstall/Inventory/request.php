<?php
include '../includes/application_top.php';
require_once('functions.php');
handlePermissions();

/**
 * @param $id
 *
 * @return array
 */
function getItemForId($id)
{
    $id = db_input($id);
    $sql = "SELECT * FROM inventory_items WHERE id = $id";
    $res = mysql_query($sql) or die(mysql_error());
    if (!($item = mysql_fetch_assoc($res))) {
        echo "No item found.";
        die();
    }
    mysql_free_result($res);

    return $item;
}

/**
 * @param $recipients
 * @param $item
 */
function sendInventoryRequestEmail($item, $recipients)
{
    $recipientsString = implode(', ', $recipients);
    mail(
        $recipientsString,
        SYSTEM_SUBDOMAIN . " Inventory request",
        "Request from " . SYSTEM_SUBDOMAIN . " for " . $_POST['units'] . " x " . $item['item_name'] . "\n" .
        ((!empty($_POST['notes'])) ? 'Notes: ' . $_POST['notes'] : ''),
        "From: " . strtolower(SYSTEM_SUBDOMAIN) . "@bungyjapan.com"
    );
}

/**
 * @param $items
 * @param $user
 */
function performItemRequest($items, $user)
{
    $data = array(
        'item_id' => $_POST['item_id'],
        'created' => $_POST['date'],
        'type_id' => 2, // Distribute
        'notes'   => $_POST['notes'],
    );
    $needed = $_POST['units']; //The number of items requested
    foreach ($items as $item) {
        if ($needed == 0) break;//If we have managed to supply all of the needed items, break

        $distributed = ($needed < $item['total']) ? $needed : $item['total'];
        $needed -= $distributed;
        $data['parent_id'] = $item['id'];
        $data['units'] = -$distributed;
        $data['unit_cost'] = $item['unit_cost'];
        $data['site_id'] = 0;
        $data['d_site_id'] = CURRENT_SITE_ID;
        $data['approved'] = 0;
        $data['user_id'] = $user->getID();
        $data['user_name'] = $user->getUserName();
        $res = db_perform('inventory_items_transactions', $data);

        /*
        $fid = mysql_insert_id();
        $data['parent_id'] = 0;
        $data['spare_id'] = $fid;
        $data['units'] = $distributed;
        $data['site_id'] = CURRENT_SITE_ID;
        $data['approved'] = 0;
        db_perform('inventory_items_transactions', $data);
        $sid = mysql_insert_id();
        $data2 = array('spare_id' => $sid);
        db_perform('inventory_items_transactions', $data2, 'update', 'id=' . $fid);
        */
    }

    // If there is no items for all request, create transaction without parent ID to be visible in Head Office
    if ($needed > 0) {
        $data['parent_id'] = 0;
        $data['units'] = -$needed;
        $data['unit_cost'] = 0;
        $data['site_id'] = 0;
        $data['d_site_id'] = CURRENT_SITE_ID;
        $data['approved'] = 0;
        $data['user_id'] = $user->getID();
        $data['user_name'] = $user->getUserName();
        $res = db_perform('inventory_items_transactions', $data);
    };
}

/**
 * @param $id
 * @return array
 */
function getItemsAtHeadOffice($id)
{
    $id = db_input($id);

    $items = array();
    $sql = "
        SELECT
            iit.id,
            iit.created,
            iit.unit_cost,
            (iit.units + IFNULL(distributed, 0)) AS total
        FROM inventory_items_transactions iit
        LEFT JOIN (
          SELECT sum(iitd.units) AS distributed, 
              parent_id 
              FROM inventory_items_transactions iitd 
              GROUP BY parent_id
        ) iid
        ON (iit.id = iid.parent_id)
        WHERE iit.item_id = $id AND 
           iit.site_id = '0' AND 
           iit.units > ABS(IFNULL(distributed, 0)) AND 
           iit.parent_id = 0
        ORDER BY iit.created ASC, id ASC;";
    $res = mysql_query($sql) or die(mysql_error());
    $total = 0;

    while ($row = mysql_fetch_assoc($res)) {
        $items[] = $row;
        $total += $row['total'];
    };

    mysql_free_result($res);

    return array($items, $total);
}

/**
 * @return int
 */
function getItemIdFromPostOrGet()
{
    $id = (int)$_GET['id'];
    if (array_key_exists('item_id', $_POST)) {
        $id = (int)$_POST['item_id'];

        return $id;
    }

    return $id;
}

$user = new BJUser();
$inventory_site_id = CURRENT_SITE_ID;
$id = getItemIdFromPostOrGet();
$item = getItemForId($id);
list($items, $total) = getItemsAtHeadOffice($id);
// post processing
$action = '';
if (isset($_POST['action'])) {
    $action = $_POST['action'];
}

switch ($action) {
    case 'save':
    case 'Save':
        //sendInventoryRequestEmail($item, ["yu@bungyjapan.com", "hiroshi@bungyjapan.com"]);
        performItemRequest($items, $user);

        ?>
        <!DOCTYPE html>
        <html>
        <head>
            <?php include "../includes/head_scripts.php"; ?>
            <script>
                $(document).ready(function () {
                    window.opener.location.reload();
                    window.opener.focus();
                    window.close();
                });
            </script>
        </head>
        <body>
        </body>
        </html>
        <?
        exit();
        break;
}
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?= $item['item_name']; ?> Requisition</title>
    <?php include "../includes/head_scripts.php"; ?>
    <style>
        #distribute-container {
            width: 380px;
            overflow: hidden;
            margin-left: auto;
            margin-right: auto;
        }

        #distribute-container label {
            float: left;
            width: 150px;
            text-align: right;
            display: block;
            clear: both;
            height: 25px;
        }

        #distribute-container input, #distribute-container textarea, #distribute-container select {
            float: left;
            margin-left: 5px;
            width: 200px;
        }

        #distribute-date {
            width: 80px !important;
            text-align: center;
        }

        h1 {
            width: 100%;
            text-align: center;
        }

        #buttons-container {
            width: 100%;
        }

        #distribute-save-button {
            float: right;
        }

        #back {
            float: left;
        }
    </style>
    <script>
        $(document).ready(function () {
            $("#distribute-date").datepicker({
                showAnim: 'fade',
                dateFormat: 'yy-mm-dd',
                currentText: 'Today'
            });
            $("#back").on('click', function () {
                window.opener.focus();
                window.close();
            });
            $("#distribute-units").change(function () {
                var result = false;
                try {
                    var val = parseInt($(this).val());
                    if (val > 0) {//&& val <= <?= (int)$total; ?>
                        //This linebreak goes missing in Firefox
                        result = true;
                    }
                } catch (e) {
                    window.alert('Quantity for requisition must be numeric');
                    //window.alert('Quantity for requisition must be numeric and less or equal to <?= (int)$total; ?>');
                    $(this).focus();
                }
                ;
                if (!result) {
                    window.alert('Quantity for requisition must be numeric');
                    //window.alert('Quantity for requisition must be numeric and less or equal to <?= (int)$total; ?>');
                    $(this).focus();
                }
                $("#distribute-save-button").attr('disabled', !result);
                return result;
            });
        });
    </script>
</head>
<body>
<div id="distribute-container">
    <form method="post">
        <h1><?= $item['item_name']; ?></h1>
        <label for="distribute-date">Date:</label>
        <input type="text" name="date" value="<?= date("Y-m-d"); ?>" id="distribute-date">
        <label for="distribute-units">Units Required:</label>
        <input type="text" name="units" value="" id="distribute-units">
        <label for="distribute-notes">Notes:</label>
        <textarea name="notes" id="distribute-notes"></textarea>
        <div class="buttons-container">
            <button type="button" id="back">Close</button>
            <button type="submit" id="distribute-save-button" name="action" value="save">Save</button>
        </div>
        <input type="hidden" name="item_id" value="<?= $item['id']; ?>">
    </form>
</div>
</body>
