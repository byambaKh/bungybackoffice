<?php
include '../includes/application_top.php';
require_once('functions.php');
handlePermissions();

$user = new BJUser();
$inventory_site_id = CURRENT_SITE_ID;

// we will use main connect there
if (CURRENT_SITE_ID <> 0) die('This page can be accessed only from Head Office');
// post processing
// select all possible inventory places
$places = array();
// generic place - not in database
//$places[0] = array(
//'name'		=> 'Head Office',
//'subdomain'	=> 'main'
//);
$sql = "SELECT * FROM sites;";
$res = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_assoc($res)) {
    $places[$row['id']] = $row;
};
$inventory_site_id = CURRENT_SITE_ID;
$current_place_name = 'Unknown'; // for testing errors
if (array_key_exists($inventory_site_id, $places)) {
    $current_place_name = $places[$inventory_site_id]['name'];
};
// get all item types
$sql = "SELECT * FROM inventory_items_types ORDER BY type_name ASC;";
$res = mysql_query($sql) or die(mysql_error());
$item_types = array();
while ($row = mysql_fetch_assoc($res)) {
    $item_types[$row['id']] = $row['type_name'];
};
mysql_free_result($res);
// get existing items
$sql = "SELECT 
				ii.*, 
				iit.type_name as type, 
				sum(IFNULL(iitr.units, 0)) as on_hand,
				sum(IFNULL(iitr.units, 0) * cast(iitr.unit_cost as signed)) as total 
		FROM inventory_items ii
		LEFT JOIN inventory_items_transactions iitr on (ii.id = iitr.item_id and iitr.site_id = '$inventory_site_id' and approved = 1)
		LEFT JOIN inventory_items_types iit on (ii.type_id = iit.id)
		WHERE ii.deleted = 1
		GROUP BY ii.id
		ORDER BY ii.item_name ASC;";
$res = mysql_query($sql) or die(mysql_error());
$items = array();
while ($row = mysql_fetch_assoc($res)) {
    $items[] = $row;
};
mysql_free_result($res);
// compatibility
$edit_fields = array();

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo SYSTEM_SUBDOMAIN; ?> Inventory :: Deleted Items</title>
    <?php include "../includes/head_scripts.php"; ?>
    <style>
        #main-container {
            width: 98%;
            margin-left: auto;
            margin-right: auto;
            font-family: Arial;
            font-size: 12px;
        }

        h1 {
            width: 100%;
            text-align: center;
        }

        #items-table {
            width: 100%;
            border-collapse: collapse;
            background-color: white;
        }

        #items-table th {
            font-size: 10px;
            border: 1px solid black;
            background-color: black;
            color: white;
            padding: 3px;
        }

        #items-table td {
            border: 1px solid black;
            padding: 3px;
            overflow: hidden;
            text-align: center;
            font-size: 10px;
        }

        .empty-header {
            background-color: white !important;
            border: none !important;
        }

        .action {
            border: 2px solid black !important;
            font-size: 10px;
            padding: 5px !important;
            text-align: center;
            /*width: 60px;*/
            cursor: pointer;
        }

        .history {
            background-color: pink;
        }

        .purchase {
            background-color: palegreen;
        }

        .distribute {
            background-color: #AFEEEE;
        }

        .adjust {
            background-color: #FFFACD;
        }

        .edit, .update {
            background-color: #F4A460;
        }

        .delete, .cancel {
            background-color: #B390FB;
        }

        .request {
            background-color: palegreen;
        }

        .retire {
            background-color: #AFEEEE;
        }

        .purchase {
            background-color: palegreen;
        }

        .item_name {
            border-left: 2px solid black !important;
        }

        .notes {
            border-right: 2px solid black !important;
        }

        #items-table td.fields {
            border: none !important;
            border-top: 2px black solid !important;
        }

        #items-table td.actions {
            border: none !important;
        }

        .fields input, .fields select {
            width: 98%;
            height: 98%;
        }

        .value input, .value select {
            width: 98%;
            height: 98%;
        }

        #items-table td.item_name, #items-table td.notes {
            text-align: left;
        }

        #action-required {
            display: none;
        }

        #action-required table {
            border-collapse: collapse;
            width: 800px;
        }

        #action-required th {
            border: 1px solid black;
            font-size: 10px;
            padding: 2px;
            text-align: center;
        }

        #action-required td {
            border: 1px solid black;
            font-size: 10px;
            padding: 2px;
            text-align: center;
        }

        #action-required td.notes {
            text-align: left;
        }

        .id {
            width: 50px;
        }

        #item-hint {
            background-color: silver;
            padding: 5px;
            border-radius: 5px;
            border: 1px solid black;
            font-size: 10px;
        }

        #item-hint table {
            border-collapse: collapse;
        }

        #item-hint table th, #item-hint table td {
            text-align: center;
            min-width: 30px;
        }

        #item-hint .item-details-total td {
            border-top: 1px solid black;
            border-bottom: 4px double black;
        }

        .normal {
            background-color: #0f0;
        }

        .medium {
            background-color: yellow;
        }

        .low {
            background-color: red;
        }

        .deleted {
            color: red;
        }
    </style>
    <script>
        <?php /*
var detailed_items = <?php echo json_encode($detailed_items); ?>;
$(document).ready(function () {
	$('.purchase').on('click', function () {
		var item_id = $(this).parent().attr('custom-item-id');
		var purchase_window = window.open('purchase.php?id=' + item_id, 'bungyjapan_purchase', 'height=280px,width=500px,location=0,menubar=0,status=0,toolbar=0');
		purchase_window.focus();
	});
	$('.history').on('click', function () {
		var item_id = $(this).parent().attr('custom-item-id');
		var history_window = window.open('history.php?id=' + item_id, 'bungyjapan_history', 'height=500,width=900,location=0,menubar=0,status=0,toolbar=0');
		history_window.focus();
	});
	$('.distribute').on('click', function () {
		var item_id = $(this).parent().attr('custom-item-id');
		var distribute_window = window.open('distribute.php?id=' + item_id, 'bungyjapan_distribute', 'height=350,width=900,location=0,menubar=0,status=0,toolbar=0, scrollbars=yes');
		distribute_window.focus();
	});
	$('.request').on('click', function () {
		var item_id = $(this).parent().attr('custom-item-id');
		var request_window = window.open('request.php?id=' + item_id, 'bungyjapan_request', 'height=280px,width=500px,location=0,menubar=0,status=0,toolbar=0');
		request_window.focus();
	});
	$('.adjust').on('click', function () {
		var item_id = $(this).parent().attr('custom-item-id');
		var adjust_window = window.open('adjust.php?id=' + item_id, 'bungyjapan_adjust', 'height=280px,width=500px,location=0,menubar=0,status=0,toolbar=0');
		adjust_window.focus();
	});
	$('.delete').on('click', row_delete);
	$(".on_hand").mouseenter(show_item_hint);
	$(".on_hand").mousemove(move_item_hint);
	$(".on_hand").mouseout(hide_item_hint);
	$('#show-incomplete').click(show_incomplete);
	$('.ids').change(function () {
		if ($(".ids:checked").length > 0) {
			$("#approve").attr('disabled', false);
		} else {
			$("#approve").attr('disabled', 'disabled');
		};
	});
	//$('#info-message').sleep(3000).slideUp('slow');
});
function row_delete() {
	var item_id = $(this).parent().attr('custom-item-id');
	if (window.confirm('Are you sure that you want to delete this record?')) {
		document.location = '?action=delete&id=' + item_id;
	};
}
function move_item_hint(e) {
	if ($("#item-hint").length == 0) return;
	$("#item-hint").css({
		'left'	: e.pageX + 20,
		'top'	: e.pageY + 10
	});
}
function show_item_hint(e) {
	if (e.target.tagName != 'TD') return;
	$("#item-hint").remove();
	e.preventDefault();
	var id = $(this).parent().attr('custom-item-id');
	var hint = $('<div id="item-hint">');
	var totals = {};
	totals.units = 0;
	totals.total = 0
//alert(id + detailed_items.hasOwnProperty(id));
	if (detailed_items.hasOwnProperty(id) && typeof detailed_items[id] != 'null') {
		var t = $('<table>').append(
			$("<tr>").html("<th>On Hand</th><th>Cost</th><th>Total</th>")
		);
		for (i in detailed_items[id]) {
			if (!detailed_items[id].hasOwnProperty(i)) continue;
			t.append(
				$("<tr>").html("<td>"+detailed_items[id][i].total+"</td><td>"+detailed_items[id][i].unit_cost+"</td><td>"+(detailed_items[id][i].total*detailed_items[id][i].unit_cost)+"</td>")
			);
			totals.units += parseInt(detailed_items[id][i].total);
			totals.total += detailed_items[id][i].total*detailed_items[id][i].unit_cost;
		};
		if (totals.units > 0) {
			totals.unit_cost = Math.ceil(totals.total / totals.units);
		} else {
			totals.unit_cost = 0;
		};
		t.append(
			$("<tr class='item-details-total'>").html("<td>"+totals.units+"</td><td>"+totals.unit_cost+"</td><td>"+totals.total+"</td>")
		);
		hint.html(t);
		hint.css({
			'position'	: 'absolute',
			'left' : e.pageX + 20,
			'top' : e.pageY + 10
		});
		//alert(hint.html());
		$('body').append(hint);
	};
}
function hide_item_hint() {
	$("#item-hint").remove();
}
function show_incomplete() {
		$('#action-required').slideDown();
		$('#show-incomplete').html('Hide Pending').unbind('click').click(hide_incomplete);
}
function hide_incomplete() {
		$('#action-required').slideUp();
		$('#show-incomplete').html('Show Pending').unbind('click').click(show_incomplete);
}
*/ ?>
    </script>
</head>
<body>
<div id="main-container">
    <?php include "../includes/main_menu.php"; ?>
    <h1>Inventory - <?php echo $current_place_name; ?> - Deleted Items</h1>
    <br/>
    <form method="post">
        <table id="items-table">
            <?php
            // fields part
            $fields = array(
                'item_name' => 'Item'
            );
            if (CURRENT_SITE_ID == 0) {
                $admin_fields = array(
                    'item_no'  => 'Item No.',
                    'supplier' => 'Supplier'
                );
                $fields = array_merge($fields, $admin_fields);
            };
            $fields = array_merge($fields, array(
                'unit'            => 'Unit',
                'unit_cost'       => 'Unit Cost',
                'units_per_order' => 'Units per Order',
                'type'            => 'Type',
                'target_level'    => 'Target Level',
                'on_hand'         => 'On Hand',
                'notes'           => 'Notes',
            ));
            // actions part
            $actions = array('history' => 'History');
            if ($inventory_site_id == 0) {
                $actions = $actions = array_merge($actions, array(
                    'purchase'   => 'Purchase',
                    'distribute' => 'Distribution',
                    'adjust'     => 'Adjustment',
                    'edit'       => 'Edit',
                    'delete'     => 'Delete'
                ));
            } else {
                $actions = $actions = array_merge($actions, array(
                    'request' => 'Requisition',
                    //'retire'	=> 'Retirement',
                    'adjust'  => 'Adjustment'
                ));
            };
            $actions = array_merge($actions, array());
            $actions = array();
            // headers
            echo "<tr>\n";
            foreach ($fields as $field_name => $field_text) {
                echo "\t<th class='$field_name'>$field_text</th>\n";
            };
            echo "\t<th colspan='" . (count($actions) + 1) . "' class='empty-header'>&nbsp;</th>\n";
            echo "</tr>\n";
            // eof headers
            // items section
            foreach ($items as $item) {
                echo "<tr custom-item-id='{$item['id']}'>\n";
                foreach ($fields as $field_name => $field_text) {
                    $add_class = "";
                    $rel = '';
                    if ($field_name == 'on_hand') {
                        switch (TRUE) {
                            case ($item['on_hand'] < 2):
                                $add_class = " low";
                                break;
                            case ($item['on_hand'] < $item['target_level']):
                                $add_class = " medium";
                                break;
                            default:
                                $add_class = " normal";
                                break;
                        };
                    };
                    if ($field_name == 'unit_cost') {
                        if ($item['on_hand'] != 0) {
                            $item[$field_name] = ceil($item['total'] / $item['on_hand']);
                        };
                    } elseif (CURRENT_SITE_ID == 0 && in_array($field_name, $edit_fields)) {
                        $add_class .= " value";
                        $rel = " rel='$field_name'";
                    };
                    $add_text = '';
                    if (FALSE && $field_name == 'notes' && $item['deleted'] == 1) {
                        $add_text = ' <span class="deleted">Pending Deletion</span>';
                    };
                    echo "\t<td class='$field_name$add_class'$rel>{$item[$field_name]}$add_text</td>\n";
                };
                echo "\t<td class='empty-header'>&nbsp;</td>\n";
                foreach ($actions as $action => $text) {
                    echo "\t<td class=\"action $action\">$text</td>\n";
                };
                echo "</tr>\n";
            };
            // items section
            // headers
            echo "<tr>\n";
            foreach ($fields as $field_name => $field_text) {
                echo "\t<th class='$field_name'>$field_text</th>\n";
            };
            echo "\t<th colspan='" . (count($actions) + 1) . "' class='empty-header'>&nbsp;</th>\n";
            echo "</tr>\n";
            // eof headers

            ?>
        </table>
    </form>
    <button onClick="javascript: document.location='/Inventory/'" id="back">Back</button>
    <?php include("ticker.php"); ?>
</div>
</body>
</html>
