<?php
include '../includes/application_top.php';
require_once('functions.php');
handlePermissions();
/**
 * @param $id
 * @return array
 */
function getItemInfo($id)
{
// get item info
    $id = db_input($id);
    $sql = "SELECT * FROM inventory_items WHERE id = $id";
    $res = mysql_query($sql) or die(mysql_error());
    if (!($item = mysql_fetch_assoc($res))) {
        echo "No item found.";
        die();
    }

    mysql_free_result($res);

    return $item;
}

/**
 * @param $id
 * @param $inventory_site_id
 *
 * @return array
 */
function getHistory($id, $inventory_site_id)
{
// get history entries
    $history = [];
    $sql = "SELECT 
			tn.title as transaction_type, iitd.site_id as d_id, iit.* 
		FROM inventory_items_transactions iit LEFT JOIN inventory_items_transactions iitd on (iit.spare_id = iitd.id), inventory_items_transaction_types tn
		WHERE iit.item_id = $id
			and iit.site_id = '$inventory_site_id' and iit.approved = 1
			and iit.type_id = tn.id
		ORDER BY created DESC, id DESC;";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        $history[] = $row;
    }

    return $history;
}

$inventory_site_id = CURRENT_SITE_ID;
$itemId = (int)$_GET['id'];
$item = getItemInfo($itemId);
$history = getHistory($itemId, $inventory_site_id);

$places = BJHelper::getPlaces();
$place_names = array();

foreach ($places as $p) {
    $place_names[$p['id']] = $p['name'];
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo $item['item_name']; ?> - History</title>
    <?php include "../includes/head_scripts.php"; ?>
    <style>
        #history-container {
            width: 100%;
            padding-left: 1%;
            padding-right: 1%;

        }

        h1 {
            width: 100%;
            text-align: center;
        }

        #buttons-container {
            width: 98%;
        }

        .back {
            float: left;
        }

        #history-table {
            border-collapse: collapse;
            border: 2px solid black;
            width: 98%;
        }

        #history-table th {
            background-color: black;
            color: white;
        }

        #history-table td {
            text-align: center;
            border: 1px solid black;
            padding: 3px;
        }
    </style>
    <script>
        $(document).ready(function () {
            $("#purchase-date").datepicker({
                showAnim: 'fade',
                dateFormat: 'yy-mm-dd',
                currentText: 'Today'
            });
            $(".back").on('click', function () {
                window.opener.focus();
                window.close();
            });
        });
    </script>
</head>
<body>
<div id="history-container">
    <h1>History : <?php echo $item['item_name']; ?></h1>
    <div class="buttons-container">
        <button type="button" class="back">Close</button>
    </div>
    <table id="history-table">
        <?php
        $fields = array(
            'd_id'             => 'Site Name',
            'transaction_type' => 'Transaction',
            'created'          => 'Date',
            'units'            => 'Units',
            'unit_cost'        => 'Cost per Unit',
            'notes'            => 'Notes'
        );

        echo "<tr>\n";
        foreach ($fields as $field_name => $field_text) {
            echo "\t<th class='$field_name'>$field_text</th>\n";
        }
        echo "</tr>\n";
        // history section
        foreach ($history as $entry) {
            echo "<tr>\n";
            foreach ($fields as $field_name => $field_text) {
                $value = $entry[$field_name];
                if ($field_name == 'd_id') {
                    $value = (!empty($value)) ? $place_names[$value] : '';
                }
                echo "\t<td class='$field_name'>{$value}</td>\n";
            }
            echo "</tr>\n";
        }
        // history section
        ?>
    </table>
    <div class="buttons-container">
        <button type="button" class="back" id="back">Close</button>
    </div>
</div>
</body>
