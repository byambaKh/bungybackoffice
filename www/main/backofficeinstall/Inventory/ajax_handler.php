<?php
include "../includes/application_top.php";
require_once('functions.php');
handlePermissions();
$user = new BJUser();
$inventory_site_id = CURRENT_SITE_ID;
$action = db_input($_GET['action']);

$type = $_GET['type'];
if (empty($type)) {
    $type = 'inventory_items';
}

if ($type == 'payroll') {
    $_POST['date'] = $_GET['date'] . '-' . $_POST['day'];
    unset($_POST['day']);
}

/**
 * @param $type
 *
 * @return array
 */
function updateLevel($type)
{
    $sql = "SELECT * FROM inventory_items_levels WHERE site_id = '" . CURRENT_SITE_ID . "' and item_id = '{$_POST['id']}';";
    $res = mysql_query($sql) or die(mysql_error());
    $action = 'insert';
    $where = '';
    $data = array(
        'site_id'      => CURRENT_SITE_ID,
        'item_id'      => $_POST['id'],
        'target_level' => $_POST['target_level']
    );

    if ($row = mysql_fetch_assoc($res)) {
        $action = 'update';
        $where = 'id=' . $row['id'];
    }
    db_perform('inventory_items_levels', $data, $action, $where) or die(mysql_error());
    // notes section
    $data = array('notes' => $_POST['notes']);
    db_perform($type, $_POST, 'update', 'id=' . $_POST['id']);
    $result = array('result' => 'success');

    return array($action, $result);
}

/**
 * @param $type
 *
 * @return array
 */
function updateRow($type)
{
    $id = $_POST['id'];
    unset($_POST['id']);
    $_POST['type_id'] = $_POST['type'];
    unset($_POST['type']);
    db_perform($type, $_POST, 'update', 'id=' . $id) or die(mysql_error());
    $result = array('result' => 'success');

    return $result;
}

/**
 * @param $type
 *
 * @return array
 */
function deleteRow($type)
{
    $sql = "delete from $type WHERE id = {$_POST['id']};";
    mysql_query($sql);
    $result = array('result' => 'success');

    return $result;
}

switch (TRUE) {
    case ($action == 'update_level') :
        list($action, $result) = updateLevel($type);
        break;

    case ($action == 'update_row') :
        $result = updateRow($type);
        break;

    case ($action == 'delete_row') :
        $result = deleteRow($type);
        break;
}

$result['request'] = array(
    'action' => $action,
    'type' => $type,
    'post' => $_POST
);

echo json_encode($result);
