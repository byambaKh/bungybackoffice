<?php
include '../includes/application_top.php';
require_once('functions.php');
handlePermissions();

$user = new BJUser();

// post processing
$action = '';
if (isset($_POST['action'])) {
    $action = $_POST['action'];
}

/**
 * @param $user
 *
 * @return array
 */
function performPurchase($user)
{
    $data = array(
        'parent_id' => 0,
        'item_id'   => $_POST['item_id'],
        'units'     => $_POST['order_units'],
        'unit_cost' => $_POST['order_cost'] / $_POST['order_units'],
        'site_id'   => CURRENT_SITE_ID,
        'approved'  => 1,
        'created'   => $_POST['order_date'],
        'type_id'   => 1, // Purchase
        'notes'     => $_POST['order_notes'],
        'user_id'   => $user->getID(),
        'user_name' => $user->getUserName(),
    );
    db_perform('inventory_items_transactions', $data);

    return $data;
}

/**
 * @return array
 */
function getItemInfo()
{
// get item info
    $id = (int)$_GET['id'];
    $sql = "SELECT * FROM inventory_items WHERE id = $id";
    $res = mysql_query($sql) or die(mysql_error());
    if (!($item = mysql_fetch_assoc($res))) {
        echo "No item found.";
        die();
    }
    mysql_free_result($res);

    return $item;
}

/**
 * @param $itemId
 * @param $purchase_id
 */
function splitPurchaseAcrossDistributionRequests($itemId, $purchase_id)
{
// check for requests without parent_id for this item ID and assign them new parent_id
    $sql = "SELECT iitt.title as tname, iit.*, ii.item_name
				FROM inventory_items_transactions iit
				LEFT JOIN inventory_items ii on (iit.item_id = ii.id)
				LEFT JOIN inventory_items_transaction_types iitt on (iit.type_id = iitt.id)
				WHERE
					iit.site_id = '" . CURRENT_SITE_ID . "' 
					and iit.item_id = '$itemId' 
					and iit.approved = 0 
					and iit.type_id = 2 
					and iit.parent_id = 0 
				ORDER BY iit.created ASC;
			";
    $res = mysql_query($sql) or die(mysql_error());
    $available = $_POST['order_units'];
    while ($row = mysql_fetch_assoc($res)) {
        if (abs($row['units']) <= $available) {
            $data = array(
                'parent_id' => $purchase_id,
                'unit_cost' => $_POST['order_cost'] / $_POST['order_units']
            );
            db_perform('inventory_items_transactions', $data, 'update', 'id=' . $row['id']);
            $available -= abs($row['units']);
        }
    }
}

switch ($action) {
    case 'save':
    case 'Save':
        $data = performPurchase($user);

        $itemId = $_POST['item_id'];
        $purchase_id = mysql_insert_id();

        splitPurchaseAcrossDistributionRequests($itemId, $purchase_id);


        ?>
        <!DOCTYPE html>
        <html>
        <head>
            <?php include "../includes/head_scripts.php"; ?>
            <script>
                $(document).ready(function () {
                    window.opener.location.reload();
                    window.opener.focus();
                    window.close();
                });
            </script>
        </head>
        <body>
        </body>
        </html>
        <?php
        die();
        break;
};
$item = getItemInfo();

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?= $item['item_name']; ?> Purchase</title>
    <?php include "../includes/head_scripts.php"; ?>
    <style>
        #purchase-container {
            width: 380px;
            overflow: hidden;
            margin-left: auto;
            margin-right: auto;
        }

        #purchase-container label {
            float: left;
            width: 150px;
            text-align: right;
            display: block;
            clear: both;
            height: 25px;
        }

        #purchase-container input, #purchase-container textarea {
            float: left;
            margin-left: 5px;
            width: 200px;
        }

        #purchase-date {
            width: 80px !important;
            text-align: center;
        }

        h1 {
            width: 100%;
            text-align: center;
        }

        #buttons-container {
            width: 100%;
        }

        #purchase-save-button {
            float: right;
        }

        #back {
            float: left;
        }
    </style>
    <script>
        $(document).ready(function () {
            $("#purchase-date").datepicker({
                showAnim: 'fade',
                dateFormat: 'yy-mm-dd',
                currentText: 'Today'
            });
            $("#back").on('click', function () {
                window.opener.focus();
                window.close();
            });
        });
    </script>
</head>
<body>
<div id="purchase-container">
    <form method="post">
        <h1><?= $item['item_name']; ?></h1>
        <label for="purchase-date">Date:</label>
        <input type="text" name="order_date" value="<?= date("Y-m-d"); ?>" id="purchase-date">
        <label for="purchase-units">Units Purchased:</label>
        <input type="text" name="order_units" value="" id="purchase-units">
        <label for="purchase-cost">Total Cost:</label>
        <input type="text" name="order_cost" value="" id="purchase-cost">
        <label for="purchase-notes">Notes:</label>
        <textarea name="order_notes" id="purchase-notes"></textarea>
        <div class="buttons-container">
            <button type="button" id="back">Close</button>
            <button type="submit" id="purchase-save-button" name="action" value="save">Save</button>
        </div>
        <input type="hidden" name="item_id" value="<?= $item['id']; ?>">
    </form>
</div>
</body>
