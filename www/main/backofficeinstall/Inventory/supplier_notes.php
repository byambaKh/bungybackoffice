<?php
require_once('../includes/application_top.php');
$itemId = null;
//GET specifies the itemId for viewing the page
if(isset($_GET['id'])) $itemId = db_input($_GET['id']);

//clean the variables before query

//POST is being used to submit data for saving
/**
 * @param $itemId
 *
 * @return string
 */
function updateSupplierNotes($itemId)
{
    $supplierNotes = db_input($_POST['supplier_notes']);
    $sql = "update inventory_items set supplier_notes = '$supplierNotes' where id = $itemId;";
    mysql_query($sql);
}

if(isset($_POST['id'])) {
    updateSupplierNotes($itemId);
}

/**
 * @param $itemId
 *
 * @return array
 */
function getItemInformation($itemId)
{
    $sql = "SELECT * FROM inventory_items WHERE id = $itemId;";
    $res = mysql_query($sql);
    $item = mysql_fetch_assoc($res);

    return $item;
}

$item = getItemInformation($itemId);
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?= $item['item_name']; ?> Adjustment</title>
    <?php include "../includes/head_scripts.php"; ?>
    <style>
        #supplier-notes{
            width: 100%;
            height: 100%;
            font-size: 2em;
            white-space:pre-wrap;
            background-color: white;
        }
        #save{
            width: 100%;
            height: 3em;
            font-size: 2.5em;
            background-color: #5cb85c;
            color: white;
        }

        #save:hover{

        }
    </style>
    <title>Supplier Notes</title>
</head>
<body>
<div id="purchase-container">
    <form method="post">
        <h1><?= $item['item_name']; ?></h1>
        <h2>Supplier Notes:</h2>
        <textarea name="supplier_notes" id="supplier-notes" rows="15" ><?= $item['supplier_notes']?></textarea>

        <div class="buttons-container">
            <button type="submit" id="save">Save</button>
        </div>
        <input type="hidden" name="id" value="<?= $item['id']; ?>">
    </form>
</div>
</body>
