<?php
require_once('../includes/application_top.php');
?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="stylesheet.css">
</head>
<body>
<?php
$temp = explode(".", $_FILES["file"]["name"]);
$originalName = $_FILES["file"]["name"];
$extension = end($temp);

$uploadDirectory = $_SERVER['DOCUMENT_ROOT'].'/uploads/uniforms/';

if ($extension == "xls") {
	if ($_FILES["file"]["error"] > 0) {
		//echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
	} else {
        $storageName = $user->getID()."_".date('YmdHis').".xls";//A unique name for storing the file
		move_uploaded_file($_FILES["file"]["tmp_name"], $uploadDirectory . $storageName);
        $user = new BJUser();

        $data = array(
            'originalName' => $originalName,
            'storageName' => $storageName,
            'filename' => 'Standard-Move-uniforms-10-2018.xls',
            'userId' => $user->getID(),
            'dateTime' => date('Y-m-d H:i:s'),
            'category' => 'Uniforms'
        );

        db_perform('uploads', $data);

		echo "Uniforms Document Successfully Uploaded.<br> ";
		echo "<a href = 'index.php'>&lt;&lt; Back</a> ";
	}

} else {
	echo "Invalid file";
}
?> 
</body>
</html>

