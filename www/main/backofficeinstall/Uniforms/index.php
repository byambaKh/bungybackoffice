<?php
require '../includes/application_top.php';
//require_once("../includes/pageParts/standardHeader.php");
echo Html::head("Head Office - Uniforms", array("main_menu.css", "companyCharter.css"));
?>
<div>
	<img src="../img/index.jpg">
	<h1>Head Office <br> Uniforms Spreadsheet</h1>

	<?php 
	echo Html::menuPageLink("Download Latest", "downloadFile.php?latest=true");
    echo Html::menuPageLink("Older Versions", "oldVersions.php");
    echo Html::menuPageLink("-", "#");
	echo Form::menuPageUpload("Upload", "uploadFile.php");
	echo Html::menuPageLink("Log Out", "/?logout=true");
	echo Html::menuPageLink("Back", "http://main.".SYSTEM_DOMAIN."/menu.php") ;
	?>
</div>
<?php require_once("../includes/pageParts/standardFooter.php");?>
