<?php
	require_once("../includes/application_top.php");

    $user = new BJUser();
    $edit = false;
    //if the user is an admin or GM send them to the edit page for the help
    if($user->hasRole(array('SysAdmin', 'General Manager'))) {
       header('location: /Manage/manageHelpPages.php?action=edit&id='.$_GET['id']);
    }

	mysql_query("SET NAMES 'utf-8';");
	if (isset($_GET['topic']) && !empty($_GET['topic'])) {
		$sql = "SELECT * FROM help where url = '".mysql_real_escape_string($_GET['topic'])."';";
		$res = mysql_query($sql) or die(mysql_error());
		if ($row = mysql_fetch_assoc($res)) {
			$_GET['id'] = $row['id'];
		};
	};
	$id = (int)$_GET['id'];
	$sql = "SELECT * FROM help WHERE id = $id;";
	$res = mysql_query($sql) or die(mysql_error());
	if ($row = mysql_fetch_assoc($res)) {
	} else {
		$row = array(
			'title'		=> 'No help',
			'content'	=> 'There is no help for this page'
		);
	};
	mysql_free_result($res);
	if (isset($_GET['action']) && $_GET['action'] == 'back_exists') {
		$data = array(
			'back_exists'	=> $_GET['back_exists']
		);
		db_perform('help', $data, 'update', 'id='.(int)$_GET['id']);
		die();
	};
?>
<!DOCTYPE html>
<html>
  <head>
<?php require_once('includes/header_tags.php'); ?>
	<meta charset="utf-8" />
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<title><?php echo SYSTEM_SUBDOMAIN; ?> Help :: <?php echo $row['title']; ?> </title>
<style>
body {
	color: white;
}
h1 {
    text-align: center;
}

</style>
</head>
<body bgcolor=black>
<div>
<h1><?php echo $row['title']; ?></h1>
<br><br>
<?php echo str_replace("\n", "<br />", base64_decode($row['content'])); ?>
</div>
</body></html>
