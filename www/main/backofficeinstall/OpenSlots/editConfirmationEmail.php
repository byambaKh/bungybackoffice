<?php
	include ("../includes/application_top.php");
	mysql_query("SET NAMES UTF8;");
	$site_id = CURRENT_SITE_ID;
    $types = array(
        'confirmation_email'            => 'User Confirmation Email',
        'confirmation_email_paid'       => 'User Confirmation Email Paid',
    );

    // select DB templates
    $templates = array();
    $sql = "SELECT * FROM templates WHERE site_id = '$site_id' and template_type in ('".implode("','", array_keys($types))."');";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        $templates[$row['template_type']][$row['lang']] = $row;
        $content[$row['lang']][$row['template_type']] = $row['template_content'];
    };
    mysql_free_result($res);
    // if there is a POST,save and go to management console
    if (!empty($_POST)) {
        foreach (array('en', 'jp') as $lang) {
            $$lang = $_POST[$lang];
            foreach ($$lang as $type => $content) {
                // DB save
                $data = array(
                    'template_content'  => stripslashes(implode("\n", $content))
                );
                if (array_key_exists($type, $templates) && array_key_exists($lang, $templates[$type])) {
                    db_perform('templates', $data, 'update', "id = '{$templates[$type][$lang]['id']}'");
                } else {
                    $data['site_id'] = $site_id;
                    $data['lang'] = $lang;
                    $data['template_type'] = $type;
                    db_perform('templates', $data);
                };
            };
        };
        Header("Location: index.php");
        die();
    };
//load default templates
    if (empty($templates)) {
        $sql = "SELECT * FROM templates WHERE site_id = '0' and template_type in ('".implode("','", array_keys($types))."');";
        $res = mysql_query($sql) or die(mysql_error());
        while ($row = mysql_fetch_assoc($res)) {
            $templates[$row['template_type']][$row['lang']] = $row;
            $content[$row['lang']][$row['template_type']] = $row['template_content'];
        };
        mysql_free_result($res);
    };

	// load Templates
	foreach (array('en', 'jp') as $lang) {
        if (array_key_exists($lang, $content) && array_key_exists('confirmation_email', $content[$lang])) {
            $file = explode("\n", $content[$lang]['confirmation_email']);

		//if ($file = file("../includes/templates/email/$lang.php")) {
			$$lang = array(
				'subject'	=> array_shift($file),
				'body'		=> implode("", $file)
			);
		} else {
			$$lang = array(
				'subject'	=> 'No template found',
				'body'		=> 'No template file found'
			);
		}
	};

?>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">

<script src="js/functions.js" type="text/javascript"></script>

<?php include '../includes/head_scripts.php'; ?>
<style>
#total-container * {
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -ms-box-sizing: border-box;
    font-family: helvetica,sans-serif;
    font-size: 12px;
}
#total-container h1 {
	font-size: 20px;
}
#total-container {
    width: 700px;
    text-align: center;
    margin-left: auto;
    margin-right: auto;
}
#total-container * div {
	width: 100%;
}
#total-container * input {
	width: 100%;
}
#total-container * textarea {
	width: 100%;
	height: 300px;
}
#total-container div.field-caption {
	text-align: left;
}
#buttons {
    clear: both;
    margin-top: 30px;
    margin-bottom: 60px;
    height: 49px;
    border: 1px solid #EEEEEE;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}
#buttons button {
    float: right;
    background-color: #FFFF66;
    border: 1px solid black;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    font-size: 14pt;
    padding: 5px;
    margin: 5px;
    padding-left: 10px;
    padding-right: 10px;
}
#buttons #back {
    float:left;
}
</style>

<title>Bungy Japan :: User Management</title>

<script>
$(document).ready(function () {
    $('#back').click(function () {
        document.location = 'index.php';
        return false;
    });
    $('#save').click(function () {
        $('#emailTemplateForm').submit();
        return true;
    });
});
</script>

</head>
<body>
<?php include '../includes/main_menu.php'; ?>
<br />
<form method="post" name="emailForm" id="emailTemplateForm">
<div id="total-container">
	<h1> Unpaid: User Confirmation Email Template</h1>
	<div class="field-caption">English email Subject</div>
	<div class="field-value"><input name="en[confirmation_email][subject]" value="<?php echo htmlspecialchars($en['subject']); ?>"></div>
	<div class="field-caption">English email Body</div>
	<div class="field-value textarea"><textarea name="en[confirmation_email][body]"><?php echo htmlspecialchars($en['body']); ?></textarea></div>
	<div class="field-caption">Available variables: {EMAIL_BOOK_DATE}, {EMAIL_BOOK_TIME}, {EMAIL_JUMP_NUMBER}, {EMAIL_CUSTOMER_NAME}, {EMAIL_CUSTOMER_PHONE}, {EMAIL_CONFIRMATION_LINK}</div>
	<br />
	<br />
	<div class="field-caption">Japanese email Subject</div>
	<div class="field-value"><input name="jp[confirmation_email][subject]" value="<?php echo htmlspecialchars($jp['subject']); ?>"></div>
	<div class="field-caption">Japanese email Body</div>
	<div class="field-value textarea"><textarea name="jp[confirmation_email][body]"><?php echo htmlspecialchars($jp['body']); ?></textarea></div>
	<div class="field-caption">Available variables: {EMAIL_BOOK_DATE}, {EMAIL_BOOK_TIME}, {EMAIL_JUMP_NUMBER}, {EMAIL_CUSTOMER_NAME}, {EMAIL_CUSTOMER_PHONE}, {EMAIL_CONFIRMATION_LINK}</div>
  	<div id="buttons">

<?php //safest hack to display both fields ... the page should be
foreach (array('en', 'jp') as $lang) {
    if (array_key_exists($lang, $content) && array_key_exists('confirmation_email_paid', $content[$lang])) {
        $file = explode("\n", $content[$lang]['confirmation_email_paid']);

        //if ($file = file("../includes/templates/email/$lang.php")) {
        $$lang = array(
            'subject' => array_shift($file),
            'body' => implode("", $file)
        );
    } else {
        $$lang = array(
            'subject' => 'No template found',
            'body' => 'No template file found'
        );
    }
}
?>

    <h1> Credit Card Paid: User  Confirmation Email Template</h1>
    <div class="field-caption">English email Subject</div>
    <div class="field-value"><input name="en[confirmation_email_paid][subject]" value="<?php echo htmlspecialchars($en['subject']); ?>"></div>
    <div class="field-caption">English email Body</div>
    <div class="field-value textarea"><textarea name="en[confirmation_email_paid][body]"><?php echo htmlspecialchars($en['body']); ?></textarea></div>
    <div class="field-caption">Available variables: {EMAIL_BOOK_DATE}, {EMAIL_BOOK_TIME}, {EMAIL_JUMP_NUMBER}, {EMAIL_CUSTOMER_NAME}, {EMAIL_CUSTOMER_PHONE}, {EMAIL_CONFIRMATION_LINK}</div>
    <br />
    <br />
    <div class="field-caption">Japanese email Subject</div>
    <div class="field-value"><input name="jp[confirmation_email_paid][subject]" value="<?php echo htmlspecialchars($jp['subject']); ?>"></div>
    <div class="field-caption">Japanese email Body</div>
    <div class="field-value textarea"><textarea name="jp[confirmation_email_paid][body]"><?php echo htmlspecialchars($jp['body']); ?></textarea></div>
    <div class="field-caption">Available variables: {EMAIL_BOOK_DATE}, {EMAIL_BOOK_TIME}, {EMAIL_JUMP_NUMBER}, {EMAIL_CUSTOMER_NAME}, {EMAIL_CUSTOMER_PHONE}, {EMAIL_CONFIRMATION_LINK}</div>
    <div id="buttons">

    <button id="back">Back</button>
    <button id="save">Save</button>
  	</div>

</div>
</form>
</body>
</html>
