<?php
include "../includes/application_top.php";

$site_id = CURRENT_SITE_ID;
$bDates = getCalendarState($site_id);
$_SESSION['start_date'] = $_POST['start_date'];
$_SESSION['end_date'] = $_POST['end_date'];

//Allows us to open up the days for editing without showing them to the customer in the calendar
?>
<!DOCTYPE html>
<html>
<head>
    <?php include 'includes/header_tags.php'; ?>
    <title></title>
    <?php include '../includes/head_scripts.php'; ?>
    <script type="text/javascript" charset="utf-8">
        $(function () {
            $mydate1 = $('#start_date').datepicker({
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd',
                altField: '#alternate',
                altFormat: 'yy-mm-dd',
                minDate: '0d',
                maxDate: new Date(<?php echo $maxDate?>),
                beforeShowDay: checkAvailability

            });
            $mydate2 = $('#end_date').datepicker({
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd',
                altField: '#alternate',
                altFormat: 'yy-mm-dd',
                beforeShow: changeToDate,
                minDate: '0d',
                maxDate: new Date(<?php echo $maxDate?>),
                beforeShowDay: checkAvailability

            });

        });
        var $myBadDates = new Array(<?php
                foreach ($bDates as $disabledate) {
                    echo " \"$disabledate\",";
                };
                 echo " 1";
            ?>);
        //                var $cntBookCal = new Array(<?php $bookDates[0]; ?>);
        function checkAvailability(mydate) {
            var $return = true;
            var $returnclass = "available";
            //$checkdate = $.datepicker.formatDate('dd/mm/yy', mydate);
            $checkdate = $.datepicker.formatDate('yy-mm-dd', mydate);
            for (var i = 0; i < $myBadDates.length; i++) {
                if ($myBadDates[i] == $checkdate) {
                    $returnclass = "unavailable";
                    return [$return, $returnclass, "Dates Locked"];
                }
            }
            return [$return, $returnclass];
        }

        function changeToDate(el, inst) {
            var o = inst.settings;
            if ($('#end_date').val() < $('#start_date').val()) {
                $('#end_date').val('');
                o.defaultDate = $('#start_date').val();
            }
            return o;
        }
        function turnOFF() {
            if (document.getElementById('offdays')) {
                //alert("DAY OFF");
                document.calcForm.action = "procOFF.php";
                document.calcForm.submit();
                return true;
            }

        }
        function turnON() {
            if (document.getElementById('ondays')) {
                //alert("DAY ON");
                document.calcForm.action = "procON.php";
                document.calcForm.submit();
                return true;
            }
        }
        function mgmtConsole() {
            if (document.getElementById('console')) {
                document.calcForm.action = "index.php";
                document.calcForm.submit();
                return true;
            }
        }
    </script>
    <style>
        .unavailable a.ui-state-default {
            background: none;
            background-color: red;
        }
    </style>
</head>
<body>
<?php include '../includes/main_menu.php'; ?><br>

<form action="" id="calcForm" method="post" name="calcForm">

    <table align="center" cellpadding="0" cellspacing="0" width="310px">
        <thead>
        <tr>
            <th></th>
            <th></th>
        </tr>
        </thead>
        <tbody>
        <tr>
            <td align="center" bgcolor="#8FBC8F" colspan="4" valign="middle">
                <h3>Process Jump Days</h3>
            </td>
        </tr>
        <tr>
            <td align="right" bgcolor="khaki">From Date:&nbsp;&nbsp;&nbsp;&nbsp;</td>

            <td bgcolor="khaki">
                <input type="text" name="start_date" id="start_date" size="14" readonly="readonly" value="<?php $_SESSION['start_date']; ?>"/>
            </td>
            <td bgcolor="khaki">

            </td>
        </tr>
        <tr>
            <td align="right" bgcolor="khaki">To Date:&nbsp;&nbsp;&nbsp;&nbsp;</td>

            <td bgcolor="khaki">
                <input type="text" name="end_date" id="end_date" size="14" readonly="readonly"/>
            </td>
            <td bgcolor="khaki">

            </td>
        </tr>
        <tr>
        </tr>
    </table>
    <table align="center" cellpadding="0" cellspacing="0" width="310px">
        <tbody>
        <tr>
            <td align="center" bgcolor="#8FBC8F" colspan="4" valign="middle">
                <input type="button" style="background-color:#FFFF66" value="Turn OFF Days" id="offdays" onclick="turnOFF();">
                <input type="button" style="background-color:#FFFF66" value="Turn ON Days" id="ondays" onclick="turnON();">
                <input type="button" style="background-color:#FFFF66" value="Back" id="console" onclick="mgmtConsole();">
                <!-- <a href="http://standardmove.com/admin/procOFF.php?start_date=<?php print $_SESSION['start_date']; ?>&end_date=<?php print $_SESSION['end_date']; ?>">GO</a> -->
            </td>
        </tr>
    </table>
</form>
</body>
</html>
