<?php
	include '../includes/application_top.php';

	$bDates = getCalendarState();
?>
<!DOCTYPE html>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">
<title>Process Jump Time</title>
<style type="text/css">

#d1 {
    display: none;
}

</style>
<style>
.unavailable span.ui-state-default {
    background-image: none;
    background-color:red;
}
td.ui-state-disabled.unavailable {
	opacity: 1;
}
</style>
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
<script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
<link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" type="text/css" media="all"/>
<link rel="stylesheet" href="/css/main_menu.css" type="text/css" media="all"/>

<!--
For now I am removing head_scripts and including the required jquery files
The reason being that this may be the cause of the page slowness, this remove the third ajax call
-->
<?php //include '../includes/head_scripts.php';

?>

<script type="text/javascript" charset="utf-8">
	$(function() {
		$mydate1 = $('#jumpdate').datepicker({
			numberOfMonths: 1,
			dateFormat: 'yy-mm-dd',
			altField: '#alternate',
			altFormat: 'yy-mm-dd',
			minDate: '0d',
			maxDate: new Date(<?php echo $maxDate?>),
			beforeShowDay: checkAvailability

		});
		loadTimes();
	});
            var $myBadDates = new Array(<?php
                foreach ($bDates as $disabledate) {
                    echo " \"$disabledate\",";
                };
                 echo " 1";
            ?>);
    //                var $cntBookCal = new Array(<?php $bookDates[0]; ?>);
            function checkAvailability(mydate){
                var $return=true;
                var $returnclass ="available";
                //$checkdate = $.datepicker.formatDate('dd/mm/yy', mydate);
                $checkdate = $.datepicker.formatDate('yy-mm-dd', mydate);
                //Disable the checking of BadDates so that all day can be selected
                //this allows us to edit days that are off in the calendar
                /*
                for(var i = 0; i < $myBadDates.length; i++)
                {
                    if($myBadDates[i] == $checkdate)
                    {
                		$return=false;
                        $returnclass= "unavailable";
                        return [$return,$returnclass,"Dates Locked"];
                    }
                }
                */
                return [$return,$returnclass];
            }

function mgmtConsole(){
	if(document.getElementById('console')){
		document.calcForm.action ="index.php";
		document.calcForm.submit();
	    return true;
	}
}
function procChk(){
    var bDate = $('#jumpdate').val();
	var times = [];
	$("input[name='isrc[]']").each(function () {
		var open = 1;
		var online_open = 1;
		if ($(this).prop('checked') == true) {
			open = 0;
		}
		if ($("input[name='online[]'][value='"+$(this).val()+"']").prop("checked") == true) {
			online_open = 0;
		};
		times.push({
			'value'			:$(this).val(),
			'open'			: open,
			'online_open'	: online_open
		});
	});
	showLoading('Saving Time Table');
        $.ajax({
                url: '../includes/ajax_helper.php?action=setTimeState',
                type: 'POST',
                dataType: 'json',
		async : false,
                data: {
			'bDate'  : encodeURIComponent(bDate),
			'times'  : JSON.stringify(times)
		},
                success: function (data) {
                        if (data['result'] == 'success') {
                                for (i in data['options']) {
                                        if (data['options'].hasOwnProperty(i)) {
                                                if (data['options'][i].open == 0) {
                                                        $("input[name='isrc[]'][value='"+data['options'][i].value+"']").prop('checked', 'checked');
                                                } else {
                                                        $("input[name='isrc[]'][value='"+data['options'][i].value+"']").prop('checked', '');
                                                };
                                                if (data['options'][i].online_open == 0) {
                                                        $("input[name='online[]'][value='"+data['options'][i].value+"']").prop('checked', 'checked');
                                                } else {
                                                        $("input[name='online[]'][value='"+data['options'][i].value+"']").prop('checked', '');
                                                };
                                        }
                                }
								$("input[type=checkbox]").prop('disabled', '');
                        } else {
							$("input[type=checkbox]").prop('checked', '');
							$("input[type=checkbox]").prop('disabled', 'disabled');
						};

			$('#hidder').hide().css('z-index', '-100');
                }
        });
	loadTimes();
	return false;
}
function loadTimes() {
	var bDate = $('#jumpdate').val();
	showLoading('Loading Time Table');
	$.ajax({
		url: '../includes/ajax_helper.php',
		dataType: 'json',
		data: 'action=getTimeState&bDate=' + encodeURIComponent(bDate),
		success: function (data) {
			if (data['result'] == 'success') {
				for (i in data['options']) {
					if (data['options'].hasOwnProperty(i)) {
						if (data['options'][i].open == 0) {
							$("input[name='isrc[]'][value='"+data['options'][i].value+"']").prop('checked', 'checked');
						} else {
							$("input[name='isrc[]'][value='"+data['options'][i].value+"']").prop('checked', '');
						};
						if (data['options'][i].online_open == 0) {
							$("input[name='online[]'][value='"+data['options'][i].value+"']").prop('checked', 'checked');
						} else {
							$("input[name='online[]'][value='"+data['options'][i].value+"']").prop('checked', '');
						};
					}
				}
				$("input[type=checkbox]").prop('disabled', '');
				$('#proc').prop('disabled', '');
			} else {
				$('#proc').prop('disabled', 'disabled');
				$("input[type=checkbox]").prop('checked', '');
				$("input[type=checkbox]").prop('disabled', 'disabled');
			};
			$('#hidder').hide().css('z-index', '-100');
		}
	});
}
function showLoading(msg) {
    var td = $("#time-table");
    var hdiv = $('<div>');
    $('#hidder')
    .css({
        "position": "absolute",
        "top": td.attr("top"),
        "left": td.outerWidth() / 2 - 255,
        "background-color": "#101010",
        "opacity": "0.9",
        "width": '510px',
        "height": td.outerHeight(),
        "z-index": '999',
        "border-radius": '3px',
        "display": "table",
        "vertical-align": "middle",
        "visibility": "visible",
        "font-weight": "bold",
        "color": "white"
    })
    .html('<div id="inner_container" style="display: table-cell; text-align: center; width: 100%; vertical-align: middle;">'+msg+'<br />Please wait<br /><img src="../web/img/loading.gif"></div>')
    .show();
}

</script>
<style>
#time-table {
	position: relative;
}
#dateForm * {
	/*z-index: -1;*/
}
</style>
</head>
<body>
<?php include '../includes/main_menu.php'; ?>
<br>
<form action="" id="dateForm" method="post" name="calcForm">
<table align="center" cellpadding="0" cellspacing="0" width="510px">
	<thead>
		<tr>
			<th></th>
			<th></th>
		</tr>
	</thead>
	<tbody>
		<tr>
			<td align="center" bgcolor="#8FBC8F" colspan="4" valign="middle">
			<h3>Block Jump Time</h3>
			</td>
		</tr>
		<tr>
			<td align="right" bgcolor="khaki">Select Date:&nbsp;&nbsp;&nbsp;&nbsp;</td>

			<td bgcolor="khaki">
				<input type="text" name="jumpdate" id="jumpdate" size="14" readonly="readonly" value="<?php echo date('Y-m-d');?>" onchange="loadTimes();"/>
			</td>
			<td bgcolor="khaki">

			</td>
		</tr>
</table>
<div>
<table align="center" cellpadding="0" cellspacing="0" width="510px">
	<tbody>
		<tr>
			<td align="center" bgcolor="#8FBC8F" colspan="4" valign="middle">
			Select CHECKBOX to Turn OFF Jump Time
			</td>
		</tr>
</table>
<div id="time-table">
<div id="hidder"></div>
<div style="z-index:100;">
<table align="center" border="1" cellpadding="0" cellspacing="0" width="510px">
	<tbody>
	<tr>
		<th align="center" bgcolor="khaki" valign="middle">Block</th>
		<th align="center" bgcolor="khaki" valign="middle">Online Only</th>
		<th align="center" bgcolor="khaki" valign="middle">Time</th>
		<th align="center" bgcolor="khaki" valign="middle">Block</th>
		<th align="center" bgcolor="khaki" valign="middle">Online Only</th>
		<th align="center" bgcolor="khaki" valign="middle">Time</th>
	</tr>
<?php
	$query = "Select * from TimeMaster order by bookingTime ASC;";
	$res = mysql_query($query);
	$i = 0;
	while ($row = mysql_fetch_assoc($res)) {
		if ($i % 2 == 0) {
			echo "<tr>\n";
		};
		echo "\t" . '<td align="center" bgcolor="khaki" valign="middle"><INPUT TYPE="checkbox" NAME="isrc[]" value="'.$row['bookingTime'].'"></td>
	<td align="center" bgcolor="khaki" valign="middle"><INPUT TYPE="checkbox" NAME="online[]" value="'.$row['bookingTime'].'"></td>
	<td align="center" bgcolor="khaki" valign="middle">' . $row['bookingTime'] . '</td>' . "\n";
		if ($i % 2 == 1) {
			echo "</tr>\n";
		};
		$i++;
	};
	if ($i % 2 == 1) {
		echo "<td bgcolor='khaki'>&nbsp;</td><td bgcolor='khaki'>&nbsp;</td><td bgcolor='khaki'>&nbsp;</td>\n</tr>\n";
	};
?>
		<tr>
			<td align="center" bgcolor="#8FBC8F" colspan="6" valign="middle">
				<input type="button"  style="background-color:#FFFF66" value="Set Times OFF" id="proc" onclick="procChk();">
				<input type="button" style="background-color:#FFFF66" value="Back" id="console" onclick="mgmtConsole();">
			</td>
		</tr>
</table>
</div>
</div>
</div>
</form>
</body>
</html>
