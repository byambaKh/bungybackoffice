<?php
	include '../includes/application_top.php';
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo SYSTEM_SUBDOMAIN; ?> OpenSlots</title>
<?php include "../includes/head_scripts.php"; ?>
<style>
h1 {
	margin: 30px;
	text-align: center;
	width: 100%;
	color: black;
}
a {
  font: 60px Arial;
  text-decoration: none;
  float:center;
  background-color: #EEEEEE;
  color: #333333;
  text-align: center;
  padding: 4px 6px 4px 6px;
  border-top: 1px solid #CCCCCC;
  border-right: 1px solid #333333;
  border-bottom: 1px solid #333333;
  border-left: 1px solid #CCCCCC;
}
.center {
    margin: auto;
	text-align: center;
    width: 60%;
    padding: 10px;
}
body {
	background-color: black;
}
</style>
</head>
<body>
	<div class="center">
		<a href="processCalDays.php">Open Days </a>
	</div>
	<div class="center">
		<a href="processDate.php">Open Times</a>
	</div>
    <div class="center">
		<a href="editConfirmationEmail.php">Edit Confirmation Email</a>
	</div>
	<div class="center">
		<a href="../index.php">Back</a>
	</div>
</body>
</html>
