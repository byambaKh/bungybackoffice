<?php

	$add_items = array(
		'photos'	=> array(
			'title'		=> 'Photos',
			'max-value'	=> 28,
			'step'		=> 1,
			'price'		=> $config['price_photo']
		),
/*
		'video'	=> array(
			'title'		=> 'Video',
			'max-value'	=> 28,
			'step'		=> 1,
			'price'		=> $config['price_video']
		),
		'gopro'	=> array(
			'title'		=> 'GoPro',
			'max-value'	=> 28,
			'step'		=> 1,
			'price'		=> $config['price_gopro']
		),
*/
		'2ndj'	=> array(
			'title'		=> '2nd Jump',
			'max-value'	=> 28,
			'step'		=> 1,
			'price'		=> $config['second_jump_rate']
		),
/*
		'tshirt'	=> array(
			'title'		=> 'T-Shirts',
			'max-value'	=> 28,
			'step'		=> 1,
			'price'		=> $config['price_tshirt']
		),
*/
		'other'	=> array(
			'title'		=> 'Other',
			'max-value'	=> 2000,
			'step'		=> 100,
			'price'		=> 1
		),
	);

?>
