<?php
require '../includes/application_top.php';
//TODO separate all of the styles out from the page
//we will store the cord log preferences inside a new table called configuration_cords
//the reason is that this data is critical and if there is an error with another file
//using configuration, it could write erronious data over our cord settings
$queryCordData 			= 'SELECT * FROM configuration_cords WHERE site_id ='.CURRENT_SITE_ID.';';
$queryCordDataResults 		= mysql_query($queryCordData);
$queryCordDataResultsCount 	= mysql_num_rows($queryCordDataResults); 

$cordsArray = array(
	'yellowLowerBound' => null,
	'yellowUpperBound' => null,
	'redLowerBound'    => null,
	'redUpperBound'    => null,
	'blueLowerBound'   => null,
	'blueUpperBound'   => null,
	'blackLowerBound'  => null,
	'blackUpperBound'  => null
);

$cordsArrayDefaults = array(
	'yellowLowerBound' => 40,
	'yellowUpperBound' => 60,
	'redLowerBound'    => 61,
	'redUpperBound'    => 75,
	'blueLowerBound'   => 76,
	'blueUpperBound'   => 90,
	'blackLowerBound'  => 91,
	'blackUpperBound'  => 110
);

//if there is no cordlog setting set it to the defaults

$action = $_GET['action']; 
$noAction = !isset($action);

//if action is null && cord data found
if($noAction && ($queryCordDataResultsCount == 1)){
	//load setting from the database
	$cordsArrayRow  = mysql_fetch_assoc($queryCordDataResults);
	$cordsArray 	= unserialize($cordsArrayRow['cord_data']);

//if action is null && result is 0 //no cord data in the database
} else if($noAction && ($queryCordDataResultsCount==0)){
	//set some default settings
	echo "No Cord Data";

	//default cord settings
	$cordsArray = $cordsArrayDefaults;

	$cordsArraySerialize = serialize($cordsArray);
	$insertCords = "INSERT INTO configuration_cords (site_id, cord_data) VALUES (".CURRENT_SITE_ID.",'$cordsArraySerialize')";
	mysql_query($insertCords);

//if action is save && and cord data found and there is $_POST data 
} else if(($action == "save") && ($queryCordDataResultsCount == 1) ){
	//update the value in the database & redirect to ticket.php
	if((isset($_POST['cordsArray']))){//if there is POST data
		$cordsUnserializeString = mysql_escape_string(serialize($_POST['cordsArray']));
		$queryUpdateCordsData = "UPDATE configuration_cords SET cord_data ='$cordsUnserializeString' WHERE site_id =".CURRENT_SITE_ID.";";
		
		mysql_query($queryUpdateCordsData);
		$cordsArray 	= $_POST['cordsArray'];

		//header("Location: ticket.php");

	} else { //if we are trying to save non POST data, ie someone is just using the save URL invalidly
		//redirect to the correct URL
		header("Location: cordCalibration.php");

	}

//if action is save && no cord data (trying to update a non existant cord pref)
} else if(($action == "save") && ($queryCordDataResultsCount == 0)){
	//set it to a default and save
	$cordsArray 	= $cordsArrayDefaults;

	$cordsArraySerialize = serialize($cordsArray);
	$insertCords = "INSERT INTO configuration_cords (site_id, cord_data) VALUES (".CURRENT_SITE_ID.",'$cordsArraySerialize')";
	mysql_query($insertCords);

} else {
	//error unknown state
	echo "Warning: Cords data is inconsistent. Please ask head office to contact support about this issue with your site name.";
}  

echo Html::head("Bungy Japan - Cord Calibration", array("/Waiver/css/stylesheet.css","reception.css"), array("reception.js", "jquery-1.11.1.js"));
?>

<div id="mainDiv" style="width:1000px;">
	<h1 class="bigRedHeader">Cord Calibration - <?php echo SYSTEM_SUBDOMAIN?></h1>

	<?php 
	echo createCordColorInput("yellow");
	echo createCordColorInput("red");
	echo createCordColorInput("blue");
	echo createCordColorInput("black");

	echo Html::inputPageButton("Cord Log", "document.location='../Operations/cord_logs.php'", "150px", "70px", "greenButton newLine", "");
	?>
	<form id="cordsForm" action="" method="POST">
	<input type='hidden' name="cordsArray[yellowLowerBound]" id="yellowLowerBoundInput" value=''>
	<input type='hidden' name="cordsArray[yellowUpperBound]" id="yellowUpperBoundInput" value=''>

	<input type='hidden' name="cordsArray[redLowerBound]" id="redLowerBoundInput" value=''>
	<input type='hidden' name="cordsArray[redUpperBound]" id="redUpperBoundInput" value=''>

	<input type='hidden' name="cordsArray[blueLowerBound]" id="blueLowerBoundInput" value=''>
	<input type='hidden' name="cordsArray[blueUpperBound]" id="blueUpperBoundInput" value=''>

	<input type='hidden' name="cordsArray[blackLowerBound]" id="blackLowerBoundInput" value=''>
	<input type='hidden' name="cordsArray[blackUpperBound]" id="blackUpperBoundInput" value=''>
	<?php echo Html::inputPageButton("Done","saveCordData()", "150px", "70px", "redButton", "");?>
	</form>
</div>

<script>
var colorSequence = ["yellow", "red", "blue", "black"];

function saveCordData(){
	document.getElementById('cordsForm').action = "cordCalibration.php?action=save";
	document.getElementById('cordsForm').submit();
	return true;
}

function cordBoundsSanityCheck(){
	//make sure that the cordbounds are logical
	//TODO check the cord bounds make sense
	//no negative numbers
	//cords stay adjacent
	//cords stay inside of max and min jump weights
	//cords upper and lower bounds stay upper and lower ie: lower cannot be greater than upper
		//and adjacent cords bounds stay consistent too

	var cordsAreSane = false;
	var allCordBoundNamesArray = [];

	//generate an array of all color bound names
	for(var i = 0;i < colorSequence.length; i++){
		color = colorSequence[i];
                allCordBoundNamesArray.push(color + "LowerBound");
                allCordBoundNamesArray.push(color + "UpperBound");
	}

	for(var i = 0;i < allCordBoundNamesArray.length; i++){
		//compare cord i to cord i+1 and check to make
		//sure i+1 does not go over array bounds
        }


	return true;
}

function modifyBound(color, boundType, delta){

	var bound = color + boundType;
	var currentValue = parseInt(document.getElementById(bound).innerHTML);
	var newValue 	 = currentValue + delta;

	var adjacentBoundColor 	= null; 
	var adjacentBound 	= null;
	var adjacentBoundValue  = null;
	var boundColorIndex = colorSequence.indexOf(color);
	var cordBoundsAreValid    = null;

	//if we are at either end of the scale there only edit that single bound
	if(((color == "yellow")&&(boundType == "LowerBound"))||((color == "black")&&(boundType == "UpperBound"))){
		cordBoundsSanityCheck();

	//if we are at an upper bound then edit the next color's lowerbound
	} else if(boundType == "UpperBound"){//edit the bound and the next bound
		adjacentBoundColor 	= colorSequence[boundColorIndex + 1];
		adjacentBound 		= adjacentBoundColor + "LowerBound";
		adjacentBoundValue 	= newValue + 1;

	//if we are at a lower bound then also edit the previous color's upperbound
	} else if(boundType == "LowerBound"){//edit the bound and the previous bound
		adjacentBoundColor 	= colorSequence[boundColorIndex - 1];
		adjacentBound 		= adjacentBoundColor + "UpperBound";
		adjacentBoundValue 	= newValue - 1;

	} else {
        	console.log("Serious Error");
	}

	if(cordBoundsSanityCheck()){
		setValueOfBound(bound, newValue);
		//if adjacentbound is set by the if clause above, edit it too
		if(adjacentBound) setValueOfBound(adjacentBound, adjacentBoundValue);
	}
}

function setValueOfBound(buttonId, value){
	document.getElementById(buttonId).innerHTML = value;
	document.getElementById(buttonId+'Input').value = value;
}

$(document).ready(function(){
	setValueOfBound('yellowLowerBound', <?php echo $cordsArray['yellowLowerBound'];?>);
	setValueOfBound('yellowUpperBound', <?php echo $cordsArray['yellowUpperBound'];?>);

	setValueOfBound('redLowerBound',    <?php echo $cordsArray['redLowerBound'];?>);
	setValueOfBound('redUpperBound',    <?php echo $cordsArray['redUpperBound'];?>);

	setValueOfBound('blueLowerBound',   <?php echo $cordsArray['blueLowerBound'];?>);
	setValueOfBound('blueUpperBound',   <?php echo $cordsArray['blueUpperBound'];?>);

	setValueOfBound('blackLowerBound',  <?php echo $cordsArray['blackLowerBound'];?>);
	setValueOfBound('blackUpperBound',  <?php echo $cordsArray['blackUpperBound'];?>);

});

</script>

<?php 
function createCordColorInput($color){
	return "<div class=\"{$color}Cord noClear\" style=\"width:250px;\">".
			Html::inputPageButton($color, "", "250px", "100px", "{$color}Button", "").
			'<div class="lowerBound newLine" style="height:100px; width:100px;">'.
				"<h1 id=\"{$color}LowerBound\">Error</h1>".
				Html::inputPageButton("<","modifyBound('{$color}', 'LowerBound', -1)", "50px", "50px", "{$color}Button buttonArrowText", "").
				Html::inputPageButton(">","modifyBound('{$color}', 'LowerBound', +1)", "50px", "50px", "{$color}Button buttonArrowText", "").
			'</div>'.

			'<div>'.
				'<h1 style="height:100px; width:50px;">-</h1>'.
			'</div>'.
			'<div class="upperBound" style="height:100px; width:100px;">'.
				"<h1 id=\"{$color}UpperBound\">Error</h1>".
				Html::inputPageButton("<","modifyBound('{$color}', 'UpperBound', -1)", "50px", "50px", "{$color}Button buttonArrowText", "").
				Html::inputPageButton(">","modifyBound('{$color}', 'UpperBound', +1)", "50px", "50px", "{$color}Button buttonArrowText", "").
			'</div>'.
		'</div>';

}

require_once("../includes/pageParts/standardFooter.php");?>
