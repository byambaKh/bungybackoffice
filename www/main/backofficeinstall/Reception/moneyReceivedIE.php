<?php
include("../includes/application_top.php");
include("checkin_config.php");
$regid = $_POST['cregid'];

if (empty($regid)) {
    $regid = $_GET['regid'];
}
if (empty($regid)) {
    $regid = $_SESSION['regid'];
}

// Walk In
if (array_key_exists('walkin', $_GET) && $_GET['walkin']) {
    $data = array(
        'site_id'     => CURRENT_SITE_ID,
        'BookingDate' => date('Y-m-d'),
        'BookingTime' => date('H:i A', mktime(date('H'), (date('i') < 29) ? 0 : 30, 0, 1, 1, 2000)),
        'NoOfJump'    => '1',
        'BookingType' => 'Phone',
        'CollectPay'  => 'Onsite',
        'Rate'        => $config['first_jump_rate'],
        'Agent'       => 'NULL',
        'Notes'       => 'WALK-IN'
    );
    db_perform('customerregs1', $data);
    $regid = mysql_insert_id();
    Header('Location: ' . $PHP_SELF . '?regid=' . $regid);
};
// EOF Walk In
$bDates = getCalendarState();

$booking = getBookingInfo($regid);

// if there is session variables for this booking, update fromthem
if (array_key_exists('post_' . (int)$regid, $_SESSION)) {
    $_POST = $_SESSION['post_' . (int)$regid];

    for ($i = 0; $i <= 3; $i++) {
        // if it is plain booking, bypass all other records
        if (count($_POST['splitTime']) == 1 && $i > 0) continue;

        if (!array_key_exists($i, $booking)) {
            $booking[$i] = $booking[$i - 1]; // set all db values for this booking
            unset($booking[$i]['CustomerRegID']); // unset reg id - it is not exists for now
            unset($booking[$i]['groopBooking']); // uset group booking
        };
        if ($_POST['splitJump'][$i] == 0) {
            //$booking[$i]['DeleteStatus'] = 1;
        };
        // update booking values
        $booking[$i]['Agent'] = $_POST['agentn'];
        // update times
        $booking[$i]['BookingTime'] = $_POST['splitTime'][$i];
        if (count($booking) > 1) {
            $booking[$i]['SplitTime1'] = $_POST['splitTime'][1];
            $booking[$i]['SplitTime2'] = $_POST['splitTime'][2];
            $booking[$i]['SplitTime3'] = $_POST['splitTime'][3];
        };
        // update rates
        if (!empty($_POST['rate'][$i])) {
            $booking[$i]['Rate'] = $_POST['rate'][$i];
        };
        // update jumps count
        $booking[$i]['NoOfJump'] = $_POST['splitJump'][$i];
        if (count($booking) > 1) {
            $booking[$i]['SplitJump1'] = $_POST['splitJump'][1];
            $booking[$i]['SplitJump2'] = $_POST['splitJump'][2];
            $booking[$i]['SplitJump3'] = $_POST['splitJump'][3];
        };
        // do some updates for master record only
        if ($i == 0) {
            // update ratetopay to agent
            $booking[$i]['RateToPay'] = $_POST['ratetopay'];
            $booking[$i]['RateToPayQTY'] = $_POST['nos'];
            // additional items
            foreach ($add_items as $code => $value) {
                $booking[$i][$code] = $_POST['add_values'][$code] * $value['price'];
                $booking[$i][$code . '_qty'] = $_POST['add_values'][$code];
            };
        };
        // collect pay
        if ($i == 0) {
            if ($_POST['CancelFee'] > 0 && $_POST['CancelFeeQTY'] > 0) {
                $booking[$i]['CancelFee'] = $_POST['CancelFee'];
                $booking[$i]['CancelFeeQTY'] = $_POST['CancelFeeQTY'];
                $booking[$i]['CancelFeeCollect'] = $_POST['CancelFeeCollect'];
            };
        };
        $booking[$i]['CollectPay'] = $_POST['CollectPay' . $i];
        $booking[$i]['CustomerLastName'] = $_POST['lastname'];
        $booking[$i]['CustomerFirstName'] = $_POST['firstname'];
        $booking[$i]['RomajiName'] = $booking[$i]['CustomerLastName'] . ' ' . $booking[$i]['CustomerFirstName'];
        $booking[$i]['SplitName1'] = $booking[$i]['RomajiName'];
        $booking[$i]['SplitName2'] = $booking[$i]['RomajiName'];
        $booking[$i]['SplitName3'] = $booking[$i]['RomajiName'];
        $booking[$i]['ContactNo'] = $_POST['teleno'];
        $booking[$i]['CustomerEmail'] = $_POST['email'];
        $booking[$i]['Notes'] = $_POST['notes'] . ((!empty($_POST['booked_by'])) ? " [ {$_POST['booked_by']} " . date("Y/m/d") . " ] " : "");
    };
};

// eof update from session array

$grpJumper = 0;
foreach ($booking as $row) {
    $grpJumper += $row['NoOfJump'];
    $defaults[] = array(
        "cregid"    => $row['CustomerRegID'],
        "jumpsno"   => $row['NoOfJump'],
        "rate"      => $row['Rate'],
        "ratetopay" => $row['RateToPay'],
        "splitTime" => $row['BookingTime']
    );
};
$totalJumps = $grpJumper;
$jumpDate = $booking[0]['BookingDate'];
$chkInTime = $booking[0]['BookingTime'];
$agent = $booking[0]['Agent'];
$lastname = $booking[0]['CustomerLastName'];
$firstname = $booking[0]['CustomerFirstName'];
$contactno = $booking[0]['ContactNo'];
$modeTransport = $booking[0]['TransportMode'];
$notes = $booking[0]['Notes'];
$collect = $booking[0]['CollectPay'];


$shrt = $_GET['tsht'];
$itm = $_GET['itm'];
$ot = $_GET['ot'];
$at = $_GET['at'];
$jpno = $_GET['jpno'];

$tot = 0;


if ($lname == '') {
    $cname = $_SESSION['cname'];
} else {
    $cname = $lname . ' ' . $fname;
}


//echo "->>>>>>".$cname;
if (!is_null($cname)) {
    $cstnames = explode(" ", $cname);
    list($cusotmerlastname, $customerfirstname) = explode(' ', $cname);
}


$_SESSION['bookingtime'] = $bookingtime;
$_SESSION['cname'] = $cname;


$staff_names = BJHelper::getStaffList('StaffListName', true);

$agents = array_map(function ($item) {
    return $item['text'];
}, getAgents());

?>
<!DOCTYPE html>
<html>
<head>
    <?php include 'includes/header_tags.php'; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
    <meta http-equiv="X-UA-Compatible" content="IE=9">


    <script src="js/functions.js" type="text/javascript"></script>

    <title>Money Received</title>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
    <link rel="stylesheet"
          href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css"
          type="text/css" media="all"/>
    <link rel="stylesheet"
          href="../css/all.css"
          type="text/css" media="all"/>

    <?php
    $max_jumps = 12;
    include 'bookings.php';
    ?>
<script type="text/javascript">
    function goProc() {
        if (<?php echo $booking[0]['Checked']; ?> == 1 )
        {
            document.mform.action = 'checkedCustomerIE.php?regid=<?php echo $regid; ?>';
            document.mform.submit();
            return;
        }
        ;
        if (document.mform.lastname.value == '') {
            alert('Please fill in your First Name!');
            return false;
        }
        if (document.mform.firstname.value == '') {
            alert('Please fill in your Last Name!');
            return false;
        }
//if(document.getElementById('gob')){
//document.mform.action ="waiverIE.php";
        document.mform.action = "calculate.php?regid=<?php echo $regid; ?>";
        document.mform.submit();
        return true;
//}
    }
    function getMValue() {
        var e = document.getElementById("tshirt").value;
        var f = document.getElementById("itema").value;

    }
    function procBack() {
        <?php if (!array_key_exists('converted_to_group', $_SESSION) || $_SESSION['converted_to_group'] != $regid) { ?>
        document.mform.action = "dailyViewIE.php";
        document.mform.submit();
        return true;
        <?php } else { ?>
        return procConvertNormal();
        <?php }; ?>
    }
    function procConvert() {
        document.mform.action = "convert_to_group.php?regid=<?php echo $regid; ?>";
        document.mform.submit();
        return true;
    }
    function procConvertNormal() {
        document.mform.action = "convert_to_normal.php?regid=<?php echo $regid; ?>";
        document.mform.submit();
        return true;
    }
    function onlyNumbers(evt) {
        var e = event || evt; // for trans-browser compatibility
        var charCode = e.which || e.keyCode;

        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;

    }
    function limitDropDown() {
        var len =  <?php echo $booking[0]['NoOfJump']; ?>;
//alert(len);
        if (len != 9999) {

            document.getElementById('noOfJump').length = len;
        }

    }
    function getXMLHTTP() { //fuction to return the xml http object
        var xmlhttp = false;
        try {
            xmlhttp = new XMLHttpRequest();
        }
        catch (e) {
            try {
                xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
            }
            catch (e) {
                try {
                    xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
                }
                catch (e1) {
                    xmlhttp = false;
                }
            }
        }

        return xmlhttp;
    }
    function getRateValue() {

        var strURL = "/Reception/loadRate.php";


        var req = getXMLHTTP();

        if (req) {

            req.onreadystatechange = function () {
                if (req.readyState == 4) {
                    // only if "OK"
                    if (req.status == 200) {
                        document.getElementById('rate').innerHTML = req.responseText;
                    } else {
                        alert("There was a problem while using XMLHTTP:\n" + req.statusText);
                    }
                }
            }
            req.open("GET", strURL, true);
            req.send(null);
        }
    }
    $(function () {
        $mydate = $('#jumpDate').datepicker({
            showAnim: 'fade',
            numberOfMonths: 1,
            //dateFormat: 'dd/mm/yy',
            dateFormat: 'yy-mm-dd',
            currentText: 'Today',
            altField: '#alternate',
            altFormat: 'yy-mm-dd',
            minDate: '0d',
            //minDate: new Date(2011, 12 , 31),
            maxDate: new Date(<?php echo $maxDate?>),
            beforeShowDay: checkAvailability,
            onSelect: getBookTimes
        });
//$('.date-pick').datePicker({ defaultDate: $.datepicker.parseDate("dd/mm/yy", new Date()) });
        getBookTimes($('#jumpDate').val(), null);
        update_foc();
        $('#rate0, #rate1, #rate2, #rate3').change(update_foc);
//triggerAgent();
    });

    function getBookTimes(bDate, inst) {
        $.ajax({
            url: '../includes/ajax_helper.php',
            dataType: 'json',
            data: 'action=getBookTimes&bDate=' + encodeURIComponent(bDate) + '&regid=<?php echo $regid; ?>',
            success: function (data) {
                if (data['result'] == 'success') {
                    var selected = $('#chkInTime').val();
                    $('#chkInTime > option').remove();
                    for (i in data['options']) {
                        if (data['options'].hasOwnProperty(i)) {
                            var option = $("<option></option>");
                            option.prop('value', data['options'][i].value);
                            option.prop('text', data['options'][i].text);
                            option.prop('selected', (data['options'][i].value === selected) ? 'selected' : '');
                            $('#chkInTime').append(option);
                        }
                    }
                }
            }
        });
    }
    var $myBadDates = new Array(<?php
            foreach ($bDates as $disabledate) {
                echo " \"$disabledate\",";
            };
             echo " 1";
        ?>);
    function checkAvailability(mydate) {
        var $return = true;
        var $returnclass = "available";
//$checkdate = $.datepicker.formatDate('dd/mm/yy', mydate);
        $checkdate = $.datepicker.formatDate('yy-mm-dd', mydate);
        for (var i = 0; i < $myBadDates.length; i++) {
            if ($myBadDates[i] == $checkdate) {
                $return = false;
                $returnclass = "unavailable";
                return [$return, $returnclass, "Dates Locked"];
            }
        }
//                    if($cntBookCal > 0){
//                        $return = true;
//                        $returnclass= "booked";
//                        return [$return,$returnclass,"Booking..."];
//                    }
        return [$return, $returnclass];
    }
    function triggerAgent() {
        if ($('#agentn').val() == 'NULL' <?php echo (SYSTEM_SUBDOMAIN == 'MINAKAMI') ? "|| $('#agentn').val() == 'SG Bungy'" : ''; ?>) {
            $('#CollectPay0, #CollectPay1, #CollectPay2, #CollectPay3, #CancelFeeCollect').val('Onsite');
            console.log("onsite");
        } else {
            $('#CollectPay0, #CollectPay1, #CollectPay2, #CollectPay3, #CancelFeeCollect').val('Offsite');
            console.log("offsite");
        }
    }
    ;

    function update_foc() {
        for (var i = 0; i < 4; i++) {
            var disabled = 'disabled';
            if ($('#rate' + i).val() == '0') {
                disabled = false;
            } else {
                $('#foc' + i).val('');
            }
            ;
            $('#foc' + i).attr('disabled', disabled);
        }
    }
</script>
<style>
    .jumps-row td {
        vertical-align: bottom;
    }

    .jumps-row td select {
        vertical-align: bottom;
    }

    .header-row {
        background-color: red;
        text-align: center;
        color: white;
    }
</style>
</head>
<body>
<form name="mform" action="" method="post">
    <table align="center" border="0" width="500px" bgcolor="#000000">
        <tr>
            <?php
            if ($booking[0]['GroupBooking'] == 0) {
                ?>
                <td align="left">
                    <input type="button" name="togroupconvert" id="togroupconvert" value="Change to Variable"
                           style="background-color:#FFFF66" onclick="procConvert();">
                </td>
            <?php
            };
            ?>
            <td align="right">
                <input type="button" name="back" id="back" value="BACK" style="background-color:#FFFF66"
                       onclick="procBack();">
            </td>
        </tr>
    </table>
    <table border="1" align="center" width="500px">
        <tbody>
        <tr>
            <td colspan="6" align="center" bgcolor="#000000">
                <font color="#FFFFFF"><b><?php echo SYSTEM_SUBDOMAIN; ?> Booking Details</b></font>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" colspan=3 align="right"><font color="#FFFFFF">Jump Date:
                    <input type="text" class="dates" name="jumpDate" id="jumpDate" size="12" readonly="readonly"
                           value="<?php echo $booking[0]['BookingDate']; ?>"<?php echo (sizeof($booking) > 1) ? ' disabled="disabled"' : ''; ?>>
                </font></td>
            <td bgcolor="#FF0000" align="right" colspan="3">
                <font color="#FFFFFF">Agent:</font>
                <?php
                $agent = $booking[0]['Agent'];
                echo "<select name='agentn' id='agentn' onChange='triggerAgent();'>";
                foreach ($agents as $a) {
                    echo "<option value='$a'" . (($agent == $a) ? 'selected="selected"' : '') . ">$a</option>";
                }
                ?>
            </td>
        </tr>
        <tr class="header-row">
            <td>Collect Pay</td>
            <td colspan="2">Jump Time</td>
            <td>Price</td>
            <td>FOC</td>
            <td>Jumps</td>
        </tr>
        <?php
        $timeTitles = array("Jump Time", "Split 1", "Split 2", "Split 3");
        foreach ($booking as $i => $row) {
            //if ($row['NoOfJump'] == 0) continue;
            echo "\t<tr class='jumps-row'>\n";
            echo "<td bgcolor=\"#FF0000\" align=\"right\" colspan=\"3\"><font color=\"#FFFFFF\">";
            ?>
            <select name="CollectPay<?php echo $i; ?>" style="float: left; margin-left: 30px !important;"
                    id="CollectPay<?php echo $i; ?>">
                <option<?php echo ($row['CollectPay'] == 'Onsite') ? ' selected="selected"' : ''; ?>>Onsite</option>
                <option<?php echo ($row['CollectPay'] == 'Offsite') ? ' selected="selected"' : ''; ?>>Offsite</option>
            </select>
            <?php
            echo "<select name=\"splitTime[$i]\" id=\"splitTime$i\" class=\"split-time\">";
            echo "<option value='{$row['BookingTime']}' selected='selected'>{$row['BookingTime']}</option>";
            echo "</select>";
            echo "</td>";
            echo "<td bgcolor=\"#FF0000\" align=\"right\"><font color=\"#FFFFFF\">";
            echo draw_pull_down_menu("rate[$i]", BJHelper::getRatesList(), $row['Rate'], "id=\"rate$i\"");
            echo "</td>";
            echo "<td bgcolor=\"#FF0000\" align=\"right\">";
            echo draw_pull_down_menu("foc$i", getFOCTitles(), $row['foc'], "id=\"foc$i\"");
            echo "</td>";
            echo "<td bgcolor=\"#FF0000\" align=\"right\">";
            echo "<select name=\"splitJump[$i]\" id=\"splitJump$i\">";
            for ($j = 0; $j <= 12; $j++) {
                $selected = ($row['NoOfJump'] == $j) ? ' selected="selected"' : '';
                echo "<option value='$j'$selected>{$j}</option>";
            };
            echo "</select>";
            echo "</td>";
            echo "\t</tr>\n";
        };
        ?>
        <tr>
            <td bgcolor="#FF0000" align="right" colspan="3">
                <select name="CancelFeeCollect" style="float: left;margin-left: 30px !important;" id="CancelFeeCollect">
                    <option<?php echo ($row['CollectPay'] == 'Onsite') ? ' selected="selected"' : ''; ?>>Onsite</option>
                    <option<?php echo ($row['CollectPay'] == 'Offsite') ? ' selected="selected"' : ''; ?>>Offsite
                    </option>
                </select>
                <font color="#FFFFFF">Cancellation:</font>
            </td>
            <td bgcolor="#FF0000" colspan="3" align="left"><font color="#FFFFFF">&yen;</font>
                <?php
                // load cancel fee rates
                $crates = BJHelper::getList('general', 'cancel_rates');
                $cancel_rates = array_map(function ($item) {
                    return $item['text'];
                }, $crates['general']['cancel_rates']);
                echo "<select name=\"CancelFee\" id=\"cancelfee\">";
                foreach ($cancel_rates as $rate) {
                    echo "<option value='{$rate}'" . (($rate == $booking[0]['CancelFee']) ? " selected='selected'" : "") . ">{$rate}</option>";
                };
                echo "</select>";
                ?>
                <font color="#FFFFFF"> X </font>
                <select name="CancelFeeQTY" id="cancelfee-qty">
                    <?php for ($i = 0; $i < 29; $i++) { ?>
                        <option
                            value="<?= $i; ?>"<?= ($i == $booking[0]['CancelFeeQTY']) ? ' selected="selected"' : ''; ?>><?= $i; ?></option>
                    <?php }; ?>
                </select>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" align="right" colspan="3">
                <font color="#FFFFFF">To Pay to Agent:</font>
            </td>
            <td bgcolor="#FF0000" colspan="3" align="left"><font color="#FFFFFF">&yen;</font>
                <?php
                echo draw_pull_down_menu("ratetopay", BJHelper::getRatesList(), $booking[0]['RateToPay'], "id=\"ratetopay\"");
                ?>
                <font color="#FFFFFF"> X </font>
                <select name="nos" id="nos">
                    <?php for ($i = 0; $i < 29; $i++) { ?>
                        <option
                            value="<?= $i; ?>"<?= ($i == $booking[0]['RateToPayQTY']) ? ' selected="selected"' : ''; ?>><?= $i; ?></option>
                    <?php }; ?>
                </select>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" align="right" colspan="3"><font color="#FFFFFF">Last Name:</font></td>
            <td bgcolor="#FF0000" colspan="3"><input type="text" name="lastname" value="<?php echo $booking[0]['CustomerLastName']; ?>" style="text-transform: uppercase"></td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" align="right" colspan="3"><font color="#FFFFFF">First Name:</font></td>
            <td bgcolor="#FF0000" colspan="3"><input type="text" name="firstname" value="<?php echo $booking[0]['CustomerFirstName']; ?>" style="text-transform: uppercase"></td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" align="right" colspan="3"><font color="#FFFFFF">Phone No:</font></td>
            <td bgcolor="#FF0000" colspan="3"><input type="text" name="teleno" value="<?php print $booking[0]['ContactNo']; ?>"></td>
        </tr>
        <!--tr>
  <td align="right" bgcolor="#FF0000" colspan="3"><font color="#FFFFFF">Email:</font></td>
  <td bgcolor="#FF0000" colspan="3"><input type="text" name="email" value="<?php //print $$booking[0]['CustomerEmail']; ?>"></td>
</tr-->
        <?php
        $i = 0;
        foreach ($add_items as $code => $item) {
            if ($i % 3 == 0) echo "\t<tr>\n";
            echo "\t\t<td bgcolor=\"#FF0000\"><font color=\"#FFFFFF\">{$item['title']}</td>\n";
            echo "\t\t<td bgcolor=\"#FF0000\">\n";
            echo "\t\t\t<select name=\"add_values[{$code}]\" style=\"width: 60px;\">\n";
            $selected = $booking[0][$code . '_qty'];
            for ($j = 0; $j <= $item['max-value']; $j += $item['step']) {
                echo "\t\t\t\t<option value=\"{$j}\"" . (($selected == $j) ? ' selected="selected"' : '') . ">{$j}</option>\n";
            };
            echo "\t\t\t</select>\n";
            echo "\t\t</td>\n";
            if ($i % 3 == 2) echo "\t</tr>\n";
            $i++;
        };
        if ($i % 3 < 3) echo "\t</tr>\n";
        ?>
        <tr>
            <td colspan="1" bgcolor="#FF0000"><font color="#FFFFFF">Notes:</font>
            </td>
            <td colspan="6" bgcolor="#FF0000">
                <textarea rows="3" cols="30" name="notes"><?php print $booking[0]['Notes']; ?></textarea>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" align="right" colspan="3"><font color="#FFFFFF">Booked by:</font>
            </td>
            <td bgcolor="#FF0000" colspan="3">
                <?php echo draw_pull_down_menu('booked_by', $staff_names, '', 'style="width: 180px;"'); ?>
            </td>
        </tr>
        <tr>
            <td bgcolor="#000000" colspan="8" align="center"><font color="#FFFFFF">Details Finalized:</font>
                <input type="checkbox" name="chk" checked="checked"/>
                <input type="hidden" name="updateCheckIn" value="1"/>
                <input type="hidden" name="MedicalConditionsChecked" value="<?=$_POST['MedicalConditionsChecked']?>"/>
                <input type="button" style="background-color:#FFFF66" value="GO" onclick="goProc();">
            </td>
        </tr>
        </tbody>
    </table>
</form>
</body>
</html>
