<!DOCTYPE html>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<meta charset="windows-31j">
<title>Insert title here</title>

<!-- Combo-handled YUI CSS files: -->
<link rel="stylesheet" type="text/css" href="http://yui.yahooapis.com/combo?2.9.0/build/reset-fonts-grids/reset-fonts-grids.css&2.9.0/build/menu/assets/skins/sam/menu.css&2.9.0/build/button/assets/skins/sam/button.css">
<!-- Combo-handled YUI JS files: -->
<script type="text/javascript" src="http://yui.yahooapis.com/combo?2.9.0/build/yuiloader-dom-event/yuiloader-dom-event.js&2.9.0/build/container/container_core-min.js&2.9.0/build/menu/menu-min.js&2.9.0/build/element/element-min.js&2.9.0/build/button/button-min.js"></script>

 <!-- Dependency source files -->

        <script type="text/javascript" src="../../build/yahoo-dom-event/yahoo-dom-event.js"></script>
        <script type="text/javascript" src="../../build/container/container_core.js"></script>


<!-- Menu source file -->
<script type="text/javascript" src="../../build/menu/menu.js"></script>

        <!-- Page-specific script -->

        <script type="text/javascript">

            /*
                 Initialize and render the MenuBar when its elements are ready 
                 to be scripted.
            */

            YAHOO.util.Event.onContentReady("productsandservices", function () {

                /*
          Instantiate a MenuBar:  The first argument passed to the constructor
          is the id for the Menu element to be created, the second is an 
          object literal of configuration properties.
                */

                var oMenuBar = new YAHOO.widget.MenuBar("productsandservices", { 
                                                            autosubmenudisplay: true, 
                                                            hidedelay: 750, 
                                                            lazyload: true });

                /*
                     Define an array of object literals, each containing 
                     the data necessary to create a submenu.
                */

                var aSubmenuData = [
                
                    {
                        id: "booking", 
                        itemdata: [ 
                            { text: "Make Booking", url: "/Reception/makeBookingIE.php?st=X89-99194573908370389502482-482-4245947534953-F43948452-G9879GJIFGJDFGR2342FSDFWERW?xyz109HHKJHKxvshdbsd68ysbdjfsdj492460576" },
                            { text: "Group Booking", url: "" },
                            { text: "Daily View Data", url: "/Reception/dailyViewIE.php?st=X89-99194573908370389502482-482-4245947534953-F43948452-G9879GJIFGJDFGR2342FSDFWERWdfds44ewrwefsdfcsadadsa?z0980idfsfsfa210521036" },
                            { text: "Change Booking", url: "/Reception/changeBookingMain.php?st=X89-99194573908370389502482-482-4245947534953-F43948452-G9879GJIFGJDFGR453tegtegdfbdsw4252?w454dfsdgsgfet6533fvs484193404" },
                            { text: "Cancel Booking", url: "/Reception/cancelBookingMain.php?st=X89-99194573908370389502482-482-4245947534953-F43948452-G9879GJIFGJDFGR2342FSDFWERWdwer45fsfvsfsfsra?0erwrrwrw699902623" }

                        ]
                    },

                    {
                        id: "agent", 
                        itemdata: [
                            { text: "Booking By Agent", url: "" },
                            { text: "Agent Invoice", url: "" },
                            { text: "Rates & Packages", url: "" }                  
                        ]    
                    },
                    
                    {
                        id: "dashboard", 
                        itemdata: [
              { text: "Dashboard View", url: "" },       
                            { text: "Daily Report", url: "" },
                            { text: "Weekly Report", url: "" },
                            { text: "Monthly Report", url: "" }
                        ] 
                    },
                    
                    {
                        id: "administration",
                        itemdata: [
                            { text: "Add User", url: "" },
                            { text: "Add Admin", url: "" },
                            { text: "Add Agent", url: "" },
                            { text: "Add Booking Type LOV", url: "" },
                            { text: "Add Commission LOV", url: "" }
                            
                        ]
                    }                    
                ];


                /*
                     Subscribe to the "beforerender" event, adding a submenu 
                     to each of the items in the MenuBar instance.
                */

                oMenuBar.subscribe("beforeRender", function () {

          var nSubmenus = aSubmenuData.length,
            i;


                    if (this.getRoot() == this) {

            for (i = 0; i < nSubmenus; i++) {
                          this.getItem(i).cfg.setProperty("submenu", aSubmenuData[i]);
            }

                    }

                });


                /*
                     Call the "render" method with no arguments since the 
                     markup for this MenuBar instance is already exists in 
                     the page.
                */

                oMenuBar.render();         
            
            });

        </script>

</head>
<body class="yui-skin-sam">
<div id="productsandservices" class="yuimenubar yuimenubarnav">
    <div class="bd">
        <ul class="first-of-type">
            <li class="yuimenubaritem first-of-type">
                <a class="yuimenubaritemlabel" href="#booking">Booking</a>
            </li>
            <li class="yuimenubaritem">
                <a class="yuimenubaritemlabel" href="#agent">Agent</a>
            </li>
            <li class="yuimenubaritem">
                <a class="yuimenubaritemlabel" href="#dashboard">Dashboard</a>
            </li>
            <li class="yuimenubaritem">
                <a class="yuimenubaritemlabel" href="#management">Management</a>
            </li>
            <li class="yuimenubaritem">
                <a class="yuimenubaritemlabel" href="#administration">Administration</a>
            </li>
        </ul>
    </div>
</div>

</body>
</html>
