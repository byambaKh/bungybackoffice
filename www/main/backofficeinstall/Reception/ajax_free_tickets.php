<?php
include "../includes/application_top.php";
include '../Operations/reports_save.php';
include 'functions.php';

//Check if the user is logged in before giving out data
$user = new BJUser();
if (!$user->isUser()) die();

$action = $_GET['action'];
$type = $_GET['type'];
if (empty($type)) {
    $type = 'free_tickets';
};
switch (TRUE) {
    case ($action == 'update_row') :
        $_POST['site_id'] = CURRENT_SITE_ID;
        $id = $_POST['id'];
        unset($_POST['id']);
        db_perform($type, $_POST, 'update', 'id=' . $id);
        $result = array('result' => 'success');
        break;
    case ($action == 'delete_row') :
        if ($_POST['delete_all']) {
            $sql = "update $type set deleted = 1 WHERE id <= {$_POST['id']};";
        } else {
            $sql = "update $type set deleted = 1 WHERE id = {$_POST['id']};";
        };
        mysql_query($sql);
        $result = array('result' => 'success');
        break;
};
$result['request'] = array(
    'action' => $action,
    'type' => $type,
    'post' => $_POST
);
echo json_encode($result);
?>
