<?php
include "../includes/application_top.php";

$user = new BJUser();

$canDelete = false;
if($user->hasRole('SysAdmin')){
    $canDelete = true;
}

function deleteWaiverWithId($id) {
    $sql = "DELETE FROM waivers WHERE id = $id";
    mysql_query($sql);
}

mysql_set_charset('utf8');
$startDate = $endDate = date("Y-m-d");

if (isset($_GET['jumpDate']) && preg_match("/^([\d]{4}-[\d]{2}-[\d]{2})$/", $_GET['jumpDate'])) {
    $startDate = $endDate = $_GET['jumpDate'];
}
if (isset($_GET['startDate']) && preg_match("/^([\d]{4}-[\d]{2}-[\d]{2})$/", $_GET['startDate'])) {
    $startDate = $_GET['startDate'];
}
if (isset($_GET['endDate']) && preg_match("/^([\d]{4}-[\d]{2}-[\d]{2})$/", $_GET['endDate'])) {
    $endDate = $_GET['endDate'];
}

$nv = 'NULL';
$waivers_sql = "SELECT cr.BookingDate, w.*, IF(nj.id IS NULL, '', 'non-jumper') as nonjumper from waivers w LEFT JOIN non_jumpers nj on (w.id = nj.waiver_id), customerregs1 cr
			WHERE 
			cr.site_id = '" . CURRENT_SITE_ID . "'
			and cr.bookingdate BETWEEN '$startDate' AND '$endDate' 
			and cr.CustomerRegID = w.bid
			";

if(isset($_POST['delete-waiver-id'])) {
    deleteWaiverWithId($_POST['delete-waiver-id']);
}
$res = mysql_query($waivers_sql) or die(mysql_error());

if (isset($_GET['action']) && $_GET['action'] == 'csv') {
    header('Content-Type: text/csv; name="waivers-' . str_replace('-', '', $cDate) . '.csv"');
    header('Content-Disposition: attachment; filename="waivers-' . str_replace('-', '', $cDate) . '.csv"');
    $headers_sent = false;
    $delimiter = ",";
    while ($row = mysql_fetch_assoc($res)) {
        if (!$headers_sent) {
            echo implode($delimiter, array_keys($row)) . "\n";
            $headers_sent = true;
        };
        $row['sig_img'] = '';
        echo implode($delimiter, array_map(function ($item) {
                return '"' . $item . '"';
            }, $row)) . "\n";
    };
    die();
};
?>
<html>
<head>
    <?php include '../includes/header_tags.php'; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=8">
    <title>View All Data</title>

    <?php include '../includes/head_scripts.php'; ?>
    <script type="text/javascript">
        $(document).ready(function () {
            $('.datepicker').datepicker({
                showAnim: 'fade',
                numberOfMonths: 1,
                //dateFormat: 'dd/mm/yy',
                dateFormat: 'yy-mm-dd',
                currentText: 'Today'
            });
            //$('.datepicker').change(function () {$(this).parents('form').submit();});
            $('input[name=startDate]').change(function () {
                $('input[name=endDate]').val($('input[name=startDate]').val());
            });

            $('.delete-button').click(function () {
                var waiverId = $(this).data('waiver-id');

                if (confirm("Are you sure you want delete waiver " + waiverId + " ?") === true) {
                    $("#delete-waiver-id").val(waiverId);
                    $("#delete-waiver-form").submit();
                    return true;
                } else {
                    return false;
                }
                //find the button id
            });
        });


    </script>
    <style>
        .redrow td {
            background-color: red;
        }
    </style>
</head>
<body>
<table align="center" border="0" width="90%">

    <tr>
        <td>
            <form>
                Start date: <input name="startDate" value="<?php echo $startDate; ?>" class="datepicker">
                <input name="endDate" value="<?php echo $endDate; ?>" type="hidden">
                <button type="submit">Create Report</button>
            </form>
            <table width="90%" border="1">
                <?php
                $res = mysql_query($waivers_sql) or die(mysql_error());
                if (mysql_num_rows($res) > 0) {
                    $headers_sent = false;
                    while ($row = mysql_fetch_assoc($res)) {

                        $addclass = $row['nonjumper'] ? ' class="redrow"' : '';
                        unset($row['nonjumper']);
                        if (!$headers_sent) {
                            echo "<tr>";

                            if($canDelete)
                                echo "<th>Action</th>";

                            foreach ($row as $key => $value) {
                                if ($key == "sig_img") continue;
                                if ($key == "paid_amount") continue;
                                echo "<th>$key</th>";
                            };
                            $headers_sent = true;
                            echo "</tr>";
                        };
                        echo "<tr$addclass>";

                        if($canDelete)
                        echo "<td><button class='delete-button' data-waiver-id='{$row['id']}'>Delete</button></td>";

                        foreach ($row as $key => $value) {
                            if ($key == "sig_img") continue;//skip images
                            if ($key == "paid_amount") continue;
                            echo "<td>";
                            $text = $value;
                            if ($key == 'sig_img') {
                                $text = "<img src='get_image.php?wid={$row['id']}' style='width: 100px;' />";
                            };
                            echo $text;
                            echo "</td>";
                        };
                        echo "</tr>\n";
                    };
                } else {
                    echo "<td>No records found</td>";
                };
                ?>
            </table>

            <form id="delete-waiver-form" method="POST">
                <input type="hidden" value="0" name="delete-waiver-id" id="delete-waiver-id">
            </form>

            <table width="90%">
                <tr>
                    <td>
                        <!--form method="get">
	<input type="hidden" name="startDate" value="<?php echo $startDate; ?>">
	<input type="hidden" name="endDate" value="<?php echo $endDate; ?>">
	<input type="hidden" name="action" value="csv">
	<button type="submit">Download CSV</button>
</form-->
                    </td>
                </tr>
            </table>

        </td>
    </tr>
</table>
</body>
</html>
