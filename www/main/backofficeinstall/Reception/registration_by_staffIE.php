<?php

require '../includes/application_top.php';
require $_SERVER['DOCUMENT_ROOT'] . '/Reception/GenerateXML.php';

$romajiname = $_POST["lastname"] . ' ' . $_POST["firstname"];


$noOfJump = $_GET['jumpno'];
$_SESSION['noOfJump'] = $noOfJump;
$jumpDate = $_SESSION['jumpDate'];
if (!empty($_GET['bookingdate'])) {
    $jumpDate = $_GET['bookingdate'];
    $_SESSION['jumpDate'] = $jumpDate;
}

$new_date_format = date('D Y/m/d', strtotime($jumpDate));

$chkInTime = $_GET['bookingtime'];
$_SESSION['chkInTime'] = $chkInTime;

$i = $noOfJump;

$addr = "NULL";
$postalcode = "NULL";
$pref = "NULL";
$otherno = 0;
$deleteStatus = 0;
$dayStatus = 0;
$timeStatus = 0;
//$confirmCode = md5(uniqid(rand()));
$email = "NULL";
//$bookingtype = "Internet";
$rate = $config['first_jump_rate'];
$bookingrecv = date("Y-m-d h:i:a");
//$collectpay = "NULL";
//$ratetopay = 0;
$checked = 0;
$splitname1 = "NULL";
$splitname2 = "NULL";
$splitname3 = "NULL";

$splittime1 = "NULL";
$splittime2 = "NULL";
$splittime3 = "NULL";

$splitjump1 = 0;
$splitjump2 = 0;
$splitjump3 = 0;

//$notes = "NULL";
$photos = 0;
$mer = "NULL";
$agent = "NULL";

$myusername = $_SESSION['myusername'];


$btyArray = BJHelper::getBookingTypes();

/*     * *************************New Code END***************************** */

if ($_POST["lastname"] && $_POST["firstname"]) {
    $rate = $_POST['rate'];
    if ($_POST['rate'] == '') {
        $rate = $config['first_jump_rate'];
        //$ratetopay = 7500;
        $ratetopay = 0;
    }
    if ($_POST['agentName'] == '') {
        $_POST['agentName'] = "NULL";
    }

    $sel_Group = $_POST['group1'];
    if ($sel_Group == 'Onsite') {
        //$ratetopay = $_POST['rate'];
        $collectpay = "Onsite";
    } else if ($sel_Group == 'Offsite') {
        $collectpay = "Offsite";
    }
    $cancelFee = 0;
    $groupBooking = 0;
    //Rate -- RateToPay
    if (isset($_POST['agtratetopay'])) {
        $RateToPay = $_POST['agtratetopay'];
        $RateToPayQTY = $_POST['nos'];
    }

    if (isset($_POST["both"]) && !empty($_POST["both"]) && $_POST["both"] != SYSTEM_SUBDOMAIN) {
        $collectpay = "Offsite";
        $rate = $config['combo_jump_rate'];
        $RateToPay = 0;
        $RateToPayQTY = 0;
        $_POST['agentName'] = ($_POST["both"] == 'MINAKAMI') ? 'MK Bungy' : 'SG Bungy';
    };

    if (isset($_POST["both"]) && !empty($_POST["both"])) {
        // fill variables from change booking page
        if (isset($_POST['chkInTime'])) {
            $chkInTime = $_POST['chkInTime'];
            $jumpDate = preg_replace("/[^\d^-]+/", '', $_POST['jumpDate']);
            $_POST['teleno'] = $_POST['contactno'];
            $_POST['modeTransport'] = $_POST['transportmode'];
            $_POST['notes'] .= " Changed to";
        };
        $_POST['notes'] .= " MK/SG Combo";
    };

    $data = array(
        "site_id"           => CURRENT_SITE_ID,
        "BookingDate"       => $jumpDate,
        "BookingTime"       => $chkInTime,
        "NoOfJump"          => $_POST['noOfJump'],
        "RomajiName"        => strtoupper($romajiname),
        "CustomerLastName"  => strtoupper($_POST['lastname']),
        "CustomerFirstName" => strtoupper($_POST['firstname']),
        "CustomerAddress"   => $addr,
        "PostalCode"        => $postalcode,
        "Prefecture"        => $pref,
        "ContactNo"         => $_POST['teleno'],
        "CustomerEmail"     => $email,
        "OtherNo"           => $otherno,
        "TransportMode"     => $_POST['modeTransport'],
        "DeleteStatus"      => 0,
        "DayStatus"         => 0,
        "TimeStatus"        => 0,
        "BookingType"       => $_POST['bookingtype'],
        "Rate"              => $rate,
        "BookingReceived"   => date("Y-m-d"),
        "CollectPay"        => $collectpay,
        "RateToPay"         => $RateToPay,
        "RateToPayQTY"      => $RateToPayQTY,
        "Checked"           => 0,
        "SplitName1"        => 'NULL',
        "SplitName2"        => 'NULL',
        "SplitName3"        => 'NULL',
        "SplitTime1"        => 'NULL',
        "SplitTime2"        => 'NULL',
        "SplitTime3"        => 'NULL',
        "SplitJump1"        => 0,
        "SplitJump2"        => 0,
        "SplitJump3"        => 0,
        "Notes"             => $_POST['notes'] . ((!empty($_POST['booked_by'])) ? " [ {$_POST['booked_by']} " . date("Y/m/d") . " ] " : ""),
        "Agent"             => $_POST['agentName'],
        "UserName"          => $myusername,
        "CancelFee"         => $cancelFee,
        "GroupBooking"      => $groupBooking
    );
    if ($data['Rate'] == 0) {
        $data['foc'] = $_POST['foc0'];
    }
    send_backup_email($data, true);
    db_perform('customerregs1', $data);

    if (isset($_POST["both"]) && !empty($_POST["both"]) && $_POST["both"] != SYSTEM_SUBDOMAIN) {
        $bid = mysql_insert_id();
        header('Location: /Reception/splitBookingIE.php?id=' . $bid . "&allowChangeNote=0");
    } else {
        header('Location: /Reception/staffDisclaimerIE.php?target=dhfjadfha789987987&result1=true&dispDt=' . $jumpDate);
        die();
    };
}
$staff_names = BJHelper::getStaffList('StaffListName', true);

?>
<html>
<head>
    <?php include 'includes/header_tags.php'; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
    <meta http-equiv="X-UA-Compatible" content="IE=9">


    <script src="js/functions.js" type="text/javascript"></script>
    <?php include '../includes/head_scripts.php'; ?>


    <title>Registration</title>
</head>
<script type="text/javascript">

    function limitDropDown() {
        var len = <?php echo $i; ?>;
        if (len != 9999) {
            document.getElementById('noOfJump').length = <?php echo $i; ?>;
        } else {
            //hideDiv();
        }
    }
    function setFocus() {
        document.regform.noOfJump.focus();
    }
    function procBack() {
        if (document.getElementById('back')) {
            var randomnumber = Math.floor(Math.random() * 999999999);
            document.regform.action = "makeBookingIE.php?dispdate=" + encodeURIComponent('<?php echo $_GET['bookingdate']; ?>');
            document.regform.submit();
        }
    }
    function onlyNumbers(evt) {
        var e = event || evt; // for trans-browser compatibility
        var charCode = e.which || e.keyCode;

        if (charCode > 31 && (charCode < 48 || charCode > 57))
            return false;

        return true;

    }
    <?php
    $current_subdomain = strtolower(SYSTEM_SUBDOMAIN);
    $double_subdomains = array('minakami', 'sarugakyo');
    $new_subdomain = '';
    if (in_array($current_subdomain, $double_subdomains)) {
        $ns = array_diff($double_subdomains, array($current_subdomain));
        $new_subdomain = array_pop($ns);
    };
    ?>
    function validate() {
        var phNoLen = document.regform.teleno.value.length;
        if (document.regform.agentName.value == '') {
            alert('Please fill Agent Name!');
            document.regform.agentName.focus();
            return false;
        }
        if (document.regform.bookingtype.value == '-1') {
            alert('Please fill Booking Type!');
            document.regform.bookingtype.focus();
            return false;
        }
        if (document.regform.lastname.value == '') {
            alert('Please fill in your Last Name!');
            document.regform.lastname.focus();
            return false;
        }
        if (document.regform.firstname.value == '') {
            alert('Please fill in your First Name!');
            document.regform.firstname.focus();
            return false;
        }
        if (document.regform.booked_by.value == '') {
            alert('Please select to Booked staff name!');
            document.regform.booked_by.focus();
            return false;
        }        
        if (document.regform.teleno.value.match(/^\s*$/) || phNoLen == 0 || phNoLen < 9 || phNoLen > 11) {
            alert("Phone No. shouldn't be NULL or less than 9 or greater than 11");
            document.regform.teleno.focus();
            return false;
        }
        if ((regform.group1[0].checked == false) && (regform.group1[1].checked == false)) {
            alert('Please fill Payment to be Made!');
            return false;
        }
        if (document.regform.rate.value == '-1') {
            alert('Please select Rate per Customer!');
            document.regform.rate.focus();
            return false;
        }
        for (var i = 0; i < 4; i++) {
            if ($("#rate" + i).val() == 0 && $("#foc" + i).val() == '') {
                alert('Please select FOC!');
                document.regform['foc' + i].focus();
                return false;
            }
            ;
        }

        document.regform.Submitinput.disabled = "true";
        document.regform.back.disabled = "true";
        if ($("#double-booking").prop('checked')) {
            var sar = window.open('about:blank', '<?php echo $new_subdomain; ?>');
            document.regform.target = "<?php echo $new_subdomain; ?>";
            var old_action = document.location.href;
            document.regform.action = document.location.href.replace(/<?php echo $current_subdomain; ?>/, '<?php echo $new_subdomain; ?>');
                document.regform.submit();
            document.regform.action = old_action;
            document.regform.target = "_self";
        }
        ;
        document.regform.Submitinput.value = "Submitting";
        document.regform.submit();
        return true;

    }

    function triggerAgt() {
        // MisterSoft rates changes
        var price = 0;
        var onsite = $("input[name=group1]:checked").val();
        var agent_rate = null;
        $('#agentName > option').each(function () {
            if ($(this).prop('selected') === true) {
                price = $(this).attr('price');
                if (onsite == "Offsite") {
                    //price = '0';
                }
                ;
                agent_rate = "0";
                if ('<?php echo SYSTEM_SUBDOMAIN; ?>' == 'MINAKAMI' && $(this).html() == 'SG Bungy') {
                    if (onsite == 'Onsite') {
                        price = '<?php echo $config['first_jump_rate']; ?>';
                        agent_rate = '7000';
                    } else {
                        price = '<?php echo $config['combo_jump_rate']; ?>';
                        agent_rate = "0";
                    };
                };
                if ('<?php echo SYSTEM_SUBDOMAIN; ?>' == 'SARUGAKYO' && $(this).html() == 'MK Bungy') {
                    if (onsite == 'Onsite') {
                        price = '<?php echo $config['first_jump_rate']; ?>';
                        agent_rate = '5000';
                    } else {
                        price = '<?php echo $config['combo_jump_rate']; ?>';
                        agent_rate = "0";
                    };
                };
            };
        });

        $('#rate0 > option').each(function () {
            if ($(this).html() == price) {
                $(this).prop('selected', true);
            };
        });
        if (agent_rate != null && agent_rate != 0) {
            $('#agtratetopay').val(agent_rate);
            if (agent_rate == "") {
                $('#nos').val(0);
            } else {
                $('#nos').val($('#noOfJump').val());
            }
        }
        // EOF MisterSoft
        var agentName = $('#agentName').val();
        if (agentName != 'NULL' && agentName != '') {
            $('#bookingtype').val('Agent');
        } else {
            $('#bookingtype').val('Phone');
        }
        ;
    }
    $(document).ready(function () {
        $("input[name=group1]").on('click', triggerAgt);
        update_foc();
        $('#rate0, #rate1, #rate2, #rate3').change(update_foc);
    });
    function update_foc() {
        for (var i = 0; i < 4; i++) {
            var disabled = 'disabled';
            if ($('#rate' + i).val() == '0') {
                disabled = false;
            } else {
                $('#foc' + i).val('');
            }
            ;
            $('#foc' + i).attr('disabled', disabled);
        }
    }

</script>

<body onload="javascript:limitDropDown();setFocus();triggerAgt();">
<br>
<form name="regform" method="post">

    <table width="420px" align="center">
        <tbody>
        <tr>
            <td colspan="2" align="center" bgcolor="#000000">
                <h2><font color="#FFFFFF">
                        <?php echo ucfirst(SYSTEM_DATABASE_INFO); ?> Booking Details</font></h2>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FF0000"><font color="#FFFFFF">Jump Date:&nbsp;<?php echo $new_date_format; ?></font></td>
            <td bgcolor="#FF0000"><font color="#FFFFFF">Jump Time:&nbsp;<?php echo $chkInTime; ?></font></td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" align="right"><font color="#FFFFFF">Select No of Jumps:</font></td>
            <td bgcolor="#FF0000">
                <select name="noOfJump" id="noOfJump">
                    <?php
                    for ($j = 1; $j <= $i; $j++) {
                        ?>
                        <option value="<?php echo $j; ?>"><?php echo $j; ?></option>
                    <?php }; ?>
                </select>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" align="right"><font color="#FFFFFF">Agent Name:</font></td>
            <td bgcolor="#FF0000"><?php draw_agent_drop_down('agentName', 'NULL', 'onChange="triggerAgt();"'); ?></td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" align="right"><font color="#FFFFFF">Booking Type:</font></td>
            <td bgcolor="#FF0000">
                <?php
                echo "<select name='bookingtype' id='bookingtype'>";
                echo "<option id='-1' value='-1' selected='selected'>--Select--</option>";
                for ($n = 0; $n < count($btyArray); $n++) {
                    $selected = '';
                    if ($btyArray[$n] == 'Phone') {
                        $selected = ' selected ';
                    }
                    if ($btyArray[$n] == 'Agent&Walkin') {
                        continue;
                    }
                    echo "<option $selected value='$btyArray[$n]'>$btyArray[$n]</option>";
                }
                ?>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" align="right"><font color="#FFFFFF">Last Name:</font></td>
            <td bgcolor="#FF0000"><input type="text" name="lastname" style="text-transform: uppercase"></td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" align="right"><font color="#FFFFFF">First Name:</font></td>
            <td bgcolor="#FF0000"><input type="text" name="firstname" style="text-transform: uppercase"></td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" align="right"><font color="#FFFFFF">Phone No:</font></td>
            <td bgcolor="#FF0000"><input type="text" name="teleno"></td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" align="right"><font color="#FFFFFF">Payment to be made:</font></td>
            <td bgcolor="#FF0000">
                <input type="radio" name="group1" value="Onsite"><font color="#FFFFFF">Onsite</font>
                <input type="radio" name="group1" value="Offsite"><font color="#FFFFFF">Offsite</font>
            </td>
        </tr>
        <tr>
            <td bgcolor="##FF0000" align="right"><font color="#FFFFFF">To Pay to Agent:</font>

            </td>
            <td bgcolor="##FF0000">
                <?php
                echo draw_pull_down_menu('agtratetopay', BJHelper::getRatesList(true), '', "id='agtratetopay'");
                ?>
                <font color="#FFFFFF">X</font>
                <select name="nos" id="nos">
                    <option value="0" selected="selected">0</option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                </select>
            </td>
            <!--
            <td bgcolor="##FF0000" align="right"><font color="#FFFFFF">Jumpers:</font>

            </td>
            -->
        </tr>
        <tr>
            <td bgcolor="#FF0000" align="right"><font color="#FFFFFF">Rate per Customer:</font></td>
            <td bgcolor="#FF0000">
                <nobr>
                    <?php
                    echo draw_pull_down_menu('rate', BJHelper::getRatesList(true), $config['first_jump_rate'], "id='rate0'");
                    echo "<font color=\"#FFFFFF\">FOC*</font>";
                    echo draw_pull_down_menu("foc0", getFOCTitles(), '', "id=\"foc0\"");
                    ?>
                </nobr>
            </td>
        </tr>

        <tr>
            <td bgcolor="#FF0000" align="right"><font color="#FFFFFF">Transportation Mode:</font></td>
            <td bgcolor="#FF0000">
                <select name="modeTransport" id="modeTransport">
                    <option value="Bullet Train">Bullet Train</option>
                    <option value="Local Train">Local Train</option>
                    <option value="Car" selected="selected">Car</option>
                    <option value="Bus">Bus</option>
                    <option value="Motorbike">Motorbike</option>
                </select>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" align="right"><font color="#FFFFFF">Notes:</font>
            </td>
            <td bgcolor="#FF0000">
                <textarea rows="3" cols="20" name="notes"></textarea>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" align="right"><font color="#FFFFFF">Booked by:</font>
            </td>
            <td bgcolor="#FF0000">
                <?php echo draw_pull_down_menu('booked_by', $staff_names, '', 'style="width: 180px;" required'); ?>
            </td>
        </tr>
        <tr>
            <td colspan="2" align="center" bgcolor="#000000">
                <input type="button" name="back" id="back" style="float: left;background-color:#FFFF66;border-style:ridge;border-color:#FF0000;" value="Back" onclick="procBack();"/>
                <input name="Submitinput" type="button" style=" float: right;background-color:#FFFF66;border-style:ridge;border-color:#FF0000;" value="Book Now" onclick="validate();"/>
                <?php
                if (!empty($new_subdomain)) {
                    ?>
                    <input name="both" type="checkbox" id="double-booking" style="float: right;" value="<?php echo SYSTEM_SUBDOMAIN; ?>">
                    <label for="double-booking" style="color: white; float: right;">Both site registration</label>
                    <?php
                };
                ?>
            </td>
        </tr>
        </tbody>
    </table>
</form>
<?php
include("ticker.php");
?>
