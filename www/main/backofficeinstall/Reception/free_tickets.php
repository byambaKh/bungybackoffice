<?php
	include '../includes/application_top.php';
	$record_type = 'free_tickets';
	$headers = array(
		"Ticket No.",
		"Issue Date",
		"Use Date",
		"Name",
		"Type",
		"Notes",
	);
	$jp_titles = array(
		"Issue Date"	=> "発行日",
		"Use Date"		=> "使用日",
	);
	$fields = array_map(function ($item) {
		return strtolower(
			preg_replace(
				"/(_)$/",
				"",
				preg_replace("([^\w]+)", "_", $item)
			)
		);
	}, $headers);
	
	$values['type'] = array_map(function ($item) {
		return array(
			'id'	=> $item,
			'text'	=> $item
		);
	}, array("NJ", "CA", "Promo", 'Gift'));

	$action = $_POST['action'];
	if (empty($action)) {
		$action = $_GET['action'];
	};
	switch (TRUE) {
		case ($action == 'Add'):
			$_POST['i']['site_id'] = CURRENT_SITE_ID;
			db_perform('free_tickets', $_POST['i']);
			Header("Location: free_tickets.php");
			die();
			break;
		case ($action == 'Update'):
			$id = $_POST['i']['id'];
			unset($_POST['i']['id']);
			save_report('free_tickets', $_POST['i'], "update", "id='{$id}'");
			Header("Location: expenses.php");
			die();
			break;
		case ($action == 'Delete'):
			$sql = "update expenses set deleted = 1 where site_id = '".CURRENT_SITE_ID."' AND id = '{$_GET['id']}'";
			mysql_query($sql) or die(mysql_error());
			Header("Location: expenses.php");
			die();
			break;
	};
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo SYSTEM_SUBDOMAIN; ?> Free Ticket List</title>
<?php include "../includes/head_scripts.php"; ?>
<style>
h1 {
	margin: 0;
	text-align: center;
	width: 100%;
	padding: 5px;
}
#expenses-table {
	width: 750px;
	margin-left: auto;
	margin-right: auto;
	border-collapse: collapse;
}
#expenses-table th {
	color: black;
	border: 1px solid black;
	padding-left: 10px;
	padding-right: 10px;
}
#expenses-table td {
	vertical-align: top;
	text-align: center;
	font-size: 11px;
	background-color: white;
	overflow: hidden;
	border: 1px solid black;
	vertical-align: middle;
	width: 70px;
}
#expenses-table input, #expenses-table select {
	font-size: 11px;
	width: 95%;
	border: 1px solid black;
}
#expenses-header td {
	background-color: red;
}
	#totals-table {
		min-width: 200px;
		margin-top: 50px;
		margin-left: 100px;
		border-collapse: collapse;
		border-width: 0px 0px 0px 0px;
		border-style: none;
		border-spacing: 0px;
	}
	#totals-table tr#header-row td {
		background-color: black;
		color: white;
		font-weight: bold;
	}
	#totals-table td {
		background-color: white;
		padding: 2px;
		border: 1px solid black;
	}
	#totals-table td.totals-value {
		text-align: right;
	}
	#totals-table tr#grand-total-row td {
		border-top: 2px solid black;
		border-bottom: 2px solid black;
		border-left: none;
		border-right: none;
		padding:2px;
		font-weight: bold;
	}
#purchaser-header {
	font-size: 11px;
}
#sheet-header {
	font-size: 11px;
}
#expenses-table input.change, #expenses-table input.delete, #expenses-table input.cancel, #expenses-table input.update {
    width: 60px;
    font-size: 12px;
    padding: 0px;
    margin: 2px;
    border: 1px solid black;
}
#expenses-table select {
	width: auto !important;
}
.action-change-delete {
	border: none !important;
}
#action-add {
	border: none !important;
}
#action-add input {
	width: 80px !important;
}
.ticket_no input, .date input {
	text-align: center;
}
#back {
	float: left;
}
</style>
<script>
	function row_update() {
		var id = $(this).attr('rel');
		var post_data = {};
		post_data.id = id;
		$(this).parent().parent().parent().children("td.value").each(function () {
			var field = $(this).attr('rel');
			post_data[field] = $(this).children('[name='+field+']').prop('value');
			if (field == 'shop_name' && $(this).children('[name='+field+'_new]').prop('value') != '') {
				post_data[field+'_new'] = $(this).children('[name='+field+'_new]').prop('value');
			};
		});
		$.ajax({
			url: 'ajax_free_tickets.php?action=update_row',
			method: 'POST', 
			data: post_data,
			context: $(this)
		}).done(function () {
			$(this).parent().parent().parent().children("td.value").each(function () {
				var field = $(this).attr('rel');
				if (field == 'shop_name' && $(this).children('[name='+field+'_new]').prop('value') != '') {
					$(this).html($(this).children('[name='+field+'_new]').prop('value'));
					fvalues[field].push({
						'id': $(this).html(), 
						'text' :$(this).html()
					});
					fvalues[field] = fvalues[field].sort(function (item1, item2) {
						return (item1.text > item2.text);
					});
				} else {
					$(this).html($(this).children('[name='+field+']').prop('value'));
				};
			});
			$(this).prop('value', 'Change').removeClass('update').addClass('change').unbind('click').click(row_change);
			$(this).parent().children(".cancel").prop('value', 'Delete').removeClass('cancel').addClass('delete').unbind('click').click(row_delete);
			update_totals();
		});
	}
	function row_delete() {
		var id = $(this).attr('rel');
		if (window.confirm("Are you sure to delete this record?")) {
			//document.location = "?action=Delete&id=" + id;
			var post_data = {"id":id};
			if ($('#delete_'+post_data.id).prop('checked')) {
				post_data.delete_all = true;
			};
			$.ajax({
				url: 'ajax_free_tickets.php?action=delete_row',
				method: 'POST', 
				data: post_data,
				context: $(this)
			}).done(function () {
				document.location.reload();
			}).fail(function () {
				alert('Record delete failed.');
			});
		};
	}
	function row_cancel() {
		var id = $(this).attr('rel');
		$(this).parent().parent().parent().children("td.value").each(function () {
			var field = $(this).attr('rel');
			$(this).html($(this).children('input[type=hidden]').prop('value'));
		});
		$(this).parent().children(".update").prop('value', 'Change').removeClass('update').addClass('change').unbind('click').click(row_change);
		$(this).prop('value', 'Delete').removeClass('camcel').addClass('delete').unbind('click').click(row_delete);
	}
	function row_change() {
		var id = $(this).attr('rel');
		$(this).parent().parent().parent().children("td.value").each(function () {
			var input_field = '';
			var field = $(this).attr('rel');
			var value = $(this).html();
			if (typeof field != 'undefined' && fvalues[field].length > 0) {
				input_field = $('<select>').prop('name', field).prop('id', field + '_edit');
				for (fv in fvalues[field]) {
					if (fvalues[field].hasOwnProperty(fv)) {
						input_field.append(
							$('<option>').attr('value', fvalues[field][fv].id).text(fvalues[field][fv].text)
						);
					};	
				};
				input_field.prop('value', $(this).html());
			} else {
				input_field = $('<input>').prop('name', field).prop('value', $(this).html()).prop('id', field + '_edit');
			};
			$(this).html(input_field);
			$(this).append(
				$('<input>').prop('type', 'hidden').prop('name', field + '_old').prop('value', value)
			);
			if (field == 'shop_name') {
				$(this).append($('<br />')).append(
					$('<input>').prop('name', field + '_new').prop('id', field + '_edit')
				);
				$("input#shop_name_edit").autocomplete({ 
					minLength: 1,
					source: window.ac_values
				});
			};
		});
		$(this).prop('value', 'Update').removeClass('change').addClass('update').unbind('click').click(row_update);
		$(this).parent().children(".delete").prop('value', 'Cancel').removeClass('delete').addClass('cancel').unbind('click').click(row_cancel);
    	$('.date, .date input, input[name=date]').datepicker({
                showAnim: 'fade',
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd',
                currentText: 'Today',
                maxDate: new Date(<?php echo $maxDate?>)
    	});

	};
	function update_totals() {
		var totals = {};
		$("#expenses-table tr").each(function () {
			var cost = $(this).children('[rel=cost]').html();
			var sheet = $(this).children('[rel=sheet]').html();
			if (typeof cost != 'undefined') {
				if (!totals.hasOwnProperty(sheet)) {
					totals[sheet] = 0;
				};
				totals[sheet] += parseInt(cost);
			};
		});
		var grand_total = 0;
		$('#totals-table tr').remove();
		$('#totals-table').append(
			$('<tr id="header-row">').append(
				$('<td>').html('SHEET')
			).append(
				$('<td>').html('TOTAL')
			)
		);
		for (i in totals) {
			if (totals.hasOwnProperty(i)) {
				grand_total += totals[i];
				$('#totals-table').append(
					$('<tr>').append(
						$('<td>').html(i)
					).append(
						$('<td>').html(totals[i]).addClass('totals-value')
					)
				);
			};
		}
		$('#totals-table').append(
			$('<tr id="grand-total-row">').append(
				$('<td>').html('')
			).append(
				$('<td>').html('&yen;' + grand_total).addClass('totals-value')
			)
		);
	};
$(document).ready(function () {
	$('.date, .date input').datepicker({
                showAnim: 'fade',
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd',
                currentText: 'Today',
				defaultDate: '<?php echo date("Y-m-d"); ?>',
                maxDate: new Date(<?php echo $maxDate?>)
	});
	$(".delete").click(row_delete);
	$('.change').unbind('click').click(row_change);
	update_totals();
	window.ac_values = [];
	for (i in fvalues.shop_name) {
		if (fvalues.shop_name.hasOwnProperty(i)) {
			window.ac_values.push(fvalues.shop_name[i].text);
		};
	};
	$("input#shop_name-new").autocomplete({ 
		minLength: 1,
		source: window.ac_values
	});
});
	var fvalues = {};
<?php
	foreach ($fields as $field) {
		echo "\tfvalues.$field = " . json_encode(array_key_exists($field, $values) ? $values[$field] : array()) . ";\n";
	};
?>
</script>
</head>
<body>
<table id="expenses-table">
<tr>
	<td colspan="<?php echo sizeof($fields); ?>" style="padding: 0px; border: 2px solid black !important;">
    	<h1><?php echo SYSTEM_SUBDOMAIN; ?> Free Ticket List</h1>
	</td>
<tr>
<?php
	//echo "<th>D</th>";
	foreach ($fields as $key => $field) {
		if (array_key_exists($headers[$key], $jp_titles)) {
			$headers[$key] = $jp_titles[$headers[$key]];
		};
		echo "\t<th id=\"$field-header\">{$headers[$key]}</th>\n";
	};
?>
		<!--th id="actions-header">Actions</th-->
</tr>
<?php
	// delte tickets more that ONE YEAR OLD
	$one_year_old_date = date("Y-m-d", mktime(0, 0, 0, date('m'), date('d'), date('Y') - 1));
	$sql = "UPDATE $record_type SET deleted = 1 WHERE site_id = '".CURRENT_SITE_ID."' AND issue_date <= '$one_year_old_date';";
	$res = mysql_query($sql) or die(mysql_error());
	$d = date('Y-m-', $current_time);
	$sql = "select * from $record_type WHERE site_id = '".CURRENT_SITE_ID."' AND deleted = 0 order by id ASC;";
	$res = mysql_query($sql);
	while ($row = mysql_fetch_assoc($res)) {
?>
<tr>
<?php
		$input = ($row['use_date'] == '0000-00-00') ? '' : "<input name='delete[{$row['id']}]' type='checkbox' value='on' id='delete_{$row['id']}'>";
		//echo "<td>$input</td>";
		foreach ($fields as $key => $field) {
			$add_class = '';
			if (preg_match("/(date)/", $field)) {
				$add_class = 'date';
			};
			if ($row[$field] == '0000-00-00') $row[$field] = '';
			echo "\t<td id=\"$field-{$row['id']}\" class=\"value $field $add_class\" rel=\"$field\">{$row[$field]}</td>\n";
		};
?>
	<td class="action-change-delete"><nobr><input type="button" value="Change" rel="<?php echo $row['id']; ?>" class="change"><input type="button" value="Delete" rel="<?php echo $row['id']; ?>" class="delete"></nobr></td>
</tr>
<?php
	};
?>
<tr>
	<th id="add-new" colspan="<?php echo sizeof($fields) + 1; ?>" style="border: none !important;">&nbsp;</th>
</tr>
<form method="POST">
<tr>
<?php
	foreach ($fields as $key => $field) {
		$record = array();
		$input_field = draw_input_field($field, $field, $record, '', false);
		if (array_key_exists($field, $values)) {
			$input_field = draw_pull_down_menu('i[' . $field . ']', $values[$field], '', 'id="' . $field . '"');
		};
		if ($field == 'shop_name') {
			$input_field .= '<br />' . draw_input_field($field . '_new', $field . '-new', $record, '', false);
		};
		$add_class = '';
		if (preg_match("/(date)/", $field)) {
			$add_class = 'date';
		};
		echo "\t<td id=\"$field-new\" class=\"$field $add_class\">" . $input_field . "</td>\n";
	};
?>
	<td id="action-add"><input type="submit" name="action" value="Add"></td>
</tr>
<tr>
	<td style="border: none !important;" colspan="<?php echo sizeof($fields) + 1; ?>">
		<button id="back" onClick="javascript: document.location='/Reception/dailyViewIE.php';" type="button">Back</button>
	</td>
</tr>
</form>
</table>
<form>
<?php include ("ticker.php"); ?>
</body>
</html>
