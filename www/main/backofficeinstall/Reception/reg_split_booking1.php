<?php
include("../includes/application_top.php");

$_POST['jumpDate'] = preg_replace("/([^-^0-9])/", "", $_POST['jumpDate']);
$target = $_GET['target'];

$cname = $_SESSION['cname'];
$bookingdate = $_SESSION['jumpDate'];
$BookingTime = $_SESSION['chkInTime'];
$_SESSION['chkInTime'] = $_POST['chkInTime'];

//$nts = $_SESSION['notes'];
//echo "---------->>>".$nts;

$allowChangeNote = 1;
if(isset($_GET['allowChangeNote']))
    $allowChangeNote = $_GET['allowChangeNote'];

$changedFrom = "";
if (($_POST['oldtime'] !== $_POST['chkInTime']) && $allowChangeNote) $changedFrom = "Changed From {$_POST['oldtime']}";

//This is to prevent the "Changed From " appearing on combo bookings
if (isset($_POST['both']) && ($_POST['both'] != '')) $changedFrom = '';

if (is_null($_POST['notes'])) {
    $nts = $_SESSION['notes'] . " $changedFrom";
} else {
    $nts = $_POST['notes'] . " $changedFrom";
}

$BookingTime1 = $_SESSION['chkInTime'];
//echo "^^^^^".$BookingTime1;
//echo "-----".$bookingdate;
//echo $BookingTime;

$cregid = $_POST['cregid'];
//echo $cregid."###";

$SplitBookName1 = $_POST['splitBookName1'];
$SplitBookName2 = $_POST['splitBookName2'];
$SplitBookName3 = $_POST['splitBookName3'];

$lastname = $_POST['lastname'];
$firstname = $_POST['firstname'];
//echo $SplitBookName1."---".$SplitBookName2."--".$SplitBookName3;

//echo ">>>".$lastname.">>>".$firstname;

$SplitTime1 = $_POST['splitTime1'];
$SplitTime2 = $_POST['splitTime2'];
$SplitTime3 = $_POST['splitTime3'];
$SplitJump1 = $_POST['splitJump1'];
$SplitJump2 = $_POST['splitJump2'];
$SplitJump3 = $_POST['splitJump3'];

$totalJump = $SplitJump1 + $SplitJump2 + $SplitJump3;

//echo $totalJump;

//echo "Booking dates ;) " .$bookingdate;
//echo "Split Jump 1 ;) " .$SplitJump1;
//echo "Split Time 1 ;) " .$SplitTime1;

$lname = $_POST['lastname'];
$fname = $_POST['firstname'];

/*******************************************New Code Start****************************/

$romaji = $lastname . ' ' . $firstname;
//echo "^^^^^^^^^^^^^^^^^^^^^^^".$romaji;
$CustomerAddress = "NULL";
$PostalCode = "NULL";
$Prefecture = "NULL";
//$ContactNo = 0;
$CustomerEmail = "NULL";
$OtherNo = "NULL";
$TransportMode = "NULL";
$deleteStatus = (isset($_POST['baction']) && $_POST['baction'] == 'cancel') ? 1 : 0;
$dayStatus = 0;
$timeStatus = 0;
$bookingType = "Internet";
$rate = 7500;
$checked = 0;
$ratetopay = 0;//$_POST['agtratetopay'];
$myusername = $_SESSION['myusername'];

$sel_Group = $_POST['group1'];
if ($sel_Group == 'Onsite') {
    //$ratetopay = $_POST['rate'];
    $collectpay = "Onsite";
} else if ($sel_Group == 'Offsite') {
    $collectpay = "Offsite";

}
$sql_sd = "SELECT BookingDate, BookingTime FROM customerregs1 WHERE CustomerRegID = '" . $cregid . "'";
$result_sd = mysql_query($sql_sd, $conn) or die(mysql_errno() . ":<b> " . mysql_error() . "</b>");
$row_sd = mysql_fetch_assoc($result_sd);
// Place Changing
$changedPlace = false;
if (isset($_POST['place']) && !empty($_POST['place'])) {
    $deleteStatus = 1;
    $changedPlace = true;
};
// Change to Combo booking
if (isset($_POST["both"]) && !empty($_POST["both"])) {
    $nts .= " Changed to MK/SG Combo";
    $_POST['nos'] = $_POST['noOfJump'];
    switch (TRUE) {
        case (SYSTEM_SUBDOMAIN == 'MINAKAMI'):
            $_POST['agtratetopay'] = '6500';
            break;
        case (SYSTEM_SUBDOMAIN == 'SARUGAKYO'):
            $_POST['agtratetopay'] = '4000';
            break;
    };
    $_POST['agtratetopay'] = $config['combo_jump_rate'];
};

if ($_POST['olddate'] != $_POST['jumpDate'] && (!isset($_POST['place']) || empty($_POST['place']))) {
    $nts .= ' Changed from ' . $_POST['olddate'] . ' ' . $_POST['oldtime'];
};
$bookingaction = '';
if ($deleteStatus && !$changedPlace) {
    $bookingaction = ' <b>Cancelled by</b>';
};

$sql_update = "UPDATE customerregs1 SET " .
    ((isset($_POST['place']) && !empty($_POST['place'])) ? '' :
        "
			BookingDate = '" . $_POST['jumpDate'] . "',
            BookingTime = '" . $_POST['chkInTime'] . "',"
    ) . "
            NoOfJump   = '" . (($deleteStatus) ? 0 : $_POST['noOfJump']) . "',
            customerlastname = '" . strtoupper($_POST['lastname']) . "',
            customerfirstname = '" . strtoupper($_POST['firstname']) . "',
            customeraddress = '" . $_POST['address'] . "',
            romajiname = '" . strtoupper($romaji) . "',
            postalcode = '" . $_POST['postalcode'] . "',
            contactno = '" . $_POST['contactno'] . "',
            customeremail = '" . $_POST['email'] . "',
            rate = '" . $_POST['rate'] . "',
            agent = '" . $_POST['agentName'] . "',
            collectpay = '" . $collectpay . "',
			DeleteStatus = '" . $deleteStatus . "',
            ratetopay = '" . $_POST['agtratetopay'] . "',
            ratetopayqty = '" . $_POST['nos'] . "',
            MedicalConditionsChecked = '" . $_POST['MedicalConditionsChecked'] . "',
            transportmode = '" . $_POST['transportmode'] . "',
            notes = '" . mysql_real_escape_string($nts) . ((!empty($_POST['booked_by'] && $allowChangeNote)) ? " [$bookingaction {$_POST['booked_by']} " . date("Y/m/d") . " ] " : "") . "',
            UserName = '" . $myusername . "',
            foc = '" . (($_POST['rate'] == 0) ? $_POST['foc0'] : '') . "'
            WHERE CustomerRegID = '" . $cregid . "' ";
$result_update = mysql_query($sql_update, $conn) or die(mysql_errno() . ":<b> " . mysql_error() . "</b>");

$old_date = $_POST['jumpDate'];
if (isset($_POST['place']) && !empty($_POST['place'])) {
    $old_date = $_POST['olddate'];
};
$old_url = "http://{$_SERVER['HTTP_HOST']}/Reception/dailyViewIE.php?dispdate={$old_date}";

if (isset($_POST['place']) && !empty($_POST['place'])) {
    $new_rate = $_POST['rate'];
    /*
    MK 7500 = SG 10000
    MK 7000 = SG 9000
    MK 6500 = SG 9000 (if "Agent" = "Canyons")
    MK 4000 = SG 4500
    */
    switch (TRUE) {
        // Minakami to Sarugakyo price updates
        case (SYSTEM_SUBDOMAIN == 'MINAKAMI' && $_POST['place'] == 'sarugakyo' && $new_rate == '7500'):
            $new_rate = 10000;
            break;
        case (SYSTEM_SUBDOMAIN == 'MINAKAMI' && $_POST['place'] == 'sarugakyo' && $new_rate == '7000'):
            $new_rate = 9000;
            break;
        case (SYSTEM_SUBDOMAIN == 'MINAKAMI' && $_POST['place'] == 'sarugakyo' && $new_rate == '6500' && $_POST['agentName'] == 'Canyons'):
            $new_rate = 9000;
            break;
        case (SYSTEM_SUBDOMAIN == 'MINAKAMI' && $_POST['place'] == 'sarugakyo' && $new_rate == '4000'):
            $new_rate = 4500;
            break;
        // Sarugakyo to Minakami price updates
        case (SYSTEM_SUBDOMAIN == 'SARUGAKYO' && $_POST['place'] == 'minakami' && $new_rate == '10000'):
            $new_rate = 7500;
            break;
        case (SYSTEM_SUBDOMAIN == 'SARUGAKYO' && $_POST['place'] == 'minakami' && $new_rate == '9000' && $_POST['agentName'] == 'Canyons'):
            $new_rate = 6500;
            break;
        case (SYSTEM_SUBDOMAIN == 'SARUGAKYO' && $_POST['place'] == 'minakami' && $new_rate == '9000'):
            $new_rate = 7000;
            break;
        case (SYSTEM_SUBDOMAIN == 'SARUGAKYO' && $_POST['place'] == 'minakami' && $new_rate == '4500'):
            $new_rate = 4000;
            break;
    };
    $cdate = preg_replace("/([\d]{4})-([\d]{2})-([\d]{2})/is", "$2/$3", $row_sd['BookingDate']);
    $ctime = preg_replace("/([^\d^:]+)/", "", $row_sd['BookingTime']);
    $places = BJHelper::getPlaces();
    $new_site_id = CURRENT_SITE_ID;
    $site = 'Unknown';
    foreach ($places as $p) {
        if ($_POST['place'] == $p['subdomain']) {
            $new_site_id = $p['id'];
        };
        if ($p['id'] == CURRENT_SITE_ID) {
            $site = $p['short_name'];
        };
    };
//temporary check
    if ($new_site_id == CURRENT_SITE_ID) {
        die("Cannot locate target Place: " . $_POST['place']);
    }

    //really bad code
    $sql = str_replace(
        array(
            "UPDATE customerregs1 SET",
            "NoOfJump   = '0'",
            "rate = '" . $_POST['rate'] . "'",
            "DeleteStatus = '1'",
            "notes = '" . mysql_real_escape_string($nts) . ((!empty($_POST['booked_by'])) ? " [$bookingaction {$_POST['booked_by']} " . date("Y/m/d") . " ] " : "") . "'",
            "WHERE CustomerRegID = '" . $cregid . "'"
        ),
        array(
            "INSERT INTO customerregs1 SET
				site_id = '" . $new_site_id . "',
				BookingDate = '" . $_POST['jumpDate'] . "',
				BookingTime = '" . $_POST['chkInTime'] . "',",
            "NoOfJump   = '" . $_POST['noOfJump'] . "'",
            "rate = '" . $new_rate . "'",
            "DeleteStatus = '0'",
            "notes = '" . mysql_real_escape_string($nts) . " Changed from $site on $cdate at $ctime" . ((!empty($_POST['booked_by'])) ? " [ {$_POST['booked_by']} " . date("Y/m/d") . " ] " : "") . "'",
            ""
        ),
        $sql_update
    );
    $res = mysql_query($sql) or die(mysql_error());
    $_SERVER['HTTP_HOST'] = str_replace(SYSTEM_DATABASE_INFO, $_POST['place'], $_SERVER['HTTP_HOST']);
    $new_url = "http://{$_SERVER['HTTP_HOST']}/Reception/dailyViewIE.php?dispdate={$_POST['jumpDate']}";
};



?>
<html>
<head>
    <?php include 'includes/header_tags.php'; ?>
    <title>Register Split Booking</title>
    <meta http-equiv="refresh" content="0; URL=<?php echo $old_url; ?>">
</head>
<?php
if (isset($_POST['place']) && !empty($_POST['place'])) {
?>
<body onLoad="document.forms['rform'].submit();">
<form target="_blank" action="<?php echo $new_url; ?>" method="post" name="rform">
    <input type="hidden" name="both" value="<?php echo SYSTEM_SUBDOMAIN; ?>">
    <input type="submit">
</form>

<?php
} else {
?>
<body>
<?php
};
?>
</body>
</html>
