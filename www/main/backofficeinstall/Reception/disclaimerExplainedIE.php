<?php

include '../includes/application_top.php';
require $_SERVER['DOCUMENT_ROOT'] . '/Reception/GenerateXML.php';

// regid passed from form
$regid = $_POST['cregid'];
if (empty($regid)) {
    $regid = $_POST['chks'];
};
if ($gbid = isGroupBooking($regid)) {
    $regid = $gbid;
}
if (empty($regid)) {
    Header('Location: dailyViewIE.php');
    die();
};
$booking = getBookingInfo($regid);
if (empty($booking)) {
    Header('Location: dailyViewIE.php');
    die();
};
// if record already checkedIN
if ($booking[0]['Checked'] == 1) {
    foreach ($booking as $b) {
        unset($_SESSION['post_' . $b['CustomerRegID']]);
    };
    if (count($booking) == 1) {
        Header('Location: convert_to_group.php?regid=' . $regid);
    } else {
        Header('Location: moneyReceivedIE.php?regid=' . $regid);
    };
    die();
};

?>
<html>
<head>
    <?php include 'includes/header_tags.php'; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
    <meta http-equiv="X-UA-Compatible" content="IE=8">

    <script src="js/functions.js" type="text/javascript"></script>

    <title>
    </title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
    <link rel="stylesheet"
          href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css"
          type="text/css" media="all"/>

    <script type="text/javascript">
        function goProc() {
            //if(!document.dform.chk.checked){
            //alert("Please check Disclaimer Explained!!!");
            //return false;
            //}
            if (document.getElementById('gob')) {
                document.dform.action = "moneyReceivedIE.php";
                document.dform.submit();
                return true;
            }
        }
        function procBack() {
            if (document.getElementById('back')) {
                document.dform.action = "dailyViewIE.php";
                document.dform.submit();
                return true;
            }
        }

        function openMedicalConditionsChecklist(language) {
            window.open("healthChecklist.php?language=" + language, "_blank", "width=800, menubar=no, toolbar=no, scrollbars=yes, location=no, resizable=yes");
        }

    </script>
</head>
<body><br>


<form name="dform" action="" method="post">
    <input type="hidden" name="cregid" value="<?php print $regid; ?>">
    <table align="center" border="0" width="500px" bgcolor="#000000">
        <tr>
            <td align="right">
                <input type="button" name="back" id="back" value="BACK" style="background-color:#FFFF66" onclick="procBack();">
            </td>
        </tr>
    </table>
    <table border="0" align="center" width="500px">
        <tbody>
        <tr>
            <td colspan="5" align="center" bgcolor="#000000">
                <font color="#FFFFFF"><b>Booking Details</b></font>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" align="center">
                <font color="#FFFFFF">Time</font>
            </td>
            <td bgcolor="#FF0000" align="center">
                <font color="#FFFFFF">Name</font>
            </td>
            <td bgcolor="#FF0000" align="center">
                <font color="#FFFFFF">Jump</font>
            </td>
            <!--
            <td bgcolor="khaki" align="center">
                    <font size='1.5'>Price&nbsp;&yen;</font>
            </td>
            -->
        </tr>
        <tr>
            <td>
                <?php

                foreach ($booking as $row) {
                    // if empty jumps - continue to next one
                    if (!$row['NoOfJump']) continue;
                    // process each booking row
                    echo "\t<tr>\n";
                    echo "\t\t<td>" . $row['BookingTime'] . "</td>\n";
                    echo "\t\t<td>" . $row['CustomerLastName'] . ' ' . $row['CustomerFirstName'] . "</td>\n";
                    echo "\t\t<td align=\"center\">" . $row['NoOfJump'] . "</td>\n";
                    echo "\t<tr>\n";
                };
                ?>
        <tr>

            <td bgcolor="#000000" colspan="2">
                <font color="#FFFFFF">Medical Conditions Checked:</font>
                <input type="hidden" name="MedicalConditionsChecked" value="0"/>
                <input type="checkbox" name="MedicalConditionsChecked" value="1"/>
            </td>
            <td bgcolor="#000000" colspan="3" align="right">
                <input type="button" name="go" value="GO" id="gob" style="background-color:#FFFF66" onclick="goProc();"/>
            </td>
        </tr>
        <tr>
            <td bgcolor="#000000" colspan="3" align="right">
                <input type="button" value="Checklist" class="checklistButton" style="margin:5px; float:left; background-color:#FFFF66" onclick="openMedicalConditionsChecklist('eng')"/>&nbsp;
                <input type="button" value="チェックリスト" class="checklistButton" style="margin:5px; float:left; background-color:#FFFF66" onclick="openMedicalConditionsChecklist('jpn')"/>&nbsp;
            </td>

        </tr>
        </tbody>
    </table>
</form>
</body>
</html>
