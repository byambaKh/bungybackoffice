<?php
include ("../includes/application_top.php");

$_SESSION['startDate'] = $_POST['startDate'];
$_SESSION['endDate'] = $_POST['endDate'];


//echo '--->'.$_POST['startDate'];
$sttime = strtotime($_SESSION['startDate']);
$totime = strtotime($_SESSION['endDate']);

?>
<!DOCTYPE html>
<html>
    <head>
<?php include '../includes/header_tags.php'; ?>
        <meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
        <meta http-equiv="X-UA-Compatible" content="IE=8">
        <script src="js/functions.js" type="text/javascript"></script>
<?php include '../includes/head_scripts.php'; ?>

        <title>Daily Booking History</title>
        
<style>
.checked td {
    background-color: lightgreen !important;
}
.deleted td {
    background-color: #faa !important;
}

#booking-history {
	border-collapse: collapse;
	width: 95%;
	margin-left: auto;
	margin-right: auto;
}
#booking-history th {
	background-color: black;
	color: white;
	border: 1px solid black;
}
#booking-history td {
	border: 1px solid black;
}
</style>        
        

        <script type="text/javascript">
            $(function() {
                $mydate1 = $('#startDate').datepicker({
                    numberOfMonths: 1,    
                    dateFormat: 'yy-mm-dd',
                    altField: '#alternate',
                    altFormat: 'yy-mm-dd'
                });
            });
            $(function() {
                $mydate2 = $('#endDate').datepicker({
                    numberOfMonths: 1,    
                    dateFormat: 'yy-mm-dd',
                    altField: '#alternate',
                    altFormat: 'yy-mm-dd'
                });
            });
			$(document).ready(function () {
				$('#startDate').change(function () {
					if ($('#endDate').val() == '' || $('#endDate').val() < $(this).val()) $('#endDate').val($(this).val());
				});
			});
            
            function checkDates(){
                var stval = $('#startDate').val();
                var edval = $('#endDate').val();
                
                if(stval == ''){
                    alert("From Date is required!!!");
                    return false;
                }
                if(edval == ''){
                    alert("To Date is required!!!");
                    return false;
                }
              
                document.dailyForm.submit();
                return true;
            }
            function procBack(){
                if(document.getElementById('back')){
                    document.dailyForm.action = '/Reception/makeBookingIE.php';
                    document.dailyForm.submit();
                    return true;
                }
                return false;
            }
        </script>    
    </head>
    <body>

        <form action="" name="dailyForm" method="post">
            <input type="hidden" name="startDate" value="<?php print $_POST['startDate']; ?>">
            <input type="hidden" name="endDate" value="<?php print $_POST['endDate']; ?>">
            <table width="40%" bgcolor="#FF0000" align="center">
                <thead>
                    <tr>
                        <td colspan="4" bgcolor="#000000" align="center">
                            <b><font color="#FFFFFF">Daily View History</font></b>
                        </td>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td align="right">
                            <b>From Date:</b>
                        </td>
                        <td align="right">
                            <input type="text" name="startDate" id="startDate" size="14"
                                   value="<?php echo $_POST['startDate']; ?>"/> 
                        </td>
                        <td align="right">
                            <b>To Date:</b>
                        </td>
                        <td align="right">
                            <input type="text" name="endDate" id="endDate" size="14" 
                                   value="<?php echo $_POST['endDate'];?>"/> 
                        </td>
                    </tr>
                    <tr>
                        <td colspan="4" align="right">
                                    <input type="button" name="historydata" value="View History Data" onclick="checkDates();">
                                    <input type="button" name="back" id="back" value="Back" onclick="procBack();">
                        </td>
                    </tr>
                </tbody>
            </table><br>
<table id="booking-history">
<tr>
	<th>Booking Date</th>
	<th>Booking Time</th>
	<th>Customer Name</th>
	<th>Jump No</th>
	<th>Agent</th>
	<th>Rate</th>
	<th>Photos</th>
	<th>ON/OFF</th>
	<th>Notes</th>
</tr>
<?php 
	$fields = array('BookingDate', 'BookingTime', 'RomajiName', 'NoOfJump', 'Agent', 'Rate', 'photos', 'CollectPay', 'Notes');
	$sql = "SELECT IF(BookingTime <> '', 1, 0) as TimePresent, customerregs1.* FROM customerregs1 WHERE site_id = '" . CURRENT_SITE_ID . "' AND (BookingDate >= '{$_SESSION['startDate']}' and BookingDate <= '{$_SESSION['endDate']}') ORDER BY TimePresent DESC, BookingDate ASC, BookingTime ASC;";
	$res = mysql_query($sql) or die(mysql_error());
	while ($row = mysql_fetch_assoc($res)) {
		if ($row['NoOfJump'] == 0 && $row['GroupBooking'] > 1) continue;
		$add_class = '';
		if ($row['DeleteStatus'] == 1) {
			$add_class=' class="deleted"';
		} elseif ($row['Checked'] == 1) {
			$add_class=' class="checked"';
		};
		echo "\t<tr$add_class>\n";
		foreach ($fields as $field) {
			if ($field == 'RomajiName') {
				$row[$field] = '<nobr>' . strtoupper($row[$field]) . '</nobr>';
			};
			echo "\t\t<td class=\"$field\">{$row[$field]}</td>\n";
		};
		echo "\t</tr>\n";
	};
?>
</table>
        </form>
    </body>
</html>
