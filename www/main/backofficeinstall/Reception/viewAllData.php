<?php
  include "../includes/application_top.php";
header('Content-Type: text/html;charset=Shift_JIS');
$cDate = date("Y-m-d");

if (isset($_GET['jumpDate']) && preg_match("/^([\d]{4}-[\d]{2}-[\d]{2})$/", $_GET['jumpDate'])) {
	$cDate = $_GET['jumpDate'];
}


function display_db_table($tablename, $conn, $header_bool, $table_params) {

    $tbVal = 'customerregs1';

    $jumpDate = $_GET['jumpDate'];
    $nv = "NULL";
    mysql_set_charset('sjis', $conn);
    if (!is_null($jumpDate)) {
        $sql_select_all = "SELECT * from $tbVal WHERE 
				site_id = '" . CURRENT_SITE_ID . "'
				AND bookingdate = '" . $jumpDate . "'" .
                " AND romajiname != '" . $nv . "'";

        display_db_query($sql_select_all, $conn, $header_bool, $table_params);
    } elseif (isset($_GET['id'])) {
        $sql_select_all = "SELECT * from $tbVal WHERE site_id = '" . CURRENT_SITE_ID . "' AND customerregid = " . (int)$_GET['id'];

        display_db_query($sql_select_all, $conn, $header_bool, $table_params);
	}
}

function display_db_query($sql_select_all, $conn, $header_bool, $table_params) {
    $result_select = mysql_query($sql_select_all, $conn) or die("display_db_query:" . mysql_error());

    $column_count = mysql_num_fields($result_select) or die("display_db_query:" . mysql_error());


    // Here the table attributes from the $table_params variable are added
    print("<TABLE $table_params width='90%' align='center'>");
    if ($header_bool) {

        print("<TR>");
        for ($column_num = 0; $column_num < $column_count; $column_num++) {
            $field_name = mysql_field_name($result_select, $column_num);
            print("<TH bgcolor='#87CEFA'>$field_name</TH>");
        }
        print("</TR>");
    }
    //Check ROW Count
    $num_rows = mysql_num_rows($result_select);

    if ($num_rows == 0) {
        echo "<center><font color='#FF0000'>No Records Found!!! Select another Date.</font><center>";
    } else {
        //$gx = new GenerateXML();
        
        while ($row = mysql_fetch_row($result_select)) {
            $fd = $row[2];
            print("<TR ALIGN=CENTER VALIGN=TOP>");
            
            for($column_num = 0; $column_num < 2; $column_num++) {
                            print("<TD style=valign='middle' >$row[$column_num]</TD>");
            }
            print("<TD style=valign='middle' >$fd</TD>");
            for ($column_num = 3; $column_num < $column_count; $column_num++) {
                print("<TD style='background-color:#FFFFFF' valign='middle' >$row[$column_num]</TD>");
            }
            print("</TR>");
        }
    }

    print("</TABLE>");
}
?>
<html>
    <head>
<?php include '../includes/header_tags.php'; ?>
        <meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
        <meta http-equiv="X-UA-Compatible" content="IE=8">
        <title>View All Data</title>

<?php include '../includes/head_scripts.php'; ?>
        <script type="text/javascript">
$(document).ready(function () {
	$('.datepicker').datepicker({
		showAnim: 'fade',
		numberOfMonths: 1,
		//dateFormat: 'dd/mm/yy',
		dateFormat: 'yy-mm-dd',
		currentText: 'Today'
	});
	$('.datepicker').change(function () {$(this).parents('form').submit();});
});

        </script>
    </head>
    <body>
        <table align="center" border="0" width="90%">

            <tr>
                <td>
<form>
Select date: <input name="jumpDate" value="<?php echo $cDate; ?>" class="datepicker">
</form>
<?php
$table = "table1";
display_db_table($table, $conn, TRUE, "border='1'");
?>

                </td>
            </tr>
        </table>
    </body>
</html>
