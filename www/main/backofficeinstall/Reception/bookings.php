<?php
 $times_schedule = getBookTimes($jumpDate, $regid);
?>
<script>
	var max_jumps = <?php echo isset($max_jumps)?$max_jumps:6; ?>;
    var times_schedule = <?php echo json_encode($times_schedule['options']); ?>;
    var defaults = <?php echo json_encode($defaults); ?>;
    function update_time_select(id, selected) {
        if (selected == null) {
            selected = $('#' + id).val();
        };
        $('#' + id + ' > option').remove();
        for (i in times_schedule) {
            if (times_schedule.hasOwnProperty(i)) {
                var option = $("<option></option>");
				var oval = times_schedule[i].value;
				var otext = times_schedule[i].text;
                if (times_schedule[i].open == 0) {
					otext = times_schedule[i].value + ' (B)';
                };
                option.prop('selected', (times_schedule[i].value === selected) ? 'selected':'');
                option.attr('avail', times_schedule[i].avail);
                if (times_schedule[i].open == 0 || times_schedule[i].avail == -6) {
                    option.prop('disabled', 'disabled');
                };
				option.val(oval).text(otext).appendTo('#' + id);
            }
        }
		$('#' + id).change(function () {
			var avail = parseInt($(this).find(":selected").attr('avail')) + max_jumps - 6;
			var id = $(this).prop('id');
			update_njump_select(id.replace("splitTime", "splitJump"), avail, null);
		});
    }
    function update_njump_select(id, maxj, selj) {
		if (selj == null) {
			selj = $('#' + id).val();
		};
        $('#' + id + ' > option').remove();
		var selected = false;
        for (var i = 0; i <= maxj; i++) {
            var option = $("<option></option>");
            var oval = i;
			var otext = i;
            if (i == selj) {
                option.prop('selected', 'selected');
				selected = true;
            };
			if (i == maxj && !selected) {
				option.prop('selected', 'selected');
			}; 
			option.val(oval).text(otext).appendTo('#' + id);
        };
    }
    function get_time_info(book_time) {
                var time_info = null;
                // found main time timeframe info
                for (var j in times_schedule) {
                        if (times_schedule.hasOwnProperty(j)) {
                                if (book_time.indexOf(times_schedule[j].value) > -1) {
                                        time_info = times_schedule[j];
                                        break;
                                };
                        };
                };
        return time_info;
    }
    function select_price_rate(id, value) {
        $('#' + id + '').val(value);
    }
    $('document').ready(function () {
        var start_time = "<?php echo $chkInTime; ?>";
        // update times drop down with right auto values
        var total_jumps = <?php echo (int)$grpJumper; ?>;
        var time_info = get_time_info(start_time);
        var max_jumps = parseInt(time_info.avail) + <?php echo isset($max_jumps)?$max_jumps:6; ?> - 6;
        var jumps_selected = 0;
        max_jumps = parseInt(time_info.avail) + <?php echo isset($max_jumps)?$max_jumps:6; ?> - 6;
        if (total_jumps > max_jumps) {
            jumps_selected = max_jumps;
        } else {
            jumps_selected = total_jumps;
        };
        if (typeof defaults[0] != 'string') {
            jumps_selected = defaults[0].jumpsno;
            select_price_rate('rate0', defaults[0].rate);
            if (parseInt(defaults[0].jumpsno) > 0) {
                //select_price_rate('ratetopay', defaults[0].ratetopay / defaults[0].jumpsno);
            };
        };
        total_jumps -= jumps_selected;
        update_njump_select('noOfJump', max_jumps, jumps_selected);

        for (var i = 1; i < 4; i++) {
			if ($("#rate"+i).length == 0) continue;
            if (defaults.hasOwnProperty(i) && typeof defaults[i] != 'string') {
                // edit record, set default time
                start_time = defaults[i].splitTime;
                time_info = get_time_info(start_time);
            } else {
                // find next available start time
                for (var j in times_schedule) {
                    if (times_schedule.hasOwnProperty(j)) {
                        if (times_schedule[j].value > start_time && parseInt(times_schedule[j].avail) > -1 && times_schedule[j].open == 1) {
                            start_time = times_schedule[j].value;
                            time_info = times_schedule[j];
                            break;
                        };
                    };
                };
            };
            jumps_selected = 0;
            max_jumps = parseInt(time_info.avail) + <?php echo isset($max_jumps)?$max_jumps:6; ?> - 6;
            if (total_jumps > max_jumps) {
                jumps_selected = max_jumps;
            } else {
                jumps_selected = total_jumps;
            };
            if (defaults.hasOwnProperty(i) && typeof defaults[i] != 'string') {
                jumps_selected = defaults[i].jumpsno;
                select_price_rate('rate'+i, defaults[i].rate);
            };
            total_jumps -= jumps_selected;
            update_njump_select('splitJump' + i, max_jumps, jumps_selected);
            update_time_select('splitTime' + i, start_time);
        };
    });
</script>
