<?php
require '../includes/application_top.php';
echo Html::head("Bungy Japan - Ticket Panel", array("/Waiver/css/stylesheet.css", "colorbox.css", "reception.css"), array("reception.js", "jquery-1.11.1.js", "jquery.colorbox.js"));

$loadedResults 		= mysql_query('SELECT * FROM settings WHERE site_id = '.CURRENT_SITE_ID.' AND key_name = "printerTicketAndCertificate"');
$settingsRow 		= mysql_fetch_assoc($loadedResults);
$savedPrintersArray  	= explode("\n", $settingsRow['value']);

$certificatePrinter = $ticketPrinter = '';//set them to an empty string if there are not results

$findPrintersResultsCount = mysql_num_rows($loadedResults);

if($findPrintersResultsCount){
	$certificatePrinter 	= trim($savedPrintersArray[0]);
	$ticketPrinter 		= trim($savedPrintersArray[1]);
} 

//TODO get cord bounds

?>

<!-- http://stackoverflow.com/questions/1096862/print-directly-from-browser-without-print-popup-window -->
<?php
//NumberPad Components
/*               
Hidden Button Div
Reveler Button
functionality code

echo Html::numpad(activatorButton, outputDisplay, title, maximumValue)

*/

function createNumpadEntry($title, $triggerId, $numpadContainerId, $outputId, $outputFormNameName, $upperLimit){
	return 
	"<script>
	$(document).ready(function(){

		$(\"#$triggerId\").colorbox({
			inline:true,
			href:\"#$numpadContainerId\",
			opacity: 0.7,
			transition: \"elastic\",
			closeButton: true,
			escKey: true
		});
	});
	</script>

	
	
	<div numpadContainerId=\"numpadContainer hiddenInline\" style=\"display:none\">
		<div id=\"$numpadContainerId\" class=\"numpadContainer hiddenNumpad\" style=\"background-color:white; color:black;\">
			<h1>$title</h1>
			<div class=\"numpadContainer\">
				<input  class=\"numpadDisplay\" id=\"{$numpadContainerId}Display\" name=\"$photoCount\">
				 
				<button class=\"numpadButton\" onclick=\"numpadNumberPressed(1,'#{$numpadContainerId}Display', '#$outputId', $upperLimit)\">1</button>
				<button class=\"numpadButton\" onclick=\"numpadNumberPressed(2,'#{$numpadContainerId}Display', '#$outputId', $upperLimit)\">2</button>
				<button class=\"numpadButton\" onclick=\"numpadNumberPressed(3,'#{$numpadContainerId}Display', '#$outputId', $upperLimit)\">3</button>
				<button class=\"numpadButton\" onclick=\"numpadNumberPressed(4,'#{$numpadContainerId}Display', '#$outputId', $upperLimit)\">4</button>
				<button class=\"numpadButton\" onclick=\"numpadNumberPressed(5,'#{$numpadContainerId}Display', '#$outputId', $upperLimit)\">5</button>
				<button class=\"numpadButton\" onclick=\"numpadNumberPressed(6,'#{$numpadContainerId}Display', '#$outputId', $upperLimit)\">6</button>
				<button class=\"numpadButton\" onclick=\"numpadNumberPressed(7,'#{$numpadContainerId}Display', '#$outputId', $upperLimit)\">7</button>
				<button class=\"numpadButton\" onclick=\"numpadNumberPressed(8,'#{$numpadContainerId}Display', '#$outputId', $upperLimit)\">8</button>
				<button class=\"numpadButton\" onclick=\"numpadNumberPressed(9,'#{$numpadContainerId}Display', '#$outputId', $upperLimit)\">9</button>
				<button class=\"numpadButton numpadButtonWide\" onclick=\"numpadNumberPressed(0,'#$numpadContainerId', '#$outputId')\">0</button>

				<button class=\"delButton\" onclick=\"numpadDeletePressed('#$numpadContainerId', '#{$numpadContainerId}Display', '#$outputId')\">DELETE</button>
				<button class=\"enterButton\" onclick=\"numpadEnterPressed()\">ENTER</button>
			</div>
		</div>
	</div>";

}

?>

<div id="mainDiv">

	<?php echo createNumpadEntry("Photo #", 	"openPhotoNumpad", 	"photoNumpad", 	"photoInput", 	"photoCount", 	100)?>
	<?php echo createNumpadEntry("Enter Weight", 	"openWeightNumpad",  	"weightNumpad", "weightInput", 	"weight", 	110)?>

	<div id="formUpperInputs" class=" clearBoth">
		<h2>Jump Ticket</h2>

		<div class="formTop">
			<div class="headerInput">
				<input type="text" name="headerInput1" size='3' class="inputText">
				<div class="inputSeparator noClear">-</div>
				<input type="text" name="weigntInput2" size='3' class="inputText noClear">
			</div>

			<div class="headerInput">
			On Site
			</div>

			<div class="headerInput">
				<input type="text" name="weightInput1" size='3' class="inputText">
				<div class="inputSeparator noClear">-</div>
				<input type="text" name="weigntInput2" size='3' class="inputText noClear">
			</div>
		</div>


	</div>
	<div id="formInnerLeft" class="newLine whiteBox">
		<div class="whiteInput doubleWidth blackBorder" onclick="updateTime()">
			<div class="inputTitle" id="timeInput" >Time</div>
			<h1 id="timeDisplay">??:??</h1>
		</div>

		<div class="whiteInput newLine blackBorder" id="openPhotoNumpad">
			<div class="inputTitle">Photo</div>
			<div class="inputBox"><input type="text" name="photoInput" size='3' value="0" id="photoInput" class="inputText"></div>
		</div>

		<div class="whiteInput blackBorder" id="jump">
			<div class="inputTitle">Jump</div>
			<div class="inputBox"><input type="text" name="jumpInput" size='3' class="inputText"></div>
		</div>
		<div class="whiteInput doubleWidth newLine blackBorder" id="openWeightNumpad">
			<div class="inputTitle">Weight</div>
			<div class="inputBox">
				<input type="text" id="weightInput" name="weightInput" size='3' class="inputText">
			</div>
		</div>
		<div class="whiteInput doubleWidth newLine blackBorder" id="yrbbk">
			<div class="inputBox">
				<!--TODO display the actual cord color name-->
				<div class="cordColor" id="cordYellow">Yellow</div> &nbsp;  
				<div class="cordColor" id="cordRed">Red</div> &nbsp;  
				<div class="cordColor" id="cordBlue">Blue</div> &nbsp;  
				<div class="cordColor" id="cordBlack">Black</div> &nbsp;  
				<div class="cordColor" id="noCordMatchLow">Warning weight below lower bound</div> &nbsp;  
				<div class="cordColor" id="noCordMatchHigh">Warning weight above upper bound</div> &nbsp;  
				<div class="cordColor" id="fatalSystemError">Warning weight above upper bound</div> &nbsp;  
			</div>
		
		</div>

		<div class="whiteInput doubleWidth newLine blackBorder" id="name">
			<div class="inputTitle">Name</div>
			<div class="inputBox"><input type="text" name="customerName" size='20' class="inputText"></div>
		</div>
		<div class="buttonDiv doubleWidth newLine" id="print">
			<?php echo html::inputPageButton("print", "printTicket('$certificatePrinter', '$ticketPrinter')", null, null, $classes = "redButton button", "walkIn");?>
		</div>

		<!--<div class="buttonDiv doubleWidth newLine" id="print"><input type="submit" class="redButton button" onClick="window.print();" value="Print"></div>-->
	</div>

	<div id="formInnerRight">
		  <?php 
		  $bdnl = "buttonDiv newLine redButton";
		  echo html::inputPageButton("Walk in",        	"gotoUrl('#');", null, null, $classes = "redButton buttonSmallText $bdnl", "walkIn");
		  echo html::inputPageButton("test", 		"gotoUrl('#');", null, null, $classes = "redButton $bdnl", "test");
		  echo html::inputPageButton("foc", 		"gotoUrl('#');", null, null, $classes = "redButton $bdnl", "foc");
		  echo html::inputPageButton("late customer",  	"gotoUrl('#');", null, null, $classes = "redButton buttonSmallSmallText $bdnl", "lateCustomer");
		  echo html::inputPageButton("price", 		"gotoUrl('#');", null, null, $classes = "redButton $bdnl", "price");
		  echo html::inputPageButton("mistake",        	"gotoUrl('#');", null, null, $classes = "redButton buttonSmallText $bdnl", "mistake");
		  echo html::inputPageButton("report", 		"gotoUrl('#');", null, null, $classes = "redButton $bdnl", "report");
		  ?>
	</div>

	<div id="formOuterRight">
		<!--TODO Add Button PHP code again-->
		<input type="submit" class="button redButton button buttonSmallText green newLine" id="goods" value="g o o d s" onclick="gotoUrl('#');" style="; ;">
		<input type="submit" class="button redButton button buttonSmallSmallText newLine" id="nonJump" value="n o n J u m p" onclick="gotoUrl('#');" style="; ;">
		<input type="submit" class="button redButton button newLine" id="questionMark" value="?" onclick="gotoUrl('#');" style="; ;">    
	</div>

	<div class="formBottom newLine">
		  <?php
		  $common = "redButton bottomButton";
		  echo html::inputPageButton("cords", 	 "gotoUrl('cordCalibration.php');", "100px", "100px", $classes = "$common blue", "cords");
		  echo html::inputPageButton("shutdown", "gotoUrl('#');", "100px", "100px", $classes = "$common buttonSmallText", "shutdown");
		  echo html::inputPageButton("enter weight",   "gotoUrl('#');", "100px", "100px", $classes = "$common buttonSmallText", "weight");
		  echo html::inputPageButton("esc",      "gotoUrl('#');", "100px", "100px", $classes = "$common ", "esc");
		  echo html::inputPageButton("settings", "gotoUrl('ticketSettings.php');", "400px", "50px", $classes = "bottomButton purpleButton", "ticketSettings");
		  ?>
	</div>
</div>
	<?php 
		$date 		= urlencode("January 30th 2014");
		$fullName 	= urlencode("Ayodeji Osokoya");
		$signatureUrl 	= urlencode("testsignature.png");
	?>
	<!--http://stackoverflow.com/questions/2064850/how-to-refresh-an-iframe-using-javascript-->
	<iframe class="hiddenCertificateFrame" name="certificate" src="<?php echo"certificate.php?fullName=$fullName&date=$date&signatureUrl=$signatureUrl"?>" style="display:none;"></iframe>
	<iframe name="ticketmini" src="ticketmini.php"></iframe>
<script>
	$(document).ready(function(){
		updateTime();
		setCordColor(60.5);
	});

	function gotoUrl(page){
        	document.location = page;
	}

	function setTimeTo(value){
		document.getElementById('timeDisplay').innerHTML = '<h1>'+ value + '</h1>';
	}

	function getHalfHourRoundedTime(){
		var dateObject = new Date();
        	var hours = dateObject.getHours();
		var thirtyMins = dateObject.getMinutes();

		thirtyMins = (thirtyMins <= 30 ? '00' : '30'); //round time to the nearest half hour
		return hours + ':' + thirtyMins;
	}

	function setCordColor(weight){
		//round it up or down
		var weightRounded = Math.round(weight);
		$('.cordColor').hide();//hide all cord colors

                var cordColor = null;
		<?php

	       	$yellowLowerBound 	= 40;
	       	$yellowUpperBound 	= 60; 
		
	       	$redLowerBound 		= 61;
	       	$redUpperBound 		= 75; 
		
	       	$blueLowerBound 	= 76;
	       	$blueUpperBound 	= 90;  
		
	       	$blackLowerBound 	= 91; 
	       	$blackUpperBound 	= 110;

		?>

		var yellowLowerBound 	= <?php echo $yellowLowerBound?>;
		var yellowUpperBound 	= <?php echo $yellowUpperBound?>;

		var redLowerBound 	= <?php echo $redLowerBound?>;
		var redUpperBound 	= <?php echo $redUpperBound?>;

		var blueLowerBound 	= <?php echo $blueLowerBound?>;
		var blueUpperBound 	= <?php echo $blueUpperBound?>;

		var blackLowerBound 	= <?php echo $blackLowerBound?>;
		var blackUpperBound  	= <?php echo $blackUpperBound?>;


		if(weightRounded < yellowLowerBound){
			$('#noCordMatchLow').show();
		}else if((weightRounded >= yellowLowerBound) && (weightRounded <= yellowUpperBound)){
			$('#cordYellow').show();
			console.log(yellowLowerBound);
			console.log(weightRounded);
			console.log(yellowUpperBound);

		} else if((weightRounded >= redLowerBound) && (weightRounded <= redUpperBound)){
			$('#cordRed').show();
	
		} else if((weightRounded >= blueLowerBound) && (weightRounded <= blueUpperBound)){
			$('#cordBlue').show();

		} else if((weightRounded >= blackLowerBound) && (weightRounded <= blackUpperBound)){
			$('#cordBlack').show();

		} else if (weightRounded > blackUpperBound){
			$('#noCordMatchHigh').show();
			
		} else{
			$('#fatalSystemError').show();
			console.log("WARNING The cord calibration system is not working. Contact support immediately and cease from using the system");
		}
		//hide all cord colors
		//get weight
		//if weight is between a range

	}

	function updateTime(){
		document.getElementById('timeDisplay').innerHTML = '<h1>' + getHalfHourRoundedTime() + '</h1>';
	}

	function getNumpadIdFromButton(numpadElement){
		var senderNumpad = $(numpadElement).parents(".hiddenNumpad");
		return senderNumpad.attr('id');
	}

	function numpadNumberPressed(keyNumber, numpadDisplayId, outputId, upperLimit){
		$(numpadDisplayId).val('' + $(numpadDisplayId).val() + keyNumber);
		//the display on the form
		$(outputId).val('' + $(numpadDisplayId).val());

		if ($(numpadDisplayId).val() > upperLimit){  
		    $(numpadDisplayId).val(upperLimit);
		    $(outputId).val('' + $(numpadDisplayId).val());
		}
	}
	 
	function numpadDeletePressed( numpadId, numpadDisplayId, outputId){

		var val = $(numpadDisplayId).val();
		if (val.length > 0) {
			$(numpadDisplayId).val(val.substr(0, val.length-1));
			$(outputId).val('' + $(numpadDisplayId).val());
		}
	}

	function numpadEnterPressed(){
		$.colorbox.close();
	}



</script>
<?php require_once("../includes/pageParts/standardFooter.php");?>
