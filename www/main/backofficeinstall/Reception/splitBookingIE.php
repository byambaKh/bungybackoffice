<?php
/* vim: set ts=4: */
include_once '../includes/application_top.php';
// check if user trying to edit GroupBooking
if (isset($_POST['chks'])) {
    if ($gbid = isGroupBooking($_POST['chks'])) {
        $baction = '';
        if (isset($_GET['baction'])) {
            $baction = '&baction=' . $_GET['baction'];
        };
        Header("Location: /Reception/groupBooking.php?gbid=" . $gbid . $baction);
        die();
    };
};
// EOF check
$allowChangeNote = 1;
if (isset($_GET['allowChangeNote']))
    $allowChangeNote = $_GET['allowChangeNote'];

$lname = $_POST['lastname'];
$fname = $_POST['firstname'];
$regid = $_POST['cregid'];
if (isset($_GET['id'])) {
    $regid = $_GET['id'];
};
if (empty($regid)) {
    $regid = $_POST['chks'];
};


$bDates = getCalendarState();

$sql_select = "SELECT bookingdate,bookingtime,noofjump,customerlastname,customerfirstname, customeraddress,postalcode,prefecture,
      contactno,customeremail,rate, agent, collectpay, ratetopay, transportmode, notes, customerregid, ratetopayqty, foc
      FROM customerregs1
      WHERE customerregid = '" . $regid . "'";


//echo $sql_select;
$result = mysql_query($sql_select, $conn) or die(mysql_errno() . ":<b> " . mysql_error() . "</b>");

$row = mysql_fetch_assoc($result);
//print_r($row);
$btime = $row['bookingtime'];
$jmpno = $row['noofjump'];
//echo "*************************".$jmpno;
$j = $jmpno; //6 - $jmpno;
//echo "+++++++++++".$j;
$_SESSION['jmplen'] = $j;

$lastname = $row['customerlastname'];
$firstname = $row['customerfirstname'];
$cname = $row['customerlastname'] . ' ' . $row['customerfirstname'];

$address = $row['customeraddress'];
$postalcode = $row['postalcode'];
$pref = $row['prefecture'];
$contactno = $row['contactno'];
$email = $row['customeremail'];
$rate = $row['rate'];
$agent = $row['agent'];
$collect = $row['collectpay'];
$pay = $row['ratetopay'];
$ratetopay = $row['ratetopay'];
$transportmode = $row['transportmode'];
$notes = $row['notes'];
$nos = $row['ratetopayqty'];
$foc = $row['foc'];

//echo "-------------<<<<<<<<<"."===>".$pay."---".$row[15];
$_SESSION['notes'] = $row['notes'];
$_POST['notes'] = $_SESSION['notes'];

$bdate = $row['bookingdate'];
$_SESSION['bookingdate'] = $bdate;


$_SESSION['cname'] = $cname;
$_SESSION['chkInTime'] = $_POST['chkInTime'];

$places = BJHelper::getPlaces();
$staff_names = BJHelper::getStaffList('StaffListName', true);

$findRatesForDomains = "SELECT `value`, subdomain, site_id FROM configuration c, sites p WHERE p.id = c.site_id AND `key` = 'first_jump_rate';";
$ratesForDomainsResults = mysql_query($findRatesForDomains);
//get all of the domains and rates in an array
while (($domainsRates[] = mysql_fetch_assoc($ratesForDomainsResults)) || array_pop($domainRates)) ;
?>
<html>
<head>
    <?php include 'includes/header_tags.php'; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
    <meta http-equiv="X-UA-Compatible" content="IE=8">

    <title>Change Booking</title>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/i18n/jquery.ui.datepicker-ja.js" type="text/javascript" charset="UTF-8"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" type="text/css" media="all"/>
    <link rel="stylesheet" href="../css/all.css" type="text/css" media="all"/>

    <?php require_once("../js/reception.js.php"); ?>

    <script type="text/javascript">
        function searchArrayForObjectWithPropertyAndValue(searchedArray, property, value) {
            for (var i = 0; i < searchedArray.length; i++) {
                if (searchedArray[i].hasOwnProperty(property)) {
                    if (searchedArray[i][property] == value)
                        return searchedArray[i];
                }
            }
            return null;
        }

        function changeRateToSubdomainRate(subdomain) {
            var domainsRates = <?php echo json_encode($domainsRates);?>;
            var subdomainInformation = searchArrayForObjectWithPropertyAndValue(domainsRates, "subdomain", subdomain);
            document.getElementById("rate").value = subdomainInformation.value;
        }

        function update_foc() {
            for (var i = 0; i < 4; i++) {
                var disabled = 'disabled';
                if ($('#rate' + i).val() == '0') {
                    disabled = false;
                } else {
                    if ($('#rate' + i).length > 0) {
                        $('#foc' + i).val('');
                    }
                    ;
                }
                ;
                $('#foc' + i).attr('disabled', disabled);
            }
            if ($('#rate').val() == '0') {
                $('#foc0').attr('disabled', false);
            } else {
                $('#foc0').attr('disabled', 'disabled');
            }
            ;
        }

        $(function () {
            update_foc();
            $('#rate').on('change', update_foc);
            $('#jumpDate').datepicker({
                showAnim: 'fade',
                numberOfMonths: 1,
                //dateFormat: 'dd/mm/yy',
                dateFormat: 'yy-mm-dd (D)',
                currentText: 'Today',
                minDate: '0d',
                //minDate: new Date(2011, 12 , 31),
                maxDate: new Date(<?php echo $maxDate?>),
                beforeShowDay: checkAvailability,
                onSelect: getBookTimes
            });
            //$('#jumpDate').datepicker(jQuery.datepicker.regional["ja"]);
            $('input[name=group1]').on('change', triggerAgt);
            $("input[name=place]").change(on_place_change);
            <?php
            $current_subdomain = strtolower(SYSTEM_SUBDOMAIN);
            $double_subdomains = array('minakami', 'sarugakyo');
            $new_subdomain = '';
            if (in_array($current_subdomain, $double_subdomains)) {
                $ns = array_diff($double_subdomains, array($current_subdomain));
                $new_subdomain = array_pop($ns);
            };
            ?>
            $("form[name=bookingForm]").on('submit', function () {
                for (var i = 0; i < 4; i++) {
                    if ($("#rate" + i).val() == 0 && $("#foc" + i).val() == '') {
                        alert('Please select FOC!');
                        document.regform['foc' + i].focus();
                        return false;
                    }
                    ;
                }
                $("input[type=submit]").prop('disabled', 'disabled').prop('value', 'Submitting');
                if ($("#double-booking").prop('checked')) {
                    var sar = window.open('about:blank', '<?php echo $new_subdomain; ?>');
                    document.bookingForm.target = "<?php echo $new_subdomain; ?>";
                    document.bookingForm.action = document.location.href.replace(/<?php echo $current_subdomain; ?>/, '<?php echo $new_subdomain; ?>').replace(/splitBookingIE\.php/, 'registration_by_staffIE.php');
                        document.bookingForm.submit();
                    document.bookingForm.action = document.location.href.replace(/splitBookingIE\.php/, 'reg_split_booking1.php');
                    document.bookingForm.target = "_self";
                }
                ;
                document.bookingForm.submit();
                return true;
            });
            <?php
            $d = explode('-', $bdate);
            $d[1]--;
            ?>
            $('#jumpDate').datepicker("setDate", new Date(<?php echo implode(', ', $d); ?>));
            getBookTimes($('#jumpDate').val(), null);
        });
        function on_place_change() {
            getBadDates($("input[name=place]:checked").val());
        }
        function getBadDates(place) {
            $.ajax({
                url: '../includes/ajax_helper.php',
                dataType: 'json',
                data: 'action=getBadDates&place=' + place,
                success: function (data) {
                    $myBadDates = data['options'];
                    getBookTimes($("#jumpDate").val());
                }
            });
        }

        function getBookTimes(bDate, inst) {
            bDate = bDate.replace(/([^-^0-9])/g, "");
            var place = $("input[name=place]:checked").val();
            var place_query = '';
            if (typeof place != 'undefined') {
                place_query = '&place=' + place;
            }
            ;
            $.ajax({
                url: '../includes/ajax_helper.php',
                dataType: 'json',
                data: 'action=getBookTimes&bDate=' + encodeURIComponent(bDate) + '&regid=<?php echo $regid; ?>' + place_query,
                success: function (data) {
                    if (data['result'] == 'success') {
                        var selected = $('#chkInTime').val();
                        $('#chkInTime > option').remove();
                        for (i in data['options']) {
                            if (data['options'].hasOwnProperty(i)) {
                                var option = $("<option></option>");
                                var oval = data['options'][i].value;
                                if (oval == "") {
                                    option.val(oval).text(data['options'][i].text).appendTo('#chkInTime');
                                    continue;
                                }
                                var avail = parseInt(data['options'][i].avail);
                                var otext = data['options'][i].value + ' (' + avail + ')';
                                if (data['options'][i].open == 0) {
                                    otext = data['options'][i].value + ' (B)';
                                }
                                ;
                                option.prop('selected', (data['options'][i].value === selected) ? 'selected' : '');
                                if (data['options'][i].open == 0 || avail < -5) {
                                    option.prop('disabled', 'disabled');
                                }
                                ;
                                option.attr('avail', avail);
                                option.val(oval).text(otext).appendTo('#chkInTime');
                            }
                        }
                        $('#chkInTime').change(function () {
                            var avail = $('#chkInTime > option:selected').attr('avail');
                            var size = parseInt(avail) + 6;
                            var selected = $('#noOfJump').val();
                            $('#noOfJump > option').remove();
                            var selected_found = false;
                            for (var i = 1; i <= size; i++) {
                                var option = $("<option></option>");
                                option.val(i).text(i).appendTo('#noOfJump');
                                if (selected == i) selected_found = true;
                            }
                            ;
                            if (!selected_found) {
                                var option = $("<option></option>");
                                option.val(selected).text(selected).appendTo('#noOfJump');
                            }
                            ;
                            $('#noOfJump').val(selected);
                        });
                        $('#chkInTime').change();
                    }
                }
            });
        }
        var $myBadDates = new Array(<?php
            foreach ($bDates as $disabledate) {
                echo " \"$disabledate\",";
            };
            echo " 1";
            ?>);
        //                var $cntBookCal = new Array(<?php $bookDates[0]; ?>);
        function checkAvailability(mydate) {
            var $return = true;
            var $returnclass = "available";
            //$checkdate = $.datepicker.formatDate('dd/mm/yy', mydate);
            $checkdate = $.datepicker.formatDate('yy-mm-dd', mydate);
            for (var i = 0; i < $myBadDates.length; i++) {
                if ($myBadDates[i] == $checkdate) {
                    $return = false;
                    $returnclass = "unavailable";
                    return [$return, $returnclass, "Dates Locked"];
                }
            }
            //                    if($cntBookCal > 0){
            //                        $return = true;
            //                        $returnclass= "booked";
            //                        return [$return,$returnclass,"Booking..."];
            //                    }
            return [$return, $returnclass];
        }

        function procBack() {
            document.location = "dailyViewIE.php";
        }
        function onlyNumbers(evt) {
            var e = event || evt; // for trans-browser compatibility
            var charCode = e.which || e.keyCode;

            if (charCode > 31 && (charCode < 48 || charCode > 57)) return false;

            return true;
        }


        function triggerAgt() {
            // MisterSoft rates changes
            var price = 0;
            var onsite = $("input[name=group1]:checked").val();
            var agent_rate = null;
            $('#agentName > option').each(function () {
                if ($(this).prop('selected') === true) {
                    price = $(this).attr('price');
                    if (onsite == "Offsite") {
                        //price = '0';
                    }
                    ;
                    agent_rate = "0";
                    if ('<?php echo SYSTEM_SUBDOMAIN; ?>' == 'MINAKAMI' && $(this).html() == 'SG Bungy') {
                        if (onsite == 'Onsite') {
                            price = '8000';
                            agent_rate = '7000';
                        } else {
                            price = '<?php echo $config['second_jump_rate']; ?>';
                        }
                        ;
                    }
                    ;
                    if ('<?php echo SYSTEM_SUBDOMAIN; ?>' == 'SARUGAKYO' && $(this).html() == 'MK Bungy') {
                        if (onsite == 'Onsite') {
                            price = '10000';
                            agent_rate = '5000';
                        } else {
                            price = '<?php echo $config['second_jump_rate']; ?>';
                        }
                        ;
                    }
                    ;
                }
                ;
            });
            $('#rate > option').each(function () {
                if ($(this).html() == price) {
                    $(this).prop('selected', true);
                }
                ;
            });
            if (agent_rate != null) {
                $('#agtratetopay').val(agent_rate);
                if (agent_rate == "") {
                    $('#nos').val(0);
                } else {
                    $('#nos').val($('#noOfJump').val());
                }
            }
            // EOF MisterSoft
            var agentName = $('#agentName').val();
            if (agentName != 'NULL' && agentName != '') {
                $('#bookingtype').val('Agent');
            } else {
                $('#bookingtype').val('Phone');
            }
            ;
        }

    </script>
    <style>
        .dates {
            width: 100px;
        }
    </style>
</head>

<body> <!-- onload="javascript:limitDropDown();"-->
<form action="reg_split_booking1.php?allowChangeNote=<?= $allowChangeNote ?>" method="post"
      name="bookingForm">
    <input type="hidden" name="email" value="<?=$email?>">
    <input type="hidden" name="bookingdate" value="<?php print $bdate; ?>">
    <input type="hidden" name="olddate" value="<?php print $bdate; ?>">
    <input type="hidden" name="oldtime" value="<?php print is_null($_POST[chkInTime]) ? $btime : $_POST['chkInTime']; ?>">
    <input type="hidden" name="cregid" value="<?php print $regid; ?>">
    <input type="hidden" name="baction" value="<?php print (isset($_GET['baction'])) ? $_GET['baction'] : ''; ?>">


    <table align="center" border="0" width="450px">
        <tr>
            <td colspan="4" bgcolor="#000000" align="center" valign="middle">
                <input type="button" name="togroupconvert" id="togroupconvert" value="Change to Variable" style="background-color:#FFFF66; float:left;" onclick="convertBookingToGroupBooking('groupBooking.php');">
            </td>

        </tr>
        <tr>
            <td colspan="4" bgcolor="#000000" align="center" valign="middle">
                <h2><font color="#FFFFFF"><?php echo ucfirst(SYSTEM_DATABASE_INFO); ?> BOOKING Details</font></h2>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" colspan="4">
                <font color="#FFFFFF"><b>Current User is:&nbsp;<?php print $_SESSION['myusername']; ?></b></font>
            </td>
        </tr>
        <?php if (!isset($_GET['baction']) || $_GET['baction'] != 'cancel') { ?>
            <tr>
                <td bgcolor="#FF0000" colspan="4"><font color="#FFFFFF"><b>Place:
                            <?php
                            if (!empty($places)) {
                                while (list(, $place) = each($places)) {
                                    if ($place['status'] == 1 && $place['subdomain'] != SYSTEM_DATABASE_INFO) {
                                        echo "<input type=\"radio\" name=\"place\" onclick=\"changeRateToSubdomainRate('{$place['subdomain']}');\" value=\"{$place['subdomain']}\">" . $place['subdomain'];

                                    };
                                };
                            }
                            ?>
                        </b></font></td>
            </tr>
        <?php }; ?>
        <tr>
            <td bgcolor="#FF0000" colspan="2"><font color="#FFFFFF">Jump Date:&nbsp;
                    <input type="text" class="dates" name="jumpDate" id="jumpDate" size="16" readonly="readonly" value="<?php echo $bdate; ?>">
                </font></td>
            <td bgcolor="#FF0000" colspan="2"><font color="#FFFFFF">Jump Time:</font>
                <select class="split-time" name="chkInTime" id="chkInTime">
                    <?php
                    if (is_null($_POST[chkInTime])) {
                        echo "<option value='$btime' selected='selected'>$btime</option>";
                    } else {
                        echo "<option value='$_POST[chkInTime]' selected='selected'>$_POST[chkInTime]</option>";
                    }
                    ?>
                </select></td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" colspan="2" align="right"><font color="#FFFFFF">Agent:&nbsp;&nbsp;</font>
            </td>
            <td bgcolor="#FF0000" colspan="2"><?php draw_agent_drop_down('agentName', $agent, 'onChange="triggerAgt();"'); ?></td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" colspan="2" align="right"><font color="#FFFFFF">Price:&nbsp;&nbsp;</font>
            </td>
            <td bgcolor="#FF0000" colspan="2"><?php
                if (is_null($rate)) {
                    $rate = '';
                };
                echo draw_pull_down_menu('rate', BJHelper::getRatesList(), $rate, "id='rate'");
                echo "<font color=\"#FFFFFF\">FOC*</font>";
                echo draw_pull_down_menu("foc0", getFOCTitles(), $foc, "id=\"foc0\"");

                ?></td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" colspan="2" align="right"><font color="#FFFFFF">To Pay to Agent:&nbsp;&nbsp;</font>

            </td>
            <td bgcolor="##FF0000" colspan="2"><font color="#FFFFFF">
                    <?php
                    echo draw_pull_down_menu('agtratetopay', BJHelper::getRatesList(true), $ratetopay, "id='agtratetopay'");
                    echo "&nbsp;X&nbsp;";
                    echo "<select name='nos' id='nos'>";
                    for ($i = 1; $i < 13; $i++) {
                        echo "<option value='$i'" . (($i == $nos) ? ' selected="selected"' : '') . ">$i</option>";
                    };
                    echo "</select>";
                    ?>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" colspan="2" align="right"><font color="#FFFFFF">Payment
                    to be made:&nbsp;&nbsp;</font></td>
            <td bgcolor="#FF0000" colspan="2"><font size='2'> <!--<input type="radio" name="group1" value="Onsite">-->
                    <?php
                    if ($collect == 'Onsite') {
                        echo "<font color='#FFFFFF'>Onsite</font><input type='radio' name='group1' value='Onsite' checked='checked'>";
                    } else {
                        echo "<font color='#FFFFFF'>Onsite</font><input type='radio' name='group1' value='Onsite'>";
                    }
                    if ($collect == 'Offsite') {
                        echo "<font color='#FFFFFF'>Offsite</font><input type='radio' name='group1' value='Offsite' checked='checked'>";
                    } else {
                        echo "<font color='#FFFFFF'>Offsite</font><input type='radio' name='group1' value='Offsite'>";
                    }
                    ?> </font></td>
        </tr>
        <tr>
            <td bgcolor="#000000" align="center" colspan="4">
                <font color="#FFFFFF">Note:&nbsp;Previously at <?php
                    if (is_null($_POST[chkInTime])) {
                        print $btime;
                    } else {
                        print $_POST[chkInTime];
                    }
                    ?>
                    <?php print $_SESSION['jmplen'] ?>
                    No Of Jumps Were Booked!!!
                </font>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" align="right" colspan="2"><font color="#FFFFFF">Select No of Jumps:&nbsp;&nbsp;</font>
            </td>
            <td bgcolor="#FF0000" colspan="2">

                <div>
                    <select name="noOfJump" id="noOfJump">
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <?php
                        if (!isset($_POST[chkInTime])) {
                            echo "<option value='$_SESSION[jmplen]' selected='selected'>$_SESSION[jmplen]</option>";
//                                    echo '<option value="1">1</option>';
//                                    echo '<option value="2">2</option>';
//                                    echo '<option value="3">3</option>';
//                                    echo '<option value="4">4</option>';
//                                    echo '<option value="5">5</option>';
//                                    echo '<option value="6">6</option>';
                        }
                        //                                else{
                        //                                    echo "<option value='$_SESSION[jmplen]' selected='selected'>$_SESSION[jmplen]</option>";
                        //                                    echo '<option value="1">1</option>';
                        //                                    echo '<option value="2">2</option>';
                        //                                    echo '<option value="3">3</option>';
                        //                                    echo '<option value="4">4</option>';
                        //                                    echo '<option value="5">5</option>';
                        //                                    echo '<option value="6">6</option>';
                        //                                }
                        ?>

                    </select>
                </div>

                <?php
                //if (!isset($_POST[chkInTime])) {
                //echo "<option value='$_SESSION[jmplen]' selected='selected'>$_SESSION[jmplen]</option>";
                //}
                ?>
                <!--                            <option value="1">1</option>
                                            <option value="2">2</option>
                                            <option value="3">3</option>
                                            <option value="4">4</option>
                                            <option value="5">5</option>
                                            <option value="6">6</option>
                                        </select>-->


                <!--<option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
                -->
            </td>
        </tr>

        <!-- New Code START -->
        <tr>
            <td bgcolor="#FF0000" colspan="2" align="right"><font color="#FFFFFF">Last
                    Name:&nbsp;&nbsp;</font></td>
            <td bgcolor="#FF0000" colspan="2"><font size='2'><input type="text"
                                                                    name="lastname" value='<?php echo $lastname; ?>' width="8" style="text-transform: uppercase"></font>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" colspan="2" align="right"><font color="#FFFFFF">First
                    Name:&nbsp;&nbsp;</font></td>
            <td bgcolor="#FF0000" colspan="2"><font size='2'><input type="text"
                                                                    name="firstname" value='<?php echo $firstname; ?>' width="8" style="text-transform: uppercase"></font>
            </td>
        </tr>
        <!--<tr>
                        <td bgcolor="#FF0000" colspan="2" align="right"><font color="#FFFFFF">Address:&nbsp;&nbsp;</font>
                        </td>
                        <td bgcolor="#FF0000" colspan="2"><font size='2'> <input type="text"
                                name="address" value='<?php echo $address; ?>' width="20"> </font></td>
                </tr>
                <tr>
                        <td bgcolor="#FF0000" colspan="2" align="right"><font color="#FFFFFF">Postal
                        Code:&nbsp;&nbsp;</font></td>
                        <td bgcolor="#FF0000" colspan="2"><font size='2'><input type="text"
                                name="postalcode" value='<?php echo $postalcode; ?>' width="8"></font>
                        </td>
                </tr>
                -->
        <tr>
            <td bgcolor="#FF0000" colspan="2" align="right"><font color="#FFFFFF">Contact
                    No:&nbsp;&nbsp;</font></td>
            <td bgcolor="#FF0000" colspan="2"><font><input type="text"
                                                           name="contactno" value='<?php echo $contactno; ?>' width="8"></font>
            </td>
        </tr>
        <!--<tr>
                        <td bgcolor="#FF0000" colspan="2" align="right"><font color="#FFFFFF">Email:&nbsp;&nbsp;</font>
                        </td>
                        <td bgcolor="#FF0000" colspan="2"><font><input type="text"
                                name="email" value='<?php echo $email; ?>' width="8"></font></td>
                </tr>
        
                -->
        <tr>
            <td bgcolor="#FF0000" colspan="2" align="right"><font color="#FFFFFF">Transport Mode:&nbsp;&nbsp;</font>
            </td>
            <td bgcolor="#FF0000" colspan="2"><font><input type="text"
                                                           name="transportmode" value='<?php echo $transportmode; ?>' width="8"></font>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" colspan="2" align="right"><font color="#FFFFFF">Notes:&nbsp;&nbsp;</font>
            </td>
            <td bgcolor="#FF0000" colspan="2">
                <textarea rows="3" cols="24" name="notes"><?php print $notes; ?></textarea>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" align="right" colspan="2">
                <font color="#FFFFFF"><?php echo (isset($_GET['baction']) && $_GET['baction'] == 'cancel') ? 'Cancelled' : 'Changed'; ?> by:</font>
            </td>
            <td bgcolor="#FF0000" colspan="2">
                <?php echo draw_pull_down_menu('booked_by', $staff_names, '', 'style="width: 180px;" required'); ?>
            </td>
        </tr>
        <tr>
            <td colspan="4" bgcolor="#000000" align="center">
                <div style="float: left;">
                    <input type="button" style="background-color: #FFFF66" name="back" id="back" value="Back" onclick="procBack();">
                </div>
                <div style="float: right">
                    <input type="submit" style="background-color: #FFFF66" value="<?php echo (isset($_GET['baction']) && $_GET['baction'] == 'cancel') ? 'Cancel' : 'Update'; ?>">
                </div>
                <?php
                if (!empty($new_subdomain) && (!isset($_GET['baction']) || $_GET['baction'] != 'cancel')) {
                    ?>
                    <input name="both" type="checkbox" id="double-booking" style="float: right;" value="<?php echo SYSTEM_SUBDOMAIN; ?>">
                    <label for="double-booking" style="color: white; float: right;">Both site registration</label>
                    <?php
                };
                ?>

            </td>
        </tr>
        <!-- New Code END -->


    </table>


</form>
</body>
</html>
<?php
include("ticker.php");
?>
