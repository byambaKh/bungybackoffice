<?php
include ("../includes/application_top.php");
  require $_SERVER['DOCUMENT_ROOT'].'/Reception/GenerateXML.php';

$tgt = $_GET['target'];

$bookingdate = $_POST['bookingdate'];
$BookingTime = $_POST['chkInTime'];


$_SESSION['chkInTime'] =  $_POST['chkInTime'];
$_SESSION['jumpDate'] = $bookingdate;
$jmpNo = $_POST['noOfJump'];
$nos = $_POST['nos'];

$nv = "NULL";


$lastname = $_POST['lastname'];
$firstname = $_POST['firstname'];
$lname = $_POST['lastname'];
$fname = $_POST['firstname'];
$romaji = $lastname.' '.$firstname;
$CustomerAddress = "NULL";
$PostalCode = "NULL";
$Prefecture = "NULL";
$ContactNo = $_POST['contactno'];
$CustomerEmail = "NULL";
$OtherNo = "NULL";
$TransportMode = $_POST['modeTransport'];
$deleteStatus = (isset($_POST['baction']) && $_POST['baction'] == 'cancel') ? 1 : 0;
$dayStatus = 0;
$timeStatus = 0;
$bookingType = "Internet";
if($_POST['rate'] == ''){
  $rate = 7500;
}else {
  $rate = $_POST['rate'];
}
$checked = 0;
$bookingrecv = date("Y-m-d h:i:a");

//$notes = "NULL";
$notes = $_POST['notes'];
$photos = 0;
$mer = "NULL";
$agent = $_POST['agentName'];
$rateToPay = $_POST['ratetopay'];
$collectPay = $_POST['CollectPay'];
$myusername = $_SESSION['myusername'];
$cancelFee = 0;
$groupBooking = 1;

if($BookingTime != ''){
  $sqlst = 'SELECT sum(noofjump) FROM customerregs1 where bookingdate = "' . $bookingdate . '"
  and bookingtime = "' . $BookingTime . '"';
  $res = mysql_query($sqlst,$conn) or die(mysql_errno().":<b> ".mysql_error()."</b>");
  $ret =  mysql_fetch_array($res);
	$data = array(
		"site_id"			=> CURRENT_SITE_ID,
		"BookingDate"		=> $bookingdate,
		"BookingTime"		=> $BookingTime,
		"NoOfJump"			=> $deleteStatus ? 0 : $jmpNo,
		"RomajiName"		=> strtoupper($romaji),
		"CustomerLastName"	=> strtoupper($lastname),
		"CustomerFirstName"	=> strtoupper($firstname),
		"CustomerAddress"	=> $CustomerAddress,
		"PostalCode"		=> $PostalCode,
		"Prefecture"		=> $Prefecture,
		"ContactNo"		=> $ContactNo,
		"CustomerEmail"		=> $CustomerEmail,
		"OtherNo"		=> $OtherNo,
		"TransportMode"		=> $TransportMode,
		"DeleteStatus"		=> $deleteStatus,
		"DayStatus"		=> $dayStatus,
		"TimeStatus"		=> $timeStatus,
		"BookingType"		=> $bookingType,
		"Rate"				=> $_POST['rate'],
		"CollectPay"		=> $collectPay,
		"RateToPay"		=> $rateToPay,
		"RateToPayQTY"		=> $nos,
		"Checked"		=> $checked,
		"SplitName1"		=> strtoupper($romaji),
		"SplitName2"		=> strtoupper($romaji),
		"SplitName3"		=> strtoupper($romaji),
		"SplitTime1"		=> $_POST['splitTime1'],
		"SplitTime2"		=> $_POST['splitTime2'],
		"SplitTime3"		=> $_POST['splitTime3'],
		"SplitJump1"		=> $_POST['splitJump1'],
		"SplitJump2"		=> $_POST['splitJump2'],
		"SplitJump3"		=> $_POST['splitJump3'],
		"Notes"			=> $notes . ((!empty($_POST['booked_by']))? " [ {$_POST['booked_by']} ".date("Y/m/d")." ] " : ""),
		"Photos"		=> $photos,
		"Merchandise"		=> $mer,
		"Agent"			=> $agent,
		"UserName"		=> $myusername,
		"CancelFee"		=> $cancelFee,
		"GroupBooking"		=> $groupBooking
	);
	if ($data['Rate'] == 0) {
		$data['foc'] = $_POST['foc'];
	}

	if (isset($_POST['gbid']) && isset($_POST['gbid'][0])) {
		// update_record
		db_perform('customerregs1', $data, 'update', 'CustomerRegID=' . $_POST['gbid'][0]);
		$groupBooking = $_POST['gbid'][0];
	} else {
		// insert_record
		$data["BookingReceived"] = $bookingrecv;
		db_perform('customerregs1', $data);
    	$groupBooking = mysql_insert_id();	
		send_backup_email($data, true);
	}
}

for ($i = 1; $i <= 3; $i++) {
	if ($_POST['splitJump' . $i] > 0) {
    			$rPay = 0;
			$data = array(
				"site_id"			=> CURRENT_SITE_ID,
				"BookingDate"		=> $bookingdate,
				"BookingTime"		=> $_POST['splitTime' . $i],
				"NoOfJump"			=> $deleteStatus ? 0 : $_POST['splitJump' . $i],
				"RomajiName"		=> strtoupper($romaji),
				"CustomerLastName"	=> strtoupper($lastname),
				"CustomerFirstName"	=> strtoupper($firstname),
				"CustomerAddress"	=> $CustomerAddress,
				"PostalCode"		=> $PostalCode,
				"Prefecture"		=> $Prefecture,
				"ContactNo"		=> $ContactNo,
				"CustomerEmail"		=> $CustomerEmail,
				"OtherNo"		=> $OtherNo,
				"TransportMode"		=> $TransportMode,
				"DeleteStatus"		=> $deleteStatus,
				"DayStatus"		=> $dayStatus,
				"TimeStatus"		=> $timeStatus,
				"BookingType"		=> $bookingType,
				"Rate"			=> $_POST['rate' . $i],
				"CollectPay"		=> $_POST['CollectPay' . $i],
				"Checked"		=> $checked,
				"SplitName1"		=> strtoupper($romaji),
				"SplitName2"		=> strtoupper($romaji),
				"SplitName3"		=> strtoupper($romaji),
				"SplitTime1"		=> $_POST['splitTime1'],
				"SplitTime2"		=> $_POST['splitTime2'],
				"SplitTime3"		=> $_POST['splitTime3'],
				"SplitJump1"		=> $_POST['splitJump1'],
				"SplitJump2"		=> $_POST['splitJump2'],
				"SplitJump3"		=> $_POST['splitJump3'],
				"Notes"			=> $notes . ((!empty($_POST['booked_by']))? " [ {$_POST['booked_by']} ".date("Y/m/d")." ] " : ""),
				"Photos"		=> $photos,
				"Merchandise"		=> $mer,
				"Agent"			=> $agent,
				"UserName"		=> $myusername,
				"CancelFee"		=> $cancelFee,
				"GroupBooking"		=> $groupBooking
			);
			if ($data['Rate'] == 0) {
				$data['foc'] = $_POST['foc' . $i];
			}
			if (isset($_POST['gbid']) && isset($_POST['gbid'][$i])) {
				// update_record
				db_perform('customerregs1', $data, 'update', 'CustomerRegID=' . $_POST['gbid'][$i]);
			} else {
				// insert_record
				$data["BookingReceived"] = $bookingrecv;
				db_perform('customerregs1', $data);
			}
	} else {
		if (isset($_POST['gbid']) && isset($_POST['gbid'][$i])) {
            //
			// update_record
            // This deletes the record...Why?
			mysql_query('DELETE FROM customerregs1 WHERE CustomerRegID=' . $_POST['gbid'][$i]);
		}
	}; 
}

Header("Location: dailyViewIE.php?dispdate=" . rawurlencode($bookingdate));
