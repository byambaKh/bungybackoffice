<?php
include "../includes/application_top.php";

//Check if the user is logged in before giving out data
$user = new BJUser();
if (!$user->isUser()) die();

$action = $_GET['action'];
$type = $_GET['type'];
if (empty($type)) {
    $type = 'non_jumpers';
};
switch (TRUE) {
    case ($action == 'getWaivers') :
        $result = array('result' => 'success');
        $result['options'] = BJHelper::getWaiversDropDown($_GET['date']);
        break;
    case ($action == 'delete_row') :
        $sql = "delete from $type WHERE id = {$_POST['id']};";
        mysql_query($sql);
        $result = array('result' => 'success');
        break;
    case ($action == 'get_one_time_pin') :
        oneTimePin::setNewOneTimePinForSite(CURRENT_SITE_ID);//set a new one time pin every time the button is pressed
        $pin = oneTimePin::getOneTimePin(CURRENT_SITE_ID);
        $result = array('result' => 'success', 'pin' => $pin);
        break;
};
$result['request'] = array(
    'action' => $action,
    'type' => $type,
    'post' => $_POST
);
echo json_encode($result);
