<?php

include '../includes/application_top.php';

include("checkin_config.php");

$booking = getBookingInfo((int)$_GET['regid']);

// get old_post fromsession
//pr($_SESSION);
//if this page is refereshed, $_SESSION['post... will be nil
//and will endup deleting the information in $booking
//and thus the information in the database
if (!isset($_POST['splitTime'])) {
    $_POST = $_SESSION['post_' . (int)$_GET['regid']];
};
//pr($_POST);
for ($i = 0; $i <= 3; $i++) {
    // if it is plain booking, bypass all other records
    if (count($_POST['splitTime']) == 1 && $i > 0) continue;

    if (!array_key_exists($i, $booking)) {
        $booking[$i] = $booking[$i - 1]; // set all db values for this booking
        unset($booking[$i]['CustomerRegID']); // unset reg id - it is not exists for now
        unset($booking[$i]['GroupBooking']); // uset group booking
    };
    if ($_POST['splitJump'][$i] == 0) {
        $booking[$i]['DeleteStatus'] = 1;
    } else {
        $booking[$i]['DeleteStatus'] = 0;
    };
    // update booking values
    $booking[$i]['Agent'] = $_POST['agentn'];
    // update times
    $booking[$i]['BookingTime'] = $_POST['splitTime'][$i];
    if (count($booking) > 1) {
        $booking[$i]['SplitTime1'] = $_POST['splitTime'][1];
        $booking[$i]['SplitTime2'] = $_POST['splitTime'][2];
        $booking[$i]['SplitTime3'] = $_POST['splitTime'][3];
    };
    // update rates
    $booking[$i]['Rate'] = $_POST['rate'][$i];
    // update jumps count
    $booking[$i]['NoOfJump'] = $_POST['splitJump'][$i];
    if (count($booking) > 1) {
        $booking[$i]['SplitJump1'] = $_POST['splitJump'][1];
        $booking[$i]['SplitJump2'] = $_POST['splitJump'][2];
        $booking[$i]['SplitJump3'] = $_POST['splitJump'][3];
    };
    // do some updates for master record only
    if ($i == 0) {
        // update ratetopay to agent
        $booking[$i]['RateToPay'] = $_POST['ratetopay'];
        $booking[$i]['RateToPayQTY'] = $_POST['nos'];
        // additional items
        foreach ($add_items as $code => $value) {
            $booking[$i][$code] = $_POST['add_values'][$code] * $value['price'];
            $booking[$i][$code . '_qty'] = $_POST['add_values'][$code];
        };
    } else {
        // update ratetopay to agent
        $booking[$i]['RateToPay'] = 0;
        $booking[$i]['RateToPayQTY'] = 0;
        // additional items
        foreach ($add_items as $code => $value) {
            $booking[$i][$code] = 0;
            $booking[$i][$code . '_qty'] = 0;
        };
    };
    // collect pay
    if ($i == 0) {
        if ($_POST['CancelFee'] > 0 && $_POST['CancelFeeQTY'] > 0) {
            $booking[$i]['CancelFee'] = $_POST['CancelFee'];
            $booking[$i]['CancelFeeQTY'] = $_POST['CancelFeeQTY'];
            $booking[$i]['CancelFeeCollect'] = $_POST['CancelFeeCollect'];
        };
    } else {
        $booking[$i]['CancelFee'] = 0;
        $booking[$i]['CancelFeeQTY'] = 0;
        $booking[$i]['CancelFeeCollect'] = 0;
    };
    $booking[$i]['CollectPay'] = $_POST['CollectPay' . $i];
    $booking[$i]['foc'] = '';
    if ($booking[$i]['Rate'] == 0) {
        $booking[$i]['foc'] = $_POST['foc' . $i];
    };
    $booking[$i]['CustomerLastName'] = $_POST['lastname'];
    $booking[$i]['CustomerFirstName'] = $_POST['firstname'];
    $booking[$i]['RomajiName'] = $booking[$i]['CustomerLastName'] . ' ' . $booking[$i]['CustomerFirstName'];
    $booking[$i]['SplitName1'] = $booking[$i]['RomajiName'];
    $booking[$i]['SplitName2'] = $booking[$i]['RomajiName'];
    $booking[$i]['SplitName3'] = $booking[$i]['RomajiName'];
    $booking[$i]['ContactNo'] = $_POST['teleno'];
    $booking[$i]['CustomerEmail'] = $_POST['email'];
    $booking[$i]['Notes'] = $_POST['notes'] . ((!empty($_POST['booked_by'])) ? " [ {$_POST['booked_by']} " . date("Y/m/d") . " ] " : "");

    $booking[$i]['MedicalConditionsChecked'] = $_POST['MedicalConditionsChecked'];
    if ($booking[$i]['DeleteStatus'] == 0) {
        $booking[$i]['Checked'] = 1;//it looks like this sets the checked in status where is this in the database
    };
};

//If $_SESSION['post_....]) is not set then calling update booking will cause blank data to be written over the
//values in the database. Since this gets unset at the end of the page, refereshing the page causes the data
//to be over written with blank data.
//we check if $_SESSION['post_1234'] is set as data from a checkin is stored here
//we check of $_POST['updateCheckIn'] is set as when we update a registration the data is not stored in session
//the same way as a Check In is (this is a bad idea)

if (isset($_SESSION['post_' . (int)$_GET['regid']]) || isset($_POST['updateCheckIn'])) {
    updateBooking($booking);
}

//this may be causing the problem we are having
unset($_SESSION['post_' . (int)$_GET['regid']]);
?>

<html>
<head>
    <?php include 'includes/header_tags.php'; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
    <meta http-equiv="X-UA-Compatible" content="IE=8">
    <meta content="yes" name="apple-mobile-web-app-capable"/>
    <meta http-equiv="refresh" content="3; URL=/Reception/dailyViewIE.php">

    <script src="js/functions.js" type="text/javascript"></script>

    <title>
    </title>
</head>
<body>
<center><h2>Customer Information had been UPDATED!!!</h2><br>
    <h3>Redirecting in 3 seconds</h3><br>
    <!--<a href="/Reception/dailyViewIE.php">Start Again</a>-->
    <img src="img/roundBusyBall.gif"/>
</center>
</body>
</html>
