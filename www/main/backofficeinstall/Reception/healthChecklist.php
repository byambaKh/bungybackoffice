<!DOCTYPE html>
<html>
<?php
$englishTitle = "Medical Conditions Checklist";

$englishChecklist = array(
	array("title" => "K1: Heart Problems", "points" => array(
		"1) Medication: Are you on any? Did you take today? Do you have it with you?",
		"2) History: Have you suffered incidents often? When was the last incident?",
		"3) Severity: Is it serious? Does it cause you pain when you do light exercise?"
		)
	),

	array("title" => "K2: High Blood Pressure", "points" => array(
		"1) Medication: Are you on any? Did you take today? Do you have it with you?",
		"2) History: Have you suffered incidents often? When was the last incident?",
		"3) Severity: Is it serious? Does it cause you problems when you do light exercise?"
		)
	),

	array("title" => "K3: Epilepsy", "points" => array(
		"1) Medication: Are you on any? Did you take today? Do you have it with you?",
		"2) History: Do you suffer attacks often? When was the last attack?",
		"3) Severity: Is it serious? What usually triggers an attack (visual stimuli, low blood sugar, etc.)?"
		)
	),

	array("title" => "K4: Dislocations", "points" => array(
		"1) Location: What part of your body?",
		"2) History: Do you suffer dislocations often? When was the last incident?",
		"3) Severity: Is it serious? Does it cause you problems when you do light exercise?"
		)
	),

	array("title" => "K5: Diabetes", "points" => array(
		"1) Medication: Are you on any? Did you take today? Do you have it with you?",
		"2) History: What type (Type 1 - from birth; Type 2 - developed)? When was the last time you went into diabetic shock?",
		"3) Severity: Do you go into shock easily? Do you have related heart problems?"
			)
	),

	array("title" => "K6: Asthma", "points" => array(
		"1) Medication: Do you use an inhaler? Do you have it with you?",
		"2) History: Do you suffer attacks often? When was the last attack?",
		"3) Severity: Is it serious? Does it cause you problems when you do light exercise or get excited?"
		)
	),

	array("title" => "K7: Previous Surgeries", "points" => array(
		"1) Type / Location: What type? What part of your body?",
		"2) History: When was it? Have you experienced any problems since then?",
		"3) Severity: Have you healed completely or are you still in the process? Does it cause you problems when you do light exercise?"
		)
	),

	array("title" => "K8: Previous Eye Surgery", "points" => array(
		"1) Type: What type?",
		"2) History: When was it? Have you experienced any problems since then?",
		"3) Severity: Did the doctor mention any restrictions? Does it cause you problems when you do light exercise?",
		"Note: If the surgery was more than 3 months previous, it should be OK."
		)
	),

	array("title" => "K9: Serious Hangover", "points" => array(
		"1) Severity: Is it serious? Do you have a headache? Were you sick this morning?",
		"2) Treatment: Have you taken pain killers? Are you hydrating?",
		"Note: Inform the customer that if anything were to happen during jump and alcohol was found in their bloodstream, then they would not be covered by the insurance."
		)
	),

	array("title" => "K10: Prosthetic Limbs", "points" => array(
		"1) Type / Location: What type? What part of your body?",
		"2) History: Have you ever experienced any problems with it while doing light exercise?",
		"3) Severity: Is there any chance that it could detach from your body during the jump?",
		"Note: Jumpers with artificial legs are required to use a body harness."
		)
	),

	array("title" => "K11: Mental Disorder", "points" => array(
		"1) Medication: Are you on any? Did you take today? Do you have it with you?",
		"2) History: Do you suffer episodes often? When was the last time you suffered an episode? Have you consulted your doctor about bungy jumping?",
		"3) Severity: Is it serious? What triggers it? Does it cause you problems when you get excited or stressed?"
		)
	),

	array("title" => "K12: Bone Disease", "points" => array(
		"1) Medication: Are you on any? Did you take today? Do you have it with you?",
		"2) History: Have you suffered breaks or fractues before? When was the last incident?",
		"3) Severity: Is it serious? Does it cause you problems when you do light exercise?"
		)
	)
);

$japaneseTitle = "健康状態チェックリスト";

$japaneseChecklist = array(
	array("title" => "K1: 心臓病", "points" => array(
		"1) 薬の服用:  服用中ですか? 今日は服用しましたか?  必要な薬を持参していますか?",
		"2) 既往歴:  たまに症状が現れますか?  最後に症状があったのはいつですか?",
		"3) 重症度:  重症ですか?  簡単なエクササイズで痛みがありますか?"
		)
	),
	array("title" => "K2: 高血圧", "points" => array(

		"1) 薬の服用:  服用中ですか? 今日は服用しましたか?  必要な薬を持参していますか?",
		"2) 既往歴:  たまに症状が現れますか?  最後に症状があったのはいつですか?",
		"3) 重症度:  重症ですか?  簡単なエクササイズで症状が起こりますか?"
		)
	),
	array("title" => "K3: てんかん", "points" => array(

		"1) 薬の服用:  服用中ですか? 今日は服用しましたか?  必要な薬を持参していますか?",
		"2) 既往歴:  たまに症状が現れますか?  最後に症状があったのはいつですか?",
		"3) 重症度:: 重症ですか? 　症状の引き金となる条件はなんですか（視覚からの刺激、低血糖、など）?"
		)
	),

	array("title" => "K4: 脱臼", "points" => array(
		"1) 部位: 身体のどの部分ですか?",
		"2) 既往歴:  たまに起こりますか?  最後に脱臼したのはいつですか?",
		"3) 重症度:  重症ですか?  簡単なエクササイズでも脱臼しますか?"
		)
	),

	array("title" => "K5: 糖尿病", "points" => array(
		"1) 薬の服用:  服用中ですか? 今日は服用しましたか?  必要な薬を持参していますか?",
		"2) 既往歴:  糖尿病の種類 (タイプ1 - 先天性; タイプ2 - 後天性）?  最後に糖尿病性機能障害が起こったのはいつですか？",
		"3) 重症度:  糖尿病性機能障害をよく起こしますか?  心臓疾患に関連性をもたらしますか?"
		)
	),

	array("title" => "K6: 喘息", "points" => array(
		"1) 薬の服用:  吸入具を使用しますか？持参していますか?",
		"2) 既往歴:  たまに症状が現れますか?  最後に症状があったのはいつですか?",
		"3) 重症度:  重症ですか?  簡単なエクササイズや軽度の興奮で症状が起こりますか?"
		)
	),

	array("title" => "K7: 手術経験をされた方", "points" => array(
		"1) 種類/部位:  どんな手術ですか? 身体のどの部分ですか?",
		"2) 既往歴: 　いつしましたか?  術後、何か問題がありましたか?",
		"3) 重症度:  完治していますか? もしくはまだ治癒はしていない状態ですか？　  簡単なエクササイズで問題を感じますか?"
		)
	),


	array("title" => "K8: 目の手術をされた方　", "points" => array(
		"1) 種類：　どんな手術ですか?",
		"2) 既往歴: 　いつしましたか?  術後、何か問題がありましたか?",
		"3) 重症度:  医師から何か制限を受けていますか?  簡単なエクササイズで問題を感じますか?",
		"注記：手術後3ヶ月経過していない場合は、参加は控えていただきます。"
		)
	),

	array("title" => "K9: 二日酔い", "points" => array(
		"1) 重症度:  重症ですか? 頭痛はしますか?  今朝は気分が悪かったですか?",
		"2) 処置:  痛み止めなど服用しましたか?  脱水症状はありませんか?",
		"注記:ジャンプ中に何かが起きて、飲酒が認められた場合は、保険が適用されない可能性があることを必ず伝えます。"
		)
	),

	array("title" => "K10: 義足等", "points" => array(
		"1) 種類/部位:  どんな種類ですか? 身体のどの部分ですか?",
		"2) 既往歴: 簡単なエクササイズ中に何か問題が発生したことはありますか？",
		"3) 重症度:  外れる可能性がありますか?",
		"注記: 義足の場合はボディハーネスのみ使用可"
		)
	),

	array("title" => "K11: 精神障害", "points" => array(
		"1) 薬の服用:  服用中ですか? 今日は服用しましたか?  必要な薬を持参していますか?",
		"2) 既往歴:  たまに症状は起こりますか? 最後に症状が起きたのはいつですか? バンジージャンプをすることについて医師に相談しましたか?",
		"3) 重症度:  重症ですか? 引き金となる条件はありますか? 興奮状態やストレスにさらされた時に症状は起こり得ますか?"
		)
	),

	array("title" => "K12: 骨粗鬆症", "points" => array(
		"1) 薬の服用:  服用中ですか? 今日は服用しましたか?  必要な薬を持参していますか?",
		"2) 既往歴: 　 骨折やひびなど起きたことはありますか? 最後のケガはいつですか?",
		"3) 重症度:  重症ですか?  簡単なエクササイズで問題を感じますか?"
		)
	)
);

function displayChecklist($checklist){
	$checklistTable = "<table>\n";
	foreach($checklist as $sectionNumber => $section){
		$checklistTable .= "\t<tr><td style='font-weight:900;'>{$section['title']}</td></tr>\n";
		foreach($section["points"] as $sectionPoints => $point){
			$checklistTable .= "\t\t<tr><td>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$point</td></tr>\n";
		}
        $checklistTable .= "\t\t<tr><td><br></td></tr>\n";
	}
	$checklistTable .= "</table>";
    return $checklistTable;
}

//Default to English
$language = 'eng';
if(isset($_GET['language'])) $language = $_GET['language'];

$title = $englishTitle;
$checklist = $englishChecklist;

if($language == "jpn"){
    $title = $japaneseTitle;
    $checklist = $japaneseChecklist;
}
echo "<head>";
echo "<title>$title</title>";
echo "<meta http-equiv='Content-type' content='text/html; charset=utf-8' />";
echo "</head>";
echo "<body>";
echo "<h1>$title</h1>\n";
echo displayChecklist($checklist);
?>
</body>
</html>
