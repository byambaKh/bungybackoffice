<?php
require '../includes/application_top.php';
echo Html::head("Bungy Japan - Ticket Panel", array("/Waiver/css/stylesheet.css", "colorbox.css", "reception.css"), array("reception.js", "jquery-1.11.1.js", "jquery.colorbox.js"));

$loadedResults 		= mysql_query('SELECT * FROM settings WHERE site_id = '.CURRENT_SITE_ID.' AND key_name = "printerTicketAndCertificate"');
$settingsRow 		= mysql_fetch_assoc($loadedResults);
$savedPrintersArray  	= explode("\n", $settingsRow['value']);

$certificatePrinter = $ticketPrinter = '';//set them to an empty string if there are not results

$findPrintersResultsCount = mysql_num_rows($loadedResults);

if($findPrintersResultsCount){
	$certificatePrinter 	= trim($savedPrintersArray[0]);
	$ticketPrinter 		= trim($savedPrintersArray[1]);
} 


?>

<!-- http://stackoverflow.com/questions/1096862/print-directly-from-browser-without-print-popup-window -->
<?php
//NumberPad Components
/*               
Hidden Button Div
Reveler Button
functionality code

echo Html::numpad(activatorButton, outputDisplay, title, maximumValue)

*/

?>

<div id="mainDiv">
	<div id="hiddenInline" style="display:none">
		<div id="showMe" class="hiddenNumpad" style="background-color:white; color:black;">
			<h1>Photo #</h1>
			<div class="numpadContainer">
				<input class="numpadDisplay" name="numpadDisplay-showMe">
				<button class="numpadButton">1</button>
				<button class="numpadButton">2</button>
				<button class="numpadButton">3</button>
				<button class="numpadButton">4</button>
				<button class="numpadButton">5</button>
				<button class="numpadButton">6</button>
				<button class="numpadButton">7</button>
				<button class="numpadButton">8</button>
				<button class="numpadButton">9</button>
				<button class="numpadButton numpadButtonWide">0</button>
				<button id="delButton">DELETE</button>
				<button id="enterButton">ENTER</button>
			</div>
		</div>
	</div>
	<div id="formUpperInputs" class=" clearBoth">
		<h2>Jump Ticket</h2>

		<div class="formTop">
			<div class="headerInput">
				<input type="text" name="headerInput1" size='3' class="inputText">
				<div class="inputSeparator noClear">-</div>
				<input type="text" name="weigntInput2" size='3' class="inputText noClear">
			</div>

			<div class="headerInput">
			On Site
			</div>

			<div class="headerInput">
				<input type="text" name="weightInput1" size='3' class="inputText">
				<div class="inputSeparator noClear">-</div>
				<input type="text" name="weigntInput2" size='3' class="inputText noClear">
			</div>
		</div>


	</div>
	<div id="formInnerLeft" class="newLine whiteBox">
		<div class="whiteInput doubleWidth blackBorder" onclick="updateTime()">
			<div class="inputTitle" id="timeInput" >Time</div>
			<h1 id="timeDisplay">??:??</h1>
		</div>

		<div class="whiteInput newLine blackBorder openPhotoNumpad" id="">
			<div class="inputTitle">Photo</div>
			<div class="inputBox"><input type="text" name="photoInput" size='3' value="0" id="photoInput" class="inputText"></div>
		</div>

		<div class="whiteInput blackBorder" id="jump">
			<div class="inputTitle">Jump</div>
			<div class="inputBox"><input type="text" name="jumpInput" size='3' class="inputText"></div>
		</div>
		<div class="whiteInput doubleWidth newLine blackBorder" id="weight">
			<div class="inputTitle">Weight</div>
			<div class="inputBox">
				<input type="text" name="weightInput1" size='3' class="inputText">
				<div class="inputSeparator noClear">-</div>
				<input type="text" name="weigntInput2" size='3' class="inputText noClear">
			</div>
		</div>
		<div class="whiteInput doubleWidth newLine blackBorder" id="yrbbk">
			<div class="inputBox">
				<!--TODO display the actual cord color name-->
				<div class="inputSeparator noClear">Y</div> &nbsp;  

				<div class="inputSeparator noClear" style="background-color: black; color: white;">R</div> &nbsp; 

				<div class="inputSeparator noClear">B</div> &nbsp; 

				<!--<input type="text" name="weightInput1" size='1' class="inputText noClear">-->
				<div class="inputSeparator noClear">BK</div> &nbsp; 
			</div>
		
		</div>

		<div class="whiteInput doubleWidth newLine blackBorder" id="name">
			<div class="inputTitle">Name</div>
			<div class="inputBox"><input type="text" name="customerName" size='20' class="inputText"></div>
		</div>
		<div class="buttonDiv doubleWidth newLine" id="print">
			<?php echo html::inputPageButton("print", "printTicket('$certificatePrinter', '$ticketPrinter')", null, null, $classes = "redButton button", "walkIn");?>
		</div>

		<!--<div class="buttonDiv doubleWidth newLine" id="print"><input type="submit" class="redButton button" onClick="window.print();" value="Print"></div>-->
	</div>

	<div id="formInnerRight">
		  <?php 
		  $bdnl = "buttonDiv newLine redButton";
		  echo html::inputPageButton("Walk in",        	"gotoUrl('#');", null, null, $classes = "redButton buttonSmallText $bdnl", "walkIn");
		  echo html::inputPageButton("test", 		"gotoUrl('#');", null, null, $classes = "redButton $bdnl", "test");
		  echo html::inputPageButton("foc", 		"gotoUrl('#');", null, null, $classes = "redButton $bdnl", "foc");
		  echo html::inputPageButton("late customer",  	"gotoUrl('#');", null, null, $classes = "redButton buttonSmallSmallText $bdnl", "lateCustomer");
		  echo html::inputPageButton("price", 		"gotoUrl('#');", null, null, $classes = "redButton $bdnl", "price");
		  echo html::inputPageButton("mistake",        	"gotoUrl('#');", null, null, $classes = "redButton buttonSmallText $bdnl", "mistake");
		  echo html::inputPageButton("report", 		"gotoUrl('#');", null, null, $classes = "redButton $bdnl", "report");
		  ?>
	</div>

	<div id="formOuterRight">
		<!--TODO Add Button PHP code again-->
		<input type="submit" class="button redButton button buttonSmallText green newLine" id="goods" value="g o o d s" onclick="gotoUrl('#');" style="; ;">
		<input type="submit" class="button redButton button buttonSmallSmallText newLine" id="nonJump" value="n o n J u m p" onclick="gotoUrl('#');" style="; ;">
		<input type="submit" class="button redButton button newLine" id="questionMark" value="?" onclick="gotoUrl('#');" style="; ;">    
	</div>

	<div class="formBottom newLine">
		  <?php
		  $common = "redButton bottomButton";
		  echo html::inputPageButton("cords", 	 "gotoUrl('cordCalibration.php');", "100px", "100px", $classes = "$common blue", "cords");
		  echo html::inputPageButton("shutdown", "gotoUrl('#');", "100px", "100px", $classes = "$common buttonSmallText", "shutdown");
		  echo html::inputPageButton("enter weight",   "gotoUrl('#');", "100px", "100px", $classes = "$common buttonSmallText", "weight");
		  echo html::inputPageButton("esc",      "gotoUrl('#');", "100px", "100px", $classes = "$common ", "esc");
		  echo html::inputPageButton("settings", "gotoUrl('ticketSettings.php');", "400px", "50px", $classes = "bottomButton purpleButton", "ticketSettings");
		  ?>
	</div>
</div>
	<?php 
		$date 		= urlencode("January 30th 2014");
		$fullName 	= urlencode("Ayodeji Osokoya");
		$signatureUrl 	= urlencode("testsignature.png");
	?>
	<iframe class="hiddenCertificateFrame" name="certificate" src="<?php echo"certificate.php?fullName=$fullName&date=$date&signatureUrl=$signatureUrl"?>" style="display:none;"></iframe>
	<iframe name="ticketmini" src="ticketmini.php"></iframe>
<script>
	function gotoUrl(page){
        	document.location = page;
	}

	function setTimeTo(value){
		document.getElementById('timeDisplay').innerHTML = '<h1>'+ value + '</h1>';
	}

	function getHalfHourRoundedTime(){
		var dateObject = new Date();
        	var hours = dateObject.getHours();
		var thirtyMins = dateObject.getMinutes();

		thirtyMins = (thirtyMins <= 30 ? '00' : '30'); //round time to the nearest half hour
		return hours + ':' + thirtyMins;
	}

	function updateTime(){
		document.getElementById('timeDisplay').innerHTML = '<h1>' + getHalfHourRoundedTime() + '</h1>';
	}

	$(document).ready(function(){
		updateTime();

		$(".openPhotoNumpad").colorbox({
			inline:true,
			href:"#showMe",
			opacity: 0.7,
			transition: "elastic",
			closeButton: true,
			escKey: true
		});
		
		function getNumpadIdFromButton(numpadElement){
			var senderNumpad = $(numpadElement).parents(".hiddenNumpad");
			return senderNumpad.attr('id');

		}

		$('.numpadButton').bind('click touchend MSPointerUp pointerup', function (e) {
			e.preventDefault();

			//the display in the popup
			//console.log($(this).parents(".hiddenNumpad"));
                        var numpadId = getNumpadIdFromButton(this);
			var numpadDisplayId = '.'.numpadDisplay +'-'+ numpadId;

			/*
			var senderNumberPadId = senderNumberPad[0].attr('id');
			console.log(senderNumberPad);
			console.log(senderNumberPadId);
			*/
			$('.numpadDisplay').val('' + $('.numpadDisplay').val() + $(this).html());
			//the display on the form
			$('#photoInput').val('' + $('.numpadDisplay').val());

			if ($('.numpadDisplay').val() > 100){  
			    $('.numpadDisplay').val(100);
			    $('#photoInput').val('' + $('.numpadDisplay').val());

				return;
			}
		});

		$('#delButton').bind('click touchend MSPointerUp pointerup', function (e) {
			e.preventDefault();
			var val = $('.numpadDisplay').val();
			if (val.length > 0) {
				$('.numpadDisplay').val(val.substr(0, val.length-1));
				$('#photoInput').val('' + $('.numpadDisplay').val());
			}
		});

		$('#enterButton').bind('click touchend MSPointerUp pointerup', function (e) {
			e.preventDefault();
			$.colorbox.close();
		});

	});
	//performAction();
</script>
<?php require_once("../includes/pageParts/standardFooter.php");?>

