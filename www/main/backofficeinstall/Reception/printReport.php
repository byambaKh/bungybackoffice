<?php
  include '../includes/application_top.php';
  include 'checkin_config.php';

  $booking = getBookingInfo((int)$_GET['regid']);

  $_POST = $_SESSION['post_' . (int)$_GET['regid']];

  for ($i = 0; $i <= 3; $i++) {
    // if it is plain booking, bypass all other records
    if (count($_POST['splitTime']) == 1 && $i > 0) continue;

    if (!array_key_exists($i, $booking)) {
        $booking[$i] = $booking[$i-1]; // set all db values for this booking
        unset($booking[$i]['CustomerRegID']); // unset reg id - it is not exists for now
        unset($booking[$i]['groopBooking']); // uset group booking
    };
    if ($_POST['splitJump'][$i] == 0) {
        //$booking[$i]['DeleteStatus'] = 1;
    };
    // update booking values
    $booking[$i]['Agent'] = $_POST['agentn'];
    // update times
    $booking[$i]['BookingTime'] = $_POST['splitTime'][$i];
    if (count($booking) > 1) {
        $booking[$i]['SplitTime1'] = $_POST['splitTime'][1];
        $booking[$i]['SplitTime2'] = $_POST['splitTime'][2];
        $booking[$i]['SplitTime3'] = $_POST['splitTime'][3];
    };
    // update rates
    $booking[$i]['Rate'] = $_POST['rate'][$i];
    // update jumps count
    $booking[$i]['NoOfJump'] = $_POST['splitJump'][$i];
    if (count($booking) > 1) {
        $booking[$i]['SplitJump1'] = $_POST['splitJump'][1];
        $booking[$i]['SplitJump2'] = $_POST['splitJump'][2];
        $booking[$i]['SplitJump3'] = $_POST['splitJump'][3];
    };
    // do some updates for master record only
    if ($i == 0) {
        // update ratetopay to agent
        $booking[$i]['RateToPay'] = $_POST['ratetopay'];
        $booking[$i]['RateToPayQTY'] = $_POST['nos'];
        // additional items
        foreach ($add_items as $code => $value) {
            $booking[$i][$code] = $_POST['add_values'][$code] * $value['price'];
            $booking[$i][$code.'_qty'] = $_POST['add_values'][$code];
        };
    };
    // collect pay
    if ($i == 0) {
        if ($_POST['CancelFee'] > 0 && $_POST['CancelFeeQTY'] > 0) {
            $booking[$i]['CancelFee'] = $_POST['CancelFee'];
            $booking[$i]['CancelFeeQTY'] = $_POST['CancelFeeQTY'];
            $booking[$i]['CancelFeeCollect'] = $_POST['CancelFeeCollect'];
        };
    };
    $booking[$i]['CollectPay'] = $_POST['CollectPay' . $i];
    $booking[$i]['CustomerLastName'] = $_POST['lastname'];
    $booking[$i]['CustomerFirstName'] = $_POST['firstname'];
    $booking[$i]['RomajiName'] = $booking[$i]['CustomerLastName'] . ' ' . $booking[$i]['CustomerFirstName'];
    $booking[$i]['SplitName1'] = $booking[$i]['RomajiName'];
    $booking[$i]['SplitName2'] = $booking[$i]['RomajiName'];
    $booking[$i]['SplitName3'] = $booking[$i]['RomajiName'];
    $booking[$i]['ContactNo'] = $_POST['teleno'];
    $booking[$i]['CustomerEmail'] = $_POST['email'];
    $booking[$i]['Notes'] = $_POST['notes'] . ((!empty($_POST['booked_by']))? " [ {$_POST['booked_by']} ".date("Y/m/d")." ] " : "");
  };

  logDebugData("SESSION");
  logDebugData($_SESSION);

?>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<title>Bungy Japan</title>

</head>
<body>
<table align="center">
  <tr>
    <td align="center"><img alt="Bungy Japan Receipt" src="img/blogowhite.jpg" align="middle"></td>
  </tr>
</table>

<table border="1" width="50%" align="center">
<tbody>

<tr>
  <td colspan="4" align="center">
    <b>BOOKING PAYMENT</b>
  </td>
</tr>
<tr>
  <td align="center">
    <b>ITEM</b>
  </td>
  <td align="center"><b>NO</b></td>  
  <td align="center"><b>RATE</b></td>  
  <td align="center"><b>TOTAL</b></td>    
</tr>
<?php
    $total = 0;
    $total_for_jumps = 0;
    foreach ($booking as $b) {
        if ($b['NoOfJump'] == 0) continue;
        if ($b['CollectPay'] == 'Onsite') $total += $b['NoOfJump'] * $b['Rate'];
?>
    <tr>
      <td align="right"> Bungy Jump/s (<?php echo $b['BookingTime']; ?>) </td>
      <td align="center"><?php echo $b['NoOfJump']; ?></td>
      <td align="right"><?php echo $b['Rate'];?></td>
      <td align="right"><?php echo $b['NoOfJump'] * $b['Rate']?></td>
    </tr>
<?php
    };
    if ($booking[0]['CancelFeeCollect'] == 'Onsite') {
        $total += $booking[0]['CancelFeeQTY'] * $booking[0]['CancelFee'];
    };
?>
    <tr>
      <td align="right">Cancellation</td>
      <td align="center"><?php echo $booking[0]['CancelFeeQTY']; ?></td>
      <td align="right"><?php echo $booking[0]['CancelFee']; ?></td>
      <td align="right"><?php echo $booking[0]['CancelFeeQTY'] * $booking[0]['CancelFee']; ?></td>
    </tr>
<?php
    $total_for_jumps = $total;
    foreach ($add_items as $code => $value) {
		if ($booking[0][$code] == 0) continue;
?>
    <tr>
      <td align="right"><?php echo $value['title']; ?></td>
      <td align="center"><?php echo ($code == 'other')?1:$booking[0][$code.'_qty'];?></td>
      <td align="right"><?php echo ($code == 'other')?$booking[0][$code]:$value['price']; ?></td>
      <td align="right"><?php echo $booking[0][$code];?></td>
    </tr>
<?php
        $total += $booking[0][$code];
    }
	if ($booking[0]['RateToPay'] != 0) {
?>
<tr>
  <td align="right">
    To Be Collected For Agent
  </td>
  <td align="center">
    <?php print $booking[0]['RateToPayQTY'];?>
  </td>
  <td align="right">
    <?php print $booking[0]['RateToPay'];?>
  </td>
  <td align="right"><?php print $booking[0]['RateToPay'] * $booking[0]['RateToPayQTY'];?></td>
</tr>
<?php
		$total += $booking[0]['RateToPay'] * $booking[0]['RateToPayQTY'];
	};
?>
<tr>
  <td colspan="4" align="right">
    <b>TOTAL:</b>&nbsp;&yen;&nbsp;<?php 
                                                print $total;?>
  </td>
</tr>
<tr>
  <td colspan="4" align="right">
    <b>Amount Paid:</b>&nbsp;&yen;&nbsp;<?php 
                      if($_GET['paid'] == ''){
                        print 0;
                      }else {
                        print $_GET['paid'];
                      }  
                      ?>
  </td>
</tr>
<tr>
  <td colspan="4" align="right">
    <b>Remaining Amount:</b>&nbsp;&yen;&nbsp;<?php 
                                                            print $_GET['paid'] - $total;?>
  </td>
</tr>

</tbody>
</table>
<table width="50%" align="center">
  <tr bgcolor="#C0C0C0">
    <td align="center">
      <b>143 Obinata, Minakami-machi, Gumna-ken</b>
    </td>
  </tr>
</table>
</body>
</html>
