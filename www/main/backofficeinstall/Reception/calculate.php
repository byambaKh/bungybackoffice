<?php

include '../includes/application_top.php';
include 'checkin_config.php';

//logDebugData(__FILE__);
//logDebugData($_POST);

$regid = (int)$_GET['regid'];
$booking = getBookingInfo((int)$_GET['regid']);

//logDebugData("Booking before processing");
//logDebugData($booking);
for ($i = 0; $i <= 3; $i++) {
    // if it is plain booking, bypass all other records
    if (count($_POST['splitTime']) == 1 && $i > 0) continue;

    if (!array_key_exists($i, $booking)) {
        $booking[$i] = $booking[$i - 1]; // set all db values for this booking
        unset($booking[$i]['CustomerRegID']); // unset reg id - it is not exists for now
        unset($booking[$i]['groopBooking']); // uset group booking
    };
    if ($_POST['splitJump'][$i] == 0) {
        //$booking[$i]['DeleteStatus'] = 1;
    };
    // update booking values
    $booking[$i]['Agent'] = $_POST['agentn'];
    // update times
    $booking[$i]['BookingTime'] = $_POST['splitTime'][$i];
    if (count($booking) > 1) {
        $booking[$i]['SplitTime1'] = $_POST['splitTime'][1];
        $booking[$i]['SplitTime2'] = $_POST['splitTime'][2];
        $booking[$i]['SplitTime3'] = $_POST['splitTime'][3];
    };
    // update rates
    $booking[$i]['Rate'] = $_POST['rate'][$i];
    // update jumps count
    $booking[$i]['NoOfJump'] = $_POST['splitJump'][$i];
    if (count($booking) > 1) {
        $booking[$i]['SplitJump1'] = $_POST['splitJump'][1];
        $booking[$i]['SplitJump2'] = $_POST['splitJump'][2];
        $booking[$i]['SplitJump3'] = $_POST['splitJump'][3];
    };
    // do some updates for master record only
    if ($i == 0) {
        // update ratetopay to agent
        $booking[$i]['RateToPay'] = $_POST['ratetopay'];
        $booking[$i]['RateToPayQTY'] = $_POST['nos'];
        // additional items
        foreach ($add_items as $code => $value) {
            $booking[$i][$code] = $_POST['add_values'][$code] * $value['price'];
            $booking[$i][$code . '_qty'] = $_POST['add_values'][$code];
        };
    } else {
        // update ratetopay to agent
        $booking[$i]['RateToPay'] = 0;
        $booking[$i]['RateToPayQTY'] = 0;
        // additional items
        foreach ($add_items as $code => $value) {
            $booking[$i][$code] = 0;
            $booking[$i][$code . '_qty'] = 0;
        };
    };
    // collect pay
    if ($i == 0) {
        if ($_POST['CancelFee'] > 0 && $_POST['CancelFeeQTY'] > 0) {
            $booking[$i]['CancelFee'] = $_POST['CancelFee'];
            $booking[$i]['CancelFeeQTY'] = $_POST['CancelFeeQTY'];
            $booking[$i]['CancelFeeCollect'] = $_POST['CancelFeeCollect'];
        };
    };
    $booking[$i]['CollectPay'] = $_POST['CollectPay' . $i];
    $booking[$i]['CustomerLastName'] = $_POST['lastname'];
    $booking[$i]['CustomerFirstName'] = $_POST['firstname'];
    $booking[$i]['RomajiName'] = $booking[$i]['CustomerLastName'] . ' ' . $booking[$i]['CustomerFirstName'];
    $booking[$i]['SplitName1'] = $booking[$i]['RomajiName'];
    $booking[$i]['SplitName2'] = $booking[$i]['RomajiName'];
    $booking[$i]['SplitName3'] = $booking[$i]['RomajiName'];
    $booking[$i]['ContactNo'] = $_POST['teleno'];
    $booking[$i]['CustomerEmail'] = $_POST['email'];
    $booking[$i]['Notes'] = $_POST['notes'] . ((!empty($_POST['booked_by'])) ? " [ {$_POST['booked_by']} " . date("Y/m/d") . " ] " : "");
    $booking[$i]['MedicalConditionsChecked'] = $_POST['MedicalConditionsChecked'];
};

//logDebugData("Booking after processing");
//logDebugData($booking);

//logDebugData("SESSION");
//logDebugData($_SESSION);
// save POST to session for next page.
$_SESSION['post_' . (int)$_GET['regid']] = $_POST;
//updateBooking($booking);

?>
<html>
<head>
    <?php include 'includes/header_tags.php'; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=8">

    <title>Calculation</title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
    <link rel="stylesheet"
          href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css"
          type="text/css" media="all"/>

    <script type="text/javascript">
        function procGo() {

            //document.calform.action = "waiverIE.php";
            var randomnumber = Math.floor(Math.random() * 999999999);
            document.calform.action = "checkedCustomerIE.php?regid=<?php echo $regid; ?>";
            document.calform.submit();
        }
        function procPaid() {
            $("#calc").val($("#amtpaid").val() - $("#total_amount").val());
            return false;
        }
        function procPrint() {
            //window.print();
            window.open('/Reception/printReport.php?regid=<?php echo $regid; ?>&paid=' + $("#amtpaid").val());
        }
        function procBack() {
            //document.calform.action = "moneyReceivedIE.php";
            var randomnumber = Math.floor(Math.random() * 999999999);
            document.calform.action = "moneyReceivedIE.php";
            document.calform.submit();
        }
    </script>
</head>
<body>
<br>
<form action="" method="post" name="calform">
    <input type="hidden" name="cregid" value="<?php echo $regid; ?>">
    <table align="center" border="0" width="400px">
        <tbody>
        <tr>
            <td colspan="8" bgcolor="#000000" align="center" valign="middle">
                <h2><font color="#FFFFFF">BOOKING PAYMENT</font></h2>
            </td>
        </tr>
        <tr>
            <th bgcolor="#FF0000">
                <font color="#FFFFFF">ITEM</font>
            </th>
            <th bgcolor="#FF0000">
                <font color="#FFFFFF">NO</font>
            </th>
            <th bgcolor="#FF0000">
                <font color="#FFFFFF">RATE</font>
            </th>
            <th bgcolor="#FF0000" colspan="5">
                <font color="#FFFFFF">TOTAL</font>
            </th>
        </tr>
        <?php
        $total = 0;
        $total_for_jumps = 0;
        foreach ($booking as $b) {
            if ($b['NoOfJump'] == 0) continue;
            if ($b['CollectPay'] == 'Onsite') $total += $b['NoOfJump'] * $b['Rate'];
            ?>
            <tr>
                <td bgcolor="#FF0000">
                    <font color="#FFFFFF">Bungy Jump/s (<?php echo $b['BookingTime']; ?>)</font>
                </td>
                <td bgcolor="#FF0000" align="center">
                    <font color="#FFFFFF"><?php echo $b['NoOfJump']; ?></font>
                </td>
                <td bgcolor="#FF0000" align="center">
                    <font color="#FFFFFF"><?php echo $b['Rate']; ?></font>
                </td>
                <td bgcolor="#FF0000" align="right" colspan="5">
                    <font color="#FFFFFF"><?php echo $b['NoOfJump'] * $b['Rate'] ?></font>
                </td>
            </tr>
            <?php
        };
        if ($booking[0]['CancelFeeCollect'] == 'Onsite') {
            $total += $booking[0]['CancelFeeQTY'] * $booking[0]['CancelFee'];
        };
        ?>
        <tr>
            <td bgcolor="#FF0000"><font color="#FFFFFF">Cancellation</font></td>
            <td bgcolor="#FF0000" align="center"><font color="#FFFFFF"><?php echo $booking[0]['CancelFeeQTY']; ?></font>
            </td>
            <td bgcolor="#FF0000" align="center"><font color="#FFFFFF"><?php echo $booking[0]['CancelFee']; ?></font>
            </td>
            <td bgcolor="#FF0000" align="right" colspan="5">
                <font color="#FFFFFF"><?php echo $booking[0]['CancelFeeQTY'] * $booking[0]['CancelFee']; ?></font></td>
        </tr>
        <?php
        $total_for_jumps = $total;
        foreach ($add_items as $code => $value) {
            ?>
            <tr>
                <td bgcolor="#FF0000"><font color="#FFFFFF"><?php echo $value['title']; ?></font></td>
                <td bgcolor="#FF0000" align="center">
                    <font color="#FFFFFF"><?php echo ($code == 'other') ? '' : $_POST['add_values'][$code]; ?></font>
                </td>
                <td bgcolor="#FF0000" align="center">
                    <font color="#FFFFFF"><?php echo ($code == 'other') ? '' : $value['price']; ?></font></td>
                <td bgcolor="#FF0000" align="right" colspan="5">
                    <font color="#FFFFFF"><?php echo $booking[0][$code]; ?></font></td>
            </tr>
            <?php
            $total += $booking[0][$code];
        }
        if ($booking[0]['RateToPayQTY'] > 0 && $booking[0]['RateToPay'] > 0) {
            ?>
            <tr>
                <td bgcolor="#FF0000">
                    <font color="#FFFFFF">To Pay to Agent:</font>
                </td>
                <td bgcolor="#FF0000" align="center">
                    <font color="#FFFFFF"><?php print $booking[0]['RateToPayQTY']; ?></font>
                </td>
                <td bgcolor="#FF0000" align="center">
                    <font color="#FFFFFF"><?php print $booking[0]['RateToPay']; ?></font>
                </td>
                <td bgcolor="#FF0000" align="right" colspan="5">
                    <font color="#FFFFFF"><?php print $booking[0]['RateToPayQTY'] * $booking[0]['RateToPay']; ?></font>
                </td>
            </tr>
            <?php
            $total += $booking[0]['RateToPayQTY'] * $booking[0]['RateToPay'];
        };
        ?>
        </tbody>
    </table>
    <table align="center" border="0" width="400px">
        <tbody>


        <tr bgcolor="#000000">
            <td colspan="6" align="center">
                <font color="#FFFFFF" size="3">TOTAL</font>
            </td>
            <!--<td align="center" colspan="1">
        <font color="#FFFFFF"><?php print $total; ?></font>
      </td>
      -->
        </tr>
        <tr bgcolor="#000000">
            <td colspan="6" align="center">
                <font color="#FFFFFF" size="6">&yen;&nbsp;
                    <?php
                    if ($booking[0]['CollectPay'] == 'Offsite') {
                        //$total -= $total_for_jumps;
                    };
                    echo $total;
                    ?>
                    <input type="hidden" name="total_amount" id="total_amount" value="<?php echo $total; ?>">
                </font>
            </td>

        </tr>
        <tr bgcolor="#000000">
            <td colspan="3" align="center">
                <font color="#FFFFFF">Amount Paid:&nbsp;&nbsp;&yen;</font>
            </td>
            <td colspan="3" align="left">
                <input type="text" name="amtpaid" id="amtpaid" value="<?php print $_POST[amtpaid]; ?>">
            </td>
        </tr>
        <tr bgcolor="#000000">
            <td colspan="3" align="center">
                <input type="button" name="paid" id="paid" value="Calculate" style="background-color:#FFFF66" onclick="procPaid();">
                <font color="#FFFFFF">&nbsp;&nbsp;&nbsp;&yen;</font>
            </td>
            <td colspan="3" align="left">
                <input type="text" name="calc" id="calc" value="0">
                <font color="#FFFFFF">Change</font>
            </td>

        </tr>
        </tbody>
    </table>
    <table align="center" border="0" width="400px">
        <tbody>
        <tr bgcolor="#000000">
            <td colspan="2" align="left">
                <input type="button" name="back" id="back" value="Back" style="background-color:#FFFF66" onclick="procBack();">
            </td>
            <td align="center" colspan="1">
                <input type="button" name="printrcv" id="printrcv" value="Print Receipt" style="background-color:#FFFF66" onclick="procPrint();">
            </td>
            <td align="right">
                <input type="button" name="go" id="go" value="GO" style="background-color:#FFFF66" onclick="procGo();">
            </td>
        </tr>
        </tbody>
    </table>
    <?php
    //echo '---1:'.$moneyVal.'-----'.$noOfJump.'--'.$ratetopay.'--'.$scj.'--'.$photos.'--'.$shirt.'--'.$itema.'--'.$other.'>>>>>>';
    //echo '---2:'.$moneyVal.'-----'.$noOfJumpNew.'--'.$ratetopay.'--'.$scj.'--'.$photos.'--'.$shirt.'--'.$itema.'--'.$other;;
    ?>
</form>
</body>
</html>
