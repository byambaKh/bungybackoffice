<?php
  include "../includes/application_top.php";

  $cDate = date("Y-m-d");
  if (isset($_POST['jumpDate']) && preg_match("/^([\d]{4}-[\d]{2}-[\d]{2})$/", $_POST['jumpDate'])) {
	$cDate = $_POST['jumpDate'];
  };

?>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
<meta http-equiv="X-UA-Compatible" content="IE=8">

<?php include '../includes/head_scripts.php'; ?>
<script src="js/functions.js" type="text/javascript"></script>

<title><?php echo SYSTEM_SUBDOMAIN; ?> History : <?php echo $cDate; ?></title>
<script type="text/javascript" charset=Shift_JIS>

$(function() {
  $mydate1 = $('#jumpDate').datepicker({
    numberOfMonths: 1,    
    dateFormat: 'yy-mm-dd',
  });
});

</script>
<style>
#history-table {
	background-color: black;
}
#history-table th {
    background-color: white;
	padding: 2px;
}
#history-table td {
    background-color: white;
	padding: 2px;
}
#history-table .checked td {
	background-color: lightgreen !important;
}
#history-table .deleted td {
	background-color: #faa !important;
}
</style>
</head>
<body>
<?php include '../includes/main_menu.php'; ?>
<br>
<form name="dailyForm" method="post">
  <table align="center" width="100%"> 
    <tbody>    
	  <tr>
		<td align="center"><h1><?php echo SYSTEM_SUBDOMAIN . ' History : ' . $cDate; ?></h1></td>
	  </tr>
	  <tr>
		<td>
			<button onClick="document.location = '/Reception/';" type="button" id="back">Back</button>
			&nbsp;
			&nbsp;
			&nbsp;
			&nbsp;
			<input id="jumpDate" name="jumpDate" onChange="document.dailyForm.submit();" value="<?php echo $cDate; ?>" style="width:100px; text-align: center;">
		</td>
	 </tr>
    </tbody>
  </table>
<table border="0px" cellspacing="1px" cellpadding="0px" id="history-table">
<?php
    $sql = "SELECT * FROM customerregs1 WHERE site_id = '".CURRENT_SITE_ID."' AND bookingdate = '".$cDate."' order by BookingTime ASC;";
	$res = mysql_query($sql) or die(mysql_error());
	$header = 1;
	$ignore = array(
		'BookingDate',
		'OtherNo',
		'TransportMode',
		'DeleteStatus',
		'DayStatus',
		'TimeStatus',
		'BookingReceived',
		'Checked',
		'place_id',
		'site_id',
	);
	while ($row = mysql_fetch_assoc($res)) {
		if ($header) {
			echo "\t<tr>\n";
			foreach ($row as $field => $value) {
				if (in_array($field, $ignore)) continue;
				echo "\t\t<th>$field</th>\n";
			};
			echo "\t</tr>\n";
			$header = 0;
		};
		$class = array();
		if ($row['Checked'] == 1) {
			$class[] = 'checked';
		};
		if ($row['DeleteStatus'] == 1) {
			$class[] = 'deleted';
		};
		echo "\t<tr class='".implode(" ", $class)."'>\n";
		foreach ($row as $field => $value) {
			if (in_array($field, $ignore)) continue;
			echo "\t\t<td>$value</td>\n";
		};
		echo "\t</tr>\n";
		
	};
?>
</table>
</form>  
</body>
</html>
