<?php
	include "../includes/application_top.php";
	unset($_SESSION['converted_to_group']);
	$regid = (int)$_GET['regid'];
	if ($regid == 0) {
		Header("Location: dailyViewIE.php");
		die();
	};
    $booking = getBookingInfo($regid);
	if ($booking[0]['GroupBooking'] = 0 || sizeof($booking) == 1) {
		Header("Location: moneyReceivedIE.php?regid=" . $regid);
		die();
	};
	$total_jumps = 0;	
	foreach ($booking as $key => $b) {
		$total_jumps += $b['NoOfJump'];
		if ($key != 0) {
			unset($booking[$key]);
		};
	};
	// for $key == 0 change group booking to 0
	$booking[0]['NoOfJump'] = $total_jumps;
	$booking[0]['GroupBooking'] = 0;
	// just to besure we not delete records with GroupBooking 0 and 1 )))
	if ($booking[0]['CustomerRegID'] > 1) {
		mysql_query("DELETE FROM customerregs1 WHERE GroupBooking = '{$booking[0]['CustomerRegID']}'");
	};
	updateBooking($booking);
	Header("Location: dailyViewIE.php");
?>
