<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<?php
	$time 		= $_GET['time'];
	$weight 	= $_GET['weight'];
	$bookingType 	= $_GET['bookingType'];
	$jumpNumber 	= $_GET['jumpNumber'];
	$photoNumber 	= $_GET['photoNumber'];
	$cordColor 	= $_GET['cordColor'];
	//we could run a check to see if cord color matches the weight
	$groupOfNumber 	= $_GET['groupOfNumber'];
	$firstName 	= $_GET['firstName'];
	$lastName 	= $_GET['lastName'];
?>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Ticket</title>
	<style type="text/css">
		@media print, screen{
			td{
				border:1px;
				border-style:solid;
				width:100px;
			}

			.bigHeader{
				font-size: 25px;
			}

			.smallText{
				font-size: 12px;
			}

			table{
				border-collapse: collapse;
				width:200px;
				height:500px;
			}

			.blackOnWhite{
				background-color: black;
				color: white;
			}

			.centerText{
				text-align: center;
			}

			.cellHeader{
				display: block;
				font-size: 15px;
				text-align: left;
			}

			.cellContent{
				font-size: 40px;
			}
		}
		
	</style>
    </head>
    <body>

    <table width="200px">
    	<tr class="blackOnWhite centerText bigHeader">
		<td colspan=2>BUNGY JAPAN</td>
	</tr>
    	<tr class="">
		<td>Time: </td>
		<td>*<?php echo $bookingType?>*</td>
	</tr>
    	<tr class="centerText">
		<td colspan=2 class="cellContent centerText"><?php echo $time;?></td>
	</tr>
    	<tr class="centerText blackOnWhite">
		<td colspan=2>
			<div class="cellHeader">Weight:<?php echo $weight;?></div>
			<div class="cellContent"></div>
		</td>
	</tr>
    	<tr class="blackOnWhite">
		<td colspan=2 class="cellContent centerText"><?php echo $cordColor;?></td>
	</tr>
    	<tr class="centerText">
		<td>
			<div class="cellHeader">Jump#</div>
			<div class="cellContent"><?php echo $jumpNumber;?></div>
		</td>
		<td>
			<div class="cellHeader">Photo#</div>
			<div class="cellContent"><?php echo $photoNumber;?></div>
		</td>
	</tr>
    	<tr>
		<td colspan=2>
			<div>
				<div>Name: </div>
				<?php if($groupOfNumber == 1):?>
					<div>Single Jumper</div>
				<?php else: ?>
					<div>Group Of: <?php echo $groupOfNumber;?></div>
				<?php endif; ?>
			</div>
			<div class="cellContent centerText"><?php echo $firstName;?></div>
			<div class="cellContent centerText smallText"><?php echo $lastName;?></div>
		</td>
	</tr>
    	<tr>
		<td colspan=2 class="blackOnWhite centerText">Japan’s Only Bungy Professional</td>
	</tr>
    </table>

    </body>
</html>
