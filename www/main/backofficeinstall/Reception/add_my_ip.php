<?php
include "../includes/application_top.php";

$client_ip = $_SERVER['REMOTE_ADDR'];

$ips = file("../IPs.txt");
$ips[] = $client_ip;
$ips = array_map('trim', $ips);
$ips = array_filter($ips);
$ips = array_unique($ips);
sort($ips);
file_put_contents("../IPs.txt", implode("\n", $ips));

$_SESSION['ip_message'] = sprintf("Your IP address: %s is added to autologon list.", $client_ip);

Header("Location: /Reception/");

?>
