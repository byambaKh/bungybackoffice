<html>
<head>
	<link rel="stylesheet" href="../Waiver/css/stylesheet.css" type="text/css" media="all" /> 
	<link rel="stylesheet" href="../css/reception.css" type="text/css" media="all" /> 
	<style>
		div{
			/*border: 1px;
			border-color: red;
			border-style: solid;*/
			background-color: black;
			display:inline;
			float:left;
			color: white;

		}

		.whiteBox, .inputTitle, .inputSeparator{
                	background-color: white;
			color: black;
		}

		.blackBorder{
			border: 1px;
			background: white;
			border-color: grey;
			border-style: solid;
			border-collapse: collapse;
		}

		input[type="text"]{
                	background-color: white;
			color: black;
		}

		#mainDiv{
			float: left;
			clear: Both;
		}

		.whiteInput{
                	height: 100px;
			width: 150px;
			float: left;
		}

		.doubleWidth{
                	width:300px;
		}

		.newLine{
                	clear:left;
		}
		.bottomButtonDiv{
                	width: 160px;
			height: 150px;
		}

		#walkin{
		}

		#test{
		}

		#foc{
		}

		#lateCustomer{
			height: 70px;
		}

		#price{
			height: 70px;
		}

		#mistake{
			height: 70px;
		}

		#report{
			height: 58px;
		}

		#print{
			height: 70px;
			width: 100%;
		}
		
		#goods{
                	height:237px;
		}

		#nonJump{
                	height:237px;
		}

		.inputText{
			float: left;
                	font-size: 30px;
		}

		.inputBox{
			float: left;
                	clear: left;
		}

		.noClear{
                	clear: none;
		}

		.inputSeparator{
			float: left;
                	font-size: 30px;
		}

		.inputTitle{
                	font-size:25px
		}

		.redButton{
			background-color:red;
		} 

		.button{
			font-family: 'pussycat_snickers';
			color: black;
			border:3px;
			border-style:outset;
			border-color:#aaa;
			font-size:40px;
			float: left;
			width: 100%;
			height: 100%;
			white-space: normal;
		}

		.bottomButton{
			font-family: 'pussycat_snickers';
			color: black;
			border:3px;
			border-style:outset;
			border-color:#aaa;
			font-size:40px;
			float: left;
			white-space: normal;
                	width: 110px;
			height: 120px;
		}

		.smallerText{
			font-size: 30px;
		}

		.buttonDiv{
			background:red;
                	width: 100px;
			height: 100px;
			float: left;

			border: 1px;
			border-color: black;
			border-style: solid;
		}

		#formOuterRight input{
			width:40px;
		}

		#formOuterRight{
			width:40px;
		}

		#questionMark{
                	height: 95px;
		}

		.green{
                	background-color:#03fe00;
		}

		.blue{
                	background-color:#0000fe;
		}

		.clearBoth{
                	clear: both;
		}

		#formUpperInputs{
                	float:left;
			display:inline;
		}



	</style>

	<script>
	function silentPrintIE(){
		if (navigator.appName == "Microsoft Internet Explorer") { 
		     var PrintCommand = '<object ID="PrintCommandObject" WIDTH=0 HEIGHT=0 CLASSID="CLSID:8856F961-340A-11D0-A96B-00C04FD705A2"></object>';
		     document.body.insertAdjacentHTML('beforeEnd', PrintCommand); 
		     PrintCommandObject.ExecWB(6, -1); PrintCommandObject.outerHTML = ""; 
		} 
		else { 
			window.print();
		} 
	}
	function sleep(milliseconds) {
	  var start = new Date().getTime();
	  for (var i = 0; i < 1e7; i++) {
	    if ((new Date().getTime() - start) > milliseconds){
	      break;
	    }
	  }
	}

	function printWithCallback(callback){
		jsPrintSetup.print();
                callback();
	}

	function printTicket(){
		//Sample code which demonstrate using of JSPrintSetup to setup print margins and call unattended print method (without print dialog).
		// set portrait orientation
		jsPrintSetup.setPrinter('Canon LBP3100/LBP3108/LBP3150');
		jsPrintSetup.setOption('marginTop', 15);
		jsPrintSetup.setOption('marginBottom', 15);
		jsPrintSetup.setOption('marginLeft', 20);
		jsPrintSetup.setOption('marginRight', 10);
		// set page header
		jsPrintSetup.setOption('headerStrLeft', 'My custom header');
		jsPrintSetup.setOption('headerStrCenter', '');
		jsPrintSetup.setOption('headerStrRight', '&PT');
		// set empty page footer
		jsPrintSetup.setOption('footerStrLeft', '');
		jsPrintSetup.setOption('footerStrCenter', '');
		jsPrintSetup.setOption('footerStrRight', '');
		// Suppress print dialog

		jsPrintSetup.setSilentPrint(true);

		jsPrintSetup.printWindow(window.frames['certificate']);
		//jsPrintSetup.print();
		jsPrintSetup.setSilentPrint(false);

		//sleep(20000);
		console.log(jsPrintSetup.getPrintersList());
		setTimeout(printCertificate, 5000);
		//silentPrintFF2();

	}

        function printCertificate(){
		jsPrintSetup.refreshOptions();
		jsPrintSetup.setSilentPrint(true);

		//jsPrintSetup.setPrinter('Canon LBP3100/LBP3108/LBP3150');
	        jsPrintSetup.setPrinter('GP-80250N Series');
		// set top margins in millimeters
		jsPrintSetup.setOption('marginTop', 15);
		jsPrintSetup.setOption('marginBottom', 15);
		jsPrintSetup.setOption('marginLeft', 20);
		jsPrintSetup.setOption('marginRight', 10);
		// set page header
		jsPrintSetup.setOption('headerStrLeft', 'My custom header');
		jsPrintSetup.setOption('headerStrCenter', '');
		jsPrintSetup.setOption('headerStrRight', '&PT');
		// set empty page footer
		jsPrintSetup.setOption('footerStrLeft', '');
		jsPrintSetup.setOption('footerStrCenter', '');
		jsPrintSetup.setOption('footerStrRight', '');
		// Suppress print dialog
		jsPrintSetup.setSilentPrint(true);
		//jsPrintSetup.print();
		jsPrintSetup.printWindow(window.frames['ticket']);

		// Do Print
		// Restore print dialog
		jsPrintSetup.setSilentPrint(false);

	}
	</script>

	<script language='VBScript'>
	Sub Print()
	       OLECMDID_PRINT = 6
	       OLECMDEXECOPT_DONTPROMPTUSER = 2
	       OLECMDEXECOPT_PROMPTUSER = 1

	       
	       'Dim prt As Printer
	       'For Each prt In Printers
	       '	If prt.DeviceName = "Canon LBP3100/LBP3108/LBP3150" Then
	       '		Set Printer = prt
	       '		Exit For
	       '	End If
	       'Next
	       'Dim PrinterSettings
	       'set PrinterSettings.PrinterName = "GP-80250N Series"
	       'set PrinterSettings.DeviceName  = "GP-80250N Series"
	       'set Printer = "Canon LBP3100/LBP3108/LBP3150"
	       '\\waiver-03\printername
	       'Dim objNetwork
	       set objNetwork = CreateObject("WScript.Network")
	       set WshShell   = CreateObject("WScript.Shell")
	       'WshShell.Run "notepad"'This runs so the Wshell is working


	       'strValue = "HKCU\Software\Microsoft\Windows NT\CurrentVersion\Windows\Device"
	       'strPrinter = WshShell.RegRead(strValue)
	       'strPrinter = Split(strPrinter, ",")(0)
	       'WshShell.Popup strPrinter
	       'http://www.techrepublic.com/forums/questions/vbscript-set-default-printer/

	       'WScript.Interactive = true

	       'Crashes IE8 Debugger
	       'If objNetwork.AddWindowsPrinterConnection("\\waiver-03\GP-80250N Series") Then 
	       '        WshShell.Popup "Added Printer Connection"
	       'End If
	       'IF objNetwork.SetDefaultPrinter "\\waiver-03\GP-80250N Series" Then
	       '        WshShell.Popup "Set Default Printer"
	       'End If
	       objNetwork.AddWindowsPrinterConnection "GP-80250N Series" \\WAIVER-03
	       objNetwork.SetDefaultPrinter "Canon LBP3100/LBP3108/LBP3150"

	       strValue = "HKCU\Software\Microsoft\Windows NT\CurrentVersion\Windows\Device"
	       strPrinter = WshShell.RegRead(strValue)
	       strPrinter = Split(strPrinter, ",")(0)
	       WshShell.Popup strPrinter

	       call WB.ExecWB(OLECMDID_PRINT, OLECMDEXECOPT_DONTPROMPTUSER,1)
	End Sub
	document.write "<object ID='WB' WIDTH=0 HEIGHT=0 CLASSID='CLSID:8856F961-340A-11D0-A96B-00C04FD705A2'></object>"
	</script>
	
</head>


	<body>
	<iframe name="ticket" src="ticket.html"></iframe>
	<iframe name="certificate" src="certificate.html" style="display:none;"></iframe>
	<input type="submit" class="redButton button" onClick="printTicket();"  value="Print">
	</body>
</html>
