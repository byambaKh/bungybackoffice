<?

date_default_timezone_set("Asia/Tokyo");
$weight = file_get_contents("/home/bungyjapan/private/weight.dat");
$updated = filemtime("/home/bungyjapan/private/weight.dat");
$current = ($updated >= time() - 4);

?>
<!DOCTYPE html>
<html>
<head>
	<title>Scale Reader App</title>
	<style>
	body { font-family: sans-serif; text-align: center; }
	</style>
</head>
<body>
<h1>Weight: <?= $weight ?></h1>
<? if ($current) : ?>
	<h4 style="color: green;">Last updated on <?= date("Y-m-d H:i:s", $updated) ?>.</h4>
<? else : ?>
	<h4 style="color: red;">This is an old reading (from <?= date("Y-m-d H:i:s", $updated) ?>).<br/>The scale app has not checked in recently.</h4>
<? endif ?>
<button onclick="window.location.reload();">Reload</button>
</body>
</html>
