<?php
	include '../includes/application_top.php';
	// load all merchandise items

	$sql = "select * from merchandise_items WHERE site_id = '".CURRENT_SITE_ID."' ORDER by sort_order asc, name ASC;";
	$res = mysql_query($sql) or die(mysql_error());
	$items = array();
	while ($row = mysql_fetch_assoc($res)) {
		$items[$row['id']] = $row;
	};

	// action performing
	$action = '';
	if (array_key_exists('action', $_POST)) {
		$action = $_POST['action'];
	};

	switch (TRUE) {
		case ($action == 'save'):
		case ($action == 'Save Sale'):
			$total = 0;
			$total_qty = 0;
			$sale_id = 0;
			//create an empty array
			$totals = array(
				'sale_total_tshirt'		=> 0,
				'sale_total_qty_tshirt'		=> 0,
				'sale_total_2nd_jump'		=> 0,
				'sale_total_qty_2nd_jump'	=> 0,
				'sale_total_photo'		=> 0,
				'sale_total_qty_photo'		=> 0,
			);

			//POST Array
			//(
			//    [tshirt] => Array
			//        (
			// 	  [59] => 0, [60] => 0, [187] => 0, [117] => 1, [118] => 1, [119] => 1,...
			//        )
			//
			//    [second_jump] => Array
			//        (
			//        [59] => 1, [60] => 0, [187] => 0, [117] => 0, [118] => 0, [119] => 0,...
			//        )
			//
			//    [photo] => Array
			//        (
			//        [59] => 0, [60] => 1, [187] => 0, [117] => 0, [118] => 0 ...
			//        )
			//
			//    [quantity] => Array
			//        (
			// 	[59] => 0, [60] => 0, [187] => 0, [117] => 6, [118] => 0 ...
			//        )
			//
			//    [sale_total] => 0
			//    [cash_received] => 0
			//    [change] => 0
			//    [action] => save
			//)

			//Each row in the form is an input of name quantity[number] which is submitted to php as
			//an array. In this case it is an array in $_POST['quantity'];
			//Example entry
			//[59] => Array
			//( [id] => 59 ,[site_id] => 2 ,[name] => 2nd Jump - 5000 ,[price] => 5000 ,[color] => silver ,[sort_order] => 1)

			foreach ($_POST['quantity'] as $id => $qty) {
				if ($qty > 0) {
					//$items is an array of all of the items with their prices
					//it is generated from the database, at the top of this file.
					//id (the key in post quantity looks up the corresponding item price)
					//$_POST['quantity'] looks like this
					//Array
					//( [59] => 6 ,[60] => 0 ,[187] => 0 ,[117] => 0 ,[118] => 0 ,[119] => 0 ...)
					//when $id = 59 $qty = 6
					//it tells us how much of each item is bought
					$total += $qty * $items[$id]['price']; //quantity * price is the total for that item
					$total_qty += $qty;//an aggregate sum of the quantity of items bought

					if (!$sale_id) {//this is initially zero but is set on line 98
						//it is the id of the sale and is the same for all items in the sale
						// creating sale in DB
						$data = array(
							'sale_time'			=> date("Y-m-d H:i:s"),
							'sale_total_tshirt'		=> 0,
							'sale_total_qty_tshirt'		=> 0,
							'sale_total_2nd_jump'		=> 0,
							'sale_total_qty_2nd_jump'	=> 0,
							'sale_total_photo'		=> 0,
							'sale_total_qty_photo'		=> 0,
							'sale_total'			=> 0,
							'sale_total_qty'		=> 0
						);
						$res = db_perform('merchandise_sales', $data);
						//inserts a blank row with a date into the db that will be updated 
						//this will be updated
						$sale_id = mysql_insert_id();
					};
					$data = array(
						'sales_id'	=> $sale_id,
						'item_id'	=> $id,
						'item_price'=> $items[$id]['price'],
						'quantity'	=> $qty
					);
					db_perform('merchandise_sales_items', $data);
					//insert the items and sales into merchandise_sales_items which is different from merchandise_sales!!!
					//Why are both of these stored?
					//Note merchandise_items is not edited as it is just a list of items and prices

					//    [tshirt] => Array
					//        (
					// 	  [59] => 0, [60] => 0, [187] => 0, [117] => 1, [118] => 1, [119] => 1,...
					//        )
					//In each item is a hidden input that represents the category the item belongs to
					//the input is set to 1 if it belongs or zero if it does not
					//
					//for example the second jump item at the top of the page includes
					//<input type='hidden' name='tshirt[59]' value="0">
					//<input type='hidden' name='second_jump[59]' value="1">
					//<input type='hidden' name='photo[59]' value="0">
					//the "Shirt - Girl - Black - XS has
					//<input type='hidden' name='tshirt[118]' value="1">
					//<input type='hidden' name='second_jump[118]' value="0">
					//<input type='hidden' name='photo[118]' value="0">
					//where the id number corresponds to the id of the item in question (the black xs girl shirt in the example)
					//These are added up in a separate total to the merchandise_sales which is a sum of 3 items
					//Shirts, 2nd jumps and photos
					//merchandise_sale_items has more information

					//A better solution would be to store everything in one table and have a category for each sale
					//sale, jump, shirt, food, and sum things upon retreval
					if ($_POST['tshirt'][$id]) {
						$totals['sale_total_tshirt'] += $items[$id]['price'] * $qty;
						$totals['sale_total_qty_tshirt'] += $qty;
					};
					if ($_POST['second_jump'][$id]) {
						$totals['sale_total_2nd_jump'] += $items[$id]['price'] * $qty;
						$totals['sale_total_qty_2nd_jump'] += $qty;
					};
					if ($_POST['photo'][$id]) {
						$totals['sale_total_photo'] += $items[$id]['price'] * $qty;
						$totals['sale_total_qty_photo'] += $qty;
					};
				};
			};

			if ($sale_id) {
				//insert the calculated total quantities and total sales into the merchandise_sales table
				//These values come from the hidden field data
				$data = array(
					'site_id'			=> CURRENT_SITE_ID,
					'sale_total_tshirt'		=> $totals['sale_total_tshirt'],
					'sale_total_qty_tshirt'		=> $totals['sale_total_qty_tshirt'],
					'sale_total_2nd_jump'		=> $totals['sale_total_2nd_jump'],
					'sale_total_qty_2nd_jump'	=> $totals['sale_total_qty_2nd_jump'],
					'sale_total_photo'		=> $totals['sale_total_photo'],
					'sale_total_qty_photo'		=> $totals['sale_total_qty_photo'],
					'sale_total'			=> $total,
					'sale_total_qty'		=> $total_qty
				);
				$res = db_perform('merchandise_sales', $data, 'update', 'id=' . $sale_id);
				$_SESSION['sale_message'] = "New sale created. Total is &yen; $total ($total_qty item(s))";
				Header("Location: dailyViewIE.php");
				die();
			};
			$_SESSION['sale_error'] = "No sale created.";
			Header("Location: merchandise.php");
			die();
			break;
	};
	
?>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8" />
<?php include '../includes/head_scripts.php'; ?>
<style>
h1 {
	text-align: center;
}
#merchandise-items {
	font-family: Verdana;
	background-color: gray;
	width: 800px;
}
#merchandise-items th {
	padding: 10px;
	background-color: #bbf;
}
#merchandise-items td {
	background-color: silver;
	padding: 5px;
	font-size: 14px;
}
.even td {
	background-color: #afa !important;
}
.item-id {
	text-align: center;
	width: 50px;
}
.item-price {
	text-align: right;
	width: 110px;
}
.item-qty {
	width: 100px !important;
}
#total-cell, #cash-cell, #change-cell {
	text-align: right;
	font-size: 14px !important;
	font-weight: bold;
}
input, select {
	width: 100px;
	text-align: center;
	font-size: 14px;
	padding: 5px;
}
#sale-total {
	font-weight: bold;
}
#cash-received {
	font-weight: bold;
	color: green;
}
#change {
	color: red;
	font-weight: bold;
}
#buttons-container {
	background-color: lightgreen;
	width: 800px;
	border-radius: 10px;
	-ms-border-radius: 10px;
	padding: 5px;
	border: 1px solid gray;
	margin-top: 20px;
	text-align: center;
	margin-left: auto;
	margin-right: auto;
	box-sizing: border-box;
	-moz-box-sizing: border-box;
	height: 46px;
}
#buttons-container button {
	border: 1px solid silver;
	padding: 5px;
	border-radius: 10px;
	font-size: 16px;
	cursor: pointer;
}
#save {
	float: right;
	background-color: red;
	color: white;
	font-weight: bold;
}
#back {
	float: left;
	background-color: silver;
}
#sale_error {
	text-align: center;
	background-color: red;
	font-family: Verdana;
	font-size: 14px;
	font-weight: bold;
	padding: 10px;
	-moz-border-radius: 10px;
	border-radius: 10px;
}
</style>

<title><?php echo SYSTEM_SUBDOMAIN; ?> Merchandise</title>
<script>
$(document).ready(function () {
	$('#sale_error').delay(2000).slideUp(500);
	$(".quantity").change(function () {
		update_totals();
	});
	$("#sale-total, #cash-received").keyup(function () {
		update_change();
	});
	$("#back").click(function () {
		document.location = 'dailyViewIE.php';
		return false;
	});
});
function update_change() {
		try {
			$("#change").val(
				parseInt($("#cash-received").val())
				-
				parseInt($("#sale-total").val())
			);
		} catch (e) {
			$("#change").val('N/A');
		};
}
function update_totals() {
	var total = 0;
	try {
		$(".quantity").each(function () {
			total += parseInt($(this).val()) * parseInt($(this).attr('custom-price'));
		});
		$("#sale-total").val(total);
		update_change();
	} catch (e) {
		$("#sale-total").val('N/A');
	};
}
</script>
</head>
<body>
<?php 
	if (array_key_exists('sale_error', $_SESSION)) {
		echo "<div id='sale_error'>{$_SESSION['sale_error']}</div>";
		unset($_SESSION['sale_error']);
	};
?>
<form method="POST">
<h1> <?php echo SYSTEM_SUBDOMAIN; ?> Merchandise</h1>
<table border="0" cellspacing="1" cellpadding="3" id="merchandise-items" align=center>
<tr>
	<th>Item Name</th>
	<th>Item Price</th>
	<th>QTY</th>
</tr>
<?php
	$i = 0;
	foreach ($items as $item) {
		$i = !$i;
?>
<tr class="<?php echo ($i) ? 'even':'odd'; ?>"<?php if (!empty($item['color'])) echo ' style="background-color: ' . $item['color'] . ';"'; ?>>
	<td class="item-name"<?php if (!empty($item['color'])) echo ' style="background-color: ' . $item['color'] . ' !important;"'; ?>>
		<?php echo $item['name']; ?>
		<input type='hidden' name='tshirt[<?php echo $item['id']; ?>]' value="<?php echo preg_match('/shirt/i', $item['name']); ?>">
		<input type='hidden' name='second_jump[<?php echo $item['id']; ?>]' value="<?php echo preg_match('/2nd jump/i', $item['name']); ?>">
		<input type='hidden' name='photo[<?php echo $item['id']; ?>]' value="<?php echo preg_match('/photo pack/i', $item['name']); ?>">
	</td>
	<td class="item-price"<?php if (!empty($item['color'])) echo ' style="background-color: ' . $item['color'] . ' !important;"'; ?>>&yen; <?php echo $item['price']; ?></td>
	<td class="item-qty"<?php if (!empty($item['color'])) echo ' style="background-color: ' . $item['color'] . ' !important;"'; ?>><select name="quantity[<?php echo $item['id']; ?>]" class="quantity" custom-price="<?php echo $item['price']; ?>">
<?php 
	for ($j = 0; $j <= 10; $j++) {
		echo "<option value='$j'>$j</option>";
	};
?>
	</select>
	</td>
</tr>
<?php
	};
?>
<tr>
	<td colspan="2" id="total-cell">Sale Total: &yen;</td>
	<td><input name="sale_total" value="0" id="sale-total"></td>
</tr>
<tr>
	<td colspan="2" id="cash-cell">Amount Paid: &yen;</td>
	<td><input name="cash_received" value="0" id="cash-received"></td>
</tr>
<tr>
	<td colspan="2" id="change-cell">Change: &yen;</td>
	<td><input name="change" value="0" id="change"></td>
</tr>
</table>
<div id="buttons-container">
	<button type="submit" name='action' value="save" id="save">Save Sale</button>
	<button id="back">Back</button>
</div>
</form>
</body>
</html>
