<?php
include('../includes/application_top.php');
include('../Reception/checkin_config.php');

/**
 * @return bool|string
 */
function getDate_()
{
    $cDate = date("Y-m-d");
    if (isset($_GET['date'])) {
        $cDate = $_GET['date'];

        return $cDate;
    }

    return $cDate;
}

/**
 * @param $types
 * @param $cDate
 */
function handlePOST($types, $cDate)
{
// cash-calculator actions
    $action = $_POST['action'];
    switch ($action) {
        case 'save':
        case 'Save':
            foreach ($types as $type) {
                $lower_type = strtolower($type);
                $action = 'insert';
                $where = '';
                // check if record exists
                $sql = "SELECT * FROM cash_calculator WHERE site_id = '" . CURRENT_SITE_ID . "' AND calculator_date = '$cDate' and cash_type = '$lower_type'";
                $res = mysql_query($sql) or die(mysql_error());
                if ($row = mysql_fetch_assoc($res)) {
                    $action = 'update';
                    $where = 'id = ' . $row['id'];
                };
                $data = array(
                    'site_id'         => CURRENT_SITE_ID,
                    'calculator_date' => $cDate,
                    'cash_type'       => $lower_type,
                    'cash_data'       => serialize($_POST[$lower_type])
                );
                db_perform('cash_calculator', $data, $action, $where);
            };
            $add_query = '';
            if ($cDate != date("Y-m-d")) {
                $add_query = '?date=' . urlencode($cDate);
            };
            Header("Location: daily_sales_report.php" . $add_query);
            exit();
            break;
    };
}

/**
 * @param $cDate
 * @param $result
 * @param $totals
 *
 * @return array
 */
function calculateOnsiteBookings($cDate, $result, $totals)
{
    $sql = "SELECT
			Rate, sum(NoOfJump) as qty, sum(Rate * NoOfJump) as total
		FROM customerregs1
		WHERE
			site_id = '" . CURRENT_SITE_ID . "'
			and BookingDate = '$cDate'
			and deletestatus = 0
			and Checked = 1
			and CollectPay = 'Onsite'
		GROUP BY Rate
		ORDER BY Rate DESC;";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        $result['a']['' . $row['Rate']] = $row;
        $totals['a']['qty'] += $row['qty'];
        $totals['a']['total'] += $row['total'];
    };
    mysql_free_result($res);

    return array($totals, $result);
}

/**
 * @param $config
 * @param $cDate
 * @param $result
 * @param $totals
 *
 * @return array
 */
function calculateGoods($config, $cDate, $result, $totals)
{
// GOODS
    // total Other goods from customerregs
    $sql = "SELECT 'Other' as Rate, sum(other / 100) as qty, sum(other) as total, SUM(2ndj / {$config['second_jump_rate']}) as total_2ndj FROM customerregs1 WHERE
			site_id = '" . CURRENT_SITE_ID . "'
            AND BookingDate = '$cDate'
            AND deletestatus = 0
            AND Checked = 1
            AND NoOfJump > 0
		GROUP BY BookingDate;";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        if ($row['qty'] > 0) {
            $row['qty'] = (int)$row['qty'];
            $result['b'][] = $row;
            $totals['b']['qty'] += $row['qty'];
            $totals['b']['total'] += $row['total'];
        };
        if ($row['total_2ndj'] > 0) {
            if (!array_key_exists($config['second_jump_rate'], $result['a'])) {
                $result['a'][$config['second_jump_rate']] = array(
                    'Rate'  => $config['second_jump_rate'],
                    'qty'   => 0,
                    'total' => 0
                );
            };
            // add 2nd jump merchanfise values
            $result['a'][$config['second_jump_rate']]['qty'] += $row['total_2ndj'];
            $result['a'][$config['second_jump_rate']]['total'] += $row['total_2ndj'] * $config['second_jump_rate'];
            $totals['a']['qty'] += $row['total_2ndj'];
            $totals['a']['total'] += $row['total_2ndj'] * $config['second_jump_rate'];
        };
    };
    mysql_free_result($res);

    return array($totals, $result);
}

/**
 * @param $cDate
 * @param $result
 * @param $totals
 * @param $config
 *
 * @return array
 */
function calculateMerchandiseGoods($cDate, $result, $totals, $config)
{
// Merchandise GOODS
    $sql = "SELECT
		DATE(sale_time) as sale_date,
		sum(sale_total) as total,
		sum(sale_total_qty) as qty,
		sum(sale_total_tshirt) as total_tshirt,
		sum(sale_total_qty_tshirt) as qty_tshirt,
		sum(sale_total_2nd_jump) as total_2nd_jump,
		sum(sale_total_qty_2nd_jump) as qty_2nd_jump,
		sum(sale_total_photo) as total_photo,
		sum(sale_total_qty_photo) as qty_photo
	FROM merchandise_sales
	WHERE
		site_id = '" . CURRENT_SITE_ID . "'
		and sale_time like '$cDate%'
	GROUP BY sale_date;";
    $res = mysql_query($sql) or die(mysql_error());
    $onsite_rates = array();
    while ($row = mysql_fetch_assoc($res)) {
        // t-shirt
        $item_name = 'Photos (500)';
        if (!array_key_exists($item_name, $result['c'])) {
            $result['a'][$item_name] = array(
                'Rate'       => $item_name,
                'qty'        => 0,
                'total'      => 0,
                'total_real' => 0
            );
        };
        $row['qty_photo'] = (int)$row['qty_photo'];
        $result['a'][$item_name]['qty'] += $row['qty_photo'];
        $result['a'][$item_name]['total'] += $row['qty_photo'] * 500;
        $result['a'][$item_name]['total_real'] += $row['total_photo'];
        $totals['a']['qty'] += $row['qty_photo'];
        $totals['a']['total'] += $row['qty_photo'] * 500;
        if ($row['qty'] - $row['qty_2nd_jump'] - $row['qty_tshirt'] - $row['qty_photo'] > 0) {
            if (!array_key_exists('Other', $result['b'])) {
                $result['b']['Other'] = array(
                    'Rate'  => 'Other',
                    'qty'   => 0,
                    'total' => 0
                );
            };
            $result['b']['Other']['qty'] += $row['qty'] - $row['qty_2nd_jump'] - $row['qty_tshirt'] - $row['qty_photo'];
            $result['b']['Other']['total'] += $row['total'] - $row['total_2nd_jump'] - $row['total_tshirt'] - $row['total_photo'];
        };
        $totals['b']['qty'] += $row['qty'] - $row['qty_2nd_jump'] - $row['qty_photo'];
        $totals['b']['total'] += $row['total'] - $row['total_2nd_jump'] - $row['total_photo'];
        // add 2nd jumps to a section
        if ($row['qty_2nd_jump'] > 0) {
            // if there is no 2nd jump rate jumps, create placeholder
            if (!array_key_exists($config['second_jump_rate'], $result['a'])) {
                $result['a'][$config['second_jump_rate']] = array(
                    'Rate'  => $config['second_jump_rate'],
                    'qty'   => 0,
                    'total' => 0
                );
            };
            // add 2nd jump merchandise values
            $result['a'][$config['second_jump_rate']]['qty'] += $row['qty_2nd_jump'];
            $result['a'][$config['second_jump_rate']]['total'] += $row['total_2nd_jump'];
            $totals['a']['qty'] += $row['qty_2nd_jump'];
            $totals['a']['total'] += $row['total_2nd_jump'];
        };
    };
    mysql_free_result($res);

    return array($totals, $result);
}

/**
 * @param $cDate
 * @param $result
 *
 * @return array
 */
function calculateTShirtSalesWithDifferentPrices($cDate, $result)
{
// tshirts from sales with different prices
    $sql = "SELECT DATE(ms.sale_time) as sales_date, msi.item_price, SUM(msi.quantity) total_qty
            FROM merchandise_items mi, merchandise_sales ms, merchandise_sales_items msi
            WHERE
                mi.name LIKE 'Shirt%'
            AND mi.id = msi.item_id
            AND msi.sales_id = ms.id
            AND ms.site_id = '" . CURRENT_SITE_ID . "'
            AND ms.sale_time LIKE '$cDate%'
            GROUP BY sales_date, item_price;";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        $price_tshirt = sprintf("%d", $row['item_price']);
        $item_name = 'T-Shirts (' . $price_tshirt . ')';
        if (!array_key_exists($item_name, $result['b'])) {
            $result['b'][$item_name] = array(
                'Rate'  => $item_name,
                'qty'   => 0,
                'total' => 0
            );
        };
        $result['b'][$item_name]['qty'] += $row['total_qty'];
        $result['b'][$item_name]['total'] += $row['total_qty'] * $price_tshirt;
    };
    krsort($result['a'], SORT_NUMERIC);

    return array($result);
}

/**
 * @param $cDate
 * @param $result
 * @param $totals
 *
 * @return array
 */
function calculateOnsiteCancellations($cDate, $result, $totals)
{
// onsite cancellation
    $sql = "SELECT
			'Cancellation' as Rate, sum(CancelFeeQTY) as qty, sum(CancelFee * CancelFeeQTY) as total
		FROM customerregs1
		WHERE
			site_id = '" . CURRENT_SITE_ID . "'
			AND BookingDate = '$cDate'
			AND CancelFeeQTY > 0
			AND CancelFee > 0
			AND Checked = 1
			AND CancelFeeCollect = 'Onsite'
		    AND NoOfJump > 0
		GROUP BY BookingDate;
	";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        $result['a'][] = $row;
        $totals['a']['qty'] += $row['qty'];
        $totals['a']['total'] += $row['total'];
    };
    mysql_free_result($res);

    return array($totals, $result);
}


/**
 * @param $cDate
 * @param $result
 * @param $totals
 *
 * @return array
 */
function calculateOffsiteJumps($cDate, $result, $totals)
{
// OFFSITE
    // Get total by Rates
    $sql = "SELECT
            Rate, sum(NoOfJump) as qty, sum(Rate * NoOfJump) as total
        FROM customerregs1
        WHERE
			site_id = '" . CURRENT_SITE_ID . "'
            and BookingDate = '$cDate'
            and deletestatus = 0
            and Checked = 1
            and CollectPay = 'Offsite'
        GROUP BY Rate
        ORDER BY Rate DESC;";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        $result['c']['' . $row['Rate']] = $row;
        $totals['c']['qty'] += $row['qty'];
        $totals['c']['total'] += $row['total'];
    };
    mysql_free_result($res);

    return array($totals, $result);
}

/**
 * @param $cDate
 * @param $result
 * @param $totals
 *
 * @return array
 */
function calculateTotalPhotosVideosGoPro($cDate, $result, $totals)
{
// total photos/videos/gopro to collect
    $sql = "SELECT 'Photos (500)' as Rate, sum(photos_qty + video_qty + gopro_qty) as qty, sum(photos_qty + video_qty + gopro_qty) * 500 as total, sum(photos + video+gopro) as total_real FROM customerregs1 WHERE
			site_id = '" . CURRENT_SITE_ID . "'
            and BookingDate = '$cDate'
            and deletestatus = 0
            and Checked = 1
			and GroupBooking < 2
		GROUP BY BookingDate;";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        if ($row['qty'] > 0) {
            $row['qty'] = (int)$row['qty'];
            if (!array_key_exists($row['Rate'], $result['a'])) {
                $result['a'][$row['Rate']] = array(
                    'Rate'       => $row['Rate'],
                    'qty'        => 0,
                    'total'      => 0,
                    'total_real' => 0,
                );
            };
            $row['qty_photo'] = (int)$row['qty_photo'];
            $result['a'][$row['Rate']]['qty'] += $row['qty'];
            $result['a'][$row['Rate']]['total'] += $row['total'];
            $result['a'][$row['Rate']]['total_real'] += $row['total_real'];
            $totals['a']['qty'] += $row['qty'];
            $totals['a']['total'] += $row['total'];
        };
    };
    mysql_free_result($res);

    return array($totals, $result);
}

/**
 * @param $cDate
 * @param $result
 * @param $totals
 *
 * @return array
 */
function tshirtsFromCustomerRegs1($cDate, $result, $totals)
{
// total tshirt from customerregs1 to collect
    $sql = "SELECT tshirt / tshirt_qty as Rate, sum(tshirt_qty) as qty, sum(tshirt) as total FROM customerregs1 WHERE
			site_id = '" . CURRENT_SITE_ID . "'
            and BookingDate = '$cDate'
            and deletestatus = 0
            and Checked = 1
			and tshirt_qty > 0
		GROUP BY BookingDate, Rate;";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        if ($row['qty'] > 0) {
            $row['qty'] = (int)$row['qty'];
            //$tshirt_price = $row['total'] / $row['qty'];
            $tshirt_price = sprintf("%d", $row['Rate']);
            $row['Rate'] = 'T-Shirts (' . $tshirt_price . ')';
            if (!array_key_exists($row['Rate'], $result['b'])) {
                $result['b'][$row['Rate']] = array(
                    'Rate'  => $row['Rate'],
                    'qty'   => 0,
                    'total' => 0,
                );
            };
            $result['b'][$row['Rate']]['qty'] += $row['qty'];
            $result['b'][$row['Rate']]['total'] += $row['total'];
            $totals['b']['qty'] += $row['qty'];
            $totals['b']['total'] += $row['total'];
        };
    };
    mysql_free_result($res);

    return array($totals, $result);
}

/**
 * @param $cDate
 * @param $result
 * @param $totals
 *
 * @return array
 */
function offsiteCancellation($cDate, $result, $totals)
{
//Offsite cancellation
    $sql = "SELECT
			'Cancellation' as Rate, sum(CancelFeeQTY) as qty, sum(CancelFee * CancelFeeQTY) as total
		FROM customerregs1
		WHERE
			site_id = '" . CURRENT_SITE_ID . "'
			and BookingDate = '$cDate'
			and CancelFeeQTY > 0
			and CancelFee > 0
			#and Checked = 1
			and CancelFeeCollect = 'Offsite'
		GROUP BY BookingDate;
	";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        $result['c'][] = $row;
        $totals['c']['qty'] += $row['qty'];
        $totals['c']['total'] += $row['total'];
    };
    mysql_free_result($res);

    return array($totals, $result);
}

/**
 * @param $cDate
 * @param $totals
 *
 * @return array
 */
function offsiteToPayTotal($cDate, $totals)
{
// OFFSITE
// TO PAY TOTAL
    $sql = "SELECT
			RateToPayQTY as qty,
			RateToPay * RateToPayQTY as total,
			customerregs1.*
		FROM customerregs1
		WHERE
			site_id = '" . CURRENT_SITE_ID . "'
            and BookingDate = '$cDate'
            and deletestatus = 0
			and RateToPay > 0
			and RateToPayQTY > 0
			and Checked = 1
            and CollectPay = 'Onsite'
			and GroupBooking < 2
		/*GROUP BY BookingDate;*/";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        if ($row['total'] > 0) {
            $totals['d']['qty'] += $row['qty'];
            $totals['d']['total'] += $row['total'];
        };
    };
    mysql_free_result($res);

    return array($totals);
}


$cDate = getDate_();
// calculator values
$nom = array(10000, 5000, 1000, 500, 100, 50, 10, 5, 1);
$types = array("OPEN", "CLOSE");

handlePOST($types, $cDate);


// totals placeholder
$totals = [
    'a' => ['title' => 'Onsite Total', 'qty' => 0, 'total' => 0],
    'b' => ['title' => 'Goods Total', 'qty' => 0, 'total' => 0],
    'c' => ['title' => 'Offsite Total', 'qty' => 0, 'total' => 0],
    'd' => ['title' => 'D. To Pay Total', 'qty' => 0, 'total' => 0]
];
$result = [];
$result['a'] = [];
$result['b'] = [];
$result['c'] = [];

// Get total by Rates
list($totals, $result) = calculateOnsiteBookings($cDate, $result, $totals);
list($totals, $result) = calculateGoods($config, $cDate, $result, $totals);
list($totals, $result) = calculateMerchandiseGoods($cDate, $result, $totals, $config);
list($result) = calculateTShirtSalesWithDifferentPrices($cDate, $result);
list($totals, $result) = calculateOnsiteCancellations($cDate, $result, $totals);
list($totals, $result) = calculateOffsiteJumps($cDate, $result, $totals);
list($totals, $result) = calculateTotalPhotosVideosGoPro($cDate, $result, $totals);
list($totals, $result) = tshirtsFromCustomerRegs1($cDate, $result, $totals);
list($totals, $result) = offsiteCancellation($cDate, $result, $totals);
list($totals) = offsiteToPayTotal($cDate, $totals);


//Difference = Balance - Receipts - Close Balance - Open Balance
// select cash calculator values
$all_values = array();
$sql = "SELECT * FROM cash_calculator WHERE site_id = '" . CURRENT_SITE_ID . "' and calculator_date = '$cDate';";
$res = mysql_query($sql) or die(mysql_error());
while ($row = mysql_fetch_assoc($res)) {
    if (!empty($row['cash_data'])) {
        $all_values[$row['cash_type']] = unserialize($row['cash_data']);
    };
};
foreach ($types as $type) {
    $lower_type = strtolower($type);
    if (!array_key_exists($lower_type, $all_values)) {
        $all_values[$lower_type] = array();
    };
    foreach ($nom as $nominal) {
        if (!array_key_exists($nominal, $all_values[$lower_type])) {
            $all_values[$lower_type][$nominal] = 0;
        };
    };
};
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?= SYSTEM_SUBDOMAIN; ?> - Daily Sales Report</title>
    <?php include "../includes/head_scripts.php"; ?>
    <style>
        @media print {
            .not-printed, .not-printed * {
                display: none !important;
            }
        }

        #sales-report {
            border-collapse: collapse;
            width: 250px;
            margin-left: 2px;
            font-family: Arial;
            float: left;
        }

        .border-bottom th {
            border-bottom: 1px solid black;
        }

        .left {
            text-align: left !important;
        }

        .right {
            text-align: right !important;
        }

        .separator td {
            height: 10px;
        }

        .section-header th {
            border: 2px solid black;
        }

        .section-item td {
            border: 1px solid black;
            vertical-align: middle;
            text-align: center;
            padding-left: 3px;
            padding-right: 3px;
            font-size: 12px;
        }

        .first-cell {
            border-left: 2px solid black !important;
        }

        .last-cell {
            border-right: 2px solid black !important;
        }

        .section-item td.currency {
            border-right: none !important;
        }

        .section-item td.currency-value {
            border-left: none !important;
        }

        .section-item td.xxx {
            font-size: 10px;
        }

        .section-footer th, .section-footer td {
            border-top: 2px solid black;
            border-bottom: 2px solid black;
            padding-left: 3px;
            padding-right: 3px;
            font-size: 12px;
        }

        .big td, .big th {
            padding-top: 3px !important;
            padding-bottom: 3px !important;
        }

        .notes td {
            padding-bottom: 100px !important;
        }

        #cash-calculator {
            float: left;
            width: 250px;
            margin-left: 50px;
            border-collapse: collapse;
        }

        .cash-section-header td, .cash-section-header th {
            border-bottom: 2px solid black !important;
            border-top: 2px solid black !important;
        }

        #cash-calculator td, #cash-calculator th {
            border: 1px solid black;
        }

        .cash-section-header th.first {
            width: 70px;
            border-left: 2px solid black !important;
            border-right: 2px solid black !important;
        }

        .cash-section-header th.second {
            width: 85px;
            max-width: 85px;
        }

        .cash-section-header th.third {
            width: 85px;
            border-right: 2px solid black !important;
        }

        .second input {
            width: 99%;
            border: 0px;
            text-align: center;
        }

        td.first {
            text-align: right;
            padding-right: 5px;
            border-left: 2px solid black !important;
            border-right: 2px solid black !important;
        }

        td.third {
            width: 85px;
            text-align: right;
            padding-right: 5px;
            border-right: 2px solid black !important;
        }

        .balance-row th {
            border-bottom: 2px solid black !important;
        }

        .balance-row th.first {
            border-left: 2px solid black !important;
            border-right: 2px solid black !important;
        }

        .balance-row th.second {
            text-align: left;
            padding-left: 5px;
            border-right: none !important;
        }

        .balance-row th.third {
            text-align: right;
            padding-right: 5px;
            border-left: none !important;
            border-right: 2px solid black !important;
        }

        .difference-row th {
            border-bottom: 2px solid black !important;
            border-top: 2px solid black !important;
        }

        .difference-row th.second {
            text-align: left;
            padding-left: 5px;
            border-right: none !important;
        }

        .difference-row th.first {
            border-left: 2px solid black !important;
            border-right: 2px solid black !important;
        }

        .photo-row th.first {
            border-left: 2px solid black !important;
            border-right: 2px solid black !important;
        }

        .receipts-row th.first {
            border-left: 2px solid black !important;
            border-right: 2px solid black !important;
        }

        .difference-row th.third {
            text-align: right;
            padding-right: 5px;
            border-left: none !important;
            border-right: 2px solid black !important;
        }

        .photo-row th.third {
            text-align: right;
            padding-right: 5px;
            border-left: none !important;
            border-right: 2px solid black !important;
        }

        .receipts-row th.third {
            text-align: right;
            padding-right: 5px;
            border-left: none !important;
            border-right: 2px solid black !important;
        }

        .actions-row td {
            text-align: center !important;
            border: none !important;
        }
    </style>
    <script>
        $(document).ready(function () {
            update_calculator();
            $(".values").on('keyup', update_calculator);
        });
        function update_calculator() {
            var nom = <?=json_encode($nom); ?>;
            var types = <?=json_encode(array_map('strtolower', $types)); ?>;
            var error_happen = false;
            var total = 0;
            var grand_total = 0;
            for (var i in types) {
                if (!types.hasOwnProperty(i)) continue;
                total = 0;
                for (var j in nom) {
                    if (!nom.hasOwnProperty(j)) continue;
                    var nominal = $('#' + types[i] + nom[j]).parent().parent().attr('custom-value');
                    if (typeof nominal != 'undefined') {
                        var val = $('#' + types[i] + nom[j]).val();
                        if (val == '') val = 0;
                        try {
                            val = parseInt(val);
                            $('#' + types[i] + nom[j] + '-total').html(val * nominal);
                            total += val * nominal;
                        } catch (e) {
                            error_happen = true;
                            $('#' + types[i] + nom[j] + '-total').html('N/A');
                        }
                    }
                }
                $('#' + types[i] + '-total').html(total);
                if (types[i] == 'open') {
                    grand_total -= total;
                } else {
                    grand_total += total;
                }
            }
            grand_total -= parseInt($('#photo-total').html());
            grand_total -= parseInt($('#receipt-total').html());
            $('#difference-total').html(grand_total);
        }
    </script>
</head>
<body>
<table id="sales-report">
    <tr class="border-bottom">
        <th colspan="3" class="left">Site</th>
        <th colspan="3"><?= SYSTEM_SUBDOMAIN; ?></th>
    </tr>
    <tr class="border-bottom">
        <th colspan="3" class="left">Date</th>
        <th colspan="3"><?= str_replace('-', '.', $cDate); ?></th>
    </tr>
    <tr class="border-bottom">
        <th colspan="3" class="left">Name</th>
        <th colspan="3">&nbsp;</th>
    </tr>
    <tr class="border-bottom">
        <th colspan="3" class="left">Total (A+B+D)</th>
        <th colspan="3" class="right">&yen;<?= number_format($totals['a']['total'] + $totals['b']['total'] + $totals['d']['total'], 0, '.', ' '); ?></th>
    </tr>
    <tr class="separator">
        <td colspan="6" class="left">&nbsp;</td>
    </tr>
    <tr class="section-header big">
        <th colspan="6">A. Bungy - ONSITE</th>
    </tr>
    <?php
    foreach ($result['a'] as $name => $row) {
        ?>
        <tr class="section-item">
            <td class="first-cell currency">&yen;</td>
            <td class="right currency-value"><?= ($row['Rate']) ? (intval($row['Rate']) != 0 ? number_format($row['Rate'], 0, '.', ' ') : $row['Rate']) : "FOC ( {$row['Rate']} )"; ?></td>
            <td class="xxx">X</td>
            <td class="right"><?= $row['qty']; ?></td>
            <td class="currency">&yen;</td>
            <td class="last-cell right currency-value"><?= ($row['total']) ? number_format($row['total'], 0, '.', ' ') : '--'; ?></td>
        </tr>
        <?php
    }
    ?>
    <tr class="section-footer">
        <th colspan="3" class="first-cell left"><?= $totals['a']['title']; ?></th>
        <th class="right"><?= $totals['a']['qty']; ?></th>
        <th class="">&yen;</th>
        <th class="last-cell right"><?= number_format($totals['a']['total'], 0, '.', ' '); ?></th>
    </tr>
    <tr class="separator">
        <td colspan="6" class="left">&nbsp;</td>
    </tr>
    <tr class="section-header big">
        <th colspan="6">B. GOODS</th>
    </tr>
    <?php
    foreach ($result['b'] as $name => $row) {
        ?>
        <tr class="section-item">
            <td class="first-cell currency">&yen;</td>
            <td class="right currency-value"><?= ($row['Rate']) ? (intval($row['Rate']) != 0 ? number_format($row['Rate'], 0, '.', ' ') : $row['Rate']) : "FOC ( {$row['Rate']} )"; ?></td>
            <td class="xxx">X</td>
            <td class="right"><?= $row['qty']; ?></td>
            <td class="currency">&yen;</td>
            <td class="last-cell right currency-value"><?= ($row['total']) ? number_format($row['total'], 0, '.', ' ') : '--'; ?></td>
        </tr>
        <?php
    }
    ?>
    <tr class="section-footer">
        <th colspan="3" class="first-cell left"><?= $totals['b']['title']; ?></th>
        <th class="right"><?= $totals['b']['qty']; ?></th>
        <th class="">&yen;</th>
        <th class="last-cell right"><?= number_format($totals['b']['total'], 0, '.', ' '); ?></th>
    </tr>
    <tr class="separator">
        <td colspan="6" class="left">&nbsp;</td>
    </tr>
    <tr class="section-header big">
        <th colspan="6">C. Bungy - OFFSITE</th>
    </tr>
    <?php
    $photos_info = array('qty' => '', 'total' => 0, 'total_real' => 0);
    $photo_item_name = 'Photos (500)';
    if (array_key_exists($photo_item_name, $result['a'])) {
        $photos_info = $result['a'][$photo_item_name];
    }
    foreach ($result['c'] as $name => $row) {
        ?>
        <tr class="section-item">
            <td class="first-cell currency">&yen;</td>
            <td class="right currency-value"><?= ($row['Rate']) ? (intval($row['Rate']) != 0 ? number_format($row['Rate'], 0, '.', ' ') : $row['Rate']) : "FOC ( {$row['Rate']} )"; ?></td>
            <td class="xxx">X</td>
            <td class="right"><?= $row['qty']; ?></td>
            <td class="currency">&yen;</td>
            <td class="last-cell right currency-value"><?= ($row['total']) ? number_format($row['total'], 0, '.', ' ') : '--'; ?></td>
        </tr>
        <?php
    }
    ?>
    <tr class="section-footer">
        <th colspan="3" class="first-cell left"><?= $totals['c']['title']; ?></th>
        <th class="right"><?= $totals['c']['qty']; ?></th>
        <th class="">&yen;</th>
        <th class="last-cell right"><?= number_format($totals['c']['total'], 0, '.', ' '); ?></th>
    </tr>
    <tr class="separator">
        <td colspan="6" class="left">&nbsp;</td>
    </tr>
    <tr class="section-footer big">
        <th colspan="3" class="first-cell left"><?= $totals['d']['title']; ?></th>
        <th class="right"><?= $totals['d']['qty']; ?></th>
        <th class="">&yen;</th>
        <th class="last-cell right"><?= number_format($totals['d']['total'], 0, '.', ' '); ?></th>
    </tr>
    <tr class="section-footer big">
        <th colspan="3" class="first-cell left">Insurance No.</th>
        <th class="right"></th>
        <th class=""></th>
        <th class="last-cell right"></th>
    </tr>
    <tr class="section-footer notes">
        <td colspan="6" class="first-cell last-cell">Notes
        </th>
    </tr>
    <tr class="not-printed">
        <td colspan="6">
            <button type="button" onClick="document.location = '/Reception/dailyViewIE.php';" id="back">Back</button>
            <button type="button" onClick="window.print();" style="float: right;">Print</button>
        </td>
    </tr>
</table>
<form method="post">
    <table id="cash-calculator" class="not-printed">
        <?php
        foreach ($types as $type) {
            $lower_type = strtolower($type);
            ?>
            <tr class="cash-section-header">
                <th class="first"><?= $type; ?></th>
                <th class="second">#</th>
                <th class="third">&yen;</th>
            </tr>
            <?php
            foreach ($nom as $value) {
                ?>
                <tr custom-value="<?= $value; ?>">
                    <td class="first"><?= $value; ?></td>
                    <td class="second">
                        <input type="text" value="<?= $all_values[$lower_type][$value]; ?>" name="<?= $lower_type; ?>[<?= $value; ?>]" id="<?= $lower_type; ?><?= $value; ?>" class="values">
                    </td>
                    <td class="third" id="<?= $lower_type; ?><?= $value; ?>-total">0</td>
                </tr>
                <?php
            }
            ?>
            <tr class="balance-row">
                <th class="first">Balance</th>
                <th class="second">&yen;</th>
                <th class="third" id="<?= $lower_type; ?>-total">0</th>
            </tr>
            <?php
        }
        ?>
        <tr class="photo-row">
            <th class="first">Photo</th>
            <th class="second" id="photo-qty"><?= $photos_info['qty']; ?></th>
            <th class="third" id="photo-total"><?= $photos_info['total_real'] - $photos_info['qty'] * 500; ?></th>
        </tr>
        <tr class="receipts-row">
            <th class="first">Receipts</th>
            <th class="third" colspan="2" id="receipt-total"><?= $totals['a']['total'] + $totals['b']['total'] + $totals['d']['total']; ?></th>
        </tr>
        <tr class="difference-row">
            <th class="first">Difference</th>
            <th class="second">&yen;</th>
            <th class="third" id="difference-total">0</th>
        </tr>
        <tr class="actions-row">
            <td colspan="3">
                <button id="save" name="action" value="save" type="submit">Save</button>
            </td>
        </tr>
    </table>
</form>
</body>
</html>
