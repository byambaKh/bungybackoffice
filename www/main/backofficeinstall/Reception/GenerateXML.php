<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GenerateXML
 *
 * @author anish.basu
 */
class GenerateXML {

    //put your code here

    public function writeToFile(
    $BookingDate, $BookingTime, $NoOfJump,
                            $RomajiName,
                            $CustomerLastName,
                            $CustomerFirstName,
                            $CustomerAddress,
                            $PostalCode,
                            $Prefecture,
                            $ContactNo,
                            $CustomerEmail,
                            $OtherNo,
                            $TransportMode,
                            $DeleteStatus,
                            $DayStatus,
                            $TimeStatus,
                            $BookingType,
                            $Rate,
                            $BookingReceived,
                            $CollectPay,
                            $RateToPay,
                            $Checked,
                            $SplitName1,
                            $SplitName2,
                            $SplitName3,
                            $SplitTime1,
                            $SplitTime2,
                            $SplitTime3,
                            $SplitJump1,
                            $SplitJump2,
                            $SplitJump3,
                            $Notes,
                            $Photos,
                            $Merchandise,
                            $Agent,
                            $UserName,
                            $CancelFee,
                            $GroupBooking
    ) {


      $myFile = $_SERVER['DOCUMENT_ROOT'] . '/util/BungyRegInfo.txt';
        $str_seperator = chr(13) . chr(10);
        $fh = fopen($myFile, 'a') or die("can't open file");
        
        fwrite($fh, 'BookingDate: '.$BookingDate);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'BookingTime: '.$BookingTime);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'No of Jump: '.$NoOfJump);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Romaji Name: '.$RomajiName);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Last Name: '.$CustomerLastName);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'First Name: '.$CustomerFirstName);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Address: '.$CustomerAddress);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Postal Code: '.$PostalCode);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Prefecture: '.$Prefecture);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Contact No.: '.$ContactNo);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Email: '.$CustomerEmail);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Other No.: '.$OtherNo);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Transport Mode: '.$TransportMode);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Delete Status: '.$DeleteStatus);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Day Status: '.$DayStatus);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Time Status: '.$TimeStatus);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Booking Type: '.$BookingType);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Rate: '.$Rate);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Booking Received: '.$BookingReceived);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Collect Pay: '.$CollectPay);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Rate To Pay: '.$RateToPay);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Checked: '.$Checked);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Split Name1: '.$SplitName1);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Split Name2: '.$SplitName2);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Split Name3: '.$SplitName3);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Split Time1: '.$SplitTime1);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Split Time2: '.$SplitTime2);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Split Time3: '.$SplitTime3);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Split Jump1: '.$SplitJump1);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Split Jump2: '.$SplitJump2);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Split Jump3: '.$SplitJump3);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Notes: '.$Notes);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Photos: '.$Photos);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Merchandise: '.$Merchandise);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Agent: '.$Agent);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'User Name: '.$UserName);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Cancel Fee: '.$CancelFee);
        fwrite($fh, $str_seperator);
        fwrite($fh, 'Variable Booking: '.$GroupBooking);
        fwrite($fh, chr(13) . chr(10));
        fwrite($fh, '-------------XXX---------------');
        fwrite($fh, chr(13) . chr(10));
        fclose($fh);





        $my_file = "BungyRegInfo.txt";
      $my_path = $_SERVER['DOCUMENT_ROOT']."/util/";
        $my_name = "Beau Retallick";
        $my_mail = "backup@bungyjapan.com";
        $my_replyto = "backup@bungyjapan.com";
        $my_subject = "This is a mail with attachment.";
        $my_message = "Booking Details Attachment";
        $this->mail_attachment($my_file, $my_path, "backup@bungyjapan.com", $my_mail, $my_name, $my_replyto, $my_subject, $my_message);


    }

    private function mail_attachment($filename, $path, $mailto, $from_mail, $from_name, $replyto, $subject, $message) {
        $file = $path . $filename;
        $file_size = filesize($file);
        $handle = fopen($file, "r");
        $content = fread($handle, $file_size);
        fclose($handle);
        //$content = chunk_split(base64_encode($content));
        $uid = md5(uniqid(time()));
        $name = basename($file);
        $header = "From: " . $from_name . " <" . $from_mail . ">\r\n";
        $header .= "Reply-To: " . $replyto . "\r\n";
        $header .= "MIME-Version: 1.0\r\n";
        $header .= "Content-Type: multipart/mixed; boundary=\"" . $uid . "\"\r\n\r\n";
        $header .= "This is a multi-part message in MIME format.\r\n";
        $header .= "--" . $uid . "\r\n";
        $header .= "Content-type:text/plain; charset=iso-8859-1\r\n";
        //$header .= "Content-Transfer-Encoding: 7bit\r\n\r\n";
        $header .= $message . "\r\n\r\n";
        $header .= "--" . $uid . "\r\n";
        $header .= "Content-Type: application/txt; name=\"" . $filename . "\"\r\n"; // use different content types here
        //$header .= "Content-Transfer-Encoding: base64\r\n";
        $header .= "Content-Disposition: attachment; filename=\"" . $filename . "\"\r\n\r\n";
        $header .= $content . "\r\n\r\n";
        $header .= "--" . $uid . "--";
        if (mail($mailto, $subject, "", $header)) {
            //echo "mail send ... OK"; // or use booleans here
        } else {
            //echo "mail send ... ERROR!";
        }
    }

    public function changeDateFormat($param){
        $srtNewDateFormat = array();
        try{
            $find = '.';
            $replace = ':';
            if(!is_null($param)){
                $srtNewDateFormat = str_replace($find, $replace, trim($param));
            }
    }catch(Exception $ex){
        print_r($ex);
    }
        return $srtNewDateFormat;
    }

}

?>
