<?php
include "../includes/application_top.php";

$redirect = $_GET['redirect'];
$regid = (int)$_GET['regid'];

if ($regid == 0) {//if there is no registration id, go back to the dailyView page
	Header("Location: dailyViewIE.php");
	die();
};

$booking = getBookingInfo($regid);

if ($booking[0]['GroupBooking'] > 0) {
	Header("Location: moneyReceivedIE.php?regid=" . $regid);
	die();
};

$booking[] = $booking[0];
$booking[1]['NoOfJump'] = 0;
$booking[1]['Rate'] = $config['first_jump_rate'];
unset($booking[1]['CustomerRegID']);
unset($booking[1]['GroupBooking']);
$booking[] = $booking[0];
$booking[2]['NoOfJump'] = 0;
$booking[2]['Rate'] = $config['first_jump_rate'];
unset($booking[2]['CustomerRegID']);
unset($booking[2]['GroupBooking']);
$booking[] = $booking[0];
$booking[3]['NoOfJump'] = 0;
$booking[3]['Rate'] = $config['first_jump_rate'];
unset($booking[3]['CustomerRegID']);
unset($booking[3]['GroupBooking']);

$booking[0]['MedicalConditionsChecked'] = $_POST['MedicalConditionsChecked'];
$booking[1]['MedicalConditionsChecked'] = $_POST['MedicalConditionsChecked'];
$booking[2]['MedicalConditionsChecked'] = $_POST['MedicalConditionsChecked'];
$booking[3]['MedicalConditionsChecked'] = $_POST['MedicalConditionsChecked'];

$_SESSION['converted_to_group'] = $_GET['regid'];

updateBooking($booking);

//Header("Location: moneyReceivedIE.php?regid=" . $regid);
if(isset($redirect)){
	updateBooking($booking);
	Header("Location: $redirect?gbid=" . $regid);
} else {
	Header("Location: {$_SERVER['HTTP_REFERER']}?regid=" . $regid);
}

?>
