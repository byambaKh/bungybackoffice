<?php 
require '../includes/application_top.php';
echo Html::head("Bungy Japan - Cord Calibration", array("/Waiver/css/stylesheet.css","reception.css"), array("reception.js", "jquery-1.11.1.js"));
?>
<div id="step3">
	<div class="header-container">
	<div class="head-image-container"> </div>
	</div>
	<div id="content-container">
		<div id="photo-number"><input name="photo_number"></div>
	</div>

	<div id="photo-keyboard-container">
		<div class="photo-keyboard">
			<div class="keyboard-button"><button>1</button></div>
			<div class="keyboard-button"><button>2</button></div>
			<div class="keyboard-button"><button>3</button></div>
			<div class="keyboard-button"><button>4</button></div>
			<div class="keyboard-button"><button>5</button></div>
			<div class="keyboard-button"><button>6</button></div>
			<div class="keyboard-button"><button>7</button></div>
			<div class="keyboard-button"><button>8</button></div>
			<div class="keyboard-button"><button>9</button></div>
			<div class="keyboard-button"><button>0</button></div>
		</div>
		<div id="enter-del-container">
			<div id="del"><button><span class="lang_en">DELETE</span></button></div>
			<div id="enter"><button><span class="lang_en">ENTER</span></button></div>
		</div>
	</div>
	<div class="back">
		<button> <span class="lang_en">&lt;&lt;BACK</span> </button>
	</div>
</div>
<script>

$(document).ready(function(){
	$('#step3 .keyboard-button button').bind('click touchend MSPointerUp pointerup', function (e) {
		e.preventDefault();
		//if ($('#step3 #photo-number input').val().length == 3) return;
		$('#step3 #photo-number input').val('' + $('#step3 #photo-number input').val() + $(this).html());

		if ($('#step3 #photo-number input').val() > 100){  
                    $('#step3 #photo-number input').val(100);
			return;
		}
	});

	$('#step3 #del  button').bind('click touchend MSPointerUp pointerup', function (e) {
		e.preventDefault();
		var val = $('#step3 #photo-number input').val();
		if (val.length > 0) {
			$('#step3 #photo-number input').val(val.substr(0, val.length-1));
		};
	});

	$('#step3 #photo-dho  button').bind('click touchend MSPointerUp pointerup', function (e) {
		e.preventDefault();
		save_var('photo_number', null);
		goto_step(4);
	});

	$('#step3 #enter  button').bind('click touchend MSPointerUp pointerup', function (e) {
		e.preventDefault();
		var val = $('#step3 #photo-number input').val();

		if (val.length > 0) {
			save_var('photo_number', val);
			goto_step(4);
		} else {
			show_info("validphoto");
		};
	});
});
</script>

<?php require_once("../includes/pageParts/standardFooter.php");?>
