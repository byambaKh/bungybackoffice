<?php
require '../includes/application_top.php';
// If we have gbid passed via GET - edit redirection
if (isset($_GET['gbid'])) {
    // fill needed values
    $gbid = (int)$_GET['gbid'];
    if ($gbid != 0) {
        $regid = $gbid;
        $gbooking = getBookingInfo($gbid);
        $grpJumper = 0;
        foreach ($gbooking as $row) {
            $grpJumper += $row['NoOfJump'];
            $defaults[] = array(
                "cregid"    => $row['CustomerRegID'],
                "jumpsno"   => $row['NoOfJump'],
                "rate"      => $row['Rate'],
                "ratetopay" => $row['RateToPay'],
                "splitTime" => $row['BookingTime']
            );
        };
        $jumpDate = $gbooking[0]['BookingDate'];
        $chkInTime = $gbooking[0]['BookingTime'];
        $agent = $gbooking[0]['Agent'];
        $lastname = $gbooking[0]['CustomerLastName'];
        $firstname = $gbooking[0]['CustomerFirstName'];
        $contactno = $gbooking[0]['ContactNo'];
        $modeTransport = $gbooking[0]['TransportMode'];
        $notes = $gbooking[0]['Notes'];
        $collect = $gbooking[0]['CollectPay'];
        $submit_title = "Update";
    };
} else {
    // EOF
    if ($_POST['grpJumpers'] == '') {
        $grpJumper = 0;
    } else {
        $grpJumper = $_POST['grpJumpers'];
    }
    $jumpDate = $_POST['jumpDate'];
    $gbid = 0;
    $chkInTime = $_POST['chkInTime'];
    $defaults = array('', '', '', '');
    $agent = ($_POST['agentName'] == '') ? 'No agent' : $_POST['agentName'];
    $lastname = $_POST['lastname'];
    $firstname = $_POST['firstname'];
    $contactno = $_POST['contactno'];
    $modeTransport = $_POST['modeTransport'];
    if (empty($modeTransport)) $modeTransport = "Car";
    $notes = $_POST['notes'];
    $collect = '';
    $submit_title = "Book Now";
}
if (isset($_GET['baction']) && $_GET['baction'] == 'cancel') {
    $submit_title = "Cancel";
};
$times_schedule = getBookTimes($jumpDate, $gbid);
$bdate = $jumpDate;

if ($tgt == 901) {
    $agent = $_SESSION['myusername'];
    //echo "------".$agent;
} elseif ($_POST['agentName'] == '') {
    //$agent = "No Agent";
    //echo "------".$agent;
} else {
    //$agent = $_POST['agentName'];
    //echo "------".$agent;  
}


$staff_names = BJHelper::getStaffList('StaffListName', true);


?>
<html>
<head>
    <?php include 'includes/header_tags.php'; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
    <meta http-equiv="X-UA-Compatible" content="IE=8">
    <!-- <meta content="yes" name="apple-mobile-web-app-capable" />
    <meta content="minimum-scale=1.0, width=device-width, maximum-scale=0.6667, user-scalable=no" name="viewport" />
    <link href="css/style.css" rel="stylesheet" media="screen" type="text/css" /> -->
    <script src="js/functions.js" type="text/javascript"></script>
    <title>Variable Booking</title>

    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
    <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
    <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" type="text/css" media="all"/>
    <style>
        .timeSelect {
            width: 100px;
        }

        .jumpSelect {
            width: 40px;
        }

        .row-header {
            background-color: red;
            text-align: center;
            color: white;
        }
    </style>

    <?php $max_jumps = 7; ?>
    <?php include 'bookings.php'; ?>
    <script type="text/javascript" charset="utf-8">
        function processSubmit() {
            if (document.bForm.lastname.value == '') {
                alert('Please fill in your Last Name!');
                return false;
            }
            if (document.bForm.firstname.value == '') {
                alert('Please fill in your First Name!');
                return false;
            }
            for (var i = 0; i < 4; i++) {
                if ($("#rate" + i).val() == 0 && $("#foc" + i).val() == '') {
                    alert('Please select FOC!');
                    document.regform['foc' + i].focus();
                    return false;
                }
                ;
            }

            //document.bForm.action = "reg_split_booking.php";
            document.bForm.action = "groupBookingInsert.php";
            document.bForm.submit();
            return true;
        }
        function procBack() {
            document.bForm.action = "agentActivities.php";
            document.bForm.submit();
        }
        function procBack1() {
            document.bForm.action = "makeBookingIE.php?dispdate=<?php print $bdate; ?>";
            document.bForm.submit();
        }
        function triggerAgt() {
            // MisterSoft rates changes
            var price = 0;
            $('#agentName > option').each(function () {
                if ($(this).prop('selected') === true) {
                    price = $(this).attr('price');
                }
                ;
            });
            $('#rate0 > option, #rate1 > option, #rate2 > option, #rate3 > option').each(function () {
                if ($(this).html() == price) {
                    $(this).prop('selected', true);
                }
                ;
            });
            if ($('#agentName').val() == 'NULL' <?php echo (SYSTEM_SUBDOMAIN == 'MINAKAMI') ? "|| $('#agentName').val() == 'SG Bungy'" : ''; ?>) {
                $('#CollectPay, #CollectPay1, #CollectPay2, #CollectPay3').val('Onsite');
            } else {
                $('#CollectPay, #CollectPay1, #CollectPay2, #CollectPay3').val('Offsite');
            }
            // EOF MisterSoft
        }
        ;
        $(document).ready(function () {
            <?php
                if (!isset($_GET['gbid'])) {
            ?>
            triggerAgt();
            <?php
                };
            ?>
            update_foc();
            $('#rate0, #rate1, #rate2, #rate3').change(update_foc);
        });
        function update_foc() {
            for (var i = 0; i < 4; i++) {
                var disabled = 'disabled';
                if ($('#rate' + i).val() == '0') {
                    disabled = false;
                } else {
                    $('#foc' + i).val('');
                }
                ;
                $('#foc' + i).attr('disabled', disabled);
            }
        }
    </script>
</head>

<body>
<form name="bForm" method="post">
    <input type="hidden" name="baction" value="<?php print isset($_GET['baction']) ? $_GET['baction'] : ''; ?>">
    <input type="hidden" name="bookingdate" value="<?php print $bdate; ?>">
    <input type="hidden" name="chkInTime" value="<?php print $chkInTime; ?>">
    <?php
    // add ids to form if it is edit form
    if (isset($gbooking) && !empty($gbooking)) {
        foreach ($gbooking as $num => $rec) {
            echo '<input type="hidden" name="gbid[]" value="' . $rec['CustomerRegID'] . '">' . "\n";
        }
    };
    // get default price per agent
    switch ($agent) {
        case 'NULL':
            $default_price = '7500';
            break;
        case 'Canyons':
        case 'Canyons Akagi':
            $default_price = '6500';
            break;
        default:
            $default_price = '7000';
    };
    // prepare price options
    ?>
    <table align="center" border="0" width="500px">
        <tbody>
        <tr>
            <td align="center" bgcolor="#000000" valign="middle" colspan="5">
                <h2><font color="#FFFFFF"><?php echo ucfirst(SYSTEM_DATABASE_INFO); ?> Variable Booking</font></h2>
            </td>

        </tr>

        <tr>
            <td bgcolor="#FF0000" align="center" colspan="2">
                <font color="#FFFFFF">Booking Date:&nbsp;<?php echo strftime("%Y-%m-%d (%a)", DateTime::createFromFormat("Y-m-d", $jumpDate)->getTimestamp()); ?></font>
            </td>
            <td bgcolor="#FF0000" colspan="3" align="center"><font color="#FFFFFF">Agent:&nbsp;
                    <?php draw_agent_drop_down('agentName', $agent, 'onChange="triggerAgt();"'); ?>
                </font></td>

        </tr>
        <tr>
            <td bgcolor="#FF0000" align="center" colspan="2">
                <font color="#FFFFFF">Total Jumpers:&nbsp;<?php echo $grpJumper; ?></font></td>

            <td bgcolor="#FF0000" align="center" colspan=3>
                <font color="#FFFFFF">Start Time:&nbsp;<?php echo $chkInTime; ?></font></td>

        </tr>
        <tr class="row-header">
            <td>Collect Pay</td>
            <td>Jump Time</td>
            <td>Price</td>
            <td>FOC</td>
            <td>Jumps</td>
        </tr>
        <tr class="row-header">
            <td>
                <select name="CollectPay" style="float: right;" id="CollectPay">
                    <option<?php echo (isset($gbooking) && isset($gbooking[0]) && $gbooking[0]['CollectPay'] == 'Onsite') ? ' selected="selected"' : ''; ?>>Onsite</option>
                    <option<?php echo (isset($gbooking) && isset($gbooking[0]) && $gbooking[0]['CollectPay'] == 'Offsite') ? ' selected="selected"' : ''; ?>>Offsite</option>
                </select>
            </td>
            <td>&nbsp;</td>
            <td>
                <?php echo draw_pull_down_menu('rate', BJHelper::getRatesList(true), $default_price, "id='rate0'"); ?>
            </td>
            <td><?php echo draw_pull_down_menu('foc', getFOCTitles(), $gbooking[0]['foc'], 'id="foc0"'); ?></td>

            <td>
                <select name='noOfJump' id='noOfJump' class="jumpSelect"> </select>
            </td>
        </tr>
        <?php
        for ($split = 1; $split < 4; $split++) {
            ?>
            <tr class="row-header">
                <td>
                    <select name="CollectPay<?php echo $split; ?>" style="float: right;" id="CollectPay<?php echo $split; ?>">
                        <option<?php echo (isset($gbooking) && isset($gbooking[$split]) && $gbooking[$split]['CollectPay'] == 'Onsite') ? ' selected="selected"' : ''; ?>>Onsite</option>
                        <option<?php echo (isset($gbooking) && isset($gbooking[$split]) && $gbooking[$split]['CollectPay'] == 'Offsite') ? ' selected="selected"' : ''; ?>>Offsite</option>
                    </select>
                </td>
                <td>
                    <select name='splitTime<?php echo $split; ?>' id='splitTime<?php echo $split; ?>' class="timeSelect" onchange='getPrice(this.value);'>
                    </select>

                </td>
                <td>
                    <?php echo draw_pull_down_menu('rate' . $split, BJHelper::getRatesList(true), $default_price, "id='rate$split'"); ?>
                </td>
                <td><?php echo draw_pull_down_menu('foc' . $split, getFOCTitles(), $gbooking[$split]['foc'], 'id="foc' . $split . '"'); ?></td>
                <td>
                    <select name="splitJump<?php echo $split; ?>" id='splitJump<?php echo $split; ?>' onchange="test();" class="jumpSelect">
                    </select>
                </td>
            </tr>
        <?php }; ?>



        <tr>
            <td bgcolor="#FF0000" align="right" colspan="2">
                <font color="#FFFFFF">*Last Name:&nbsp;</font>
                <input type="text" name="lastname" value="<?php print $lastname; ?>" style="text-transform: uppercase" size="15">
            </td>
            <td bgcolor="#FF0000" colspan="3" align="right">
                <font color="#FFFFFF">*First Name:&nbsp;</font>
                <input type="text" name="firstname" value="<?php print $firstname; ?>" style="text-transform: uppercase" size="15">
            </td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" align="right" colspan="2"><font color="#FFFFFF">Contact No:</font>&nbsp;
                <input type="text" name="contactno" size="15" value="<?php print $contactno; ?>">
            </td>
            <td bgcolor="#FF0000" align="right" colspan="3"><font color="#FFFFFF">Transport:</font>&nbsp;
                <select name="modeTransport" id="modeTransport">
                    <option value='--Select--' disabled='disabled'>--Select--</option>
                    <?php
                    $mt = array("Bullet Train", "Local Train", "Car", "Bus", "Motorbike");
                    foreach ($mt as $t) {
                        ?>
                        <option value="<?= $t; ?>"<?= ($t == $modeTransport) ? ' selected="selected"' : ''; ?>><?= $t; ?></option>
                    <?php
                    };
                    ?>

                </select>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" align="right" colspan="2"><font color="#FFFFFF">To Pay to Agent:</font>
            </td>
            <td bgcolor="#FF0000" align="left" colspan="3">&nbsp;
                <font color="#FFFFFF">&yen;</font>
                <?php echo draw_pull_down_menu("ratetopay", BJHelper::getRatesList(true), $gbooking[0]['RateToPay'], "id='ratetopay'"); ?>
                <font color="#FFFFFF">X</font>
                <select name="nos" id="nos">
                    <?php for ($i = 1; $i < 29; $i++) { ?>
                        <?php
                        $selected = '';
                        if ($i == intval($gbooking[0]['RateToPayQTY'])) $selected = "selected";
                        ?>
                        <option <?= $selected; ?> value="<?= $i; ?>"<?= ($i == $grpJumper) ? '' : ''; ?>><?= $i; ?></option>
                    <?php }; ?>
                </select>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" align="right" colspan="2">
                <font color="#FFFFFF"><?php echo (!isset($_GET['gbid'])) ? 'Booked' : ((isset($_GET['baction']) && $_GET['baction'] == 'cancel') ? 'Cancelled' : 'Updated'); ?> by:</font>
            </td>
            <td bgcolor="#FF0000" colspan="3">
                <?php echo draw_pull_down_menu('booked_by', $staff_names, '', 'style="width: 180px;"'); ?>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" align="center" colspan="5"><font color="#FFFFFF">Notes:</font>
                <textarea rows="3" cols="35" name="notes"><?php echo $notes; ?></textarea>
            </td>
        </tr>
        </tbody>
    </table>
    <table align="center" border="0" width="496px" cellspacing="0" cellpadding="2">
        <tr>
            <td bgcolor="#000000" align="left" colspan="2">
                <input type="button" style="width:76;background-color:#FFFF66" name="operation" id="back" value="Back" onclick="procBack1();"/>
            </td>
            <td bgcolor="#000000" align="right" colspan="2">
                <input type="button" style="width:76;background-color:#FFFF66" name="operation" id="sbmt" value="<?php echo $submit_title; ?>" onclick="processSubmit();"/>
            </td>
        </tr>
    </table>

</form>
</body>
</html>
<?php
include("ticker.php");
?>
