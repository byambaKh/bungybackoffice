<?php
include '../includes/application_top.php';
$current_time = time();
$d = date('Y-m', $current_time);
if (isset($_GET['date'])) {
    $current_time = mktime(0, 0, 0, substr($_GET['date'], 5, 2), substr($_GET['date'], 8, 2), substr($_GET['date'], 0, 4));
} else {
    $current_time = mktime(0, 0, 0, substr($d, 5, 2), 15, substr($d, 0, 4));
};
$record_type = 'non_jumpers';
// total work function
$headers = array(
    "Date",
    "Last Name",
    "First Name",
);
$fields = array_map(function ($item) {
    return strtolower(
        preg_replace(
            "/(_)$/",
            "",
            preg_replace("([^\w]+)", "_", $item)
        )
    );
}, $headers);
// check if needed table exists
$values = array();
$action = $_POST['action'];
if (empty($action)) {
    $action = $_GET['action'];
};
switch (TRUE) {
    case ($action == 'Add'):
        $data = array(
            'waiver_id' => $_POST['i']['wid']
        );
        db_perform($record_type, $data);
        Header("Location: non_jumper.php");
        die();
        break;
    case ($action == 'Delete'):
        $sql = "delete from $record_type where site_id = '" . CURRENT_SITE_ID . "' AND id = '{$_GET['id']}'";
        mysql_query($sql) or die(mysql_error());
        Header("Location: $record_type.php");
        die();
        break;
    case ($action == 'CSV'):
        $d = date('Y-m', $current_time);
        $sql = "select * from $record_type WHERE site_id = '" . CURRENT_SITE_ID . "' AND `date` like '$d%' order by `date` ASC;";
        Header('Content-Type: text/csv; name="' . $record_type . $d . '.csv"');
        Header('Content-Disposition: attachment; filename="' . $record_type . $d . '.csv"');
        $res = mysql_query($sql);
        echo '"' . implode('","', $headers) . '"' . "\r\n";
        $balance = 0;
        while ($row = mysql_fetch_assoc($res)) {
            $row['total_work'] = getTotalWork($row['start'], $row['finish']);
            $row['day'] = preg_replace("/([\d]{4}-[\d]{2}-[0]?)/", "", $row['date']);
            foreach ($fields as $field) {
                echo '"' . $row[$field] . '"' . ($field == $fields[sizeof($fields) - 1] ? '' : ',');
            };
            echo "\r\n";
        };
        die();
        break;
};
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Non-Jumpers</title>
    <?php include "../includes/head_scripts.php"; ?>
    <style>
        h1 {
            margin: 0;
            background-color: red;
            text-align: center;
            width: 100%;
        }

        #payroll-table {
            background-color: black;
            min-width: 400px;
        }

        #payroll-table td {
            background-color: white;
        }

        #payroll-table .row-error td {
            background-color: red;
        }

        #payroll-table th {
            background-color: black;
            color: white !important;
        }

        #payroll-table th#out-header {
            background-color: red;
            color: white;
        }

        #payroll-table th#in-header {
            background-color: #00ff00;
            color: white;
        }

        #payroll-table td {
            vertical-align: top;
            text-align: center;
        }

        #payroll-header td {
            background-color: yellow;
        }

        #monthly-totals {
            float: right;
        }

        #date-new input {
            width: 90px;
            text-align: center;
        }
    </style>
</head>
<body>
<?php include 'includes/main_menu.php'; ?>
<script>
    var current_date = "<?php echo date("Y-m", $current_time); ?>";

    function row_update() {
        var id = $(this).attr('rel');
        var post_data = {};
        post_data.id = id;
        $(this).parent().parent().parent().children("td.value").each(function () {
            var field = $(this).attr('rel');
            post_data[field] = $(this).children('[name=' + field + ']').prop('value');
        });
        if (post_data['start'] != post_data['finish']) {
            $(this).parent().parent().parent().removeClass('row-error');
        } else {
            $(this).parent().parent().parent().addClass('row-error');
        }
        ;
        $.ajax({
            url: 'ajax_handler.php?action=update_row&type=<?php echo $record_type; ?>&date=' + current_date,
            method: 'POST',
            data: post_data,
            context: $(this)
        }).done(function () {
            $(this).parent().parent().parent().children("td.value").each(function () {
                var field = $(this).attr('rel');
                $(this).html($(this).children('[name=' + field + ']').prop('value'));
            });
// update total work
            try {
                var st = $(this).parent().parent().parent().children("td.start").html().split(':');
                var fi = $(this).parent().parent().parent().children("td.finish").html().split(':');
                var diff = parseInt(fi[0], 10) - parseInt(st[0], 10) + (parseInt(fi[1], 10) - parseInt(st[1], 10)) / 60;
                if (diff >= 9) {
                    diff -= 0.25;
                }
                ;
                diff -= 0.75;
                $(this).parent().parent().parent().children("td.total_work").html(diff.toFixed(2));
            } catch (e) {
                $(this).parent().parent().parent().children("td.total_work").html('N/A');
            }
// EOF total work
            $(this).prop('value', 'Change').removeClass('update').addClass('change').unbind('click').click(row_change);
            $(this).parent().children(".cancel").prop('value', 'Delete').removeClass('cancel').addClass('delete').unbind('click').click(row_delete);
        });
    }
    function row_delete() {
        var id = $(this).attr('rel');
        if (window.confirm("Are you sure to delete this record?")) {
            //document.location = "?action=Delete&id=" + id;
            var post_data = {"id": id};
            $.ajax({
                url: 'ajax_handler.php?action=delete_row&type=<?php echo $record_type; ?>&date=' + current_date,
                method: 'POST',
                data: post_data,
                context: $(this)
            }).done(function () {
                $(this).parent().parent().parent().remove();
            }).fail(function () {
                alert('Record delete failed.');
            });
        }
        ;
    }
    function row_cancel() {
        var id = $(this).attr('rel');
        $(this).parent().parent().parent().children("td.value").each(function () {
            var field = $(this).attr('rel');
            $(this).html($(this).children('input[type=hidden]').prop('value'));
        });
        $(this).parent().children(".update").prop('value', 'Change').removeClass('update').addClass('change').unbind('click').click(row_change);
        $(this).prop('value', 'Delete').removeClass('camcel').addClass('delete').unbind('click').click(row_delete);
    }
    function row_change() {
        var id = $(this).attr('rel');
        $(this).parent().parent().parent().children("td.value").each(function () {
            var input_field = '';
            var field = $(this).attr('rel');
            var value = $(this).html();
            if (typeof field != 'undefined' && fvalues[field].length > 0) {
                input_field = $('<select>').prop('name', field);
                for (fv in fvalues[field]) {
                    if (fvalues[field].hasOwnProperty(fv)) {
                        input_field.append(
                            $('<option>').prop('value', fvalues[field][fv].id).text(fvalues[field][fv].text)
                        );
                    }
                    ;
                }
                ;
                input_field.prop('value', $(this).html());
            } else {
                input_field = $('<input>').prop('name', field).prop('value', $(this).html());
            }
            ;
            $(this).html(input_field);
            $(this).append(
                $('<input>').prop('type', 'hidden').prop('name', field + '_old').prop('value', value)
            );
        });
        $(this).prop('value', 'Update').removeClass('change').addClass('update').unbind('click').click(row_update);
        $(this).parent().children(".delete").prop('value', 'Cancel').removeClass('delete').addClass('cancel').unbind('click').click(row_cancel);
        $('.date, .date input, input[name=date]').datepicker({
            showAnim: 'fade',
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
            currentText: 'Today',
            maxDate: new Date(<?php echo $maxDate?>)
        });

    }
    ;
    $(document).ready(function () {
        $('.date, .date input').datepicker({
            showAnim: 'fade',
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
            currentText: 'Today',
            maxDate: new Date(<?php echo $maxDate?>)
        });
        $(".delete").click(row_delete);
        $(".remove-all").click(function () {
            return window.confirm('Are you sure to delete all this month records?');
        });
        $('.change').unbind('click').click(row_change);
        $('#monthly-totals').click(function () {
            var monthly_totals_window = window.open('payroll_totals.php?date=<?php echo date("Y-m", $current_time); ?>', 'monthly_totals_window', 'height=500,width=900,location=0,menubar=0,status=0,toolbar=0,scrollbars=1');
            monthly_totals_window.focus();
        });
        $('#date-new input').change(function () {
            var el = $("#wid-new select");
            el.children("option").remove();
            el.append($('<option>').text('Loading...'));
            $.ajax({
                url: 'ajax_handler.php?action=getWaivers&date=' + $(this).val(),
                method: 'POST',
                dataType: 'json',
                context: $(this)
            }).success(function (data) {
                var el = $("#wid-new select");
                el.children("option").remove();
                if (data['result'] == 'success') {
                    for (var i in data['options']) {
                        if (data['options'].hasOwnProperty(i)) {
                            var o = $('<option>').text(data['options'][i].text).val(data['options'][i].id);
                            el.append(o);
                        }
                        ;
                    }
                    ;
                }
                ;
                if (el.children("option").length < 1) {
                    el.append($('<option>').text('No waivers for this date'));
                }
                ;
            });
        });
        $('#date-new input').trigger('change');
    });
    var fvalues = {};
    <?php
        $d = date("Y-m-d");
        $values['wid'] = BJHelper::getWaiversDropDown($d);
        echo "\tfvalues.wid = " . json_encode($values['wid']) . ";\n";
        foreach ($fields as $field) {
            echo "\tfvalues.$field = " . json_encode(array_key_exists($field, $values) ? $values[$field] : array()) . ";\n";
        };
    ?>
</script>
<table id="payroll-table" align="center">
    <tr>
        <td colspan="<?php echo sizeof($fields) + 1; ?>">
            <table border="0" cellspacing="0" cellpadding="0" width="100%" id="payroll-header">
                <tr>
                    <td colspan="2"><h1> Non-Jumpers </h1></td>
                </tr>
            </table>
        </td>
    <tr>
        <?php
        foreach ($fields as $key => $field) {
            echo "\t<th id=\"$field-header\">{$headers[$key]}</th>\n";
        };
        ?>
        <th id="actions-header">Actions</th>
    </tr>
    <?php
    $d = date("Y-m-d", mktime(0, 0, 0, date('m'), date('d'), date('Y') - 1));
    $sql = "select
				nj.id,
				cr.BookingDate as 'date',
				w.lastname as last_name,
				w.firstname as first_name
			from non_jumpers nj 
			LEFT JOIN waivers w on (nj.waiver_id = w.id) 
			LEFT JOIN customerregs1 cr on (w.bid = cr.CustomerRegID) 
			WHERE 
				cr.site_id = '" . CURRENT_SITE_ID . "'
				AND cr.BookingDate > '$d' 
			order by BookingDate ASC;";
    if ($res = mysql_query($sql)) {
        while ($row = mysql_fetch_assoc($res)) {
            ?>
            <tr>
                <?php
                foreach ($fields as $key => $field) {
                    echo "\t<td id=\"$field-{$row['id']}\" class=\"" . (($field == 'total_work') ? '' : 'value') . " $field\" rel=\"$field\">{$row[$field]}</td>\n";
                };
                ?>
                <td id="action-change-delete">
                    <nobr><input type="button" value="Delete" rel="<?php echo $row['id']; ?>" class="delete"></nobr>
                </td>
            </tr>
        <?php
        };
    } else {
        ?>
        <tr>
            <td id="no-record-exists" colspan="<?php echo sizeof($fields) + 1; ?>">No data available
            </th>
        </tr>
    <?php
    };
    ?>
    <form method="POST">
        <tr>
            <?php
            $record = array('date' => date("Y-m-d"));
            foreach ($fields as $key => $field) {

                //$fields[$key] =
                if ($field == 'last_name') continue;
                $colspan = '';
                if ($field == 'first_name') {
                    $colspan = " colspan='2'";
                    $field = 'wid';
                };
                $input_field = draw_input_field($field, $field, $record, '', false);
                if (array_key_exists($field, $values)) {
                    $input_field = draw_pull_down_menu('i[' . $field . ']', $values[$field]);
                };

                echo "\t<td id=\"$field-new\" class=\"$field\"$colspan>" . $input_field . "</td>\n";
            };
            ?>
            <td id="action-add"><input type="submit" name="action" value="Add"></td>
        </tr>
    </form>
</table>
<div style="text-align: center;">
    <br/>
    <button id="close" onClick="javascript: window.close();">Close</button>
</div>
<?php include("ticker.php"); ?>
</body>
</html>
