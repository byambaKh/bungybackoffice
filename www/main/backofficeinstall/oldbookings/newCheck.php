<?php
//SELECT site_id, BookingDate, BookingTime, SUM(NoOfJump) FROM customerregs1 WHERE BookingDate >= NOW() GROUP BY site_id, BookingDate, BookingTime;
include("../includes/application_top.php");
include('functions.php');
$language = setLanguage();
requireHeader($language);
?>

<?php
function waiverLink()
{
    $language = setLanguage();
    echo 'http://bungyjapan.com/downloads/waiver_' . strtolower(APP_SUBDOMAIN) . '_' . $language . '.pdf';
}
function populateLocation()
{
    $language = setLanguage();
    $sites = getPlaces(); // get the site list
    $sitesColumn = ($language == 'jpn' ? array_column($sites, 'name_jp') : array_column($sites, 'name')); //grab the correct column based on language
    foreach ($sitesColumn as $site) 
    {
        $site = mb_convert_encoding($site, 'UTF8', 'SJIS');
        echo "<option value=" . $site . ">" . $site . "</option>";
    }    
}
function populateNumJumpers()
{
    $maxJumpers = 6;
    for($i = 1; $i <= $maxJumpers; $i++)
    {
        echo "<option value=" . $i . ">" . $i . "</option>";
    }
}
function pollFutureDays()
{
    $query = 
    "SELECT table1.site_id, table1.BookingDate, table1.BookingTime, IFNULL(table2.jumpers, 0) AS jumpers 
        FROM 
            (SELECT 
                site_id,
                CONVERT(BOOK_DATE, DATE) AS BookingDate,
                RIGHT(CONCAT('0', BOOK_TIME), 8) AS BookingTime
            FROM Time_State 
            WHERE
                OPERATION_DESC = 'Time ON'
                AND BOOK_DATE >= LEFT(NOW(),10)) AS table1
        LEFT JOIN 
            (SELECT 
                site_id, 
                BookingDate, 
                BookingTime, 
                SUM(NoOfJump) AS jumpers
            FROM customerregs1 
            WHERE 
                BookingDate >= LEFT(NOW(),10)
                AND BookingTime IS NOT NULL
            GROUP BY site_id, BookingDate, BookingTime) as table2
        ON 
        table1.site_id = table2.site_id
        AND table1.BookingDate = table2.BookingDate
        AND table1.BookingTime = table2.BookingTime;";
    $result = queryForRows($query);
    return $result;
}
?>
<head>
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all"/>
    <link rel="stylesheet" href="../js/bookings/jqueryui/1.8.10/themes/base/jquery-ui.css" type="text/css" media="all"/>
    <script src="../js/bookings/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script src="../js/bookings/jqueryui/1.10.1/jquery-ui.min.js" type="text/javascript"></script>

    <script>
    $( 
        function()
        {
            $( "#datepicker" ).datepicker
            ({
                dateFormat: "yy-mm-dd",
                minDate: 0,
                maxDate: new Date(<?php echo $maxDateCustomer;?>),
                beforeShowDay: checkAvailability
            });
        }
    );
    
    function dateToYMD(date) 
    {
        var d = date.getDate();
        var m = date.getMonth() + 1; //Month from 0 to 11
        var y = date.getFullYear();
        return '' + y + '-' + (m<=9 ? '0' + m : m) + '-' + (d <= 9 ? '0' + d : d);
    }

    function checkAvailability(date)
    {
        console.log(dateToYMD(date));
        var test = [1,''];
        return test;
    }
    </script>
</head>
<?php= drawProgressForIndexAndLanguage(1, $language) ?>
<div id="centerColumn">
        <div id ="sectionTop"class="alignLeft half">
            <div id = "inner">Check Availability</div>
        </div>
        <div id ="sectionTop" class="alignRight half">
            <div id = "inner">
                <a href= <?php waiverLink()?> download target="_blank">
                    <img class="u20Button" src="img/waiver_download_<?= $language ?>.png">
                </a>
            </div>
        </div>
    <div id="slice">
        <h5>We are sorry, credit card bookings are currently unavailable.</h5>
    </div>
    <div id="slice">
        Jump Location: <select id ="dropdown"><?php populateLocation()?></select>
    </div>
    <div id="slice">
        <div class="half alignLeft">
            Select Number of Jumpers:
        </div> 
        <div class="half alignRight">
            <select id ="dropdown"><?php populateNumJumpers()?></select>
        </div>
    </div>
    <div id="slice">
        <div class="half alignLeft">
            Jump Date: 
        </div> 
        <div class="half alignRight">
            <input type="text" id="datepicker" class="alignRight" readonly value="Select">
        </div>
    </div>
    <div id="slice">Check-In Time: <select id ="dropdown" disabled></select></div>
    <input type="submit" class="wide-button" name="operation" id="search" value="Search" onclick="document.pressed=this.value">
    <div id="slice">We can usually only fit 6 jumpers in each half hour time slot. For larger groups please contact us directly at 0278-72-8133.</div>
    <div id="slice">Only times with availability will appear in the 'Check-In Time:' field. The words "Fully Booked" will appear on days that are already fully booked.</div>
    <div id="slice">We are unable to accept bookings for the "Minakami / Sarugakyo Same-Day Combo" on-line. Please call us at 0278-72-8133 to book.</div>
</div>
<?php 
    $result = pollFutureDays();
    var_dump($result);
?>
<?php requireFooter($language); ?>
