<?php
include("../includes/application_top.php");
require('functions.php');
//ini_set('display_errors', 'On');
//error_reporting(E_ALL & ~E_DEPRECATED & ~E_NOTICE);
/**
 * @param $language
 *
 * @return array
 */
function createPlaces($language)
{
    $places = getPlaces();

    $placesColumn = 'name';
    if ($language == 'jpn') $placesColumn = 'name_jp';

    foreach ($places as $id => $place) {
        $places[$id]['text'] = mb_convert_encoding($place[$placesColumn], 'UTF8', 'SJIS');//places table is in SJIS
    }

    $menuHeader = [
        'id'        => '0',
        'name'      => 'Please Select',
        'name_jp'   => '選択する',
        'subdomain' => ''
    ];
    $menuHeader['text'] = $menuHeader[$placesColumn];//choose the correct header for the current language
    array_unshift($places, $menuHeader);

    return $places;
}

/**
 * @return array
 */
function createTimesArray()
{
    $times = [
        ['id' => 0, 'text' => 'Select', 'status' => 0]
    ];

    if (isset($_POST['chkInTime'])) {
        $times[] = ['id' => $_POST['chkInTime'], 'text' => $_POST['chkInTime'], 'status' => 1];

        return $times;
    }

    return $times;
}

/**
 *
 * @return string
 */
function findBadDates()
{
    $bDates = getCalendarState();
    $bDates = array_unique(array_merge($bDates, getFullDates()));

    $badDates = '';
    foreach ($bDates as $disabledate) {
        $badDates .= " \"$disabledate\",";
    }
    $badDates .= " 1";

    return $badDates;
}

function drawPeopleDropDown($i)
{
    $index = 1;
    $output = '';

    for ($index = 1; $index <= $i; $index++) {
        $output .= "<option value='$index'>$index</option>\n";
    }

    return $output;
}

function incrementViewCounter()
{
    // Add correct path to your countlog.txt file.
    $path = './karaokeCount.txt';

    // Opens countlog.txt to read the number of hits.
    $file  = fopen( $path, 'r' );
    $count = fgets( $file, 1000 );
    fclose( $file );

    // Update the count.
    $count = abs( intval( $count ) ) + 1;

    // Opens countlog.txt to change new hit number.
    $file = fopen( $path, 'w' );
    fwrite( $file, $count );
    fclose( $file );
}

$language = setLanguage();

$languages = [

    'eng' => [
        '_selectNumber' => 'Select Number of Jumpers:',
        '_creditCardNotRequired'               => "CREDIT CARD NOT REQUIRED",
        '_creditCardNotAvailable'                  => "We are sorry, credit card bookings are currently unavailable.",
        '_jumpPlace'                           => 'Jump Place',
        '_jumpDate'                            => 'Jump Date',
        '_checkInTime'                         => 'Check-In Time',
        '_checkAvailability'                   => "Check Availability",
        '_pleaseSelectADate'                   => 'Please Select A Date To Continue',
        '_weCanUsuallyOnlyFit'                 => "We can usually only fit {$config['online_slots']} jumpers in each half hour time slot. For larger groups please contact us directly at {$config['site_phone']}.",
        '_search'                              => 'Search',
        '_weAreUnableToAcceptBookings'         => "We are unable to accept bookings for the \"Minakami / Sarugakyo Same-Day Combo\" on-line. Please call us at {$config['site_phone']} to book.",
        '_onlyTimesWithAvailabilityWillAppear' => "Only times with availability will appear in the 'Check-In Time:' field. The words <span>\"Fully Booked\"</span> will appear on days that are already fully booked. ",
        '_sorryUnavailable'                    => "Sorry, but our online booking system is currently unavailable. To check availability or make a booking, please contact us at {$config['site_phone']} or email us at:",
        '_fullyBooked' => 'Fully Booked'
    ],

    'jpn' => [
        '_selectNumber' => "人数をお選びください：",
        '_creditCardNotRequired'               => "クレジットカードは入りません",
        '_creditCardNotAvailable'              => "クレジットカードは一時的にご利用不可能となっております",
        '_jumpPlace'                           => '場所',
        '_jumpDate'                            => 'ご希望日',
        '_checkInTime'                         => 'ご希望時間',
        '_checkAvailability'                   => "空き状況のご確認",
        '_pleaseSelectADate'                   => '日付を選択してください。。。',
        '_weCanUsuallyOnlyFit'                 => "30分の時間帯で最大６名様のご予約が可能です。大勢でのご予約や空き状況などのご質問等がございましたら、電話番号までご連絡ください。({$config['site_phone']})",
        '_search'                              => '空き状況検索',
        '_weAreUnableToAcceptBookings'         => "みなかみバンジー&amp;猿ヶ京バンジー同日コンボは、インターネットご予約は出来ません。お手数ですが、ご予約はお電話で承ります。({$config['site_phone']})",
        '_onlyTimesWithAvailabilityWillAppear' => "予約受付が可能の時間のみが表示されます。「空きなし」の表示が出る場合は、その日は既に満員です",
        '_sorryUnavailable'                    => "現在、オンライン予約はできません。予約や空き状況の確認は、電話： {$config['site_phone']} または メール：",
        '_fullyBooked' => '空きなし'
    ],

    'chn' => [
    ],
];

extract($languages[$language]);
/* disabled for now.
if(isset($_GET['discount']))
{
    if(!isset($_SESSION['discount'])) incrementViewCounter();
    $_SESSION['discount'] = $_GET['discount'];
}
*/
$_SESSION['jumpDate'] = $_POST['jumpDate'];
$_SESSION['myTime'] = $_POST['alternate'];
$_SESSION['chkInTime'] = $_POST['chkInTime'];
$badDates = findBadDates();
$placesMenuArray = createPlaces($language);
$pullDownMenuPlace = draw_pull_down_menu(
    "jumpPlace",
    $placesMenuArray,
    CURRENT_SITE_ID,
    'id="jumpPlace" onChange="javascript:doRedirect();"',
    false
);
$times = createTimesArray();
$pullDownMenuTime = draw_pull_down_menu(
    'chkInTime',
    $times,
    '',
    'id="chkInTime"'
);

requireHeader($language);
?>
<link rel="stylesheet" href="../js/bookings/jqueryui/1.8.10/themes/base/jquery-ui.css" type="text/css" media="all"/>
<script src="../js/bookings/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
<script src="../js/bookings/jqueryui/1.10.1/jquery-ui.min.js" type="text/javascript"></script>
<script src="/js/core.js" type="text/javascript"></script>
<script type="text/javascript" language="javascript" src="js/script.js"></script>
<script type="text/javascript" charset="utf-8">

    function OnSubmitForm() {
        if (document.getElementById('jumpDate')) {
            document.sform.submit();
        }

        if (document.pressed == '<?=$_search?>') {
            var ddvalue = document.getElementById('chkInTime').value;
            if (ddvalue == 'Select') {
                return false;
            }
            if (document.getElementById('jumpDate').value == ''
                || (ddvalue == 0)) {
                alert("<?=$_pleaseSelectADate?>");
                return false;
            } else {
                //document.sform.action = getTransferProtocol() + $('#jumpPlace option:selected').attr('subdomain') + '.bungyjapan.com/bookings/search.php';
                document.sform.action = "<?= getBaseUrl() ?>search.php";
                document.sform.submit();
                return true;
            }
        }
    }

    function doRedirect() {
        if (typeof $('#jumpPlace option:selected').attr('subdomain') != 'undefined' && $('#jumpPlace option:selected').attr('subdomain') != '') {
            document.sform.action = getTransferProtocol() + $('#jumpPlace option:selected').attr('subdomain') + '.bungyjapan.com/bookings/?lang=<?=$language?>&discount=<?=$_SESSION['discount']?>';
            document.sform.submit();
        }
    }

    function processGroup() {
        if (document.getElementById('group')) {
            document.sform.action = "groupBooking.php";
            document.sform.submit();
            return true;
        }
    }


    function getAvTime() {
        var varDate = document.getElementById('jumpDate').value;
        alert(varDate);
    }

    function chckDate() {
        if (document.sform.jumpDate.value == '') {
            alert("Please input date");
            return false;
        } else {
            document.sform.submit();
            return true;
        }
    }

    $(function () {
        var d = new Date();
        $mydate = $('#jumpDate').datepicker({
            numberOfMonths: 1,
            dateFormat: 'yy-mm-dd',
            altField: '#alternate',
            altFormat: 'yy-mm-dd',
            minDate: '0d',
            maxDate: new Date(<?php echo $maxDateCustomer;?>),
            beforeShowDay: checkAvailability,
            onSelect: getBookTimes
        });
        getBookTimes($('#jumpDate').val());
        $('#jumpPlace').change(update_submit);

    });

    function getBookTimes(bDate, inst) {
        $.ajax({
            url: '../includes/ajax_helper.php',
            dataType: 'json',
            async: false,
            data: 'action=getBookTimes&bDate=' + encodeURIComponent(bDate) + '&regid=<?php echo $regid; ?>&online=1&checkForPast=TRUE',
            success: function (data) {
                if (data['result'] == 'success') {
                    var noTime = true;
                    var selected = $('#chkInTime').val();
                    $('#chkInTime > option').remove();
                    for (i in data['options']) {
                        if (data['options'].hasOwnProperty(i)) {
                            data['options'][i].avail = parseInt(data['options'][i].avail);
                            if (data['options'][i].value == '') continue;
                            if (data['options'][i].avail < 1) continue;
                            var option = $("<option> </option>");
                            option.attr('avail', data['options'][i].avail);
                            option.prop('selected', (data['options'][i].value === selected) ? 'selected' : '');
                            if (data['options'][i].open == 0) {
                                continue;
                            }
                            option.text(data['options'][i].value).val(data['options'][i].value).appendTo('#chkInTime');
                            noTime = false;
                        }
                    }
                    if (noTime) {
                        $("<option></option>").text('<?=$_fullyBooked?>').attr('selected', 'selected').appendTo('#chkInTime');
                    }
                    update_submit();
                }
            }
        });
    }

    function update_submit() {
        if ($('#jumpPlace').val() > 0 && ($('#chkInTime').val() != '<?=$_fullyBooked?>')) {
            $('#search').attr('disabled', false);
        } else {
            $('#search').attr('disabled', 'disabled');
        }
    }


    var $myBadDates = new Array( <?=$badDates?>);

    function checkAvailability(mydate) {
        var $return = true;
        var $returnclass = "available";
        $checkdate = $.datepicker.formatDate('yy-mm-dd', mydate);
        for (var i = 0; i < $myBadDates.length; i++) {
            if ($myBadDates[i] == $checkdate) {
                $return = false;
                $returnclass = "unavailable";
                return [$return, $returnclass, "<?=$_fullyBooked?>"];
            }
        }
        return [$return, $returnclass];
    }

    function pro() {
        document.sform.operation.disabled = false;
    }
</script>
<?= drawProgressForIndexAndLanguage(1, $language) ?>
<div id="content-container">
    <?php if ($siteStatus == 1) { ?>
        <form method="post" name="sform" onsubmit="OnSubmitForm();">
            <table class="booking-table">
                <tbody>
                <tr>
                    <td colspan="1">
                        <h3><?= $_checkAvailability ?></h3>
                    </td>
                    <td colspan="4">
                        <a href="http://bungyjapan.com/downloads/waiver_<?php echo strtolower(APP_SUBDOMAIN) ?>_<?= $language ?>.pdf" download target="_blank">
                            <img class="u20Button" src="img/waiver_download_<?= $language ?>.png">
                        </a>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <!-- <h4><?= $_creditCardNotRequired ?></h4> -->
                        <h4><?= $_creditCardNotAvailable ?></h4>
                    </td>
                </tr>

                <tr>
                    <td><?= $_jumpPlace ?>:</td>
                    <td colspan="3"> <?= $pullDownMenuPlace ?></td>
                </tr>
                <tr>
                    <td><?= $_jumpDate ?>:</td>
                    <td colspan="3">
                        <input type="text" name="jumpDate" id="jumpDate" readonly="readonly" value="<?php print ($_POST['jumpDate']) ? $_POST['jumpDate'] : date('Y-m-d'); ?>"/>
                    </td>
                </tr>
                <tr>
                    <td colspan="4">
                        <?= $_weCanUsuallyOnlyFit ?><br><br>
                        <?= $_onlyTimesWithAvailabilityWillAppear ?>
                    </td>
                </tr>
                <tr>
                    <td colspan="1"><?= $_selectNumber ?>:</td>
                    <td colspan="3">

                        <select name="noOfJump" id="noOfJump">
                            <?= drawPeopleDropDown(6) ?>
                        </select>
                    </td>
                    <!--
                    <td>
                        <span class="hotspot" onmouseover="tooltip.show();" onmouseout="tooltip.hide(<?= $_weCanUsuallyOnlyFit ?>);">
                            <img src="../img/question_mark.gif" alt="????"/>
                        </span>
                    </td>
                    -->
                </tr>
                <tr>
                    <td colspan="1"><?= $_checkInTime ?>:</td>
                    <td colspan="3"> <?= $pullDownMenuTime ?> </td>
                    <!--
                    <td>
                        <span class="hotspot" onmouseover="tooltip.show();" onmouseout="tooltip.hide(<?= $_weCanUsuallyOnlyFit ?>);">
                            <img src="../img/question_mark.gif" alt="????"/>
                        </span>
                    </td>
                    -->
                </tr>
                <tr>
                    <td colspan="4">
                        <input type="submit" class="wide-button" name="operation" id="search" value="<?= $_search ?>" onclick="document.pressed=this.value"/>
                    </td>
                </tr>
            </table>
            <br>
            <table>
                <tr>
                    <td colspan="4">
                        <?= $_weAreUnableToAcceptBookings ?>
                    </td>
                </tr>
            </table>
            <br>
        </form>

    <?php } else { ?>
        <table>
            <tbody>
            <tr>
                <td colspan="8">
                    <?= $_sorryUnavailable ?>: <b><a href="mailto:<?= $email ?>"><?= $email ?></a></b>
                </td>
            </tr>
            </tbody>
        </table>
    <?php } ?>
</div>

<? requireFooter($language); ?>
