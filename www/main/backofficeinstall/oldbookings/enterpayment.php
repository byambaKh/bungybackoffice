<?php
include("../includes/application_top.php");
require("functions.php");
/*
ini_set('display_errors', 'On');
error_reporting(E_ALL);
*/

/**
 * @param $config
 * @param $php_errormsg
 *
 * @return mixed
 * @throws Exception
 */
function getTokenFromInpendium($config, $php_errormsg = '')
{
    setSessionVariables();
    //from copyandpayconfig.php
    $url = "https://inpendium.net/frontend/GenerateToken";
    $sender = "8a8394c1477d5dd001478f5cc518387f";
    $userLogin = "8a8394c1477d5dd001478f5cc5193883";
    $userPwd = "GkS4R2bj";
    $secret = "Hm35nkwxBMWWEddt";
    $currency = "JPY";
    $chanel = $config['copyandpay_channel'];

    $data = array(
        "SECURITY.SENDER"              => $sender,
        "TRANSACTION.CHANNEL"          => $chanel,
        "IDENTIFICATION.TRANSACTIONID" => time(),
        "USER.LOGIN"                   => $userLogin,
        "USER.PWD"                     => $userPwd,
        //"USER.SECRET" => $secret,
        "PAYMENT.TYPE"                 => "DB",
        "PRESENTATION.AMOUNT"          => $_SESSION['total_price'],
        "PRESENTATION.CURRENCY"        => $currency,
        "NAME.GIVEN"                   => mb_convert_encoding($_SESSION['address_last'], "UTF-8", $_SESSION['address_encoding']),
        "NAME.FAMILY"                  => mb_convert_encoding($_SESSION['address_first'], "UTF-8", $_SESSION['address_encoding']),
        "ADDRESS.STREET"               => trim(
            mb_convert_encoding($_SESSION['address_street'], "UTF-8", $_SESSION['address_encoding'])
            . " " .
            mb_convert_encoding($_SESSION['address_street2'], "UTF-8", $_SESSION['address_encoding'])
        ),
        "ADDRESS.ZIP"                  => mb_convert_encoding($_SESSION['address_zip'], "UTF-8", $_SESSION['address_encoding']),
        "ADDRESS.CITY"                 => mb_convert_encoding($_SESSION['address_city'], "UTF-8", $_SESSION['address_encoding']),
        "ADDRESS.STATE"                => mb_convert_encoding($_SESSION['address_state'], "UTF-8", $_SESSION['address_encoding']),
        "CONTACT.PHONE"                => mb_convert_encoding($_SESSION['address_phone'], "UTF-8", $_SESSION['address_encoding']),
        "CONTACT.MOBILE"               => $_SESSION['address_mobile'] ?
            mb_convert_encoding($_SESSION['address_mobile'], "UTF-8", $_SESSION['address_encoding'])
            :
            mb_convert_encoding($_SESSION['address_phone'], "UTF-8", $_SESSION['address_encoding']),
        "ADDRESS.COUNTRY"              => "JP",
        "CONTACT.EMAIL"                => $_SESSION['regemail'],
    );

    if ($config['copyandpay_mode'] == 'TEST') {
        $data['TRANSACTION.MODE'] = "INTEGRATOR_TEST";
    } else {
        $data['TRANSACTION.MODE'] = "LIVE";
    };

    $_SESSION['copyandpay_token_request'] = $data;

    $params = array('http' => array(
        'method'  => 'POST',
        'content' => http_build_query($data),
    ));
    $ctx = stream_context_create($params);
    $fp = @fopen($url, 'rb', false, $ctx);
    if (!$fp) {
        throw new Exception("Problem with $url, $php_errormsg");
    }
    $response = @stream_get_contents($fp);
    if ($response === false) {
        throw new Exception("Problem reading data from $url, $php_errormsg");
    }

    $responseJson = json_decode($response, true);
    $token = $responseJson['transaction']['token'];
    if ($config['copyandpay_mode'] == 'TEST') {
        echo "<div class='alert'>Please note, Credit card processing in TEST mode, <br />no payment will be processed</div>";
    };

    return $token;
}

function setSessionVariables()
{
    if(count($_POST) > 0) {
        $_SESSION['address_encoding'] = "UTF-8";
        $_SESSION['address_first'] = $_POST['address_first'];
        $_SESSION['address_last'] = $_POST['address_last'];
        $_SESSION['address_street'] = $_POST['address_street'];
        $_SESSION['address_street2'] = $_POST['address_street2'];
        $_SESSION['address_zip'] = $_POST['address_zip'];
        $_SESSION['address_city'] = $_POST['address_city'];
        $_SESSION['address_state'] = $_POST['address_state'];
        $_SESSION['address_phone'] = $_POST['address_phone'];
        $_SESSION['address_mobile'] = $_POST['address_phone'];

        $_SESSION['jumpPlace'] = $_POST['jumpPlace'];
        $_SESSION['jumpDate'] = $_POST['jumpDate'];
        $_SESSION['chkInTime'] = $_POST['chkInTime'];
        $_SESSION['noOfJump'] = $_POST['noOfJump'];
        $_SESSION['lastname'] = $_POST['lastname'];
        $_SESSION['firstname'] = $_POST['firstname'];
        $_SESSION['regemail'] = $_POST['email'];
        $_SESSION['teleno'] = $_POST['teleno'];
        $_SESSION['total_price'] = $_POST['total_price'];
        $_SESSION['modeTransport'] = $_POST['modeTransport'];
        $_SESSION['payby'] = $_POST['payby'];

        $_SESSION['jump_place'] = $_POST['jumpPlace'];
        $_SESSION['jump_date'] = $_POST['jumpDate'];
        $_SESSION['check_in_time'] = $_POST['chkInTime'];

    } 
}

/**
 * @return int
 */
function getTotalRegistered()
{
    $total_registered = 0;
    $sql = "SELECT count(*) AS total FROM customerregs1
			WHERE
				ContactNo = '" . db_input($_POST['address_phone']) . "'
				AND DeleteStatus = 0
				AND Checked = 0
				AND bookingDate >= '" . date("Y-m-d") . "'";
    $res = mysql_query($sql);
    if ($row = mysql_fetch_assoc($res)) {
        $total_registered += $row['total'];
    };

    return $total_registered;
}

$total_registered = getTotalRegistered();

if ($total_registered > 0) {
    header("Location: failedCreditCardBooking.php?error_code=denied_phone");
    exit();
}


if (isset($_SESSION['backfrom']) && $_SESSION['backfrom'] == "paymentreturn") {
    $error_msg = $_SESSION['payment_msg'];
    unset($_SESSION['backfrom']);
    unset($_SESSION['payment_msg']);

} else {
    if (!isset($_POST['payby'])) {
        header('Location: index.php');
    }

    setSessionVariables();
}

$token = getTokenFromInpendium($config, $php_errormsg);

$languages = [
    'eng' => [
        '_enterPaymentInformation'  => "Enter Payment Information",
        '_paymentProcessingFailure' => "Payment processing failure. Please checking information below:",
        '_masterVisa'               => "MASTER VISA AMEX JCB",
        '_back'                     => "Back"
    ],
    'jpn' => [
        '_enterPaymentInformation'  => "支払情報",
        '_paymentProcessingFailure' => "支払い処理が失敗しました。　以下の詳細をご確認ください：",
        '_masterVisa'               => "MASTER VISA AMEX JCB",
        '_back'                     => "戻る"
    ],
];

$language = setLanguage();
extract($languages[$language]);
requireHeader($language);
?>
<?php //include 'includes/header_tags.php'; ?>
<style type="text/css">
    #ad {
        padding-top: 220px;
        padding-left: 10px;
    }

    .alert {
        display: block;
        background-color: yellow;
        color: red;
        font-weight: bold;
        text-align: center;
        margin-bottom: 20px;
    }
</style>

<script
    src="https://inpendium.net/frontend/widget/v3/widget.js?language=en&style=card">
</script>

<script type="text/javascript">
    function procBack() {
        document.backform.action = 'registration.php';
        document.backform.submit();
        return true;
    }
</script>
<div id="total-container">
    <table>
        <tbody>
        <tr>
            <td>
                <h3><font><b><?= $_enterPaymentInformation ?></b></font></h3>
            </td>
        </tr>
        <? if ($error_msg != "") { ?>
            <tr>
                <td>
                    <div>
                        <span><?= $_paymentProcessingFailure ?></span><br>
                        <p><?= $error_msg; ?></p></div>
                </td>
            </tr>

        <? } ?>
        <tr>
            <td>
                <form action="<?= getBaseUrl()?>paymentreturn.php" id="<?= $token; ?>">
                    <?= $_masterVisa ?>
                </form>
            </td>
        </tr>
        <tr>
            <td>
                <form name="backform" method="post" action="">
                    <input type="hidden" name="jumpPlace" value="<?= $_SESSION['jump_place']; ?>">
                    <input type="hidden" name="jumpDate" value="<?= $_SESSION['jump_date']; ?>">
                    <input type="hidden" name="chkInTime" value="<?= $_SESSION['check_in_time']; ?>">
                    <input type="hidden" name="noOfJump" value="<?= $_SESSION['noOfJump']; ?>">
                    <input type="hidden" name="lastname" value="<?= $_SESSION['lastname']; ?>">
                    <input type="hidden" name="firstname" value="<?= $_SESSION['firstname']; ?>">
                    <input type="hidden" name="email" value="<?= $_SESSION['regemail']; ?>">
                    <input type="hidden" name="teleno" value="<?= $_SESSION['teleno']; ?>">
                    <input type="hidden" name="modeTransport" value="<?= $_SESSION['modeTransport']; ?>">
                    <input type="hidden" name="payby" value="<?= $_SESSION['payby']; ?>">
                    <input type="hidden" name="backfrom" value="fromcard">
                    <input type="button" value="<?= $_back ?>" id="back" onClick="procBack();">
                </form>
            </td>
        </tr>
        </tbody>
    </table>
</div>
<? requireFooter($language) ?>
