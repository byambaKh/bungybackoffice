<?php
include("../includes/application_top.php");
require_once("functions.php");
//ini_set('display_errors', 'On');
//error_reporting(E_ALL & ~E_DEPRECATED & ~E_NOTICE);



function createBookingAndSendConfirmationEmail($creditCardSimulation, $lang, $jumpDate, $chkInTime, $noOfJump, $romajiname, $confirmCode, $config, $paidByCreditCard, $subdomain)
{
    if (!isset($creditCardSimulation)) $creditCardSimulation = false;
    if (!isset($lang)) $lang = 'jp';
    if ($lang == 'jp') {
        setlocale(LC_ALL, "ja_JP.sjis");
        $booking_dir = 'Yoyaku';
    } else {
        setlocale(LC_ALL, "en_GB.utf8");
        $booking_dir = 'Booking';
    }

    $booking_dir = "bookings";//ignore the old bookings directories for the new system

    $to = $_POST['email'];
    $jdate = DateTime::createFromFormat('Y-m-d', $jumpDate);
	$emailLink ="http://" . $_SERVER['HTTP_HOST'] . "/$booking_dir/Thankyou.php?passkey=$confirmCode"; 

    $replacement = array(
        'EMAIL_BOOK_DATE'         => mb_convert_encoding(strftime("%Y-%m-%d (%a)", $jdate->getTimestamp()), 'UTF-8', 'SJIS'),
        'EMAIL_BOOK_TIME'         => $chkInTime,
        'EMAIL_JUMP_NUMBER'       => $noOfJump,
        'EMAIL_CUSTOMER_NAME'     => strtoupper($romajiname),
        'EMAIL_CUSTOMER_PHONE'    => $_POST['teleno'],
        //'EMAIL_CONFIRMATION_LINK' => $emailLink 
        'EMAIL_CONFIRMATION_LINK' => "<a href='$emailLink'>$emailLink</a>"
    );
    $headers = "Return-Path: {$config['confirmation_email']}\r\n";
    //$headers .= 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
    mysql_query("SET NAMES UTF8;");

    //if paid figure out if the booking was paid or not was paid
    //paidByCreditCard is defined in the calling script paymentreturn.php
    if (!isset ($paidByCreditCard)) $paidByCreditCard = false;
    $template = $paidByCreditCard ? BJHelper::getPaidConfirmationEmailTemplate($lang) : BJHelper::getConfirmationEmailTemplate($lang);

    if ($template) {
        $file = explode("\n", $template);
        $subject = trim(array_shift($file));
        $body = implode("\n", $file);

    } else {
        mail("{$config['confirmation_email']}", "Cannot locate customer confirmation template for $lang.", print_r($replacement, true), $headers);

    }

    $body = str_replace(
        array_map(function ($el) {
            return '{' . $el . '}';
        }, array_keys($replacement)),
        array_values($replacement),
        $body
    );



    /*
    //Old mail code pre qr code embed
    $boundary = "next-part-" . rand(1000, 9999) . '-' . rand(100000, 999999);
    //$headers .= 'Content-Transfer-Encoding: base64' . "\r\n";
    $headers = "Return-Path: {$config['confirmation_email']}\r\n";
    $headers .= "Reply-To: $subdomain@bungyjapan.com\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: multipart/alternative;\r\n";
    $headers .= "\tboundary=\"$boundary\"\r\n";

    $content = chunk_split(base64_encode($body));

    $content = chunk_split(base64_encode($body));
    //$content = chunk_split(base64_encode($body));
    //$text_content = chunk_split(base64_encode(strip_tags($body) . "\n" . $replacement['EMAIL_CONFIRMATION_LINK']));
    $body = <<<EOT
--$boundary
Content-Type: text/plain; charset="UTF-8"
Content-Transfer-Encoding: base64

$content

--$boundary--
EOT;
    */
    $phone = db_input($_POST['teleno']);
    $time = db_input($_SESSION['chkInTime']);
    $qrString = json_encode([
        'tel'  => $phone,
        'time' => $chkInTime
    ]);

    $base64QRImgString = BookingQRCode::base64ImgString($qrString, true, 15);
    $body .= "<br><br>".$base64QRImgString;

    $boundary = "next-part-" . rand(1000, 9999) . '-' . rand(100000, 999999);
    //$headers .= 'Content-Transfer-Encoding: base64' . "\r\n";
    $headers = "Return-Path: {$config['confirmation_email']}\r\n";
    $headers .= "Reply-To: $subdomain@bungyjapan.com\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: multipart/alternative;\r\n";
    $headers .= "\tboundary=\"$boundary\"\r\n";

    $body = str_replace("\r\n", "<br>", $body);
    $body = str_replace("\n", "<br>", $body);
    $body = str_replace("\r", "<br>", $body);
    $content = chunk_split(base64_encode($body));

    $body = <<<EOT
--$boundary
Content-Type: text/html; charset="UTF-8"
Content-Transfer-Encoding: base64

$content

--$boundary--
EOT;

    mail($to, '=?UTF-8?B?' . base64_encode($subject) . '?=', $body, $headers, "-f {$config['confirmation_email']}");

}

function unsetSession()
{
    unset($_SESSION['jumpPlace']);
    unset($_SESSION['jumpDate']);
    unset($_SESSION['chkInTime']);
    unset($_SESSION['noOfJump']);
    unset($_SESSION['lastname']);
    unset($_SESSION['firstname']);
    unset($_SESSION['regemail']);
    unset($_SESSION['teleno']);
    unset($_SESSION['total_price']);
    unset($_SESSION['modeTransport']);
    unset($_SESSION['payby']);
    unset($_SESSION['discount']);
}

function handlePOST($jumpDate, $chkInTime, $noOfJump, $romajiname, $creditCardSimulation, $config, $paidByCreditCard, $subdomain)
{
    $confirmCode = createConfirmationCode();
    if ($_POST["lastname"] && $_POST["firstname"] && $_POST["email"] && $_POST["teleno"] && $_POST["modeTransport"] && !isset($_POST['backfrom'])
    ) {

        $emailOptOut = $_POST['email_opt_out'] == "on" ? 1 : 0;
        $booking = [
            'site_id'           => CURRENT_SITE_ID,
            'ConfirmCode'       => $confirmCode,
            'BookingDate'       => $jumpDate,
            'BookingTime'       => $chkInTime,
            'NoOfJump'          => $noOfJump,
            'RomajiName'        => strtoupper($romajiname),
            'CustomerLastName'  => strtoupper($_POST['lastname']),
            'CustomerFirstName' => strtoupper($_POST['firstname']),
            'ContactNo'         => $_POST['teleno'],
            'CustomerEmail'     => $_POST['email'],
            'TransportMode'     => $_POST['modeTransport'],
            'place_id'          => $_POST['jumpPlace'],
            'email_opt_out'     => $emailOptOut,
            'created'           => date('Y-m-d H:i:s'),
            'discount'          => applyDiscount($jumpDate),
            'discount_code'      => getDiscountCode()
        ];

        $result = db_perform('customerregs_temp1', $booking);

        if (mysql_insert_id() > 0) {
            $lang = 'jp';
            if ($_SESSION['language'] == 'eng') $lang = 'en';

            createBookingAndSendConfirmationEmail(
                $creditCardSimulation,
                $lang,
                $jumpDate,
                $chkInTime,
                $noOfJump,
                $romajiname,
                $confirmCode,
                $config,
                $paidByCreditCard,
                $subdomain
            );

            header('Location: regcomplete.php');
            exit();
        }
    }
}

function applyDiscount($jumpDate)
{
    if(isset($_SESSION['discount']))
    {
        if($_SESSION['discount'] == 'karaoke' && time() < strtotime('2019-03-15') && strtotime($jumpDate) < strtotime('2019-04-01'))
        {
            return 1000;
        }
    }
}

function getDiscountCode()
{
    if(isset($_SESSION['discount']))
    {
        return substr(mysql_real_escape_string($_SESSION['discount']),0,45);
    }
}

function createConfirmationCode()
{
    $confirmCode = md5(uniqid(rand()));
    return $confirmCode;
}

/**
 * @param $_firstName
 * @param $_lastName
 * @param $_postcode
 * @param $_prefecture
 * @param $_city
 * @param $_address
 * @param $_addressOther
 * @param $_phone
 */
function drawCreditCardInformationForm($_firstName, $_lastName, $_postcode, $_prefecture, $_city, $_address, $_addressOther, $_phone, $language)
{
    $japanese = "
                            $_firstName *<br/>
                            <input type='text' name='address_first' value='" . addslashes($_SESSION['address_first']) . "'/><br/>
                            $_lastName *<br/>
                            <input type='text' name='address_last' value='" . addslashes($_SESSION['address_last']) . "'/><br/>
                            $_postcode *<br/>
                            <input type='text' name='address_zip' value='" . addslashes($_SESSION['address_zip']) . "'/><br/>
                            $_prefecture *<br/>
                            <input type='text' name='address_state' value='" . addslashes($_SESSION['address_state']) . "'/><br/>
                            $_city *<br/>
                            <input type='text' name='address_city' value='" . addslashes($_SESSION['address_city']) . "'/><br/>
                            $_address *<br/>
                            <input type='text' name='address_street' value='" . addslashes($_SESSION['address_street']) . "'/><br/>
                            $_addressOther <br/>
                            <input type='text' name='address_street2' value='" . addslashes($_SESSION['address_street2']) . "'/><br/>
                            $_phone *<br/>
                            <input type='text' name='address_phone' value='" . addslashes($_SESSION['address_phone']) . "'/><br/>
                            <input type='hidden' name='address_mobile' value=''/><br/>";


    $english = "
                            $_firstName *<br/>
                            <input type='text' name='address_first' value='" . addslashes($_SESSION['address_first']) . "'/><br/>
                            $_lastName *<br/>
                            <input type='text' name='address_last' value='" . addslashes($_SESSION['address_last']) . "'/><br/>
                            $_address *<br/>
                            <input type='text' name='address_street' value='" . addslashes($_SESSION['address_street']) . "'/><br/>
                            $_addressOther <br/>
                            <input type='text' name='address_street2' value='" . addslashes($_SESSION['address_street2']) . "'/><br/>
                            $_city *<br/>
                            <input type='text' name='address_city' value='" . addslashes($_SESSION['address_city']) . "'/><br/>
                            $_prefecture *<br/>
                            <input type='text' name='address_state' value='" . addslashes($_SESSION['address_state']) . "'/><br/>
                            $_postcode *<br/>
                            <input type='text' name='address_zip' value='" . addslashes($_SESSION['address_zip']) . "'/><br/>
                            $_phone *<br/>
                            <input type='text' name='address_phone' value='" . addslashes($_SESSION['address_phone']) . "'/><br/>
                            <input type='hidden' name='address_mobile' value=''/><br/>";

    if ($language == 'jpn') return $japanese;
    else return $english;
}

$jumpPlace = $_POST['jumpPlace'];
$jumpDate = $_POST['jumpDate'];
$chkInTime = $_POST['chkInTime'];
$noOfJump = $_POST['noOfJump'];

if (!empty($_POST['lastname'])) {
    $lstname .= $_POST['lastname'];
}
if (!empty($_POST['firstname'])) {
    $frstname .= $_POST['firstname'];
}

$romajiname = $lstname . ' ' . $frstname;

handlePOST($jumpDate, $chkInTime, $noOfJump, $romajiname, $creditCardSimulation, $config, $paidByCreditCard, $subdomain);

$main_price = getFirstJumpRate(CURRENT_SITE_ID, $jumpDate);

$total_price = $noOfJump * $main_price;

$_SESSION['total_price'] = $total_price;

$languages = [
    'eng' => [
        "_pleaseFillInFirstName"      => "Please fill in your First Name!",
        "_pleaseFillInLastName"       => "Please fill in your Last Name!\"",
        "_notValidEmail"              => "Not a valid e-mail address!",
        "_telephoneShouldNotBe"       => "Telephone No. shouldn't be NULL or less than 9 or greater than 11",
        "_pleaseFillInTransportation" => "Please fill in your Transportation Mode!",
        "_pleaseFillInAddress"        => "Please fill in your Address",
        "_pleaseFillInPrefecture"     => "Please fill in your Phone Prefecture",
        "_pleaseFillInCity"           => "Please fill in your Phone City",
        "_pleaseFillInPostalCode"     => "Please fill in your Postal Code",
        "_PleaseFillInPhoneNumber"    => "Please fill in your Phone Number!",
        "_jumperRegistration"         => "Jumper Registration",
        "_jumpDate"                   => "Jump Date:",
        "_jumpTime"                   => "Jump Time:",
        "_numberOfJumpers"            => "Number of Jumpers:",
        "_firstName"                  => "First Name: [Use only English characters]",
        "_lastName"                   => "Last Name: [Use only English characters]",
        "_useOnlyEnglish"             => "[Use only English characters]",
        "_pleaseEnterAPCMail"         => "Please enter a PC mail address to which the confirmation email can be sent. Entering a mobile mail address may result in the system being unable to complete your booking.",
        "_email"                      => "Email:",
        "_confirmation"               => "The above email address will be used to confirm your booking",
        "_telephoneNo"                => "Telephone Number:[Use only numbers, no spaces or special characters]",
        "_useOnlyNumbers"             => "[Use only numbers; no spaces or special characters]",
        "_transportation"             => "Transportation:",
        "_select"                     => "Select",
        "_bulletTrain"                => "Bullet Train",
        "_localTrain"                 => "Local Train",
        "_car"                        => "Car",
        "_bus"                        => "Bus",
        "_motorbike"                  => "Motorbike",
        "_totalPrice"                 => "Total Price:",
        "_methodOfPayment"            => "Method of Payment:",
        "_onTheDay"                   => "On-the-day, by Cash",
        "_creditCard"                 => "Now, by Credit Card",
        "_billingInformation"         => "Billing Information",
        "_address"                    => "Address:",
        "_addressOther"               => "Address (Other):",
        "_allowEmail"                 => "Bungy Japan will send you promotional info about new jumps, new locations, and competitions. If you do not want to receive this info please check this box.",
        "_prefecture"                 => "Prefecture:",
        "_city"                       => "City:",
        "_postcode"                   => "Postcode:",
        "_phone"                      => "Phone:",
        "_yourCCStatement"            => "<strong>Your credit card statement will read:</strong><br/> AW*bungyjapan810278728133",
        "_returnPolicy"               => "<strong>Return Policy:</strong><br/> For cancellations made on the day before your scheduled jump, a 50% cancellation fee will be levied.<br/> For cancellations made on the day of your scheduled jump, a 100% cancellation fee will be levied.",
        "_allFieldsMandatory"         => "* All fields are mandatory",
        "_back"                       => "Back",
        "_bookNow"                    => "Book Now",
    ],

    'jpn' => [
        "_pleaseFillInFirstName"      => "名前をもう一度ご確認下さい！",
        "_pleaseFillInLastName"       => "名前をもう一度ご確認下さい！",
        "_notValidEmail"              => "Ｅ−ＭＡＩＬが必要です！",
        "_telephoneShouldNotBe"       => "電話番号をもう一度ご確認下さい！",//TODO this is not a clear explanation
        "_pleaseFillInTransportation" => "利用予定交通手段が必要です！",
        "_pleaseFillInAddress"        => "住所が入力されていません。",
        "_pleaseFillInPrefecture"     => "都道府県が入力されていません。",
        "_pleaseFillInCity"           => "市区町村が入力されていません。",
        "_pleaseFillInPostalCode"     => "郵便番号が入力されていません。",
        "_PleaseFillInPhoneNumber"    => "電話番号が入力されていません。",
        "_jumperRegistration"         => "お申込代表者様のご登録",
        "_jumpDate"                   => "ご予約日：",
        "_jumpTime"                   => "ご予約時間：",
        "_numberOfJumpers"            => "ご予約人数：",
        "_firstName"                  => "名<small>(ローマ字)</small>：",
        "_lastName"                   => "姓<small>(ローマ字)</small>：",
        "_useOnlyEnglish"             => "[ローマ字で記入]",
        "_pleaseEnterAPCMail"         => "お客様のPCアドレスを入力してください。携帯のアドレスを入力すると予約が完了できない可能性があります。",
        "_email"                      => "Eメール：",
        "_confirmation"               => "上記のEメールに予約確認メールが自動的に送信されます。",
        "_telephoneNo"                => "携帯電話番号：[数字のみ記入]",
        "_useOnlyNumbers"             => "[数字のみ]",
        "_transportation"             => "交通手段：",
        "_select"                     => "選択",
        "_bulletTrain"                => "新幹線",
        "_localTrain"                 => "普通電車",
        "_car"                        => "車",
        "_bus"                        => "バス",
        "_motorbike"                  => "バイク",
        "_totalPrice"                 => "合計金額：",
        "_methodOfPayment"            => "支払い方法：",
        "_onTheDay"                   => "当日現金払い",
        "_creditCard"                 => "クレジットカード前払い",
        "_billingInformation"         => "ご請求先情報",
        "_address"                    => "番地以降：",
        "_addressOther"               => "ビル名等：",
        "_allowEmail"                 => "Bungy Japanの新しいロケ、ジャンプ仕方と大会の情報をお受け取りになりたくない場合は、チェックボックスにチェックを入れてください。",
        "_prefecture"                 => "都道府県：",
        "_city"                       => "市区町村：",
        "_phone"                      => "電話番号：",
        "_postcode"                   => "郵便番号",
        "_yourCCStatement"            => "<strong>クレジットカード明細書には以下のように記載されます：</strong><br/> 「AW*bungyjapan810278728133」",
        "_returnPolicy"               => "<strong>キャンセル料金について：</strong><br/> キャンセル料は前日キャンセルの場合ジャンプ料金の50%, 当日の場合100%となりますのでご了承下さい。",
        "_allfieldsmandatory"         => "*印は必須項目です",//not there
        "_back"                       => "戻る",
        "_bookNow"                    => "登録する",
    ],
    'chn' => [
        "_pleaseFillInFirstName"        => "",
        "_pleaseFillInLastName"         => "",
        "_notValidEmail"                => "",
        "_telephoneShouldNotBe"         => "",
        "_pleaseFillInInTransportation" => "",
        "_pleaseFillInInAddress"        => "",
        "_pleaseFillInInPrefecture"     => "",
        "_pleaseFillInInCity"           => "",
        "_pleaseFillInInPostalCode"     => "",
        "_PleaseFillInPhoneNumber"      => "",
        "_jumperRegistration"           => "",
        "_jumpDate"                     => "",
        "_numberOfJumpers"              => "",
        "_firstName"                    => "",
        "_lastName"                     => "",
        "_useOnlyEnglish"               => "",
        "_pleaseEnterAPCMail"           => "",
        "_email"                        => "",
        "_telephoneNo"                  => "",
        "_useOnlyNumbers"               => "",
        "_transportation"               => "",
        "_select"                       => "",
        "_bulletTrain"                  => "",
        "_localTrain"                   => "",
        "_Car"                          => "",
        "_Bus"                          => "",
        "_Motorbike"                    => "",
        "_totalPrice"                   => "",
        "_methodOfPayment"              => "",
        "_onTheDay"                     => "",
        "_creditCard"                   => "",
        "_billingInformation"           => "",
        "_address"                      => "",
        "_addressOther"                 => "",
        "_prefecture"                   => "",
        "_city"                         => "",
        "_postcode"                     => "",
        "_yourCCStatement"              => "",
        "_returnPolicy"                 => "",
        "_allfieldsmandatory"           => ""
    ]
];

$language = setLanguage();
extract($languages[$language]);


requireHeader($language);
?>
<script type="text/javascript">
    function validate() {

        var x = document.regform.email.value;
        var phNoLen = document.regform.teleno.value.length;
        var atpos = x.indexOf("@");
        var dotpos = x.lastIndexOf(".");


        if (document.regform.firstname.value == '') {
            alert('<?=$_pleaseFillInFirstName?>');
            document.regform.firstname.focus();
            return false;
        }
        if (document.regform.lastname.value == '') {
            alert('<?=$_pleaseFillInLastName?>');
            document.regform.lastname.focus();
            return false;
        }

        if (atpos < 1 || dotpos < atpos + 2 || dotpos + 2 >= x.length || x == '') {
            alert("<?=$_notValidEmail?>");
            document.regform.email.focus();
            return false;
        }

        if (document.regform.teleno.value.match(/^\s*$/) || phNoLen == 0 || phNoLen < 10) {
            alert("<?=$_telephoneShouldNotBe?>");
            document.regform.teleno.focus();
            return false;
        }

        if (document.regform.modeTransport.value == '-1') {
            alert('<?=$_pleaseFillInTransportation?>');
            document.regform.modeTransport.focus();
            return false;
        }

        <?php if ($_SERVER['REMOTE_ADDR'] == "192.241.171.20" || $config['copyandpay_enable'] == 'YES') { ?>

        if (document.regform.payby[1].checked == true) {
            document.regform.action = '<?=getBaseUrl()?>enterpayment.php';
            document.regform.address_phone.value = document.regform.address_phone.value.replace(/\D/g, '');
            document.regform.address_mobile.value = document.regform.address_mobile.value.replace(/\D/g, '');
            if (document.regform.address_first.value == '') {
                alert('<?=$_pleaseFillInFirstName?>');
                document.regform.address_first.focus();
                return false;
            }
            if (document.regform.address_last.value == '') {
                alert('<?=$_pleaseFillInLastName?>');
                document.regform.address_last.focus();
                return false;
            }
            if (document.regform.address_street.value == '') {
                alert('<?=$_pleaseFillInAddress?>');
                document.regform.address_street.focus();
                return false;
            }
            if (document.regform.address_city.value == '') {
                alert('<?=$_pleaseFillInCity?>');
                document.regform.address_city.focus();
                return false;
            }
            if (document.regform.address_state.value == '') {
                alert('<?=$_pleaseFillInPrefecture?>');
                document.regform.address_state.focus();
                return false;
            }
            if (document.regform.address_zip.value == '') {
                alert('<?=$_pleaseFillInPostalCode?>');
                document.regform.address_zip.focus();
                return false;
            }
            if (document.regform.address_phone.value == '') {
                alert('<?=$_pleaseFillInPhoneNumber?>');
                document.regform.address_phone.focus();
                return false;
            }
        } else {
            document.regform.action = '';
        }
        <?php }; ?>

        document.regform.submit();
        return true;
    }

    function procType() {
        var flen = document.regform.firstname.value.length;
        for (var i = 0; i < flen; i++) {
            if (document.regform.firstname.value.charCodeAt(i) > 255) {
                //alert('Please fill in your First Name in ENGLISH!');
                document.regform.firstname.value = "";
                document.regform.firstname.focus();
                return false;
            }
        }

    }

    function procType1() {
        var llen = document.regform.lastname.value.length;
        for (var i = 0; i < llen; i++) {
            if (document.regform.lastname.value.charCodeAt(i) > 255) {
                //alert('Please fill in your Last Name in ENGLISH!');
                document.regform.lastname.value = "";
                document.regform.lastname.focus();
                return false;
            }
        }

    }

    function goback() {
        if (document.getElementById('back')) {
            document.regform.action = "searchIndex.php";
            document.regform.submit();
            return true;
        }
    }

    function procBack() {
        document.regform.action = 'disclaimer.php';
        document.regform.submit();
        return true;
    }
</script>

<?= drawProgressForIndexAndLanguage(4, $language) ?>
<div id="content-container">
    <form name="regform" method="post" action="">
        <input type="hidden" name="jumpPlace" value="<?= $jumpPlace ?>">
        <input type="hidden" name="jumpDate" value="<?= $jumpDate ?>">
        <input type="hidden" name="chkInTime" value="<?= $chkInTime ?>">
        <input type="hidden" name="noOfJump" value="<?= $noOfJump ?>">
        <table>
            <thead>
            </thead>
            <tbody>
            <tr class="underline">
                <td colspan="3">
                    <h3><?= $_jumperRegistration ?></h3>
                </td>
            </tr>
            <tr class="underline">
                <td><?= $_jumpDate ?></td>
                <td colspan="2"><strong><?= date_format(new DateTime($jumpDate), 'Y-m-d l'); ?></strong></td>
                <!--<td></td>-->
            </tr>
            <tr class="underline">
                <td><?= $_jumpTime ?></td>
                <td colspan="2"><strong><?= str_replace('.', ':', $chkInTime); ?></strong></td>
                <!--<td></td>-->
            </tr>
            <tr>
                <td><?= $_numberOfJumpers ?></td>
                <td colspan="2"><strong><?= $noOfJump; ?></strong></td>
                <!--<td></td>-->
            </tr>
            </tbody>
        </table>
        <div class="form-bounds">
            <table>
                <thead>
                </thead>
                <tbody>
                <tr>
                    <td colspan="3"><?= $_firstName ?><br>
                        <input type="text" name="firstname" value="<?= $firstname ?>">
                    </td>
                </tr>
                <tr>
                    <td colspan="3"><?= $_lastName ?><input type="text" name="lastname" value=""></td>
                </tr>

                <tr>
                    <td colspan="3"><span class="emphasis"><?= $_pleaseEnterAPCMail ?> </span></td>
                </tr>
                <tr>
                    <td colspan="3"><?= $_email ?><br><input type="text" name="email" value=""></td>
                </tr>
                <tr>
                    <td colspan="3"><span class="emphasis"><?= $_confirmation ?></span></td>
                </tr>
                <tr>
                    <td colspan="3"><?= $_telephoneNo ?><input type="text" class="numeric-input" name="teleno" value="">
                    </td>
                </tr>
                <tr>
                    <input type="hidden" name="email_opt_out" value="off">

                    <td colspan="3"><input type="checkbox" name="email_opt_out">&nbsp;&nbsp;<?= $_allowEmail ?></td>
                </tr>
                <tr>
                    <td><?= $_transportation ?></td>
                    <td colspan="2">
                        <select name="modeTransport" id="modeTransport">
                            <option value="-1" id="-1">--<?= $_select ?>--</option>
                            <option value="Bullet Train"><?= $_bulletTrain ?></option>
                            <option value="Local Train"><?= $_localTrain ?></option>
                            <option value="Car"><?= $_car ?></option>
                            <option value="Bus"><?= $_bus ?></option>
                            <option value="Motorbike"><?= $_motorbike ?></option>
                        </select>
                    </td>
                </tr>
                </tbody>
            </table>
        </div>
        <table>
            <thead>
            </thead>
            <tbody>
            <?php

            if ($_SERVER['REMOTE_ADDR'] == "192.241.171.20" || $config['copyandpay_enable'] == 'YES') { ?>
            <tr>
                <td><strong><?= $_totalPrice ?></strong></td>

                <td colspan="2">
                    <input type="hidden" name="total_price" value="<?= $total_price ?>">
                    <strong>&yen; <?= number_format($total_price); ?></strong>
                </td>
            </tr>
            <tr>
                <td><?= $_methodOfPayment ?></td>
                <td colspan="2">

                    <?php
                    $val_pay_by = "cash";
                    if ((isset($_POST['payby']) && $_POST['payby'] == "card") || (isset($_SESSION['payby']) && $_SESSION['payby'] == "card")) {
                        $val_pay_by = "card";
                    }
                    ?>
                    <input type="radio" name="payby" id="paycash" value="cash" <?php if ($val_pay_by == "cash") echo "checked"; ?>>
                    &nbsp;
                    &nbsp;
                    <img src="/img/payment-cash.png"/>
                    <label for="paycash" id="paycashlabel"><?= $_onTheDay ?></label>
                    <br/>
                    <input type="radio" name="payby" id="paycard" value="card" <?php if ($val_pay_by == "card") echo "checked"; ?>>
                    &nbsp;
                    &nbsp;
                    <img src="/img/payment-card.png"/>
                    <label for="paycard" id="paycardlabel"><?= $_creditCard ?></label>
                </td>
            </tr>
            </tbody>
        </table>
        <table>
            <tbody>
            <tr>
                <td colspan="3">
                    <div id="ccinfo" class="form-bounds">
                        <strong><h4><?= $_billingInformation ?></h4></strong>
                        <div><?php

                            echo drawCreditCardInformationForm(
                                $_firstName,
                                $_lastName,
                                $_postcode,
                                $_prefecture,
                                $_city,
                                $_address,
                                $_addressOther,
                                $_phone,
                                $language
                            );

                            ?>
                            &nbsp;<?= $_allFieldsMandatory ?>
                            <br>
                            <br>
                        </div>
                        <?= $_yourCCStatement ?>
                        <br>
                        <br>
                        <?= $_returnPolicy ?>
                    </div>
                </td>
            </tr>
            <tr>
                <td colspan="3">
                </td>
            </tr>
            <?php }; ?>
            <tr>
                <td colspan="3">
                    <input class="button wide-button" type="button" value="<?= $_bookNow ?>" onClick="procType();procType1();validate();">
                </td>
            </tr>
            <tr>
                <td colspan="3">
                    <input class="button wide-button back" type="button" value="<?= $_back ?>" id="back" onClick="procBack();">
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
<script>
    var methodChanged = function () {
        if (document.getElementById('paycard').checked) {
            document.getElementById('ccinfo').style.display = "block";
            if (document.regform.address_first.value == "") {
                document.regform.address_first.value = document.regform.firstname.value;
            }
            if (document.regform.address_last.value == "") {
                document.regform.address_last.value = document.regform.lastname.value;
            }
            if (document.regform.address_phone.value == "") {
                document.regform.address_phone.value = document.regform.teleno.value;
            }
        } else {
            document.getElementById('ccinfo').style.display = "none";
        }
    };
    document.getElementById('paycard').onchange = methodChanged;
    document.getElementById('paycash').onchange = methodChanged;
    document.getElementById('paycard').onmouseup = methodChanged;
    document.getElementById('paycash').onmouseup = methodChanged;
    document.getElementById('paycard').onkeyup = methodChanged;
    document.getElementById('paycash').onkeyup = methodChanged;
    document.getElementById('paycardlabel').onclick = methodChanged;
    document.getElementById('paycashlabel').onclick = methodChanged;
    setTimeout(methodChanged, 100);

    jQuery(document).ready(function () {
        jQuery(".numeric-input").keydown(function (e) {
            // Allow: backspace, delete, tab, escape, enter and .
            if (jQuery.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
                    // Allow: Ctrl+A
                (e.keyCode == 65 && e.ctrlKey === true) ||
                    // Allow: Ctrl+C
                (e.keyCode == 67 && e.ctrlKey === true) ||
                    // Allow: Ctrl+X
                (e.keyCode == 88 && e.ctrlKey === true) ||
                    // Allow: home, end, left, right
                (e.keyCode >= 35 && e.keyCode <= 39)) {
                // let it happen, don't do anything
                return;
            }
            // Ensure that it is a number and stop the keypress
            if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
                e.preventDefault();
            }
        });
    });

</script>
<? requireFooter($language); ?>

