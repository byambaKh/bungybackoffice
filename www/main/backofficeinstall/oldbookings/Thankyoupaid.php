<?php
include("../includes/application_top.php");
require("functions.php");
if (!isset($creditCardSimulation)) $creditCardSimulation = false;
$language = setLanguage();
requireHeader($language);

?>
<?php include 'includes/header_tags.php'; ?>
<script src="../js/bookings/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
<script src="../js/bookings/jqueryui/1.10.1/jquery-ui.min.js" type="text/javascript"></script>
<script src="/js/core.js" type="text/javascript"></script>

</head>
<div id="total-container">

    <?php
    $languageCodes = [
        'jpn' => 'jp',
        'eng' => 'en',
    ];

    $languageCode = $languageCodes[$language];

    if (!empty($error_code)) {
        if($error_code == 'success') echo drawProgressForIndexAndLanguage(5, $language);
        if ($content = BJHelper::getTemplate($error_code, $languageCode)) {
            echo mb_convert_encoding($content, 'UTF8','SJIS');
        } else {
            echo "Error happen: " . $error_code;
        }
    } else {
        $content = BJHelper::getTemplate('success_paid', $languageCode);
        echo mb_convert_encoding($content, 'UTF8','SJIS');
    }
    ?>
</div>
<? requireFooter($language); ?>
