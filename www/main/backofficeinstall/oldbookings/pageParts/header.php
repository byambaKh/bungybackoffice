<?php
$languages = [
    'eng' => [
        '_minakamiBungy'         => 'Minakami Bungy (42m)',
        '_sarugakyoBungy'        => 'Sarugakyo Bungy (62m)',
        '_ryujinBungy'           => 'Ryujin Bungy (100m)',
        '_itsukiBungy'           => 'Itsuki Bungy (66m)',
        '_fujiBungy'             => 'Fuji Bungy (54m)',
        '_kaiunBungy'             => 'Kaiun Bungy (30m)',
        '_yambaBungy'             => 'Yamba Bungy (106m)',


        '_minakamiReservations'  => 'Minakami Reservations',
        '_sarugakyoReservations' => 'Sarugakyo Reservations',
        '_ryujinReservations'    => 'Ryujin Reservations',
        '_itsukiReservations'    => 'Itsuki Reservations',
        '_fujiReservations'		 => 'Fuji Reservations',
        '_kaiunReservations'     => 'Kaiun Reservations',
        '_yambaReservations'	 => 'Yamba Reservations',

        '_bungyJapan'            => 'Bungy Japan',
        '_whatIs'                => 'What is Bungy Jumping?',
        '_gallery'               => 'Gallery',
        '_faq'                   => 'FAQ',
        '_contact'               => 'Contact',
        '_menu'                  => 'Menu',
        '_booking'               => 'Booking',
        '_ourBridges'            => 'Our Bridges',
        '_about'                 => 'About',
        '_home'                  => 'Home',
        '_privacyPolicy'         => 'Privacy Policy',

    ],
    'jpn' => [
        '_minakamiBungy'         => 'みなかみバンジー(42m)',
        '_sarugakyoBungy'        => '猿ヶ京バンジー(62m)',
        '_ryujinBungy'           => '竜神バンジー(100m)',
        '_itsukiBungy'           => '五木村バンジー(66m)',
        '_fujiBungy'		     => '富士バンジー(54m)',
        '_kaiunBungy'            => '開運(奈良県)バンジー(30m)',
        '_yambaBungy'            => '八ッ場バンジー(106m)',



        '_minakamiReservations'  => 'みなかみバンジー(42m)',
        '_sarugakyoReservations' => '猿ヶ京バンジー(62m)',
        '_ryujinReservations'    => '竜神バンジー(100m)',
        '_itsukiReservations'    => '五木村バンジー(66m)',
        '_kaiunReservations'	 => '開運(奈良県)バンジー(30m)',
        '_fujiReservations'      => '富士バンジー(54m)',
        '_yambaReservations'	 => '八ッ場バンジー(106m)',

        '_bungyJapan' => 'Bungy Japanについて',
        '_whatIs'     => 'バンジージャンプとは?',
        '_gallery'    => 'ギャラリー',
        '_faq'        => 'Q&amp;A',
        '_contact'    => 'お問い合わせ',
        '_menu'       => 'メニュー',
        '_booking'    => 'ご予約',
        '_ourBridges' => 'ブリッジ',
        '_about'      => 'バンジージャンプとは'
    ],
];

function drawLanguageSwitcher($language)
{

    $switcher = '';
    if ($language == 'jpn') {
        $switcher = " <a href='?lang=eng' class='lang_sel_sel icl-en'> <img class='iclflag' src='theme_files/en.png' alt='en' title='English'> &nbsp;</a> ";
    } else {
        $switcher = "<a href='?lang=jpn' class='lang_sel_sel icl-ja'> <img class='iclflag' src='theme_files/ja.png' alt='ja' title='日本語'>&nbsp; </a>";
    }

    return "
    <li>
        <ul>
            <li class='icl-ja'>
            $switcher
            </li>
        </ul>
    </li>
    ";

}

$lang = convertLanguageCodes($language);//used for language links in wordpress
extract($languages[$language]);

?>
<!DOCTYPE html>
<!--[if IE 8]>
<html class="no-js ie8" lang="en-US"><![endif]-->
<!--[if IE 9]>
<html class="no-js ie9" lang="en-US"><![endif]-->
<!--[if gt IE 9]><!-->
<html style="" class=" js no-touch" lang="en-US"><!--<![endif]-->
<head>
    <meta http-equiv="content-type" content="text/html; charset=UTF-8">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=0"/>
    <title>Booking | Bungy Japan</title>

    <link rel="stylesheet" href="theme_files/language-selector.css" type="text/css" media="all">
    <link rel="profile" href="https://gmpg.org/xfn/11">
    <link rel="pingback" href="https://www.bungyjapan.com/xmlrpc.php">
    <link href="theme_files/css_002.css" rel="stylesheet" type="text/css">
    <meta name="robots" content="noindex,follow">
    <link rel="alternate" type="application/rss+xml" title="Bungy Japan » Feed" href="https://www.bungyjapan.com/feed/?lang=<?= $lang ?>">
    <link rel="alternate" type="application/rss+xml" title="Bungy Japan » Comments Feed" href="https://www.bungyjapan.com/comments/feed/?lang=<?= $lang ?>">
    <link rel="stylesheet" id="sb_instagram_styles-css" href="theme_files/sb-instagram.css" type="text/css" media="all">
    <link rel="stylesheet" id="sb_instagram_icons-css" href="theme_files/font-awesome_002.css" type="text/css" media="all">
    <link rel="stylesheet" id="responsive-lightbox-nivo-front-css" href="theme_files/nivo-lightbox.css" type="text/css" media="all">
    <link rel="stylesheet" id="responsive-lightbox-nivo-front-template-css" href="theme_files/default.css" type="text/css" media="all">
    <link rel="stylesheet" id="rs-plugin-settings-css" href="theme_files/settings.css" type="text/css" media="all">
    <style type="text/css">
        .tp-caption a {
            color: #ff7302;
            text-shadow: none;
            -webkit-transition: all 0.2s ease-out;
            -moz-transition: all 0.2s ease-out;
            -o-transition: all 0.2s ease-out;
            -ms-transition: all 0.2s ease-out;
        }

        .tp-caption a:hover {
            color: #ffa902;
        }
    </style>
    <link rel="stylesheet" id="rs-captions-css" href="theme_files/dynamic-captions.css" type="text/css" media="all">
    <link rel="stylesheet" id="x-shortcodes-integrity-light-css" href="theme_files/integrity-light.css" type="text/css" media="all">
    <link rel="stylesheet" id="magnific-popup-au-css" href="theme_files/magnific-popup.css" type="text/css" media="all">
    <link rel="stylesheet" id="youtube-channel-css" href="theme_files/youtube-channel.css" type="text/css" media="all">
    <link rel="stylesheet" id="x-integrity-light-css" href="theme_files/integrity-light_002.css" type="text/css" media="all">
    <link rel="stylesheet" id="x-font-standard-css" href="theme_files/css.css" type="text/css" media="all">
    <link rel="stylesheet" id="dashicons-css" href="theme_files/dashicons.css" type="text/css" media="all">
    <link rel="stylesheet" id="thickbox-css" href="theme_files/thickbox.css" type="text/css" media="all">
    <link rel="stylesheet" id="front_end_youtube_style-css" href="theme_files/baze_styles_youtube.css" type="text/css" media="all">
    <script src="theme_files/fbevents.js" async=""></script>
    <script async="" src="theme_files/www-widgetapi.js" id="www-widgetapi-script" type="text/javascript"></script>
    <script type="text/javascript" src="theme_files/jquery.js"></script>
    <script type="text/javascript" src="theme_files/jquery_004.js"></script>
    <script type="text/javascript" src="theme_files/jquery-migrate.js"></script>
    <script type="text/javascript" src="theme_files/nivo-lightbox.js"></script>
    <script type="text/javascript">
        /* <![CDATA[ */
        var rlArgs = {
            "script": "nivo",
            "selector": "lightbox",
            "custom_events": "",
            "activeGalleries": "1",
            "effect": "fade",
            "keyboardNav": "1",
            "errorMessage": "The requested content cannot be loaded. Please try again later."
        };
        /* ]]> */
    </script>
    <script type="text/javascript" src="theme_files/front.js"></script>
    <script type="text/javascript" src="theme_files/jquery_002.js"></script>
    <script type="text/javascript" src="theme_files/backstretch-2.js"></script>
    <script type="text/javascript" src="theme_files/modernizr-2.js"></script>
    <script type="text/javascript" src="theme_files/x.js"></script>
    <script type="text/javascript" src="theme_files/jplayer-2.js"></script>
    <script type="text/javascript" src="theme_files/jquery-ui-1.js"></script>
    <script type="text/javascript" src="theme_files/imagesloaded-3.js"></script>
    <script type="text/javascript" src="theme_files/video-4.js"></script>
    <script type="text/javascript" src="theme_files/bigvideo-1.js"></script>
    <script type="text/javascript" src="theme_files/comment-reply.js"></script>
    <script type="text/javascript" src="theme_files/youtube_embed_front_end.js"></script>
    <script type="text/javascript" src="theme_files/iframe_api"></script>
    <script type='text/javascript' src='theme_files/external-tracking.min.js?ver=6.4.9'></script>
    <link rel="shortlink" href="https://www.bungyjapan.com/?p=6674">
    <meta name="generator" content="WPML ver:3.1.4 stt:1,63,28;0">
    <link rel="alternate" hreflang="en-US" href="https://www.bungyjapan.com/faq/?lang=<?= $lang ?>">
    <link rel="alternate" hreflang="ja" href="https://www.bungyjapan.com/faq-2/">
    <!-- Google Analytics Tracking by Google Analyticator 6.4.9: https://www.videousermanuals.com/google-analyticator/ -->
    <script type="text/javascript">
        var analyticsFileTypes = [''];
        var analyticsSnippet = 'disabled';
        var analyticsEventTracking = 'enabled';
    </script>
    <script type="text/javascript">
        (function (i, s, o, g, r, a, m) {
            i['GoogleAnalyticsObject'] = r;
            i[r] = i[r] || function () {
                    (i[r].q = i[r].q || []).push(arguments)
                }, i[r].l = 1 * new Date();
            a = s.createElement(o),
                m = s.getElementsByTagName(o)[0];
            a.async = 1;
            a.src = g;
            m.parentNode.insertBefore(a, m)
        })(window, document, 'script', '//www.google-analytics.com/analytics.js', 'ga');
        ga('create', 'UA-32182202-1', 'auto');
        ga('require', 'linkid', 'linkid.js');

        ga('send', 'pageview');
    </script>
    <style type="text/css">#lang_sel ul li {
            background-color: white;
        }
    </style>
    <link rel="shortcut icon" href="https://www.bungyjapan.com/wp-content/themes/x/framework/img/global/fav.png">
    <link rel="apple-touch-icon-precomposed" href="https://www.bungyjapan.com/wp-content/uploads/2014/03/touch-icon.png">
    <meta name="msapplication-TileColor" content="#ce000c">
    <meta name="msapplication-TileImage" content="/wp-content/uploads/2014/03/tile-icon1.png">
    <meta name="generator" content="Powered by Visual Composer - drag and drop page builder for WordPress.">
    <style type="text/css">
        body {
            background: #f3f3f3 url(theme_files/pat.png) center top repeat;
        }

        a, h1 a:hover, h2 a:hover, h3 a:hover, h4 a:hover, h5 a:hover, h6 a:hover, .x-topbar .p-info a:hover, .x-breadcrumb-wrap a:hover, .widget ul li a:hover, .widget ol li a:hover, .widget.widget_text ul li a, .widget.widget_text ol li a, .widget_nav_menu .current-menu-item > a, .x-widgetbar .widget ul li a:hover, .x-twitter-widget ul li a, .x-accordion-heading .x-accordion-toggle:hover, .x-comment-author a:hover, .x-comment-time:hover, .x-close-content-dock:hover i {
            color: #ff2a13;
        }

        a:hover, .widget.widget_text ul li a:hover, .widget.widget_text ol li a:hover, .x-twitter-widget ul li a:hover, .x-recent-posts a:hover .h-recent-posts {
            color: #d80f0f;
        }

        a.x-img-thumbnail:hover, .x-slider-revolution-container.below, .page-template-template-blank-3-php .x-slider-revolution-container.above, .page-template-template-blank-6-php .x-slider-revolution-container.above {
            border-color: #ff2a13;
        }

        .entry-thumb:before, .pagination span.current, .flex-direction-nav a, .flex-control-nav a:hover, .flex-control-nav a.flex-active, .jp-play-bar, .jp-volume-bar-value, .x-dropcap, .x-skill-bar .bar, .x-pricing-column.featured h2, .h-comments-title small, .x-entry-share .x-share:hover, .x-highlight, .x-recent-posts .x-recent-posts-img, .x-recent-posts .x-recent-posts-img:before, .tp-bullets.simplebullets.round .bullet:hover, .tp-bullets.simplebullets.round .bullet.selected, .tp-bullets.simplebullets.round-old .bullet:hover, .tp-bullets.simplebullets.round-old .bullet.selected, .tp-bullets.simplebullets.square-old .bullet:hover, .tp-bullets.simplebullets.square-old .bullet.selected, .tp-bullets.simplebullets.navbar .bullet:hover, .tp-bullets.simplebullets.navbar .bullet.selected, .tp-bullets.simplebullets.navbar-old .bullet:hover, .tp-bullets.simplebullets.navbar-old .bullet.selected, .tp-leftarrow.default, .tp-rightarrow.default {
            background-color: #ff2a13;
        }

        .x-nav-tabs > .active > a, .x-nav-tabs > .active > a:hover {
            -webkit-box-shadow: inset 0 3px 0 0 #ff2a13;
            box-shadow: inset 0 3px 0 0 #ff2a13;
        }

        .x-recent-posts a:hover .x-recent-posts-img, .tp-leftarrow.default:hover, .tp-rightarrow.default:hover {
            background-color: #d80f0f;
        }

        .x-main {
            width: 69.536945%;
        }

        .x-sidebar {
            width: 25.536945%;
        }

        .x-topbar {
            background-color: transparent;
        }

        .x-navbar .x-nav > li > a:hover, .x-navbar .x-nav > .current-menu-item > a {
            -webkit-box-shadow: inset 0 4px 0 0 #ff2a13;
            box-shadow: inset 0 4px 0 0 #ff2a13;
        }

        body.x-navbar-fixed-left-active .x-navbar .x-nav > li > a:hover, body.x-navbar-fixed-left-active .x-navbar .x-nav > .current-menu-item > a {
            -webkit-box-shadow: inset 8px 0 0 0 #ff2a13;
            box-shadow: inset 8px 0 0 0 #ff2a13;
        }

        body.x-navbar-fixed-right-active .x-navbar .x-nav > li > a:hover, body.x-navbar-fixed-right-active .x-navbar .x-nav > .current-menu-item > a {
            -webkit-box-shadow: inset -8px 0 0 0 #ff2a13;
            box-shadow: inset -8px 0 0 0 #ff2a13;
        }

        .x-topbar .p-info, .x-topbar .p-info a, .x-navbar .x-nav > li > a, .x-nav-collapse .sub-menu a, .x-breadcrumb-wrap a, .x-breadcrumbs .delimiter {
            color: #b7b7b7;
        }

        .x-navbar .x-nav > li > a:hover, .x-navbar .x-nav > .current-menu-item > a, .x-navbar .x-navbar-inner .x-nav-collapse .x-nav > li > a:hover, .x-navbar .x-navbar-inner .x-nav-collapse .x-nav > .current-menu-item > a, .x-navbar .x-navbar-inner .x-nav-collapse .sub-menu a:hover {
            color: #272727;
        }

        .rev_slider_wrapper {
            border-bottom-color: #ff2a13;
        }

        .x-navbar-static-active .x-navbar .x-nav > li > a, .x-navbar-fixed-top-active .x-navbar .x-nav > li > a {
            height: 90px;
            padding-top: 34px;
        }

        .x-navbar-fixed-left-active .x-navbar .x-nav > li > a, .x-navbar-fixed-right-active .x-navbar .x-nav > li > a {
            padding-top: 19px;
            padding-bottom: 19px;
            padding-left: 7%;
            padding-right: 7%;
        }

        .sf-menu li:hover ul, .sf-menu li.sfHover ul {
            top: 75px;;
        }

        .sf-menu li li:hover ul, .sf-menu li li.sfHover ul {
            top: -0.75em;
        }

        .x-navbar-fixed-left-active .x-widgetbar {
            left: 235px;
        }

        .x-navbar-fixed-right-active .x-widgetbar {
            right: 235px;
        }

        .x-container-fluid.width {
            width: 88%;
        }

        .x-container-fluid.max {
            max-width: 1200px;
        }

        .x-comment-author, .x-comment-time, .comment-form-author label, .comment-form-email label, .comment-form-url label, .comment-form-rating label, .comment-form-comment label, .widget_calendar #wp-calendar caption, .widget_calendar #wp-calendar th, .widget_calendar #wp-calendar #prev, .widget_calendar #wp-calendar #next, .widget.widget_recent_entries li a, .widget_recent_comments a:last-child, .widget.widget_rss li .rsswidget {
            font-weight: 400;
        }

        @media (max-width: 979px) {
            .x-navbar-fixed-left .x-container-fluid.width, .x-navbar-fixed-right .x-container-fluid.width {
                width: 88%;
            }

            .x-nav-collapse .x-nav > li > a:hover, .x-nav-collapse .sub-menu a:hover {
                -webkit-box-shadow: none;
                box-shadow: none;
            }

            .x-navbar-fixed-left-active .x-widgetbar {
                left: 0;
            }

            .x-navbar-fixed-right-active .x-widgetbar {
                right: 0;
            }
        }

        body {
            font-size: 14px;
            font-weight: 400;
        }

        a:focus, select:focus, input[type="file"]:focus, input[type="radio"]:focus, input[type="checkbox"]:focus {
            outline: thin dotted #333;
            outline: 5px auto #ff2a13;
            outline-offset: -1px;
        }

        h1, h2, h3, h4, h5, h6, .h1, .h2, .h3, .h4, .h5, .h6 {
            font-weight: 400;
            letter-spacing: -1px;
        }

        .entry-header, .entry-content {
            font-size: 14px;
        }

        .x-brand {
            font-weight: 400;
            letter-spacing: -3px;
        }

        .x-brand img {
            width: 125px;
        }

        .x-main.full {
            float: none;
            display: block;
            width: auto;
        }

        @media (max-width: 979px) {
            .x-main.left, .x-main.right, .x-sidebar.left, .x-sidebar.right {
                float: none;
                display: block;
                width: auto;
            }
        }

        .x-btn-widgetbar {
            border-top-color: #000000;
            border-right-color: #000000;
        }

        .x-btn-widgetbar:hover {
            border-top-color: #444444;
            border-right-color: #444444;
        }

        body.x-navbar-fixed-left-active {
            padding-left: 235px;
        }

        body.x-navbar-fixed-right-active {
            padding-right: 235px;
        }

        .x-navbar {
            font-size: 12px;
        }

        .x-navbar .x-nav > li > a {
            font-weight: 400;
            font-style: normal;
        }

        .x-navbar-fixed-left, .x-navbar-fixed-right {
            width: 235px;
        }

        .x-navbar-fixed-top-active .x-navbar-wrap {
            height: 90px;
        }

        .x-navbar-inner {
            min-height: 90px;
        }

        .x-btn-navbar {
            margin-top: 20px;;
        }

        .x-btn-navbar, .x-btn-navbar.collapsed {
            font-size: 24px;
        }

        .x-brand {
            font-size: 54px;
            font-size: 5.4rem;
            margin-top: 5px;
        }

        body.x-navbar-fixed-left-active .x-brand, body.x-navbar-fixed-right-active .x-brand {
            margin-top: 30px;
        }

        @media (max-width: 979px) {
            body.x-navbar-fixed-left-active, body.x-navbar-fixed-right-active {
                padding: 0;
            }

            body.x-navbar-fixed-left-active .x-brand, body.x-navbar-fixed-right-active .x-brand {
                margin-top: 5px;
            }

            .x-navbar-fixed-top-active .x-navbar-wrap {
                height: auto;
            }

            .x-navbar-fixed-left, .x-navbar-fixed-right {
                width: auto;
            }
        }

        .x-btn, .button, [type="submit"] {
            color: #ffffff;
            border-color: #ac1100;
            background-color: #cd0007;
        }

        .x-btn:hover, .button:hover, [type="submit"]:hover {
            color: #ffffff;
            border-color: #600900;
            background-color: #cd0007;
        }

        .x-btn.x-btn-real, .x-btn.x-btn-real:hover {
            margin-bottom: 0.25em;
            text-shadow: 0 0.075em 0.075em rgba(0, 0, 0, 0.65);
        }

        .x-btn.x-btn-real {
            -webkit-box-shadow: 0 0.25em 0 0 #a71000, 0 4px 9px rgba(0, 0, 0, 0.75);
            box-shadow: 0 0.25em 0 0 #a71000, 0 4px 9px rgba(0, 0, 0, 0.75);
        }

        .x-btn.x-btn-real:hover {
            -webkit-box-shadow: 0 0.25em 0 0 #ce000c, 0 4px 9px rgba(0, 0, 0, 0.75);
            box-shadow: 0 0.25em 0 0 #ce000c, 0 4px 9px rgba(0, 0, 0, 0.75);
        }

        .x-btn.x-btn-flat, .x-btn.x-btn-flat:hover {
            margin-bottom: 0;
            text-shadow: 0 0.075em 0.075em rgba(0, 0, 0, 0.65);
            -webkit-box-shadow: none;
            box-shadow: none;
        }

        .x-btn.x-btn-transparent, .x-btn.x-btn-transparent:hover {
            margin-bottom: 0;
            border-width: 3px;
            text-shadow: none;
            text-transform: uppercase;
            background-color: transparent;
            -webkit-box-shadow: none;
            box-shadow: none;
        }

        .x-btn-circle-wrap:before {
            width: 172px;
            height: 43px;
            background: url(theme_files/btn-circle-top-small.png) center center no-repeat;
            -webkit-background-size: 172px 43px;
            background-size: 172px 43px;
        }

        .x-btn-circle-wrap:after {
            width: 190px;
            height: 43px;
            background: url(theme_files/btn-circle-bottom-small.png) center center no-repeat;
            -webkit-background-size: 190px 43px;
            background-size: 190px 43px;
        }

        .x-btn, .x-btn:hover, .button, .button:hover, [type="submit"], [type="submit"]:hover {
            text-shadow: 0 0.075em 0.075em rgba(0, 0, 0, 0.5);
        }

        .x-btn, .button, [type="submit"] {
            border-radius: 0.25em;
        }</style>
    <style type="text/css">
        .portfolio .post_meta {
            display: none !important;
        }

        .x-recent-posts a {
            border: 0px;
        }

        .x-recent-posts .x-recent-posts-date {
            display: none;
        }

        .custom {
            /* latin-ext */
            /* latin */
            /* latin-ext */
            /* latin */
            /* latin-ext */
            /* latin */
            /* latin-ext */
            /* latin */
            /* latin-ext */
            /* latin */
            /* latin-ext */
            /* latin */
        }

        @font-face {

        .custom {
            font-family: 'Raleway';
            font-style: normal;
            font-weight: 200;
            src: local("Raleway ExtraLight"), local("Raleway-ExtraLight"), url(fonts/cbAbzEjxTdN5KKmS-gA0tQsYbbCjybiHxArTLjt7FRU.woff2) format("woff2");
            unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
        }

        }
        @font-face {

        .custom {
            font-family: 'Raleway';
            font-style: normal;
            font-weight: 200;
            src: local("Raleway ExtraLight"), local("Raleway-ExtraLight"), url(fonts/8KhZd3VQBtXTAznvKjw-kwzyDMXhdD8sAj6OAJTFsBI.woff2) format("woff2");
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
        }

        }
        @font-face {

        .custom {
            font-family: 'Raleway';
            font-style: normal;
            font-weight: 300;
            src: local("Raleway Light"), local("Raleway-Light"), url(fonts/ZKwULyCG95tk6mOqHQfRBAsYbbCjybiHxArTLjt7FRU.woff2) format("woff2");
            unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
        }

        }
        @font-face {

        .custom {
            font-family: 'Raleway';
            font-style: normal;
            font-weight: 300;
            src: local("Raleway Light"), local("Raleway-Light"), url(fonts/-_Ctzj9b56b8RgXW8FAriQzyDMXhdD8sAj6OAJTFsBI.woff2) format("woff2");
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
        }

        }
        @font-face {

        .custom {
            font-family: 'Raleway';
            font-style: normal;
            font-weight: 400;
            src: local("Raleway"), local("Raleway-Regular"), url(fonts/YZaO6llzOP57DpTBv2GnyFKPGs1ZzpMvnHX-7fPOuAc.woff2) format("woff2");
            unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
        }

        }
        @font-face {

        .custom {
            font-family: 'Raleway';
            font-style: normal;
            font-weight: 400;
            src: local("Raleway"), local("Raleway-Regular"), url(fonts/QAUlVt1jXOgQavlW5wEfxQLUuEpTyoUstqEm5AMlJo4.woff2) format("woff2");
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
        }

        }
        @font-face {

        .custom {
            font-family: 'Raleway';
            font-style: normal;
            font-weight: 500;
            src: local("Raleway Medium"), local("Raleway-Medium"), url(fonts/Li18TEFObx_yGdzKDoI_cgsYbbCjybiHxArTLjt7FRU.woff2) format("woff2");
            unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
        }

        }
        @font-face {

        .custom {
            font-family: 'Raleway';
            font-style: normal;
            font-weight: 500;
            src: local("Raleway Medium"), local("Raleway-Medium"), url(fonts/CcKI4k9un7TZVWzRVT-T8wzyDMXhdD8sAj6OAJTFsBI.woff2) format("woff2");
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
        }

        }
        @font-face {

        .custom {
            font-family: 'Raleway';
            font-style: normal;
            font-weight: 600;
            src: local("Raleway SemiBold"), local("Raleway-SemiBold"), url(fonts/STBOO2waD2LpX45SXYjQBQsYbbCjybiHxArTLjt7FRU.woff2) format("woff2");
            unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
        }

        }
        @font-face {

        .custom {
            font-family: 'Raleway';
            font-style: normal;
            font-weight: 600;
            src: local("Raleway SemiBold"), local("Raleway-SemiBold"), url(fonts/xkvoNo9fC8O2RDydKj12bwzyDMXhdD8sAj6OAJTFsBI.woff2) format("woff2");
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
        }

        }
        @font-face {

        .custom {
            font-family: 'Raleway';
            font-style: normal;
            font-weight: 700;
            src: local("Raleway Bold"), local("Raleway-Bold"), url(fonts/WmVKXVcOuffP_qmCpFuyzQsYbbCjybiHxArTLjt7FRU.woff2) format("woff2");
            unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
        }

        }
        @font-face {

        .custom {
            font-family: 'Raleway';
            font-style: normal;
            font-weight: 700;
            src: local("Raleway Bold"), local("Raleway-Bold"), url(fonts/JbtMzqLaYbbbCL9X6EvaIwzyDMXhdD8sAj6OAJTFsBI.woff2) format("woff2");
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
        }

        }
        @font-face {

        .custom {
            font-family: 'Raleway';
            font-style: normal;
            font-weight: 800;
            src: local("Raleway ExtraBold"), local("Raleway-ExtraBold"), url(fonts/QoPu455RxV2raYSIFXAMBQsYbbCjybiHxArTLjt7FRU.woff2) format("woff2");
            unicode-range: U+0100-024F, U+1E00-1EFF, U+20A0-20AB, U+20AD-20CF, U+2C60-2C7F, U+A720-A7FF;
        }

        }
        @font-face {

        .custom {
            font-family: 'Raleway';
            font-style: normal;
            font-weight: 800;
            src: local("Raleway ExtraBold"), local("Raleway-ExtraBold"), url(fonts/1ImRNPx4870-D9a1EBUdPAzyDMXhdD8sAj6OAJTFsBI.woff2) format("woff2");
            unicode-range: U+0000-00FF, U+0131, U+0152-0153, U+02C6, U+02DA, U+02DC, U+2000-206F, U+2074, U+20AC, U+2212, U+2215, U+E0FF, U+EFFD, U+F000;
        }

        }

        #lang_sel ul ul {
            border-top: 0px solid white !important;
        }

        #lanBtn, #lang_sel ul a {
            background-color: #white !important;

        }

        .tp-caption h1 {
            color: white
        }

        nav.x-nav-collapse.in.collapse {
            -webkit-box-shadow: 0px 20px 33px 0px rgba(0, 0, 0, 0.75);
            -moz-box-shadow: 0px 20px 33px 0px rgba(0, 0, 0, 0.75);
            box-shadow: 0px 20px 33px 0px rgba(0, 0, 0, 0.75);
            height: auto !important;
        }</style>

    <script src="theme_files/roundtrip.js" type="text/javascript" async="true"></script>
    <!--
    <div style="width: 1px; height: 1px; display: inline; position: absolute;">
        <img style="border-style:none;" alt="" src="theme_files/out_007.gif" height="1" width="1">
        <img style="border-style:none;" alt="" src="theme_files/out_002.gif" height="1" width="1">
        <img style="border-style:none;" alt="" src="theme_files/out.gif" height="1" width="1">
        <img style="border-style:none;" alt="" src="theme_files/out_003.gif" height="1" width="1">
        <img style="border-style:none;" alt="" src="theme_files/out_005.gif" height="1" width="1">
        <img style="border-style:none;" alt="" src="theme_files/out_008.gif" height="1" width="1">
        <img style="border-style:none;" alt="" src="theme_files/out_004.gif" height="1" width="1">
        <img style="border-style:none;" alt="" src="theme_files/out_006.gif" height="1" width="1">
        <img style="border-style:none;" alt="" src="theme_files/a.gif" height="1" width="1">
        <img src="theme_files/seg.gif" height="1" width="1">
    </div>
    -->
    <link rel="stylesheet" href="css/style.css" type="text/css" media="all">
</head>
<body class="page page-id-6674 page-template page-template-template-blank-1-php x-integrity x-integrity-light x-navbar-static-active x-full-width-layout-active x-full-width-active x-post-meta-disabled x-portfolio-meta-disabled wpb-js-composer js-comp-ver-3.7.4 vc_responsive">
<!--
BEGIN #top.site
-->

<div id="top" class="site">


    <header class="masthead" role="banner">


        <div class="x-navbar-wrap">
            <div class="x-navbar">
                <div class="x-navbar-inner x-container-fluid max width">
                    <a href="https://www.bungyjapan.com/?lang=<?= $lang ?>" class="x-brand img" title="">
                        <img src="theme_files/logo-test22.png" alt=""> </a>
                    <a href="#" class="x-btn-navbar collapsed" data-toggle="collapse" data-target=".x-nav-collapse">
                        <i class="x-icon-bars"></i>
                        <span class="visually-hidden"><?= $_menu ?></span>
                    </a>
                    <nav style="max-height: 575px; overflow: scroll;" class="x-nav-collapse collapse" role="navigation">
                        <div id="lang_sel">
                            <ul>
                                <?= drawLanguageSwitcher($language) ?>
                            </ul>
                        </div>
                        <ul id="menu-primary" class="x-nav sf-menu sf-js-enabled">
                            <li id="menu-item-7443" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-7443">
                                <a class="sf-with-ul" href="#"><?= $_booking ?>
                                    <span class="sf-sub-indicator"></span></a>
                                <ul style="display: none; visibility: hidden;" class="sub-menu">
                                    <li id="menu-item-7476" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7476">
                                        <a href="https://minakami.bungyjapan.com/bookings/?lang=<?= $language ?>"><?= $_minakamiReservations ?></a>
                                    </li>
                                    <li id="menu-item-7477" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7477">
                                        <a href="https://sarugakyo.bungyjapan.com/bookings/?lang=<?= $language ?>"><?= $_sarugakyoReservations ?></a>
                                    </li>
                                    <li id="menu-item-7478" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7478">
                                        <a href="https://ryujin.bungyjapan.com/bookings/?lang=<?= $language ?>"><?= $_ryujinReservations ?></a>
                                    </li>
                                    <li id="menu-item-7479" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7479">
                                        <a href="https://itsuki.bungyjapan.com/bookings/?lang=<?= $language ?>"><?= $_itsukiReservations ?></a>
                                    </li>
                                    <li id="menu-item-7479" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7479">
                                        <a href="https://fuji.bungyjapan.com/bookings/?lang=<?= $language ?>"><?= $_fujiReservations ?></a>
                                    </li>
                                    <li id="menu-item-7480" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7480">
                                        <a href="https://kaiun.bungyjapan.com/bookings/?lang=<?= $language ?>"><?= $_kaiunReservations ?></a>
                                    </li>
                                    <li id="menu-item-7485" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-7480">
                                        <a href="https://yamba.bungyjapan.com/bookings/?lang=<?= $language ?>"><?= $_yambaReservations ?></a>
                                    </li>
                                    
                                </ul>
                            </li>
                            <li id="menu-item-8082" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-8082">
                                <a class="sf-with-ul"><?= $_about ?><span class="sf-sub-indicator"></span></a>
                                <ul style="display: none; visibility: hidden;" class="sub-menu">
                                    <li id="menu-item-8084" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8084">
                                        <a href="https://www.bungyjapan.com/about/?lang=<?= $lang ?>"><?= $_bungyJapan ?></a>
                                    </li>
                                    <li id="menu-item-7786" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7786">
                                        <a href="https://www.bungyjapan.com/gallery/?lang=<?= $lang ?>"><?= $_gallery ?></a>
                                    </li>
                                    <li id="menu-item-7787" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-7787">
                                        <a href="https://www.bungyjapan.com/what-is-bungy-jump/?lang=<?= $lang ?>"><?= $_whatIs ?></a>
                                    </li>
                                </ul>
                            </li>
                            <li id="menu-item-8251" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children menu-item-8251">
                                <a class="sf-with-ul" href="#"><?= $_ourBridges ?><span class="sf-sub-indicator"></span></a>
                                <ul style="display: none; visibility: hidden;" class="sub-menu">
                                    <li id="menu-item-8273" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8273">
                                        <a href="https://www.bungyjapan.com/bungy-spots/minakami/?lang=<?= $lang ?>"><?= $_minakamiBungy ?></a>
                                    </li>
                                    <li id="menu-item-8248" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8248">
                                        <a href="https://www.bungyjapan.com/bungy-spots/ryujin/?lang=<?= $lang ?>"><?= $_ryujinBungy ?></a>
                                    </li>
                                    <li id="menu-item-8258" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8258">
                                        <a href="https://www.bungyjapan.com/bungy-spots/sarugakyo/?lang=<?= $lang ?>"><?= $_sarugakyoBungy ?></a>
                                    </li>
                                    <li id="menu-item-8282" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8282">
                                        <a href="https://www.bungyjapan.com/bungy-spots/itsukimura/?lang=<?= $lang ?>"><?= $_itsukiBungy ?></a>
                                    </li>
                                    <li id="menu-item-8282" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8282">
                                        <a href="https://www.bungyjapan.com/bungy-spots/fuji/?lang=<?= $lang ?>"><?= $_fujiBungy ?></a>
                                    </li>
                                    <li id="menu-item-8283" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8283">
                                        <a href="https://www.bungyjapan.com/bungy-spots/kaiun/?lang=<?= $lang ?>"><?= $_kaiunBungy ?></a>
                                    </li>
                                    <li id="menu-item-8284" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8283">
                                        <a href="https://www.bungyjapan.com/bungy-spots/yamba/?lang=<?= $lang ?>"><?= $_yambaBungy ?></a>
                                    </li>
                                    
                                </ul>
                            </li>
                            <li id="menu-item-6684" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-6674 current_page_item menu-item-6684">
                                <a href="https://www.bungyjapan.com/faq/?lang=<?= $lang ?>"><?= $_faq ?></a></li>
                            <li id="menu-item-6697" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-6697">
                                <a href="https://www.bungyjapan.com/contact/?lang=<?= $lang ?>"><?= $_contact ?></a></li>
                        </ul>
                    </nav> <!-- end .x-nav-collapse.collapse -->
                </div> <!-- end .x-navbar-inner -->
            </div> <!-- end .x-navbar -->
        </div> <!-- end .x-navbar-wrap -->
    </header>
    <link href="theme_files/font-awesome.css" rel="stylesheet">
    <div class="x-main x-container-fluid max width offset" role="main">
        <article id="post-6674" class="post-6674 page type-page status-publish hentry no-post-thumbnail">
            <div class="entry-wrap entry-content">
