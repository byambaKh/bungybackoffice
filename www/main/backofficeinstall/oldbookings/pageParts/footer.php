<?php
$languages = [
    'eng' => [
        '_bungyJapan'    => 'Bungy Japan',
        '_whatIs'        => 'What is Bungy Jumping?',
        '_gallery'       => 'Gallery',
        '_faq'           => 'FAQ',
        '_contact'       => 'Contact',
        '_booking'       => 'Booking',
        '_ourBridges'    => 'ourBridges',
        '_about'         => 'About',
        '_home'          => 'Home',
        '_privacyPolicy' => 'Privacy Policy',
        '_reservations'  => 'Reservations',
        '_Recruitment'   => 'Recruitment',
        '_call'          => "<div id='text_icl-4' class='widget widget_text_icl'><h4 class='h-widget'>Call us</h4> <div class='textwidget'>Call Center 0278-72-8133<br> </div> </div>",
        '_address'       => "<div id='text_icl-6' class='widget widget_text_icl'><h4 class='h-widget'>Head Office</h4> <div class='textwidget'>〒379-1612<br> 200 Obinata<br> Minakami-machi, Tone-gun<br> Gunma-ken, 379-1612 <br> TEL: 0278-25-4638<br> FAX: 0278-25-4639<br></div> </div>",
        '_onlineBooking' => "<div id='text_icl-3' class='widget widget_text_icl'> <div class='textwidget'> <a href='https://minakami.bungyjapan.com/bookings/?lang=eng' target='_blank'> <button class='btn-bungy-grey'>　Online Booking</button> </a><br> or call us at<br> 0278-72-8133 </div> </div>",

    ],
    'jpn' => [
        '_bungyJapan'    => 'Bungy Japanについて',
        '_whatIs'        => 'バンジージャンプとは?',
        '_gallery'       => 'ギャラリー',
        '_faq'           => 'Q&amp;A',
        '_contact'       => 'お問い合わせ',
        '_menu'          => 'メニュー',
        '_booking'       => 'ご予約',
        '_ourBridges'    => 'ブリッジ',
        '_about'         => 'バンジージャンプとは',
        '_home'          => 'Home',
        '_privacyPolicy' => 'Privacy Policy',
        '_reservations'  => 'Reservations',
        '_Recruitment'   => 'Recruitment',
        '_call'          => "<div id='text_icl-5' class='widget widget_text_icl'><h4 class='h-widget'>お問い合わせ</h4> <div class='textwidget'>コー​​ルセンター 0278-72-8133</div> </div>",
        '_address'       => "<div id='text_icl-7' class='widget widget_text_icl'><h4 class='h-widget'>本社</h4> <div class='textwidget'>〒379-1612<br> 200小日向<br> みなかみ町、利根郡<br> 群馬県、 379-1612<br> TEL ： 0278-25-4638<br> FAX： 0278-25-4639<br></div></div>",
        '_onlineBooking' => "<div id='text_icl-2' class='widget widget_text_icl'> <div class='textwidget'> <a href='https://minakami.bungyjapan.com/bookings/?lang=jpn' target='_blank'> <button class='btn-bungy-grey'>　オンライン予約</button> </a><br> お電話<br> 0278-72-8133 </div></div>",
    ],
];

$lang = convertLanguageCodes($language);//used for language links in wordpress
extract($languages[$language]);
?>
<span class="visually-hidden"><span class="author vcard"><span class="fn">bungyjapan</span></span><span class="entry-title">FAQ</span><time class="entry-date updated" datetime="2014-03-28T10:43:05+00:00">03.28.2014</time></span>
<!-- </article>end .hentry -->
</div> <!-- end .x-main.x-container-fluid.max.width.offset -->
<a class="x-scroll-top right fade" href="#top" title="Back to Top">
    <i class="x-icon-angle-up"></i>
</a>

<script>

    jQuery(document).ready(function ($) {

        //
        // Scroll top anchor.
        //
        // 1. Get the number of pixels to the bottom of the page.
        // 2. Get the difference from the body height and the bottom offset.
        // 3. Output the adjusted height for the page for acurate percentage parameter.
        //

        jQuery.fn.scrollBottom = function () {
            return $(document).height() - this.scrollTop() - this.height();
        };

        var windowObj = $(window);
        var body = $('body');
        var bodyOffsetBottom = windowObj.scrollBottom(); // 1
        var bodyHeightAdjustment = body.height() - bodyOffsetBottom; // 2
        var bodyHeightAdjusted = body.height() - bodyHeightAdjustment; // 3
        var scrollTopAnchor = $('.x-scroll-top');

        function sizingUpdate() {
            var bodyOffsetTop = windowObj.scrollTop();
            if (bodyOffsetTop > ( bodyHeightAdjusted * 0.75 )) {
                scrollTopAnchor.addClass('in');
            } else {
                scrollTopAnchor.removeClass('in');
            }
        }

        windowObj.bind('scroll', sizingUpdate).resize(sizingUpdate);
        sizingUpdate();

        scrollTopAnchor.click(function () {
            $('html,body').animate({scrollTop: 0}, 850, 'easeInOutExpo');
            return false;
        });

    });

</script>


<footer class="x-colophon top" role="contentinfo">
    <div class="x-container-fluid max width">
        <div class="x-row-fluid">
            <div class="x-span3">
                <div id="text-3" class="widget widget_text logo-footer">
                    <img src="theme_files/logo-footer.jpg">
                </div>
            </div>
            <div class="x-span3" style="display:none">
                <div id="text-3" class="widget widget_text">
                    <img src="theme_files/book-footer.jpg">
                </div>
            </div>
            <div class="x-span3 footerBook">
                <?= $_onlineBooking ?>
            </div>
            <div class="x-span3">
                <?= $_address ?>
            </div>
            <div class="x-span3">
                <?= $_call ?>
            </div>
        </div> <!-- end .x-row-fluid -->
    </div> <!-- end .x-container-fluid.max.width -->
</footer> <!-- end .x-colophon.top -->
<footer class="x-colophon bottom" role="contentinfo">
    <div class="x-container-fluid max width">
        <ul id="menu-footer" class="x-nav">
            <li id="menu-item-8299" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8299">
                <a href="https://www.bungyjapan.com/?lang=<?= $lang ?>"><?= $_home ?></a></li>
            <li id="menu-item-8300" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8300">
                <a href="http://www.bungyjapan.com/about/?lang=<?= $lang ?>"><?= $_bungyJapan ?></a></li>
            <li id="menu-item-8298" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8298">
                <a href="http://www.bungyjapan.com/our-bridges/?lang=<?= $lang ?>"><?= $_ourBridges ?></a></li>
            <li id="menu-item-8297" class="menu-item menu-item-type-custom menu-item-object-custom menu-item-8297">
                <a href="https://minakami.bungyjapan.com/bookings/?lang=<?= $lang ?>g"><?= $_reservations ?></a></li>
            <li id="menu-item-8296" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8296">
                <a href="http://www.bungyjapan.com/contact/?lang=<?= $lang ?>"><?= $contact ?></a></li>
            <li id="menu-item-8295" class="menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-6674 current_page_item menu-item-8295">
                <a href="http://www.bungyjapan.com/faq/?lang=<?= $lang ?>"><?= $faq ?></a></li>
            <li id="menu-item-8294" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8294">
                <a href="http://www.bungyjapan.com/recruitment/?lang=<?= $lang ?>"><?= $_recruitment ?></a></li>
            <li id="menu-item-8292" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-8292">
                <a href="http://www.bungyjapan.com/privacy-policy/?lang=<?= $lang ?>"><?= $_privacyPolicy ?></a></li>
        </ul>
        <div class="x-social-global">
            <a data-original-title="Facebook" data-toggle="tooltip" data-placement="top" data-trigger="hover" href="https://ja-jp.facebook.com/bungyjapan" class="facebook" title="" target="_blank"><i class="x-social-facebook"></i></a><a data-original-title="Twitter" data-toggle="tooltip" data-placement="top" data-trigger="hover" href="https://twitter.com/bungyjapan/status/354042666062860288" class="twitter" title="" target="_blank"><i class="x-social-twitter"></i></a><a data-original-title="YouTube" data-toggle="tooltip" data-placement="top" data-trigger="hover" href="https://www.youtube.com/user/bungyjapan" class="youtube" title="" target="_blank"><i class="x-social-youtube"></i></a><a data-original-title="Instagram" data-toggle="tooltip" data-placement="top" data-trigger="hover" href="https://www.instagram.com/bungyjapan/" class="instagram" title="" target="_blank"><i class="x-social-instagram"></i></a>
        </div>
        <div class="x-colophon-content">
            <p style="letter-spacing: 2px; text-transform: uppercase; opacity: 0.5; filter: alpha(opacity=50);">2016 Copyright by Bungy Japan. All rights reserved.</p>
            <p style="letter-spacing: 2px; text-transform: uppercase; opacity: 0.5; filter: alpha(opacity=50);">STANDARD MOVE LTD UK Office – THE APEX, 2 SHERIFFS ORCHARD, COVENTRY, WEST MIDLANDS, ENGLAND, CV1 3PP</p>
        </div>

    </div> <!-- end .x-container-fluid.max.width -->
</footer> <!-- end .x-colophon.bottom -->

<!--
END #top.site
-->

<!-- YouTube Channel 3 -->
<script type="text/javascript">function ytc_init_MPAU() {
        jQuery('.ytc-lightbox').magnificPopupAU({
            disableOn: 320,
            type: 'iframe',
            mainClass: 'ytc-mfp-lightbox',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false
        });
    }
    jQuery(window).on('load', function () {
        ytc_init_MPAU();
    });
    jQuery(document).ajaxComplete(function () {
        ytc_init_MPAU();
    });</script>
<script type="text/javascript" src="theme_files/jquery_003.js"></script>
<script type="text/javascript">
    /* <![CDATA[ */
    var _wpcf7 = {
        "loaderUrl": "https:\/\/www.bungyjapan.com\/wp-content\/plugins\/contact-form-7\/images\/ajax-loader.gif",
        "sending": "Sending ..."
    };
    /* ]]> */
</script>
<script type="text/javascript" src="theme_files/scripts.js"></script>
<script type="text/javascript">
    /* <![CDATA[ */
    var sb_instagram_js_options = {"sb_instagram_at": "1655064710.1677ed0.e505ffde18b34c3bbce4ed9fc4dac035"};
    /* ]]> */
</script>
<script type="text/javascript" src="theme_files/sb-instagram.js"></script>
<script type="text/javascript" src="theme_files/x-shortcodes.js"></script>
<script type="text/javascript" src="theme_files/easing-1.js"></script>
<script type="text/javascript" src="theme_files/flexslider-2.js"></script>
<script type="text/javascript" src="theme_files/collapse-2.js"></script>
<script type="text/javascript" src="theme_files/alert-2.js"></script>
<script type="text/javascript" src="theme_files/tab-2.js"></script>
<script type="text/javascript" src="theme_files/transition-2.js"></script>
<script type="text/javascript" src="theme_files/tooltip-2.js"></script>
<script type="text/javascript" src="theme_files/popover-2.js"></script>
<script type="text/javascript" src="theme_files/jquery_005.js"></script>
<script type="text/javascript" src="theme_files/hoverintent-7.js"></script>
<script type="text/javascript" src="theme_files/superfish-1.js"></script>
<script type="text/javascript">
    /* <![CDATA[ */
    var thickboxL10n = {
        "next": "Next >",
        "prev": "< Prev",
        "image": "Image",
        "of": "of",
        "close": "Close",
        "noiframes": "This feature requires inline frames. You have iframes disabled or your browser does not support them.",
        "loadingAnimation": "https:\/\/www.bungyjapan.com\/wp-includes\/js\/thickbox\/loadingAnimation.gif"
    };
    /* ]]> */
</script>
<script type="text/javascript" src="theme_files/thickbox.js"></script>
<script type="text/javascript">
    /* <![CDATA[ */
    var icl_vars = {"current_language": "en", "icl_home": "https:\/\/www.bungyjapan.com?lang=<?=$lang?>"};
    /* ]]> */
</script>
<script type="text/javascript" src="theme_files/sitepress.js"></script>
<!--[if lt IE 9]>
<script type="text/javascript" src="/wp-content/themes/x/framework/js/vendor/selectivizr-1.0.2.min.js"></script><![endif]-->


<script>
    jQuery(".x-nav-collapse").css({
        maxHeight: jQuery(window).height() - jQuery(".x-navbar").height() + "px",
        overflow: "scroll"
    });

    //Adroll
    adroll_adv_id = "DSCBWXJ3YJF6VDKZKCPAUK";
    adroll_pix_id = "S4ZWHB57YJBIXDJG5TMMGP";
    // OPTIONAL: provide email to improve user identification
    // adroll_email = "username@example.com";
    (function () {
        var _onload = function () {
            if (document.readyState && !/loaded|complete/.test(document.readyState)) {
                setTimeout(_onload, 10);
                return
            }
            if (!window.__adroll_loaded) {
                __adroll_loaded = true;
                setTimeout(_onload, 50);
                return
            }
            var scr = document.createElement("script");
            var host = (("https:" == document.location.protocol) ? "https://s.adroll.com" : "http://a.adroll.com");
            scr.setAttribute('async', 'true');
            scr.type = "text/javascript";
            scr.src = host + "/j/roundtrip.js";
            ((document.getElementsByTagName('head') || [null])[0] ||
            document.getElementsByTagName('script')[0].parentNode).appendChild(scr);
        };
        if (window.addEventListener) {
            window.addEventListener('load', _onload, false);
        }
        else {
            window.attachEvent('onload', _onload)
        }
    }());
</script>
<script src="theme_files/S4ZWHB57YJBIXDJG5TMMGP.js" type="text/javascript" async="true"></script>
</body>
</html>
