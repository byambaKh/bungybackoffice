<?php
require_once("../includes/application_top.php");
require('functions.php');

/**************************************************************************
  For some strange reason PHP setting $_SESSION['jumpDate'] and
  $_SESSION['chkInTime'] to NULL when this file loads. There is no
  code that modifies them here and things work fine locally but not
  on the server
**************************************************************************/

function queryInpendiumServerWithToken($token)
{
    $url = "https://inpendium.net/frontend/GetStatus;jsessionid=" . $token;

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
    $resultPayment = curl_exec($ch);
    curl_close($ch);

    $resultJson = json_decode($resultPayment, true);

    return $resultJson;
}

function generateMessageForEmail($msg, $resultJson, $language)
{
    $bookingLanguage = '';
    if($language == 'jpn') {
        $bookingLanguage = 'Japanese';

    } else if ($language == 'eng'){
        $bookingLanguage = 'English';
    }

    $msg['General Information']['Site'] = $_SERVER['HTTP_HOST'];
    $msg['General Information']['Language'] = $bookingLanguage;
    $msg['Jump Information']['Date'] = $_SESSION['jump_date'];
    $msg['Jump Information']['Time'] = $_SESSION['check_in_time'];
    $msg['Jump Information']['Count'] = $_SESSION['noOfJump'];
    $msg['Booking Information']['First Name'] = mb_convert_encoding($_SESSION['firstname'], "UTF-8", $_SESSION['address_encoding']);
    $msg['Booking Information']['Last Name'] = mb_convert_encoding($_SESSION['lastname'], "UTF-8", $_SESSION['address_encoding']);
    $msg['Booking Information']['E-mail'] = mb_convert_encoding($_SESSION['regemail'], "UTF-8", $_SESSION['address_encoding']);
    $msg['Booking Information']['Phone'] = mb_convert_encoding($_SESSION['teleno'], "UTF-8", $_SESSION['address_encoding']);
    $msg['Booking Information']['Transport'] = mb_convert_encoding($_SESSION['modeTransport'], "UTF-8", $_SESSION['address_encoding']);
    $msg['Booking Information']['Total'] = mb_convert_encoding($_SESSION['total_price'], "UTF-8", $_SESSION['address_encoding']);
    $msg['Billing Information']['First Name'] = mb_convert_encoding($_SESSION['address_first'], "UTF-8", $_SESSION['address_encoding']);
    $msg['Billing Information']['Last Name'] = mb_convert_encoding($_SESSION['address_last'], "UTF-8", $_SESSION['address_encoding']);
    $msg['Billing Information']['Address'] = mb_convert_encoding($_SESSION['address_street'], "UTF-8", $_SESSION['address_encoding']);
    $msg['Billing Information']['Address 2'] = mb_convert_encoding($_SESSION['address_street2'], "UTF-8", $_SESSION['address_encoding']);
    $msg['Billing Information']['Postal Code'] = mb_convert_encoding($_SESSION['address_zip'], "UTF-8", $_SESSION['address_encoding']);
    $msg['Billing Information']['City'] = mb_convert_encoding($_SESSION['address_city'], "UTF-8", $_SESSION['address_encoding']);
    $msg['Billing Information']['Prefecture'] = mb_convert_encoding($_SESSION['address_state'], "UTF-8", $_SESSION['address_encoding']);
    $msg['Billing Information']['Phone'] = mb_convert_encoding($_SESSION['address_phone'], "UTF-8", $_SESSION['address_encoding']);
    $msg['Billing Information']['Mobile'] = mb_convert_encoding($_SESSION['address_mobile'], "UTF-8", $_SESSION['address_encoding']);
    $msg['Transaction Information']['Card Holder'] = $resultJson['transaction']['account']['holder'];
    $msg['Transaction Information']['Card Type'] = $resultJson['transaction']['account']['brand'];
    $msg['Transaction Information']['Result'] = $resultJson['transaction']['processing']['result'];
    $msg['Transaction Information']['Code'] = $resultJson['transaction']['processing']['return']['code'];
    $msg['Transaction Information']['Message'] = $resultJson['transaction']['processing']['return']['message'];

    $msgFlat = "";
    foreach ($msg as $k => $v) {
        $msgFlat .= "<h1>" . $k . "</h1>";
        foreach ($v as $kk => $vv) {
            $msgFlat .= "<strong>" . $kk . ": </strong>";
            $msgFlat .= $vv . "<br>";
        }
    }

    return $msgFlat;
}

function sendMessageToDeveloper($msgFlat, $resultJson, $creditCardSimulation, $headers, $config)
{
    $msgFlat .= "<h1>PHP Session</h1>";
    $msgFlat .= "<pre>" . mb_convert_encoding(var_export($_SESSION, true), "UTF-8", $_SESSION['address_encoding']) . "</pre>";
    $msgFlat .= "<h1>Payment Return</h1>";
    $msgFlat .= "<pre>" . var_export($resultJson, true) . "</pre>";


    if (!$creditCardSimulation) {
        //@mail("support@jts.ec", 'NEW BUNGY CC PAYMENT', $msgFlat, $headers, "-f {$config['confirmation_email']}");
        //@mail("greencj72@gmail.com", 'NEW BUNGY CC PAYMENT', $msgFlat, $headers, "-f {$config['confirmation_email']}");
    }
}

function getPriceException()
{
    $priceException = -1;//A -1 price exception indicates that something has gone wrong finding a price exception for this booking
    //This query finds the current price for the agent "Credit Card" and the site price exception for that agent
    $queryUserRolesPrice = "SELECT price FROM user_roles WHERE user_id = 10000102 AND site_id =" . CURRENT_SITE_ID . ";";

    $resultsUserRolesPrice = mysql_query($queryUserRolesPrice);

    if (mysql_num_rows($resultsUserRolesPrice) == 1) {
        $userRolesPriceArray = mysql_fetch_assoc($resultsUserRolesPrice);
        $priceException = $userRolesPriceArray['price'];

        return $priceException;
    }

    return $priceException;
}

function unsetSession()
{
    unset($_SESSION['jumpPlace']);
    unset($_SESSION['jumpDate']);
    unset($_SESSION['chkInTime']);
    unset($_SESSION['noOfJump']);
    unset($_SESSION['lastname']);
    unset($_SESSION['firstname']);
    unset($_SESSION['regemail']);
    unset($_SESSION['teleno']);
    unset($_SESSION['total_price']);
    unset($_SESSION['modeTransport']);
    unset($_SESSION['payby']);
    unset($_SESSION['address_encoding']);
    unset($_SESSION['address_first']);
    unset($_SESSION['address_last']);
    unset($_SESSION['address_street']);
    unset($_SESSION['address_street2']);
    unset($_SESSION['address_zip']);
    unset($_SESSION['address_city']);
    unset($_SESSION['address_state']);
    unset($_SESSION['address_phone']);
    unset($_SESSION['address_mobile']);
}

function confirmBooking($passkey)
{
    global $config;
    $sql = "SELECT * FROM customerregs_temp1 WHERE confirmcode = '" . $passkey . "'";
    $result = mysql_query($sql);
    $error_code = "success";
    $booking = array();
    if ($result && ($booking = mysql_fetch_assoc($result))) {
        // check total of registered jumps for this date/time
        $total = getNoOfJumpsForTimeSlot($booking);
        // check if this tel number already have jump registered with status not deleted and not checked-out
        $total_registered = getOtherBookingsWithSamePhoneNumber($booking);

        if ($booking['Agent'] === "Credit Card") $total_registered = 0;//allow credit card to make multiple bookings in a day

        /*
        // This has been removed, by this time it is too late to deny a booking as payment has been accepted
        if ($total + $booking['NoOfJump'] > $config['online_slots']) {
            // if currently booked plus this booking > 6 then deny booking
            $error_code = "denied";

        } else
        */

        if ($total_registered > 0) {
            // if registered no deleted and no checked out records exists - deny booking
            $error_code = "denied_phone";
        } else {

            // normal booking process
            // delete temp record
            // unset temp ID and ConfirmCode
            $tempRegId = $booking['CustomerRegID'];
            unset($booking['CustomerRegID']);
            unset($booking['ConfirmCode']);
            // fill other default values
            $booking['OtherNo'] = '';
            $booking['BookingType'] = 'Website';
            $booking['Rate'] = getFirstJumpRate(CURRENT_SITE_ID, $booking['BookingDate']);
            $booking['RateToPay'] = 0;
            $booking['BookingReceived'] = date("Y-m-d H:i:s");
            $booking['created'] = date("Y-m-d H:i:s");


            //if this is a not a credit card booking, use these defaults
            //else these things have already been defined for us in the
            //paymentreturn.php page
            if ($booking['Agent'] != "Credit Card") {
                $booking['CollectPay'] = 'Onsite';
                $booking['Agent'] = 'NULL';
                $booking['Notes'] = 'Internet (' . date("Y/m/d") . ")";
            }

            if ($r = db_perform('customerregs1', $booking)) {
                $error_code = 'success';
                $sql = "DELETE FROM customerregs_temp1 WHERE CustomerRegID = '" . $tempRegId . "'";
                mysql_query($sql);//delete the booking only after it is saved
            } else {
                $error_code = 'notsaved';
            }
            echo mysql_error();
            $booking['status_code'] = $error_code;
            send_backup_email($booking);
        }
    } else {
        $error_code = "incorrect";
    }
    return $error_code;
}

/**
 * @param $booking
 *
 * @return array
 */
function getOtherBookingsWithSamePhoneNumber($booking)
{
    $total_registered = 0;
    $sql = "SELECT count(*) AS total FROM customerregs1
			WHERE
				ContactNo = '" . db_input($booking['ContactNo']) . "'
				AND DeleteStatus = 0
				AND Checked = 0
				AND bookingDate >= '" . date("Y-m-d") . "'";
    $res = mysql_query($sql);

    if ($row = mysql_fetch_assoc($res)) {
        $total_registered += $row['total'];
    };

    return $total_registered;
}

/**
 * @param $booking
 *
 * @return array
 */
function getNoOfJumpsForTimeSlot($booking)
{

    $total = 0;
    $sql = "SELECT SUM(NoOfJump) as total
			from customerregs1
			where
				site_id = '" . CURRENT_SITE_ID . "' and
				DeleteStatus = 0 and
				BookingDate = '{$booking['BookingDate']}' and
				BookingTime = '{$booking['BookingTime']}'";
    $res = mysql_query($sql);
    if ($row = mysql_fetch_assoc($res)) {
        $total = $row['total'];
    }

    return $total;
}

function sendRegistrationEmail($chkInTime, $noOfJump, $romajiname, $confirmCode, $config, $subdomain)
{
    $jumpDate = $_SESSION['jump_date'];

    $lang = 'jp';
    $paidByCreditCard = true;

    if (!isset($lang)) $lang = 'jp';

    if ($lang == 'jp') {
        setlocale(LC_ALL, "ja_JP.sjis");
        $booking_dir = 'Yoyaku';
    } else {
        setlocale(LC_ALL, "en_GB.utf8");
        $booking_dir = 'Booking';
    }

    $booking_dir = "bookings";//ignore the old bookings

    $to = $_POST['email'];
    $jdate = DateTime::createFromFormat('Y-m-d', $jumpDate);

    //echo "Send Registration Email jumpDate $jumpDate <br>";

    $replacement = array(
        'EMAIL_BOOK_DATE'         => mb_convert_encoding(strftime("%Y-%m-%d (%a)", $jdate->getTimestamp()), 'UTF-8', 'SJIS'),
        'EMAIL_BOOK_TIME'         => $chkInTime,
        'EMAIL_JUMP_NUMBER'       => $noOfJump,
        'EMAIL_CUSTOMER_NAME'     => strtoupper($romajiname),
        'EMAIL_CUSTOMER_PHONE'    => $_POST['teleno'],
        'EMAIL_CONFIRMATION_LINK' => "http://" . $_SERVER['HTTP_HOST'] . "/$booking_dir/Thankyou.php?passkey=$confirmCode"
    );

    $headers = "Return-Path: {$config['confirmation_email']}\r\n";
    //$headers .= 'MIME-Version: 1.0' . "\r\n";
    $headers .= 'Content-type: text/html; charset=UTF-8' . "\r\n";
    mysql_query("SET NAMES UTF8;");

    //if paid //figure out if the booking was paid or not was paid
    //paidByCreditCard is defined in the calling script paymentreturn.php
    if (!isset ($paidByCreditCard)) $paidByCreditCard = false;
    $template = $paidByCreditCard ? BJHelper::getPaidConfirmationEmailTemplate($lang) : BJHelper::getConfirmationEmailTemplate($lang);

    if ($template) {
        $file = explode("\n", $template);
        $subject = trim(array_shift($file));
        $body = implode("\n", $file);

    } else {
        mail("{$config['confirmation_email']}", "Cannot locate customer confirmation template for $lang.", print_r($replacement, true), $headers);

    }

    $body = str_replace(
        array_map(function ($el) {
            return '{' . $el . '}';
        }, array_keys($replacement)),
        array_values($replacement),
        $body
    );

/*
     //Old email code pre QR
    $boundary = "next-part-" . rand(1000, 9999) . '-' . rand(100000, 999999);
    //$headers .= 'Content-Transfer-Encoding: base64' . "\r\n";
    $headers = "Return-Path: {$config['confirmation_email']}\r\n";
    $headers .= "Reply-To: $subdomain@bungyjapan.com\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: multipart/alternative;\r\n";
    $headers .= "\tboundary=\"$boundary\"\r\n";

    $content = chunk_split(base64_encode($body));
    //$text_content = chunk_split(base64_encode(strip_tags($body) . "\n" . $replacement['EMAIL_CONFIRMATION_LINK']));
    $body = <<<EOT
--$boundary
Content-Type: text/plain; charset="UTF-8"
Content-Transfer-Encoding: base64

$content

--$boundary--
EOT;

    mail($to, '=?UTF-8?B?' . base64_encode($subject) . '?=', $body, $headers, "-f {$config['confirmation_email']}");
*/

    $phone = db_input($_POST['teleno']);

    $qrString = json_encode([
        'tel'  => $phone,
        'time' => $chkInTime
    ]);

    $base64QRImgString = BookingQRCode::base64ImgString($qrString, true, 15);
    $body .= "<br><br>".$base64QRImgString;

    $boundary = "next-part-" . rand(1000, 9999) . '-' . rand(100000, 999999);
    //$headers .= 'Content-Transfer-Encoding: base64' . "\r\n";
    $headers = "Return-Path: {$config['confirmation_email']}\r\n";
    $headers .= "Reply-To: $subdomain@bungyjapan.com\r\n";
    $headers .= "MIME-Version: 1.0\r\n";
    $headers .= "Content-Type: multipart/alternative;\r\n";
    $headers .= "\tboundary=\"$boundary\"\r\n";

    $body = str_replace("\r\n", "<br>", $body);
    $content = chunk_split(base64_encode($body));

    $body = <<<EOT
--$boundary
Content-Type: text/html; charset="UTF-8"
Content-Transfer-Encoding: base64

$content

--$boundary--
EOT;

    mail($to, '=?UTF-8?B?' . base64_encode($subject) . '?=', $body, $headers, "-f {$config['confirmation_email']}");
}

function createTempBooking($resultJson)
{
    //it would be better to just create a finished booking instead
    $noteDateString = date("Y-m-d");
    $noteCardString = '';
    $romajiname = $_SESSION['lastname'] . ' ' . $_SESSION['firstname'];
    $noOfJump = $_SESSION['noOfJump'];
    $chkInTime = $_SESSION['check_in_time'];
    $jumpDate = $_SESSION['jump_date'];

    global $creditCardSimulation;
    if (!$creditCardSimulation) {
        $jumpPlace = $_SESSION['jumpPlace'];
        $_POST['email'] = $_SESSION['regemail'];//setting post so that the email script works
        $noteCardString = "Card Name: {$resultJson['transaction']['account']['holder']}";
    }

    $confirmCode = md5(uniqid(rand()));
    $dateString = date('Y\/m\/d');

    if ($creditCardSimulation) {
        $_POST['email'] = 'developer@standardmove.com';
        $chkInTime = "07:00 AM";
        $noOfJump = 3;
        $_SESSION['jumpPlace'] = CURRENT_SITE_ID;//site id
        $_SESSION['check_in_time'] = "07:00 AM";
        $_SESSION['noOfJump'] = 3;
        $romajiname = "Joe Smith";
        $_SESSION['teleno'] = substr(rand(111111111, 999999999), 0, 9);
        $_POST['teleno'] = substr(rand(111111111, 999999999), 0, 9);
        $_SESSION['regemail'] = "developer@standardmove.com";
        $_SESSION['modeTransport'] = "Car";
        $priceException = 1000;
        $noteCardString = "Joe Smith";
        $_SESSION['firstname'] = "Joe";
        $_SESSION['lastname'] = "Smith";
        $_SESSION['jump_date'] = date("Y-m-d");
    }

    $bookingData = [
        'site_id'           => CURRENT_SITE_ID,
        'ConfirmCode'       => $confirmCode,
        'BookingDate'       => $_SESSION['jump_date'],
        'BookingTime'       => $_SESSION['check_in_time'],
        'NoOfJump'          => $_SESSION['noOfJump'],
        'RomajiName'        => strtoupper($romajiname),
        'CustomerLastName'  => strtoupper($_SESSION['firstname']),
        'CustomerFirstName' => strtoupper($_SESSION['lastname']),
        'CustomerAddress'   => "NULL",
        'PostalCode'        => "NULL",
        'Prefecture'        => "NULL",
        'ContactNo'         => $_SESSION['teleno'],
        'CustomerEmail'     => $_SESSION['regemail'],
        'TransportMode'     => $_SESSION['modeTransport'],
        'place_id'          => $_SESSION['jumpPlace'],
        'Rate'              => getPriceException(),//undefined
        'Agent'             => "Credit Card",
        'Notes'             => "$noteDateString\n $noteCardString",
        'CollectPay'        => "Offsite"
    ];

    db_perform('customerregs_temp1', $bookingData);

    return array($jumpDate, $chkInTime, $noOfJump, $romajiname, $confirmCode, $bookingData);
}

function sendEmails($subdomain, $creditCardSimulation, $resultJson, $language)
{
    global $config;
    $headers = "Return-Path: {$config['confirmation_email']}\r\n";
    $headers .= "Reply-To: $subdomain@bungyjapan.com\r\n";
    $headers .= 'Content-type: text/html; charset=utf-8' . "\r\n";
    $headers .= "MIME-Version: 1.0\r\n";

    $msg = [];

    $msgFlat = '';
    if (!$creditCardSimulation) {
        $msgFlat = generateMessageForEmail($msg, $resultJson, $language);
    }
    if (!$creditCardSimulation) {
        @mail("dez@bungyjapan.com", 'NEW BUNGY CC PAYMENT', $msgFlat, $headers, "-f {$config['confirmation_email']}");
    }


    //sendMessageToDeveloper($msgFlat, $resultJson, $creditCardSimulation, $headers, $config);
}

if (!isset($creditCardSimulation)) $creditCardSimulation = false;

if ($creditCardSimulation) {
    $_GET["token"] = '';
    $_SESSION['address_encoding'] = "SJIS";
}

$token = $_GET["token"];

$resultJson = queryInpendiumServerWithToken($token);

$language = $_SESSION['language'];
sendEmails($subdomain, $creditCardSimulation, $resultJson, $language);

if ($creditCardSimulation) {
    $resultJson = array();
    $resultJson['transaction']['processing']['result'] = "ACK";
    $resultJson['transaction']['account']['holder'] = 'TESTING';
}

$successfulPayment = strstr($resultJson['transaction']['processing']['result'], "ACK");

if ($successfulPayment) {
    $priceException = getPriceException();
    list($jumpDate, $chkInTime, $noOfJump, $romajiname, $confirmCode, $bookingData) = createTempBooking($resultJson);

    if (mysql_insert_id() > 0) {
        //we insert the booking in to customerregs_temp1 then use the same code that converts it in to an actual
        //booking by including Thankyou_header.php. This is done for legacy reasons since Thankyou_header contains
        //a lot of logic we would need to reimpliment if we were going to insert directly
        //we set this before including Thankyou_header.php so that the confirmation code
        //can be used to automatically verify the booking and the code can function
        //---------------------------------------------------
        confirmBooking($confirmCode);

        sendRegistrationEmail($chkInTime, $noOfJump, $romajiname, $confirmCode, $config, $subdomain);
        //---------------------------------------------------
        unsetSession();
        header('Location: Thankyoupaid.php');
    }

} else {

    $_SESSION['backfrom'] = "paymentreturn";
    $_SESSION['payment_msg'] = $resultJson['transaction']['processing']['return']['message'];

    header('Location: enterpayment.php');
}
