<?php
include "../includes/application_top.php";

$_SESSION['noOfJump'] = $_POST["noOfJump"];

include 'includes/header_tags.php';
require("functions.php");
$languages = [
    'eng' => [
        '_conditionsAndPolicies' => "
            <tr>
                <td>
                    <h3>Conditions & Policies</h3>
                </td>
            </tr>
            <tr>
                <td>
                <h4>Physical Conditions:</h4>
                </td>
            </tr>
            <tr>
                <td>
                    1) Jumpers must be a minimum of 15 years old (13 years old at Nara Bungy). All jumpers under the age of 20 require their parent's or guardian's signature on the Waiver of Liability to jump. Those who will not be accompanied by their parent/guardian need to request a Waiver ahead of time and bring a signed copy with them on the day of their jump.
                    <br>
                    <br>
                    2) Jumpers must weigh a minimum of 40kg and a maximum of 105kg (100kg at Nara Bungy). No exceptions will be made.
                    <br>
                    <br>
                    3) As a general rule, if you are active at the moment and can take part in regular sporting activities (such as jogging, skiing or tennis),
                    then bungy jumping should be fine. Pregnant women however, are not allowed to jump.
                    <br>
                    <br>
                    4) If you suffer from or are taking medication for any of the following conditions, please consult your physician prior to booking and then inform our staff
                    of the conditions when you check in on the day of your jump: Heart Conditions, Epilepsy, High Blood Pressure, Diabetes, Asthma, Bone Disorders,
                    Neurological Disorders, Recent or Recurring Dislocations, Recent Eye Surgery, Prosthetic Limbs.
                </td>
            </tr>
            <tr>
                <td>
                    <h4>Alcohol Policy:</h4>
                </td>
            </tr>
            <tr>
                <td>
                    Due to safety, liability &amp; insurance issues, anyone found to have consumed alcohol on the day will not be allowed to bungy jump.
                </td>
            </tr>
            <tr>
                <td>
                <h4>Cancellation Policy:</h4>
                </td>
            </tr>
            <tr>
                <td>
                    A cancellation fee of 50% will be charged for bookings canceled the day before and 100% for bookings canceled on the day of the booking.
                    This also applies to partial cancellations where the original number of jumpers is reduced.
                </td>
            </tr>
            <tr>
                <td>
                    <h4>Weather Policy:</h4>
                </td>
            </tr>
            <tr>
                <td>
                    We operate in most weather conditions and will jump in the rain. The only time bungy will cease operations is if heavy &amp; consistent rains
                    cause the river to rise to a level that is not safe for our recovery raft to operate in.
                </td>
            </tr>
            <tr>
                <td>
                    <h4>Clothing &amp; Footwear:</h4>
                </td>
            </tr>
            <tr>
                <td>
                    Loose fitting &amp; comfortable clothes that are suitable for doing light physical activity are suggested. Weather can change suddenly in
                    the mountains so please come prepared with a warm outer layer as well as rainwear (especially during the Spring and Autumn).
                    <br>
                    <br>
                    Comfortable shoes with laces are recommended since the gravel path back from the riverside can be slippery and uneven. Sandals, \"Crocs\",
                    etc. are also fine. Please do not wear high-heels or boots that cover your ankles.

                </td>
            </tr>
            <tr>
            <tr>
                <td>
                    <h4>Payment:</h4>
                </td>
            </tr>
            <tr>
                    <td>
                        <div>
                            Payment on site is cash only. Credit cards will not be accepted.
                        </div>
                    </td>
                </tr>
            </tr>


        ",
        "_back" => "Back",
        "_agree" => "I have read &amp; understand the\nabove Conditions &amp; Policies"
    ],
    'jpn' => [
        '_conditionsAndPolicies' => "
                        <tr>
                    <td>
                        <h3><font>参加条件 / 注意事項</font></h3>
                    </td>
                </tr>
                
				<tr>
					<td><h4>健康条件：</h4></td>
				<tr>
                <tr>
                    <td>
                        <p>1. 年齢１５歳以上（奈良バンジーは13歳以上）、２０歳未満の場合は、ご両親等の法定代理人の方にご同伴いただくか、確認書に署名していただき、ご持参いただく必要があります。</p>

                        <p>2. 体重４０kg ー１０５kg (奈良バンジーは100kgまで)。</p>

                        <p>3. 持病（心臓病、高血圧、てんかん等）をお持ちの方や、脚や腕などに手術のご経験のある方は事前に医師とご相談ください。また、許可を得た場合も、当日受付の際に必ずスタッフにお知らせ下さい。</p>

                        <p>4. 妊娠されている方は参加できません。</p>
                    </td>
                </tr>
				<tr> <td><h4>飲酒：</h4></td> <tr>
                <tr>
                    <td>
                            当日飲酒をされている方はバンジージャンプに参加できません。
                    </td>
                </tr>
				<tr> <td><h4>キャンセル料：</h4></td> <tr>
                <tr>
                    <td>
                        <div>
                                ご予約完了後、キャンセルされる場合はお電話のみで対応可能となります。キャンセル料は前日キャンセルの場合ジャンプ料金の50%, 当日の場合100%となりますのでご了承下さい。
                        </div>
                    </td>
                </tr>
				<tr> <td><h4>天候：</h4></td> <tr>
                <tr>
                    <td>
                        <div>
                                雨天でも決行いたします。ただし、台風注意報・急激な川の増水がある場合は中止とさせていただく可能性がございますのでご了承下さい。その際は決定次第すみやかにご連絡をいたします。
                        </div>
                    </td>
                </tr>
				<tr> <td><h4>服装：</h4></td> <tr>
                <tr>
                    <td>
                        <p>1. <font>歩きやすい靴（ビーチサンダル等のかかとのない物でも可能だが、ブーツやヒールなどは不可）</font></p>

                        <p><font>2. 雨天時は濡れる可能性がある為、レインコートや着替えなどをご持参下さい。</font></p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <h4>支払い：</h4>
                    </td>
                </tr>
                <tr>
                    <td>
                        <div>
                            現地でのお支払いは現金のみとなっております。クレジットカードはご使用できませんのでご了承ください。
                        </div>
                    </td>
                </tr>

        ",
        "_back" => "戻る",
        "_agree" => "上記を承諾する"

    ]
];

$language = setLanguage();
requireHeader($language);
extract($languages[$language]);

?>
<script type="text/javascript">
    function processContinue() {
        if (document.getElementById('cont')) {
            document.disform.submit();
            return true;
        }
    }
    function procBack() {
        document.disform.action = "search.php";
        document.disform.submit();
        return true;
    }
</script>

<?= drawProgressForIndexAndLanguage(3, $language) ?>
<div id="content-container">
    <form name="disform" action="<?=getBaseUrl()?>registration.php" method="post">
        <input type="hidden" name="jumpPlace" value="<?= $_POST['jumpPlace']; ?>">
        <input type="hidden" name="jumpDate" value="<?= $_POST['jumpDate']; ?>">
        <input type="hidden" name="chkInTime" value="<?= $_POST['chkInTime']; ?>">
        <input type="hidden" name="noOfJump" value="<?= $_POST['noOfJump']; ?>">
        <table>
            <tbody>
            <a href="http://bungyjapan.com/downloads/waiver_<?php echo strtolower(APP_SUBDOMAIN) ?>_<?= $language ?>.pdf" download target="_blank">
                            <img class="u20ButtonSmaller" src="img/waiver_download_<?= $language ?>.png">
            </a>
            <?=$_conditionsAndPolicies?>
            <tr>
                <td>
                    <input class="button wide-button" type="button" name="cont" id="cont" value="<?=$_agree?>" onclick="processContinue();"/>
                </td>
            </tr>
            <tr>
                <td>
                    <input class="button wide-button back" type="button" name="cont" id="bck" value="<?=$_back?>" onclick="procBack();"/>
                </td>
            </tr>
            </tbody>
        </table>
    </form>
</div>
<? requireFooter($language) ?>

