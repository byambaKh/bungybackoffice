<?php

include "../includes/application_top.php";
require("functions.php");
/**
 * @param $i
 * @param $schedules
 *
 * @return array
 */
function getNextAndPreviousSlot($i, $schedules)
{
    $nextSlotTime = false;
    $previousSlotTime = false;
    $nextSlot = false;
    $previousSlot = false;
    $currentSlot = false;
    $currentSlotTime = $_POST['chkInTime'];

    if ($i >= $_POST['noOfJump']) {
        $currentSlotTime = $_POST['chkInTime'];
        $currentSlot = true;
    } else {

        $nextSlot = findNextAvailableTimeForNPeople($_POST['chkInTime'], $_POST['noOfJump'], $schedules);
        $previousSlot = findPreviousAvailableTimeForNPeople($_POST['chkInTime'], $_POST['noOfJump'], $schedules);

        $nextSlotTime = array_key_exists($nextSlot, $schedules['options']) ? $schedules['options'][$nextSlot]['value'] : false;
        $previousSlotTime = array_key_exists($previousSlot, $schedules['options']) ? $schedules['options'][$previousSlot]['value'] : false;

    }

    return [
        $currentSlotTime,
        $currentSlot,
        $nextSlotTime,
        $nextSlot,
        $previousSlotTime,
        $previousSlot
    ];
}

/**
 * @param $time
 * @param $schedules
 *
 * @return bool
 *
 */
function findIndexOfTime($time, $schedules)
{
    foreach ($schedules['options'] as $index => $schedule) {
        if ($schedule['value'] == $time && $schedule['open'] == 1) {
            return $index;
        }
    }

    return false;
}

function findPreviousAvailableTimeForNPeople($time, $people, $schedules)
{
    $index = findIndexOfTime($time, $schedules);

    $index--;
    for (; $index >= 0; $index--) {
        if ($schedules['options'][$index]['open'] == 0) continue; //skip closed slots
        if ($schedules['options'][$index]['avail'] >= $people) return $index;
    }

    return false;
}

function findNextAvailableTimeForNPeople($time, $people, $schedules)
{
    $index = findIndexOfTime($time, $schedules);

    $index++;//skip the current slot

    for (; $index < count($schedules['options']); $index++) {
        if ($schedules['options'][$index]['open'] == 0) continue; //skip closed slots
        if ($schedules['options'][$index]['avail'] >= $people) return $index;
    }

    return false;
}


function createDisplayTime($language)
{
    $display_time = '';

    if ($_POST["jumpDate"] && $_POST["chkInTime"]) {

        if ($language == 'jpn') {
            //setlocale(LC_TIME, 'japan');
            $locale = setlocale(LC_CTYPE, 0);
            $source_dt = $_POST['jumpDate'] . ' ' . str_replace(array('AM', 'PM'), '', $_POST['chkInTime']);
            $timestamp = DateTime::createFromFormat("Y-m-d H:i ", $source_dt)->getTimestamp();
            $display_time = mb_convert_encoding(strftime("%Y-%m-%d (%a) %p %H:%M ", $timestamp), 'UTF8', 'SJIS');
            echo '';

        } else if ($language = 'eng') {
            $display_time = "{$_POST['jumpDate']} at {$_POST['chkInTime']}";

        }
    }

    return $display_time;
}

/**
 * @param $schedules
 *
 * @return int
 */
function getAvailableJumpsForSlot($schedules, $time)
{
    $i = 0;
    foreach ($schedules['options'] as $schedule) {
        if ($schedule['value'] == $time && $schedule['open'] == 1) {
            $i = $schedule['avail'];
        };
    };
    if ($i < 1) {
        $i = 0;
    };

    return $i;
}

function drawPeopleDropDown($i)
{
    $index = 1;
    $output = '';

    for ($index = 1; $index <= $i; $index++) {
        $output .= "<option value='$index'>$index</option>\n";
    }

    return $output;
}

$_SESSION['jumpDate'] = $_POST['jumpDate'];
$_SESSION['chkInTime'] = $_POST["chkInTime"];
$_SESSION['noOfJump'] = $_POST["noOfJump"];

searchedForTimeSlot::insertSearchedForTimeSlot($_POST['jumpDate'], $_POST["chkInTime"], CURRENT_SITE_ID);

$schedules = getBookTimes($_POST["jumpDate"], 0);
$i = getAvailableJumpsForSlot($schedules, $_POST["chkInTime"]);
//$display_time = createDisplayTime($language, $schedules, $i);

if ($_POST['noOfJump'] <= 1) $people = "{$_POST['noOfJump']} person";
else $people = "{$_POST['noOfJump']} people";
$language = setLanguage();

list(
    $currentSlotTime,
    $currentSlot,
    $nextSlotTime,
    $nextSlot,
    $previousSlotTime,
    $previousSlot
    ) = getNextAndPreviousSlot($i, $schedules);

$languages = [
    'eng' => [
        '_largeGroups'  => "If you are trying to make reservations for a large group or are having trouble finding an available time slot, please contact us directly at {$config['site_phone']}.",
        '_proceed'      => "How would you like to proceed?",
        '_noSlots' => "There are no bookings available today for {$_POST['noOfJump']} people. Please call call {$config['site_phone']} as there are some free slots and you can split your booking up over multiple times. Or you can try search again on a different day." ,
        '_nextSlotOpen' => "{$_POST['chkInTime']} is not available for {$_POST['noOfJump']} people. The closest time is $nextSlotTime.",
        '_previousSlotOpen' => "{$_POST['chkInTime']} is not available for {$_POST['noOfJump']} people. The closest time is $previousSlotTime",
        '_nextAndPreviousOpen' => "{$_POST['chkInTime']} is not available for {$_POST['noOfJump']} people. The closest available times are $previousSlotTime and $nextSlotTime.",
        '_currentSlotOpen'   => "{$i} space(s) are available!",
        /*
        '_noJumps'      => "On $display_time, there are NO jumps available. <br><span class='emphasis'><h4><a href='/bookings/?lang=eng'>Please Search Again</a></h4></span>;",
        '_allJumps'     => "On $display_time, all JUMPS are available !!!",
        '_someJumps'    => "On $display_time, <span class='emphasis'>$i JUMPS</span> &nbsp;are available",
        */
        '_bookAtNext'   => "Book at $nextSlotTime",
        '_bookAtPrevious'   => "Book at $previousSlotTime",
        '_searchAgain'  => "Search Again at a Different Time",
        '_next'         => "Next"
    ],

    'jpn' => [
        '_largeGroups'  => "何度か検索されても空いているお時間 が見つからない場合はお電話で承ります。{$config['site_phone']}",
        '_proceed'      => "",
        '_noSlots'   => "本日{$_POST['noOfJump']} 名様で予約可能な時間帯がありません。　前後空きのある時間帯を分割して予約可能な場合もありますのでコールセンター　{$config['site_phone']}までお問い合わせください。 または別の日で再検索することができます。",
        '_nextSlotOpen' => "{$_POST['noOfJump']} 名様の空きがありません。もっとも近い時間帯は　{$nextSlotTime} です。",
        '_previousSlotOpen' => "{$_POST['noOfJump']} 名様の空きがありません。もっとも近い時間帯は　{$previousSlotTime} です。",
        '_nextAndPreviousSlotOpen' => "{$_POST['noOfJump']} 名様の空きがありません。　近い時間帯で予約可能な時間は　{$previousSlotTime}　と {$nextSlotTime} です。",
        '_currentSlotOpen'   => "{$i} 名様まで空いています。",
        /*
        '_noJumps'      => "$display_time、空きがありません。<br><h4><span class='emphasis'><a href='/bookings/?lang=jpn'>別の時間を探す</a></span></h4>",//TODO the logic for this differs between english and Japanese pages
        '_allJumps'     => "$display_time、には、すべてのャンプが可能です。",
        '_someJumps'    => "$display_time、<span class='emphasis'>$i 名様</span>まで空いています。",
        */
        '_bookAtNext'   => "$nextSlotTime 予約",
        '_bookAtPrevious'   => "$previousSlotTime 予約",
        '_searchAgain'  => "別の時間を探す",
        '_next'         => "次へ"
    ],

    'chn' => [
        '_largeGroups'  => "",
        '_proceed'      => "",
        '_SelectNumber' => "",
        '_noJumps'      => "",
        '_allJumps'     => "",
        '_someJumps'    => "",
        '_searchAgain'  => ""
    ]
];

extract($languages[$language]);

$showNextButton = false;
$showPreviousSlotButton = false;
$showNextSlotButton = false;


if ($currentSlot){
    $showNextButton = true;
    $message = $_currentSlotOpen;
} else if ($nextSlot && $previousSlot) {
    $showNextSlotButton = true;
    $showPreviousSlotButton = true;
    $message = $_nextAndPreviousSlotOpen;

} else if ($nextSlot) {
    $message = $_nextSlotOpen;
    $showNextSlotButton = true;

} else if ($previousSlot) {
    $message = $_previousSlotOpen;
    $showPreviousSlotButton = true;

} else {
    //no slots available
    $message = $_noSlots;
}

if ($i === null) {
    header('Location: index.php');//go back to start if $i = null
    exit();
}


requireHeader($language);
?>
<script type="text/javascript">

    function goback() {
        if (document.getElementById('back')) {
            document.myform.action = "/bookings/";
            document.myform.submit();
            return true;
        }
    }

    function limitDropDown() {
        var avail = <?php echo $i; ?>;
        //alert(len);
        if (avail > 0) {
            document.getElementById('noOfJump').length = avail;
        } else {
            hideDiv();
        }
    }
    function hideDiv() {
        document.getElementById('dv2').style.display = "none";
    }

    window.onload = function () {
        //limitDropDown();
    };

    jQuery(document).ready(function () {
        jQuery('.time-button').click(function (event) {
            event.preventDefault();
            var time = jQuery(this).data('booking-time');
            console.log('time-button :' + time + ' pressed.');
            jQuery('#check-in-time').val(time);
            document.myform.submit();
        });
    });

</script>
<style>
    h2, h4 {
        text-align: center;
    }

</style>
<?= drawProgressForIndexAndLanguage(2, $language) ?>
<div id="content-container">
    <form action="<?= getBaseUrl() ?>disclaimer.php" name="myform" method="post">
        <input type="hidden" name="jumpPlace" value="<?= $_POST['jumpPlace']; ?>">
        <input type="hidden" name="jumpDate" value="<?= $_POST['jumpDate']; ?>">
        <input id="check-in-time" type="hidden" name="chkInTime" value="<?= $_POST['chkInTime']; ?>">
        <input type="hidden" name="noOfJump" value="<?= $_POST['noOfJump']; ?>">

        <h2> <?= $message ?> </h2>
        <br>
        <div id="dv2">
            <table>
                <? if ($showNextButton): ?>
                    <tr>
                        <td colspan="3">
                            <input class="wide-button" type="submit" id="gobutton" value="<?= $_next ?>">
                        </td>
                    </tr>
                <? endif; ?>
                <? if ($showPreviousSlotButton): ?>
                    <tr>
                        <td colspan="3">
                            <input class="wide-button time-button" data-booking-time="<?= $previousSlotTime ?>" type="submit" id="go-next-slot-button" value="<?= $_bookAtPrevious ?>">
                        </td>
                    </tr>
                <? endif; ?>
                <? if ($showNextSlotButton): ?>
                    <tr>
                        <td colspan="3">
                            <input class="wide-button time-button" data-booking-time="<?= $nextSlotTime ?>" type="submit" id="go-previous-slot-button" value="<?= $_bookAtNext ?>">
                        </td>
                    </tr>
                <? endif; ?>
                <tr>
                    <td colspan="3">
                        <input type="button" id="back" class="button back wide-button" value="<?= $_searchAgain ?>" onClick="goback();">
                    </td>
                </tr>
                <tr>
                    <td colspan="3">
                        <br>
                        <?= $_largeGroups ?>
                    </td>
                </tr>
            </table>
        </div>
    </form>
</div>
<? requireFooter($language); ?>

