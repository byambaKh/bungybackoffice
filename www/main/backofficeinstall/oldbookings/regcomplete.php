<?php include '../includes/application_top.php';
require("functions.php");

if (!isset($creditCardSimulation)) $creditCardSimulation = false;

$language = setLanguage();
requireHeader($language);

$languages = [
    'eng' => [
        '_anEmailContaining' => "
            <h5>An email containing a confirmation link has been sent to the email address that you provided.</h5>
            <h5>If you don't find the email in your inbox, don't forget to check your junk email folder as well.</h5>
            <h2> Your booking cannot be completed until you have clicked on that confirmation link. </h2>
            <h5>To avoid disappointment and guarantee your requested date &amp; time, please confirm as soon as possible.</h5>
        ",
        "_return" => "Return to Bungy Japan's Homepage"

    ],
    'jpn' => [
        '_anEmailContaining' => "
        <h1>＊注意　この時点ではまだ予約は完了しておりません。</h1>
        <h5> 前の画面で記入いただいたメールアドレスに予約内容確認の メールを送信しました。このメール内に予約確定のリンクが 記載されていますので、そのリンクをクリックして予約を 確定させてください。<br>
        弊社の自動メールが迷惑メールとして認識されている場合が ございますので、お客様の迷惑メールフォルダもご確認を お願いいたします。<br>
        尚、予約確定にあたってトラブルを防ぐため、できるだけ早めに予約の確認を行ってください。<br>
        <br>
        </h5> ",
        "_return" => "バンジージャパンのホームページへ戻る"
    ],
];

extract($languages[$language]);
unset($_SESSION['jumpPlace']);
unset($_SESSION['jumpDate']);
unset($_SESSION['chkInTime']);
unset($_SESSION['noOfJump']);
unset($_SESSION['lastname']);
unset($_SESSION['firstname']);
unset($_SESSION['regemail']);
unset($_SESSION['teleno']);
unset($_SESSION['total_price']);
unset($_SESSION['modeTransport']);
unset($_SESSION['payby']);
unset($_SESSION['discount']);
?>
<?= drawProgressForIndexAndLanguage(4, $language) ?>
<div id="content-container">
    <?=$_anEmailContaining?>
    <br>
    <!--
    <div>
        <input type="button" value="<?=$_return?>" onClick="document.location='http://www.bungyjapan.com/'">
    </div>-->
</div>
<? requireFooter($language); ?>

  
  
