<?php
require_once("../includes/application_top.php");
require("functions.php");
//include("../includes/Thankyou_header.php");
//------------------------>

/**
 * @param $booking
 *
 * @return int
 */
function getCountOfBookingsRegisteredWithPhoneNumber($booking)
{
    $total_registered = 0;
    $sql = "SELECT count(*) AS total FROM customerregs1
			WHERE
				ContactNo = '" . db_input($booking['ContactNo']) . "'
				AND DeleteStatus = 0
				AND Checked = 0
				AND bookingDate >= '" . date("Y-m-d") . "'";
    $res = mysql_query($sql);

    if ($row = mysql_fetch_assoc($res)) {
        $total_registered += $row['total'];
    }

    return $total_registered;
}

/**
 * @param $booking
 *
 * @return array
 */
function totalJumpsInTimeSlot($booking)
{
    $total = 0;
    $sql = "SELECT SUM(NoOfJump) as total
			from customerregs1
			where
				site_id = '" . CURRENT_SITE_ID . "' and
				DeleteStatus = 0 and
				BookingDate = '{$booking['BookingDate']}' and
				BookingTime = '{$booking['BookingTime']}'";
    $res = mysql_query($sql);
    if ($row = mysql_fetch_assoc($res)) {
        $total = $row['total'];
    }

    return $total;
}

/**
 * @param $config
 * @param $booking
 *
 * @return array
 */
function confirmBooking($booking)
{
    global $config;
    // normal booking process
    // delete temp record
    // unset temp ID and ConfirmCode

    $id = $booking['CustomerRegID'];

    unset($booking['CustomerRegID']);
    unset($booking['ConfirmCode']);
    // fill other default values
    $booking['OtherNo'] = '';
    $booking['BookingType'] = 'Website';
    $booking['Rate'] = getFirstJumpRate(CURRENT_SITE_ID, $booking['BookingDate']);
    $booking['RateToPay'] = 0;
    $booking['BookingReceived'] = date("Y-m-d H:i:s");

    //if this is a not a credit card booking, use these defaults
    //else these things have already been defined for us in the
    //paymentreturn.php page
    if ($booking['Agent'] != "Credit Card") {
        $booking['CollectPay'] = 'Onsite';
        $booking['Agent'] = 'NULL';
        $booking['Notes'] = 'Internet (' . date("Y/m/d") . ")";
    }
    
    if($booking['discount']) $booking['Rate'] -= $booking['discount'];
    if($booking['discount']) $booking['Notes'] .= ' Discounted (' . $booking['discount'] . ' yen)';
    unset($booking['discount']);
    
    if ($r = db_perform('customerregs1', $booking)) {
        $error_code = 'success';
        $sql = "DELETE FROM customerregs_temp1 WHERE CustomerRegID = $id";
        mysql_query($sql);
    } else {
        $error_code = 'notsaved';
    }
    $booking['status_code'] = $error_code;

    return array($booking, $error_code);
}

/**
 * @param $passkey
 * @param $config
 *
 * @return string
 */
function confirmOrRejectBookingForPasskey($passkey)
{
    global $config;
    $passkey = mysql_real_escape_string($passkey);

    $sql = "SELECT * FROM customerregs_temp1 WHERE confirmcode = '" . $passkey . "'";
    $result = mysql_query($sql);
    $error_code = "success";
    $booking = array();
    if ($result && ($booking = mysql_fetch_assoc($result))) {
        // check total of registered jumps for this date/time
        $total = totalJumpsInTimeSlot($booking);
        // check if this tel number already have jump registered with status not deleted and not checked-out
        $total_registered = getCountOfBookingsRegisteredWithPhoneNumber($booking);

        if ($booking['Agent'] === "Credit Card") $total_registered = 0;//allow credit card to make multiple bookings in a day
        $total_registered = 0;//Allow multiple bookings with the same phone number for regular bookings as well

        if ($total + $booking['NoOfJump'] > $config['online_slots']) {
            //if this booking has less space than available
            $error_code = "denied";
            return $error_code;

        } else if ($total_registered > 0) {
            // if registered phone number records exists - deny booking
            $error_code = "denied_phone";
            return $error_code;

        } else {
            list($booking, $error_code) = confirmBooking($booking);
            send_backup_email($booking);
            return $error_code;
        }
    } else {
        $error_code = "incorrect";
        return $error_code;

    }
}

$passkey = $_GET['passkey'];
$error_code = confirmOrRejectBookingForPasskey($passkey);
//------------------------->
$language = setLanguage();
requireHeader($language);

$languageCodes = [
    'jpn' => 'jp',
    'eng' => 'en',
];

$languageCode = $languageCodes[$language];

if (!empty($error_code)) {
    if ($error_code == 'success') echo drawProgressForIndexAndLanguage(5, $language);
    if ($content = BJHelper::getTemplate($error_code, $languageCode)) {
        echo mb_convert_encoding($content, 'UTF8', 'SJIS');
    } else {
        echo "Error happen: " . $error_code;
    }
} else {
    echo drawProgressForIndexAndLanguage(5, $language);
    echo mb_convert_encoding(BJHelper::getTemplate('success', $languageCode), 'UTF8', 'SJIS');
}
requireFooter($language);
