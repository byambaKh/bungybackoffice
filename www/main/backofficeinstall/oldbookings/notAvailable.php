<?php
require_once("../includes/application_top.php");
require("functions.php");
header("Last-Modified: " . gmdate("D, d M Y H:i:s") . " GMT");
header("Cache-Control: no-store, no-cache, must-revalidate");
header("Cache-Control: post-check=0, pre-check=0", false);
header("Pragma: no-cache");

$languages = [
    'eng' => [
        '_notAvailable' => 'Not Available!!!',
    ],
    'jpn' => [
        '_notAvailable' => '使用不可！！！',
    ],
    'chn' => [
    ],
];
$language = setLanguage();

requireHeader($language);
extract($languages[$language]);
?>

<center><h2><?=$_notAvailable?></h2></center><br>
<? requireFooter($language) ?>
