<?php

require_once("../includes/application_top.php");
require('functions.php');
$error_code = $_GET['error_code'];

$language = setLanguage();//3 letter code
$lang = convertLanguageCodes($language);//2 letter code


require('pageParts/header.php');

if ($lang == 'ja') {
    $lang = 'jp';
}//templates table use Ja instead of Jp
echo  mb_convert_encoding(BJHelper::getTemplate($error_code, $lang), 'UTF8', 'SJIS');

require('pageParts/footer.php');
