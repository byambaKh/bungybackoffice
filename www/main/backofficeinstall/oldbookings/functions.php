<?php

function requireHeader($language)
{
    //require("pageParts/header_$language.php");
    require("pageParts/header.php");
}

function requireFooter($language)
{
    require("pageParts/footer.php");
}

//converts from 3 letters(used in the booking system) to 2 letters (userd in wordpress)
function convertLanguageCodes($language)
{
    $lang = 'ja';
    if ($language == 'eng') {
        $lang = 'en';
    }

    return $lang;
}

function setLanguage()
{
    $language = 'jpn';
    //if the language is set in the url
    if (isset($_GET['lang'])) {
        //set the session to that language and return it
        $_SESSION['language'] = $_GET['lang'];

        return $_GET['lang'];
    }
    //if the language is set in the session
    if (isset($_SESSION['language'])) {
        return $_SESSION['language'];
    }

    return $language;//the default langauge
}


function setLanguage_()
{
    $language = 'eng';//default to Japanese

    if (isset($_GET['lang'])) {
        if (
            isset($_SESSION['language']) &&
            (
            ($_SESSION['language'] == $_GET['lang']) //no change in language
                //||(!isset($_SESSION['language'])) //no language specified in the $_GET
            )
        ) {
            return $_SESSION['language'];
        }

        $supportedLanguages = ['eng', 'jpn'/*, 'chn'*/];
        if (in_array($_GET['lang'], $supportedLanguages)) {
            $_SESSION['lang'] = $language = $_GET['lang'];
        }
    } else if (isset($_SESSION['language'])) {
        $language = $_SESSION['language'];
    }


    return $language;

    //if the language is being set in $_GET change to that
}

function getBaseUrl()
{
    return getTransferProtocol() . "{$_SERVER['HTTP_HOST']}/bookings/";
}

function drawProgress($index)
{
    return "<div class='progress-bar'> <img src='img/progress_$index.png'></div>";
}

function drawProgressForIndexAndLanguage($index, $language)
{
    return "
<div id ='progress'>
    <img id='progress-bar' src='img/progress_{$index}.png'>
    <img id='progress-description' src='img/progress_{$language}.png'>
    <img id='progress-description-small' src='img/progress_{$language}_small.png'>
</div>";

}
