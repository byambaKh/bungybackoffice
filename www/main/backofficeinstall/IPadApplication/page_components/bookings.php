<div class="row">
    <div class="col-sm-10">
        <button class="btn btn-default">&lt; Back</button>
    </div>
</div>

<fieldset>
    <legend class="col-md-10">Minakami Summary</legend>
    <div class="form-group col-sm-10">
        <label for="first-name" class="col-sm-2 control-label">Total Jumps</label>

        <div class="col-sm-8">
            <span>54 (22 Morning &amp; 32 Afternoon)</span>
        </div>
    </div>
    <div class="form-group col-sm-10">
        <label for="first-name" class="col-sm-2 control-label">Check Ins</label>

        <div class="col-sm-8">
            <span>14</span>
        </div>
    </div>
    <div class="form-group col-sm-10">
        <label for="first-name" class="col-sm-2 control-label">Date</label>

        <div class="col-sm-8">
            <span>[calendar date input]</span>
        </div>
    </div>

</fieldset>
<fieldset>
    <legend>Waivers</legend>
    <h4>This booking has 4 Waivers but 2 Jumps are free.
        <small>Please have the remaining customers check in.</small>
    </h4>
    <table class="table table-striped table-hover">
        <tr class="">
            <th>Time</th>
            <th>Name</th>
            <th>Jumps</th>
            <th>Agent</th>
            <th>Price (&yen;)</th>
            <th>Photos</th>
            <th>Cancel</th>
            <th>Payment (Onsite/Offsite)</th>
            <th>Notes</th>
        </tr>
        <tr class="success">
            <td>9:00</td>
            <td>Jospeh Banks</td>
            <td>4</td>
            <td>--</td>
            <td>24,000</td>
            <td>3</td>
            <td>No</td>
            <td>Offsite</td>
            <td>
                <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et</small>
            </td>
        </tr>
        <tr class="success">
            <td>9:00</td>
            <td>Lisa Snow</td>
            <td>1</td>
            <td>TOP</td>
            <td>6,000</td>
            <td>0</td>
            <td>No</td>
            <td>Offsite</td>
            <td>
                <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et</small>
            </td>
        </tr>
        <tr class="success">
            <td>9:00</td>
            <td>Timmy Sparks</td>
            <td>5</td>
            <td>Canyons</td>
            <td>30,000</td>
            <td>3</td>
            <td>No</td>
            <td>Onsite</td>
            <td>
                <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et</small>
            </td>
        </tr>
        <tr class="success">
            <td>9:00</td>
            <td>Leon Stone</td>
            <td>2</td>
            <td>MTB</td>
            <td>12,000</td>
            <td>0</td>
            <td>Yes</td>
            <td>Offsite</td>
            <td>
                <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et</small>
            </td>
        </tr>
        <tr class="danger">
            <td>9:00</td>
            <td>Timmy Rogers</td>
            <td>4</td>
            <td>Max</td>
            <td>24,000</td>
            <td>1</td>
            <td>No</td>
            <td>Onsite</td>
            <td>
                <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et</small>
            </td>
        </tr>
        <tr class="danger">
            <td>9:00</td>
            <td>Sarah Baker</td>
            <td>3</td>
            <td>--</td>
            <td>18,000</td>
            <td>0</td>
            <td>No</td>
            <td>Onsite</td>
            <td>
                <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et</small>
            </td>
        </tr>
        <tr class="warning">
            <td>9:00</td>
            <td>Jospeh Banks</td>
            <td>4</td>
            <td>--</td>
            <td>24,000</td>
            <td>3</td>
            <td>No</td>
            <td>Offsite</td>
            <td>
                <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et</small>
            </td>
        </tr>
        <tr class="danger">
            <td>9:00</td>
            <td>Lisa Snow</td>
            <td>1</td>
            <td>TOP</td>
            <td>6,000</td>
            <td>0</td>
            <td>No</td>
            <td>Offsite</td>
            <td>
                <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et</small>
            </td>
        </tr>
        <tr class="danger">
            <td>9:00</td>
            <td>Timmy Sparks</td>
            <td>5</td>
            <td>Canyons</td>
            <td>30,000</td>
            <td>3</td>
            <td>No</td>
            <td>Onsite</td>
            <td>
                <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et</small>
            </td>
        </tr>
        <tr class="danger">
            <td>9:00</td>
            <td>Leon Stone</td>
            <td>2</td>
            <td>MTB</td>
            <td>12,000</td>
            <td>0</td>
            <td>Yes</td>
            <td>Offsite</td>
            <td>
                <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit...</small>
            </td>
        </tr>
        <tr class="success">
            <td>9:00</td>
            <td>Timmy Rogers</td>
            <td>4</td>
            <td>Max</td>
            <td>24,000</td>
            <td>1</td>
            <td>No</td>
            <td>Onsite</td>
            <td>
                <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit...</small>
            </td>
        </tr>
        <tr class="success">
            <td>9:00</td>
            <td>Sarah Baker</td>
            <td>3</td>
            <td>--</td>
            <td>18,000</td>
            <td>0</td>
            <td>No</td>
            <td>Onsite</td>
            <td>
                <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit...</small>
            </td>
        </tr>
        <tr class="success">
            <td>9:00</td>
            <td>Jospeh Banks</td>
            <td>4</td>
            <td>--</td>
            <td>24,000</td>
            <td>3</td>
            <td>No</td>
            <td>Offsite</td>
            <td>
                <small>Lorem ipsum dolor sit amet, consectetur adipiscing ...</small>
            </td>
        </tr>
        <tr class="warning">
            <td>9:00</td>
            <td>Lisa Snow</td>
            <td>1</td>
            <td>TOP</td>
            <td>6,000</td>
            <td>0</td>
            <td>No</td>
            <td>Offsite</td>
            <td>
                <small>Lorem ipsum dolor sit amet, consectetur adipiscing ...</small>
            </td>
        </tr>
        <tr class="success">
            <td>9:00</td>
            <td>Timmy Sparks</td>
            <td>5</td>
            <td>Canyons</td>
            <td>30,000</td>
            <td>3</td>
            <td>No</td>
            <td>Onsite</td>
            <td>
                <small>Lorem ipsum dolor sit amet, consectetur adipiscing ...</small>
            </td>
        </tr>
        <tr class="warning">
            <td>9:00</td>
            <td>Leon Stone</td>
            <td>2</td>
            <td>MTB</td>
            <td>12,000</td>
            <td>0</td>
            <td>Yes</td>
            <td>Offsite</td>
            <td>
                <small>Lorem ipsum dolor sit amet, consectetur adipiscing ...</small>
            </td>
        </tr>
        <tr class="danger">
            <td>9:00</td>
            <td>Timmy Rogers</td>
            <td>4</td>
            <td>Max</td>
            <td>24,000</td>
            <td>1</td>
            <td>No</td>
            <td>Onsite</td>
            <td>
                <small>Lorem ipsum dolor sit amet, consectetur adipiscing ...</small>
            </td>
        </tr>
        <tr class="success">
            <td>9:00</td>
            <td>Sarah Baker</td>
            <td>3</td>
            <td>--</td>
            <td>18,000</td>
            <td>0</td>
            <td>No</td>
            <td>Onsite</td>
            <td>
                <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et</small>
            </td>
        </tr>
        <tr>
            <td colspan="10">
                <button type="button" class="btn btn-primary col-sm-8 btn-block btn-lg">Add Booking</button>
            </td>
        </tr>
    </table>
</fieldset>
<fieldset>
    <legend>Actions</legend>
    <button type="button" class="btn btn-primary col-sm-8 btn-block btn-lg">Add Booking</button>
    <button type="button" class="btn btn-primary col-sm-8 btn-block btn-lg">Add Second Jump</button>
    <button type="button" class="btn btn-primary col-sm-8 btn-block btn-lg">All Waivers</button>
    <button type="button" class="btn btn-primary col-sm-8 btn-block btn-lg">Add Cord Report</button>
    <button type="button" class="btn btn-primary col-sm-8 btn-block btn-lg">Add Daily Sales Report</button>
    <button type="button" class="btn btn-primary col-sm-8 btn-block btn-lg">Discount Auth Code</button>
</fieldset>
