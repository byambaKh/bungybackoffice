<h1>MINA01 Self Check-In Machine</h1>
<fieldset>
    <legend class="col-xs-10">Summary</legend>
    To Impliment: Tickets and Certificate printed, cash taken in, balance
    <!--
    <div class="form-group col-xs-10">
        <label for="" class="control-label col-xs-2">Summary</label>
    </div>
    -->
</fieldset>
<fieldset>
    <legend class="col-xs-10">Cash Machine</legend>
    <div class="form-group col-xs-10">
        <label for="box-1000" class="col-xs-2 control-label ">&yen;1000 Box</label>

        <div class="col-xs-8">
            <input type="text" name="box-1000" class="form-control" value="">
        </div>
    </div>

    <div class="form-group col-xs-10">
        <label for="box-5000" class="control-label col-xs-2">&yen;5000 Box</label>

        <div class="col-xs-8">
            <input type="text" name="box-5000" class="form-control" value="">
        </div>
    </div>

    <div class="form-group col-xs-10">
        <label for="box-recycler" class="control-label col-xs-2">Recycler</label>

        <div class="col-xs-8">
            <input type="text" name="box-recycler" class="form-control" value="">
        </div>
    </div>
</fieldset>

<fieldset>
    <legend class="col-xs-10">Scale</legend>
    <div class="form-group col-xs-10">
        <label for="weight-offset" class="col-xs-2">Weight Offset (Kgs)</label>

        <div class="col-xs-8">
            <input type="text" class="form-control" name="weight-offset" value="0">
        </div>
    </div>
</fieldset>

<fieldset>
    <legend class="col-xs-10">Ticket Printer</legend>
    <div class="form-group col-xs-10">
        <label for="tickets-left-in-roll" class="control-label col-xs-2">Tickets Left In Roll</label>

        <div class="col-xs-8">
            <input class="form-control" name="tickets-left-in-roll" value="">
        </div>
        <button class="btn btn-primary btn-block btn-lg">Reload Printer</button>
    </div>
</fieldset>

<fieldset>
    <legend class="col-md-10">Certificate Printer</legend>
    <label for="paper-ticket-count" class="control-label xs-col-2">Certificate</label>
    <input type="text" name="paper-ticket-count" class="form-control xs-col-8" value="">
    <h4>20 Certificates printed so far</h4>
    <button class="btn btn-success btn-block btn-lg">Update</button>
</fieldset>

<fieldset>
    <legend>Actions</legend>
    <button class="btn btn-primary col-xs-8 btn-block btn-lg">Print Certificate</button>
    <button class="btn btn-success col-xs-8 btn-block btn-lg">Cash Out</button>
    <button class="btn btn-success col-xs-8 btn-block btn-lg">Recycle 1000 Yens</button>
    <button class="btn btn-success col-xs-8 btn-block btn-lg">Send All To Recycler</button>
</fieldset>
