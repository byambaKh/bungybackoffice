<fieldset>
    <legend class="col-md-8">Customer Information</legend>
    <div class="form-group col-sm-10">
        <label for="first-name" class="col-sm-2 control-label">First Name</label>

        <div class="col-sm-8">
            <input type="text" name="first-name" class="form-control" placeholder="First Name" required>
        </div>
    </div>

    <div class="form-group col-sm-10">
        <label for="last-name" class="col-sm-2 control-label">Last Name</label>

        <div class="col-sm-8">
            <input type="text" name="last-name" class="form-control" placeholder="" required>
        </div>
    </div>

    <div class="form-group col-sm-10">
        <label for="phone-number" class="col-sm-2 control-label">Phone Number</label>

        <div class="col-sm-8">
            <input type="text" name="transport" class="form-control" placeholder="" required>
        </div>
    </div>

    <div class="form-group col-sm-10">
        <label for="transport" class="col-sm-2 control-label">Transportation</label>

        <div class="col-sm-8">
            <select name="transport" class="form-control">
                <option value="" name="Trains">Local Train</option>
                <option value="" name="Bullet">Bullet Train</option>
                <option value="" name="Car">Car</option>
                <option value="" name="Bus">Bus</option>
                <option value="" name="Motorbike">Motorbike</option>
            </select>
        </div>
    </div>
</fieldset>

<fieldset>
    <legend class="col-md-10">Jump Information</legend>
    <div class="form-group col-sm-10">
        <label for="place" class="col-sm-2 control-label">Place</label>

        <div class="col-sm-8">
            <select name="place" class="form-control">
                <option value="0">Minakami</option>
                <option value="1">Sarugakyo</option>
                <option value="2">Ryujin</option>
                <option value="3">...</option>
                <option value="4">...</option>
            </select>
        </div>
    </div>

    <div class="form-group col-sm-10">
        <label for="time" class="col-sm-2 control-label">Time</label>

        <div class="col-sm-8">
            <input type="text" name="time" class="form-control" placeholder="" required>
        </div>
    </div>

    <div class="form-group col-sm-10">
        <label for="date" class="col-sm-2 control-label">Date</label>

        <div class="col-sm-8">
            <input type="" name="date" class="form-control" placeholder="" required>
        </div>
    </div>

    <div class="form-group col-sm-10">
        <label for="number-of-jumps" class="col-sm-2 control-label">Number of Jumps</label>

        <div class="col-sm-8">
            <select name="" class="form-control">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
            </select>
        </div>
    </div>
</fieldset>

<fieldset>
    <legend class="col-md-10">Payment Information</legend>
    <div class="form-group col-sm-10">
        <label for="payment" class="col-sm-2 control-label">Payment Made</label>

        <div class="col-sm-8">
            <input type="radio" name="payment" value="Onsite"> Onsite
            <input type="radio" name="payment" value="Offsite"> Offsite
        </div>
    </div>

    <div class="form-group col-sm-10">
        <label for="Agent" class="col-sm-2 control-label">Agent</label>

        <div class="col-sm-8">
            <select name="agent" class="form-control">
                <option value="noagent">No Agent</option>
                <option value="agent1">Agent1</option>
                <option value="agent2">Agent2</option>
                <option value="agent3">Agent3</option>
                <option value="agent4">Agent4</option>
                <option value="agent5">Agent5</option>
                <option value="agent6">Agent6</option>
                <option value="agent7">Agent7</option>
                <option value="agent8">Agent8</option>
            </select>
        </div>
    </div>

    <div class="form-group col-sm-10">

        <label for="price" class="col-sm-2 control-label">Price</label>

        <div class="col-sm-3">
            <select name="price" class="form-control">
                <option value="agent1">8000</option>
                <option value="agent2">7000</option>
                <option value="agent3">6000</option>
                <option value="agent4">5000</option>
                <option value="agent5">4000</option>
            </select>
        </div>

        <label for="price-count" class="col-sm-1 control-label">X</label>

        <div class="col-sm-3">
            <select name="price-count" class="form-control">
                <option value="1">1</option>
                <option value="2">2</option>
                <option value="3">3</option>
                <option value="4">4</option>
                <option value="5">5</option>
                <option value="6">6</option>
            </select>
        </div>
    </div>
</fieldset>

<fieldset>
    <legend class="col-md-10">Notes</legend>
    <div class="form-group col-sm-10">
        <label for="notes" class="col-sm-2 control-label">Booking Notes</label>

        <div class="col-sm-8">
			<textarea name="notes" class="form-control" rows="" coloumns="">
			Notes regarding the booking
			</textarea>
        </div>
    </div>

    <div class="form-group col-sm-10">
        <label for="changed-by" class="col-sm-2 control-label">Changed By</label>

        <div class="col-sm-8">
            <select name="changed-by" class="form-control">
                <option value="nostaff">----</option>
                <option value="staff1">Staff1</option>
                <option value="staff2">Staff2</option>
                <option value="staff3">Staff3</option>
                <option value="staff4">Staff4</option>
                <option value="staff5">Staff5</option>
                <option value="staff6">Staff6</option>
                <option value="staff7">Staff7</option>
                <option value="staff8">Staff8</option>
            </select>
        </div>
    </div>
    <button type="button" class="btn btn-success col-sm-10 btn-block btn-lg">Update</button>
</fieldset>


<fieldset>
    <legend>Actions</legend>
    <button type="button" class="btn btn-primary col-sm-10 btn-block btn-lg">Add Second Jump</button>
    <button type="button" class="btn btn-danger col-sm-10 btn-block btn-lg">Cancel Booking</button>
</fieldset>
