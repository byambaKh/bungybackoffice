<div class="row">
    <div class="col-sm-10">
        <button class="btn btn-default">&lt; Back</button>
    </div>
</div>

<fieldset>
    <legend class="col-md-12">Booking Summary</legend>
    <div class="form-group col-sm-10">
        <label for="first-name" class="col-sm-2 control-label">Booking Name</label>

        <div class="col-sm-8">
            <p>John Smith</p>
        </div>
    </div>

    <div class="form-group col-sm-10">
        <label for="first-name" class="col-sm-2 control-label">Phone Number</label>

        <div class="col-sm-8">
            <p>0123-456-7890</p>
        </div>
    </div>

    <div class="form-group col-sm-10">
        <label for="first-name" class="col-sm-2 control-label">Email</label>

        <div class="col-sm-8">
            <p>customer@bungyjapan.com</p>
        </div>
    </div>

    <div class="form-group col-sm-10">
        <label for="first-name" class="col-sm-2 control-label">Location</label>

        <div class="col-sm-8">
            <p>Minakami</p>
        </div>
    </div>

    <div class="form-group col-sm-10">
        <label for="first-name" class="col-sm-2 control-label">Date &amp; Time</label>

        <div class="col-sm-8">
            <p>2015-09-10 @ 10:00 AM</p>
        </div>
    </div>
</fieldset>

<fieldset>
    <legend class="col-md-10">Waiver Information</legend>
    <div class="form-group col-sm-10">
        <label for="first-name" class="col-sm-2 control-label">First Name</label>

        <div class="col-sm-8">
            <input type="text" name="first-name" class="form-control" placeholder="" required>
        </div>
    </div>

    <div class="form-group col-sm-10">
        <label for="last-name" class="col-sm-2 control-label">Last Name</label>

        <div class="col-sm-8">
            <input type="text" name="last-name" class="form-control" placeholder="" required>
        </div>
    </div>

    <div class="form-group col-sm-10">
        <label for="phone-number" class="col-sm-2 control-label">Phone Number</label>

        <div class="col-sm-8">
            <input type="text" name="transport" class="form-control" placeholder="" required>
        </div>
    </div>

    <div class="form-group col-sm-10">
        <label for="phone-number" class="col-sm-2 control-label">Email</label>

        <div class="col-sm-8">
            <input type="text" name="transport" class="form-control" placeholder="" required>
        </div>
    </div>

    <div class="form-group col-sm-10">
        <label for="phone-number" class="col-sm-2 control-label">Birthday</label>

        <div class="col-sm-8">
            <input type="text" name="transport" class="form-control" placeholder="" required>
        </div>
    </div>

    <div class="form-group col-sm-10">
        <label for="phone-number" class="col-sm-2 control-label">Weight</label>

        <div class="col-sm-8">
            <input type="text" name="transport" class="form-control" placeholder="" required>
        </div>
    </div>

    <div class="form-group col-sm-10">
        <label for="phone-number" class="col-sm-2 control-label">Prefecture</label>

        <div class="col-sm-8">
            <input type="text" name="transport" class="form-control" placeholder="" required>
        </div>
    </div>

    <div class="form-group col-sm-10">
        <label for="phone-number" class="col-sm-2 control-label">Sex</label>

        <div class="col-sm-8">
            <select name="" class="form-control">
                <option value="male">male</option>
                <option value="female">female</option>
            </select>
        </div>
    </div>
        <button type="button" class="btn btn-success btn-block btn-lg">Save/Update</button>
</fieldset>


<fieldset>
    <legend>Actions</legend>
    <button type="button" class="btn btn-primary col-sm-8 btn-block btn-lg">Print Certificate</button>
    <button type="button" class="btn btn-primary col-sm-8 btn-block btn-lg">Print Ticket</button>
</fieldset>
