<div class="row">
    <div class="col-xs-10">
        <h1>Minakami Self Check In Machines Overview</h1>
    </div>
</div>
<div class="row">
    <div class="machine col-xs-10 bg-success">
        <h2 class="col-xs-10">MINA01</h2>

        <div class="cash-box col-xs-3">
            <h3>Cash Machine</h3>
            <h4>1000 Yen x 30</h4>
            <h4>5000 Yen x 30</h4>
            <h4>Status: Ready</h4>
        </div>
        <div class="ticket-printer col-xs-3">
            <h3>Ticket Printer</h3>
            <h4>~200 Tickets Left</h4>
        </div>
        <div class="certificate-printer col-xs-3">
            <h3>Certificate Printer</h3>
            <h4></h4>
        </div>
        <div class="scales col-xs-3">
            <h3>Scales Printer</h3>
            <h4>Weight Offset +2 Kgs</h4>
        </div>
        <div class="col-xs-10">
            <button type="button" class="btn btn-primary btn-lg btn-block view-machine" data-machine-name="MINA01">View Machine</button>
        </div>
    </div>
    <div class="machine col-xs-10 bg-danger">
        <h2 class="col-xs-10">MINA02</h2>

        <div class="cash-box col-xs-3">
            <h3>Cash Machine</h3>
            <h4>1000 Yen x 30</h4>
            <h4>5000 Yen x 30</h4>
            <h4>Status: Ready</h4>
        </div>
        <div class="ticket-printer col-xs-3">
            <h3>Ticket Printer</h3>
            <h4>~200 Tickets Left</h4>
        </div>
        <div class="certificate-printer col-xs-3">
            <h3>Certificate Printer</h3>
            <h4></h4>
        </div>
        <div class="scales col-xs-3">
            <h3>Scales Printer</h3>
            <h4>Weight Offset +2 Kgs</h4>
        </div>
        <div class="col-xs-10">
            <button type="button" class="btn btn-primary btn-lg btn-block view-machine" data-machine-name="MINA02">View Machine</button>
        </div>
    </div>
    <div class="machine col-xs-10 bg-warning">
        <h2 class="col-xs-10">MINA03</h2>

        <div class="cash-box col-xs-3">
            <h3>Cash Machine</h3>
            <h4>1000 Yen x 30</h4>
            <h4>5000 Yen x 30</h4>
            <h4>Status: Ready</h4>
        </div>
        <div class="ticket-printer col-xs-3">
            <h3>Ticket Printer</h3>
            <h4>~200 Tickets Left</h4>
        </div>
        <div class="certificate-printer col-xs-3">
            <h3>Certificate Printer</h3>
            <h4></h4>
        </div>
        <div class="scales col-xs-3">
            <h3>Scales Printer</h3>
            <h4>Weight Offset +2 Kgs</h4>
        </div>
        <div class="col-xs-10">
            <button type="button" class="btn btn-primary btn-lg btn-block view-machine" data-machine-name="MINA03">View Machine</button>
        </div>
    </div>
</div>
