<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">
    <!--Specify the icon and the mobile version-->
    <link rel="stylesheet" href="/css/bootstrap/3.3.6/css/bootstrap.css">
    <link rel="stylesheet" href="/css/bootstrap/3.3.6/css/bootstrap-theme.css">
    <link rel="stylesheet" href="css/main.css"/>

    <title>Self Check In Edit Booking</title>
</head>
<body>
<div class="container-fluid">
    <div class="row-fluid">
        <div id="tabs-buttons" class="col-xs-2 fixed"><!--Should collapse to hamburger menu on phones-->
            <ul class="">
                <li id="bookings-tab-button" class="tab-button"><a href="#bookings-tab">Bookings</a></li>
                <li id="machines-tab-button" class="tab-button"><a href="#machines-tab">Machines</a></li>
                <li id="operations-tab-button" class="tab-button"><a href="#operations-tab">Operations</a></li>
            </ul>
        </div>

        <!-- hide and show based on which has been pressed-->
        <div id="tabs" class="col-xs-10 col-xs-offset-2">
            <div id="machines-tab" class="tab-view">
                <!--<div id="machines" class="tab-view-section">tab-view-section</div>-->


                <div id="machines" class="tab-view-section">
                    <? require_once('page_components/machines.php'); ?>
                </div>
                <div id="machine-detail" class="tab-view-section">
                    <? require_once('page_components/machine.php'); ?>
                </div>
            </div>

            <div id="bookings-tab" class="tab-view">
                <div id="_bookings" class="tab-view-section">
                    <? //require_once('page_components/bookings.php'); ?>
                    <table id="bookings">
                    <!--Dynamically generate the table rows-->
                    </table>
                </div>
                <div id="booking-detail" class="tab-view-section">
                    <? require_once('page_components/booking.php'); ?>
                </div>
                <div id="waivers" class="tab-view-section">
                    <? require_once('page_components/waivers.php'); ?>
                </div>
                <div id="waivers-detail" class="tab-view-section">
                    <? require_once('page_components/waiver.php'); ?>
                </div>
            </div>

            <div id="operations-tab" class="tab-view">
                <div id="cord-range" class="tab tab-view-section"></div>
                <p>Show Cord Range</p>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="/js/jquery/2.1.4/jquery-2.1.4.js"></script>
<script type="text/javascript" src="/css/bootstrap/3.3.6/js/bootstrap.js"></script>
<script type="text/javascript" src="bower_components/underscore/underscore.js"></script>
<script type="text/javascript" src="bower_components/backbone/backbone.js"></script>
<script type="text/javascript" src="js/testData.js"></script>
<script type="text/template" id="booking-template" src="templates/booking.html">
    <tr>
        <td><%= firstname =></td>
        <td><%= lastname =></td>
        <td><%= email =></td>
        <td><%= bookingDate =></td>
        <td><%= agent =></td>
        <td><%= notes =></td>
    </tr>
</script>

<!--
<script type="text/template" id="bookings-template" src="templates/bookings.html"></script>
<script type="text/template" id="" src="templates/bookingSummary.tmpl"></script>
<script type="text/template" id="" src="templates/bookingsSummary.tmpl"></script>
<script type="text/template" id="" src="templates/waiver.tmpl"></script>
<script type="text/template" id="" src="templates/waivers.tmpl"></script>
<script type="text/template" id="" src="templates/machine.tmpl"></script>
<script type="text/template" id="" src="templates/machines.tmpl"></script>
-->

<script type="text/javascript" src="js/singlePageIPadApp.js"></script>
<script type="text/javascript" src="js/app.js"></script>
</body>
</html>
