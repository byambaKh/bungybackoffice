<?php
require_once('../includes/application_top.php');
?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="stylesheet.css">
</head>
<body>
<?php
$temp = explode(".", $_FILES["file"]["name"]);
$originalName = $_FILES["file"]["name"];
$extension = end($temp);

$uploadDirectory = $_SERVER['DOCUMENT_ROOT'].'/uploads/signage/';

if ($extension == "xls") {
	if ($_FILES["file"]["error"] > 0) {
		//echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
	} else {
        $storageName = $user->getID()."_".date('YmdHis').".xls";//A unique name for storing the file
		move_uploaded_file($_FILES["file"]["tmp_name"], $uploadDirectory . $storageName);
        $user = new BJUser();

        $data = array(
            'originalName' => $originalName,
            'storageName' => $storageName,
            'filename' => 'Standard-Move-Signage-09-2015.xls',
            'userId' => $user->getID(),
            'dateTime' => date('Y-m-d H:i:s'),
            'category' => 'Signage'
        );

		/*
        $mail = new PHPMailer;
        $mail->From = 'noreply@bungyjapan.com';
        $mail->FromName = 'Standard Move Signage';

        $recipients = [
            "beau@bungyjapan.com",
            "dave@bungyjapan.com",
            "dez@bungyjapan.com",
            "developer@standardmove.com",
        ];

        foreach ($recipients as $recipient) {
            $mail->addAddress($recipient); // Site Manager
        }

        $mail->addReplyTo('noreply@bungyjapan.com', 'Signage Update');
        $mail->isHTML(true); // Set email format to HTML
        $mail->CharSet = 'UTF-8';

        $mail->Subject = "Updated Signage";
        $mail->Body = "The Company charter has been updated. Please go to
        <a href='http://main.bungyjapan.com/Signage/index.php'>Signage Page</a> and Download The Latest Version.";

        $mail->send();
		*/

        db_perform('uploads', $data);

		echo "Signage Document Successfully Uploaded.<br> ";
		echo "<a href = 'index.php'>&lt;&lt; Back</a> ";
	}

} else {
	echo "Invalid file";
}
?> 
</body>
</html>

