<?php 
    require_once ('../includes/application_top.php');
  	  $date = date('Y-m-d');


      //$jcMina = jumpsCordColours($date, 1); //jumpCounts
      $jcSaru = jumpsCordColours($date, 2); //jumpCounts
      //$jcRyuj = jumpsCordColours($date, 3); //jumpCounts
      //$jcItsuki = jumpsCordColours($date, 4); //jumpCounts
      //$jcNara = jumpsCordColours($date, 10); //jumpCounts
      //$jcFuji = jumpsCordColours($date, 12); //jumpCounts

function cordBounds($date = NULL, $site_id = NULL)
{
    $query = "SELECT * FROM daily_reports WHERE site_id = $site_id AND date = '$date';";
    $results = queryForRows($query);

    if(count($results) == 1)
        $cordBounds = $results[0];
    else
        echo "No cord bounds for the day";

    //get the weight from the daily report
    $minWeight			 = 40;

    $bounds = array();

    $bounds['yellowMin'] = (int)$minWeight;
    $bounds['yellowMax'] = (int)$cordBounds['eq_cord_tw_yellow'];

    $bounds['redMin']	 = (int)$bounds['yellowMax'] + 1;
    $bounds['redMax']	 = (int)$cordBounds['eq_cord_tw_red'];

    $bounds['blueMin']   = (int)$bounds['redMax'] + 1;
    $bounds['blueMax']   = (int)$cordBounds['eq_cord_tw_blue'];

    $bounds['blackMin']  = (int)$bounds['blueMax'] + 1;
    $bounds['blackMax']  = (int)$cordBounds['eq_cord_tw_black'];

    return $bounds;
}

function jumpsCordColours($date, $site_id)
{
    //get the jumps for the site as rows
        //left join waivers and on customerregs1 where site_id and booking date ...
    //foreach row
    $query = "
        SELECT CustomerRegID, bid, ws.id waiver_id, NoOfJump, weight_kg, BookingDate, nj.waiver_id as non_jump_waiver_id, ws.`firstname`, ws.lastname FROM customerregs1 cr
        INNER JOIN waivers ws ON (ws.bid = cr.CustomerRegID)
        LEFT JOIN `non_jumpers` nj ON (nj.`waiver_id` = ws.id)
        WHERE cr.site_id = $site_id AND cr.BookingDate = '$date' AND cr.deleteStatus = 0 AND cr.NoOfJump > 0;
    ";

    $rows = queryForRows($query);
    $jumpsCords = array(
        'yellow' => 0,
        'red' => 0,
        'blue' => 0,
        'black' => 0,
        'underweight' => 0,
        'overweight' => 0,
        'total' => 0,
        'nonJump' => 0
    );

    foreach($rows as $row)
    {
        if($row['non_jump_waiver_id'])
        {
            $jumpsCords['nonJump']++;
            continue;
        }
        $colour =  getCordColorForWeightKg($row['weight_kg'], $date, $site_id);
        $jumpsCords[$colour]++;
        $jumpsCords['total']++;
    }

    return $jumpsCords;
}

function getCordColorForWeightKg($weight, $date = NULL, $site_id = NULL)
{
    if (!is_numeric($weight)) {
      echo 'Non Numeric Weight Encountered. Cannot supply a cord color.';
    }
//the boundaries are stored as integers so weight must be rounded up
    $weight = round($weight);

    $cordBounds = cordBounds($date, $site_id);

    $yellowMin = $cordBounds['yellowMin'];
    $yellowMax = $cordBounds['yellowMax'];

    $redMin = $cordBounds['redMin'];
    $redMax = $cordBounds['redMax'];

    $blueMin = $cordBounds['blueMin'];
    $blueMax = $cordBounds['blueMax'];

    $blackMin = $cordBounds['blackMin'];
    $blackMax = $cordBounds['blackMax'];

    if ($weight < $yellowMin) {
        $output = "underweight";

    } else if (($weight >= $yellowMin) && ($weight <= $yellowMax)) {
        $output = "yellow";

    } else if (($weight >= $redMin) && ($weight <= $redMax)) {
        $output = "red";

    } else if (($weight >= $blueMin) && ($weight <= $blueMax)) {
        $output = "blue";

    } else if (($weight >= $blackMin) && ($weight <= $blackMax)) {
        $output = "black";

    } else {
        $output = "overweight";
    }

    return "$output";
}

//All Sarugakyo cords

$redSaru = $jcSaru['red'];
$yellowSaru = $jcSaru['yellow'];
$blueSaru = $jcSaru['blue'];
$blackSaru = $jcSaru['black'];

//All Minakami cords

//$redMina = $jcMina['red'];
//$yellowMina = $jcMina['yellow'];
//$blueMina = $jcMina['blue'];
//$blackMina = $jcMina['black'];

//All Ryujin cords

//$redRyuj = $jcRyuj['red'];
//$yellowRyuj = $jcRyuj['yellow'];
//$blueRyuj = $jcRyuj['blue'];
//$blackRyuj = $jcRyuj['black'];

//All Fuji cords

//$redFuji = $jcFuji['red'];
//$yellowFuji = $jcFuji['yellow'];
//$blueFuji = $jcFuji['blue'];
//$blackFuji = $jcFuji['black'];

//All Kaiun cords

//$redNara = $jcNara['red'];
//$yellowNara = $jcNara['yellow'];
//$blueNara = $jcNara['blue'];
//$blackNara = $jcNara['black'];

//All Itsuki cords

//$redItsuki = $jcItsuki['red'];
//$yellowItsuki = $jcItsuki['yellow'];
//$blueItsuki = $jcItsuki['blue'];
//$blackItsuki = $jcItsuki['black'];

?>
<html>
<head>
    <!-- Meta Values -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="Dashboard for Self Checkin Machines">
    <meta name="author" content="Bungee Japan">

    <title>Cord - Report</title>

    <!-- CSS -->
    <link rel="stylesheet" href="css/customize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="css/animate.css">
    <link href="css/plugins/iCheck/custom.css" rel="stylesheet">

</head>
<body>
<div class="mishead">
    <div class="container">
        <h1 class="page-head text-center">Cord Overview</h1>
    </div>
</div>
<section id="site-status">
    <div class="container inner-sm">
        <div class="row">
            <div class="col-md-4 col-sm-6">
                <div class="site">
                    <div class="text-center name"><span>Minakami<span></div>
                    <div class="machines">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Cord</th>
                                <th>Number of Jumps</th>
<!--                                <th>Condition</th>-->
<!--                                <th>Up-time</th>-->
<!--                                <th>No. of restarts</th>-->
<!--                                <th>Balance</th>-->
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="text-center" >Red</td>
<!--                                <td class="text-center"><span class="label label-success" data-toggle="modal" data-target="#change">Ok</span></td>-->
<!--                                <td class="text-center"><i class="fa fa-lg fa-lightbulb-o faa-vertical faa-flash green" aria-hidden="true"></i></td>-->
                                <td class="text-center" >processing... </td>
<!--                                <td class="text-center" >2</td>-->
<!--                                <td class="text-center" >---</td>-->
                            </tr>
                            <tr>
                                <td class="text-center">Blue</td>
<!--                                <td class="text-center"><span class="label label-success" data-toggle="modal" data-target="#change">Ok</span></td>-->
<!--                                <td class="text-center"><i class="fa fa-lg fa-lightbulb-o faa-vertical faa-flash green" aria-hidden="true"></i></td>-->
                                <td class="text-center" >processing... </td>
<!--                                <td class="text-center" >2</td>-->
<!--                                <td class="text-center" >---</td>-->
                            </tr>
                            <tr>
                                <td class="text-center">Yellow</td>
<!--                                <td class="text-center"><span class="label label-success" data-toggle="modal" data-target="#change">Ok</span></td>-->
<!--                                <td class="text-center"><i class="fa fa-lg fa-lightbulb-o faa-vertical faa-flash green" aria-hidden="true"></i></td>-->
                                <td class="text-center" >processing... </td>
<!--                                <td class="text-center" >2</td>-->
<!--                                <td class="text-center" >---</td>-->
                            </tr>
                            <tr>
                                <td class="text-center">Black</td>
                                <td class="text-center" >processing... </td>
<!--                                <td class="text-center"><i class="fa fa-lightbulb-o faa-vertical blue" aria-hidden="true"></i></td>-->
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="site">
                    <div class="text-center name"><span>Sarugakyo<span></div>
                    <div class="machines">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Cord</th>
                                <th>Number of Jumps</th>
                                <!--                                <th>Condition</th>-->
                                <!--                                <th>Up-time</th>-->
                                <!--                                <th>No. of restarts</th>-->
                                <!--                                <th>Balance</th>-->
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="text-center" >Red</td>
<!--                                <td class="text-center"><span class="label label-success" data-toggle="modal" data-target="#change">Ok</span></td>-->
<!--                                <td class="text-center"><i class="fa fa-lg fa-lightbulb-o faa-vertical faa-flash green" aria-hidden="true"></i></td>-->
                                <td class="text-center" ><?php echo $redSaru; ?></td>
<!--                                <td class="text-center" >2</td>-->
<!--                                <td class="text-center" >---</td>-->
                            </tr>
                            <tr>
                                <td class="text-center">Blue</td>
<!--                                <td class="text-center"><span class="label label-success" data-toggle="modal" data-target="#change">Ok</span></td>-->
<!--                                <td class="text-center"><i class="fa fa-lg fa-lightbulb-o faa-vertical faa-flash green" aria-hidden="true"></i></td>-->
                                <td class="text-center" ><?php echo $blueSaru; ?></td>
<!--                                <td class="text-center" >2</td>-->
<!--                                <td class="text-center" >---</td>-->
                            </tr>
                            <tr>
                                <td class="text-center">Yellow</td>
<!--                                <td class="text-center"><span class="label label-success" data-toggle="modal" data-target="#change">Ok</span></td>-->
<!--                                <td class="text-center"><i class="fa fa-lg fa-lightbulb-o faa-vertical faa-flash green" aria-hidden="true"></i></td>-->
                                <td class="text-center" ><?php echo $yellowSaru; ?></td>
<!--                                <td class="text-center" >2</td>-->
<!--                                <td class="text-center" >---</td>-->
                            </tr>
                            <tr>
                                <td class="text-center">Black</td>
                                <td class="text-center" ><?php echo $blackSaru; ?></td>
<!--                                <td class="text-center"><i class="fa fa-lightbulb-o faa-vertical blue" aria-hidden="true"></i></td>-->
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="site">
                    <div class="text-center name"><span>Ryujin<span></div>
                    <div class="machines">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Cord</th>
                                <th>Number of Jumps</th>
                                <!--                                <th>Condition</th>-->
                                <!--                                <th>Up-time</th>-->
                                <!--                                <th>No. of restarts</th>-->
                                <!--                                <th>Balance</th>-->
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="text-center" >Red</td>
<!--                                <td class="text-center"><span class="label label-success" data-toggle="modal" data-target="#change">Ok</span></td>-->
<!--                                <td class="text-center"><i class="fa fa-lg fa-lightbulb-o faa-vertical faa-flash green" aria-hidden="true"></i></td>-->
                                <td class="text-center" >processing...</td>
<!--                                <td class="text-center" >2</td>-->
<!--                                <td class="text-center" >---</td>-->
                            </tr>
                            <tr>
                                <td class="text-center">Blue</td>
<!--                                <td class="text-center"><span class="label label-danger tool-ch" data-tooltip="tooltip" title="Motherboard gone slow, need replacement" data-toggle="modal" data-target="#change">Error</span></td>-->
<!--                                <td class="text-center"><i class="fa fa-lightbulb-o faa-vertical red" aria-hidden="true"></i></td>-->
                                <td class="text-center" >processing...</td>
<!--                                <td class="text-center" >2</td>-->
<!--                                <td class="text-center" >---</td>-->
                            </tr>
                            <tr>
                                <td class="text-center">Yellow</td>
<!--                                <td class="text-center"><span class="label label-danger tool-ch" data-tooltip="tooltip" title="Ticket printer dead, needs replacement" data-toggle="modal" data-target="#change">Error</span></td>-->
<!--                                <td class="text-center"><i class="fa fa-lightbulb-o faa-vertical red" aria-hidden="true"></i></td>-->
                                <td class="text-center" >processing...</td>
<!--                                <td class="text-center" >2</td>-->
<!--                                <td class="text-center" >---</td>-->
                            </tr>
                            <tr>
                                <td class="text-center">Black</td>
                                <td class="text-center" >processing...</td>
<!--                                <td class="text-center"><i class="fa fa-lightbulb-o faa-vertical blue" aria-hidden="true"></i></td>-->
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="site">
                    <div class="text-center name"><span>Fuji<span></div>
                    <div class="machines">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Cord</th>
                                <th>Number of Jumps</th>
                                <!--                                <th>Condition</th>-->
                                <!--                                <th>Up-time</th>-->
                                <!--                                <th>No. of restarts</th>-->
                                <!--                                <th>Balance</th>-->
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="text-center" >Red</td>
<!--                                <td class="text-center"><span class="label label-success" data-toggle="modal" data-target="#change">Ok</span></td>-->
<!--                                <td class="text-center"><i class="fa fa-lg fa-lightbulb-o faa-vertical faa-flash green" aria-hidden="true"></i></td>-->
                                <td class="text-center" >processing...</td>
<!--                                <td class="text-center" >2</td>-->
<!--                                <td class="text-center" >---</td>-->
                            </tr>
                            <tr>
                                <td class="text-center">Blue</td>
<!--                                <td class="text-center"><span class="label label-success" data-toggle="modal" data-target="#change">Ok</span></td>-->
<!--                                <td class="text-center"><i class="fa fa-lg fa-lightbulb-o faa-vertical faa-flash green" aria-hidden="true"></i></td>-->
                                <td class="text-center" >processing...</td>
<!--                                <td class="text-center" >2</td>-->
<!--                                <td class="text-center" >---</td>-->
                            </tr>
                            <tr>
                                <td class="text-center">Yellow</td>
<!--                                <td class="text-center"><span class="label label-danger tool-ch" data-tooltip="tooltip" title="Dead power source, needs replacement" data-toggle="modal" data-target="#change">Error</span></td>-->
<!--                                <td class="text-center"><i class="fa fa-lightbulb-o faa-vertical red" aria-hidden="true"></i></td>-->
                                <td class="text-center" >processing...</td>
<!--                                <td class="text-center" >2</td>-->
<!--                                <td class="text-center" >---</td>-->
                            </tr>
                            <tr>
                                <td class="text-center">Black</td>
                                <td class="text-center" >processing...</td>
<!--                                <td class="text-center"><i class="fa fa-lightbulb-o faa-vertical blue" aria-hidden="true"></i></td>-->
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="site">
                    <div class="text-center name"><span>Itsuki<span></div>
                    <div class="machines">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Cord</th>
                                <th>Number of Jumps</th>
                                <!--                                <th>Condition</th>-->
                                <!--                                <th>Up-time</th>-->
                                <!--                                <th>No. of restarts</th>-->
                                <!--                                <th>Balance</th>-->
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="text-center" >Red</td>
<!--                                <td class="text-center"><span class="label label-danger tool-ch" data-tooltip="tooltip" title="Printer and scale not working" data-toggle="modal" data-target="#change">Error</span></td>-->
<!--                                <td class="text-center"><i class="fa fa-lightbulb-o faa-vertical red" aria-hidden="true"></i></td>-->
                                <td class="text-center" >processing...</td>
<!--                                <td class="text-center" >2</td>-->
<!--                                <td class="text-center" >---</td>-->
                            </tr>
                            <tr>
                                <td class="text-center">Blue</td>
<!--                                <td class="text-center"><span class="label label-danger tool-ch" data-tooltip="tooltip" title="Printer not working" data-toggle="modal" data-target="#change">Error</span></td>-->
<!--                                <td class="text-center"><i class="fa fa-lightbulb-o faa-vertical red" aria-hidden="true"></i></td>-->
                                <td class="text-center" >processing...</td>
<!--                                <td class="text-center" >2</td>-->
<!--                                <td class="text-center" >---</td>-->
                            </tr>
                            <tr>
                                <td class="text-center">Yellow</td>
<!--                                <td class="text-center"><span class="label label-danger tool-ch" data-tooltip="tooltip" title="Printer and scale not working" data-toggle="modal" data-target="#change">Error</span></td>-->
<!--                                <td class="text-center"><i class="fa fa-lightbulb-o faa-vertical red" aria-hidden="true"></i></td>-->
                                <td class="text-center" >processing...</td>
<!--                                <td class="text-center" >2</td>-->
<!--                                <td class="text-center" >---</td>-->
                            </tr>
                            <tr>
                                <td class="text-center">Black</td>
                                <td class="text-center" >processing...</td>
<!--                                <td class="text-center"><i class="fa fa-lightbulb-o faa-vertical blue" aria-hidden="true"></i></td>-->
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-md-4 col-sm-6">
                <div class="site">
                    <div class="text-center name"><span>Kaiun<span></div>
                    <div class="machines">
                        <table class="table table-hover">
                            <thead>
                            <tr>
                                <th>Cord</th>
                                <th>Number of Jumps</th>
                                <!--                                <th>Condition</th>-->
                                <!--                                <th>Up-time</th>-->
                                <!--                                <th>No. of restarts</th>-->
                                <!--                                <th>Balance</th>-->
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <td class="text-center" >Red</td>
<!--                                <td class="text-center"><span class="label label-danger tool-ch" data-tooltip="tooltip" title="Printer and scale not working" data-toggle="modal" data-target="#change">Error</span></td>-->
<!--                                <td class="text-center"><i class="fa fa-lightbulb-o faa-vertical red" aria-hidden="true"></i></td>-->
                                <td class="text-center" >processing...</td>
<!--                                <td class="text-center" >2</td>-->
<!--                                <td class="text-center" >---</td>-->
                            </tr>
                            <tr>
                                <td class="text-center">Blue</td>
<!--                                <td class="text-center"><span class="label label-danger tool-ch" data-tooltip="tooltip" title="Printer not working" data-toggle="modal" data-target="#change">Error</span></td>-->
<!--                                <td class="text-center"><i class="fa fa-lightbulb-o faa-vertical red" aria-hidden="true"></i></td>-->
                                <td class="text-center" >processing...</td>
<!--                                <td class="text-center" >2</td>-->
<!--                                <td class="text-center" >---</td>-->
                            </tr>
                            <tr>
                                <td class="text-center">Yellow</td>
<!--                                <td class="text-center"><span class="label label-danger tool-ch" data-tooltip="tooltip" title="Printer and scale not working" data-toggle="modal" data-target="#change">Error</span></td>-->
<!--                                <td class="text-center"><i class="fa fa-lightbulb-o faa-vertical red" aria-hidden="true"></i></td>-->
                                <td class="text-center" >processing...</td>
<!--                                <td class="text-center" >2</td>-->
<!--                                <td class="text-center" >---</td>-->
                            </tr>
                            <tr>
                                <td class="text-center">Black</td>
                                <td class="text-center" >processing...</td>
<!--                                <td class="text-center"><i class="fa fa-lightbulb-o faa-vertical blue" aria-hidden="true"></i></td>-->
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>


<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui-1.10.4.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>

<script src="js/plugins/iCheck/icheck.min.js"></script>
</body>
</html>