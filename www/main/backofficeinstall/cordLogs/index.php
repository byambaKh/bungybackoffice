<?php
require_once('../includes/application_top.php');

function getAllActiveSites()
{
    $places = queryForRows("SELECT * FROM sites WHERE status = 1 AND id <> 6 ORDER BY id");//id 6 = test site so ignore

    $allPlaces = [];
    foreach ($places as $key => $place) {
        $allPlaces[$place['id']] = $place;
    }

    return $places;
}

function cordBounds($date = NULL, $site_id = NULL)
{
    //$query = "SELECT * FROM daily_reports WHERE site_id = $site_id AND date = '$date';"; //Get todays report.
    $query = "SELECT * FROM daily_reports WHERE site_id = $site_id ORDER BY date DESC LIMIT 1;"; //Get the latest report.

    $results = queryForRows($query);

    if (count($results) == 1)
        $cordBounds = $results[0];
    else
        echo "No cord bounds for the day";

    //get the weight from the daily report
    $minWeight = 40;

    $bounds = array();

    $bounds['yellowMin'] = (int)$minWeight;
    $bounds['yellowMax'] = (int)$cordBounds['eq_cord_tw_yellow'];

    $bounds['redMin'] = (int)$bounds['yellowMax'] + 1;
    $bounds['redMax'] = (int)$cordBounds['eq_cord_tw_red'];

    $bounds['blueMin'] = (int)$bounds['redMax'] + 1;
    $bounds['blueMax'] = (int)$cordBounds['eq_cord_tw_blue'];

    $bounds['blackMin'] = (int)$bounds['blueMax'] + 1;
    $bounds['blackMax'] = (int)$cordBounds['eq_cord_tw_black'];

    return $bounds;
}

function jumpsCordColours($date, $site_id)
{
    //get the jumps for the site as rows
    //left join waivers and on customerregs1 where site_id and booking date ...
    //foreach row
    $query = "
        SELECT CustomerRegID, bid, ws.id waiver_id, NoOfJump, weight_kg, BookingDate, nj.waiver_id as non_jump_waiver_id, ws.`firstname`, ws.lastname FROM customerregs1 cr
        INNER JOIN waivers ws ON (ws.bid = cr.CustomerRegID)
        LEFT JOIN `non_jumpers` nj ON (nj.`waiver_id` = ws.id)
        WHERE cr.site_id = $site_id AND cr.BookingDate = '$date' AND cr.deleteStatus = 0 AND cr.NoOfJump > 0;
    ";

    $rows = queryForRows($query);
    $jumpsCords = array(
        'yellow'      => 0,
        'red'         => 0,
        'blue'        => 0,
        'black'       => 0,
        'underweight' => 0,
        'overweight'  => 0,
        'total'       => 0,
        'nonJump'     => 0
    );

    foreach ($rows as $row) {
        if ($row['non_jump_waiver_id']) {
            $jumpsCords['nonJump']++;
            continue;
        }
        $colour = getCordColorForWeightKg($row['weight_kg'], $date, $site_id);
        $jumpsCords[$colour]++;
        $jumpsCords['total']++;
    }

    return $jumpsCords;
}

function getCordColorForWeightKg($weight, $date = NULL, $site_id = NULL)
{
    if (!is_numeric($weight)) {
        echo 'Non Numeric Weight Encountered. Cannot supply a cord color.';
    }
//the boundaries are stored as integers so weight must be rounded up
    $weight = round($weight);

    $cordBounds = cordBounds($date, $site_id);

    $yellowMin = $cordBounds['yellowMin'];
    $yellowMax = $cordBounds['yellowMax'];

    $redMin = $cordBounds['redMin'];
    $redMax = $cordBounds['redMax'];

    $blueMin = $cordBounds['blueMin'];
    $blueMax = $cordBounds['blueMax'];

    $blackMin = $cordBounds['blackMin'];
    $blackMax = $cordBounds['blackMax'];

    if ($weight < $yellowMin) {
        $output = "underweight";

    } else if (($weight >= $yellowMin) && ($weight <= $yellowMax)) {
        $output = "yellow";

    } else if (($weight >= $redMin) && ($weight <= $redMax)) {
        $output = "red";

    } else if (($weight >= $blueMin) && ($weight <= $blueMax)) {
        $output = "blue";

    } else if (($weight >= $blackMin) && ($weight <= $blackMax)) {
        $output = "black";

    } else {
        $output = "overweight";
    }

    return "$output";
}

function drawSiteCordInfoBox($siteInfo)
{
    echo '';
    ?>
    <div class="col-md-4 col-sm-6">
        <div class="site">
            <div class="text-center name"><span><?=$siteInfo['name']?><span></div>
            <div class="machines">
                <table class="table table-hover">
                    <thead>
                    <tr>
                        <th>Cord</th>
                        <th>Number of Jumps</th>
                        <!--                                <th>Condition</th>-->
                        <!--                                <th>Up-time</th>-->
                        <!--                                <th>No. of restarts</th>-->
                        <!--                                <th>Balance</th>-->
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td class="text-center">Red</td>
                        <!--                                <td class="text-center"><span class="label label-success" data-toggle="modal" data-target="#change">Ok</span></td>-->
                        <!--                                <td class="text-center"><i class="fa fa-lg fa-lightbulb-o faa-vertical faa-flash green" aria-hidden="true"></i></td>-->
                        <td class="text-center"><?= $siteInfo['cords']['report']['red']['usage']['today']; ?></td>
                        <!--                                <td class="text-center" >2</td>-->
                        <!--                                <td class="text-center" >---</td>-->
                    </tr>
                    <tr>
                        <td class="text-center">Blue</td>
                        <!--                                <td class="text-center"><span class="label label-success" data-toggle="modal" data-target="#change">Ok</span></td>-->
                        <!--                                <td class="text-center"><i class="fa fa-lg fa-lightbulb-o faa-vertical faa-flash green" aria-hidden="true"></i></td>-->
                        <td class="text-center"><?= $siteInfo['cords']['report']['blue']['usage']['today']; ?></td>
                        <!--                                <td class="text-center" >2</td>-->
                        <!--                                <td class="text-center" >---</td>-->
                    </tr>
                    <tr>
                        <td class="text-center">Yellow</td>
                        <!--                                <td class="text-center"><span class="label label-success" data-toggle="modal" data-target="#change">Ok</span></td>-->
                        <!--                                <td class="text-center"><i class="fa fa-lg fa-lightbulb-o faa-vertical faa-flash green" aria-hidden="true"></i></td>-->
                        <td class="text-center"><?= $siteInfo['cords']['report']['yellow']['usage']['today']; ?></td>
                        <!--                                <td class="text-center" >2</td>-->
                        <!--                                <td class="text-center" >---</td>-->
                    </tr>
                    <tr>
                        <td class="text-center">Black</td>
                        <td class="text-center"><?= $siteInfo['cords']['report']['black']['usage']['today']; ?></td>
                        <!--                                <td class="text-center"><i class="fa fa-lightbulb-o faa-vertical blue" aria-hidden="true"></i></td>-->
                    </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
    <?
}

function getLatestCordUsageDataForSiteId($siteId)
{
    $query = "
SELECT
    `date`,
	eq_cord_jn_yellow AS todayYellow,
	eq_cord_jn_blue AS todayBlue,
	eq_cord_jn_red AS todayRed,
	eq_cord_jn_black AS todayBlack,
	eq_cord_ct_yellow AS totalYellow,
	eq_cord_ct_blue AS totalBlue,
	eq_cord_ct_red AS totalRed,
	eq_cord_ct_black AS totalBlack,
	eq_cord_cc_yellow_num AS cordCodeYellow,
	eq_cord_cc_blue_num AS cordCodeBlue,
	eq_cord_cc_red_num AS cordCodeRed,
	eq_cord_cc_black_num AS cordCodeBlack	
FROM 
	daily_reports
	
WHERE site_id = $siteId
	ORDER BY DATE DESC 
	LIMIT 1;
    ";

    $r = queryForRows($query)[0];
    if(!is_array($r)) exit('Error No Daily Report in Operations/daily_report.php has Been Set for this Site.');
    $data = [
        'date' => $r['date'],
        'report' => [
            'yellow' => ['usage' => ['today' => $r['todayYellow'], 'total' => $r['totalYellow']], 'code' => "SL".$r['cordCodeYellow']],
            'blue'   => ['usage' => ['today' => $r['todayBlue'], 'total' => $r['totalBlue']], 'code' => "L".$r['cordCodeBlue']],
            'red'    => ['usage' => ['today' => $r['todayRed'], 'total' => $r['totalRed']], 'code' => "H".$r['cordCodeRed']],
            'black'  => ['usage' => ['today' => $r['todayBlack'], 'total' => $r['totalBlack']], 'code' => "SH".$r['cordCodeBlack']],
        ]
    ];

    return $data;
}
$date = date('Y-m-d');

$activeSites = getAllActiveSites();

$siteData = [];

foreach($activeSites as $index => $site) {
   $siteData[$index]['name'] = $site['display_name'];
   $siteData[$index]['id'] = $site['id'];
   $siteData[$index]['cords'] = getLatestCordUsageDataForSiteId($site['id']);
}

?>
<html>
<head>
    <!-- Meta Values -->
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
    <meta name="description" content="Dashboard for Self Checkin Machines">
    <meta name="author" content="Bungee Japan">

    <title>Cord - Report</title>

    <!-- CSS -->
    <link rel="stylesheet" href="css/customize.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <link rel="stylesheet" href="font-awesome/css/font-awesome.css">
    <link rel="stylesheet" href="css/animate.css">
    <link rel="stylesheet" href="css/plugins/iCheck/custom.css">

</head>
<body>
<div class="mishead">
    <div class="container">
        <h1 class="page-head text-center">Cord Overview</h1>
    </div>
</div>
<section id="site-status">
    <div class="container inner-sm">
        <div class="row">
            <?
            foreach($siteData as $singleSiteData) {
                drawSiteCordInfoBox($singleSiteData);
            }

            ?>
        </div>
    </div>
</section>


<script src="js/jquery.min.js"></script>
<script src="js/jquery-ui-1.10.4.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/custom.js"></script>

<script src="js/plugins/iCheck/icheck.min.js"></script>
</body>
</html>