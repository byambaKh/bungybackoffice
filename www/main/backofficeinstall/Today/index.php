<?
require_once('../includes/application_top.php');

$day   = (int)date("d");
$month = date("m");
$year  = date("Y");

$sql = "
            SELECT u.`UserName`, rp.roster_day, rs.site_id, rp.place, rpa.name
                FROM roster_staff rs, roster_position rp, users u, roster_position_all rpa
                WHERE
                    rs.roster_month = '$year$month'
                    AND rs.place = rp.place
                    AND rp.site_id = rs.site_id
                    AND rp.roster_month = '$year$month'
                    AND u.UserID = rs.staff_id
                    AND roster_day = $day
                    AND rp.site_id = 0
                    AND rpa.id = rp.position_id
                    AND rpa.name IN ('OFC' ,'TRN', 'TYO', 'MK', 'SG', 'IB', 'KY', 'NR', 'JPN', 'INTL', 'P&V1', 'P&V2', 'BM', 'CRD', 'CC') #Dont show jumpstaff
                ORDER BY UserName ASC;
        ";

$people = queryForRows($sql);

function drawPeopleList($people)
{
    $return = "<ul class='people-list'>";
    foreach($people as $person) {
        $return.="<li>{$person["UserName"]}</li>";
    }
    $return .= "</ul>";
    return $return;
}

$title = "Who's Working Today?";
$content = drawPeopleList($people);

require("templates/peopleList.php");

