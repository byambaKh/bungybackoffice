<?php
	include "../includes/application_top.php";
	mysql_query('SET NAMES utf8;');

	if (array_key_exists('action', $_POST) && $_POST['action'] == 'add') {
		$data = array(
			'issue_date'	=> $_POST['issue_date'],
			'site_id'		=> (CURRENT_SITE_ID == 0) ? $_POST['site_id'] : CURRENT_SITE_ID,
			'viewed_by'		=> $_POST['viewed_by'],
			'name'			=> $_POST['name'],
			'description'	=> $_POST['description']
		);
		db_perform('issues', $data);
		$_SESSION['issue_message'] = "Issue successfully saved.";
		$to_list = array();
		if (!empty($config['site_manager_email']) && $_POST['viewed_by']) {
			$to_list[] = $config['site_manager_email'];
		};
		if (!empty($config['general_manager_email'])) {
			$to_list[] = $config['general_manager_email'];
		};
		if (!$_POST['viewed_by']) {
			$to_list[] = 'beau@bungyjapan.com';
		};
		if (!empty($to_list)) {
			$to_list = array_unique($to_list);
			$places = BJHelper::getPlaces();
			$place_name = 'Unknown';
			foreach ($places as $place) {
				if ($place['id'] == $data['site_id']) {
					$place_name = $place['name'];
				};
			};

			mail(
				implode(", ", $to_list),
				"New issue submitted",
				"Site: " . $place_name . "\r\n" .
				"Name: " . ($_POST['name'] ? $_POST['name'] : 'Anonymously') . "\r\n" .
				"Viewed By: " . ($_POST['viewd_by'] ? 'All Staff' : 'Management Only') . "\r\n" .
				"Date: " . $_POST['issue_date'] . "\r\n" .
				"Description: \r\n" . $_POST['description'] . "\r\n",
                "Return-Path: backup@bungyjapan.com\r\n",
                "-f backup@bungyjapan.com"
			);
		};
		Header("Location: submit_issue.php");
		die();
	};
    // Load staff Names list
    //$staff_names = BJHelper::getStaffList('StaffListName', false);
	$staff_names = array();
	$name = ucfirst($_SESSION['myusername']);
    $staff_names[] = array(
        'id'    => $name,
        'text' => $name
    );
    $staff_names[] = array(
        'id'    => '',
        'text' => 'Anonymously'
    );
	$my_staff_name = $name;
	$viewed_by_list = array(
		array(
			'id'	=> 1,
			'text'	=> 'All Staff'
		),
		array(
			'id'	=> 0,
			'text'	=> 'Management Only'
		),
	);
	$viewed_by = 1;
	

?>
<!DOCTYPE html>
<html>
  <head>
  <meta charset="utf-8">
<?php include '../includes/header_tags.php'; ?>
	<title><?php echo SYSTEM_SUBDOMAIN; ?> :: Issues :: Submit Issue</title>
<?php include '../includes/head_scripts.php'; ?>
<script>
$(document).ready(function () {
    $(".date-selection").datepicker({
        showAnim: 'fade',
        numberOfMonths: 1,
        dateFormat: 'yy-mm-dd',
        currentText: 'Today'
    });

    $('#name').change(function(){
        //Hide the other option
        if($(this).val() == ''){
            //Set the Viewed by option to management only
            $('#issue-viewed').val(0);//An anonymous submission
            $('#issue-viewed').find('[value=1]').prop('disabled', 'disabled');

        } else {
            $('#issue-viewed').find('[value=1]').prop('disabled', false);

        };

    });

    $('#issue-viewed').change(function(){
        console.log($(this).val());
        console.log('viewed changed')

    });
});

</script>
<style>
h1 {
    color: white;
}
body div {
text-align: center;
}
label {
	width: 500px;
	color: white;
	display: inline-block;
	text-align: left;
}
textarea {
	width: 500px;
}
#add-issue {
	width: 500px;
	margin-right: auto;
	margin-left: auto;
	text-align: left;
	color: white;
}
#add-issue-buttons-area {
	clear: both;
}
#add-issue-button {
	float: right;
}
#back {
	float: left;
}
.date-selection {
	width: 80px;
	text-align: center;
}
#system-message {
    background-color: #55FF55;
    text-align: center;
    line-height: 150%;
}
</style>
</head>
<body bgcolor=black>
<?php include 'includes/main_menu.php'; ?>
<div>
<img src="/img/index.jpg">
<h1><?php echo SYSTEM_SUBDOMAIN; ?> New Issue</h1>
<?php
 if (array_key_exists('issue_message', $_SESSION) && !empty($_SESSION['issue_message'])) {
    echo "<div id='system-message'>";
    echo $_SESSION['issue_message'];
    echo "</div>";
    unset($_SESSION['issue_message']);
?>
<script>
	$(document).ready(function () {
		$("#system-message").delay(2000).slideUp(600);
	});
</script>
<?php
 };
	$p = BJHelper::getPlaces();
	$places = array();
	$current_site_name = '';
	foreach ($p as $place) {
		$places[] = array(
			'id'	=> $place['id'],
			'text'	=> $place['name']
		);
		if (CURRENT_SITE_ID != 0 && $place['id'] == CURRENT_SITE_ID) {
			$current_site_name = $place['name'];
		};
	}; 
?>
<div id="add-issue">
<form method="post">
<label for="site">Site</label><br />
<?php 
	if (CURRENT_SITE_ID == 0) {
		echo draw_pull_down_menu("site_id", $places, CURRENT_SITE_ID, 'id="site"');
	} else {
		echo $current_site_name;
	};
?>
<br />
<label for="issue-title">Your name</label><br />
<?php echo draw_pull_down_menu("name", $staff_names, $my_staff_name, 'id="name"');?>
<br />
<label for="issue-viewed">Viewed By</label><br />
<?php echo draw_pull_down_menu("viewed_by", $viewed_by_list, $viewed_by, 'id="issue-viewed"');?>
<br />
<label for="issue-date">Issue date</label><br />
<input name="issue_date" class="date-selection" type="text" value="<?php echo date("Y-m-d"); ?>">
<br />
<label for="issue-description">Issue description</label><br />
<textarea rows="10" cols="30" name="description"></textarea>
<div id="add-issue-buttons-area">
    <button id="add-issue-button" name="action" value="add" type="submit"><span>Add issue</span></button>
    <button id="back" name="action" value="cancel" onClick="document.location='/Issues/';" type="button">Back</button>
</div>
</form>
</div>

<br><br>
</div>
</body></html>
