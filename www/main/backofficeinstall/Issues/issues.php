<?php
	include "../includes/application_top.php";
	mysql_query('SET NAMES utf8;');

	$read_only = false;
    if (!$user->hasRole(array('SysAdmin', 'General Manager', 'Site Manager'))) {
		//Header("Location: /Issues/");
		//die();
		$read_only = true;
    };
	$delete_possible = false;
    if ($user->hasRole(array('SysAdmin', 'General Manager'))) {
		$delete_possible = true;
	};

	//$end = date("Y-m-d");
	//$start = date("Y-m-d", time() - 7 * 24 * 60 * 60);
	$action = '';
	if (isset($_POST['action'])) {
		$action = $_POST['action'];
	};
	if (!$read_only) {
		switch ($action) {
			case 'show':
				//$start = $_POST['start'];
				//$end = $_POST['end'];
				break;
			case 'delete':
				if (!$delete_possible) break;
				$issue_id = $_POST['id'];
				mysql_query("DELETE FROM issues WHERE id = '$issue_id';") or die(mysql_error());
				mysql_query("DELETE FROM issues_response WHERE issue_id = '$issue_id';") or die(mysql_error());
				Header('Location: issues.php');
			case 'response':
				//$start = $_POST['start'];
				//$end = $_POST['end'];
				$id = $_POST['id'];
				$rid = $_POST['rid'];
				$data = array(
					'issue_id'		=> $id,
					'response_date'	=> $_POST['response_date'],
					'name'			=> $_POST['name'],
					'description'	=> $_POST['description'],
				);
				db_perform('issue_response', $data);
				Header('Location: issues.php');
				break;
			case 'csv':
				//$start = $_POST['start'];
				//$end = $_POST['end'];
				$sql = "SELECT issue_date as 'DATE', name as 'Name', description as 'Description' FROM issues 
	" /*
					WHERE 
						issue_date >= '$start'
					and issue_date <= '$end'
	*/  ."
					ORDER BY issue_date ASC;";
				$res = mysql_query($sql) or die(mysql_error());
				$d = str_replace('-', '', $start) . '-' . str_replace('-', '', $end);
				$d = '';
				Header('Content-Type: text/csv; name="issues'.$d.'.csv"');
				Header('Content-Disposition: attachment; filename="issues'.$d.'.csv"');
				$headers = true;
				define('CSV_SEP', ',');
				while ($row = mysql_fetch_assoc($res)) {
					if ($headers) {
						echo implode(CSV_SEP, array_map('prepare_csv', array_keys($row))) . "\r\n";
						$headers = false;
					};
					echo implode(CSV_SEP, array_map('prepare_csv', $row)) . "\r\n";
				};
				die();
				break;
		};
	};
	function prepare_csv($item) {
		return '"' . str_replace('"', '""', $item) . '"';
	};
    // Load staff Names list
	$staff_names = BJHelper::getStaffList('StaffListName', false);
	array_unshift($staff_names, array(
        'id'    => '',
        'text' => 'Anonymously'
    ));
	$places = BJHelper::getPlaces();
	$places_by_id = array();
	foreach ($places as $p) {
		$places_by_id[$p['id']] = $p;
	};
	$response_names = BJHelper::getStaffListByRole(array('General Manager', 'Site Manager', 'SysAdmin'));
?>
<!DOCTYPE html>
<html>
  <head>
	<meta charset="utf-8">
<?php include '../includes/header_tags.php'; ?>
    <title>Bungy Japan Issues :: Issues List</title>
<?php include '../includes/head_scripts.php'; ?>
<script>
$(document).ready(function () {
    $(".date-selection").datepicker({
        showAnim: 'fade',
        numberOfMonths: 1,
        dateFormat: 'yy-mm-dd',
        currentText: 'Today'
    });
});

</script>
<style>
body {
	background-color: black;
	color: white;
}
#container {
	width: 700px;
	margin-left: auto;
	margin-right: auto;
	text-align: center;
}
#container h1 {
	width: 100%;
	text-align: center;
}
#date-selection-form {
	padding: 10px;
	text-align: center;
}
.date-selection {
	width: 70px;
}
#issues-list table {
	width: 700px;
	border-collapse: collapse;
	text-align: left;
}
#issues-list table td {
	border: 1px solid silver;
	padding: 3px;
	background-color: gray;
	vertical-align: top;
}
#issues-list table th {
	border: 1px solid silver;
	padding: 3px;
	background-color: gray;
	text-align: center;
}
.issue-date, .issue-name, .issue-site {
	width: 100px;
	text-align: center;
}
.issues-desc {
	text-align: left !important;
}
.response td {
	background-color: green !important;
}
.issue-description textarea {
	width: 99%;
	height: 70px;
}
.issue-date input {
	width: 99%;
}
#back {
	float: left;
}
.clear {
	clear: both;
}
.issue-resp {
	width: 170px !important;
	text-align: center;
}
</style>
<script>
 var bj_staff = <?php echo json_encode($staff_names); ?>;
 var rbj_staff = <?php echo json_encode($response_names); ?>;
 $(document).ready(function () {
 	$('.add-response').on('click', function () {
		if ($('#new-response').length) {
			$('#new-response').remove();
			//return;
		};
		var iid = $(this).attr('custom-issue-id');
		var tr = $('<tr>').addClass('response').attr('id', 'new-response');
		// append date
		var d = new Date();
		var udate = $('<input name="response_date">')
			.prop('value', '<?php echo date("Y-m-d"); ?>')
			.addClass('date-selection')
			.datepicker({
        		showAnim: 'fade',
        		numberOfMonths: 1,
        		dateFormat: 'yy-mm-dd',
        		currentText: 'Today'
    		});
		tr.append($('<td>').html('&nbsp;'));
		tr.append($('<td>')
			.addClass('issue-date')
			.append(udate)
			.append($('<input type="hidden" name="id" value="'+iid+'">'))
		);
		// append name select
		var uname = $('<select name="name">');
		for (var i in rbj_staff) {
			if (!rbj_staff.hasOwnProperty(i)) continue;
			var opt = $('<option>').prop('value', rbj_staff[i].id).text(rbj_staff[i].text);
			uname.append(opt);
		};
		tr.append($('<td>').addClass('issue-name').append(uname));
		// append text
		var utext = $('<textarea name="description" />');
		tr.append($('<td>').addClass('issue-description').append(utext));
		// append submit button
		var ubutton = $('<button>')
			.text('Save')
			.prop('name', 'action')
			.prop('value', 'response').on('click', function () {
				var post_data = {};
				post_data.response_date = $('#new-response input[name=response_date]').val();
				post_data.id = $('#new-response input[name=id]').val();
				post_data.name = $('#new-response select[name=name]').val();
				post_data.description = $('#new-response textarea[name=description]').val();
				post_data.action = 'response';
				var post_url = document.location.href;
				$.ajax({
        			'url': post_url,
        			'type': 'post',
        			'data': post_data,
        			'success': function(data) {
						document.location.reload();
                 	}
    			});
		});
		tr.append($('<td>').addClass('issue-description').append(ubutton));
		tr.insertAfter($('#issue' + iid));
 	});
	$(".delete-issue").on('click', function () {
		if (window.confirm("Are you sure to delete this issue and all responses?")) {
			var post_data = {};
			post_data.id = $(this).attr('custom-issue-id');
			post_data.action = 'delete';
			var post_url = document.location.href;
			$.ajax({
				'url': post_url,
				'type': 'post',
				'data': post_data,
				'success': function(data) {
					document.location.reload();
				}
			});
		};
	});
 });
</script>
</head>
<body>
<?php include 'includes/main_menu.php'; ?>
<div id="container">
		<h1><?php //echo SYSTEM_SUBDOMAIN; ?> Issues List</h1>
	<!--div id="date-selection-form">
		Please select date range<br />
<br />
		<label for="start-date">Start:</label>
		<input type="text" name="start" id="start-date" class="date-selection" value="<?php echo $start; ?>">
		<label for="end-date">End:</label>
		<input type="text" name="end" id="end-date" class="date-selection" value="<?php echo $end; ?>">
<br />
<br />
-->
		<button id="back" type="button" name="action" value="back" onClick="document.location='/Issues/';">Back</button>
<br class="clear">
<!--
		<button type="submit" name="action" value="show">Show issues</button>
<br />
	</div-->
<br />
	<div id="issues-list">
		<table>
		<tr>
			<th>Site</th>
			<th>Date</th>
			<th>Name</th>
			<th>Issue</th>
<?php if (!$read_only) { ?>
			<th>Response</th>
<?php }; ?>
		</tr>
<?php
    /*
	$sql = "SELECT * FROM issues ";
	if (CURRENT_SITE_ID != 0) {
		$sql .= " WHERE site_id = '".CURRENT_SITE_ID."'";
	};
	if (!$delete_possible) {
		if (CURRENT_SITE_ID != 0) {
			$sql .= " AND ";
		} else {
			$sql .= " WHERE ";
		};
		$sql .= "viewed_by = 1";
	};
	$sql .= " ORDER BY issue_date ASC;";
    */

    $username = $_SESSION['myusername'];
    $siteId = CURRENT_SITE_ID;
    if (CURRENT_SITE_ID != 0) { //We are on main so show issues from all locations
        if($read_only) {//read_only is for non management users
            #Query for a Standard User
            $sql = "SELECT * FROM issues WHERE site_id = '$siteId' AND `name` != '' AND (viewed_by = 1 OR `name`= '$username') ORDER BY issue_date ASC;";// #hide anonymous
        } else {
            #Query for Management
            $sql = "SELECT * FROM issues WHERE site_id = '$siteId' ORDER BY issue_date ASC;";// #hide anonymous
        }
    } else { //One of the normal sites, (non main)
        if($read_only) {//read_only is for non management users
            #Query for Standard User on Main
            $sql = "SELECT * FROM issues WHERE `name` != '' AND (viewed_by = 1 OR `name`= '$username') ORDER BY issue_date ASC;"; #hide anonymous
        } else {
            #Query for Management On Main
            $sql = "SELECT * FROM issues ORDER BY issue_date ASC;";// #hide anonymous
        }
    }
    //echo $sql;
	$res = mysql_query($sql) or die(mysql_error());
	while ($row = mysql_fetch_array($res)) {
		if (empty($row['name'])) $row['name'] = 'Anonymously';
		echo "\t<tr id='issue{$row['id']}'>\n";
		echo "\t\t<td class='issue-site'>{$places_by_id[$row['site_id']]['name']}</td>";
		echo "\t\t<td class='issue-date'>{$row['issue_date']}</td>";
		echo "\t\t<td class='issue-name'>{$row['name']}</td>";
		echo "\t\t<td class='issue-desc'>".preg_replace("/([^\s]{30})/", "\\1 ", $row['description']) . "</td>";
		if (!$read_only) {
			echo "\t\t<td class='issue-resp'><button custom-issue-id='{$row['id']}' class='add-response' type='button'>Response</button>";
			if ($delete_possible) {
				echo "<button custom-issue-id='{$row['id']}' class='delete-issue' type='button'>Delete</button>";
			};
			echo "</td>";
		};
		echo "\t</tr>\n";
		//if (!$read_only) {
			$rsql = "select * FROM issue_response WHERE issue_id = {$row['id']};";
			$rres = mysql_query($rsql) or die(mysql_error());
			while ($rrow = mysql_fetch_assoc($rres)) {
				if (empty($rrow['name'])) $rrow['name'] = 'Anonymously';
				echo "\t<tr class='response'>\n";
				echo "\t\t<td class='issue-site'>&nbsp;</td>";
				echo "\t\t<td class='issue-date'>{$rrow['response_date']}</td>";
				echo "\t\t<td class='issue-name'>{$rrow['name']}</td>";
				echo "\t\t<td class='issue-desc'>".preg_replace("/([^\s]{30})/", "\\1 ", $rrow['description']) . "</td>";
				echo "\t\t<td class='response-desc'>&nbsp;</td>";
				echo "\t</tr>\n";
	
			};
		//};
	};
?>
		</table>
	</div>
<br />
	<button name="action" type="submit" value="csv">Download as CSV</button>
</form>
</div>
</body>
</html>
