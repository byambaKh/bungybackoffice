<?php
	include "../includes/application_top.php";
?>
<!DOCTYPE html>
<html>
  <head>
<?php include 'includes/header_tags.php'; ?>
	<title><?php echo SYSTEM_SUBDOMAIN; ?> :: Issues </title>
<script>
function procLogout(){
  document.location ="?logout=true";
  return true;
}
</script>
<style>
h1 {
    color: white;
}
button:disabled div {
    color: silver;
}
div {
    text-align: center;
}
button {
    background-image: url('/img/blank-button.png');
    width: 417px;
    height: 63px;
    border: none;
    background-position: center;
    margin: 1px;
	overflow: hidden;
	padding: 0px;
}
button div {
    width: 100%;
    height: 100%;
    font-size: 40px;
    color: white;
    font-weight: normal;
    font-family: Verdana;
	margin: 0px;
	padding: 0px;
}
</style>
</head>
<body bgcolor=black>
<div>
<img src="/img/index.jpg">
<h1><?php echo SYSTEM_SUBDOMAIN; ?> Issues</h1>
<?php 
	$buttons = array(
		'Submit Issue'	=> 'submit_issue.php',
		'View Issues'	=> '',
		'-'		=> '',
		'-'		=> '',
		'-'		=> '',
		'-'		=> '',
		'-'		=> '',
		'-'		=> '',
		'Back'	=> '/',
	);
	if ($user->hasRole(array('SysAdmin', 'General Manager', 'Site Manager', 'Staff', 'Staff +'))) {
		$buttons['View Issues'] = 'issues.php';
	};
	$i = 0;
	while (list($text, $url) = each($buttons)) {
		if (empty($text)) continue;
		$i++;
?>
<br><button onClick="document.location = '<?php echo $url; ?>';"<?php echo (empty($url))?' disabled':''; ?>><div><?php echo $text; ?></div></button>
<?php
	};
?>
<br><br>
</div>
</body></html>
