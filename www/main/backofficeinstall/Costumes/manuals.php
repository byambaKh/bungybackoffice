<?php
	include '../includes/application_top.php';
// be sure we works with main DB
	mysql_query("SET NAMES 'utf8';");
	$can_edit = false;
	if ($user->hasRole(array('General Manager', 'SysAdmin'))) {
		$can_edit = true;
	};

	if($user->getUserName() == "RomainD") $can_edit = true;
    if($user->getUserName() == "Hiroshi") $can_edit = true;

	if ($can_edit) {
	$action = $_POST['action'];
	switch (TRUE) {
		case ($action == 'add'):
		case ($action == 'Add'):
			$data = $_POST['i'];
			unset($data['action']);
			$data['site_id'] = CURRENT_SITE_ID;
			db_perform('training_manuals', $data);
			Header("Location: manuals.php");
			die();
			break;
		case ($action == 'Update'):
			Header("Location: /Training/?tid=" . $_SESSION['tid']);
			die();
			break;
		case ($action == 'Delete'):
			$sql = "delete from training where id = '{$_GET['id']}'";
			mysql_query($sql) or die(mysql_error());
			Header("Location: /Training/?tid=" . $_SESSION['tid']);
			die();
			break;
		case ($action == 'CSV'):
			$current_month = date("Y-m", $current_time);
			Header('Content-Type: text/csv; name="training'.$current_month.'.csv"');
			Header('Content-Disposition: attachment; filename="training'.$current_month.'.csv"');
			echo '"' . implode('","', $headers) . '"' . "\r\n";
			//for ($i = 1; $i <= 12; $i++) {
    			//$d = $current_year . '-' . sprintf("%02d-", $i);
    			$sql = "select * from training WHERE `date` like '$current_month%' order by `date` ASC;";
    			$res = mysql_query($sql);
				$balance = 0;
    			while ($row = mysql_fetch_assoc($res)) {
					foreach ($fields as $field) {
						echo '"' . $row[$field] . '"' . ($field == $fields[sizeof($fields)-1] ? '' : ',');
					};
					echo "\r\n";
				};
			//};
			die();
			break;
	};
	};
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title>Manuals</title>
<?php include "../includes/head_scripts.php"; ?>
	<script src="manuals_titles.js"></script>
<style>
h1 {
	margin: 30px;
	text-align: center;
	width: 100%;
	color: black;
}
#manuals-table {
	margin-left: auto;
	margin-right: auto;
	width: 800px;
}
#manuals-table th {
	background-color: silver;
	font-size: 30px;
	padding: 10px;
}
#manuals-table input {
	width: 98%;
}
#manuals-table td.actions input {
    width: 49%;
}
.actions {
	width: 150px;
}
.new-title {
	font-weight: bold;
	text-align: center;
}
</style>
</head>
<body>
<?php include 'includes/main_menu.php'; ?>
<script>
$(document).ready(function () {
	$(".delete").click(row_delete);
	$('.change').unbind('click').click(row_change);
    //$('.publishCheckbox').bind('click', publishToggle);
    $('.publishCheckbox').change(publishToggle);
});
</script>
<?php
	$form_fields = array('title_en', 'title_ja'); 
?>
<table id="manuals-table">

<tr>
    <td colspan="<?php echo sizeof($form_fields) + $can_edit; ?>" class="no-borders">&nbsp;</td>
<tr>
<tr>
    <th colspan="<?php echo sizeof($form_fields) + $can_edit; ?>" class="header-title">Manuals</th>
<tr>
<tr>
    <td colspan="<?php echo sizeof($form_fields) + $can_edit; ?>" class="no-borders">&nbsp;</td>
<tr>
    <?php if($can_edit) :?>
<tr>
	<?php 
	for($i = 0; $i < (sizeof($form_fields) + $can_edit); $i++){
		echo "<td></td>";
	}
	?>
    <td>Publish</td>
</tr>
<?php endif ?>
<?php
    $ndelete = array('Raft', 'Reception', 'Customer Service', 'Jump Operator', 'Jump Master', 'Site Manager', 'Photographer');
	$sql = "SELECT * FROM training_manuals WHERE site_id = '".CURRENT_SITE_ID."';";
	$res = mysql_query($sql) or die(mysql_error());
	while ($row = mysql_fetch_assoc($res)) {
        if(!$row['published'] && !$can_edit) continue;
		echo "<tr rel='{$row['id']}'>";
		echo "<td class='value' rel='title_en'><a href='index.php?id={$row['id']}&preview=1'>" . $row['title_en'] . '</a></td>';
		echo "<td class='value' rel='title_ja'><a href='index.php?id={$row['id']}&preview=1'>" . $row['title_ja'] . '</a></td>';
		if ($can_edit) {
			echo "<td class='actions'>";
			echo "<input class='change' type='button' value='Change'>";
			if (!in_array($row['title_en'], $ndelete)) {
				echo "<input class='delete' type='button' value='Delete'>";
			}
			echo "</td>";
		};
		$checked = "";
        if($can_edit) {
            if ($row['published']) $checked = "checked";
            echo "<td><input class='publishCheckbox' type='checkbox' name='publish_{$row['id']}' value='{$row['id']}' $checked></td>\n";
        }
		echo "</tr>";
	};
?>
</tr>
<?php if ($can_edit) { ?>
<form method="post">
<tr>
	<td class="new-title">Title English</td>
	<td class="new-title">Title Japanese</td>
	<td class="new-title">&nbsp;</td>
</tr>
<tr>
<?php
			echo "<td>";
			echo draw_input_field('title_en', 'title_en', array('title_en'=>''));
			echo "</td>";
			echo "<td>";
			echo draw_input_field('title_ja', 'title_ja', array('title_ja'=>''));
			echo "</td>";
			echo "<td>";
			echo "<button name='action' class='add' type='submit' value='add'>Add</button>";
			echo "</td>";
?>
</tr>
</form>
<?php }; ?>
<tr>
    <td colspan="<?php echo sizeof($form_fields) + $can_edit; ?>" class="no-borders">&nbsp;</td>
<tr>
<tr>
    <td colspan='<?php echo sizeof($form_fields) + $can_edit; ?>'><button id='back' onClick="document.location = '/Training/';">Back</button></td>
</tr>
</table>
<br />
<br />

</form>
<?php include ("ticker.php"); ?>
</body>
</html>
