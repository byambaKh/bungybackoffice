<?php 
if (CURRENT_SITE_ID == 0) {
echo "<div id='site-colours'>";
    foreach ($scolors as $pid => $color) {
        if ($pid == 0) continue;
?>
    <div class="color-list">
        <div class="color-item-site" style="background-color: <?php echo $color; ?> !important;<?php echo ($color == '#080') ? ' color: white !important;' : ''; ?>"><?php echo BJHelper::getPlaceName($pid); ?></div>
    </div>
<?php
    };
echo "</div>";
};
?>
