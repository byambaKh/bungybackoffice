<?php
include '../includes/application_top.php';

$edit_events = false;
$user = new BJUser();
if ($user->hasRole('Site Manager') || $user->hasRole('General Manager') || $user->hasRole('SysAdmin') || $user->hasRole('Calendar')) {
    $edit_events = true;    
};

// select all sites
$sites = BJHelper::getPlacesDropDown();
// EOF sites

// load color config settings
$colors = BJHelper::getSiteEventColors();
$scolors = BJHelper::getSiteEventColors();
// EOF load color
if (isset($_GET['month'])) {
    if (isset($_GET['year'])) {
        $_GET['date'] = $_GET['year'];
    } else {
        $_GET['date'] = date("Y");
    };
    $_GET['date'] .= '-' . $_GET['month'] . '-01';
};
$current_time = time();
$d = date('Y-m-d', $current_time);
if (isset($_GET['date'])) {
    if (strlen($_GET['date']) < 8) {
        $_GET['date'] .= '-15';
    };
    $current_time = mktime(0, 0, 0, substr($_GET['date'], 5, 2), substr($_GET['date'], 8, 2), substr($_GET['date'], 0, 4));
} else {
    $current_time = mktime(0, 0, 0, substr($d, 5, 2), substr($d, 8, 2), substr($d, 0, 4));
};
if (isset($_POST) && array_key_exists('filter', $_POST)) {
    $_GET['filter'] = $_POST['filter'];
};
if (isset($_GET['filter']) && !empty($_GET['filter'])) {
    foreach ($_GET['filter'] as $f) {
        switch ($f) {
            case ('events'):
                define('SHOW_ONLY_EVENTS', true);
                break;
            case ('site-open'):
                define('SHOW_ONLY_OPEN_DAYS', true);
                break;
            case ('work-days'):
                define('SHOW_ONLY_I_WORK', true);
                break;
        };
    };
};
define('SHOW_ONLY_EVENTS', false);
define('SHOW_ONLY_I_WORK', false);
define('SHOW_ONLY_OPEN_DAYS', false);
$first_day_time = mktime(0, 0, 0, date("m", $current_time), 1, date("Y", $current_time));
$last_day_time = mktime(0, 0, 0, date("m", $current_time), date("t", $current_time)+1, date("Y", $current_time));
$start_time = $first_day_time - date('w', $first_day_time) * 24 * 60 * 60;
$today_date = date("Y-m-d", $first_day_time);
// Get Calendar State records for this month
$cstate = array();
$cclass = array();
$odates = array();
if (CURRENT_SITE_ID > 0) {
    $sql = "SELECT * FROM calendar_state
			WHERE 
				site_id = " . CURRENT_SITE_ID . "
				AND BOOK_DATE >= '" . date("Y-m-d", $first_day_time) . "'
				AND BOOK_DATE <= '" . date("Y-m-d", $last_day_time) . "' ORDER BY BOOK_DATE ASC;";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        $cstate[$row['BOOK_DATE']] = $row;
        switch (true) {
            case ($row['USER_VIEW_STATUS'] == 9 && $row['OP_VIEW_STATUS'] == 9):
                $cclass[$row['BOOK_DATE']] = 'open';
                $odates[] = $row['BOOK_DATE'];
                break;
            case ($row['USER_VIEW_STATUS'] == 1 && $row['OP_VIEW_STATUS'] == 1):
                $cclass[$row['BOOK_DATE']] = 'closed';
                break;
            case ($row['USER_VIEW_STATUS'] == 9 && $row['OP_VIEW_STATUS'] == 1):
                $cclass[$row['BOOK_DATE']] = 'only-online';
                $odates[] = $row['BOOK_DATE'];
                break;
        };
    };
    mysql_free_result($res);
    // Check UserID for show only I work filter
    $dates = array();
    if (SHOW_ONLY_OPEN_DAYS) {
        foreach ($cstate as $bdate => $row) {
            if ($row['OP_VIEW_STATUS'] == 9) {
                $dates[] = $bdate;
            };
        };
    };
};
if (SHOW_ONLY_I_WORK) {
    $dates = array();
    $uid = $_SESSION['user']['UserID'];
    $sql = "SELECT rp.roster_day
			FROM roster_staff rs
			INNER JOIN roster_position rp
				ON (rs.roster_month = rp.roster_month AND rs.place = rp.place)
			WHERE 
				rs.site_id = " . CURRENT_SITE_ID . "
				AND rp.site_id = " . CURRENT_SITE_ID . "
				AND rs.roster_month = " . date("Ym", $current_time) . "
			ORDER BY roster_day ASC;";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        $dates[] = date("Y-m-", $current_time) . sprintf("%02d", $row['roster_day']);
    };
    mysql_free_result($res);
};
$rurl = '';
include 'post_processing.php';
$all_data = array();
// roster info
if (!SHOW_ONLY_EVENTS && CURRENT_SITE_ID > 0) {
    $sql = "SELECT rp.roster_day, rpa.name AS position, u.username
		FROM roster_position rp, roster_staff rs, users u, roster_position_all rpa
		WHERE
			rp.site_id = " . CURRENT_SITE_ID . "
			AND rs.site_id = " . CURRENT_SITE_ID . "
			AND rp.roster_month = '" . date("Ym", $first_day_time) . "'
			AND rs.roster_month = '" . date("Ym", $first_day_time) . "'
			AND rp.position_id = rpa.id";
    if (!empty($dates)) {
        $days = array();
        foreach ($dates as $date) {
            $days[] = (int)substr($date, 8, 2);
        };
        if (!empty($days)) {
            $sql .= " and rp.roster_day in (" . implode(', ', $days) . ")";
        };
    };
    $sql .= "
			and rp.place = rs.place
			and rs.staff_id = u.UserID";
    $res = mysql_query($sql) or die(mysql_error());
    $rdate = date("Y-m-", $first_day_time);
    while ($row = mysql_fetch_assoc($res)) {
        $temp_date = $rdate . sprintf("%02d", $row['roster_day']);        
        if (!array_key_exists($temp_date, $all_data)) {
            $all_data[$temp_date] = array();
        };
        $all_data[$temp_date]['Roster'][] = array(
            'class' => 'roster',
            'data'  => '<span><nobr>' . $row['username'] . ' - ' . $row['position'] . '</nobr></span>'
        );
    };
    mysql_free_result($res);
};

// booking received info Byamba - add 2020-06-13
if (!SHOW_ONLY_EVENTS && CURRENT_SITE_ID > 0) {
      $sql = "SELECT DATE_FORMAT(cr.BookingReceived, '%Y-%m-%d') AS BookingReceived, count(cr.CustomerRegID) AS total_received, sum(cr.NoOfJump) as Jumps 
      FROM customerregs1 cr
            WHERE 
                cr.site_id = " . CURRENT_SITE_ID . "    
                and DATE_FORMAT(cr.BookingReceived, '%Y-%m-%d') >= '". date("Y-m-d", $first_day_time). "'
                and DATE_FORMAT(cr.BookingReceived, '%Y-%m-%d') <= '". date("Y-m-d", $last_day_time) . "'          
                and cr.DeleteStatus = '0'
                and cr.CancelFeeQTY = '0'
                and cr.NoOfJump > 0
            GROUP BY DATE_FORMAT(cr.BookingReceived, '%Y-%m-%d')";

    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        if (!array_key_exists($row['BookingReceived'], $all_data)) {
            $all_data[$row['BookingReceived']] = array();
        };

        if( date("Y-m-d", $first_day_time) > '2019-06-30')
        {
            $all_data[$row['BookingReceived']]['New'][] = array(
            'class'           => 'BookingReceived',
            'total_breceived' =>  $row['total_received'], 
            'Jumpers'   => $row['Jumps'],
            );            
        }        
    };
    
    mysql_free_result($res);
};


// Bookings info
if (!SHOW_ONLY_EVENTS && CURRENT_SITE_ID > 0) {
    $sql = "SELECT cr.BookingDate, count(cr.CustomerRegID) AS total_bookings, sum(cr.NoOfJump) AS total_jumps, min(cr.BookingTime) AS first, max(cr.BookingTime) AS last
			FROM customerregs1 cr
			WHERE 
				cr.site_id = " . CURRENT_SITE_ID . "";
    if (!empty($dates)) {
        $sql .= " AND cr.BookingDate in ('" . implode("', '", $dates) . "')";
    } else {
        $sql .= "
				AND cr.BookingDate >= '" . date("Y-m-d", $first_day_time) . "'
				and cr.BookingDate <= '" . date("Y-m-d", $last_day_time) . "'";
    };
    $sql .= "
				and cr.DeleteStatus = '0'
				and cr.NoOfJump > 0
			GROUP BY cr.BookingDate;";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        if (!array_key_exists($row['BookingDate'], $all_data)) {
            $all_data[$row['BookingDate']] = array();
        };
        $all_data[$row['BookingDate']]['Bookings'][] = array(
            'class'          => 'bookings',
            'total_jumps'    => $row['total_jumps'],
            'total_bookings' => $row['total_bookings'],
            'first'          => $row['first'],
            'last'           => $row['last'],
        );
    };
    
    mysql_free_result($res);
};
if (empty($dates)) {
    $ctime = $first_day_time;
    while ($ctime <= $last_day_time) {
        $dates[] = date("Y-m-d", $ctime);
        $ctime += 24 * 60 * 60;
    };
};
foreach ($dates as $d) {
    // Events info
    $sql = "SELECT *
			FROM calendar_events
			WHERE ";
    if (CURRENT_SITE_ID > 0) {
        $sql .= "	site_id = '" . CURRENT_SITE_ID . "'";
    } else {
        // all events
        $sql .= " 1 ";
    };

    $sql .= "
                and (
					(event_date <= '" . $d . "' and event_finish_date >= '" . $d . "') or
					(event_date = '$d' and event_finish_date = '0000-00-00')
				)
			ORDER BY start_time ASC";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        if (!array_key_exists($d, $all_data)) {
            $all_data[$d] = array();
        };
        $all_data[$d]['Events'][] = array(
            'class'             => 'events',
            'id'                => $row['id'],
            'site_id'           => $row['site_id'],
            'title'             => mb_convert_encoding($row['title'], 'UTF-8', 'SJIS'),
            'all_day'           => $row['all_day_event'],
            'description'       => mb_convert_encoding($row['description'], 'UTF-8', 'SJIS'),
            'start_time'        => $row['start_time'],
            'end_time'          => $row['end_time'],
            'event_date'        => $row['event_date'],
            'event_finish_date' => ($row['event_finish_date'] == '0000-00-00') ? $row['event_date'] : $row['event_finish_date'],
            'color'             => $row['color'],
        );
    };
    mysql_free_result($res);
};
// load start/end drop-down values
$values = array(
    'start'  => BJHelper::getCalendarEventTimes(),
    'finish' => BJHelper::getCalendarEventTimes()
);

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title><?php echo SYSTEM_SUBDOMAIN; ?> SITE CALENDAR - <?php echo date("F Y", $current_time); ?></title>
    <?php include "../includes/head_scripts.php"; ?>
    <script language="javascript" src="/js/tooltip.js"></script>
    <link rel="stylesheet" href="stylesheet.css" type="text/css" media="all"/>

    <style>
        #calendar-header-table th.banner {
            background-color: <?php echo array_key_exists(CURRENT_SITE_ID, $colors) ? $colors[CURRENT_SITE_ID] : 'white'; ?>;
        <?php
            if (array_key_exists(CURRENT_SITE_ID, $colors) && $colors[CURRENT_SITE_ID] == '#080') {
                echo "\tcolor: white !important;";
            };
        ?>
        }
    </style>
    <script>
        $(document).ready(function () {
            $(".calendar-row td").on('mouseenter', function () {
                $('.calendar-row td').each(function () {
                    $(this).removeClass('active-day');
                });
                if (!$(this).hasClass('not-this-month')) {
                    $(this).addClass('active-day');
                }
                ;
            });
            $(".calendar-row td").on('click', function (e) {
                // for some other child element
                var current_date = '<?php echo date("Y-m", $current_time); ?>';
                if (e.target != $(this)[0]) return;
                document.location = 'daily_view.php?date=' + current_date + '-' + $(this).attr('custom-day');
            });
            $(".calendar-day, .Bookings, .Roster").on('click', function (e) {
                // for some other child element
                var current_date = '<?php echo date("Y-m", $current_time); ?>';
                if (e.target != $(this)[0]) return;
                var day = $(this).parent().attr('custom-day');
                if (typeof day == 'undefined') {
                    day = $(this).parent().parent().attr('custom-day');
                }
                ;
                document.location = 'daily_view.php?date=' + current_date + '-' + day;
            });
        });
    </script>
    <?php include "add_event_js.php"; ?>
</head>
<body>
<?php include '../includes/main_menu.php'; ?>
<table id="calendar-header-table">
    <tr>
        <th class='banner'>
            <?php include 'color_reference.php'; ?>
            <h1><?php echo SYSTEM_SUBDOMAIN; ?> SITE CALENDAR <br/><?php echo strtoupper(date("F Y", $current_time)); ?>
            </h1>
        </th>
    </tr>
    <form>
        <tr>
            <th class='banner'>
                Show only:
                Events
                <input type="checkbox" name="filter[]" value="events"<?php echo (isset($_GET['filter']) && !empty($_GET['filter']) && in_array('events', $_GET['filter'])) ? ' checked="checked"' : ''; ?>>
                Days I Work
                <input type="checkbox" name="filter[]" value="work-days"<?php echo (isset($_GET['filter']) && !empty($_GET['filter']) && in_array('work-days', $_GET['filter'])) ? ' checked="checked"' : ''; ?>>
                Site Open Days<input type="checkbox" name="filter[]" value="site-open"<?php echo (isset($_GET['filter']) && !empty($_GET['filter']) && in_array('site-open', $_GET['filter'])) ? ' checked="checked"' : ''; ?>>
                <button name='action' value='filter' type="submit">Update</button>
            </th>
        </tr>
    </form>
    <tr>
        <th>
            <div id="current-date">
                <form method="get" style="dispaly: inline-block;">
                    <?php
                    // hide filter variables ifisset
                    if (isset($_GET['filter'])) {
                        foreach ($_GET['filter'] as $f) {
                            echo "<input type='hidden' name='filter[]' value='$f'>";
                        };
                    };
                    ?>
                    <button id="previous_date" name="date" value="<?php echo date("Y-m", $current_time - 30 * 24 * 60 * 60); ?>" class="clickable square-button">&lt;</button>
                    <button id="today" name="date" value="<?php echo date("Y-m-d"); ?>" class="clickable">Today</button>
                    <button id="next_date" name="date" value="<?php echo date("Y-m", $current_time + 30 * 24 * 60 * 60); ?>" class="clickable square-button">&gt;</button>
                    <?php if (CURRENT_SITE_ID == 0) { ?>
                        <button id="colour-settings" name="colour-settings" value="colour-settings" class="clickable" type="button">Colour Settings</button>
                    <?php }; ?>
                </form>
                <?php if ($edit_events) { ?>
                    <button onClick="show_add_event_form();" id="show-add-event-form" class="clickable" type="button">Add event</button>
                <?php }; ?>
                <?php
                include "../includes/switch_site.php";
                ?>
            </div>
            <div id="drop-down-date">

            </div>
        </th>
    </tr>
</table>
<table id="calendar-table">
    <?php
    echo "\t<tr class='calendar-row'>\n";
    for ($i = 0; $i < 7; $i++) {
        echo "\t\t<th>\n" . date('l', $start_time + $i * 24 * 60 * 60) . "</th>";
    };
    echo "\t</tr>\n";
    $calendar_current_time = $start_time;
    while ($calendar_current_time < $last_day_time) {
        echo "\t<tr class='calendar-row'>\n";
        $current_col = 0;
        while ($current_col < 7) {
            $add_class = '';
            if (date("m", $current_time) != date("m", $calendar_current_time)) {
                $add_class = ' not-this-month';
            }
            $calendar_date = date("Y-m-d", $calendar_current_time);
            if (array_key_exists($calendar_date, $cclass)) {
                $add_class .= ' ' . $cclass[$calendar_date];
            };
            echo "\t\t<td class='$add_class' custom-day=\"" . date("d", $calendar_current_time) . "\">\n";
            echo "\t\t\t<div class='calendar-day'>" . date("j", $calendar_current_time) . "</div>\n";
            if (array_key_exists($calendar_date, $all_data) && !empty($all_data[$calendar_date])) {
                foreach ($all_data[$calendar_date] as $section => $section_details) {
                    $tooltip = '';
                    $section_info = '';
                    $events_html = '';                    
                    switch (TRUE) {
                        case ($section == 'Events'):
                            foreach ($section_details as $row) {
                                $s = '';
                                switch (TRUE) {
                                    case (!empty($row['color']) && CURRENT_SITE_ID == $row['site_id']) :
                                        $s = " style='background-color: #{$row['color']} !important;'";
                                        break;
                                    case (array_key_exists($row['site_id'], $colors)) :
                                        if ($colors[$row['site_id']] == '#080') {
                                            $s = " style='background-color: {$colors[$row['site_id']]} !important; color: white !important;'";
                                        } else {
                                            $s = " style='background-color: {$colors[$row['site_id']]} !important;'";
                                        };
                                        break;
                                }
                                $events_html .= "<div $s class=\"cell-content {$row['class']}\" onmouseover=\"show_event_tooltip('{$row['id']}');\" onmouseout=\"tooltip.hide();\"";
                                foreach ($row as $key => $value) {
                                    if ($key == 'class') continue;
                                    $events_html .= ' custom-' . str_replace('_', '-', $key) . '="' . htmlspecialchars($value) . '"';
                                };
                                $events_html .= '>';
                                if (CURRENT_SITE_ID == 0 && $row['site_id'] != 0) {
                                    $events_html .= BJHelper::getShortPlaceName($row['site_id']) . ' ';
                                };
                                if (!$row['all_day']) {
                                    $events_html .= $row['start_time'] . '-' . $row['end_time'] . ' ';
                                };
                                $events_html .= $row['title'];
                                $events_html .= "</div>";
                            };
                            break;
                        case ($section == 'Roster'):
                            foreach ($section_details as $row) {
                                $tooltip .= "<div class=\"cell-content {$row['class']}\">" . $row['data'] . "</div>";
                                $section_info++;
                            };
                            break;
                        case ($section == 'New'):                            
                            $section_info = "{$section_details[0]['Jumpers']} from {$section_details[0]['total_breceived']} ";                            
                            break;
                        case ($section == 'Bookings') :
                            $section_info = "{$section_details[0]['total_bookings']}/{$section_details[0]['total_jumps']}";                            
                            $tooltip = "<nobr>Total bookings: " . $section_details[0]['total_bookings'] . "</nobr><br />";
                            $tooltip .= "<nobr>Total jumps: " . $section_details[0]['total_jumps'] . "</nobr><br />";
                            $tooltip .= "<nobr>First jump: " . $section_details[0]['first'] . "</nobr><br />";
                            $tooltip .= "<nobr>Last jump: " . $section_details[0]['last'] . "</nobr>";
                            break;
                    };
                    echo "\t\t\t<div class='cell-content $section'";
                    if ($tooltip != '') {
                        echo " onmouseover=\"tooltip.show('" . htmlspecialchars($tooltip) . "');\" onmouseout=\"tooltip.hide();\"";
                    };
                    if ($section != 'Events') {
                        echo ">" . $section;
                    } else {
                        echo ">";
                        echo $events_html;
                    };
                    if ($section_info != '') {
                        echo " ($section_info)";
                    };
                    echo "</div>\n";
                };
            };
            echo "\t\t</td>\n";
            $current_col++;
            $calendar_current_time += 24 * 60 * 60;
        };
        echo "\t</tr>\n";        
    };
    ?>
</table>
<?php include "add_event_form.php"; ?>
<div id="calendar-buttons">
    <button onClick="javascript: document.location='/'" id="back">Back</button>
</div>
<?php include("ticker.php"); ?>
</body>
</html>
