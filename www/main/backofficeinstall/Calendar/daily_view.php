<?php
	include '../includes/application_top.php';

	$edit_events = false;
    $user = new BJUser();
    if ($user->hasRole('Site Manager') || $user->hasRole('General Manager') || $user->hasRole('SysAdmin')) {
        $edit_events = true;
    };

	$sites = BJHelper::getPlacesDropDown();
	$colors = BJHelper::getSiteEventColors();
	$scolors = BJHelper::getSiteEventColors();

	if (isset($_GET['month'])) {
		if (isset($_GET['year'])) {
			$_GET['date'] = $_GET['year'];
		} else {
			$_GET['date'] = date("Y");
		};
		$_GET['date'] .= '-' . $_GET['month'] . '-01';
	};
    $current_time = time();
    $d = date('Y-m-d', $current_time);
    if (isset($_GET['date'])) {
		if (strlen($_GET['date']) < 8) {
			$_GET['date'] .= '-15';
		};
        $current_time = mktime(0, 0, 0, substr($_GET['date'], 5, 2), substr($_GET['date'], 8, 2), substr($_GET['date'], 0, 4));
    } else {
        $current_time = mktime(0, 0, 0, substr($d, 5, 2), substr($d, 8, 2), substr($d, 0, 4));
    };
    //since this is modified when we use the roster, I am going to store a copy of it that will be used in
    //add_event_js.php
	$today_date = date("Y-m-d", $current_time);
    $event_date = $today_date;

    $rurl = 'daily_view.php';
    include 'post_processing.php';

	$all_data = array();
// roster info
	$sql = "SELECT rp.roster_day, rpa.name as position, u.StaffListName as username
		FROM roster_position rp, roster_staff rs, users u, roster_position_all rpa
		WHERE
			rp.site_id = " . CURRENT_SITE_ID . "
			AND rs.site_id = " . CURRENT_SITE_ID . "
			AND rp.roster_month = ".date("Ym", $current_time) . "
			AND rs.roster_month = ".date("Ym", $current_time) . "
			and rp.roster_day = ".date("j", $current_time);
	$sql .= "
			and rp.position_id = rpa.id
			and rp.place = rs.place
			and rs.staff_id = u.UserID";
	$res = mysql_query($sql) or die(mysql_error());
	while ($row = mysql_fetch_assoc($res)) {
		$today_date = $rdate . sprintf("%02d", $row['roster_day']);
		if (!array_key_exists($today_date, $all_data)) {
			$all_data[$today_date] = array();
		};
		$all_data[$today_date]['Roster'][] = array(
			'class'		=> 'roster',
			'data'		=> '<span><nobr>' . $row['username'] . ' - ' . $row['position'] . '</nobr></span>',
			'name'		=> $row['username'],
			'position'	=> $row['position']
		);
	};
	mysql_free_result($res);
// Bookings info
	$sql = "SELECT cr.BookingDate, cr.BookingTime, cr.NoOfJump, cr.RomajiName
			FROM customerregs1 cr
			WHERE
				site_id = " . CURRENT_SITE_ID . "
				AND cr.BookingDate = '".date("Y-m-d", $current_time)."'
				and cr.DeleteStatus = '0'
				and cr.NoOfJump > 0
			ORDER BY cr.BookingTime ASC;";
	$res = mysql_query($sql) or die(mysql_error());
	$bookings = array();
	while ($row = mysql_fetch_assoc($res)) {
		$row['BookingTime'] = str_replace(" AM", "", $row['BookingTime']);
		$row['BookingTime'] = str_replace(" PM", "", $row['BookingTime']);
		$bookings[$row['BookingTime']][] = array(
			'name'	=> $row['RomajiName'], 
			'jumps' => $row['NoOfJump'],
		);
	};
	mysql_free_result($res);
// Events info
	$d = date("Y-m-d", $current_time);
	$sql = "SELECT *
			FROM calendar_events
			WHERE ";
        if (CURRENT_SITE_ID > 0) {
                $sql .= "   site_id = '".CURRENT_SITE_ID."'";
        } else {
            // all events
            $sql .= " 1 ";
        };

        $sql .= "
                and (
                    (event_date <= '".$d."' and event_finish_date >= '".$d."') or
                    (event_date = '$d' and event_finish_date = '0000-00-00')
                )
            ORDER BY start_time ASC";
	$res = mysql_query($sql) or die(mysql_error());
	$events = array();
	while ($row = mysql_fetch_assoc($res)) {
		$row['start_time'] = preg_replace("/^([\d]{1}):([\d]{2})/is", "0\\1:\\2", $row['start_time']);
		if (!preg_match("/^([\d]{2}:([03][0]))/is", $row['start_time'])) $row['start_time'] = '06:00';
		if ($row['all_day_event']) $row['start_time'] = '06:00';
		$events[$row['start_time']][] = array(
			'class'			=> 'events',
			'id'			=> $row['id'], 
			'site_id'		=> $row['site_id'], 
			'title'             => mb_convert_encoding($row['title'], 'UTF-8', 'SJIS'),
			'all_day'           => $row['all_day_event'],
			'description'       => mb_convert_encoding($row['description'], 'UTF-8', 'SJIS'),
			'start_time'	=> $row['start_time'],
			'end_time'		=> $row['end_time'],
			'event_date'	=> $row['event_date'],
			'event_finish_date'	=> ($row['event_finish_date'] == '0000-00-00') ? $row['event_date'] : $row['event_finish_date'],
			'color'	=> $row['color'],
		);
	};
	mysql_free_result($res);
// load start/end drop-down values
    $values = array(
        'start'     => BJHelper::getCalendarEventTimes(),
        'finish'    => BJHelper::getCalendarEventTimes()
    );

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo SYSTEM_SUBDOMAIN; ?> SITE CALENDAR - <?php echo date("d F Y", $current_time); ?></title>
<?php include "../includes/head_scripts.php"; ?>
	<script language="javascript" src="/js/tooltip.js"></script>
	<link rel="stylesheet" href="stylesheet.css" type="text/css" media="all" />
<style>
/* Daily view */
#daily-table {
	width: 98%;
	margin-right: auto;
	margin-left: auto;
	border-collapse: collapse;
	margin: 1%;
	table-layout: fixed;
}
#daily-table td {
	border: 1px solid silver;
	min-height: 20px;
}
#daily-table td.time-cell {
	max-width: 50px;
	width: 50px !important;
	text-align: right;
	vertical-align: top;
}
#daily-table td.daily-cell {
	min-height: 20px;
	height: 20px;
}
/* Roster table */
#roster-table {
	width: 200px;
	border-collapse: collapse;
	margin: 1%;
}
#roster-table td {
	border: 1px solid silver;
	padding: 3px;
}
/* EOF Daily view */
#calendar-header-table th.banner {
    background-color: <?php echo array_key_exists(CURRENT_SITE_ID, $colors) ? $colors[CURRENT_SITE_ID] : 'white'; ?>;
<?php
    if (array_key_exists(CURRENT_SITE_ID, $colors) && $colors[CURRENT_SITE_ID] == '#080') {
        echo "\tcolor: white !important;";
    };
?>
}
.events {
	float: left;
}
</style>
<?php include "add_event_js.php"; ?>
</head>
<body>
<?php include '../includes/main_menu.php'; ?>
<table id="calendar-header-table">
<tr>
	<th colspan="3" class="banner">
<?php include 'color_reference.php'; ?>
		<h1><?php echo SYSTEM_SUBDOMAIN; ?> SITE CALENDAR <br /><?php echo strtoupper(date("d F Y", $current_time)); ?></h1>
	</th>
</tr>
<tr>
    <th colspan=3 valign=top>
		<div id="current-date">
			<form method="get" style="display: inline-block;">
				<button id="previous_date" name="date" value="<?php echo date("Y-m-d", $current_time - 24 * 60 * 60); ?>" class="clickable square-button">&lt;</button>
				<button id="today" name="date" value="<?php echo date("Y-m-d"); ?>" class="clickable">Today</button>
				<button id="next_date" name="date" value="<?php echo date("Y-m-d", $current_time + 24 * 60 * 60); ?>" class="clickable square-button">&gt;</button>
			</form>
<?php if ($edit_events) { ?>
<button onClick="show_add_event_form();" id="show-add-event-form" class="clickable">Add event</button>
<?php }; ?>
<?php
    include "../includes/switch_site.php";
?>
		</div>
	</th>
</tr>
</table>
<?php
	if (!empty($all_data[$today_date]['Roster'])) {
		echo "<table id='roster-table'>\n";
		echo "<tr>\n";
		echo "\t<th colspan=2>Staff</th>\n";
		echo "</tr>\n";
		foreach ($all_data[$today_date]['Roster'] as $id => $row) {
			echo "<tr>\n";
			echo "\t<td>{$row['name']}</td>\n";
			echo "\t<td>{$row['position']}</td>\n";
			echo "</tr>\n";
		};
		echo "</table>\n";
	};
?>
<table id="daily-table">
<tr>
	<th style="width: 50px;">&nbsp;</th>
	<th>Events</th>
	<th>Bookings</th>
</tr>
<?php
	for ($i = 0; $i < 13; $i++) {
		echo "<tr class='daily-row'>\n";
		echo "\t<td rowspan='2' class='time-cell'>".sprintf("%02d:00", 6+$i)."</td>\n";
		$time_key = sprintf("%02d:00", 6+$i);
		$events_string = '';
		if (array_key_exists($time_key, $events)) {
			foreach ($events[$time_key] as $row) {
				$times = ($row['all_day']) ? 'All day event' : 'Finish: ' . $row['end_time'];
				$attr = '';
				foreach ($row as $name => $val) {
					$attr .= ' custom-' . str_replace('_', '-', $name) . '="' . str_replace('"', '&quot;', $val) . '"'; 
				};
				$attr .= " onmouseover=\"show_event_tooltip('{$row['id']}');\" onmouseout=\"tooltip.hide();\"";
				$s = '';
				switch (TRUE) {
					case (!empty($row['color']) && CURRENT_SITE_ID == $row['site_id']) :
						$s = " style='background-color: #{$row['color']} !important;'";
						break;
					case (array_key_exists($row['site_id'], $colors)) :
						if ($colors[$row['site_id']] == '#080') {
							$s = " style='background-color: {$colors[$row['site_id']]} !important; color: white !important;'";
						} else {
							$s = " style='background-color: {$colors[$row['site_id']]} !important;'";
						};
						break;
				}
				if (CURRENT_SITE_ID == 0 && $row['site_id'] != 0) {
					$row['title'] = BJHelper::getShortPlaceName($row['site_id']) . ' ' . $row['title'];
				};
				$events_string .= '<div class="events"'.$attr.' ' . $s.'>' . $row['title'] . ' (' . $times . ')</div>';
			};
		};
		echo "\t<td class='daily-cell'>$events_string</td>\n";
		$time_key = sprintf("%02d:00", 6+$i);
		$bookings_string = '';
		if (array_key_exists($time_key, $bookings)) {
			foreach ($bookings[$time_key] as $row) {
				$bookings_string .= (empty($bookings_string)) ? '' : ', ';
				$bookings_string .= $row['name'] . ' (' . $row['jumps'] . ')';
			};
		};
		echo "\t<td class='daily-cell'>$bookings_string</td>\n";
		echo "</tr>\n";
		echo "<tr class='daily-row'>\n";
		$time_key = sprintf("%02d:30", 6+$i);
		$events_string = '';
		if (array_key_exists($time_key, $events)) {
			foreach ($events[$time_key] as $row) {
				$times = ($row['all_day']) ? 'All day event' : 'Finish: ' . $row['end_time'];
				$attr = '';
				foreach ($row as $name => $val) {
					$attr .= ' custom-' . str_replace('_', '-', $name) . '="' . str_replace('"', '&quot;', $val) . '"'; 
				};
				$attr .= " onmouseover=\"show_event_tooltip('{$row['id']}');\" onmouseout=\"tooltip.hide();\"";
				$s = '';
				switch (TRUE) {
					case (!empty($row['color']) && CURRENT_SITE_ID == $row['site_id']) :
						$s = " style='background-color: #{$row['color']} !important;'";
						break;
					case (array_key_exists($row['site_id'], $colors)) :
						if ($colors[$row['site_id']] == '#080') {
							$s = " style='background-color: {$colors[$row['site_id']]} !important; color: white !important;'";
						} else {
							$s = " style='background-color: {$colors[$row['site_id']]} !important;'";
						};
						break;
				}
				if (CURRENT_SITE_ID == 0 && $row['site_id'] != 0) {
					$row['title'] = BJHelper::getShortPlaceName($row['site_id']) . ' ' . $row['title'];
				};

				$events_string .= '<div class="events"'.$attr.' ' . $s.'>' . $row['title'] . ' (' . $times . ')</div>';
			};
		};
		echo "\t<td class='daily-cell'>$events_string</td>\n";
		//echo "\t<td class='daily-cell'></td>\n";
		$bookings_string = '';
		if (array_key_exists($time_key, $bookings)) {
			foreach ($bookings[$time_key] as $row) {
				$bookings_string .= (empty($bookings_string)) ? '' : ', ';
				$bookings_string .= $row['name'] . ' (' . $row['jumps'] . ')';
			};
		};
		echo "\t<td class='daily-cell'>$bookings_string</td>\n";
		echo "</tr>\n";
	};
?>
</table>
<?php include "add_event_form.php"; ?>
<div id="calendar-buttons">
<button onClick="javascript: document.location='/Calendar/?date=<?php echo date("Y-m-d", $current_time); ?>';" id="Back">Back</button>
</div>
<?php include ("ticker.php"); ?>
</body>
</html>
