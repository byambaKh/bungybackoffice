<script>
$(document).ready(function () {
<?php if (CURRENT_SITE_ID == 0) { ?>
	$('#colour-settings').click(function () {
		var cwindow = window.open('color_manager.php', 'cwindow', 'height=580px,width=350px,location=0,menubar=0,status=0,toolbar=0');
		cwindow.focus();
	});
<?php }; ?>
	$('#color').change(function () {
		var val = $(this).val();
		if (val == '') {
			val = 'FFF';
		};
		$(this).css('background-color', '#' + val);
	});
<?php if ($edit_events) { ?>
	$("#delete-event-button").on('click', function () {
		return window.confirm('Are you sure to delete this event?');
	});
	$(".events").on('click', function (e) {
		var current_date = '<?php echo date("Y-m", $current_time); ?>';
		e.preventDefault();
		$('#calendar-table, #daily-table').slideUp('slow', function () {
			$("#add-event").slideDown('slow');
			$("#calendar-buttons").slideUp('slow');
		});
		//$('#add-event input[name=event_date]').val(current_date + '-' + $(this).parent().parent().attr('custom-day'));
		$('#add-event input[name=event_date]').val($(this).attr('custom-event-date'));
		$('#add-event input[name=event_title]').val($(this).attr('custom-title'));
		$('#add-event textarea[name=event_description]').val($(this).attr('custom-description'));
		$('#add-event select[name=event_start]').val($(this).attr('custom-start-time'));
		$('#add-event select[name=event_end]').val($(this).attr('custom-end-time'));
		$('#add-event input[name=id]').val($(this).attr('custom-id'));
<?php if (CURRENT_SITE_ID == 0) { ?>
		$('#add-event select[name=site_id]').val($(this).attr('custom-site-id'));
<?php }; ?>
		$('#add-event select[name=color]').val($(this).attr('custom-color'));
		$('#add-event select[name=color]').trigger('change');
		var ad = $(this).attr('custom-all-day');
		if (ad == 1) {
			$('#add-event input[name=all_day]').prop('checked', 1);
		} else {
			$('#add-event input[name=all_day]').prop('checked', 0);
		};
		$("#add-event-button").val('update').html('Update');
		$("#all-day-event").trigger('change');
		// after all day event change to set right end date
		$('#add-event input[name=event_finish_date]').val($(this).attr('custom-event-finish-date'));
		$("#form-title").html('Update Event');
		$("#delete-event-button").css('display', 'block');
	});
<?php } else { ?>
	$(".events").on('click', function (e) {
        // for some other child element
        if (e.target != $(this)[0]) return;
		var current_date = '<?php echo date("Y-m", $current_time); ?>';
        document.location = 'daily_view.php?date=' + current_date + '-' + $(this).parent().parent().attr('custom-day');
    });
<?php }; ?>
	$(".date-selection").datepicker({
		showAnim: 'fade',
		numberOfMonths: 1,
		dateFormat: 'yy-mm-dd',
		currentText: 'Today'
	});
	$('#add-event select[name=event_start]').change(function () {
		if ($(this).val() > $('#add-event select[name=event_end]').val()) {
			$('#add-event select[name=event_end]').val($(this).val());
		};
	});
	on_all_day_change();
	$("#all-day-event").on('change', on_all_day_change);
});
	function on_all_day_change() {
		if ($("#all-day-event").prop('checked') == 1) {
			$('#time-selection').slideUp();
			$('#finish-date-selection').slideDown();
			$('#finish-date-selection > input').val($('#add-event input[name=event_date]').val());
		} else {
			$('#time-selection').slideDown();
			$('#finish-date-selection').slideUp();
		};
	}
	function clear_add_event_form() {
		//var d = '<?php //echo $today_date; ?>';//this variable is defined in daily_view.php but is corrupted if there is roster data
        var d = '<?php echo $event_date; ?>';//this variable is defined in daily_view.php
		$("#form-title").html('Add new event');
        $('#add-event input[name=event_date]').val(d);
        $('#add-event input[name=event_finish_date]').val(d);
        $('#add-event input[name=event_title]').val('');
        $('#add-event textarea[name=event_description]').val('');
        $('#add-event input[name=event_start]').val('');
        $('#add-event input[name=event_end]').val('');
        $('#add-event input[name=id]').val('');
		$('#add-event input[name=all_day]').prop('checked', 0);
		$('#add-event select[name=color]').val('');
<?php if (CURRENT_SITE_ID == 0) { ?>
		$('#add-event select[name=site_id]').val(0);
<?php }; ?>
		$("#all-day-event").trigger('change');
		$("#delete-event-button").css('display', 'none');
	}
<?php if ($edit_events) { ?>
	function show_add_event_form() {
		clear_add_event_form();
		$('#calendar-table, #daily-table').slideUp('slow', function () {
			$('#add-event').slideDown();
			$('#calendar-buttons').slideUp('slow');
			$('#add-event-button').val('add').html('Add Event');
		});
		return false;
	}
	function hide_add_event_form() {
		$('#add-event').slideUp('slow', function () {$('#calendar-table, #daily-table').slideDown();$('#calendar-buttons').slideDown('slow');});
	}
	function show_event_tooltip(id) {
		var el = $('div.events[custom-id='+id+']');

		var html = '';
		var container = $('<div>');
		container.append($('<div>').prop('class', 'tooltip-event-title').html(el.attr('custom-title')));
		var allday = el.attr('custom-all-day');
		if (allday == 1) {
			container.append($('<div>').prop('class', 'tooltip-event-time').html('All Day Event'));
		} else {
			container.append($('<div>').prop('class', 'tooltip-event-time').html('Time: ' + el.attr('custom-start-time') + '-' + el.attr('custom-end-time')));
		};
		var desc = el.attr('custom-description');
		if (desc != '') {
			container.append($('<div>').prop('class', 'tooltip-event-description').html(el.attr('custom-description')));
		};
		html += container.html();
		tooltip.show(html);
	};
<?php }; ?>
</script>
