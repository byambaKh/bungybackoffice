<?php if ($edit_events) { ?>
<div id="add-event">
<form method="post">
<?php
    // hide filter variables ifisset
    if (isset($_GET['filter'])) {
        foreach ($_GET['filter'] as $f) {
            echo "<input type='hidden' name='filter[]' value='$f'>";
        };
    };
?>
<div id="form-title">Add new event</div>
<input type="hidden" name="id" value="">
<label for="event-title">Title</label>
<input name="event_title" class="new-event" type="text">
<br />
<label for="event-date">Date</label>
<input name="event_date" class="new-event date-selection" type="text">
<br />
<?php if (CURRENT_SITE_ID == 0) { ?>
<label for="event-date">Site</label>
<?php echo draw_pull_down_menu('site_id', $sites); ?>
<br />
<?php }; ?>
<label for="all_day">Full day event</label>
<input type="checkbox" name="all_day" value="1" id="all-day-event">
<br />
<div id="finish-date-selection">
<label for="event-finish-date">Finish Date</label>
<input name="event_finish_date" class="new-event date-selection" type="text">
<br />
</div>
<div id="time-selection">
<label for="event-date">Start time</label>
<?php echo draw_pull_down_menu('event_start', $values['start'], '', 'type="text"'); ?>
<br />
<label for="event-date">End time</label>
<?php echo draw_pull_down_menu('event_end', $values['finish'], '', 'type="text"'); ?>
</div>
<div id="color-selection">
<label for="color">Event Color</label>
<?php echo draw_pull_down_menu('color', BJHelper::getEventColorsDropDown(), '', 'id="color"'); ?>
<br />
</div>
<label for="event-description">Description</label>
<textarea rows="10" cols="30" name="event_description"></textarea>
<div id="add-event-buttons-area">
	<button id="add-event-button" name="action" value="add" type="submit"><span>Add event</span></button>
	<button id="delete-event-button" name="action" value="delete" type="submit"><span>Delete event</span></button>
	<button id="close-event-form" name="action" value="cancel" onClick="hide_add_event_form();return false;">Close form</button>
</div>
</form>
</div>
<?php }; ?>
