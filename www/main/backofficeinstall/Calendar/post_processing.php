<?php
    $action = '';
    if (array_key_exists('action', $_POST)) {
        $action = $_POST['action'];
    };

if ($edit_events) {
	switch ($action) {
		case ('delete'):
			$sql = "DELETE FROM calendar_events WHERE id='{$_POST['id']}'";
			$res = mysql_query($sql) or die(mysql_error());
			
            $query_str = '';
			$query_str = '?date=' . $_POST['event_date'];
            if (isset($_GET['filter']) && is_array($_GET['filter'])) {
                $query_str = '&' . implode('&', array_map(function ($item) {return 'filter[]=' . $item;}, $_GET['filter']));
            };
            Header("Location: /Calendar/$rurl" . $query_str);
			break;
		case ('update'):
		case ('add'):
			$data = array(
				'title'				=> mb_convert_encoding($_POST['event_title'], 'SJIS', 'UTF-8'),
				'event_date'		=> $_POST['event_date'],
				'event_finish_date'	=> (!empty($_POST['event_finish_date']) && $_POST['all_day'] == 1 && $_POST['event_finish_date'] > $_POST['event_date']) ? $_POST['event_finish_date'] : $_POST['event_date'],
				'all_day_event'		=> (int)$_POST['all_day'],
				'description'		=> mb_convert_encoding($_POST['event_description'], 'SJIS', 'UTF-8'),
				'start_time'		=> $_POST['event_start'],
				'end_time'			=> $_POST['event_end']
			);
			//if (CURRENT_SITE_ID == 0 && !empty($_POST['color'])) {
				$data['color'] = $_POST['color'];
			//};
			if (CURRENT_SITE_ID == 0 && !empty($_POST['site_id'])) {
				$data['site_id'] = $_POST['site_id'];
			} else {
				$data['site_id'] = CURRENT_SITE_ID;
			};
			$db_action = 'insert';
			$where = '';
			if ($action == 'update') {
				$where = 'id='.(int)$_POST['id'];
				$db_action = 'update';
			};
			db_perform('calendar_events', $data, $db_action, $where);
			$query_str = '?date=' . $_POST['event_date'];
			if (isset($_GET['filter']) && is_array($_GET['filter'])) {
				$query_str = '&' . implode('&', array_map(function ($item) {return 'filter[]=' . $item;}, $_GET['filter']));
			};
			Header("Location: /Calendar/$rurl" . $query_str);
			break;
	};
};
