<?php
    // places selection
    $places = BJHelper::getPlaces();
    $allowed_sites = array();
    foreach ($places as $p) {
        if ($p['subdomain'] != 'main' && $p['hidden'] == 1) continue;
        //if ($p['active'] == 0) continue;
        if ($user->hasPermission('Calendar', $p['id'])) {
			// bypass head office for other roles
			if ($p['subdomain'] == 'main' && !$user->hasRole(array('SysAdmin', 'Site Manager', 'General Manager'))) continue;
            $allowed_sites[] = array(
                'id'    => $p['subdomain'],
                'text'  => $p['name'],
                'subdomain' => $p['subdomain']
            );
        };
    };
    if (count($allowed_sites) > 1) {
		$sql = "SELECT * from users WHERE UserID = '{$_SESSION['uid']}';";
		$res = mysql_query($sql) or die(mysql_error());
		if ($user = mysql_fetch_array($res)) {
			$query_string = array();
			foreach ($_GET as $k => $v) {
				$query_string[] = $k . '=' . rawurlencode($v);
			};
			if (!empty($query_string)) {
				$query_string = '?' . implode("&", $query_string);
			} else {
				$query_string = '';
			};
?>
    <div id="switch-site">
        <form style="display: inline-block;" method="post">
        Switch Site: <?php echo draw_pull_down_menu('target', $allowed_sites, strtolower(SYSTEM_SUBDOMAIN), 'id="target"'); ?>
        </form>
    </div>
<script>
	var bjuser = '<?php echo $user['UserName']; ?>';
	var bjpass = '<?php echo $user['UserPwd']; ?>';
	$(document).ready(function () {
		$('#target').change(function () {
			var form = $('#switch-site > form');
			form.append($('<input type=hidden name=auth_user>').val(bjuser));
			form.append($('<input type=hidden name=auth_pass>').val(bjpass));
			form.prop('action', 'http://' + $(this).children(':selected').attr('subdomain') + '.bungyjapan.com' + '<?php echo $PHP_SELF . $query_string; ?>');
			form.submit();
		});
	});
</script>
<?php
		};
    };
?>
