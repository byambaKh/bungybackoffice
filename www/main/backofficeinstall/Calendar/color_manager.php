<?php
	include '../includes/application_top.php';

	if (CURRENT_SITE_ID != 0) {
		Header("Location: /Calendar/");
		die();
	};

	$edit_events = false;
	$user = new BJUser();
	if ($user->hasRole('Site Manager') || $user->hasRole('General Manager') || $user->hasRole('SysAdmin')) {
		$edit_events = true;
	};
// GET processing
	if (isset($_GET['action'])) {
		$where = '';
		$db_action  = 'insert';
		switch ($_GET['action']) {
			case 'update':
				$where = 'id=' . $_GET['id'];
				$db_action = 'update';
			case 'add':
				$data = array(
					'color'	=> $_GET['color'],
					'title'	=> $_GET['title']
				);
				db_perform('calendar_event_colours', $data, $db_action, $where);
				break;
			case 'delete':
				$sql = "DELETE FROM calendar_event_colours WHERE id = '{$_GET['id']}';";
				mysql_query($sql);
				break;
		};
		Header('Location: color_manager.php');
		die();
	};
// EOF GET processing

// select all sites
	$sites = BJHelper::getPlacesDropDown();
// EOF sites
	// load color config settings
	$scolors = BJHelper::getSiteEventColors();
	$ecolors = BJHelper::getEventColors();
	// EOF load color

	$colors = array(0, 8, 15);
	$c = array();
	for ($i = 0; $i < 3; $i++) {
		for ($j=0; $j < 3; $j++) {
			for ($k=0; $k < 3; $k++) {
				$c[] = sprintf("%X%X%X", $colors[$i], $colors[$j], $colors[$k]);
			};
		};
	};
	$avail_colors = $c;

?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<title><?php echo SYSTEM_SUBDOMAIN; ?> SITE CALENDAR - Colour Settings</title>
<?php include "../includes/head_scripts.php"; ?>
	<script language="javascript" src="/js/tooltip.js"></script>

<style>
#main-container {
	width: 302px;
	margin-left: auto;
	margin-right: auto;
}
#header {
	width: 302px;
	border: 2px solid black;
	background-color: red;
	margin-top: 20px;
	margin-bottom: 20px;
	margin-left: auto;
	margin-right: auto;
}
#header h1 {
	margin: 0px;
	padding: 0px;
	font-size: 20px;
	text-align: center;
}
.color-list {
	margin-top: 2px;
	margin-bottom: 2px;
	display: block;
}
.color-item, .color-item-change, .color-item-delete {
	width: 100px;
	margin: 0px;
	padding: 0px;
	float: left;
	display: block;
	text-align: center;
}
.color-item {
	border: 1px solid black;
}
.color-item-site {
	border: 1px solid black;
	width: 300px;
	text-align: center;
}
.color-item input, 
.color-item-change input, 
.color-item-delete input,
.color-item button, 
.color-item-change button, 
.color-item-delete button  {
	width: 80%;
	margin-left: 10%;
	margin-right: 10%;
}
.clear {
	clear : both;
}
</style>
<script>
$(document).ready(function () {
	$('#close').click(function () {
		window.opener.location.reload();
		window.opener.focus();
		window.close();
	});
	$('#color').change(function () {
		$(this).parent().css('background-color', '#' + $(this).val());
		$(this).css('background-color', '#' + $(this).val());
	});
	$('#color').change();
	$('.delete').click(function () {
		if (window.confirm("Are you sure to delete this event colour?")) {
			document.location = 'color_manager.php?action=delete&id=' + $(this).attr('custom-id');
		};
	});
	$('.add').click(function () {
		document.location = 'color_manager.php?action=add&color=' + $('#color').val() + '&title=' + $('#title').val();
	});
	$('.change').unbind('click');
	$('.change').click(change_handler);
});	
function change_handler() {
		$('#color-change').each(function () {
			$(this).parent().html($(this).parent().attr('custom-title'));
			$(".update").removeClass('update').addClass('change').html('Change');
			$('.change').unbind('click');
			$('.change').click(change_handler);
		});
		var dd = $("#color").parent().html();
		var ci = $(this).parent().parent().children('.color-item');
		ci.html(dd);
		ci.children('select').attr('id', 'color-change');
		ci.children('select').val(ci.attr('custom-color')).css('background-color', '#' + ci.attr('custom-color'));
		$(this).removeClass('change').addClass('update');
		$(this).html('Update');
		$('#color-change').change(function () {
			$(this).parent().css('background-color', '#' + $(this).val());
			$(this).css('background-color', '#' + $(this).val());
		});
		$('.update').unbind('click');
		$('.update').click(update_handler);
	}
function update_handler() {
	document.location = 'color_manager.php?action=update' + 
		'&color=' + $('#color-change').val() + 
		'&id=' + $(this).attr('custom-id') + 
		'&title=' + $(this).parent().parent().children('.color-item').attr('custom-title');
}
</script>
</head>
<body>
<div id="main-container">
	<div id="header"><h1>Event Colour Settings</h1></div>
<?php
	foreach ($scolors as $pid => $color) {
		if ($pid == 0) continue;
?>
	<div class="color-list">
		<div class="color-item-site" style="background-color: <?php echo $color; ?> !important;<?php echo ($color == '#080') ? ' color: white !important;' : ''; ?>"><?php echo BJHelper::getPlaceName($pid); ?></div>
	</div>
<?php
	};
?>
	<br class="clear" />
<?php
	foreach ($ecolors as $color) {
?>
	<div class="color-list">
		<div class="color-item" style="background-color: #<?php echo $color['color']; ?> !important;" custom-color="<?php echo $color['color']; ?>" custom-title="<?php echo $color['title']; ?>"><?php echo $color['title']; ?></div>
		<div class="color-item-change"><button class="change" custom-id="<?php echo $color['id']; ?>">Change</button></div>
		<div class="color-item-delete"><button class="delete" custom-id="<?php echo $color['id']; ?>">Delete</button></div>
		<br class="clear" />
	</div>
<?php
	};
?>
	<br class="clear" />
	<div class="color-new">
		<div class="color-item">
			<select name='color' id='color'>
<?php 
	foreach ($avail_colors as $color) {
		echo "<option value='$color' style='background-color: #$color !important;'>#$color</option>";
	};
?>
			</select>
		</div>
		<div class="color-item-change"><input type="text" name="title" value='' id="title"></div>
		<div class="color-item-delete"><button class="add">Add</button></div>
		<br class="clear" />
	</div>
	<br class="clear" />
<button id="close">Close</button>
	<br class="clear" />
</div>
	<br class="clear" />
<?php include ("ticker.php"); ?>
</body>
</html>
