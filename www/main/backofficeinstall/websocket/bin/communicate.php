<?php
use Ratchet\Server\IoServer;
use Ratchet\Http\HttpServer;
use Ratchet\WebSocket\WsServer;
use BungyJapan\Chat;

require dirname(__DIR__) . '/vendor/autoload.php';
date_default_timezone_set("Asia/Tokyo");
error_reporting(E_ALL & ~E_DEPRECATED & ~E_NOTICE);
ini_set('display_errors', 'On');

try {
    $server = IoServer::factory(
        new HttpServer(
            new WsServer(
                new Chat()
            )
        ),
        8080
    );

	$server->run();
} catch (\React\Socket\ConnectionException $e) {
	echo "The server is already running on port 8080\n";
}
