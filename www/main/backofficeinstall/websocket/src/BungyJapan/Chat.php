<?php
namespace BungyJapan;

//use BungyJapan\Jcm;
use Ratchet\MessageComponentInterface;
use Ratchet\ConnectionInterface;

class Chat implements MessageComponentInterface
{
    protected $clients;//splObject of $clientsAndConnection
    protected $machines;//
    protected $outputFile;//

    //this will parse the message and send it out
    private function logMessage($text)
    {
        fwrite($this->outputFile, date('Y-m-d H:i:s - ') . "$text\n");
    }

    private function sendMessageToAllConnected($msg)
    {
        $this->logMessage($msg);

        foreach ($this->clients as $client) {
            // The sender is not the receiver, send to each client connected
            $client->send($msg);
            echo "Sending to $client->resourceId: $msg\n";

        }
    }

    private function sendMessageToSender($from, $msg)
    {
        $this->logMessage($msg);

        foreach ($this->clients as $client) {
            if ($from !== $client) {
                // The sender is not the receiver, send to each client connected
                $client->send($msg);
            }
        }
    }

    public function __construct()
    {
        $this->clients = new \SplObjectStorage;

        $path = dirname(dirname(dirname(dirname(__FILE__)))) . "/uploads/websocketlog.txt";
        $this->outputFile = fopen($path, "a+");
        $this->logMessage(date('Y-m-d H:i:s') . " Websocket Server Started");
    }

    public function onOpen(ConnectionInterface $conn)
    {
        // Store the new connection to send messages to later
        $this->clients->attach($conn);

        $openMessage = "A new client has connected ({$conn->resourceId})";
        $this->logMessage($openMessage);
    }

    public function onMessage(ConnectionInterface $from, $msg)
    {
        $this->logMessage($msg);
        $numRecv = count($this->clients) - 1;
        echo sprintf('Connection %d sending message "%s" to %d other connection%s' . "\n"
            , $from->resourceId, $msg, $numRecv, $numRecv == 1 ? '' : 's');

        //TODO http://socketo.me/docs/push check out this and zmq
        //TODO Decrypt message
        $messageArray = json_decode($msg, true);
        if ($messageArray == NULL) return; //return if a message is invalid JSON to prevent crashes

        //If the message is to the server, perform the action required
        //Else broadcast it to all of the machines

        //ipad-client -> server
        //server -> ipad-client

        //machine -> server
        //server -> machine

        //if the any of these block, messages will be received by the server once
        //the function returns
        //check that the message is an array
        if ($messageArray['to'] == 'server') {
            if ($messageArray['device'] == 'scales') {

            } else if ($messageArray['device'] == 'cashMachine') {
                //call the jcm.php and have it send a message back
                $jcm = new Jcm();
                $commandParts = explode(" ", $messageArray['message']);//split the command in to parts
                $command = $commandParts[1];
                $host = $commandParts[0];

                $commandOutput = $jcm->command($messageArray['message']);//run the create transaction command

                if (in_array($command, ["transaction_create_cash", "transaction_create_coupon", "transaction_create_payout"])) {

                    $transactionOutput = $jcm->transaction_listen_new();

                    $returnMessage = [
                        'from'              => 'server',
                        'to'                => $host,
                        'message'           => $transactionOutput,
                        'device'            => 'cashMachine',
                        'messageIdentifier' => $messageArray['messageIdentifier']
                    ];

                    $this->sendMessageToAllConnected(json_encode($returnMessage));
                    $messageArray['device'] = 'browser';//rebroadcast the message but set to browser so that the webpage can update
                    $this->sendMessageToAllConnected(json_encode($messageArray));

                } else {

                    $returnMessage = [
                        'from'              => 'server',
                        'to'                => $host,
                        'message'           => $commandOutput,
                        'device'            => 'cashMachine',
                        'messageIdentifier' => $messageArray['messageIdentifier']
                    ];

                    $this->sendMessageToAllConnected(json_encode($returnMessage));
                    $messageArray['device'] = 'browser';//rebroadcast the message but set to browser so that the webpage can update
                    $this->sendMessageToAllConnected(json_encode($messageArray));
                }

                //TODO send return message back to the JCM
                //TODO Message the browser with an update
                //server to machine name device = browser

                //None of the functions will block the for long
                //create an array of machines indexed by name
                echo '';

            } else if ($messageArray['device'] == 'browser') {
                //call the jcm.php and have it send a message back
                $jcm = new Jcm();
                $commandParts = explode(" ", $messageArray['message']);//split the command in to parts
                $command = $commandParts[1];
                $host = $commandParts[0];

                if (in_array($command, ["booking_check_if_locked_and_paid", "lock_booking", "unlock_booking", "set_booking_paid"])) {
                    $commandOutput = $jcm->command($messageArray['message']);

                    $returnMessage = [
                        'from'              => 'server',
                        'to'                => $host,
                        'message'           => $commandOutput,
                        'device'            => 'browser',
                        'messageIdentifier' => $messageArray['messageIdentifier']
                    ];

                    $this->sendMessageToAllConnected(json_encode($returnMessage));
                    //$messageArray['device'] = 'browser';//rebroadcast the message but set to browser so that the webpage can update
                    //$this->sendMessageToAllConnected(json_encode($messageArray));

                }
            } else if ($messageArray['device'] == 'certificatePrinter') { //will not happen

            } else if ($messageArray['device'] == 'ticketPrinter') { //will not happen

            } else if ($messageArray['device'] == 'wristbandPrinter') { //will not happen

            } else if ($messageArray['device'] == 'iPadClient') { //

            } else if ($messageArray['device'] == '') {

            }

        } else {//Think about the potential vulnerabilities of this
            $this->sendMessageToAllConnected($msg);
            //broadcast it to everyone
        }

        //$this->sendMessageToAllConnected($msg);
        //message from the java running on the machine -> server
        //
        //if it is a message to the cash machine
        //call the jcm.php script. transaction_listen_new is not relevant anymore
        //if message from the scales reader
        //broadcast it

    }

    public function onClose(ConnectionInterface $conn)
    {
        // The connection is closed, remove it, as we can no longer send it messages
        $this->clients->detach($conn);
        $closeMessage = "Connection {$conn->resourceId} has disconnected\n";
        $this->logMessage($closeMessage);
    }

    public function onError(ConnectionInterface $conn, \Exception $e)
    {
        $errorMessage = "An error has occurred: {$e->getMessage()}";
        $this->logMessage($errorMessage);
        echo $errorMessage . "\n";

        $conn->close();
    }
}
