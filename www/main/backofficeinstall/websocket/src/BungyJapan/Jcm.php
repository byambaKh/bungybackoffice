<?
namespace BungyJapan;
Class Jcm
{
    private $hostname;
    private $mysql;
    private $host;
    private $logDirectory;

    function __construct()
    {
        //$this->logDirectory = "/Users/bungyjapan/Developer/bungyjapan/jcm.log";
        //$this->mysql = new MySQL("bungyjapan_book_main", "root", "root", "localhost");
        $this->mysql = new MySQL("bungyjapan_book_main", "backofficesystem", "61a560add5c43f41b9403820a488edbc", "localhost:3307");
    }

    function set_booking_paid($args)
    {
        $bookingId = $args[0];
        $query = "UPDATE customerregs1 SET selfCheckInPaid = 1 WHERE CustomerRegID = $bookingId OR GroupBooking = $bookingId";
        $this->mysql->executeQuery($query);

        return $this->booking_check_if_locked_and_paid($args);
    }

    function lock_booking($args)
    {
        $bookingId = $args[0];
        $query = "UPDATE customerregs1 SET isLocked = 1 WHERE CustomerRegID = $bookingId";
        $this->mysql->executeQuery($query);
        $message = ['function' => __FUNCTION__, 'booking_lock' => true, 'bookingId' => $bookingId];

        //return $message;
        return $this->booking_check_if_locked_and_paid($args);

    }

    /**
     * Check if the booking is currently locked by another jcm. This prevents multiple payments for the same booking
     * when it is a group or combo booking that is all being paid at once.
     *
     * @param $bookingId
     *
     * @return array if locked, false if not.
     */
    function booking_check_if_locked_and_paid($args)
    {
        $bookingId = $args[0];
        $locked = $this->booking_check_if_locked($bookingId);
        $paid = $this->booking_check_if_paid($bookingId);

        $message = ['function' => __FUNCTION__, 'booking_lock' => $locked, 'booking_paid' => $paid, 'bookingId' => $bookingId];

        return $message;

    }

    function booking_check_if_locked($bookingId)
    {
        $query = "SELECT customerregid FROM customerregs1 WHERE CustomerRegID = $bookingId AND isLocked= 1";
        $count = $this->mysql->countRows($query);

        if ($count) {
            return true;
        } else {
            return false;
        }

    }

    function booking_check_if_paid($bookingId)
    {
        $query = "SELECT customerregid FROM customerregs1 WHERE CustomerRegID = $bookingId AND selfCheckInPaid = 1";
        $count = $this->mysql->countRows($query);

        if ($count) {
            return true;
        } else {
            return false;
        }

    }

    function unlock_booking($args)
    {
        $bookingId = $args[0];
        $query = "UPDATE customerregs1 SET isLocked = 0 WHERE CustomerRegID = $bookingId";
        $this->mysql->executeQuery($query);
        //$message = ['function' => __FUNCTION__, 'booking_lock' => false, 'bookingId' => $bookingId];
        //skip checking as these queries take too long
        //return $this->booking_check_locked($bookingId);
        return $this->booking_check_if_locked_and_paid($args);
        //return $message;
    }

    function command($commandString)
    {
        //$this->logDirectory = "/Users/bungyjapan/Developer/bungyjapan/jcm.log";
        //$this->mysql = new MySQL("bungyjapan_book_main", "root", "root", "localhost");

        //$this->logDirectory = "/home/bungyjapan/private/jcm/jcm.log";
        //$this->mysql = new MySQL("bungyjapan_book_main", "root", "root", "localhost");
        $this->mysql = new MySQL("bungyjapan_book_main", "backofficesystem", "61a560add5c43f41b9403820a488edbc", "localhost:3307");
        $argv = explode(" ", $commandString);
        //array_shift($argv);//dump the name of the script as we dont need it

        file_put_contents($this->logDirectory, $this->now() . " > " . implode(" ", $argv) . "\n", FILE_APPEND);

        //get rid of this
        $this->hostname = array_shift($argv);
        $this->host = $this->mysql->fetchSelectorRow("jcm_host", array('hostname' => $this->hostname));
        $methodName = array_shift($argv);

        if (method_exists($this, $methodName)) {
            //$output = call_user_func([$this, $methodName], $argv);
            $output = $this->$methodName($argv);    //call the value method named in command
            return $output;
        }
    }

    function now()
    {
        return date("Y-m-d H:i:s");
    }

    function host_config()
    {
        return $this->output("OK", array($this->host['port']));
    }

    function output($status, $data = null)
    {
        $o = array();
        $o[] = $status;
        if (is_array($data)) {
            $o = array_merge($o, $data);
        } else if (strlen($data)) {
            $o[] = $data;
        }
        //file_put_contents($this->logDirectory, $this->now() . " < " . implode(",", $o) . "\n", FILE_APPEND);
        $output = implode(",", $o);//this prints to the console for the jcm to see
        return $output;
    }

    function host_recycler_setcount($args)
    {
        // [0] = Count of bills in Box 1
        // [1] = Count of bills in Box 2
        $this->mysql->updateRows("jcm_host", array('box1' => $args[0], 'box2' => $args[1]), array('id' => $this->host['id']));

        return $this->output("OK");
    }

    function host_recycler_getcount()
    {
        // [0] = Count of bills in Box 1
        // [1] = Count of bills in Box 2
        //return $this->mysql->fetchSelectorRow("jcm_transaction", array('box1' => $this->host['box1'], 'box2' => $this->host['box2']));
        return ("OK,{$this->host['box1']},{$this->host['box2']}");
        //$this->mysql->fetchSelectorRow('jcm_host', ['hostname' => '']);
        //return $this->output("OK");
    }

    function transaction_create_cash($args)
    {
        // [0] = Total amount of transaction
        $this->mysql->executeQuery("UPDATE `jcm_transaction` SET `state` = 'FAILED' WHERE `host` = {$this->host['id']} AND `state` NOT IN ('COMPLETED','CANCELLED')");
        $id = $this->mysql->insertRow("jcm_transaction", array('created' => $this->now(), 'host' => $this->host['id'], 'state' => "NEW", 'type' => "CASH", 'total' => $args[0]));

        return $this->output("OK", $id);
    }

    function transaction_create_coupon()
    {
        $this->mysql->executeQuery("UPDATE `jcm_transaction` SET `state` = 'FAILED' WHERE `host` = {$this->host['id']} AND `state` NOT IN ('COMPLETED','CANCELLED')");
        $id = $this->mysql->insertRow("jcm_transaction", array('created' => $this->now(), 'host' => $this->host['id'], 'state' => "NEW", 'type' => "COUPON"));

        return $this->output("OK", $id);
    }

    function transaction_create_payout($args)
    {
        // [0] = Total amount of payout (NOTE: this should be negative)
        $this->mysql->executeQuery("UPDATE `jcm_transaction` SET `state` = 'FAILED' WHERE `host` = {$this->host['id']} AND `state` NOT IN ('COMPLETED','CANCELLED')");
        $id = $this->mysql->insertRow("jcm_transaction", array('created' => $this->now(), 'host' => $this->host['id'], 'state' => "NEW", 'type' => "PAYOUT", 'total' => $args[0]));

        return $this->output("OK", $id);
    }

    function transaction_listen_new()
    {
        while (true) {
            $trans = $this->mysql->fetchSelectorRow("jcm_transaction", array('host' => $this->host['id'], 'state' => "NEW"));
            if ($trans['id']) {
                $this->mysql->updateRows("jcm_transaction", array('state' => "STARTED"), array('id' => $trans['id']));

                return $this->output("OK", $trans);
                break;
            }
            sleep(1); // TODO: Change this to notify events instead.
        }
    }

    function transaction_check_balance($args)
    {
        // [0] = Transaction ID
        $trans = $this->transaction($args[0]);

        return $this->output("OK", $trans['balance']);
    }

    function transaction($id)
    {
        return $this->mysql->fetchSelectorRow("jcm_transaction", array('id' => $id, 'host' => $this->host['id']));
    }

    function transaction_check_remaining($args)
    {
        // [0] = Transaction ID
        $trans = $this->transaction($args[0]);

        return $this->output("OK", $trans['total'] - $trans['balance']);
    }

    function transaction_check_cancel($args)
    {
        // [0] = Transaction ID
        $trans = $this->transaction($args[0]);
        if ($trans['state'] == "CANCEL") {
            return $this->output("CANCEL");
        } else {
            return $this->output("OK");
        }
    }

    function transaction_cancel($args)
    {
        // [0] = Transaction ID
        $this->mysql->updateRows("jcm_transaction", array('state' => "CANCEL"), array('id' => $args[0]));

        return $this->output("OK");
    }

    function event_cancelled($args)
    {
        // [0] = Transaction ID
        $trans = $this->transaction($args[0]);
        $this->event($trans, "CANCELLED");
        $this->mysql->updateRows("jcm_transaction", array('state' => "CANCELLED"), array('id' => $trans['id']));

        return $this->output("OK");
    }

    function event($trans, $type, $data = null)
    {
        return $this->mysql->insertRow("jcm_event", array('created' => $this->now(), 'host' => $this->host['id'], 'transaction' => $trans['id'], 'type' => $type, 'data' => $data));
    }

    function event_accept_cash($args)
    {
        // [0] = Transaction ID
        // [1] = Bill Amount
        $trans = $this->transaction($args[0]);
        $bill = $args[1];
        $this->event($trans, "ACCEPT_CASH", $bill);
        if ($trans['balance'] < $trans['total']) {
            $this->mysql->updateRows("jcm_transaction", array('balance' => $trans['balance'] + $bill), array('id' => $trans['id']));
            $trans = $this->transaction($trans['id']);
            if ($trans['balance'] < $trans['total']) {
                return $this->output("CONTINUE");
            } elseif ($trans['balance'] == $trans['total']) {
                $this->mysql->updateRows("jcm_transaction", array('state' => "COMPLETED"), array('id' => $trans['id']));

                return $this->output("STOP");
            } elseif ($trans['balance'] > $trans['total']) {
                $this->mysql->updateRows("jcm_transaction", array('state' => "CHANGE"), array('id' => $trans['id']));

                return $this->output("PAYOUT", array($trans['balance'] - $trans['total']));
            }
        } else {
            return $this->output("REJECT");
        }
    }

    function event_payout($args)
    {
        // [0] = Transaction ID
        // [1] = Payout Bill
        $trans = $this->transaction($args[0]);
        $bill = $args[1];
        $this->event($trans, "PAYOUT", $bill);
        $this->mysql->updateRows("jcm_transaction", array('balance' => $trans['balance'] - $bill), array('id' => $trans['id']));
        $trans = $this->transaction($trans['id']);
        if ($trans['type'] == "CASH" && $trans['state'] == "CHANGE" && $trans['balance'] == $trans['total']) {
            $this->mysql->updateRows("jcm_transaction", array('state' => "COMPLETED"), array('id' => $trans['id']));
        } else if ($trans['type'] == "PAYOUT" && $trans['state'] == "STARTED" && $trans['balance'] == $trans['total']) {
            $this->mysql->updateRows("jcm_transaction", array('state' => "COMPLETED"), array('id' => $trans['id']));
        }

        return $this->output("OK");
    }

    function event_accept_coupon($args)
    {
        // [0] = Transaction ID
        // [1] = Coupon Code
        $trans = $this->transaction($args[0]);
        $coupon = $args[1];
        $this->event($trans, "ACCEPT_COUPON", $coupon);
        if (true) { // TODO: Check here for coupon validity
            return $this->output("IDLE");
        } else {
            return $this->output("REJECT");
        }
    }
}

require_once('MySQL.php');
require_once('MySQLIterator.php');

//$jcm = new Jcm();
//echo $jcm->command('MINA01 host_recycler_getcount');
//$jcm->command('MINA01 transaction 25');

//echo '';
//error_reporting(E_ALL);
//ini_set('display_errors', 'On');
//$test = new Jcm();
//$test->command("MINA01 lock_booking 1");

/*
echo $test->booking_check_locked(63122);//0
echo $test->booking_lock(63122);//1
echo $test->booking_unlock(63122);//0
*/


