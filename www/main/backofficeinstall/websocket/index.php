<!doctype html>
<html>
<head>
    <title>WebSocket</title>
	<meta charset="UTF-8">
    <style type="text/css">
        #log {
            width:600px; 
            height:300px; 
            border:1px solid #7F9DB9; 
            overflow:auto;
            padding:10px;
        }
        #msg {
            width:300px;
        }
    </style>
	<script src="//cdn.jsdelivr.net/sockjs/1.0.0/sockjs.min.js"></script>
     
    <script type="text/javascript">
    var socket;
 
    function init() {
        //var host = "ws://localhost:8080/chat"; // SET THIS TO YOUR SERVER
        var host = "ws://minakami.bungyjapan.com:8080"; // SET THIS TO YOUR SERVER
         
        try
        {
            //socket = new SockJS(host);
            socket = new WebSocket(host);
            log('WebSocket - status ' + socket.readyState);
            //log('WebSocket - status ' + socket.readyState);
             
            socket.onopen = function(msg) 
            { 
                if(this.readyState == 1)
                {
                    log("We are now connected to websocket server. readyState = " + this.readyState); 
                }
            };
             
            //Message received from websocket server
            socket.onmessage = function(msg) 
            { 
                log(" [ + ] Received: " + msg.data); 
            };
             
            //Connection closed
            socket.onclose = function(event) 
            { 
                log("Disconnected - status " + this.readyState); 

				    var reason;
					alert(event.code);
					// See http://tools.ietf.org/html/rfc6455#section-7.4.1
					if (event.code == 1000)
						reason = "Normal closure, meaning that the purpose for which the connection was established has been fulfilled.";
					else if(event.code == 1001)
						reason = "An endpoint is \"going away\", such as a server going down or a browser having navigated away from a page.";
					else if(event.code == 1002)
						reason = "An endpoint is terminating the connection due to a protocol error";
					else if(event.code == 1003)
						reason = "An endpoint is terminating the connection because it has received a type of data it cannot accept (e.g., an endpoint that understands only text data MAY send this if it receives a binary message).";
					else if(event.code == 1004)
						reason = "Reserved. The specific meaning might be defined in the future.";
					else if(event.code == 1005)
						reason = "No status code was actually present.";
					else if(event.code == 1006)
					   reason = "The connection was closed abnormally, e.g., without sending or receiving a Close control frame";
					else if(event.code == 1007)
						reason = "An endpoint is terminating the connection because it has received data within a message that was not consistent with the type of the message (e.g., non-UTF-8 [http://tools.ietf.org/html/rfc3629] data within a text message).";
					else if(event.code == 1008)
						reason = "An endpoint is terminating the connection because it has received a message that \"violates its policy\". This reason is given either if there is no other sutible reason, or if there is a need to hide specific details about the policy.";
					else if(event.code == 1009)
					   reason = "An endpoint is terminating the connection because it has received a message that is too big for it to process.";
					else if(event.code == 1010) // Note that this status code is not used by the server, because it can fail the WebSocket handshake instead.
						reason = "An endpoint (client) is terminating the connection because it has expected the server to negotiate one or more extension, but the server didn't return them in the response message of the WebSocket handshake. <br /> Specifically, the extensions that are needed are: " + event.reason;
					else if(event.code == 1011)
						reason = "A server is terminating the connection because it encountered an unexpected condition that prevented it from fulfilling the request.";
					else if(event.code == 1015)
						reason = "The connection was closed due to a failure to perform a TLS handshake (e.g., the server certificate can't be verified).";
					else
						reason = "Unknown reason";
					console.log(event.code + ": " + reason);

					//$("#thingsThatHappened").html($("#thingsThatHappened").html() + "<br />" + "The connection was closed for reason: " + reason);
            };
             
            socket.onerror = function()
            {
                log("Some error");
            }
        }
         
        catch(ex)
        { 
            log('Some exception : '  + ex); 
        }
         
        $("msg").focus();
    }
 
    function send()
    {
        var txt, msg;
        txt = $("msg");
        msg = txt.value;
         
        if(!msg) 
        { 
            alert("Message can not be empty"); 
            return; 
        }
         
        txt.value="";
        txt.focus();
         
        try
        { 
            socket.send(msg); 
            log('Sent : ' + msg); 
        } 
        catch(ex) 
        { 
            log(ex); 
        }
    }
     
    function quit()
    {
        if (socket != null) 
        {
            log("Goodbye!");
            socket.close();
            socket=null;
        }
    }
 
    function reconnect() 
    {
        quit();
        init();
    }
 
    // Utilities
    function $(id)
    { 
        return document.getElementById(id); 
    }
     
    function log(msg)
    { 
        $('log').innerHTML += '<br />' + msg; 
        $('log').scrollTop = $('log').scrollHeight;
    }
     
    function onkey(event)
    { 
        if(event.keyCode==13)
        { 
            send(); 
        } 
    }
    </script>
 
</head>
 
<body onload="init()">
 
    <h3>WebSocket</h3>
 
    <div id="log"></div>
 
    Enter Message <input id="msg" type="textbox" onkeypress="onkey(event)"/>
 
    <button onclick="send()">Send</button>
    <button onclick="quit()">Quit</button>
    <button onclick="reconnect()">Reconnect</button>
 
</body>
</html>
