<?
 include "../includes/application_top.php";
$_SESSION['startDate1'] = $_POST['startDate1'];
$_SESSION['endDate1'] = $_POST['endDate1'];
?>

<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">
<link href="css/style.css" rel="stylesheet" media="screen" type="text/css" />

<link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/themes/base/jquery-ui.css" type="text/css" media="all" />
<link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all" />
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script>
<script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/jquery-ui.min.js" type="text/javascript"></script>
<script src="js/functions.js" type="text/javascript">



</script>


<title>Logged in</title>
<style type="text/css">

#d100 {
    display: none;
}

</style>
<script type="text/javascript" charset="utf-8">
function OnSubmitForm() {  
  if(document.pressed == 'Make Booking') {
    document.lsform.action ="agentBooking.php";
    document.lsform.submit();
    return true;      
  }
  else if (document.pressed == 'View') {
    document.lsform.action ="agentChangeRate.php";
    document.lsform.submit();
    return true;    
  }
  
  return false;    
}
function procs(){
  var sval = document.getElementById('startDate1');
  var eval = document.getElementById('endDate1');
  //alert(sval);
  if(sval.value == ''){
    alert("Please select Start Date");
    return false;
  }
  if(eval.value == ''){
    alert("Please select End Date");
    return false;
  }
  
  document.lsform.action ="agentInvoice.php";
  document.lsform.submit();
}
function toggle(id) {
  var state = document.getElementById(id).style.display;
  if (state == 'block') {
    document.getElementById(id).style.display = 'none';
  } else {
    document.getElementById(id).style.display = 'block';
  }
}

function processLogout(){
  if(document.getElementById('logout')){
    document.lsform.action ="?logout=true";
    document.lsform.submit();
    return true;
  }
}
function procMonthInvoice(month){
	  var d = new Date();
      document.lsform.action = "../AgentInvoice/agent_invoice.php?date="+d.getFullYear() + '-' + month;
      document.lsform.submit();
      return true;
}
$(function() {
  $mydate1 = $('#startDate1').datepicker({
    numberOfMonths: 1,
    showCurrentAtPos: 0,    
    dateFormat: 'yy-mm-dd',
    altField: '#alternate',
    altFormat: 'yy-mm-dd',
    maxDate: new Date(<?php echo $maxDate?>)
  });
  $mydate1 = $('#endDate1').datepicker({
    numberOfMonths: 1,
    showCurrentAtPos: 0,    
    dateFormat: 'yy-mm-dd',
    altField: '#alternate',
    altFormat: 'yy-mm-dd',
    maxDate: new Date(2011, 11 , 31)
  });
});
</script>
</head>
<body>
<br>
<form method="post" name="lsform" onsubmit="return OnSubmitForm();">
<input type="hidden" name="startDate1" value="<?php print $_POST['startDate1'];?>">
<input type="hidden" name="endDate1" value="<?php print $_POST['endDate1'];?>">  
 <table border="0" align="center" width="420px" >
    <tbody>
       <tr></tr><tr></tr>
      <tr align="center" bgcolor="#000000" >
        <td colspan="2"><font color="#FFFFFF"><b>Please select actvities from below:</b></font></td>
      </tr>
      <tr  bgcolor="#FF0000">
        <td align="right" ><font color="#FFFFFF">Make a Booking:</font></td>
        <td align="center">
          <input style="WIDTH: 150px; HEIGHT: 25px; background-color:#FFFF66" name="upload" id="upload" value="Make Booking" onclick="document.pressed=this.value" size="19" type="submit">
        </td>
      </tr>       
    <tr>
       <td bgcolor="#FF0000" align="right"><font color="#FFFFFF">View Invoice:</font></td>
      <td bgcolor="#FF0000" align="center">
          <!-- <input style="WIDTH: 150px; HEIGHT: 25px; background-color:#FFFF66" id="delete" value="View Invoice" onclick="document.pressed=this.value" type="submit"> -->
          <input style="WIDTH: 150px; HEIGHT: 25px; 
                background-color:#FFFF66" 
                id="inv" 
                value="Invoice Date Range" 
                size="19" type="submit" onclick="toggle('d100');">
        </td>

    </tr>
      <tr  bgcolor="#FF0000">
        <td align="right"><font color="#FFFFFF">View Agent Rate & Packages:</font></td>
        <td align="center">
          <input style="WIDTH: 150px; HEIGHT: 25px; background-color:#FFFF66" id="download" value="View" onclick="document.pressed=this.value" size="19" type="submit">
          
        </td>
      </tr>
    </tbody>
  </table>  
<br>
 <table border="0" align="center" width="420px" >
    <tbody>
       <tr>
         <td align="right">
           <input type="button" style="width:76;background-color:#FFFF66" name="logout" id="logout" value="Logout" onclick="processLogout();">
         </td>
       </tr>
      
    </tbody>
  </table>    
<div id="d100">

<table  width="420px" align="center" bgcolor="#FF0000" border="0">
  <tbody>
    <tr> 
      <td bgcolor="#000000" colspan="6" align="center">
        <font color="#FFFFFF" size="2">CLICK ON RESPECTIVE MONTH TO GENERATE INVOICE</font>
      </td>
    </tr>
    <tr>
<?php 
	for ($i = 1; $i <=12; $i++) { 
		$t = mktime(0, 0, 0, $i, 15, date("Y"));
		$m = strtoupper(date("M", $t));
?>
      <td align="center">
        <input type="button" name="<?php echo $m; ?>" id="<?php echo $m; ?>" value="<?php echo $m; ?>" onclick="procMonthInvoice('<?php printf("%02d", $i); ?>');">
      </td>
<?php 
		if ($i == 6) {
?>
    </tr>
    <tr>
<?php
		};
	}; 
?>
    </tr>
    <!--<tr>
      <td align="right"><font color="#FFFFFF">Start Date:</font></td>
      <td>
        <input type="text" class="bigtext" name="startDate1" id="startDate1" size="12" readonly="readonly"/>
      </td>
      <td>  
      <font color="#FFFFFF">End Date:</font>  
      </td>
      <td>
        <input type="text" class="bigtext" name="endDate1" id="endDate1" size="12" readonly="readonly"/>
      </td>
    </tr>
    
    
    <tr>
      <td colspan="4" align="right">
        <input type="button" name="generate" id="generate" value="Generate Invoice" onclick="procs();">
      </td>
    </tr>
    -->
  </tbody>
  
</table>

</div>

</form>
</body>

</html>
