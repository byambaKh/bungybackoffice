<?php
?>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=9">
<title>Test AJAX</title>
<script type="text/javascript">
function ajaxFunction(jumptime){  
  var loaderphp = "load.php";  
  
  var xmlHttp;  
  try{    
    xmlHttp=new XMLHttpRequest();    
  }catch(e){    
  try  {      
    xmlHttp=new ActiveXObject("Msxml2.XMLHTTP");    
  }catch(e){      
  try  {        

    xmlHttp=new ActiveXObject("Microsoft.XMLHTTP");      
  }catch(e){        
    alert("Your browser does not support AJAX!");        
    return false;      
    }    
  }  
}   
xmlHttp.onreadystatechange=function(){    
  if(xmlHttp.readyState==4){      
    document.getElementById('jumpno').innerHTML = xmlHttp.responseText;        
  }  
}    
  xmlHttp.open("GET", loaderphp+"?jumptime="+jumptime,true);    
  xmlHttp.send(null);
}

</script>
</head>
<body>
<form action="" method="post">
Select:
  <select name="jumptime" id="jumptime" onchange="ajaxFunction(this.value)">
    <option value='07.00 AM'>07.00 AM</option>
    <option value='07.30 AM'>07.30 AM</option>
  </select>
  <select name="jumpno" id="jumpno" >
  
  </select>
</form>  
</body>
</html>
