<?php
 include "../includes/application_top.php";
?>
<!DOCTYPE html>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">
<link href="css/style.css" rel="stylesheet" media="screen" type="text/css" />
<title>Agent Help</title>
<style>
ul {
	list-style: none;
}
p, table {
	margin-left: 100px;
}
ul ul {
	margin-left: 150px;
}
.more-space {
	margin-left: 28px;
}
table {
	border-collapse: collapse;
}
table th {
	border: 2px solid black;
}
table td {
	vertical-align: bottom;
	padding-left: 5px;
}
</style>
</head>
<body>
<br>
<h1>予約作成	</h1>

<ul>
	<li>1）予約日を選択<br>
<p>
左上の”日付”をクリックしてカレンダーが表示されたら予約日を選択してください。
</p>
	</li>
	<li>2）予約時間を選択
<p>
３０分間に６人まで。各時間帯の隣の（　）に空き人数が表示されています。<br>
ご希望の時間に空きが足りない場合、次の時間へ残りの人数を入力してください。<br>
時間帯が分かれてご予約されているグループの場合、全員の集合時間は一番早い時間となります。
<br><br>
注意点：グループ人数の半分以上が希望時間に空きがなければ、待ち時間が発生しますので他の時間帯を選択してください。
</p>
	</li>
	<li>3）予約の人数を選択。
<p>
選択した時間帯に表示されている数字は、空き人数のみです。
</p>
	</li>
	<li>４）バンジー単価を選択
		<ul>
			<li>①一番高い値段がバンジー卸値  [初期値設定されています]</li>
			<li>②一番安い値段が当日二回目ジャンプ料金</li>
			<li>③その間の値段がリピーター料金　※要認定書+身分証明書提示された方</li>
			<li>④バンジー無料クーポン券を持っている方は、"０"を選択してください。</li>
		</ul>
<p>
		備考：同じ予約で違う料金で参加する場合は、他の欄も使って人数・料金設定をしてください。									
</p>
	</li>
	<li>5）支払い場所を選択
		<ul>
			<li>Bungy = バンジージャパンにてお支払い</li>
			<li>Agent ＝御社にてお支払い</li>
		</ul>
		<br>
	</li>
	<li>６）バンジーがお預かり金額を入力（必要な場合）
		<p>
「支払い所場」が "Agent" の場合は、入力する必要はありませんので灰色に表示されます。<br>
「支払い場所」が "Bungy" の場合は、バンジージャパンが御社の為にお預かりする金額を入力してください。<br>
		</p>
	</li>
	<li>７）代表者様の姓字入力（ローマ字で） <br><br></li>
	<li>８）代表者様の名前入力（ローマ字で） <br><br></li>
	<li>９）代表者様の当日連絡がとれる携帯番号入力（数字のみ）<br><br></li>
	<li>１０）備考欄
		<ul>
			<li>例）	ラフティングコンボ＠集合時間</li>
			<li class='more-space'>キャニオニングコンボ＠13：00　翌日</li>
		</ul>
		<br>
	<li>１１）ご予約内容にお間違えがなければ、右下　<b>[予約登録]</b>　ボタンをクリックすると、ご予約内容確認のページが表示されます。<br><br>
	</li>
	<li>１２）メインメニューへ戻るには左下 <b>[ << 戻る ] </b>ボタンをクリックしてください。<br><br>
	</li>
	<li><u>予約確認</u>
<p>
	<table id='s11'>
		<tr>
			<th>場所</th>
			<td colspan="2">バンジージャンプする場所</td>
		</tr>
		<tr>
			<th>日付 </th>
			<td colspan="2">ご予約日</td>
		</tr>
		<tr>
			<th>時間</th>
			<td colspan="2">ご予約時間 </td>
		</tr>
		<tr>
			<th>名前 </th>
			<td colspan="2">代表者様のフルネーム </td>
		</tr>
		<tr>
			<th>人数 </th>
			<td colspan="2">バンジージャンプする人数 </td>
		</tr>
		<tr>
			<th>単価 </th>
			<td colspan="2">バンジージャンプする１人分の値段 </td>
		</tr>
		<tr>
			<th>支払い場所 </th>
			<td>料金お支払い場所：</td>
			<td rowspan="2">バンジージャパンにてお支払いの場合は”Bungy”と表示されます。<br>
御社にてお支払いの場合は”Agent”と表示されます。</td>

		</tr>
		<tr>
			<td>&nbsp;</td>
			<td>&nbsp;</td>
		</tr>
		<tr>
			<th>バンジーが<br>
お預かり金額
</th>
			<td colspan="2">バンジージャパンが御社の為にお預かりする金額：</td>
		</tr>
		<tr>
			<th>備考 </th>
			<td colspan="2">この予約について連絡事項がある場合は、メッセージを入力してください。 </td>
		</tr>
	</table>
</p>
	</li>
	<li>メインメニューへ戻るには左下 <b>[ << 戻る ]</b> ボタンをクリックしてください。<br><br></li>

	<li>続けて他のご予約するには右下<b>[ 新規予約作成 ]</b>ボタンをクリックしてください。<br><br></li>


	<li><u>月間別請求明細<br><br></u></li>

	<li>１）請求内容を月間別で確認する。<br><br></li>

	<li>※バンジージャパンへチェックインしたお客様のみのデータが反映されます。
<p>
	<table>
	<tr>
		<th>場所</th><td>バンジージャンプされた場所</td>
	</tr>
	<tr>
		<th>日付</th><td>ご予約日</td>
	</tr>
	<tr>
		<th>時間</th><td>ご予約時間</td>
	</tr>
	<tr>
		<th>名前</th><td>代表者様のフルネーム</td>
	</tr>
	<tr>
		<th>人数</th><td>バンジージャンプした人数</td>
	</tr>
	<tr>
		<th>単価</th><td>バンジージャンプする１人分の値段</td>
	</tr>
	<tr>
		<th>御受取金額</th><td>バンジャージャパンがお預りした合計金額</td>
	</tr>
	<tr>
		<th>御支払金額</th><td>御社がお預かりされた合計金額</td>
	</tr>
	<tr>
		<th>今回合計金額</th><td>御支払金額と御受取金額の差額分を差し引いた合計金額</td>
	</tr>

	</table>
</p>
	</li>
	<li>メインメニューへ戻るには左下 <b>[ << 戻る ]</b>ボタンをクリックしてください。
	<br><br><br></li>


<li><u>パスワード変更</u><br><br></li>

<li>1）前回迄のパスワードを入力</li>
<li>2）新しいパスワードを入力</li>
<li>3）新しいパスワードを再入力</li>
<li>4）右下「登録」ボタンをクリック<br><br></li>

<li>メインメニューへ戻るには左下 <b>[ << 戻る ]</b>ボタンをクリックしてください。<br><br><br></li>


<li><u>ログアウトする</u><br><br></li>

<li>ログイン状態を解除する。<br><br><br></li>


<li><u>ヘルプ</u><br><br></li>

<li>ご不明点がございましたら参考にしてください。<br><br></li>

<li>ご予約内容のご変更・キャンセル・お問い合わせは下記へお問い合わせください。<br></li>
<li>尚、お電話による対応時間は午前9時から午後6時迄となりますので、予めご了承下さい。<br></li>
<li>電話番号：　0278-72-8133<br><br></li>

<li>また、ご予約内容のご変更・キャンセル・お問い合わせはメールでも承っております。<br></li>
<li>メールによる返答は24時間以内とさせていただきますので、予めご了承ください。<br></li>
<br>
<li>ご希望のサイトにお送りください：<br></li>
<li>minakami@bungyjapan.com<br></li>
<li>sarugakyo@bungyjapan.com<br></li>
<li>ryujin@bungyjapan.com<br></li>
<li>itsuki@bungyjapan.com<br></li>
<li><br><br></li>
<li><button onClick="document.location='/';"><< 戻る</button></li>
</ul>
</body>
</html>
