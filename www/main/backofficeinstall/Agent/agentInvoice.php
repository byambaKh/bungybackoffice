<?php
include ("../includes/application_top.php");

  $_SESSION['startDate1'] = $_GET['startDate1'];
  $_SESSION['endDate1'] = $_GET['endDate1'];


function display_db_query($sql_select, $conn, $header_bool, $table_params) {
  $result_select = mysql_query($sql_select, $conn)
  or die("display_db_query:" . mysql_error());

  // find out the number of columns in result
  $column_count = mysql_num_fields($result_select)  
  or die("display_db_query:" . mysql_error());
  
  $num_rows = mysql_num_rows($result_select);
  if($num_rows != 0){

  // Here the table attributes from the $table_params variable are added
  //print("<div id = 'dv100'>");
  //print ("<br>");
  print("<TABLE $table_params width='80%' align='left'>");
  // optionally print a bold header at top of table
  if($header_bool) {
    
    print("<TR>");
    for($column_num = 0; $column_num < $column_count; $column_num++) {
      $field_name = mysql_field_name($result_select, $column_num);
      print("<TH bgcolor='#87CEFA'>$field_name</TH>");
    }
    print("<TH bgcolor='#F9B287'>Total Amount&nbsp;&yen;(Jump x Rate)</TH>");
    print("</TR>");
  }
  if($num_rows == 0){
     
  }else{  
    while($row = mysql_fetch_row($result_select)) {
      $amount = $row[4]*$row[5];
      print("<TR ALIGN=CENTER VALIGN=TOP>");
        for($column_num = 0; $column_num < $column_count; $column_num++) {        
            print("<TD valign='middle' >$row[$column_num]</TD>");
              
        }
        print("<TD valign='middle' >&yen;&nbsp;$amount</TD>");  
      print("</TR>");
      $tamount = $tamount + $amount;
    }
    print("<TR ALIGN=CENTER VALIGN=TOP>");
    print("<TD valign='middle' align='right' colspan='6'><b>Grand Total</b></TD>");
    print("<TD valign='middle' align='center' colspan='1'><b>&yen;&nbsp;$tamount</B</TD>");
  }
  print("</TABLE>");
  //print("</div>"); 
}else {
  echo "<center><h2>INVOICE for this month in not available!!!Please search again.</h2></center>";
  }
}
function display_db_table($tablename, $conn, $header_bool, $table_params) {
  
  
  echo "<h3>INVOICE Date Between: ".$_SESSION['startDate1']." and ".$_SESSION['endDate1']."</h3>";

  $sql_select = "SELECT bookingdate as 'Booking Date', bookingtime as Time,
        UPPER(customerlastname) as 'Last Name', UPPER(customerfirstname) as 'First Name', noofjump as 'Jump No', rate as Rate
        FROM customerregs1
        WHERE bookingdate between '".$_SESSION['startDate1']."'"." AND '".$_SESSION['endDate1']."'".
        " AND agent = '".$_SESSION['myusername']."' ORDER BY bookingdate asc";

  display_db_query($sql_select, $conn, $header_bool, $table_params);

}
//session_destroy();
?>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<title>Agent Invoice</title>
<script type="text/javascript">
function processBack(){
  if(document.getElementById('back')){
    <?php 
      $_GET['startDate1'] = null;
      $_GET['endDate1'] = null;
    ?>
    document.agtf.action = "agentActivities.php";
    document.agtf.submit();
  }
}
</script>
</head>
<body>
<form action="" method="post" name="agtf">
<table width="80%" align="left">
<tbody>
  <tr>
    <td>
      <h3>INVOICE for Agent:&nbsp;<?php echo strtoupper($_SESSION['myusername']);?></h3>           
    </td>    
  </tr>
  <tr>
    <td align="left">
      <?php
        $table = "table1";
        display_db_table($table, $conn, TRUE, "border='1'");
      ?>             
    </td>
  </tr>
  <tr>
    <td colspan="5" align="center">
      <INPUT type="button" value="Back" name="bk" id="back" onClick="processBack();"> 
      <INPUT type="button" value="Close Window" onClick="window.close()"> 
    </td>

  </tr>
</tbody>
</table>

</form>
</body>
</html>
