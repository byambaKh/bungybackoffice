<?php
require '../includes/application_top.php';
// If we have gbid passed via GET - edit redirection
if (isset($_GET['gbid'])) {
    // fill needed values
    $gbid = (int)$_GET['gbid'];
    if ($gbid != 0) {
        $regid = $gbid;
        $gbooking = getBookingInfo($gbid);
        $grpJumper = 0;
        foreach ($gbooking as $row) {
            $grpJumper += $row['NoOfJump'];
            $defaults[] = array(
                "cregid"    => $row['CustomerRegID'],
                "jumpsno"   => $row['NoOfJump'],
                "rate"      => $row['Rate'],
                "ratetopay" => $row['RateToPay'],
                "splitTime" => $row['BookingTime']
            );
        };
        $jumpDate = $gbooking[0]['BookingDate'];
        $chkInTime = $gbooking[0]['BookingTime'];
        $agent = $gbooking[0]['Agent'];
        $lastname = $gbooking[0]['CustomerLastName'];
        $firstname = $gbooking[0]['CustomerFirstName'];
        $contactno = $gbooking[0]['ContactNo'];
        $modeTransport = $gbooking[0]['TransportMode'];
        $notes = $gbooking[0]['Notes'];
        $collect = $gbooking[0]['CollectPay'];
        $submit_title = "Update";
    };
} else {
    // EOF
    if ($_POST['grpJumpers'] == '') {
        $grpJumper = 0;
    } else {
        $grpJumper = $_POST['grpJumpers'];
    }
    $jumpDate = date("Y-m-d");
    $gbid = 0;
    $chkInTime = $_POST['chkInTime'];
    $defaults = array('', '', '', '');
    $agent = ($_POST['agentName'] == '') ? 'No agent' : $_POST['agentName'];
    $lastname = $_POST['lastname'];
    $firstname = $_POST['firstname'];
    $contactno = $_POST['contactno'];
    $modeTransport = $_POST['modeTransport'];
    if (empty($modeTransport)) $modeTransport = "Car";
    $notes = $_POST['notes'];
    $collect = '';
    $submit_title = $lang == 'en' ? "Book Now" : "予約登録";
}
if (isset($_GET['baction']) && $_GET['baction'] == 'cancel') {
    $submit_title = "Cancel";
};
$times_schedule = getBookTimes($jumpDate, $gbid);
$bdate = $jumpDate;

$agent = $_SESSION['myusername'];
$bDates = getCalendarState();
$bDates = array_unique(array_merge($bDates, getFullDates()));

// Get all user subdomains
$user = new BJUser();
$places = $user->getAgentPlaces();
$agent_places = array();
$current_site_name = ucfirst(strtolower(SYSTEM_DATABASE_INFO));
foreach ($places as $p) {
    // get default price for subdomain
    $sql = "select `value` from configuration WHERE site_id = '{$p['id']}' AND `key` = 'default_agent_price'";
    $res = mysql_query($sql) or die(mysql_error());
    $price = $config['first_jump_rate'];
    if ($row = mysql_fetch_assoc($res)) {
        $price = $row['value'];
    };
    $agent_places[] = array(
        'id'    => $p['subdomain'],
        'text'  => ($lang == 'en') ? $p['name'] : $p['name_jp'],
        'price' => $price
    );
};
$current_place = ucfirst($places[0]['id']);

?>
<!DOCTYPE html>
<html>
<head>
    <?php include '../includes/header_tags.php'; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=Shift_JIS">
    <meta http-equiv="X-UA-Compatible" content="IE=8">
    <title><?php echo ($lang == 'en') ? 'Agent Booking' : '予約表 '; ?></title>

    <!-- head scripts-->
    <?php include '../includes/head_scripts.php'; ?>
    <!-- end head scripts-->
    <style>
        .timeSelect {
            width: 100px;
        }

        .jumpSelect {
            width: 40px;
        }

        .row-header {
            background-color: red;
            text-align: center;
            color: white;
        }

        .booking-date {
            text-align: center;
        }

        .unavailable, .unavailable * {
            background-color: #FAA !important;
        }
    </style>

    <?php $max_jumps = 7; ?>
    <!-- bookings.php-->
    <?php include 'bookings.php'; ?>
    <!-- end bookings.php-->
    <script type="text/javascript" charset="utf-8">
        var repeater_rate = <?php echo ($config['repeater_rate'] > 0) ? $config['repeater_rate'] : 'null'; ?>;
        var second_jump_rate = <?php echo ($config['second_jump_rate'] > 0) ? $config['second_jump_rate'] : 'null'; ?>;
        var agent_price = <?php echo $config['default_agent_price']; ?>;
        var agent_name = "<?php echo $_SESSION['myusername'];?>";

        function processSubmit() {
            var jump_selected = false;
            $("#splitJump0, #splitJump1, #splitJump2, #splitJump3").each(function () {
                if ($(this).val() > 0) {
                    jump_selected = true;
                }
                ;
            });
            if (!jump_selected) {
                alert('Please select the No. of Jumps');
                return false;
            }
            ;
            var nre = /^([\d]*)$/;
            var ere = /^([a-zA-Z\s]*)$/;
            if (!nre.test(document.bForm.payment_for_agent.value)) {
                alert('Please enter only numerical values in the \'Payment rec\'d for Agent\' field.');
                document.bForm.payment_for_agent.focus();
                return false;
            }
            if (document.bForm.lastname.value == '' || !ere.test(document.bForm.lastname.value)) {
                alert('<?php echo ($lang == 'en') ? 'Please enter only English characters in the \'Last Name\' field.' : '代表者様の姓字をローマ字で入力してください。'; ?>');
                document.bForm.lastname.focus();
                console.log(document.bForm.lastname.value);
                return false;
            }
            if (document.bForm.firstname.value == '' || !ere.test(document.bForm.firstname.value)) {
                alert('<?php echo ($lang == 'en') ? 'Please enter only English characters in the \'First Name\' field.' : '代表者様の名前をローマ字で入力してください。'; ?>');
                document.bForm.firstname.focus();
                return false;
            }
            if (document.bForm.contactno.value == '' || !nre.test(document.bForm.contactno.value)) {
                alert('<?php echo ($lang == 'en') ? 'Please enter only numerical values in the \'Contact No\' field.' : '携帯番号をハイフンなしで数字のみ入力してください。';?>');
                document.bForm.contactno.focus();
                return false;
            }
            var repeater_selected = false;
            var second_selected = false;
            $("#rate0, #rate1, #rate2, #rate3").each(function () {
                if ($(this).val() == repeater_rate) {
                    repeater_selected = true;
                }
                ;
                if ($(this).val() == second_jump_rate) {
                    second_selected = true;
                }
                ;
            });
            if (repeater_selected) {
                window.alert('You have chosen the Repeat jumper price for for some jumps. Please inform the customer that they will need to show the Certificate from their previous jump and a piece of photo ID when checking in at Bungy Japan in order to qualify for the Repeater price.');
            }
            ;
            if (second_selected) {
                if (!window.confirm('You have chosen the 2nd jump price for some jumps. This rate is only available to a customer who plans to do more than one jump on the same day. If this is correct, please click [ OK ].')) {
                    return false;
                }
                ;
            }
            ;
            document.bForm.action = "agentBookingInsert.php";
            document.bForm.submit();
            return true;
        }
        function procBack() {
            document.location = '/';
        }
        function procBack1() {
            document.location = '/';
        }
        function triggerAgt() {
            // MisterSoft rates changes
            var price = 0;
            $('#agentName > option').each(function () {
                if ($(this).prop('selected') === true) {
                    price = $(this).attr('price');
                }
                ;
            });
            $('#rate0 > option, #rate1 > option, #rate2 > option, #rate3 > option').each(function () {
                if ($(this).html() == price) {
                    $(this).prop('selected', true);
                }
                ;
            });
            if ($('#agentName').val() == 'NULL' <?php echo (SYSTEM_SUBDOMAIN == 'MINAKAMI') ? "|| $('#agentName').val() == 'SG Bungy'" : ''; ?>) {
                $('#CollectPay0, #CollectPay1, #CollectPay2, #CollectPay3').val('Onsite');
            } else {
                $('#CollectPay0, #CollectPay1, #CollectPay2, #CollectPay3').val('Offsite');
            }
            // EOF MisterSoft
        }
        ;
        $(document).ready(function () {
            $mydate = $('#jumpDate').datepicker({
                showAnim: 'fade',
                numberOfMonths: 1,
                dateFormat: 'yy-mm-dd',
                currentText: 'Today',
                altField: '#alternate',
                altFormat: 'yy-mm-dd',
                minDate: '0d',
                maxDate: new Date(<?php echo $maxDate?>),
                beforeShowDay: checkAvailability,
                onSelect: getBookTimesTables
            });
            $("#pfa").attr('disabled', 'disabled');
            on_place_change();
            $('#CollectPay0, #CollectPay1, #CollectPay2, #CollectPay3').change(function () {
                var collect_possible = false;
                $('#CollectPay0, #CollectPay1, #CollectPay2, #CollectPay3').each(function () {
                    if ($(this).val() == 'Onsite') collect_possible = true;
                });
                if (collect_possible) {
                    $("#pfa").attr('disabled', false);
                } else {
                    $("#pfa").attr('disabled', 'disabled');
                }
                ;
            });


            <?php
                if (!isset($_GET['gbid'])) {
            ?>
            triggerAgt();
            <?php
                };
            ?>

        });
        //sets up the form with custom values depending on the agents name
        //called after the last ajax call.
        function customizeForAgent() {
            if (agent_name == "Media") {
                $("#lastname").val("FILMING");
                $("#rate0, #rate1, #rate2, #rate3").html("<option value='0'>0</option>");
                $("#rate0, #rate1, #rate2, #rate3").val("0");
                $("#CollectPay0, #CollectPay1, #CollectPay2, #CollectPay3").html("<option value='Offsite'>Agent</option>");
                $("#CollectPay0, #CollectPay1, #CollectPay2, #CollectPay3").val("Offsite");
                $("#pfa").attr("disabled", "disabled");
            }
        }

        function on_place_change() {
            getBadDates($("select[name=place]").val());
            getAgentPrices($("select[name=place]").val());
        }

        function getAgentPrices(place) {
            $.ajax({
                url: '../includes/ajax_helper.php',
                dataType: 'json',
                data: 'action=getAgentPrices&place=' + place,
                success: function (data) {
                    if (data['result'] == 'success') {
                        repeater_rate = data['repeater_rate'];
                        second_jump_rate = data['second_jump_rate'];
                        var prices = $("#rate0, #rate1, #rate2, #rate3");
                        prices.children("option").remove();
                        var price_value = 0;
                        for (var i in data['options']) {
                            if (!data['options'].hasOwnProperty(i)) continue;
                            var op = $('<option value="' + data['options'][i].id + '">' + data['options'][i].text + '</option>');
                            prices.append(op);
                            price_value = data['options'][i].id;
                        }
                        ;
                        prices.val(price_value);
                        customizeForAgent();
                    }
                    ;
                }
            });
        }
        function getBadDates(place) {
            $.ajax({
                url: '../includes/ajax_helper.php',
                dataType: 'json',
                data: 'action=getBadDates&place=' + place,
                success: function (data) {
                    $myBadDates = data['options'];
                    getBookTimesTables($("#jumpDate").val());
                }
            });
        }
        var $myBadDates = new Array(<?php
			foreach ($bDates as $disabledate) {
    			echo " \"$disabledate\",";
			} echo " 1";
			?>);

        function checkAvailability(mydate) {
            var $return = true;
            var $returnclass = "available";
            $checkdate = $.datepicker.formatDate('yy-mm-dd', mydate);
            for (var i = 0; i < $myBadDates.length; i++) {
                if ($myBadDates[i] == $checkdate) {
                    $return = false;
                    $returnclass = "unavailable";
                    return [$return, $returnclass, "Dates Locked"];
                }
            }
            return [$return, $returnclass];
        }
        function update_foc() {
            for (var i = 0; i < 4; i++) {
                var disabled = 'disabled';
                if ($('#rate' + i).val() == '0') {
                    disabled = false;
                } else {
                    $('#foc' + i).val('');
                }
                ;
                $('#foc' + i).attr('disabled', disabled);
            }
        }
    </script>
</head>

<body>
<form name="bForm" method="post">
    <input type="hidden" name="baction" value="<?php print isset($_GET['baction']) ? $_GET['baction'] : ''; ?>">
    <input type="hidden" name="bookingdate" value="<?php print $bdate; ?>">
    <input type="hidden" name="chkInTime" value="<?php print $chkInTime; ?>">
    <?php
    // add ids to form if it is edit form
    if (isset($gbooking) && !empty($gbooking)) {
        foreach ($gbooking as $num => $rec) {
            echo '<input type="hidden" name="gbid[]" value="' . $rec['CustomerRegID'] . '">' . "\n";
        }
    };
    // get default price per agent
    switch ($agent) {
        case 'NULL':
            $default_price = '7500';
            break;
        case 'Canyons':
        case 'Canyons Akagi':
            $default_price = '6500';
            break;
        default:
            $default_price = '7000';
    };
    // prepare price options
    ?>
    <table align="center" border="0" width="500px">
        <tbody>
        <tr>
            <td align="center" bgcolor="#000000" valign="middle" colspan="5">
                <h2><font color="#FFFFFF"><?php echo ($lang == 'en') ? 'Agent Booking' : '予約表 '; ?></font></h2>
            </td>

        </tr>

        <tr>
            <td bgcolor="#FF0000" align="center" colspan="3">
                <font color="#FFFFFF"><?php echo ($lang == 'en') ? 'Booking Date:' : '予約日'; ?> &nbsp;
                    <input type="text" class="booking-date" name="jumpDate" id="jumpDate" size="12" readonly="readonly"
                           value="<?php echo $jumpDate; ?>"></font>
            </td>
            <td bgcolor="#FF0000" colspan="2" align="right">
                <font color="#FFFFFF"><?php echo ($lang == 'en') ? 'Location:' : '場所'; ?>
                    <?php

                    if (count($places) >= 1) {
                        echo draw_pull_down_menu('place', $agent_places, null, "id='place' onChange='on_place_change();'");
                    } else {
                        echo $current_site_name;
                    };
                    ?>
                </font>
            </td>

        </tr>
        <tr class="row-header">
            <td><?php echo ($lang == 'en') ? 'Time' : '時間'; ?></td>
            <td><?php echo ($lang == 'en') ? 'No. of jumps' : '人数'; ?></td>
            <td><?php echo ($lang == 'en') ? 'Price' : 'バンジー単価'; ?></td>
            <td><?php echo ($lang == 'en') ? 'Payment Rec\'d' : '支払い場所'; ?></td>
            <td><?php echo ($lang == 'en') ? 'payment Rec\'d <br />for Agent' : 'バンジーが<br>お預かり金額'; ?></td>
        </tr>
        <?php
        for ($split = 0; $split < 4; $split++) {
            ?>
            <tr class="row-header">
                <td>
                    <select name='splitTime<?php echo $split; ?>' id='splitTime<?php echo $split; ?>' class="timeSelect">
                    </select>

                </td>
                <td>
                    <select name="splitJump<?php echo $split; ?>" id='splitJump<?php echo $split; ?>' class="jumpSelect">
                    </select>
                </td>
                <td>
                    <?php echo draw_pull_down_menu('rate' . $split, BJHelper::getAgentRatesList($current_place), $config['default_agent_price'], "id='rate$split'"); ?>
                </td>
                <td>
                    <select name="CollectPay<?php echo $split; ?>" id="CollectPay<?php echo $split; ?>">
                        <option<?php echo (isset($gbooking) && isset($gbooking[$split]) && $gbooking[$split]['CollectPay'] == 'Onsite') ? ' selected="selected"' : ''; ?> value="Onsite">Bungy</option>
                        <option<?php echo (isset($gbooking) && isset($gbooking[$split]) && $gbooking[$split]['CollectPay'] == 'Offsite') ? ' selected="selected"' : ''; ?> value="Offsite">Agent</option>
                    </select>
                </td>
                <?php if ($split == 0) { ?>
                    <td rowspan="4"><input type="text" name="payment_for_agent" value="" size=12 id="pfa"></td>
                <?php }; ?>
            </tr>
        <?php }; ?>



        <tr>
            <td bgcolor="#FF0000" align="right" colspan="3">
                <font color="#FFFFFF">*<?php echo ($lang == 'en') ? 'Last Name' : '代表者様の姓字'; ?>:&nbsp;</font>
                <input type="text" id="lastname" name="lastname" value="<?php print $lastname; ?>" style="text-transform: uppercase" size="15">
            </td>
            <td bgcolor="#FF0000" colspan="2" align="center">
                <font color="#FFFFFF"><?php echo ($lang == 'en') ? 'Agent' : 'エージェント'; ?>:&nbsp;
                    <input type="hidden" name="agentName" value="<?php echo $agent; ?>"><b><?php echo $agent; ?></b></font>
            </td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" align="right" colspan="3">
                <font color="#FFFFFF">*<?php echo ($lang == 'en') ? 'First Name' : '代表者様の名前'; ?>:&nbsp;</font>
                <input type="text" name="firstname" value="<?php print $firstname; ?>" style="text-transform: uppercase" size="15">
            </td>
            <td bgcolor="#FF0000" align="right" colspan="1">
                <font color="#FFFFFF"><?php echo ($lang == 'en') ? 'Contact No' : '代表者様の<br>
携帯番号'; ?>:</font>&nbsp;
            </td>
            <td bgcolor="#FF0000" align="right" colspan="1">
                <input type="text" name="contactno" size="15" value="<?php print $contactno; ?>">
            </td>
        </tr>
        <tr>
            <td bgcolor="#FF0000" align="center" colspan="5" valign="top">
                <font color="#FFFFFF"><?php echo ($lang == 'en') ? 'Notes' : '備考'; ?>:</font>
                <textarea rows="3" cols="35" name="notes"><?php echo $notes; ?></textarea>
            </td>
        </tr>
        </tbody>
    </table>
    <table align="center" border="0" width="496px" cellspacing="0" cellpadding="2">
        <tr>
            <td bgcolor="#000000" align="left" colspan="2">
                <input type="button" style="width:76;background-color:#FFFF66" name="operation" id="back" value="<?php echo ($lang == 'en') ? 'Back' : '<< 戻る'; ?>" onclick="procBack1();"/>
            </td>
            <td bgcolor="#000000" align="right" colspan="2">
                <input type="button" style="width:76;background-color:#FFFF66" name="operation" id="sbmt" value="<?php echo $submit_title; ?>" onclick="processSubmit();"/>
            </td>
        </tr>
    </table>

</form>
</body>
</html>
<?php
include("ticker.php");
?>
