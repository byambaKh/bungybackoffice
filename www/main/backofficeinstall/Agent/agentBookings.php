<?
include "../includes/application_top.php";

$user = new BJUser();
$places = $user->getAgentPlaces();
$pcount = 0;
$agent_places = array();
$agent_places[] = array(
    'id'   => 'all',
    'text' => ($lang == 'en') ? 'All' : '全部'
);
$current_site_name = ($lang == 'en') ? 'All' : '全部';
$current_site_id = null;
$all_places = array();
foreach ($places as $p) {
    $pcount++;
    $agent_places[] = array(
        'id'   => $p['subdomain'],
        'text' => ($lang == 'en') ? $p['name'] : mb_convert_encoding($p['name_jp'], 'UTF-8', 'Shift_JIS')
    );
    $all_places[$p['id']] = $p['subdomain'];
    $place_domains[] = $p['subdomain'];
    if (isset($_GET['filter_site']) && $_GET['filter_site'] == $p['subdomain']) {
        $current_site_name = ucfirst($p['subdomain']);
        $current_site_id = $p['id'];
    };
};
if (count($places) == 1) {
    $current_site_name = ucfirst($places[0]['subdomain']);
    $current_site_id = $places[0]['id'];
};
if (!is_null($current_site_id)) {
    $add_where = " and site_id = $current_site_id ";
} elseif (!empty($places)) {
    $add_where = " and site_id in (" . implode(', ', array_map(function ($item) {
            return $item['id'];
        }, $places)) . ")";
} else {
    $add_where = '';
};
$cols = 0;
if (!isset($_GET['filter_site']) || $_GET['filter_site'] == 'all') {
    $cols = 1;
};

?>
<!DOCTYPE html>
<html>
<head>
    <?php include 'includes/header_tags.php'; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=8">

    <?php include 'includes/head_scripts.php'; ?>


    <title><?php echo ($lang == 'en') ? 'Agent Bookings' : '予約表'; ?></title>
    <style>
        table {
            border-collapse: collapse;
        }

        .page-header td {
            background-color: black;
            padding: 5px;
        }

        .table-header td {
            text-align: center;
            color: white;
            background-color: black;
            font-weight: bold;
            font-size: 11pt;
            font: Arial;
            border-left: 1px solid white;
            border-right: 1px solid white;
            white-space: nowrap;
        }

        .table-row {
            background-color: black;
        }

        .table-row td {
            border: 1px solid black;
            text-align: center;
            color: white;
            background-color: red;
            font-weight: bold;
            font: Arial;
            font-size: 10pt;
        }

        .table-row td.name {
            text-align: left;
            padding-left: 3px;
        }

        form {
            display: inline;
        }

        button {
            float: left;
            background-color: yellow;
            border: 1px solid black;
        }

        #make {
            float: right !important;
        }
    </style>
    <script>
        $(document).ready(function () {
            $("#back").click(function () {
                document.location = '/';
            });
            $("#make").click(function () {
                document.location = '/Agent/agentBooking.php';
            });
        });
    </script>
</head>
<body>
<br>
<table border="0" align="center" width="780px">
    <tbody>
    <tr class="page-header">
        <td colspan="<?php echo 6 + $cols; ?>" align="left">
            <font color="#FFFFFF"><b>&nbsp;<?php echo $current_site_name; ?> <?php echo ($lang == 'en') ? 'Agent Bookings' : '予約表'; ?></b></font>
        </td>
        <td colspan="2" align="right">
            <?php if (count($places) != 1) { ?>
                <font color="#FFFFFF"><?php echo ($lang == 'en') ? 'Filter by' : '検索フィルター'; ?>:
                    <form><?php echo draw_pull_down_menu('filter_site', $agent_places, $_GET['filter_site'], "onChange='this.form.submit();'"); ?></form>
                </font>
            <?php } else { ?>
                &nbsp;
            <?php }; ?>
        </td>
    </tr>
    <tr bgcolor="#FFFFFF">
        <td colspan="<?php echo 8 + $cols; ?>">&nbsp;</td>
    </tr>
    <tr class="table-header">
        <?php if ($cols) { ?>
            <td><?php echo ($lang == 'en') ? 'Site' : '場所'; ?></td>
        <?php }; ?>
        <td><?php echo ($lang == 'en') ? 'Date' : '予約日 '; ?></td>
        <td><?php echo ($lang == 'en') ? 'Time' : '時間'; ?></td>
        <td><?php echo ($lang == 'en') ? 'Name' : '代表者名'; ?></td>
        <td><?php echo ($lang == 'en') ? 'No. of<br />Jumps' : '人数'; ?></td>
        <td><?php echo ($lang == 'en') ? 'Price' : 'バンジー単価'; ?></td>
        <td><?php echo ($lang == 'en') ? 'Payment<br />Received?' : '支払い場所'; ?></td>
        <td><?php echo ($lang == 'en') ? 'Collected<br />for Agent' : 'バンジーが<br> お預かり金額'; ?></td>
        <td><?php echo ($lang == 'en') ? 'Notes' : '備考'; ?></td>
    </tr>
    <?php
    $today_date = date("Y-m-d");
    $agent = $_SESSION['myusername'];
    $sql = "SELECT * FROM customerregs1
		WHERE 
			BookingDate >= '$today_date' 
			$add_where 
			and deleteStatus = 0
			and Agent = '$agent'
		ORDER BY BookingDate ASC, BookingTime ASC;";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        $date_received = str_replace('-', '/', substr($row['BookingReceived'], 0, 10));
        ?>
        <tr class="table-row">
            <?php if ($cols) { ?>
                <td><?php echo ucfirst($all_places[$row['site_id']]); ?></td>
            <?php }; ?>
            <td><?php echo $row['BookingDate']; ?></td>
            <td><?php echo $row['BookingTime']; ?></td>
            <td class='name'><?php echo $row['RomajiName']; ?></td>
            <td><?php echo $row['NoOfJump']; ?></td>
            <td><?php echo $row['Rate']; ?></td>
            <td><?php echo ($row['CollectPay'] == 'Onsite') ? 'Bungy' : 'Agent'; ?></td>
            <td><?php echo ($row['RateToPay'] * $row['RateToPayQTY'] > 0) ? $row['RateToPay'] * $row['RateToPayQTY'] : 0; ?></td>
            <td><?php echo mb_convert_encoding($row['Notes'], 'UTF-8', 'Shift_JIS'); //"Agent ($date_received)"; ?></td>
        </tr>
    <?php
    };
    ?>
    <tr bgcolor="#FFFFFF">
        <td colspan="<?php echo 8 + $cols; ?>">&nbsp;</td>
    </tr>
    <tr bgcolor="#FFFFFF">
        <td colspan="<?php echo 8 + $cols; ?>">
            <button id="back"><?php echo ($lang == 'en') ? 'Back' : '<< 戻る'; ?></button>
            <button id="make"><?php echo ($lang == 'en') ? 'Make another booking' : '新規予約作成'; ?></button>
        </td>
    </tr>
    </tbody>
</table>

</div>

</form>
</body>

</html>
