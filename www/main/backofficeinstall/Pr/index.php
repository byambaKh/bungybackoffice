<?php
require_once('../includes/application_top.php');

$siteIds = [1, 2, 3, 4, 10, 12];
$publicRelationCollection = [];
$dateTime = null;

if(isset($_GET['date'])) {
    $dateString = $_GET['date'];

    if (preg_match('/^[1-2][0-9][0-9][0-9]-[0-1][0-9]$/', $dateString)) { //date of format 2014-12
        //$dateTime = DateTime::createFromFormat("Y-m", $dateString);//For the date 2015-09 dateTime returns the October 2015-10-01
        //it looks like a bug in PHP
        $dateTime = DateTime::createFromFormat("Y-m-d", $dateString."-01");

    } else if (preg_match('/^[1-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]$/', $dateString)) {//date of format 2014-12-01
        $dateTime = DateTime::createFromFormat("Y-m-d", $dateString);

    }
}

//if time is empty or not a valid date time string assume it is now
//or if the user is not an admin only show today
if($dateTime == null) $dateTime = new DateTime('now');

//Calculate the dates for the next and previous pages
//Most of this logic can be wrapped up in to a class/set of functions and be reused for other pages
$dateTime->setDate($dateTime->format('Y'), $dateTime->format('m'), 1);
//--------Create Strings for the URL next and previous--------------------//
$dateTimeBefore = clone $dateTime;
$dateTimeAfter  = clone $dateTime;

$dateTimeBefore ->modify("-1 month");
$dateTimeAfter  ->modify("+1 month");

$datePageTitleString = $dateTime->format('F Y');
$datetimeMonthString = $dateTime->format('Y-m');
$dateAfterUrlString = $dateTimeAfter->format('Y-m');//the date string for the link to the preceding page
$dateBeforeUrlString = $dateTimeBefore->format('Y-m');//the date string for the link to the next page



if(isset($_POST['report'])) {
    PublicRelationsAnalysis::save($_POST['report']);
}

foreach($siteIds as $siteId)
{
   $publicRelationCollection[] = new PublicRelationsAnalysis($datetimeMonthString, $siteId);
}

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Public Relations</title>
    <?php include "../includes/head_scripts.php"; ?>
    <link type="text/css" href="../css/bootstrap/3.3.6/css/bootstrap.css" rel="stylesheet"/>
    <link type="text/css" href="css/publicrelations_php.css" rel="stylesheet"/>
<script>
    $(document).ready(function () {
        $("#back").click(function () {
            document.location = '/Analysis/?action=analysis';
        });
        $("#home").click(function () {
            document.location = '/';
        });
    });
</script>
</head>
<body>
<h1>Public Relations Analysis</h1>
<div id="container">
    <div class="month-advance">
        <a href="?date=<?=$dateBeforeUrlString?>">&lt;&lt;&lt;</a>
        <?=$datePageTitleString ?>
        <a href="?date=<?=$dateAfterUrlString?>">&gt;&gt;&gt;</a>
    </div>
    <form method="POST">
    <?

    foreach($publicRelationCollection as $publicRelation)
    {
        echo $publicRelation->draw();
    }

    ?>
        <input type="submit" value="Save"/>
    </form>
    <button id="back">Back</button>
    <button id="home">Home</button>
</div>
</body>
</html>
