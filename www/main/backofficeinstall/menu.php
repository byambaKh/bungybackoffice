<?php
include "includes/application_top.php";
//Only users of type Accountant, SysAdmin, General Manager, Financial, Construction etc can access Head Office
if (!($user->hasRole('Accountant')
	|| $user->hasRole('SysAdmin')
	|| $user->hasRole('General Manager')
	|| $user->hasRole('Financial')
	|| $user->hasRole('Construction')
	|| $user->hasRole('Price Waterhouse Cooper')
    || $user->hasRole('InventoryManager')
	|| $user->hasRole('Staff')
    || ($user->getUserName() == 'Hiroshi')
	|| ($user->getUserName() == 'Yu')
	|| ($user->getUserName() == 'Sachiko')
	))
    {
        auth_user();
        exit();
    }
?>
<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width">
	<?php include 'includes/header_tags.php'; ?>
	<title><?php echo SYSTEM_SUBDOMAIN; ?> System </title>
	<script>
		function procLogout(){
			document.location ="?logout=true";
			return true;
		}
		function open_page(url, newwin) {
			if (newwin) {
				var nwin = window.open(url, '_blank');
				nwin.focus();
			} else {
				document.location = url;
			}
		}
	</script>
	<style>
		h1, h2 {
			color: white;
		}
		button:disabled div {
			color: silver;
		}
		div {
			text-align: center;
		}
		button {
			background-image: url('img/blank-button.png');
			width: 417px;
			height: 63px;
			border: none;
			background-position: center;
			margin: 1px;
			overflow: hidden;
			padding: 0px;
		}
		button div {
			width: 100%;
			height: 100%;
			font-size: 40px;
			color: white;
            background: firebrick;
			font-weight: normal;
			font-family: Verdana;
			margin: 0px;
			padding: 0px;
		}

		#searchBox{
			clear: none;
			display:inline;
			width: auto;
		}

	</style>
</head>
<body bgcolor=black>
<div>
	<img src="/img/index.jpg">
	<h1>Head Office</h1>
	<h2>Main Menu</h2>
	<a href="http://standardmove.com/webmail/" target="_blank"><img src="/img/webmail.png" width="25" height="25" style="margin-bottom: -10px;"></a>
	<form id="searchBox" action="https://www.google.com/search" method="get" target="_blank" style="font-size: 35px">
		<input type="text" name="q">
		<input type="submit" value="search">
	</form>

	<?php
	$buttons = array();
	switch (TRUE) {
		case ($user->hasRole('Accountant')):
			$buttons = array(
				'Accountant'=> '/Accountant/',
				'-'		=> '',
				'-'		=> '',
				'-'		=> '',
				'-'		=> '',
				'-'		=> '',
				'-'		=> '',
				'Roster'=> '/Roster/',
				'Change Password'	=> '/User/change_password.php',
				'Log Out'	=> '/menu.php?logout=true',
				'Back'	=> '/',
			);
			break;
		case ($user->hasRole('SysAdmin')):
			$buttons = array(
				'Reception' => '/Reception/index.php',
				'Company Charter'	=> '/companyCharter/index.php',
				'Action Register'	=> '/actionRegister/index.php',
				'Site Info' => '/SiteInfo/',
				'Bungy Companies'	=> 'http://standardmove.com/companies/',
				'Calendar Edit Adv'	=> '/Calendar/',
				'Roster'=> '/Roster/',
				'Charts'	=> '/Analysis/charts.php',
				'CordLog Files' => '/cordLogFiles/',
				'Operations Adv Edit'	=> '',
				'Report Issue Adv'	=> '',
				'Signage'			=> '/Signage/',
                'Uniforms'			=> '/Uniforms/',
				'Bookkeeping Adv'	=> '/Analysis/',
				'Inventory Adv'		=> '/Inventory/',
				'Project System'	=> 'http://standardmove.com/projects/',
				'Online Storage'	=> 'http://standardmove.com/storage/',
				'Change Pass Adv'	=> '/Manage/manageUserAccount.php',
				'Bungy Worldwide'	=> 'http://standardmove.com/companies/',
				'Log Out'	=> '/menu.php?logout=true',
				'Back'	=> '/',
			);
			break;

		case ($user->hasRole('Financial')):
			$buttons = array(
				'Bookkeeping Adv'	=> '/Bookkeeping/',
				'Calendar Edit Adv'	=> '/Calendar/',
				'Roster'=> '/Roster/',
				'Analysis' => '/Analysis/',
				'Inventory' => '/Inventory/',
				'Charts'	=> '/Analysis/charts.php',
				'Project System'	=> 'http://standardmove.com/projects/',
				'Online Storage'	=> 'http://standardmove.com/storage/',
				'Change Password' => '/User/change_password.php',
				'Log Out'	=> '/menu.php?logout=true',
				'Back'	=> '/',
			);
			break;

		case ($user->hasRole('General Manager')):
			$buttons = array(
				'Company Charter'	=> '/companyCharter/index.php',
				'Action Register'	=> '/actionRegister/index.php',
				'Calendar Edit Adv'	=> '/Calendar/',
				'Site Info' => '/SiteInfo/',
				'Signage'	  => '/Signage/',
				'Roster'=> '/Roster/',
				'Log Out'	=> '/menu.php?logout=true',
				'Back'	=> '/',
			);
			break;
		case ($user->hasRole('Construction')):
			$buttons = array(
				'Roster'=> '/Roster/',
				'Project System'	=> 'http://standardmove.com/projects/',
				'OwnCloud'	=> 'http://standardmove.com/storage/',
				'Change Password' => '/User/change_password.php',
				'Log Out'	=> '/menu.php?logout=true',
				'Back'	=> '/',
			);
			break;
		case ($user->hasRole('Price Waterhouse Cooper')):
			$buttons = array(
				/*'Bookkeeping'=> '/Bookkeeping/',*/
				'Inventory'	=> '/Inventory/',
				'Change Password' => '/User/change_password.php',
				'Log Out'	=> '/menu.php?logout=true',
				'Back'	=> '/',
			);
			break;

		case ($user->getUserName() == 'Hiroshi'):
			$buttons = array(
				'Inventory Adv' => '/Inventory/',
				'CordLog Files' => '/cordLogFiles/',
				'OwnCloud'	=> 'http://standardmove.com/storage/',
				'Change Password' => '/User/change_password.php',
				'Log Out'	=> '/menu.php?logout=true',
				'Back'	=> '/',
			);
			break;

		case ($user->getUserName() == 'Yu'):
			$buttons = array(
				'Inventory Adv' => '/Inventory/',
				'OwnCloud'	=> 'http://standardmove.com/storage/',
				'Change Password' => '/User/change_password.php',
				'Log Out'	=> '/menu.php?logout=true',
				'Back'	=> '/',
			);
			break;

		case ($user->getUserName() == 'Sachiko'):
			$buttons = array(
				'Inventory Adv' => '/Inventory/',
				'OwnCloud'	=> 'http://standardmove.com/storage/',
				'Change Password' => '/User/change_password.php',
				'Log Out'	=> '/menu.php?logout=true',
				'Back'	=> '/',
			);
			break;
        
        case ($user->hasRole('InventoryManager')):
			$buttons = array(
				'Inventory'		=> '/Inventory/',
				'Log Out'	=> '/menu.php?logout=true',
				'Back'	=> '/',
			);
			break;
        
		default:
			$buttons = array(
				'Log Out'	=> '/menu.php?logout=true',
				'Back'	=> '/',
			);
			break;

	};
	$i = 0;
	while (list($text, $url) = each($buttons)) {
		if (empty($text)) continue;
		$i++;
		?>
		<br><button onClick="open_page('<?php echo $url; ?>', <?php echo (int)(strstr($url, 'http://') !== FALSE); ?>);"<?php echo (empty($url))?' disabled':''; ?>><div><?php echo $text; ?></div></button>
		<?php
	};
	?>
	<br><br>
</div>
</body></html>

