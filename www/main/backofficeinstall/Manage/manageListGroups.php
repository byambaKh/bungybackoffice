<?php
	include ("../includes/application_top.php");
	$action = '';
	$record_type = 'lists_groups';
	if (isset($_POST['action'])) {
		$action = $_POST['action'];
	};
	if (empty($action) && isset($_GET['action'])) {
		$action = $_GET['action'];
	};
	switch ($action) {
		case 'add':
		case 'Add Group':
			$data = array(
				'name'	=> $_POST['name'],
				'alias'	=> $_POST['alias']
			);
			db_perform('lists_groups', $data);
			Header('Location: manageListGroups.php');
			die();
			break;
		case 'delete':
			$id = (int)$_GET['id'];
			$sql = "DELETE FROM lists_groups WHERE id = '$id'";
			$res = mysql_query($sql);
			Header('Location: manageListGroups.php');
			die();
			
	};
	
?>
<html>
<head>
<title>List Manager - Groups</title>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">

<script src="js/functions.js" type="text/javascript"></script>

<?php include '../includes/head_scripts.php'; ?>
<style>
table textarea {
	width: 100%;
	height: 400px;
}
button {
	border: 1px solid silver;
	background-color: yellow;
	border-radius: 5px;
	padding: 5px;
	margin: 5px;
	font-size: 20px;
	font-family: Arial;
}
#back {
	margin-left: 0px !important;
	float: left;
}
#update {
	margin-right: 0px !important;
	float: right;
}
#list-groups {
	max-width: 800px;
	margin-left: auto;
	margin-right: auto;
	width: 600px;
	border-collapse: collapse;
}
#list-groups td, #list-groups th {
	border: 1px black solid;
	padding: 3px;
	font-family: Arial;
	font-size: 12px;
}
.add, .lists, .edit, .delete, .change, .cancel {
	margin: 0px !important;
	padding: 2px !important;
	border-radius: 2px;
	font-size: 12px;
}
input {
	width: 98%;
}
.no-borders {
	border: none !important;
}
.header {
	background-color: #FFA500;
	border: 1px solid #FFA500 !important;
	border-bottom: 1px solid black !important;
	text-align: center;
	padding: 5px;
}
</style>

</head>
<?php include 'row_edit_functions.php'; ?>
<script>
    function row_delete() {
        if (window.confirm("Are you sure to delete this group and all lists?")) {
            document.location = 'manageListGroups.php?action=delete&id=' + $(this).parents('tr').attr('custom-id');
        };
    }

	var fvalues = {'name':[], 'alias':[]};

	$(document).ready(function () {
		$('#back').on('click', function () {
			document.location = '/Manage/';
			return false;
		});
		$(".delete").click(row_delete);
		$(".lists").click(function () {
			document.location = 'manageLists.php?group_id=' + $(this).parents('tr').attr('custom-id');
		});
		$('.edit').unbind('click').click(row_change);
	});
</script>
<body>
<?php include '../includes/main_menu.php'; ?>
<br />
<?php
 if (array_key_exists('result_message', $_SESSION) && !empty($_SESSION['result_message'])) {
	echo "<div id='system-message'>";
	switch ($_SESSION['result_message']) {
		case "insert":
			$message = "Item added";
			break;
		case "update":
			$message = "Item updated";
			break;
		case "delete":
			$message = "Item deleted";
			break;
	};
	echo $message;
	echo "</div>";
	unset($_SESSION['result_message']);
 };
?>
<?php include "lists_navigation.php"; ?>
<table id="list-groups">
	<tr>
		<td colspan="4" class="header"><b><?php echo SYSTEM_SUBDOMAIN; ?> - List Manager - Groups</b></td>
	</tr>
	<tr>
		<th class="group-id">ID</th>
		<th class="group-name">Name</th>
		<th class="group-alias">Alias</th>
		<th class="group-actions">Actions</th>
	</tr>
<?php
	$sql = "SELECT * FROM lists_groups ORDER by name ASC;";
	$res = mysql_query($sql) or die(mysql_error());
	while ($row = mysql_fetch_assoc($res)) {
		echo "\t<tr custom-id='{$row['id']}'>\n";
		echo "\t\t<td class='group-id'>{$row['id']}</td>\n";
		echo "\t\t<td class='group-name value' rel='name'>{$row['name']}</td>\n";
		echo "\t\t<td class='group-alias value' rel='alias'>{$row['alias']}</td>\n";
		echo "\t\t<td class='group-actions'><button class='lists'>Lists</button><button class='edit'>Edit</button><button class='delete'>Delete</button></td>\n";
		echo "\t</tr>\n";
	}
?>
	<tr>
		<th colspan="4">New group</th>
	</tr>
<form method="post">
	<tr>
		<td class="group-id">&nbsp;</td>
		<td class="group-name"><input type="text" name="name" value=""></td>
		<td class="group-alias"><input type="text" name="alias" value=""></td>
		<td class="group-actions"><button class="add" name="action" value="add" type="submit">Add group</button></td>
	</tr>
</form>
<tr>
<td colspan="4" class="no-borders">
<button id="back">Back</button>
</td>
</tr>
</table>
</body>
</html>
