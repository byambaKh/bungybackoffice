<?php
include("../includes/application_top.php");
//This was an attempt to use the code from agentInvoiceFromMgmt.php to solve the issue with outstanding balances on this
//page being wrong. Ended up using the old query which seems to solve the issue for now
function lastInvoiceForAgent($agent)
{
/*
//*****************CALCULATE ALL AGENT BALANCES
SELECT SUM(invoice - payment + TotalToCollect + totalCancel) AS outstanding, agent FROM (#
#SELECT * FROM (

	(
	SELECT
		site_id,
		LAST_DAY(DATE_FORMAT(bookingdate, '%Y-%m-01')) AS `date`,
		-1*SUM(IF(CollectPay = 'Offsite', 0, RateToPay * RateToPayQTY)) AS 'payOut',
		SUM(IF(CollectPay = 'Offsite', Rate * NoOfJump, 0)) AS 'TotalToCollect',
		SUM(IF(CancelFeeCollect = 'Offsite', CancelFee * CancelFeeQTY, 0)) AS totalCancel,
		0 AS payment,
		-1*SUM(IF(CollectPay = 'Offsite', 0, RateToPay * RateToPayQTY)) AS invoice,
		agent,
		'invoice' AS `type`
		FROM
			customerregs1 AS cr
		WHERE
			BookingDate >= '2014-01-01'
			AND `BookingDate` <= '2016-06-30'
			#AND Agent = @agent
			AND (
				( #Bookings made offsite that were completed via an agent
				DeleteStatus = 0
				AND Checked = 1
				AND CollectPay = 'Offsite'
				AND NoOfJump > 0
				) OR (
				#Bookings made Onsite that were completed via an agent
				DeleteStatus = 0
				AND Checked = 1
				AND NoOfJump > 0 #prevents group bookings appearing multiple times
				AND RateToPay > 0
				AND RateToPayQTY > 0
				AND CollectPay = 'Onsite'
				) OR (
				#partial cancellations through setting noOfJumps to zero in from dailyview checkin
				CancelFeeCollect = 'Offsite'
				AND NoOfJump = 0
				)
			)
		GROUP BY agent #,LAST_DAY(BookingDate)
		ORDER BY agent, `date` ASC
	)

	UNION ALL
	(
	SELECT
		site_id,
		`date`,
		-1*`out` AS payOut,
		-1*`in` AS totalToCollect,
		0 AS totalCancel,
		-1*`out` AS payment,
		0 AS invoice,
		company AS agent,
		'payment' AS `type`

	FROM
		banking
	WHERE
		#company = @agent AND
		description = 'SALES DEPOSIT FROM Rafting Company, Pension ETC For Bungy Sales'
		AND `date` >= '2014-01-01'
		AND `date` <= '2016-06-30'
	ORDER BY `agent`,`date` ASC
	)
 )  AS agent_data #WHERE agent = @agent
 WHERE agent IN ('Activity Japan', 'Agent', 'Ariston Camp', 'Asoview', 'Ayutei', 'Big Wave', 'Blue Monkey Lodge', 'Bokengoya', 'Canyons', 'Credit Card', 'Ever Green', 'Forest&Water', 'Grand Volee', 'H2O', 'Hoshino Furu Mori', 'I Love Outdoors', 'JTB Japan', 'Jaran', 'Kappa Club', 'Lakewalk', 'MK Kogen', 'MTB Japan', 'Mac Para', 'Matsubaya', 'Max', 'Media', 'Minakamikan', 'NULL', 'Nature Navigator', 'New World Tourist', 'OTHER', 'One Piece', 'Ozmall', 'P-Asanebo', 'P-Blueberry', 'P-La Neige', 'P-Matsubaya', 'P-Michinoku', 'P-Mogu House', 'P-Pal', 'P-Shobun', 'P-Todomatsu', 'P-Yamaji', 'Panorama', 'SG Bungy', 'Sabaidee', 'Shiobara', 'Skiers Place', 'Sotoasobi', 'TOP', 'Tanigawa Pure 21', 'Tenjin Lodge', 'Tokyo Gaijin', 'Tokyo Snow', 'Trip Piece', 'Uncle Bear', 'Veltra', 'With Sports')

 GROUP BY agent
 ORDER BY `agent`,`date`;

*/
    global $domain;
    $_GET['agent'] = $agent;

    $all_data = array();
    $invoices = array();
    $payments = array();
    list($year, $month, $day) = explode('-', date("Y-m-d"));
    $start = date("Y-m-d", mktime(0, 0, 0, $month, 1, $year));
    $finish = date("Y-m-t", mktime(0, 0, 0, $month, 1, $year));
    //echo "INVOICE Date Between: " . $start . "---" . $finish . "<br /><br />";
    $places = BJHelper::getPlaces();
    $today = date("Y-m-d");

    // get full invoice history
    //$start_date = date('Y-01-01');
    $start_date = '2014-01-01';//start calculating after from the same date that viewAgentInvoice.php starts

    foreach ($places as $place) {//the calculation happens on each domain
        if (in_array($place['subdomain'], array('test'))) continue;//ignore the test domain

        //All of this only happens on main, because there are not jumps
        //SUM all of the jumps, cancellations
        if ($place['subdomain'] != 'main') {//get all of the jumps for that particular agent in customerregs1
            $sql = "SELECT
								'" . ucfirst($place['subdomain']) . "' as site,
								bookingdate,
								bookingtime,
								RomajiName as name,

								/*Money from Jumps Agent*/
								/*Why are NoOfJump and RateToPayQTY different???*/
								IF(CollectPay = 'Offsite', NoOfJump, RateToPayQTY) as noofjump, /*If collectPay is offsite get NoOfJump or else get RateToPayQTY*/
								/**/
								IF(CollectPay = 'Offsite', Rate, RateToPay) as 'UnitCost',      /*If collect pay is off site get the rate, or else get the rateToPay*/
								/**/
								IF(CollectPay = 'Offsite', 0, RateToPay * RateToPayQTY) as 'TotalToPay', /*... get 0, or RateToPay * RateToPayQTY*/
								/**/
								IF(CollectPay = 'Offsite', Rate * NoOfJump, 0) as 'TotalToCollect',/*... Rate*NoOfJump or 0*/

								/*Money from Cancellations Agent*/
								/**/
								IF(CancelFeeCollect = 'Offsite', CancelFee, 0) as CancelFee, /*...CancelFee or 0*/
								/**/
								IF(CancelFeeCollect = 'Offsite', CancelFeeQTY, 0) as CancelFeeQTY,/*...CancelFee Qty or 0*/
								/**/
								IF(CancelFeeCollect = 'Offsite', CancelFeeCollect, 0) as CancelFeeCollect,/*...CancelFeeCollect*/
								/**/
									IF(CancelFeeCollect = 'Offsite', CancelFee * CancelFeeQTY, 0) as total_cancel,/*CancelFeeQTY * CancelFeeQTY or 0 */
									Checked
									from customerregs1 as cr
									WHERE BookingDate between '$start' AND '$finish'
								        AND site_id = '" . $place['id'] . "'
										AND Agent = '{$_GET['agent']}'
										AND (
									(
									    /*Offsite is they owe us*/
										CollectPay = 'Offsite'
										AND NoOfJump > 0
										AND ( Checked = 1 OR BookingDate >= '$today')
										AND DeleteStatus = 0
									) OR (
										/*Onsite we owe them*/
										DeleteStatus = 0
										AND NoOfJump > 0 /*Prevents group bookings from appearing multiple times*/
										AND ( Checked = 1 OR BookingDate >= '$today')
										AND RateToPay > 0
										AND RateToPayQTY > 0
										AND CollectPay = 'Onsite'
									) OR (
										/*Partial Cancellations by setting number of jumps to zero from Reception/dailyViewIE.php*/
										CancelFeeCollect = 'Offsite'
										AND NoOfJump = 0
									)
								)
									ORDER BY BookingDate ASC, RomajiName ASC;
								";

            $res = mysql_query($sql) or die(mysql_error());
            while ($row = mysql_fetch_assoc($res)) {
                $key = $row['bookingdate'] . ' ' . $domain . ' ' . $row['bookingtime'];
                if (!array_key_exists($key, $all_data)) {
                    $all_data[$key] = array();
                };
                $all_data[$key][] = $row;
            };

            //select invoice amounts
            $sql = "SELECT
									site_id,
									DATE_FORMAT(bookingdate, '%Y-%m') as invoice_date,
									SUM(IF(CollectPay = 'Offsite', 0, RateToPay * RateToPayQTY)) as 'TotalToPay',
									SUM(IF(CollectPay = 'Offsite', Rate * NoOfJump, 0)) as 'TotalToCollect',
									SUM(IF(CancelFeeCollect = 'Offsite', CancelFee * CancelFeeQTY, 0)) as total_cancel
									from customerregs1 as cr
									WHERE
                                        BookingDate >= '" . $start_date . "'
                                        AND site_id = '" . $place['id'] . "'
                                            AND Agent = '{$_GET['agent']}'
                                            AND (
                                                (
                                                    /*Bookings made offsite that were completed via an agent*/
                                                    DeleteStatus = 0
                                                    AND Checked = 1
                                                    AND CollectPay = 'Offsite'
                                                    AND NoOfJump > 0
                                                ) OR (
                                                    /*Bookings made Onsite that were completed via an agent*/
                                                    DeleteStatus = 0
                                                    AND Checked = 1
                                                    AND NoOfJump > 0
                                                    AND RateToPay > 0
                                                    AND RateToPayQTY > 0
                                                    AND CollectPay = 'Onsite'
                                                ) OR (
                                                    /*partial cancellations through setting noOfJumps to zero in from dailyview checkin*/
                                                    CancelFeeCollect = 'Offsite'
                                                    AND NoOfJump = 0
                                                )
                                            )
                                    GROUP BY invoice_date
                                    ORDER BY invoice_date ASC; ";

            $res = mysql_query($sql) or die(mysql_error());
            while ($row = mysql_fetch_assoc($res)) {
                list($y, $m) = explode('-', $row['invoice_date']);
                $key = $row['invoice_date'] . '-' . date("t", mktime(0, 0, 0, $m, 15, $y));
                if ($row['TotalToPay'] != 0 || $row['TotalToCollect'] != 0) {
                    if (!array_key_exists($key, $invoices)) {
                        $invoices[$key] = array(
                            'amount' => 0,
                        );
                    };
                    $invoices[$key]['amount'] += $row['TotalToCollect'] - $row['TotalToPay'];
                    $invoices[$key]['amount'] += $row['total_cancel'];
                };
            };
        }; // all except main
        //we use the description SALES DEGET FROM Rafting ... because we want to ignore other payments to the company as these are not part of the account
        $sql = "SELECT * FROM banking WHERE company = '{$_GET['agent']}' AND description = 'SALES DEPOSIT FROM Rafting Company, Pension ETC For Bungy Sales' AND site_id = '" . $place['id'] . "' and `date` >= '$start_date' ORDER BY `date` ASC;";
        //$sql = "SELECT sum(`in`-`out`) as total FROM banking WHERE company = '{$_GET['agent']}' and site_id in (" . implode(', ', $all_places) . ") and `date` >= '$start_date' /*and `date` < '$finish_date'*/ and description = 'SALES DEPOSIT FROM Rafting Company, Pension ETC For Bungy Sales';";

        $res = mysql_query($sql) or die(mysql_error());
        while ($row = mysql_fetch_assoc($res)) {
            $key = $row['date'];
            if ($row['in'] != 0 || $row['out'] != 0) {
                if (!array_key_exists($key, $payments)) {
                    $payments[$key] = array(
                        'amount' => 0,
                    );
                };
                $payments[$key]['amount'] += $row['in'] - $row['out'];
            };
        };
    };
    //TODO: Find out how outstanding $balance and Ramount are calculated
    // calculate previous balance
    $first_day = date("2014-01-01");
    $balance = 0;
    $transactions = array();//A list of all transactions in from invoices and payments

    foreach ($invoices as $d => $i) {//sum up all of the invoices
        if ($d < $first_day) {
            $balance += $i['amount'];
        } else {
            $key = $d . '-i';//What?
            if (!array_key_exists($key, $transactions)) {
                $transactions[$key] = array();
            };
            $transactions[$key][] = $i['amount'];
        };
    };

    foreach ($payments as $d => $p) {//subtract payments
        if ($d < $first_day) {
            $balance -= $p['amount'];
        } else {
            $key = $d . '-p';
            if (!array_key_exists($key, $transactions)) {
                $transactions[$key] = array();
            };
            $transactions[$key][] = $p['amount'];
        };
    };

    ksort($transactions);
    $fields = array(
        'site'           => 'Site',
        'bookingdate'    => 'Booking Date',
        'bookingtime'    => 'Time',
        'name'           => 'Customer Name',
        'noofjump'       => 'Jump No',
        'UnitCost'       => 'Rate',
        'TotalToPay'     => 'Total To Pay',
        'TotalToCollect' => 'Total To Collect'
    );

    $grand_total = 0;
    $jump_total = 0;
    $pay_total = 0;
    $cgrand_total = 0;
    $cjump_total = 0;
    $cpay_total = 0;
    foreach ($all_data as $key_array) {//This does not run at the for TOP so we can ignore this.
        foreach ($key_array as $row) {
            $tr_class = $row['Checked'] ? 'checked' : '';
            // show only records with jumps and rates
            if ($row['TotalToPay'] || $row['TotalToCollect']) {
                //echo "<tr class='$tr_class'>\n";
                foreach ($fields as $fname => $ftitle) {
                    $text = $row[$fname];
                    //echo "\t<td class='$fname'>$text</td>\n";
                };
                $grand_total += $row['TotalToCollect'];
                $pay_total += $row['TotalToPay'];
                $jump_total += $row['noofjump'];
                //echo "</tr>\n";
                if ($row['Checked']) {
                    $cgrand_total += $row['TotalToCollect'];
                    $cpay_total += $row['TotalToPay'];
                    $cjump_total += $row['noofjump'];
                };
            };
            if ($row['CancelFee'] && $row['CancelFeeQTY'] && $row['CancelFeeCollect'] == 'Offsite') {
                //echo "<tr class='$tr_class'>\n";
                $mapping = array(
                    'noofjump' => 'CancelFeeQTY',
                    'UnitCost' => 'CancelFee',
                );
                foreach ($fields as $fname => $ftitle) {
                    $text = $row[$fname];
                    if (array_key_exists($fname, $mapping)) {
                        $text = $row[$mapping[$fname]];
                    };
                    if ($fname == 'TotalToCollect') {
                        $text = $row['CancelFee'] * $row['CancelFeeQTY'];
                    };
                    if ($fname == 'name') $text .= ' (Cancel)';
                    //echo "\t<td class='$fname'>$text</td>\n";
                };
                $grand_total += $row['CancelFee'] * $row['CancelFeeQTY'];
                $jump_total += $row['CancelFeeQTY'];
                if ($row['Checked']) {
                    $cgrand_total += $row['CancelFee'] * $row['CancelFeeQTY'];
                    $cjump_total += $row['CancelFeeQTY'];
                };
                //echo "</tr>\n";
            };
        };
    };

    $balances = array();
    $lastInvoiceAmount = $lastInvoiceDate = '';

    foreach ($transactions as $d => $t) {
        preg_match("/(.*?)-([ip])$/", $d, $m);

        $today = DateTime::createFromFormat('Y-m-d', date('Y-m-d'));
        $transactionDate = DateTime::createFromFormat('Y-m-d', $m[1]);

        //The huge calculation above will calculate how much that agent owes us for the end of this month
        //we do not want to include this value until the month has ended and they have been invoiced.
        //So if the final transaction date is greater than today skip it.
        if($transactionDate > $today) continue;
        $difference = $today->diff($transactionDate);
        //Calculate how long ago the last invoice was in months
        $differenceInMonths = $difference->format("%m");


        foreach ($t as $amount) {
            $balance += (($m[2] == 'i') ? $amount : -$amount);
            //skip transactions that are not invoices so that we know the correct date and amount of the previous invoice
            //date and amount, but do include payments so that the outstanding balance is correct
            if($m[2] == 'i') {
                $lastInvoiceAmount = $amount;
                $lastInvoiceDate = $m[1];
            }

            $balances[$m[1]] = [
                'invoiceAmount'  => $lastInvoiceAmount,
                'agent'   => $agent,
                'balance' => $balance,
                'date' => $lastInvoiceDate,
                'monthsAgo' => $differenceInMonths,
            ];
        }
    }

    return end($balances);
}

$agents = BJHelper::getAgents();
/*
foreach($agents as $agent) {
    if($agent['text'] == 'NULL') continue;
    $agentBalance = balanceForAgent($agent['text']);
    if($agentBalance === 0)
    if(count($agentBalance) > 0) {
        $agentBalances[$agent['text']] = $agentBalance;
    }
}
*/

$agent = '';
$date = date('Y-m-d');

if (isset($_GET['date'])) $date = $_GET['date'];
if (isset($_GET['agent'])) $selectedAgent = $_GET['agent'];
echo '';

?>
<html>
<head>
    <?php include 'includes/header_tags.php'; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=8">
    <link rel="stylesheet" href="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/themes/base/jquery-ui.css" type="text/css" media="all"/>
    <link rel="stylesheet" href="http://static.jquery.com/ui/css/demo-docs-theme/ui.theme.css" type="text/css" media="all"/>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.4.4/jquery.min.js" type="text/javascript"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.8.10/jquery-ui.min.js" type="text/javascript"></script>
    <script src="js/functions.js" type="text/javascript"></script>

    <title>Agent Management</title>
    <style type="text/css">

        #d100 {
            display: none;
        }

        #back {
            float: left;
            HEIGHT: 21px;
            background-color: #FFFF66;
        }

        #outstanding {
            margin-top: 20px;
            border-collapse: collapse;
            margin-left: auto;
            margin-right: auto;
        }

        #outstanding th {
            padding: 5px;
            background-color: cyan;
            border: 2px solid black;
        }

        #outstanding td {
            padding: 5px;
            text-align: center;
            border: 2px solid black;
        }

        .red {
            background-color: red;
        }
    </style>
    <script type="text/javascript" charset="utf-8">
        function procInvoice() {
            var agentName = encodeURIComponent($("#agent").val());
            var startDate = encodeURIComponent($("#startDate").val());
            //document.agentForm.action = "agentInvoiceFromMgmt.php?agent=" + agentName + "&startDate=" + startDate;
            window.open("agentInvoiceFromMgmt.php?agent=" + agentName + "&startDate=" + startDate, '_blank');
            //document.agentForm.submit();
            //return true;
        }
        $(function () {
            $mydate1 = $('#startDate').datepicker({
                numberOfMonths: 1,
                showCurrentAtPos: 0,
                dateFormat: 'yy-mm-dd',
                altField: '#alternate',
                altFormat: 'yy-mm-dd',
                maxDate: new Date(<?php echo $maxDate?>)
            });
        });
    </script>
</head>
<body>
<br>

<form action="" method="GET" name="agentForm">
    <table align="center" width="480px" bgcolor="#D3D3D3">
        <tbody>
        <tr>
            <td colspan="6" align="center" bgcolor="#FFA500">
                <b>View Agent Invoice</b>
            </td>
        </tr>
        <tr>
            <td colspan="1" align="right">
                Agent(s) List:
            </td>
            <td>
                <?php
                echo draw_agent_drop_down('agent', $agent);
                ?>
            </td>
            <td align="right">
                Date:
            </td>
            <td>
                <input type="text" class="bigtext" name="startDate" id="startDate" size="12" readonly="readonly" value="<?= $date ?>"/>
            </td>
        </tr>
        <tr>
            <td colspan="6" align="right">
                <input id="back" type="button" name="vinv" value="Back" onClick="javascript:document.location='/Manage/';">
                <input style="HEIGHT: 21px; background-color:#FFFF66" type="button"
                       name="vinv" id="vinv" value="Invoice" onclick="procInvoice();">
            </td>
        </tr>
        </tbody>
    </table>
    <table id="outstanding">
        <tr>
            <th>Agent・会社</th>
            <th>Date - Last Invoice<br/>前回請求書日付</th>
            <th>Amount - Last Invoice<br/>前回請求書金額</th>
            <th>Outstanding Balance<br/>未払額</th>
        </tr>

        <?php
        //$invoices = array();
        //$payments = array();
        //$places = BJHelper::getPlaces();
        //$all_places = array();
        //$totals = array();
        //foreach ($places as $place) {
        //    if (in_array($place['subdomain'], array('test'))) continue;
        //    $all_places[] = $place['id'];
        //};
        //$start_date = '2014-01-01';
        //$finish_date = date("Y-m-01");
        ///*
        // * This is my old fix for the query but this generates wrong numbers despite the WHERE conditions being inconsistent
        // * with everything else.
        // * The second query works for now.
        // */
        //$sql = "SELECT
		//	Agent,
        //    DATE_FORMAT(BookingDate, '%Y-%m') as invoice_date,
        //    SUM(IF(CollectPay = 'Offsite', 0, RateToPay * RateToPayQTY)) as 'TotalToPay',
        //    SUM(IF(CollectPay = 'Offsite', Rate * NoOfJump, 0)+IF(CancelFeeCollect = 'Offsite', CancelFee * CancelFeeQTY, 0)) as 'TotalToCollect'
        //    from customerregs1 as cr
        //    WHERE
        //        site_id in (" . implode(', ', $all_places) . ")
		//		AND BookingDate >= '$start_date'
		//		AND BookingDate < '$finish_date'
        //        AND Agent <> 'NULL'
        //        AND (
        //            (
        //                /*Bookings made offsite that were completed via an agent*/
        //                DeleteStatus = 0
        //                AND Checked = 1
        //                AND CollectPay = 'Offsite'
        //                AND NoOfJump > 0
        //            ) OR (
        //                /*Bookings made Onsite that were completed via an agent*/
        //                DeleteStatus = 0
        //                AND Checked = 1
        //                AND RateToPay > 0
        //                AND RateToPayQTY > 0
        //                AND CollectPay = 'Onsite'
        //            ) OR (
        //                /*partial cancellations through setting noOfJumps to zero in from dailyview checkin*/
        //                CancelFeeCollect = 'Offsite'
        //                AND NoOfJump = 0
        //            )
        //        )
		//	GROUP BY Agent, invoice_date
        //    ORDER BY Agent ASC, invoice_date ASC;
 	    //";


        //get all of the bookings from customerregs1 that have agents
        //sum rateToPay as TotalToPay
        //sum the cancellation fess an and jumps as total to collect

        //The jumps are the amount owned by the agent
        /*
        $sql = "SELECT
                    Agent,
                    DATE_FORMAT(BookingDate, '%Y-%m') as invoice_date,
                    SUM(IF(CollectPay = 'Offsite', 0, RateToPay * RateToPayQTY)) as 'TotalToPay',
                    SUM(IF(CollectPay = 'Offsite', Rate * NoOfJump, 0)+IF(CancelFeeCollect = 'Offsite', CancelFee * CancelFeeQTY, 0)) as 'TotalToCollect'
                    from customerregs1 as cr
                    WHERE
                        site_id in (" . implode(', ', $all_places) . ")
                        and BookingDate >= '$start_date'
                        and BookingDate < '$finish_date'
                        and DeleteStatus = 0
                        and Checked = 1
                        and Agent <> 'NULL'
                        and (
                            (CollectPay = 'Offsite' and NoOfJump > 0)
                            or (
                                RateToPay > 0
                                and RateToPayQTY > 0
                                and CollectPay = 'Onsite'
                            )
                        )
                    GROUP BY Agent, invoice_date
                    ORDER BY Agent ASC, invoice_date ASC;
             ";
        */
        //$res = mysql_query($sql) or die(mysql_error());
        //while ($row = mysql_fetch_assoc($res)) {
        //    if (in_array($row['Agent'], array('MK Bungy', 'SG Bungy'))) continue;
        //    $invoices[$row['Agent']] = $row;
        //    if (!array_key_exists($row['Agent'], $totals)) $totals[$row['Agent']] = 0;
        //    $totals[$row['Agent']] += $row['TotalToCollect'] - $row['TotalToPay'];
        //};
        ////Add all of the
        ////$finish_date = date("Y-m-t");
        ////The banking total is the amount paid by the banking company
        //foreach ($totals as $agent => $sum) {
        //    //$sql = "SELECT sum(`in`-`out`) as total FROM banking WHERE company = '{$agent}' and site_id in (" . implode(', ', $all_places) . ") and `date` >= '$start_date' AND `date` < '$finish_date' and description = 'SALES DEPOSIT FROM Rafting Company, Pension ETC For Bungy Sales';";
        //    //remove end date as this is not in agentInvoiceFromMgmt
        //    $sql = "SELECT sum(`in`-`out`) as total FROM banking WHERE company = '{$agent}' and site_id in (" . implode(', ', $all_places) . ") and `date` >= '$start_date' and description = 'SALES DEPOSIT FROM Rafting Company, Pension ETC For Bungy Sales';";

        //    //echo $sql."<br><br>";

        //    $res = mysql_query($sql) or die(mysql_error());
        //    while ($row = mysql_fetch_assoc($res)) {
        //        $totals[$agent] -= $row['total'];
        //    };
        //    // bypass agents with zero balance
        //    if ($totals[$agent] == 0) continue;
        //    list($year, $month) = explode('-', $invoices[$agent]['invoice_date']);
        //    $class = ($totals[$agent] > $invoices[$agent]['TotalToCollect'] - $invoices[$agent]['TotalToPay']) ? 'red' : '';
        //    ?>
        <!--
            <tr>
                <td><?php //echo $agent; ?></td>
                <td><?php //echo date("Y-m-t", mktime(0, 0, 0, $month, 15, $year)); ?></td>
                <td><?php //echo $invoices[$agent]['TotalToCollect'] - $invoices[$agent]['TotalToPay']; ?></td>
                <td class="<?php //echo $class; ?>"><?php //echo $totals[$agent]; ?></td>
            </tr>
        //};
            -->

        <?php
        foreach($agents as $agent)
        {
            if($agent['text'] == 'NULL') continue;
            $lastInvoice = lastInvoiceForAgent($agent['text']);
            if(!($lastInvoice['balance'])) continue;//if the balance is zero or null, continue
            $class = ($lastInvoice['monthsAgo'] >= 2) ? 'red' : '';//if the invoice amount is greater than 2 years make it red

            echo "
            <tr>
                <td>{$agent['text']}</td>
                <td>{$lastInvoice['date']}</td>
                <td>{$lastInvoice['invoiceAmount']}</td>
                <td class='$class'>{$lastInvoice['balance']}</td>
            </tr>

            ";
        }
        ?>
    </table>
</form>
</body>
</html>
