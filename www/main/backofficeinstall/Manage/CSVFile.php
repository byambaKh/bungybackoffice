<?php

class CSVFile {
	
	function CSVFile($rs, $quotes=false, $filename="Download.csv"){
		ob_start();
		$this->setRs($rs);
		$this->setQuotes($quotes);
		$this->setFilename($filename);
		$this->dumpFile();
	}
	function setRs($rs){
		$this->_rs = $rs;
	}
	function setQuotes($quotes){
		if($quotes)
			$this->_quotes = '"';
		else
			$this->_quotes = "";
	}
	function setFilename($filename){
		$this->_filename = $filename;
	}
	function setFields(){
		mysql_data_seek($this->_rs, 0);
		$temp = mysql_fetch_assoc($this->_rs);
		$this->_fields = array_keys($temp);
		mysql_data_seek($this->_rs, 0);
	}
	function dumpFile() {		
		mysql_data_seek($this->_rs, 0);
		$lastfield = "";
		ob_end_clean();
		header("Pragma: public");
		header("Expires: 0");
		header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
		header("Cache-Control: public");
		header("Content-Description: File Transfer");
		header("Content-Type: application/unknown");
		header("Content-Disposition: filename=" . $this->_filename . "\r\n"); 
		header("Content-Transfer-Encoding: binary");

		if ($row = mysql_fetch_assoc($this->_rs)) {
			if($this->_quotes != "") {
				$allfields = $this->_quotes . implode('","',str_replace('"','""',array_keys($row))) . $this->_quotes;
				//echo $allfields;
			}else{
				$allfields = implode(',',array_keys($row));
				//echo $allfields;
			}
			echo("$allfields\r\n");
    		do {
        	if($this->_quotes != "") {
				$allfields = $this->_quotes . implode('","',$row) . $this->_quotes;
				//echo $allfields;
			}else{
				$allfields = implode(',',$row);
				//echo $allfields;
			}			
        	echo "$allfields\r\n";
    		} while ($row = mysql_fetch_assoc($this->_rs));
		}
		exit();
	}
}


?>