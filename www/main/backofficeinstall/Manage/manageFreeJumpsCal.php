<?php
	include ("../includes/application_top.php");

	$site_id = CURRENT_SITE_ID;

	$current_id = '';
	$action_result = "";
	$cmonth = date('Ym');

	if (isset($_GET['year'])) {
		$cmonth = $_GET['year'] . $_GET['month'];
	};

	if (!empty($_POST)) {
		$cmonth = $_POST['year'] . $_POST['month'];
		switch ($_POST['action']) {
			case 'update': 
			case 'Update': 
				$d = $_POST['year'] . '-' . $_POST['month'] . '-';
				// delete previous records
				$sql = "DELETE FROM free_jumps_calendar WHERE site_id = '$site_id' AND fj_date like '$d%' ;";
				$res = mysql_query($sql) or die('Cannot delete old records');
				if (!array_key_exists('days', $_POST)) $_POST['days'] = array();
				foreach ($_POST['days'] as $day => $value) {
					$data = array(
						'site_id'	=> $site_id,
						'fj_date'	=> $d . sprintf("%02d", $day),
						'status'	=> 0
					);
					db_perform('free_jumps_calendar', $data);
				};
				$action_result = 'update';
				break;
		};
	};
	if (!empty($action_result)) {
		$_SESSION['result_message'] = $action_result;
		Header("Location: manageFreeJumpsCal.php?year=" . $_POST['year'] . '&month=' . $_POST['month']);
		die();
	};
	$current_time = mktime(0, 0, 0, substr($cmonth, 4, 2), 15, substr($cmonth, 0, 4)); 
    $first_day_time = mktime(0,0,0, date("m", $current_time), 1, date("Y", $current_time));
    $last_day_time = mktime(0,0,0, date("m", $current_time), date("t", $current_time), date("Y", $current_time));
    $start_time = $first_day_time - date('w', $first_day_time) * 24 * 60 * 60;

	// empty user record
	$year = substr($cmonth, 0, 4);
	$month = substr($cmonth, 4, 2);
	$year_values = array();
	for ($i = 2010; $i <= date('Y') + 1; $i++) {
		$year_values[] = array(
			'id'		=> $i,
			'text'		=> $i
		);
	};
	$month_values = array();
	for ($i = 1; $i <= 12; $i++) {
		$month_values[] = array(
			'id'		=> sprintf("%02d", $i),
			'text'		=> date("F", mktime(0, 0, 0, $i, 15, 2013))
		);
	};
// Get Calendar State records for this month
    $sql = "SELECT * from calendar_state
        WHERE 
			site_id = '$site_id'
			AND BOOK_DATE >= '".date("Y-m-d", $first_day_time) . "'
            AND BOOK_DATE <= '".date("Y-m-d", $last_day_time) . "' ORDER BY BOOK_DATE ASC;";
    $res = mysql_query($sql) or die(mysql_error());
    $cstate = array();
    $cclass = array();
    $odates = array();
    while ($row = mysql_fetch_assoc($res)) {
        $cstate[$row['BOOK_DATE']] = $row;
        switch (true) {
            case ($row['USER_VIEW_STATUS'] == 9 && $row['OP_VIEW_STATUS'] == 9):
                $cclass[$row['BOOK_DATE']] = 'open';
                $odates[] = $row['BOOK_DATE'];
                break;
            case ($row['USER_VIEW_STATUS'] == 1 && $row['OP_VIEW_STATUS'] == 1):
                $cclass[$row['BOOK_DATE']] = 'closed';
                break;
            case ($row['USER_VIEW_STATUS'] == 9 && $row['OP_VIEW_STATUS'] == 1):
                $cclass[$row['BOOK_DATE']] = 'only-online';
                $odates[] = $row['BOOK_DATE'];
                break;
        };
    };
    mysql_free_result($res);
// Get disabled free jump days
	$disabled_days = array();
	$cdate = preg_replace("/([\d]{4})([\d]{2})/", "\\1-\\2", $cmonth);
	$sql = "SELECT * FROM free_jumps_calendar WHERE site_id = '$site_id' AND fj_date like '$cdate-%' order by fj_date ASC;";
	$res = mysql_query($sql) or die(mysql_error());
	while ($row = mysql_fetch_assoc($res)) {
		if ($row['status'] == 0) {
			$disabled_days[] = $row['fj_date'];
		};
	};
?>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">

<script src="js/functions.js" type="text/javascript"></script>

<?php include '../includes/head_scripts.php'; ?>
<style>
#system-message {
    background-color: #55FF55;
    text-align: center;
    line-height: 150%;
}

.days td {
    background-color: white !important;
}
#calendar-table td select option {
    background-color: white;
}
.even .staff, .even  .staff *, .even .selected, .even .selected * {
    background-color: lightyellow !important;
}
.odd .staff, .odd .staff *, .odd .selected, .odd .selected * {
    background-color: lightcyan !important;
}
#calendar-table td select {
    width: 200%;
}
#calendar-table td {
    overflow: hidden;
}
#calendar-table {
    width: 100%;
    background-color: black;
    table-layout: fixed;
}
.clickable {
    position: relative;
    margin: 5px;
    padding: 5px;
    font-family:Verdana;
    font-size: 20px;
    line-height: 20px;
    height: 30px;
    vertical-align: middle;
    border: 1px solid silver;
    color: silver;
    background-color: lightgrey;
}
.calendar-row {
    min-height: 20px !important;
}
.calendar-row th {
    background-color: black !important;
    color: white !important;
}
.calendar-row td {
    vertical-align: top;
    min-height: 20px !important;
    min-width: 20px;
    height: 20px;
    background-color: #eee;
}
.calendar-day {
    background-color: #eee;
    text-align: right;
	width: 30px;
	float: right;
}
.not-this-month, .not-this-month * {
    background-color: #ccc !important;
}
.active-day, .active-day * {
    background-color: #ddd !important;
}
.cell-content {
    width: 95%;
}
#update {
	float: right;
}
.error, .error * {
	background-color: #f00 !important;
}
.open, .open * {
	background-color: #afa !important;
}
.closed, .closed * {
	background-color: #faa !important;
}
.open.disabled, .open.disabled * {
	background-color: #ffa !important;
}
#legend {
	margin-bottom: 20px;
	border-collapse: collapse;
	border: 1px solid silver;
	margin-right: auto;
	margin-left: auto;
}
#legend td {
	border: 1px solid silver;
	padding: 12px;
	padding-top: 5px;
	padding-bottom: 5px;
	
}
.content {
	float: left;
}
</style>

<title>Bungy Japan :: Free Jumps Calendar Management</title>

</head>
<script type="text/javascript">
function procBack(){
	document.location = 'index.php';
}
$(document).ready(function () {
	$("#current-year, #current-month").on('change', function () {
		$('#dForm').submit();
	});
});
</script>
<body>
<?php include '../includes/main_menu.php'; ?>
<br />
<?php
 if (array_key_exists('result_message', $_SESSION) && !empty($_SESSION['result_message'])) {
	echo "<div id='system-message'>";
	switch ($_SESSION['result_message']) {
		case "update":
			$message = "Free Jumps Calendar updated";
			break;
	};
	echo $message;
	echo "</div>";
	unset($_SESSION['result_message']);
 };
?>
<form method="post" name="dForm" id="dForm">
<table align="center" width="480px" bgcolor="#D3D3D3">
	<tr>
		<td colspan="4" align="center" bgcolor="#FFA500"><b>Please select year/month</b></td>
	</tr>
	<tr>
		<td align="right" bgcolor="#FFA500">Year:</td>
		<td align="left" bgcolor="#FFA500">
			<?php echo draw_pull_down_menu('year', $year_values, $year, 'id="current-year"'); ?>
		</td>
		<td align="right" bgcolor="#FFA500">Month:</td>
		<td align="left" bgcolor="#FFA500">
			<?php echo draw_pull_down_menu('month', $month_values, $month, 'id="current-month"'); ?>
		</td>
	</tr>
</table>
</form>
<table id='legend'>
<tr>
	<td>Legend: </td>
	<td class="error">Not in calendar state</td>
	<td class="open">Site open</td>
	<td class="open disabled">Site open / free jump disabled</td>
	<td class="closed">Site closed</td>
	<td class="not-this-month">Not this month</td>
</tr>
</table>
<form method="post" name="jForm">
<input type="hidden" name="year" value="<?php echo $year; ?>">
<input type="hidden" name="month" value="<?php echo $month; ?>">
<table id="calendar-table">
<?php
    echo "\t<tr class='calendar-row'>\n";
    for ($i = 0; $i < 7; $i++) {
        echo "\t\t<th>\n".date('l', $start_time + $i * 24 * 60 * 60) . "</th>";
    };
    echo "\t</tr>\n";
    $calendar_current_time = $start_time;
    while ($calendar_current_time < $last_day_time) {
        echo "\t<tr class='calendar-row'>\n";
        $current_col = 0;
        while ($current_col < 7) {
			$day = date('d', $calendar_current_time);
			$d = date('Y-m-d', $calendar_current_time);
            $calendar_date = date("Y-m-d", $calendar_current_time);
            $add_class= '';
            if (date("m", $current_time) != date("m", $calendar_current_time)) {
                $add_class = ' not-this-month';
				$content = '';
            } else {
				$checked = (in_array($d, $disabled_days)) ? ' checked="checked"' : '';
				$content = '<input type="checkbox" name=days['.$day.']' . $checked . '> Disable';
				if (!empty($checked)) {
					$add_class .= ' disabled';
				};
			};
            if (array_key_exists($calendar_date, $cclass)) {
                $add_class .= ' ' . $cclass[$calendar_date];
				if ($cclass[$calendar_date] != 'open') {
					$content = '';
				};
				if ($cclass[$calendar_date] == 'closed') {
					$content = 'Site Closed';
				};
            } else {
				if (empty($add_class)) {
					$content = '';
					$add_class .= ' error';
				};
			};
            echo "\t\t<td class='$add_class' custom-day=\"".date("d", $calendar_current_time)."\">\n";
			echo "\t\t\t<div class='content'>$content</div>\n";
            echo "\t\t\t<div class='calendar-day'>" . date("j", $calendar_current_time) . "</div>\n";
            echo "\t\t</td>\n";
            $current_col++;
            $calendar_current_time += 24 * 60 * 60;
        };
        echo "\t</tr>\n";
    };
?>
</table>
<button type='button' id="back" onclick="procBack();">Back</button>
<button name='action' type='submit' value='update' id="update">Update</button>
</form>
</body>
</html>
