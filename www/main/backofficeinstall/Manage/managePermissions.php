<?php
	include ("../includes/application_top.php");
	
	$ignored_dirs = array('.', '..', 'includes', 'Booking', 'Yoyaku', 'img', 'css', 'cgi-bin', 'js', 'temp', 'new', 'm', 'util', 'web', 'mobile', 'Bungy', 'Controller', 'Testing');

	$current_atid = null;
	$action_result = "";
	if (isset($_GET['access_type_id']) && !empty($_GET['access_type_id'])) {
		$current_atid = $_GET['access_type_id'];
	};
	if (isset($_POST['access_type_id']) && !empty($_POST['access_type_id'])) {
		$current_atid = $_POST['access_type_id'];
	};

	if (!empty($_POST)) {
		switch ($_POST['action']) {
			case 'update':
			case 'Update': 
				$sql = "DELETE FROM access_type_permissions WHERE access_type_id = '$current_atid'";
				mysql_query($sql);
				foreach ($_POST['directories'] as $directory) {
					$data = array(
						'access_type_id'	=> $current_atid,
						'directory'			=> $directory
					);
					db_perform('access_type_permissions', $data);
				};
				Header("Location: managePermissions.php?access_type_id=".$current_atid);
				die();
				break;
		};
	} else {
		switch (TRUE) {
			case array_key_exists('action', $_GET):
				if ($_GET['action'] == 'delete') {
					$current_uid = (int)$_GET['uid'];
					$sql = "DELETE FROM users WHERE UserID = '$current_uid'";
					mysql_query($sql);
					$current_uid = '';
					$action_result = 'delete';
				} elseif ($_GET['action'] == 'edit') {
					$current_uid = (int)$_GET['uid'];
				};
				break;
		};
	};
	if (!empty($action_result)) {
		$_SESSION['result_message'] = $action_result;
		Header("Location: manageUserAccount.php");
		die();
	};
	$sql = "SELECT * FROM access_types order by type_name ASC";
	$res = mysql_query($sql) or die(mysql_error());
	$access_types = array();
	while ($row = mysql_fetch_assoc($res)) {
		$access_types[] = array(
			'id'	=> $row['id'],
			'text'	=> $row['type_name'] . " ({$row['alias']})"
		);
		if (is_null($current_atid)) {
			$current_atid = $row['id'];
		};
	};
	// read all accessible directories
	$dh = opendir('../');
	while ($file = readdir($dh)) {
		if (!is_dir('../' . $file)) continue;
		if (in_array($file, $ignored_dirs)) continue;
		$directories[] = $file;
	};
	sort($directories);
?>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">

<script src="js/functions.js" type="text/javascript"></script>

<?php include '../includes/head_scripts.php'; ?>
<style>
.users-table-row1 {
	background-color: #CCCCFF;
}
.users-table-row2 {
	background-color: #CCFFCC;
}
#system-message {
	background-color: #55FF55;
	text-align: center;
	line-height: 150%;
}
.user-access-type {
	background-color: #fdd;
	padding: 20px;
}
.user-access-type h2 {
	margin: 0px;
	padding:0px;
}
.users-table-header {
	background-color: #fdd;
}
.at-table-check {
	width: 30px;
	max-width: 30px;
	text-align: center;
}
.at-table-directory {
	padding-left: 5px;
}
#update {
	float: right;
}
</style>

<title>Bungy Japan :: User Management</title>

</head>
<script type="text/javascript">
function procAdd(){
	if(document.uForm.usrnm.value == ''){
		alert("User Name is required!!!");
		return false;
	}
	if(document.uForm.pwds.value == ''){
		alert("Password is required!!!");
		return false;
	}
	document.uForm.submit();
	return true;
}
function procBack(){
	if(document.getElementById('back') ){
		document.uForm.action = "index.php";
		document.uForm.submit();
	}
}
function deleteConfirm(uid, username) {
	var answer = window.confirm('Are you sure to delete "'+username+'" user?') 
	if (answer == true) {
		document.location = "manageUserAccount.php?action=delete&uid=" + uid;
	};
	return answer;
}
</script>
<body>
<?php include '../includes/main_menu.php'; ?>
<br />
<?php
 if (array_key_exists('result_message', $_SESSION) && !empty($_SESSION['result_message'])) {
	echo "<div id='system-message'>";
	switch ($_SESSION['result_message']) {
		case "insert":
			$message = "User added";
			break;
		case "update":
			$message = "User updated";
			break;
		case "delete":
			$message = "User deleted";
			break;
	};
	echo $message;
	echo "</div>";
	unset($_SESSION['result_message']);
 };
?>
<?php include 'user_management_menu.php'; ?>
<table align="center" width="480px" bgcolor="#D3D3D3">
	<tbody>
		<tr>
			<td colspan="8" align="center" bgcolor="#FFA500"><b>Permissions Management</b>
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500">User Role:</td>
			<td align="left" bgcolor="#FFA500">
				<?php echo draw_pull_down_menu('access_type_id', $access_types, $current_atid, 'id="access-type-id"'); ?>
			</td>
		</tr>
	</tbody>
</table>
<br />
<form method="post" name="uForm">
<table align="center" width="300" id="users-table">
<tr class="users-table-header">
    <th class="users-table-delete" colspan="2">Directory permissions</th>
</tr>
<tr class="users-table-header">
    <th class="users-table-delete" colspan="2">
		<a href="#" id="check-all">Check all</a> | 
		 <a href="#" id="uncheck-all">Uncheck all</a>
	</th>
</tr>
<?php
	$sql = "select * from access_type_permissions WHERE access_type_id = '$current_atid' order by directory asc";
	$res = mysql_query($sql);
	$current_selected = array();
	while($row = mysql_fetch_assoc($res)) {
		$current_selected[] = $row['directory'];
	};
	$i = 0;
	foreach ($directories as $directory) {
		$i = !$i;
		$checked = (in_array($directory, $current_selected)) ? ' checked="checked"' : '';
?>
	<tr class="users-table-row<?php echo ($i+1); ?>">
		<td class="at-table-check"><input name="directories[]" value="<?php echo $directory; ?>" type="checkbox"<?php echo $checked; ?>></td>
		<td class="at-table-directory"><?php echo $directory; ?></td>
	</tr>
<?php
	};
?>
<tr class="users-table-header">
    <td class="at-table-update" colspan="2">
		<button type="button" id="back">Back</button>
		<button type="submit" name="action" value="update" id="update">Update</button>
	</td>
</tr>
</table>
<script>
$(document).ready(function (){
	$('#access-type-id').change(function (event){
		document.location = 'managePermissions.php?access_type_id=' + $(this).val();
	});
	$("#check-all, #uncheck-all").click(function (e) {
		var checked = ($(this).attr('id') == 'check-all') ? true : false;
		$('.at-table-check input').prop('checked', checked);
		return false;
	});
	$("#back").click(function () {
		document.location = '/Manage/';
	});
});
</script>

</form>
</body>
</html>
