<?php
	include '../includes/application_top.php';
?>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">
<link href="css/style.css" rel="stylesheet" media="screen" type="text/css" />

<?php include '../includes/head_scripts.php'; ?>
<script src="js/functions.js" type="text/javascript"></script>
  <title><?php echo SYSTEM_SUBDOMAIN; ?> - System Admin</title>
<style type="text/css">

	#d100 {
	    display: none;
	}

</style>  
<script type="text/javascript">
function porcessAction1(){
	if(document.getElementById('upload')){
		document.mForm.upload.disabled = true;
			
	       window.location = '/Manage/importCSV.php';
	}
}
function porcessDelete(){
	if(document.getElementById('delete')){
		var answer = confirm("Are you sure to DELETE the reocrds?");
		if (answer)
			window.location = '/Manage/deleteCSVRecords.php';
			else{
		}
			
	}
}
	function porcessAction2(){
		
		if(document.getElementById('download')){
		       //alert("Download clicked...");
		       window.location = '/Manage/downloadCSV.php';
		}	
}
function processCal(){
	if(document.getElementById('calendar')){
		window.location = '/Manage/processCalDays.php';
	}	
}
function processTime(){
	if(document.getElementById('procTime')){
		window.location = '/Manage/processDate.php';
	}	
}
function porcessSiteOn(){
	if(document.getElementById('procSiteOn')){
		window.location = '/Manage/processSiteOn.php';
	}
}
function porcessSiteOff(){
	if(document.getElementById('procSiteOff')){
		window.location = '/Manage/processSiteOff.php';
	}
}

//function addName(){
	//if(document.getElementById('procName')){
	       //window.location = '/Manage/importCompanyNames.php';
	//}
//}
function procAgents(){
	if(document.getElementById('agent')){
	       //window.location = '/Manage/manageAgent.php';
		window.location = '/Manage/viewAgentInvoice.php';
	}
}
function procNewAgents(){
	if(document.getElementById('nagent')){
	       //window.location = '/Manage/manageAgent.php';
		window.location = '/Manage/newAgent.php';
	}
}
function procEditAgents(){
	if(document.getElementById('eagent')){
	       window.location = '/Manage/editAgent.php';

	}
}

function procAccountMgmt(){
	if(document.getElementById('usraccount')){
		window.location = '/Manage/manageUsers.php';
	}
}
function procStaffList(){
	window.location = '/Manage/manageStaffNames.php';
}
function procConfirmationEmail(){
	window.location = '/Manage/editConfirmationEmail.php';
}
function procMerchandiseItems(){
	window.location = 'manageMerchandiseItems.php';
}
function procListManager(){
	window.location = 'manageLists.php';
}
function procHowToManager(){
	window.location = 'manageHelpPages.php';
}
function procFreeJumpsManager(){
	window.location = 'manageFreeJumps.php';
}
function procFreeJumpsCalManager(){
	window.location = 'manageFreeJumpsCal.php';
}
function procConfirmationPopup(){
	window.location = '/Manage/editConfirmationPopup.php';
}
function procConfiguration(){
	window.location = '/Manage/editConfiguration.php';
}
function procWeight(){
    window.location = '/Manage/switchWeight.php';
}
function procBack(){
	if(document.getElementById('back')){
		document.location = '/';
		document.mForm.action = "/";
		document.mForm.submit();
	}
	
}
function toggle(id) {
	var state = document.getElementById(id).style.display;
	if (state == 'block') {
		document.getElementById(id).style.display = 'none';
	} else {
		document.getElementById(id).style.display = 'block';
	}
}

</script>
</head>

<body>
<?php include '../includes/main_menu.php'; ?><br />
<form name="mForm" method="post">
  <table border="0" align="center" width="350px" >
    <tbody>
    <tr>
    </tr><tr>
    </tr>
    	<tr align="center" bgcolor="#FFFFFF">
    		<td colspan="4"><h3><?php echo SYSTEM_SUBDOMAIN; ?> - System Admin</h3></td>
    	</tr>
      <tr align="center" bgcolor="#000000" >
        <td colspan="2"><font color="#FFFFFF"><b>Please Select Task</b></font></td>
      </tr>
<?php 
	$user = new BJUser();
	if ($user->hasRole('SysAdmin')) {
?>

      <tr  bgcolor="#FF0000">
        <td align="right" ><font color="#FFFFFF">Upload CSV Data:</font></td>

        <td><input style="WIDTH: 90px; HEIGHT: 21px; background-color:#FFFF66" name="upload" id="upload" onclick="porcessAction1();" value="UPLOAD" size="19" type="button"></td>
        
      </tr>
      	
	  <tr>
	  	 <td bgcolor="#FF0000" align="right"><font color="#FFFFFF"><b>Delete CSV Records:</b></font></td>
	  	<td bgcolor="#FF0000">
      		<input style="WIDTH: 90px; HEIGHT: 21px; background-color:#00FF7F" id="delete" onclick="porcessDelete();" value="DELETE" type="button">
      	</td>
	  </tr>
      <tr  bgcolor="#FF0000">
        <td align="right"><font color="#FFFFFF">Download CSV Data:</font></td>

        <td><input style="WIDTH: 90px; HEIGHT: 21px; background-color:#FFFF66" id="download" onclick="porcessAction2();" value="DOWNLOAD" size="19" type="button"></td>
      </tr>
<?php }; ?>
      <tr  bgcolor="#FF0000">
        <td align="right"><font color="#FFFFFF">Turn ON|OFF Calendar Days:</font></td>

        <td><input style="WIDTH: 90px; HEIGHT: 21px; background-color:#FFFF66" id="calendar" onclick="processCal();" value="ON|OFF" type="button"></td>
      </tr>

      <tr  bgcolor="#FF0000">
        <td align="right"><font color="#FFFFFF">Turn OFF|ON Time:</font></td>

        <td><input style="WIDTH: 90px; HEIGHT: 21px; background-color:#FFFF66" id="procTime" onclick="processTime();" value="OFF|ON" size="19" type="button"></td>
      </tr>

      <tr  bgcolor="#FF0000">
        <td align="right"><font color="#FFFFFF">Booking Site ON|OFF:</font></td>

        <td>
        	<input style="WIDTH: 40px; HEIGHT: 21px; background-color:#FFFF66" id="procSiteOn" onclick="porcessSiteOn();" value="ON" size="19" type="button">
        	<input style="WIDTH: 45px; HEIGHT: 21px; background-color:#FFFF66" id="procSiteOff" onclick="porcessSiteOff();" value="OFF" size="19" type="button">
        </td>
      </tr>
      <!-- 
      <tr  bgcolor="#FF0000">
        <td align="right"><font color="#FFFFFF">Add Name:</font></td>
        <td><input style="WIDTH: 90px; HEIGHT: 21px; background-color:#FFFF66" id="procName" onclick="addName();" value="ADD" size="19" type="button"></td>
      </tr>
      -->
      <tr  bgcolor="#FF0000">
        <td align="right"><font color="#FFFFFF">AGENTS ACC Manage:</font></td>
        <td>
        	<input style="font-size:10px; WIDTH: 110px; HEIGHT: 21px; background-color:#FFFF66" 
							id="agent" 
							value="View Agent Invoice"
							type="button" onclick="procAgents();"><br>
			<input style="font-size:10px; WIDTH: 110px; HEIGHT: 21px;
							background-color:#FFFF66" 
							id="nagent" 
							value="Add A New Agent" size="19" 
							type="button" onclick="procNewAgents();"><br>
			<input style="font-size:10px; WIDTH: 110px; HEIGHT: 21px;
							background-color:#FFFF66" 
							id="eagent" 
							value="Edit Agent Details" size="19" 
							type="button" onclick="procEditAgents();">								
		</td>
      </tr>
      <tr  bgcolor="#FF0000">
        <td align="right"><font color="#FFFFFF">USERS ACC Manage:</font></td>
        <td><input style="WIDTH: 90px; HEIGHT: 21px; background-color:#FFFF66" id="usraccount" onclick="procAccountMgmt();" value="MANAGE" size="19" type="button"></td>
      </tr>
      <tr  bgcolor="#FF0000">
        <td align="right"><font color="#FFFFFF">STAFF List Manage:</font></td>
        <td><input style="WIDTH: 90px; HEIGHT: 21px; background-color:#FFFF66" id="usraccount" onclick="procStaffList();" value="MANAGE" size="19" type="button"></td>
      </tr>
      <tr  bgcolor="#FF0000">
        <td align="right"><font color="#FFFFFF">Customer Confirmation E-mail:</font></td>
        <td><input style="WIDTH: 90px; HEIGHT: 21px; background-color:#FFFF66" id="usraccount" onclick="procConfirmationEmail();" value="EDIT" size="19" type="button"></td>
      </tr>
      <tr  bgcolor="#FF0000">
        <td align="right"><font color="#FFFFFF">Customer Confirmation Pop-up:</font></td>
        <td><input style="WIDTH: 90px; HEIGHT: 21px; background-color:#FFFF66" id="usraccount" onclick="procConfirmationPopup();" value="EDIT" size="19" type="button"></td>
      </tr>
      <tr  bgcolor="#FF0000">
        <td align="right"><font color="#FFFFFF">System Configuration:</font></td>
        <td><input style="WIDTH: 90px; HEIGHT: 21px; background-color:#FFFF66" id="usraccount" onclick="procConfiguration();" value="EDIT" size="19" type="button"></td>
      </tr>
      <tr  bgcolor="#FF0000">
        <td align="right"><font color="#FFFFFF">Manage Merchandise Items:</font></td>
        <td><input style="WIDTH: 90px; HEIGHT: 21px; background-color:#FFFF66" id="usraccount" onclick="procMerchandiseItems();" value="MANAGE" size="19" type="button"></td>
      </tr>
      <tr  bgcolor="#FF0000">
        <td align="right"><font color="#FFFFFF">List Manager:</font></td>
        <td><input style="WIDTH: 90px; HEIGHT: 21px; background-color:#FFFF66" id="usraccount" onclick="procListManager();" value="MANAGE" size="19" type="button"></td>
      </tr>
      <tr  bgcolor="#FF0000">
        <td align="right"><font color="#FFFFFF">How To Manager:</font></td>
        <td><input style="WIDTH: 90px; HEIGHT: 21px; background-color:#FFFF66" id="howto" onclick="procHowToManager();" value="MANAGE" size="19" type="button"></td>
      </tr>
      <tr  bgcolor="#FF0000">
        <td align="right"><font color="#FFFFFF">Free Jumps Calendar Manager:</font></td>
        <td><input style="WIDTH: 90px; HEIGHT: 21px; background-color:#FFFF66" id="free-jumps-calendar" onclick="procFreeJumpsCalManager();" value="MANAGE" size="19" type="button"></td>
      </tr>
      <tr  bgcolor="#FF0000">
        <td align="right"><font color="#FFFFFF">Free Jumps Manager:</font></td>
        <td><input style="WIDTH: 90px; HEIGHT: 21px; background-color:#FFFF66" id="free-jumps" onclick="procFreeJumpsManager();" value="MANAGE" size="19" type="button"></td>
      </tr>
      <tr  bgcolor="#FF0000">
        <td align="right"><font color="#FFFFFF">Turn ON|OFF iPad Weight:</font></td>
        <td><input style="WIDTH: 90px; HEIGHT: 21px; background-color:#FFFF66" id="weight-switch" onclick="procWeight();" value="ON|OFF" size="19" type="button"></td>
      </tr>
      <tr>
        <td bgcolor="#000000" colspan="2" align="center">
        	<input style="WIDTH: 80px; HEIGHT: 25px; background-color:#FFFF66" id="back" onclick="procBack();" value="Back" size="19" type="button">
        </td>
		      	
      </tr>
    </tbody>
  </table>
  <div id="d100">
	  <table align="right">
	  	<tbody>
	  		<tr>
				<td>
					<input style="WIDTH: 160px; HEIGHT: 21px; background-color:#FFFF66" 
								id="procTime"  
								onclick="toggle('d100');" 
								value="View Agent Invoice" type="submit">
				</td>
			</tr>
			<tr>
				<td>
					<input style="WIDTH: 160px; HEIGHT: 21px; background-color:#FFFF66" id="procTime" onclick="processTime();" value="Add A New Agent" type="button">
				</td>
			</tr>
			<tr>
				<td>
					<input style="WIDTH: 160px; HEIGHT: 21px; background-color:#FFFF66" id="procTime" onclick="processTime();" value="Edit Existing Agent Details" type="button">
				</td>
			</tr>
	  	</tbody>
	  </table>
  </div>
 </form> 
</body>
</html>
