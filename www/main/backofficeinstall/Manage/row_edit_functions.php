<script>
    function row_update() {
        var id = $(this).parents('tr').attr('custom-id');
        var post_data = {};
        post_data.id = id;
        $(this).parent().parent().children("td.value").each(function () {
            var field = $(this).attr('rel');
            post_data[field] = $(this).children('[name='+field+']').prop('value');
            if (field == 'shop_name' && $(this).children('[name='+field+'_new]').prop('value') != '') {
                post_data[field+'_new'] = $(this).children('[name='+field+'_new]').prop('value');
            };
            if (field == 'company' && $(this).children('[name='+field+'_new]').prop('value') != '') {
                post_data[field+'_new'] = $(this).children('[name='+field+'_new]').prop('value');
            };
        });
        $.ajax({
            url: 'ajax_handler.php?action=update_row&type=<?php echo $record_type; ?>',
            method: 'POST',
            data: post_data,
            context: $(this)
        }).done(function () {
            $(this).parent().parent().children("td.value").each(function () {
                var field = $(this).attr('rel');
                if ((field == 'shop_name' || field == 'company') && $(this).children('[name='+field+'_new]').prop('value') != '') {
                    $(this).html($(this).children('[name='+field+'_new]').prop('value'));
                    fvalues[field].push({
                        'id': $(this).html(),
                        'text' :$(this).html()
                    });
                    fvalues[field] = fvalues[field].sort(function (item1, item2) {
                        return (item1.text > item2.text);
                    });
                } else {
                    $(this).html($(this).children('[name='+field+']').prop('value'));
                };
            });
            $(this).html('Edit').removeClass('update').addClass('edit').unbind('click').click(row_change);
            $(this).parent().children(".cancel").text('Delete').removeClass('cancel').addClass('delete').unbind('click').click(row_delete);
        });
    }
    function row_cancel() {
        var id = $(this).parents('tr').attr('custom-id');
        $(this).parent().parent().children("td.value").each(function () {
            var field = $(this).attr('rel');
            $(this).html($(this).children('input[type=hidden]').prop('value'));
        });
        $(this).parent().children(".edit").text('Edit').removeClass('edit').addClass('edit').unbind('click').click(row_change);
        $(this).text('Delete').removeClass('camcel').addClass('delete').unbind('click').click(row_delete);
    }
    function row_change() {
        var id = $(this).parents('tr').attr('custom-id');
        $(this).parent().parent().children("td.value").each(function () {
            var input_field = '';
            var field = $(this).attr('rel');
            var value = $(this).html();
            if (typeof field != 'undefined' && fvalues[field].length > 0) {
                input_field = $('<select>').prop('name', field);
                for (fv in fvalues[field]) {
                    if (fvalues[field].hasOwnProperty(fv)) {
                        input_field.append(
                            $('<option>').prop('value', fvalues[field][fv].id).text(fvalues[field][fv].text)
                        );
                    };
                };
                input_field.prop('value', $(this).html());
            } else {
                input_field = $('<input>').prop('name', field).prop('value', $(this).html());
            };
            $(this).html(input_field);
            $(this).append(
                $('<input>').prop('type', 'hidden').prop('name', field + '_old').prop('value', value)
            );
            if (field == 'shop_name' || field == 'company') {
                $(this).append($('<br />')).append(
                    $('<input>').prop('name', field + '_new')
                );
            };
        });
        $(this).text('Update').removeClass('change').addClass('edit').unbind('click').click(row_update);
        $(this).parent().children(".delete").text('Cancel').removeClass('delete').addClass('cancel').unbind('click').click(row_cancel);
    };
</script>
