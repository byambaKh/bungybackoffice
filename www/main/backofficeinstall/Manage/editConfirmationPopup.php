<?php
	include ("../includes/application_top.php");
	mysql_query("SET NAMES UTF8;");
	$site_id = CURRENT_SITE_ID;
	$types = array(
		'success'		    => 'Success Booking',
        'success_paid'		=> 'Credit Card Success Booking',
		'denied'		    => 'No Slots Available',
		'denied_phone'	    => 'Booking with this phone number exists',
		'incorrect'		    => 'Incorrect Verification Code',
		'notsaved'		    => 'Technical issues',
	);
	// select DB templates
	$templates = array();
	$sql = "SELECT * FROM templates WHERE site_id = '$site_id' and template_type in ('".implode("','", array_keys($types))."');";
	$res = mysql_query($sql) or die(mysql_error());
	while ($row = mysql_fetch_assoc($res)) {
		$templates[$row['template_type']][$row['lang']] = $row;
		$content[$row['lang']][$row['template_type']] = $row['template_content'];
	};
	mysql_free_result($res);
	// if there is a POST,save and go to management console
	if (!empty($_POST)) {
		foreach (array('en', 'jp') as $lang) {
			$$lang = $_POST[$lang];
			foreach ($$lang as $type => $content) {
				file_put_contents("../includes/templates/errors/$lang/$type.html", $content);
				// DB save
				$data = array(
					'template_content'	=> $content
				);
				if (array_key_exists($type, $templates) && array_key_exists($lang, $templates[$type])) {
					db_perform('templates', $data, 'update', "id = '{$templates[$type][$lang]['id']}'");
				} else {
					$data['site_id'] = $site_id;
					$data['lang'] = $lang;
					$data['template_type'] = $type;
					db_perform('templates', $data);
				};
			};
		};
		Header("Location: index.php");
		die();
	};
/*
	// load Templates
	foreach (array('en', 'jp') as $lang) {
		foreach (array_keys($types) as $type) {
			if ($file = file_get_contents("../includes/templates/errors/$lang/$type.html")) {
				$content[$lang][$type] = $file;
			} else {
				$content[$lang][$type] = 'No template found';
			}
		};
	};
*/
	// select DB templates
	if (empty($templates)) {
		$sql = "SELECT * FROM templates WHERE site_id = '0' and template_type in ('".implode("','", array_keys($types))."');";
		$res = mysql_query($sql) or die(mysql_error());
		while ($row = mysql_fetch_assoc($res)) {
			$templates[$row['template_type']][$row['lang']] = $row;
			$content[$row['lang']][$row['template_type']] = $row['template_content'];
		};
		mysql_free_result($res);
	};
?>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">

<script src="js/functions.js" type="text/javascript"></script>

<?php include '../includes/head_scripts.php'; ?>
<style>
#total-container * {
    box-sizing: border-box;
    -moz-box-sizing: border-box;
    -webkit-box-sizing: border-box;
    -ms-box-sizing: border-box;
    font-family: helvetica,sans-serif;
    font-size: 12px;
}
#total-container h1 {
	font-size: 20px;
}
#total-container h2 {
	font-size: 16px;
}
#total-container {
    width: 700px;
    text-align: center;
    margin-left: auto;
    margin-right: auto;
}
#total-container * div {
	width: 100%;
}
#total-container * input {
	width: 100%;
}
#total-container * textarea {
	width: 100%;
	height: 300px;
}
#total-container div.field-caption {
	text-align: left;
}
#buttons {
    clear: both;
    margin-top: 30px;
    margin-bottom: 60px;
    height: 49px;
    border: 1px solid #EEEEEE;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
}
#buttons button {
    float: right;
    background-color: #FFFF66;
    border: 1px solid black;
    border-radius: 5px;
    -moz-border-radius: 5px;
    -webkit-border-radius: 5px;
    font-size: 14pt;
    padding: 5px;
    margin: 5px;
    padding-left: 10px;
    padding-right: 10px;
}
#buttons #back {
    float:left;
}
</style>

<title>Bungy Japan :: User Management</title>

<script>
$(document).ready(function () {
    $('#back').click(function () {
        document.location = 'index.php';
        return false;
    });
    $('#save').click(function () {
        $('#emailTemplateForm').submit();
        return true;
    });
});
</script>

</head>
<body>
<?php include '../includes/main_menu.php'; ?>
<br />
<form method="post" name="emailForm" id="emailTemplateForm">
<div id="total-container">
	<h1>User Confirmation Popup Templates</h1>
	<?php 
	foreach ($types as $type => $title) {
		echo "<div class=\"template-caption\"><h2>$title PopUp Template</h2></div>";
		foreach (array('en' => 'English', 'jp' => 'Japanese') as $lang => $lang_title) {
			$enc = mb_detect_encoding($content[$lang][$type]);
			if ($enc == 'SJIS') {
				$content[$lang][$type] = mb_convert_encoding($content[$lang][$type], 'SJIS', 'UTF-8');
			};
	?>
	<div class="field-caption"><?php echo $lang_title; ?> Template</div>
	<div class="field-value textarea"><textarea name="<?php echo $lang; ?>[<?php echo $type; ?>]"><?php echo htmlspecialchars($content[$lang][$type]); ?></textarea></div>
	<br />
	<?php
		};
		echo "<br />";
	};
	?>
  	<div id="buttons">
    	<button id="back">Back</button>
    	<button id="save">Save</button>
  	</div>

</div>
</form>
</body>
</html>
