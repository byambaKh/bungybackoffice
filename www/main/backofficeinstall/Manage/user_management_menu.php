<?php
	$user = new BJUser();
?>
<div style="width: 500px; margin-right: auto; margin-left: auto; text-align: center; margin-bottom: 20px;">
<?php echo showLink("manageSites.php", 'Sites'); ?> | 
<?php echo showLink("manageUsers.php", 'Users'); ?> | 
<?php echo showLink("manageAccessTypes.php", 'Access Types'); ?> | 
<?php echo showLink("managePermissions.php", 'Permissions'); ?> |
<?php echo showLink("manageRoles.php", 'Roles'); ?> 
</div>
<?php 
	function showLink($url, $name) {
		$urlself = ($url == basename($_SERVER['PHP_SELF']));
		if (!$urlself) {
			echo "<a href='$url'>";
		};
		echo $name;
		if (!$urlself) {
			echo "</a>";
		};
	}
?>
