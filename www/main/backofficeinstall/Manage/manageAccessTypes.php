<?php
	include ("../includes/application_top.php");

	$user = new BJUser();
	$current_id = '';
	$action_result = "";

	if (!empty($_POST)) {
		switch (TRUE) {
			case isset($_POST['update']): 
			case isset($_POST['addaccesstype']): 
				$type = 'insert';
				$parameters = '';
				if (!empty($_POST['id'])) {
					$type = 'update';
					$parameters = 'id=' . (int)$_POST['id'];
				};
				$action_result = $type;
				$data = array (
					'type_name'	=> $_POST['type_name'],
					'alias'		=> $_POST['alias'],
				);
				db_perform('access_types', $data, $type, $parameters);
				break;
		};
	} else {
		switch (TRUE) {
			case array_key_exists('action', $_GET):
				if ($_GET['action'] == 'delete') {
					$current_id = (int)$_GET['id'];
					$sql = "DELETE FROM access_types WHERE id = '$current_id'";
					mysql_query($sql);
					$current_id = '';
					$action_result = 'delete';
				} elseif ($_GET['action'] == 'edit') {
					$current_id = (int)$_GET['id'];
				};
				break;
		};
	};
	if (!empty($action_result)) {
		$_SESSION['result_message'] = $action_result;
		Header("Location: manageAccessTypes.php");
		die();
	};
	// empty place record
	$pinfo = array(
		'id'		=> '',
		'type_name'	=> '',
		'alias'		=> '',
	);
	if (!empty($current_id)) {
		$sql = "select * from access_types WHERE id = ". (int)$current_id;
		$res = mysql_query($sql);
		if ($res) {
			$row = mysql_fetch_assoc($res);
			$pinfo = $row;
		};
	};


?>
<!DOCTYPE html>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">

<script src="js/functions.js" type="text/javascript"></script>

<?php include '../includes/head_scripts.php'; ?>
<style>
.access-types-table-row1 {
	background-color: #CCCCFF;
}
.access-types-table-row2 {
	background-color: #CCFFCC;
}
#system-message {
	background-color: #55FF55;
	text-align: center;
	line-height: 150%;
}
.place-access-type {
	background-color: #fdd;
	padding: 20px;
}
.place-access-type h2 {
	margin: 0px;
	padding:0px;
}
.access-types-table-header {
	background-color: #fdd;
}
td {
	vertical-align: top;
}
td small {
	color: green;
}
</style>

<title>Bungy Japan :: Access Types Management</title>

<script type="text/javascript">
function procAdd(){
	if(document.uForm.usrnm.value == ''){
		alert("User Name is required!!!");
		return false;
	}
	if(document.uForm.pwds.value == ''){
		alert("Password is required!!!");
		return false;
	}
	document.uForm.submit();
	return true;
}
function procBack(){
	if(document.getElementById('back') ){
		document.uForm.action = "index.php";
		document.uForm.submit();
	}
}
function deleteConfirm(id, typename) {
	var answer = window.confirm('Are you sure to delete "'+typename+'" access type?') 
	if (answer == true) {
		document.location = "manageRoles.php?action=delete&id=" + id;
	};
	return answer;
}
</script>
</head>
<body>
<?php include '../includes/main_menu.php'; ?>
<br />
<?php
 if (array_key_exists('result_message', $_SESSION) && !empty($_SESSION['result_message'])) {
	echo "<div id='system-message'>";
	switch ($_SESSION['result_message']) {
		case "insert":
			$message = "Access Type added";
			break;
		case "update":
			$message = "Access Type updated";
			break;
		case "delete":
			$message = "Access Type deleted";
			break;
	};
	echo $message;
	echo "</div>";
	unset($_SESSION['result_message']);
 };
?>
<form method="post" name="uForm">
<?php include "user_management_menu.php"; ?>
<table align="center" width="480px" bgcolor="#D3D3D3">
	<tbody>
		<tr>
			<td colspan="8" align="center" bgcolor="#FFA500"><b>Access Type Management</b>
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500">Access Type ID:</td>
			<td align="left" bgcolor="#FFA500">
				<?php echo $pinfo['id']; ?>
				<input type="hidden" name="id" value="<?php echo $pinfo['id']; ?>">
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500">Access Type Name:</td>
			<td align="left" bgcolor="#FFA500">
				<input type="text" name="type_name" value="<?php echo $pinfo['type_name']; ?>">
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500" >Access Type Alias:</td>
			<td align="left" bgcolor="#FFA500">
				<input type="text" name="alias" value="<?php echo $pinfo['alias']; ?>">
			</td>
		</tr>
		<tr>
			<td colspan="8" align="right" bgcolor="#FFA500">
<?php if ($pinfo['id'] != '') { ?>
				<input type="submit" name="update" id="update-user" value="Update Access Type">
<?php } else { ?>
				<input type="submit" name="addaccesstype" id="add-user" value="Add Access Type" onclick="procAdd();">
<?php }; ?>
				<input type="button" name="back" id="back" value="Back" onclick="procBack();">
			</td>

			
		</tr>
	</tbody>
</table>

<table align="center" width="600" id="access-types-table">
<tr>
	<th class="header" colspan="7"><h2>Access Types</h2></th>
</tr>
<tr class="access-types-table-header">
	<th class="access-types-table-typename">Access Type Name</th>
	<th class="access-types-table-alias">Access Type Alias</th>
	<th class="access-types-table-actions">Manage</th>
</tr>

<?php
	$sql = "select * from access_types order by type_name desc";
	$res = mysql_query($sql);
	$i = 1;
	$at = '';
	while($row = mysql_fetch_assoc($res)) {
		$i = !$i;
?>
	<tr class="access-types-table-row<?php echo ($i+1); ?>">
		<td class="access-types-table-typename"><?php echo $row['type_name']; ?></td>
		<td class="access-types-table-alias"><?php echo $row['alias']; ?></td>
		<td class="access-types-table-actions">
			<a href="?action=edit&id=<?php echo $row['id']; ?>">Edit</a> | 
			<a class="place-delete" href="#" atid="<?php echo $row['id']; ?>" typename="<?php echo $row['type_name']; ?>">Delete</a>
		</td>
	</tr>
<?php
	};
?>
</table>
<script>
$(document).ready(function (){
	$('.place-delete').click(function (event){
		event.preventDefault();
		return deleteConfirm($(this).attr('atid'), $(this).attr('typename'));
	});
});
</script>

</form>
</body>
</html>
