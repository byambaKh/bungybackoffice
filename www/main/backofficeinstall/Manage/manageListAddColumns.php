<?php
	include ("../includes/application_top.php");
	$action = '';
	$record_type = 'lists_add_columns';
	if (isset($_POST['action'])) {
		$action = $_POST['action'];
	};
	if (empty($action) && isset($_GET['action'])) {
		$action = $_GET['action'];
	};
	switch ($action) {
		case 'add':
		case 'Add Column':
			$data = array(
				'list_id'	=> $_POST['list_id'],
				'name'		=> $_POST['name'],
				'alias'		=> $_POST['alias']
			);
			db_perform('lists_add_columns', $data);
			Header('Location: manageListAddColumns.php?list_id=' . $_POST['list_id']);
			die();
			break;
		case 'delete':
			$id = (int)$_GET['id'];
			$sql = "DELETE FROM lists_add_columns WHERE id = '$id'";
			$res = mysql_query($sql);
			$sql = "DELETE FROM lists_add_items WHERE column_id = '$id'";
			$res = mysql_query($sql);
			Header('Location: manageListAddColumns.php?list_id=' . $_GET['list_id']);
			die();
			
	};
	
    // select available lists
    $sql = "SELECT l.id, l.name, g.name as group_name FROM lists l LEFT JOIN lists_groups g on (l.group_id = g.id) order by l.name;";
    $res = mysql_query($sql) or die(mysql_error());
    $lists = array();
    $list_found = false;
    $first_list = null;
    while ($row = mysql_fetch_assoc($res)) {
        if (!array_key_exists($row['group_name'], $lists)) {
            $lists[$row['group_name']] = array();
        };
        if (is_null($first_list)) {
            $first_list = $row;
        };
        $lists[$row['group_name']][] = array(
            'id'    => $row['id'],
            'text'  => $row['name'],
        );
        if ($list_id == $row['id']) {
            $list_found = true;
            $list_name = $row['name'];
        };
    }
    if (!$list_found) {
        $list_id = $first_list['id'];
        $list_name = $first_list['name'];
    };

?>
<html>
<head>
<title>List Manager - Additional Columns</title>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">

<script src="js/functions.js" type="text/javascript"></script>

<?php include '../includes/head_scripts.php'; ?>
<style>
table textarea {
	width: 100%;
	height: 400px;
}
button {
	border: 1px solid silver;
	background-color: yellow;
	border-radius: 5px;
	padding: 5px;
	margin: 5px;
	font-size: 20px;
	font-family: Arial;
}
#back {
	margin-left: 0px !important;
	float: left;
}
#update {
	margin-right: 0px !important;
	float: right;
}
#list-groups {
	max-width: 800px;
	margin-left: auto;
	margin-right: auto;
	width: 600px;
	border-collapse: collapse;
}
#list-groups td, #list-groups th {
	border: 1px black solid;
	padding: 3px;
	font-family: Arial;
	font-size: 12px;
}
.add, .lists, .edit, .delete, .values, .update, .cancel {
	margin: 0px !important;
	padding: 2px !important;
	border-radius: 2px;
	font-size: 12px;
	margin-left: 2px !important;
	margin-right: 2px !important;
}
input {
	width: 98%;
}
.no-borders {
	border: none !important;
}
.header {
	background-color: #FFA500;
	border: 1px solid #FFA500 !important;
	border-bottom: 1px solid black !important;
	text-align: center;
	padding: 5px;
}
</style>
</head>
<?php include 'row_edit_functions.php'; ?>
<script>
    var fvalues = {'name':[], 'alias':[]};

	$(document).ready(function () {
		$('#back').on('click', function () {
			document.location = '/Manage/manageListGroups.php';
			return false;
		});
		$(".delete").click(row_delete);
		$(".values").click(function () {
			document.location = 'manageListValues.php?list_id=' + $(this).parents('tr').attr('custom-id');
		});
        $('.edit').unbind('click').click(row_change);
		$('#list-id').change(change_list);
	});
    function row_delete() {
        if (window.confirm("Are you sure to delete this Column?")) {
            document.location = 'manageListAddColumns.php?action=delete&list_id=<?php echo $list_id; ?>&id=' + $(this).parents('tr').attr('custom-id');
        };
    }
	function change_list() {
		document.location = 'manageListAddColumns.php?list_id=' + $(this).val();
	}
</script>
<body>
<?php include '../includes/main_menu.php'; ?>
<br />
<?php
 if (array_key_exists('result_message', $_SESSION) && !empty($_SESSION['result_message'])) {
	echo "<div id='system-message'>";
	switch ($_SESSION['result_message']) {
		case "insert":
			$message = "Item added";
			break;
		case "update":
			$message = "Item updated";
			break;
		case "delete":
			$message = "Item deleted";
			break;
	};
	echo $message;
	echo "</div>";
	unset($_SESSION['result_message']);
 };
?>
<?php include "lists_navigation.php"; ?>
<table id="list-groups">
	<tr>
		<td colspan="4" class="header">
			<h1><?php echo SYSTEM_SUBDOMAIN; ?> - List Manager - Additional Columns</h1>
List:
<?php
    echo "<select name='list_id' id='list-id'>\n";
    foreach ($lists as $group => $values) {
        echo "\t<optgroup label='$group'>\n";
        $options_exists = false;
        foreach ($values as $list) {
            $options_exists = true;
            $selected = ($list['id'] == $list_id) ? ' selected' : '';
            echo "\t\t<option value='{$list['id']}'$selected>{$list['text']}</option>\n";
        };
        if (!$options_exists) {
            echo "\t\t<option disabled>No lists</option>\n";
        };
        echo "\t</optgroup>\n";
    };
    echo "</select>"
?>
		</td>
	</tr>
	<tr>
		<th class="group-id">ID</th>
		<th class="group-name">Column Name</th>
		<th class="group-alias">Column Alias</th>
		<th class="group-actions">Actions</th>
	</tr>
<?php
	$sql = "SELECT * FROM lists_add_columns WHERE list_id = '$list_id' ORDER by name ASC;";
	$res = mysql_query($sql) or die(mysql_error());
	while ($row = mysql_fetch_assoc($res)) {
		echo "\t<tr custom-id='{$row['id']}'>\n";
		echo "\t\t<td class='group-id'>{$row['id']}</td>\n";
		echo "\t\t<td class='group-name value' rel='name'>{$row['name']}</td>\n";
		echo "\t\t<td class='group-alias value' rel='alias'>{$row['alias']}</td>\n";
		echo "\t\t<td class='group-actions'><button class='edit'>Edit</button><button class='delete'>Delete</button></td>\n";
		echo "\t</tr>\n";
	}
?>
	<tr>
		<th colspan="4">New Column</th>
	</tr>
<form method="post">
	<tr>
		<td class="group-id">&nbsp;</td>
		<td class="group-name"><input type="text" name="name" value=""></td>
		<td class="group-alias"><input type="text" name="alias" value=""></td>
		<td class="group-actions"><input type="hidden" name="list_id" value="<?php echo $list_id; ?>"><button class="add" name="action" value="add" type="submit">Add Column</button></td>
	</tr>
</form>
<tr>
<td colspan="4" class="no-borders">
<button id="back">Back</button>
</td>
</tr>
</table>
</body>
</html>
