<?php
 include "../includes/application_top.php";

function getWeightSwitch()
{
    $site_id = CURRENT_SITE_ID;
    $sql = "SELECT takeWeight FROM bungyjapan_book_main.sites WHERE id = $site_id;";
    $res = mysql_query($sql) or die(mysql_error());
    $data = mysql_fetch_row($res);
    if ($data[0] == true) echo "checked";
}

?>
<html>
    <body>
        <div>
            <input type="checkbox" id="takeWeight" name="feature" value="takeWeight" <?php getWeightSwitch();?>/>
            <label for="takeWeight">Take weight</label>
            <button type="button" onclick="submit(takeWeight.checked)">Save</button>
            <button type="button" onclick="location.href='./';">Back</button>
            <span id="message"></span>
            <script>
                function submit(str) 
                {
                    var xmlhttp = new XMLHttpRequest();
                    xmlhttp.onreadystatechange = function() 
                    {
                        if (this.readyState == 4 && this.status == 200) 
                        {
                            document.getElementById("message").innerHTML = this.responseText;
                        }
                    };
                    xmlhttp.open("GET", "switchWeightRequest.php?checked=" + str, true);
                    xmlhttp.send();
                    document.getElementById("message").innerHTML = "Saved";
                }
            </script>
        </div>
    </body>
</html>