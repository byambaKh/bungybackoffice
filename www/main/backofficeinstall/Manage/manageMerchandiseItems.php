<?php
	include ("../includes/application_top.php");

	$site_id = CURRENT_SITE_ID;

	$current_mid = '';
	$action_result = "";

	if (!empty($_POST)) {
		switch (TRUE) {
			case isset($_POST['update']): 
			case isset($_POST['additem']): 
				// update sort order of all items
				if (isset($_POST['additem'])) {
					$sql = "update merchandise_items SET sort_order = sort_order+1 WHERE site_id = '$site_id' AND sort_order >= {$_POST['sort_order']};";
					mysql_query($sql);
				};
				$type = 'insert';
				$parameters = '';
				if (!empty($_POST['mid'])) {
					$type = 'update';
					$parameters = 'id=' . (int)$_POST['mid'];
				};
				$action_result = $type;
				$data = array (
					'site_id'	=> $site_id,
					'name'		=> $_POST['name'],
					'price'		=> $_POST['price'],
					'color'		=> $_POST['color'],
					'sort_order'	=> $_POST['sort_order']
				);
				db_perform('merchandise_items', $data, $type, $parameters);
				break;
		};
	} else {
		switch (TRUE) {
			case array_key_exists('action', $_GET):
				if ($_GET['action'] == 'delete') {
					$current_mid = (int)$_GET['mid'];
					$sql = "select sort_order from merchandise_items where id = '$current_mid';";
					$res = mysql_query($sql) or die(mysql_error());
					if ($row = mysql_fetch_assoc($res)) {
						$sort_order = $row['sort_order'];
						$sql = "update merchandise_items SET sort_order = sort_order-1 WHERE site_id = '$site_id' AND sort_order > {$sort_order};";
						mysql_query($sql);
					};
					$sql = "DELETE FROM merchandise_items WHERE id = '$current_mid'";
					mysql_query($sql);
					$current_mid = '';
					$action_result = 'delete';
				} elseif ($_GET['action'] == 'edit') {
					$current_mid = (int)$_GET['mid'];
				};
				break;
		};
	};
	if (!empty($action_result)) {
		$_SESSION['result_message'] = $action_result;
		Header("Location: manageMerchandiseItems.php");
		die();
	};
	// empty item record
	$minfo = array(
		'id'	=> '',
		'name'	=> '',
		'price'	=> '',
		'color'	=> '',
		'sort_order'=> '0',
	);
	if (!empty($current_mid)) {
		$sql = "select * from merchandise_items WHERE id = ". (int)$current_mid;
		$res = mysql_query($sql);
		if ($res) {
			$item_row = mysql_fetch_assoc($res);
			$minfo = $item_row;
		};
	};


?>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">

<script src="js/functions.js" type="text/javascript"></script>

<?php include '../includes/head_scripts.php'; ?>
<style>
.mitems-table-row1 {
	background-color: #CCCCFF;
}
.mitems-table-row2 {
	background-color: #CCFFCC;
}
#system-message {
	background-color: #55FF55;
	text-align: center;
	line-height: 150%;
}
.item-access-type {
	background-color: #fdd;
	padding: 20px;
}
.item-access-type h2 {
	margin: 0px;
	padding:0px;
}
.mitems-table-header {
	background-color: #fdd;
}
</style>

<title>Bungy Japan :: Merchandise Items Management</title>

</head>
<script type="text/javascript">
function procAdd(){
    if(document.miForm.name.value == ''){
        alert("Item Name is required!!!");
        return false;
    }
    if(document.miForm.price.value == ''){
        alert("Item Price is required!!!");
        return false;
    }
    document.miForm.submit();
    return true;
}
function procBack(){
	if(document.getElementById('bck') ){
		document.miForm.action = "index.php";
		document.miForm.submit();
	}
}
function deleteConfirm(mid, name) {
	var answer = window.confirm('Are you sure to delete "'+name+'" item?') 
	if (answer == true) {
		document.location = "manageMerchandiseItems.php?action=delete&mid=" + mid;
	};
	return answer;
}
</script>
<body>
<?php include '../includes/main_menu.php'; ?>
<br />
<?php
 if (array_key_exists('result_message', $_SESSION) && !empty($_SESSION['result_message'])) {
	echo "<div id='system-message'>";
	switch ($_SESSION['result_message']) {
		case "insert":
			$message = "Item added";
			break;
		case "update":
			$message = "Item updated";
			break;
		case "delete":
			$message = "Item deleted";
			break;
	};
	echo $message;
	echo "</div>";
	unset($_SESSION['result_message']);
 };
?>
<form method="post" name="miForm">
<table align="center" width="480px" bgcolor="#D3D3D3">
	<tbody>
		<tr>
			<td colspan="8" align="center" bgcolor="#FFA500"><b>Merchandise Items Management</b>
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500">Item ID:</td>
			<td align="left" bgcolor="#FFA500">
				<?php echo $minfo['id']; ?>
				<input type="hidden" name="mid" value="<?php echo $minfo['id']; ?>">
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500">Item Name:</td>
			<td align="left" bgcolor="#FFA500">
				<input type="text" name="name" value="<?php echo $minfo['name']; ?>">
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500" >Item Price :</td>
			<td align="left" bgcolor="#FFA500">
				<input type="input" name="price" value="<?php echo $minfo['price']; ?>"> &yen;
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500" >Row Color :</td>
			<td align="left" bgcolor="#FFA500">
<?php
	$colors = array(
		array(
			'id'	=> 'silver',
			'text'	=> 'silver'
		),
		array(
			'id'	=> '#afa',
			'text'	=> 'green'
		),
		array(
			'id'	=> '#bbf',
			'text'	=> 'blue'
		),
	);
	echo draw_pull_down_menu('color', $colors, $minfo['color']);
?>
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500" >Sort Order:</td>
			<td align="left" bgcolor="#FFA500">
				<input type="text" name="sort_order" value="<?php echo $minfo['sort_order']; ?>">
			</td>
		</tr>
	
		<tr>
			<td colspan="8" align="right" bgcolor="#FFA500">
<?php if ($minfo['id'] != '') { ?>
				<input type="submit" name="update" id="update-item" value="Update Item">
<?php } else { ?>
				<input type="submit" name="additem" id="add-item" value="Add Item" onclick="procAdd();">
<?php }; ?>
				<input type="button" name="bck" id="bck" value="Back" onclick="procBack();">
			</td>

			
		</tr>
	</tbody>
</table>

<table align="center" width="600" id="mitems-table">
<tr class="mitems-table-header">
	<th class="mitems-table-delete">X</th>
	<th class="mitems-table-name">Item Name</th>
	<th class="mitems-table-price">Item Price</th>
	<th class="mitems-table-sort">Sort</th>
	<th class="mitems-table-actions">Manage</th>
</tr>

<?php
	$sql = "select * from merchandise_items WHERE site_id = '$site_id' order by sort_order asc, name asc";
	$res = mysql_query($sql) or die(mysql_error());
	$i = 1;
	$at = '';
	while($row = mysql_fetch_assoc($res)) {
		$i = !$i;
?>
	<tr class="mitems-table-row<?php echo ($i+1); ?>"<?php if (!empty($row['color'])) echo "style=\"background-color: {$row['color']}\""; ?>>
		<td class="mitems-table-delete"><input name="mid[]" value="<?php echo $row['id']; ?>" type="checkbox"></td>
		<td class="mitems-table-name"><?php echo $row['name']; ?></td>
		<td class="mitems-agent-price">&yen; <?php echo $row['price']; ?></td>
		<td class="mitems-agent-sort"><?php echo $row['sort_order']; ?></td>
		<td class="mitems-table-actions">
			<a href="?action=edit&mid=<?php echo $row['id']; ?>">Edit</a> | 
			<a class="item-delete" href="#" mid="<?php echo $row['id']; ?>" name="<?php echo $row['name']; ?>">Delete</a>
		</td>
	</tr>
<?php
	};
?>
</table>
<script>
$(document).ready(function (){
	$('.item-delete').click(function (event){
		event.preventDefault();
		return deleteConfirm($(this).attr('mid'), $(this).attr('name'));
	});
});
</script>

</form>
</body>
</html>
