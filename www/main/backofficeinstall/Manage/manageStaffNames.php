<?php
	include ("../includes/application_top.php");
	mysql_set_charset('utf8');

	$current_uid = '';
	$action_result = "";

	if (!empty($_POST)) {
		switch (TRUE) {
			case isset($_POST['update']): 
			case isset($_POST['adduser']): 
				$type = 'insert';
				$parameters = '';
				if (!empty($_POST['userid'])) {
					$type = 'update';
					$parameters = 'id=' . (int)$_POST['userid'];
				};
				$action_result = $type;
				$data = array (
					'name'		=> $_POST['name']
				);
				db_perform('staff', $data, $type, $parameters);
				break;
		};
	} else {
		switch (TRUE) {
			case array_key_exists('action', $_GET):
				if ($_GET['action'] == 'delete') {
					$current_uid = (int)$_GET['uid'];
					$sql = "DELETE FROM staff WHERE id = '$current_uid'";
					mysql_query($sql);
					$current_uid = '';
					$action_result = 'delete';
				} elseif ($_GET['action'] == 'edit') {
					$current_uid = (int)$_GET['uid'];
				};
				break;
		};
	};
	if (!empty($action_result)) {
		$_SESSION['result_message'] = $action_result;
		Header("Location: manageStaffNames.php");
		die();
	};
	// empty user record
	$uinfo = array(
		'id'	=> '',
		'name'	=> ''
	);
	if (!empty($current_uid)) {
		$sql = "select * from staff WHERE id = ". (int)$current_uid;
		$res = mysql_query($sql);
		if ($res) {
			$user_row = mysql_fetch_assoc($res);
			$uinfo = $user_row;
		};
	};


?>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">

<script src="js/functions.js" type="text/javascript"></script>

<?php include '../includes/head_scripts.php'; ?>
<style>
.users-table-row1 {
	background-color: #CCCCFF;
}
.users-table-row2 {
	background-color: #CCFFCC;
}
#system-message {
	background-color: #55FF55;
	text-align: center;
	line-height: 150%;
}
</style>

<title>Bungy Japan :: User Management</title>

</head>
<script type="text/javascript">
function procAdd(){
	if(document.uForm.usrnm.value == ''){
		alert("User Name is required!!!");
		return false;
	}
	if(document.uForm.pwds.value == ''){
		alert("Password is required!!!");
		return false;
	}
	document.uForm.submit();
	return true;
}
function procBack(){
	if(document.getElementById('bck') ){
		document.uForm.action = "index.php";
		document.uForm.submit();
	}
}
function deleteConfirm(uid, username) {
	var answer = window.confirm('Are you sure to delete "'+username+'" user?') 
	if (answer == true) {
		document.location = "manageStaffNames.php?action=delete&uid=" + uid;
	};
	return answer;
}
</script>
<body>
<?php include '../includes/main_menu.php'; ?>
<br />
<?php
 if (array_key_exists('result_message', $_SESSION) && !empty($_SESSION['result_message'])) {
	echo "<div id='system-message'>";
	switch ($_SESSION['result_message']) {
		case "insert":
			$message = "Staff Member added";
			break;
		case "update":
			$message = "Staff Member updated";
			break;
		case "delete":
			$message = "Staff Member deleted";
			break;
	};
	echo $message;
	echo "</div>";
	unset($_SESSION['result_message']);
 };
?>
<form method="post" name="uForm">
<table align="center" width="480px" bgcolor="#D3D3D3">
	<tbody>
		<tr>
			<td colspan="8" align="center" bgcolor="#FFA500"><b>Staff Member Management</b>
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500">Staff Member ID:</td>
			<td align="left" bgcolor="#FFA500">
				<?php echo $uinfo['id']; ?>
				<input type="hidden" name="userid" value="<?php echo $uinfo['id']; ?>">
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500">Staff Member Name:</td>
			<td align="left" bgcolor="#FFA500">
				<input type="text" name="name" value="<?php echo $uinfo['name']; ?>">
			</td>
		</tr>
		<tr>
			<td colspan="8" align="right" bgcolor="#FFA500">
<?php if ($uinfo['id'] != '') { ?>
				<input type="submit" name="update" id="update-user" value="Update Staff Member">
<?php } else { ?>
				<input type="submit" name="adduser" id="add-user" value="Add Staff Member" onclick="procAdd();">
<?php }; ?>
				<input type="button" name="bck" id="bck" value="Back" onclick="procBack();">
			</td>

			
		</tr>
	</tbody>
</table>

<table align="center" width="480" id="users-table">
<tr id="users-table-header">
	<th class="users-table-username">Staff Member Name</th>
	<th class="users-table-actions">Manage</th>
</tr>

<?php
	$sql = "select * from staff order by name asc";
	$res = mysql_query($sql);
	$i = 1;
	while($row = mysql_fetch_assoc($res)) {
		$i = !$i;
?>
	<tr class="users-table-row<?php echo ($i+1); ?>">
		<td class="users-table-username"><?php echo $row['name']; ?></td>
		<td class="users-table-permissions"><?php echo $row['access_type']; ?></td>
		<td class="users-table-actions">
			<a href="?action=edit&uid=<?php echo $row['id']; ?>">Edit</a> | 
			<a class="user-delete" href="#" uid="<?php echo $row['id']; ?>" username="<?php echo $row['name']; ?>">Delete</a>
		</td>
	</tr>
<?php
	};
?>
</table>
<script>
$(document).ready(function (){
	$('.user-delete').click(function (event){
		event.preventDefault();
		return deleteConfirm($(this).attr('uid'), $(this).attr('username'));
	});
});
</script>

</form>
</body>
</html>
