<?php
	include ("../includes/application_top.php");

	$current_id = '';
	$action_result = "";
	$cyear = date('Y');

    if (isset($_GET['year'])) {
        $cyear = $_GET['year'];
    };

	if (!empty($_POST)) {
		$cyear = $_POST['year'];
		switch (TRUE) {
			case isset($_POST['update']): 
			case isset($_POST['addjumps']): 
				$type = 'insert';
				$parameters = '';
				if (!empty($_POST['id'])) {
					$type = 'update';
					$parameters = 'id=' . (int)$_POST['id'];
				};
				$action_result = $type;
				$data = array (
					'user_id'		=> $_POST['user_id'],
					'jumps'			=> $_POST['jumps'],
					'year'			=> $cyear,
				);
				db_perform('free_jumps', $data, $type, $parameters);
				break;
		};
	} else {
		switch (TRUE) {
			case array_key_exists('action', $_GET):
				if ($_GET['action'] == 'delete') {
					$current_id = (int)$_GET['id'];
					$sql = "DELETE FROM free_jumps WHERE id = '$current_id'";
					mysql_query($sql);
					$current_id = '';
					$action_result = 'delete';
				} elseif ($_GET['action'] == 'edit') {
					$current_id = (int)$_GET['id'];
				};
				$cyear = $_GET['year'];
				$_POST['year'] = $_GET['year'];
				break;
		};
	};
	if (!empty($action_result)) {
		$_SESSION['result_message'] = $action_result;
		Header("Location: manageFreeJumps.php?year=" . $_POST['year']);
		die();
	};
	// empty user record
	$jinfo = array(
		'id'	=> '',
		'jumps'	=> '',
		'year'	=> '',
	);
	if (!empty($current_id)) {
		$sql = "select * from free_jumps WHERE id = ". (int)$current_id;
		$res = mysql_query($sql);
		if ($res) {
			$jumps_row = mysql_fetch_assoc($res);
			$jinfo = $jumps_row;
		};
	};
	// get all users for drop down
	$users_values = BJHelper::getStaffList();
	$users = array();
	foreach ($users_values as $u) {
		$users[$u['id']] = $u['text'];
	};
	$year = $cyear;
	$year_values = array();
	for ($i = 2010; $i <= date('Y') + 1; $i++) {
		$year_values[] = array(
			'id'		=> $i,
			'text'		=> $i
		);
	};
?>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">

<script src="js/functions.js" type="text/javascript"></script>

<?php include '../includes/head_scripts.php'; ?>
<style>
.users-table-row1 {
	background-color: #CCCCFF;
}
.users-table-row2 {
	background-color: #CCFFCC;
}
#system-message {
	background-color: #55FF55;
	text-align: center;
	line-height: 150%;
}
.user-access-type {
	background-color: #fdd;
	padding: 20px;
}
.user-access-type h2 {
	margin: 0px;
	padding:0px;
}
.users-table-header {
	background-color: #fdd;
}
</style>

<title>Bungy Japan :: Free Jumps Management</title>

</head>
<script type="text/javascript">
function procAdd(){
	if(document.jForm.usrnm.value == ''){
		alert("Staff Name is required!!!");
		return false;
	}
	if(document.jForm.pwds.value == ''){
		alert("Password is required!!!");
		return false;
	}
	document.jForm.submit();
	return true;
}
function procBack(){
	if(document.getElementById('back') ){
		document.jForm.action = "index.php";
		document.jForm.submit();
	}
}
function deleteConfirm(id, username) {
	var answer = window.confirm('Are you sure to delete "'+username+'" jumps?') 
	if (answer == true) {
		document.location = "manageFreeJumps.php?action=delete&id=" + id + '&year=' + $('#current-year').val();
	};
	return answer;
}
$(document).ready(function () {
	$("#current-year").on('change', function () {
		$('#dForm').submit();
	});
});
</script>
<body>
<?php include '../includes/main_menu.php'; ?>
<br />
<?php
 if (array_key_exists('result_message', $_SESSION) && !empty($_SESSION['result_message'])) {
	echo "<div id='system-message'>";
	switch ($_SESSION['result_message']) {
		case "insert":
			$message = "Free Jumps added";
			break;
		case "update":
			$message = "Free Jumps updated";
			break;
		case "delete":
			$message = "Free Jumps deleted";
			break;
	};
	echo $message;
	echo "</div>";
	unset($_SESSION['result_message']);
 };
?>
<form method="post" name="dForm" id="dForm">
<table align="center" width="480px" bgcolor="#D3D3D3">
	<tr>
		<td colspan="4" align="center" bgcolor="#FFA500"><b>Please select year</b></td>
	</tr>
	<tr>
		<td align="right" bgcolor="#FFA500">Year:</td>
		<td align="left" bgcolor="#FFA500">
			<?php echo draw_pull_down_menu('year', $year_values, $year, 'id="current-year"'); ?>
		</td>
	</tr>
</table>
</form>
<form method="post" name="jForm">
<table align="center" width="480px" bgcolor="#D3D3D3">
	<tbody>
		<tr>
			<td colspan="8" align="center" bgcolor="#FFA500"><b>Free Jumps Management</b></td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500">Rec ID:</td>
			<td align="left" bgcolor="#FFA500">
				<?php echo $jinfo['id']; ?>
				<input type="hidden" name="id" value="<?php echo $jinfo['id']; ?>">
				<input type="hidden" name="year" value="<?php echo $year; ?>">
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500">Staff Name:</td>
			<td align="left" bgcolor="#FFA500">
				<?php echo draw_pull_down_menu('user_id', $users_values, $jinfo['user_id']); ?>
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500" >Free Jumps :</td>
			<td align="left" bgcolor="#FFA500">
				<input type="text" name="jumps" value="<?php echo $jinfo['jumps']; ?>">
			</td>
		</tr>
		<tr>
			<td colspan="8" align="right" bgcolor="#FFA500">
<?php if ($jinfo['id'] != '') { ?>
				<input type="submit" name="update" id="update-jumps" value="Update Jumps">
<?php } else { ?>
				<input type="submit" name="addjumps" id="add-jumps" value="Add Jumps" onclick="procAdd();">
<?php }; ?>
				<input type="button" name="back" id="back" value="Back" onclick="procBack();">
			</td>

			
		</tr>
	</tbody>
</table>

<table align="center" width="600" id="users-table">
<tr>
	<th>ID</th>
	<th>User Name</th>
	<th>Free Jumps</th>
	<th>Manage</th>
</tr>
<?php
	$sql = "select * from free_jumps where year = $cyear order by id ASC";
	$res = mysql_query($sql);
	$i = 1;
	$at = '';
	while($row = mysql_fetch_assoc($res)) {
		$i = !$i;
?>
	<tr class="users-table-row<?php echo ($i+1); ?>">
		<td class="users-table-delete"><?php echo $row['id']; ?></td>
		<td class="users-table-username"><?php echo (array_key_exists($row['user_id'], $users)) ? $users[$row['user_id']] : 'Unknown'; ?></td>
		<td class="users-table-permissions"><?php echo $row['jumps']; ?></td>
		<td class="users-table-actions">
			<a href="?action=edit&id=<?php echo $row['id']; ?>&year=<?php echo $year; ?>&year=<?php echo $year; ?>">Edit</a> | 
			<a class="user-delete" href="#" id="<?php echo $row['id']; ?>" username="<?php echo (array_key_exists($row['user_id'], $users)) ? $users[$row['user_id']] : 'Unknown'; ?>">Delete</a>
		</td>
	</tr>
<?php
	};
?>
</table>
<script>
$(document).ready(function (){
	$('.user-delete').click(function (event){
		event.preventDefault();
		return deleteConfirm($(this).attr('id'), $(this).attr('username'));
	});
});
</script>

</form>
</body>
</html>
