<?php
include("../includes/application_top.php");
$site_id = CURRENT_SITE_ID;
// if there is a POST,save and go to management console
if (!empty($_POST)) {
    $c = $_POST['c'];
    foreach ($c as $key => $value) {
        $sql = "SELECT * FROM configuration WHERE site_id = '$site_id' AND `key` = '$key';";
        $data = array('value' => $value);
        $res = mysql_query($sql) or die(mysql_error());
        if ($row = mysql_fetch_assoc($res)) {
            db_perform('configuration', $data, 'update', 'site_id=' . $site_id . ' AND `key`="' . $key . '"');
        } else {
            // get default record
            $sql = "SELECT * FROM configuration WHERE site_id = '0' and `key` = '$key';";
            $res = mysql_query($sql) or die(mysql_error());
            $data = mysql_fetch_assoc($res);
            unset($data['id']);
            $data['value'] = $value;
            $data['site_id'] = $site_id;
            db_perform('configuration', $data);
        };
    };
    Header("Location: index.php");
    die();
};

/**
 * @param $c
 */
function outputTextInput($c)
{
    echo '<input name="c[' . $c['key'] . ']" value="' . $c['value'] . '">';
}

/**
 * @param $site_id
 * @return array
 */
function loadTemplates($site_id)
{
    $sql = "SELECT * FROM configuration WHERE site_id = 0 or site_id = '$site_id' order by site_id ASC, `id` ASC, `group` ASC;";
    $res = mysql_query($sql) or die(mysql_error());
    $config = array();
    while ($row = mysql_fetch_assoc($res)) {
        $config[$row['key']] = $row;
    };
    return $config;
}

/**
 * @param $c
 * @return mixed
 */
function outputSelectInput($c)
{
    $values = array();
    if (!empty($c['values'])) $values = json_decode($c['values']);
    echo '<select name="c[' . $c['key'] . ']">';
    foreach ($values as $k => $val) {
        $selected = ($val == $c['value']) ? ' selected="selected"' : '';
        if (is_numeric($k)) $k = $val;
        echo '<option value="' . htmlspecialchars($val) . '"' . $selected . '>' . $k . '</option>';
    };
    echo '</select>';
    return $c;
}

$configEdit = loadTemplates($site_id);

?>
<html>
<head>
    <?php include 'includes/header_tags.php'; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=8">

    <script src="js/functions.js" type="text/javascript"></script>

    <?php include '../includes/head_scripts.php'; ?>
    <style>
        #total-container * {
            box-sizing: border-box;
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            -ms-box-sizing: border-box;
            font-family: helvetica, sans-serif;
            font-size: 12px;
        }

        #total-container h1 {
            font-size: 20px;
        }

        #total-container {
            width: 700px;
            text-align: center;
            margin-left: auto;
            margin-right: auto;
        }

        #total-container * div {
            width: 100%;
        }

        #total-container * input {
            width: 100%;
        }

        #total-container * textarea {
            width: 100%;
            height: 300px;
        }

        #total-container div.field-caption {
            float: left;
            text-align: right;
            width: 200px;
            line-height: 21px;
        }

        #total-container div.field-value {
            float: left;
            margin-left: 5px;
        }

        #buttons {
            clear: both;
            margin-top: 30px;
            margin-bottom: 60px;
            height: 49px;
            border: 1px solid #EEEEEE;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
        }

        #buttons button {
            float: right;
            background-color: #FFFF66;
            border: 1px solid black;
            border-radius: 5px;
            -moz-border-radius: 5px;
            -webkit-border-radius: 5px;
            font-size: 14pt;
            padding: 5px;
            margin: 5px;
            padding-left: 10px;
            padding-right: 10px;
        }

        #buttons #back {
            float: left;
        }
    </style>

    <title>Bungy Japan :: <?php echo SYSTEM_SUBDOMAIN; ?> Configuration Management</title>

    <script>
        $(document).ready(function () {
            $('#back').click(function () {
                document.location = 'index.php';
                return false;
            });
            $('#save').click(function () {
                $('#emailTemplateForm').submit();
                return true;
            });
        });
    </script>

</head>
<body>
<?php include '../includes/main_menu.php'; ?>
<br/>
<form method="post" name="emailForm" id="emailTemplateForm">
    <div id="total-container">
        <h1><?php echo SYSTEM_SUBDOMAIN; ?> Configuration</h1>

        <?php foreach ($configEdit as $c) { ?>
            <div class="field-caption"><?php echo $c['title']; ?> :</div>
            <div class="field-value">
                <?php
                switch ($c['type']) {
                    case 'select':
                        $c = outputSelectInput($c);
                        break;
                    default:
                        outputTextInput($c);
                        break;
                };
                ?></div>
            <br/>
            <br/>
        <?php }; ?>

        <div id="buttons">
            <button id="back">Back</button>
            <button id="save">Save</button>
        </div>

    </div>
</form>
</body>
</html>
