<?php
include("../includes/application_top.php");
$_GET['startDate'] = urldecode($_GET['startDate']);
$_GET['agent'] = urldecode($_GET['agent']);

?>
<!DOCTYPE html>
<html>
<head>
    <?php include 'includes/header_tags.php'; ?>
    <title>Agent Invoice</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=8">
    <script type="text/javascript">
        function processBack() {
            if (document.getElementById('back')) {
                document.agtf.action = '<?="viewAgentInvoice.php?date={$_GET['startDate']}&agent=".urlencode($_GET['agent'])?>';
            }
        }
    </script>
    <style>
        #agent-records {
            border-collapse: collapse;
            margin-left: 1%;
            margin-right: 1%;
            width: 98%;
            border: 2px solid silver;
        }

        #agent-records th, #agent-records td {
            border: 1px solid silver;
            text-align: center;
        }

        #agent-records th {
            background-color: #87CEFA;
        }

        .confirmed-totals th {
            background-color: #5aa !important;
        }

        .th-grand-total {
            text-align: right !important;
            padding-right: 10px !important;
        }

        .th-grand-total, .th-grand-total-value {
            background-color: white !important;
        }

        .checked td {
            background-color: #5aa;
            color: white;
        }

        .name {
            text-align: left !important;
            padding-left: 10px;
        }

        #transactions {
            border-collapse: collapse;
        }

        #transactions th {
            width: 150px;
            border: 2px solid black;
            background-color: #00B5F6;
        }

        #transactions td {
            text-align: center;
            border: 2px solid black;
            background-color: #99E5F6;
        }
    </style>
</head>
<body>
<form action="" method="post" name="agtf">
    <br>
    <table width="100%" align="left">
        <tbody>
        <tr>
            <td>
                <h3>INVOICE for <?php echo strtoupper($_GET['agent']); ?></h3>
            </td>
        </tr>
        <tr>
            <td align="left">
                <table id="agent-records">
                    <?php
                    $all_data = array();
                    $invoices = array();
                    $payments = array();
                    list($year, $month, $day) = explode('-', $_GET['startDate']);
                    $start = date("Y-m-d", mktime(0, 0, 0, $month, 1, $year));
                    $finish = date("Y-m-t", mktime(0, 0, 0, $month, 1, $year));
                    echo "INVOICE Date Between: " . $start . "---" . $finish . "<br /><br />";
                    $places = BJHelper::getPlaces();
                    $today = date("Y-m-d");

                    // get full invoice history
                    //$start_date = date('Y-01-01');
                    $start_date = '2014-01-01';//start calculating after from the same date that viewAgentInvoice.php starts
                    //TODO use the following two queries instead of the code
                    /*
                     //this query does the same thing as the first one in the loop, without requiring a for loop
                     SELECT
                        display_name AS site,
                        bookingdate,
                        bookingtime,
                        RomajiName AS name,

                        #Money from Jumps Agent
                        #Why are NoOfJump and RateToPayQTY different???
                        IF(CollectPay = 'Offsite', NoOfJump, RateToPayQTY) AS noofjump, #If collectPay is offsite get NoOfJump or else get RateToPayQTY

                        IF(CollectPay = 'Offsite', Rate, RateToPay) AS 'UnitCost',      #If collect pay is off site get the rate, or else get the rateToPay

                        IF(CollectPay = 'Offsite', 0, RateToPay * RateToPayQTY) AS 'TotalToPay', #... get 0, or RateToPay * RateToPayQTY

                        IF(CollectPay = 'Offsite', Rate * NoOfJump, 0) AS 'TotalToCollect',#... Rate*NoOfJump or 0

                        #Money from Cancellations Agent

                        IF(CancelFeeCollect = 'Offsite', CancelFee, 0) AS CancelFee, #...CancelFee or 0

                        IF(CancelFeeCollect = 'Offsite', CancelFeeQTY, 0) AS CancelFeeQTY,#...CancelFee Qty or 0

                        IF(CancelFeeCollect = 'Offsite', CancelFeeCollect, 0) AS CancelFeeCollect,#...CancelFeeCollect*

                        IF(CancelFeeCollect = 'Offsite', CancelFee * CancelFeeQTY, 0) AS total_cancel,#CancelFeeQTY * CancelFeeQTY or 0
                        Checked
                        from customerregs1 AS cr
                        LEFT JOIN places AS p ON (p.id = cr.site_id)
                        WHERE BookingDate between '2016-02-01' AND '2016-02-29'
                            #AND site_id = '4'
                            AND Agent = 'Asoview'
                            AND (
                        (
                            #Offsite is they owe us
                            CollectPay = 'Offsite'
                            AND NoOfJump > 0
                            AND ( Checked = 1 OR BookingDate >= '2016-03-17')
                            AND DeleteStatus = 0
                        ) OR (
                            #Onsite we owe them
                            DeleteStatus = 0
                            AND NoOfJump > 0 #Prevents group bookings from appearing multiple times
                            AND ( Checked = 1 OR BookingDate >= '2016-03-17')
                            AND RateToPay > 0
                            AND RateToPayQTY > 0
                            AND CollectPay = 'Onsite'
                        ) OR (
                            #Partial Cancellations by setting number of jumps to zero from Reception/dailyViewIE.php
                            CancelFeeCollect = 'Offsite'
                            AND NoOfJump = 0
                            )
                        )
                        ORDER BY BookingDate ASC;

                     */

                    /*
                     //this replaces the second query in the loop, and all of the calculation php
                     #http://stackoverflow.com/questions/2563918/create-a-cumulative-sum-column-in-mysql
                     Could add a cumulative sum
                     SELECT * FROM (
                        (
                        SELECT
                            site_id,
                            LAST_DAY(DATE_FORMAT(bookingdate, '%Y-%m-01')) AS `date`,
                            SUM(IF(CollectPay = 'Offsite', 0, RateToPay * RateToPayQTY)) AS 'payOut',
                            SUM(IF(CollectPay = 'Offsite', Rate * NoOfJump, 0)) AS 'TotalToCollect',
                            SUM(IF(CancelFeeCollect = 'Offsite', CancelFee * CancelFeeQTY, 0)) AS totalCancel,
                            'invoice' AS `type`
                            FROM
                                customerregs1 AS cr
                            WHERE
                                BookingDate >= '2014-01-01'
                                #AND site_id = '1'
                                AND Agent = 'Asoview'
                                AND (
                                    ( #Bookings made offsite that were completed via an agent
                                    DeleteStatus = 0
                                    AND Checked = 1
                                    AND CollectPay = 'Offsite'
                                    AND NoOfJump > 0
                                    ) OR (
                                    #Bookings made Onsite that were completed via an agent
                                    DeleteStatus = 0
                                    AND Checked = 1
                                    AND NoOfJump > 0 #prevents group bookings appearing multiple times
                                    AND RateToPay > 0
                                    AND RateToPayQTY > 0
                                    AND CollectPay = 'Onsite'
                                    ) OR (
                                    #partial cancellations through setting noOfJumps to zero in from dailyview checkin
                                    CancelFeeCollect = 'Offsite'
                                    AND NoOfJump = 0
                                    )
                                )
                            GROUP BY `date`
                            ORDER BY `date` ASC
                        )

                        UNION ALL
                        (
                        SELECT
                            site_id,
                            `date`,
                            `out` AS payOut,
                            `in` AS totalToCollect,
                            0 AS totalCancel,
                            'payment' AS `type`
                        FROM
                            banking
                        WHERE
                            company = 'Asoview'
                            AND description = 'SALES DEPOSIT FROM Rafting Company, Pension ETC For Bungy Sales'
                            #AND site_id = '10'
                            AND `date` >= '2014-01-01'
                        ORDER BY `date` ASC
                        )
                     )  AS agent_data
                     ORDER BY `date`;
                     */
                    foreach ($places as $place) {//the calculation happens on each domain
                        if (in_array($place['subdomain'], array('test'))) continue;//ignore the test domain

                        //All of this only happens on main, because there are no jumps
                        //SUM all of the jumps, cancellations
                        if ($place['subdomain'] != 'main') {//get all of the jumps for that particular agent in customerregs1
                            $sql = "SELECT
								'" . ucfirst($place['subdomain']) . "' as site,
								bookingdate,
								bookingtime,
								RomajiName as name,

								/*Money from Jumps Agent*/
								/*Why are NoOfJump and RateToPayQTY different???*/
								IF(CollectPay = 'Offsite', NoOfJump, RateToPayQTY) as noofjump, /*If collectPay is offsite get NoOfJump or else get RateToPayQTY*/
								/**/
								IF(CollectPay = 'Offsite', Rate, RateToPay) as 'UnitCost',      /*If collect pay is off site get the rate, or else get the rateToPay*/
								/**/
								IF(CollectPay = 'Offsite', 0, RateToPay * RateToPayQTY) as 'TotalToPay', /*... get 0, or RateToPay * RateToPayQTY*/
								/**/
								IF(CollectPay = 'Offsite', Rate * NoOfJump, 0) as 'TotalToCollect',/*... Rate*NoOfJump or 0*/

								/*Money from Cancellations Agent*/
								/**/
								IF(CancelFeeCollect = 'Offsite', CancelFee, 0) as CancelFee, /*...CancelFee or 0*/
								/**/
								IF(CancelFeeCollect = 'Offsite', CancelFeeQTY, 0) as CancelFeeQTY,/*...CancelFee Qty or 0*/
								/**/
								IF(CancelFeeCollect = 'Offsite', CancelFeeCollect, 0) as CancelFeeCollect,/*...CancelFeeCollect*/
								/**/
									IF(CancelFeeCollect = 'Offsite', CancelFee * CancelFeeQTY, 0) as total_cancel,/*CancelFeeQTY * CancelFeeQTY or 0 */
									Checked
									from customerregs1 as cr
									WHERE BookingDate between '$start' AND '$finish'
								        AND site_id = '" . $place['id'] . "'
										AND Agent = '{$_GET['agent']}'
										AND (
									(
									    /*Offsite is they owe us*/
										CollectPay = 'Offsite' 
										AND NoOfJump > 0
										AND ( Checked = 1 OR BookingDate >= '$today')
										AND DeleteStatus = 0
									) OR (
										/*Onsite we owe them*/
										DeleteStatus = 0
										AND NoOfJump > 0 /*Prevents group bookings from appearing multiple times*/
										AND ( Checked = 1 OR BookingDate >= '$today')
										AND RateToPay > 0
										AND RateToPayQTY > 0
										AND CollectPay = 'Onsite'
									) OR (
										/*Partial Cancellations by setting number of jumps to zero from Reception/dailyViewIE.php*/
										CancelFeeCollect = 'Offsite'
										AND NoOfJump = 0
									)
								)
									ORDER BY BookingDate ASC, RomajiName ASC;
								";

                            $res = mysql_query($sql) or die(mysql_error());
                            while ($row = mysql_fetch_assoc($res)) {
                                $key = $row['bookingdate'] . ' ' . $domain . ' ' . $row['bookingtime'];
                                if (!array_key_exists($key, $all_data)) {
                                    $all_data[$key] = array();
                                }
                                $all_data[$key][] = $row;
                            }

                            //select invoice amounts
                            $sql = "SELECT
									site_id,
									DATE_FORMAT(bookingdate, '%Y-%m') as invoice_date,
									SUM(IF(CollectPay = 'Offsite', 0, RateToPay * RateToPayQTY)) as 'TotalToPay',
									SUM(IF(CollectPay = 'Offsite', Rate * NoOfJump, 0)) as 'TotalToCollect',
									SUM(IF(CancelFeeCollect = 'Offsite', CancelFee * CancelFeeQTY, 0)) as total_cancel
									from customerregs1 as cr
									WHERE 
                                        BookingDate >= '" . $start_date . "'
                                        AND site_id = '" . $place['id'] . "'
                                            AND Agent = '{$_GET['agent']}'
                                            AND (
                                                (
                                                    /*Bookings made offsite that were completed via an agent*/
                                                    DeleteStatus = 0
                                                    AND Checked = 1
                                                    AND CollectPay = 'Offsite'
                                                    AND NoOfJump > 0
                                                ) OR (
                                                    /*Bookings made Onsite that were completed via an agent*/
                                                    DeleteStatus = 0
                                                    AND Checked = 1
                                                    AND NoOfJump > 0
                                                    AND RateToPay > 0
                                                    AND RateToPayQTY > 0
                                                    AND CollectPay = 'Onsite'
                                                ) OR (
                                                    /*partial cancellations through setting noOfJumps to zero in from dailyview checkin*/
                                                    CancelFeeCollect = 'Offsite'
                                                    AND NoOfJump = 0
                                                )
                                            )
                                    GROUP BY invoice_date
                                    ORDER BY invoice_date ASC; ";

                            //echo $sql."<br>";
                            $res = mysql_query($sql) or die(mysql_error());
                            while ($row = mysql_fetch_assoc($res)) {
                                list($y, $m) = explode('-', $row['invoice_date']);
                                $key = $row['invoice_date'] . '-' . date("t", mktime(0, 0, 0, $m, 15, $y));
                                if ($row['TotalToPay'] != 0 || $row['TotalToCollect'] != 0) {
                                    if (!array_key_exists($key, $invoices)) {
                                        $invoices[$key] = array(
                                            'amount' => 0,
                                        );
                                    }
                                    $invoices[$key]['amount'] += $row['TotalToCollect'] - $row['TotalToPay'];
                                    $invoices[$key]['amount'] += $row['total_cancel'];
                                }
                            }
                        } // all except main
                        //we use the description SALES DEGET FROM Rafting ... because we want to ignore other payments to the company as these are not part of the account
                        $sql = "SELECT * FROM banking WHERE company = '{$_GET['agent']}' AND description = 'SALES DEPOSIT FROM Rafting Company, Pension ETC For Bungy Sales' AND site_id = '" . $place['id'] . "' and `date` >= '$start_date' ORDER BY `date` ASC;";
                        //$sql = "SELECT sum(`in`-`out`) as total FROM banking WHERE company = '{$_GET['agent']}' and site_id in (" . implode(', ', $all_places) . ") and `date` >= '$start_date' /*and `date` < '$finish_date'*/ and description = 'SALES DEPOSIT FROM Rafting Company, Pension ETC For Bungy Sales';";

                        $res = mysql_query($sql) or die(mysql_error());
                        while ($row = mysql_fetch_assoc($res)) {
                            $key = $row['date'];
                            if ($row['in'] != 0 || $row['out'] != 0) {
                                if (!array_key_exists($key, $payments)) {
                                    $payments[$key] = array(
                                        'amount' => 0,
                                    );
                                }
                                $payments[$key]['amount'] += $row['in'] - $row['out'];
                            }
                        }
                    }
                    //TODO: Find out how outstanding $balance and Ramount are calculated
                    // calculate previous balance
                    $first_day = date("2014-01-01");
                    $balance = 0;
                    $transactions = array();//A list of all transactions in from invoices and payments

                    foreach ($invoices as $d => $i) {//sum up all of the invoices
                        if ($d < $first_day) {
                            $balance += $i['amount'];
                        } else {
                            $key = $d . '-i';//What?
                            if (!array_key_exists($key, $transactions)) {
                                $transactions[$key] = array();
                            }
                            $transactions[$key][] = $i['amount'];
                        }
                    }

                    foreach ($payments as $d => $p) {//subtract payments
                        if ($d < $first_day) {
                            $balance -= $p['amount'];
                        } else {
                            $key = $d . '-p';
                            if (!array_key_exists($key, $transactions)) {
                                $transactions[$key] = array();
                            }
                            $transactions[$key][] = $p['amount'];
                        }
                    }

                    ksort($transactions);
                    $fields = array(
                        'site'           => 'Site',
                        'bookingdate'    => 'Booking Date',
                        'bookingtime'    => 'Time',
                        'name'           => 'Customer Name',
                        'noofjump'       => 'Jump No',
                        'UnitCost'       => 'Rate',
                        'TotalToPay'     => 'Total To Pay',
                        'TotalToCollect' => 'Total To Collect'
                    );
                    echo "<tr>\n";
                    foreach ($fields as $name => $title) {
                        echo "\t<th class='th-$name'>$title</th>\n";
                    }
                    echo "</tr>\n";
                    $grand_total = 0;
                    $jump_total = 0;
                    $pay_total = 0;
                    $cgrand_total = 0;
                    $cjump_total = 0;
                    $cpay_total = 0;
                    foreach ($all_data as $key_array) {//This does not run at the for agent TOP so we can ignore this.
                        foreach ($key_array as $row) {
                            $tr_class = $row['Checked'] ? 'checked' : '';
                            // show only records with jumps and rates
                            if ($row['TotalToPay'] || $row['TotalToCollect']) {
                                echo "<tr class='$tr_class'>\n";
                                foreach ($fields as $fname => $ftitle) {
                                    $text = $row[$fname];
                                    echo "\t<td class='$fname'>$text</td>\n";
                                }
                                $grand_total += $row['TotalToCollect'];
                                $pay_total += $row['TotalToPay'];
                                $jump_total += $row['noofjump'];
                                echo "</tr>\n";
                                if ($row['Checked']) {
                                    $cgrand_total += $row['TotalToCollect'];
                                    $cpay_total += $row['TotalToPay'];
                                    $cjump_total += $row['noofjump'];
                                }
                            }
                            if ($row['CancelFee'] && $row['CancelFeeQTY'] && $row['CancelFeeCollect'] == 'Offsite') {
                                echo "<tr class='$tr_class'>\n";
                                $mapping = array(
                                    'noofjump' => 'CancelFeeQTY',
                                    'UnitCost' => 'CancelFee',
                                );
                                foreach ($fields as $fname => $ftitle) {
                                    $text = $row[$fname];
                                    if (array_key_exists($fname, $mapping)) {
                                        $text = $row[$mapping[$fname]];
                                    }
                                    if ($fname == 'TotalToCollect') {
                                        $text = $row['CancelFee'] * $row['CancelFeeQTY'];
                                    }
                                    if ($fname == 'name') $text .= ' (Cancel)';
                                    echo "\t<td class='$fname'>$text</td>\n";
                                }
                                $grand_total += $row['CancelFee'] * $row['CancelFeeQTY'];
                                $jump_total += $row['CancelFeeQTY'];
                                if ($row['Checked']) {
                                    $cgrand_total += $row['CancelFee'] * $row['CancelFeeQTY'];
                                    $cjump_total += $row['CancelFeeQTY'];
                                }
                                echo "</tr>\n";
                            }
                        }
                    }
                    echo "<tr class='confirmed-totals'>\n";
                    echo "\t<th class='th-jump-total' colspan='" . (count($fields) - 4) . "' style='text-align: right;'>Confirmed Totals: </th>\n";
                    echo "\t<th class='th-jump-total-value'> $cjump_total</th>\n";
                    echo "\t<th class='th-jump-total'>&nbsp;</th>\n";
                    echo "\t<th class='th-jump-total-value'> $cpay_total</th>\n";
                    echo "\t<th class='th-jump-total-value'> $cgrand_total</th>\n";
                    echo "</tr>\n";
                    echo "<tr>\n";
                    echo "\t<th class='th-grand-total' colspan='" . (count($fields) - 1) . "'>Grand Total</th>\n";
                    echo "\t<th class='th-grand-total-value'>&yen; " . ($grand_total - $pay_total) . "</th>\n";
                    echo "</tr>\n";
                    ?>
                </table>
            </td>
        </tr>
        <tr>
            <td colspan="7" align="right">
                <INPUT type="button" value="Back" name="back" id="back" onClick="processBack();">
            </td>
        </tr>
        <tr>
            <td colspan="7">
                <table id="transactions">
                    <tr>
                        <th>Date・日付</th>
                        <th>Item・事項</th>
                        <th>Amount・金額</th>
                        <th>Outstanding Balance<br/>
                            未払額
                        </th>
                    </tr>
                    <tr>
                        <td><?php echo date("Y-01-01"); ?></td>
                        <td>Previous Balance</td>
                        <td><?php echo $balance; ?></td>
                        <td><?php echo $balance; ?></td>
                    </tr>
                    <?php
                    foreach ($transactions as $d => $t) {
                        preg_match("/(.*?)-([ip])$/", $d, $m);
                        foreach ($t as $amount) {
                            $balance += (($m[2] == 'i') ? $amount : -$amount);
                            ?>
                            <tr>
                                <td><?php echo $m[1]; ?></td>
                                <td><?php echo ($m[2] == 'i') ? 'Invoice' : "Payment Rec'd"; ?></td>
                                <td><?php echo $amount; ?></td>
                                <td><?php echo $balance; ?></td>
                            </tr>
                        <?php
                        }
                    }
                    ?>
                </table>
            </td>
        </tr>
        </tbody>
    </table>
    <br>
</form>
</body>
</html>
<?
