<?php
	require_once("../includes/application_top.php");
	
	mysql_query("SET NAMES 'utf-8';");

	$current_id = '';
	$action_result = "";

	if (!empty($_POST)) {
		switch (TRUE) {
			case isset($_POST['update']): 
			case isset($_POST['addhelp']): 
				$type = 'insert';
				$parameters = '';
				if (!empty($_POST['id'])) {
					$type = 'update';
					$parameters = 'id=' . (int)$_POST['id'];
				};
				$action_result = $type;
				$data = array (
					'title'		=> $_POST['title'],
					'url'		=> $_POST['url'],
					'content'	=> base64_encode($_POST['content']),
					'active'	=> $_POST['active'],
				);
				db_perform('help', $data, $type, $parameters);
				break;
		};
	} else {
		switch (TRUE) {
			case array_key_exists('action', $_GET):
				if ($_GET['action'] == 'delete') {
					$current_id = (int)$_GET['id'];
					$sql = "DELETE FROM help WHERE id = '$current_id'";
					mysql_query($sql);
					$current_id = '';
					$action_result = 'delete';
				} elseif ($_GET['action'] == 'edit') {
					$current_id = (int)$_GET['id'];
				};
				break;
		};
	};
	if (!empty($action_result)) {
		$_SESSION['result_message'] = $action_result;
		Header("Location: manageHelpPages.php");
		die();
	};
	// empty user record
	$help_row = array(
		'id'		=> '',
		'title'		=> '',
		'url'		=> '',
		'content'	=> '',
		'active'	=> '0',
		'back_exists'=> '1',
	);
	if (!empty($current_id)) {
		$sql = "select * from help WHERE id = ". (int)$current_id;
		$res = mysql_query($sql);
		if ($res) {
			$help_row = mysql_fetch_assoc($res);
		};
	};


?>
<html>
<head>
<?php require_once('includes/header_tags.php'); ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">

<script src="js/functions.js" type="text/javascript"></script>

<?php require_once('../includes/head_scripts.php'); ?>
<style>
.help-table-row1 {
	background-color: #CCCCFF;
}
.help-table-row2 {
	background-color: #CCFFCC;
}
#system-message {
	background-color: #55FF55;
	text-align: center;
	line-height: 150%;
}
.user-access-type {
	background-color: #fdd;
	padding: 20px;
}
.user-access-type h2 {
	margin: 0px;
	padding:0px;
}
.help-table-header {
	background-color: #fdd;
}
#help-content {
	height: 300px;
	width: 100%;
}
</style>

<title>Bungy Japan :: Help Pages Management</title>

</head>
<script type="text/javascript">
function procAdd(){
	if(document.hForm.title.value == ''){
		alert("Title is required!!!");
		return false;
	}
	if(document.hForm.url.value == ''){
		alert("URL is required!!!");
		return false;
	}
	document.hForm.submit();
	return true;
}
function procBack(){
	if(document.getElementById('back') ){
		document.hForm.action = "index.php";
		document.hForm.submit();
	}
}
function deleteConfirm(id, title) {
	var answer = window.confirm('Are you sure to delete "'+title+'" help page?') 
	if (answer == true) {
		document.location = "manageHelpPages.php?action=delete&id=" + id;
	};
	return answer;
}
</script>
<body>
<?php require_once('../includes/main_menu.php'); ?>
<br />
<?php
 if (array_key_exists('result_message', $_SESSION) && !empty($_SESSION['result_message'])) {
	echo "<div id='system-message'>";
	switch ($_SESSION['result_message']) {
		case "insert":
			$message = "Help Page added";
			break;
		case "update":
			$message = "Help Page updated";
			break;
		case "delete":
			$message = "Help Page deleted";
			break;
	};
	echo $message;
	echo "</div>";
	unset($_SESSION['result_message']);
 };
?>
<form method="post" name="hForm">
<table align="center" width="800" bgcolor="#D3D3D3">
	<tbody>
		<tr>
			<td colspan="8" align="center" bgcolor="#FFA500"><b>Help Pages Management</b>
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500">ID:</td>
			<td align="left" bgcolor="#FFA500">
				<?php echo $help_row['id']; ?>
				<input type="hidden" name="id" value="<?php echo $help_row['id']; ?>">
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500">Page Title:</td>
			<td align="left" bgcolor="#FFA500">
				<input type="text" name="title" value="<?php echo $help_row['title']; ?>">
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500">Page URL:</td>
			<td align="left" bgcolor="#FFA500">
				<input type="text" name="url" value="<?php echo $help_row['url']; ?>"> (from server root, i.e. "/Bookkeeping/banking.php")
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500" >Page Status:</td>
			<td align="left" bgcolor="#FFA500">
				<select name="active">
					<option value="0"<?php echo ($help_row['active'])?'':' selected="selected"'; ?>>InActive</option>
					<option value="1"<?php echo ($help_row['active'])?' selected="selected"':''; ?>>Active</option>
				</select>
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500" valign="top">Page Content:</td>
			<td align="left" bgcolor="#FFA500">
				<textarea name="content" id="help-content"><?php echo base64_decode($help_row['content']); ?></textarea>
			</td>
		</tr>
	
		<tr>
			<td colspan="8" align="right" bgcolor="#FFA500">
<?php if ($help_row['id'] != '') { ?>
				<input type="submit" name="update" id="update-user" value="Update Page">
<?php } else { ?>
				<input type="submit" name="addhelp" id="add-user" value="Add Page" onclick="procAdd();">
<?php }; ?>
				<input type="button" name="back" id="back" value="Back" onclick="procBack();">
			</td>

			
		</tr>
	</tbody>
</table>

<table align="center" width="800" id="help-table">
<tr>
	<th class="user-access-type" colspan="7"><h2>Help Pages</h2></th>
</tr>
<tr class="help-table-header">
	<th class="help-table-id">ID</th>
	<th class="help-table-title">Page Title</th>
	<th class="help-table-url">Page URL</th>
	<th class="help-table-active">Active</th>
	<th class="help-table-active">Back detected</th>
	<th class="help-table-actions">Manage</th>
</tr>

<?php
	$sql = "select * from help order by URL ASC, title asc";
	$res = mysql_query($sql);
	$i = 1;
	$at = '';
	while ($row = mysql_fetch_assoc($res)) {
?>
	<tr class="help-table-row<?php echo ($i+1); ?>">
		<td class="help-table-id"><?php echo $row['id']; ?></td>
		<td class="help-table-title"><?php echo $row['title']; ?></td>
		<td class="help-table-url"><?php echo $row['url']; ?></td>
		<td class="help-agent-active"><?php echo $row['active'] ? 'Active' : 'InActive'; ?></td>
		<td class="help-table-back-detected"><?php echo $row['back_exists'] ? 'YES' : 'NO'; ?></td>
		<td class="help-table-actions">
			<a href="?action=edit&id=<?php echo $row['id']; ?>">Edit</a> | 
			<a class="help-delete" href="#" id="<?php echo $row['id']; ?>" title="<?php echo $row['title']; ?>">Delete</a>
		</td>
	</tr>
<?php
	};
?>
</table>
<script>
$(document).ready(function (){
	$('.help-delete').click(function (event){
		event.preventDefault();
		return deleteConfirm($(this).attr('id'), $(this).attr('title'));
	});
});
</script>

</form>
</body>
</html>
