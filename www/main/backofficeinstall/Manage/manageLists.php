<?php
	include ("../includes/application_top.php");
	$action = '';
	$record_type = 'lists';
	if (isset($_POST['action'])) {
		$action = $_POST['action'];
	};
	if (empty($action) && isset($_GET['action'])) {
		$action = $_GET['action'];
	};
	switch ($action) {
		case 'add':
		case 'Add List':
			$data = array(
				'group_id'	=> $_POST['group_id'],
				'name'		=> $_POST['name'],
				'alias'		=> $_POST['alias']
			);
			db_perform('lists', $data);
			Header('Location: manageLists.php?group_id=' . $_POST['group_id']);
			die();
			break;
		case 'delete':
			$id = (int)$_GET['id'];
			$sql = "DELETE FROM lists WHERE id = '$id'";
			$res = mysql_query($sql);
			Header('Location: manageLists.php');
			die();
			
	};
	
	$group_id = (int)$_GET['group_id'];
	// get group info
	$sql = "SELECT * FROM lists_groups order by `name` ASC;";
	$res = mysql_query($sql) or die(mysql_error());
	$groups = array();
	while ($row = mysql_fetch_assoc($res)) {
		if (!isset($first_group)) {
			$first_group = $row;
		};
		if ($group_id == $row['id']) {
			$current_group = $row;
		};
		$groups[] = array(
			'id'		=> $row['id'],
			'text'		=> $row['name'] . ' (' . $row['alias'] . ')'
		);
	};
	if (!isset($current_group)) {
		$current_group = $first_group;
	};
	$group_id = $current_group['id'];
	$group_name = $current_group['name'];
?>
<html>
<head>
<title>List Manager - <?php echo $list_name; ?> Lists</title>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">

<script src="js/functions.js" type="text/javascript"></script>

<?php include '../includes/head_scripts.php'; ?>
<style>
table textarea {
	width: 100%;
	height: 400px;
}
button {
	border: 1px solid silver;
	background-color: yellow;
	border-radius: 5px;
	padding: 5px;
	margin: 5px;
	font-size: 20px;
	font-family: Arial;
}
#back {
	margin-left: 0px !important;
	float: left;
}
#update {
	margin-right: 0px !important;
	float: right;
}
#list-groups {
	max-width: 800px;
	margin-left: auto;
	margin-right: auto;
	width: 600px;
	border-collapse: collapse;
}
#list-groups td, #list-groups th {
	border: 1px black solid;
	padding: 3px;
	font-family: Arial;
	font-size: 12px;
}
.add, .lists, .edit, .delete, .values, .update, .cancel {
	margin: 0px !important;
	padding: 2px !important;
	border-radius: 2px;
	font-size: 12px;
	margin-left: 2px !important;
	margin-right: 2px !important;
}
input {
	width: 98%;
}
.no-borders {
	border: none !important;
}
.header {
	background-color: #FFA500;
	border: 1px solid #FFA500 !important;
	border-bottom: 1px solid black !important;
	text-align: center;
	padding: 5px;
}
</style>

<title>Bungy Japan :: List Manager</title>

</head>
<?php include 'row_edit_functions.php'; ?>
<script>
    var fvalues = {'name':[], 'alias':[]};

	$(document).ready(function () {
		$('#back').on('click', function () {
			document.location = '/Manage/manageListGroups.php';
			return false;
		});
		$(".delete").click(row_delete);
		$(".values").click(function () {
			document.location = 'manageListValues.php?list_id=' + $(this).parents('tr').attr('custom-id');
		});
        $('.edit').unbind('click').click(row_change);
		$('.change-group').change(change_group);
	});
    function row_delete() {
        if (window.confirm("Are you sure to delete this group and all lists?")) {
            document.location = 'manageLists.php?action=delete&id=' + $(this).parents('tr').attr('custom-id');
        };
    }
	function change_group() {
		document.location = 'manageLists.php?group_id=' + $(this).val();
	}
</script>
<body>
<?php include '../includes/main_menu.php'; ?>
<br />
<?php
 if (array_key_exists('result_message', $_SESSION) && !empty($_SESSION['result_message'])) {
	echo "<div id='system-message'>";
	switch ($_SESSION['result_message']) {
		case "insert":
			$message = "Item added";
			break;
		case "update":
			$message = "Item updated";
			break;
		case "delete":
			$message = "Item deleted";
			break;
	};
	echo $message;
	echo "</div>";
	unset($_SESSION['result_message']);
 };
?>
<?php include "lists_navigation.php"; ?>
<table id="list-groups">
	<tr>
		<td colspan="4" class="header">
			<h1><?php echo SYSTEM_SUBDOMAIN; ?> - List Manager - Lists</h1>
			Group: <?php echo draw_pull_down_menu('group_id', $groups, $group_id, 'class="change-group"'); ?>
		</td>
	</tr>
	<tr>
		<th class="group-id">ID</th>
		<th class="group-name">Name</th>
		<th class="group-alias">Alias</th>
		<th class="group-actions">Actions</th>
	</tr>
<?php
	$sql = "SELECT * FROM lists WHERE group_id = '$group_id' ORDER by name ASC;";
	$res = mysql_query($sql) or die(mysql_error());
	while ($row = mysql_fetch_assoc($res)) {
		echo "\t<tr custom-id='{$row['id']}'>\n";
		echo "\t\t<td class='group-id'>{$row['id']}</td>\n";
		echo "\t\t<td class='group-name value' rel='name'>{$row['name']}</td>\n";
		echo "\t\t<td class='group-alias value' rel='alias'>{$row['alias']}</td>\n";
		echo "\t\t<td class='group-actions'><button class='values'>Values</button><button class='edit'>Edit</button><button class='delete'>Delete</button></td>\n";
		echo "\t</tr>\n";
	}
?>
	<tr>
		<th colspan="4">New list</th>
	</tr>
<form method="post">
	<tr>
		<td class="group-id">&nbsp;</td>
		<td class="group-name"><input type="text" name="name" value=""></td>
		<td class="group-alias"><input type="text" name="alias" value=""></td>
		<td class="group-actions"><input type="hidden" name="group_id" value="<?php echo $group_id; ?>"><button class="add" name="action" value="add" type="submit">Add List</button></td>
	</tr>
</form>
<tr>
<td colspan="4" class="no-borders">
<button id="back">Back</button>
</td>
</tr>
</table>
</body>
</html>
