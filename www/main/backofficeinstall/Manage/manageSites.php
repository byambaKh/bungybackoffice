<?php
	include ("../includes/application_top.php");

	$user = new BJUser();
	$current_id = '';
	$action_result = "";

	if (!empty($_POST)) {
		switch (TRUE) {
			case isset($_POST['update']): 
			case isset($_POST['addplace']): 
				$type = 'insert';
				$parameters = '';
				if (!empty($_POST['id'])) {
					$type = 'update';
					$parameters = 'id=' . (int)$_POST['id'];
				};
				$action_result = $type;
				$data = array (
					'name'			=> $_POST['name'],
					'name_jp'		=> mb_convert_encoding($_POST['name_jp'], 'SJIS', 'UTF-8'),
					'status'		=> $_POST['status'],
					'hidden'		=> $_POST['hidden'],
					'subdomain'		=> $_POST['subdomain'],
				);
				db_perform('sites', $data, $type, $parameters);
				break;
		};
	} else {
		switch (TRUE) {
			case array_key_exists('action', $_GET):
				if ($_GET['action'] == 'delete') {
					$current_id = (int)$_GET['id'];
					$sql = "DELETE FROM sites WHERE id = '$current_id'";
					mysql_query($sql);
					$current_id = '';
					$action_result = 'delete';
				} elseif ($_GET['action'] == 'edit') {
					$current_id = (int)$_GET['id'];
				};
				break;
		};
	};
	if (!empty($action_result)) {
		$_SESSION['result_message'] = $action_result;
		Header("Location: manageSites.php");
		die();
	};
	// empty place record
	$pinfo = array(
		'id'		=> '',
		'name'		=> '',
		'name_jp'	=> '',
		'status'	=> '0',
		'hidden'	=> '1',
		'subdomain'	=> '',
	);
	if (!empty($current_id)) {
		$sql = "select * FROM sites WHERE id = ". (int)$current_id;
		$res = mysql_query($sql);
		if ($res) {
			$row = mysql_fetch_assoc($res);
			$row['name_jp'] = mb_convert_encoding($row['name_jp'], 'UTF-8', 'SJIS');
			$pinfo = $row;
		};
	};
	$yes_no_options = array(
		array(
			'id'	=> '0',
			'text'	=> 'No'
		),
		array(
			'id'	=> '1',
			'text'	=> 'Yes'
		)
	);


?>
<!DOCTYPE html>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">

<script src="js/functions.js" type="text/javascript"></script>

<?php include '../includes/head_scripts.php'; ?>
<style>
.places-table-row1 {
	background-color: #CCCCFF;
}
.places-table-row2 {
	background-color: #CCFFCC;
}
#system-message {
	background-color: #55FF55;
	text-align: center;
	line-height: 150%;
}
.place-access-type {
	background-color: #fdd;
	padding: 20px;
}
.place-access-type h2 {
	margin: 0px;
	padding:0px;
}
.places-table-header {
	background-color: #fdd;
}
td {
	vertical-align: top;
}
td small {
	color: green;
}
</style>

<title>Bungy Japan :: User Management</title>

</head>
<script type="text/javascript">
function procAdd(){
	if(document.uForm.usrnm.value == ''){
		alert("User Name is required!!!");
		return false;
	}
	if(document.uForm.pwds.value == ''){
		alert("Password is required!!!");
		return false;
	}
	document.uForm.submit();
	return true;
}
function procBack(){
	if(document.getElementById('back') ){
		document.uForm.action = "index.php";
		document.uForm.submit();
	}
}
function deleteConfirm(id, placename) {
	var answer = window.confirm('Are you sure to delete "'+placename+'" site?') 
	if (answer == true) {
		document.location = "manageSites.php?action=delete&id=" + id;
	};
	return answer;
}
</script>
<body>
<?php include '../includes/main_menu.php'; ?>
<br />
<?php
 if (array_key_exists('result_message', $_SESSION) && !empty($_SESSION['result_message'])) {
	echo "<div id='system-message'>";
	switch ($_SESSION['result_message']) {
		case "insert":
			$message = "User added";
			break;
		case "update":
			$message = "User updated";
			break;
		case "delete":
			$message = "User deleted";
			break;
	};
	echo $message;
	echo "</div>";
	unset($_SESSION['result_message']);
 };
?>
<form method="post" name="uForm">
<?php include "user_management_menu.php"; ?>
<table align="center" width="480px" bgcolor="#D3D3D3">
	<tbody>
		<tr>
			<td colspan="8" align="center" bgcolor="#FFA500"><b>Site/Place Management</b>
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500">Place ID:</td>
			<td align="left" bgcolor="#FFA500">
				<?php echo $pinfo['id']; ?>
				<input type="hidden" name="id" value="<?php echo $pinfo['id']; ?>">
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500">Place Name:</td>
			<td align="left" bgcolor="#FFA500">
				<input type="text" name="name" value="<?php echo $pinfo['name']; ?>">
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500" >Place Name JP:</td>
			<td align="left" bgcolor="#FFA500">
				<input type="text" name="name_jp" value="<?php echo $pinfo['name_jp']; ?>">
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500" >Active:<br />
<small>Show or not in Online Booking</small></td>
			<td align="left" bgcolor="#FFA500"><?php echo draw_pull_down_menu('status', $yes_no_options, $pinfo['status']); ?></td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500" >Hidden:<br />
<small>Gray in Online Booking</small></td>
			<td align="left" bgcolor="#FFA500"><?php echo draw_pull_down_menu('hidden', $yes_no_options, $pinfo['hidden']); ?></td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500">Subdomain:</td>
			<td align="left" bgcolor="#FFA500">
				<input type="text" name="subdomain" value="<?php echo $pinfo['subdomain']; ?>">
			</td>
		</tr>
		<tr>
			<td colspan="8" align="right" bgcolor="#FFA500">
<?php if ($pinfo['id'] != '') { ?>
				<input type="submit" name="update" id="update-user" value="Update Place">
<?php } else { ?>
				<input type="submit" name="addplace" id="add-user" value="Add Place" onclick="procAdd();">
<?php }; ?>
				<input type="button" name="back" id="back" value="Back" onclick="procBack();">
			</td>

			
		</tr>
	</tbody>
</table>

<table align="center" width="600" id="places-table">
<tr>
	<th class="header" colspan="7"><h2>Places</h2></th>
</tr>
<tr class="places-table-header">
	<th class="places-table-placename">Place Name</th>
	<th class="places-table-placename-jp">Place Name JP</th>
	<th class="places-table-status">Active</th>
	<th class="places-table-hidden">Hidden</th>
	<th class="places-table-subdomain">Subdomain</th>
	<th class="places-table-actions">Manage</th>
</tr>

<?php
	$sql = "select * FROM sites order by name desc";
	$res = mysql_query($sql);
	$i = 1;
	$at = '';
	while($row = mysql_fetch_assoc($res)) {
		$i = !$i;
		$row['name_jp'] = mb_convert_encoding($row['name_jp'], 'UTF-8', 'SJIS');
?>
	<tr class="places-table-row<?php echo ($i+1); ?>">
		<td class="places-table-placename"><?php echo $row['name']; ?></td>
		<td class="places-table-placename-jp"><?php echo $row['name_jp']; ?></td>
		<td class="places-table-status"><?php echo $row['status'] ? 'Yes' : 'No'; ?></td>
		<td class="places-agent-hidden"><?php echo $row['hidden'] ? 'Yes' : 'No'; ?></td>
		<td class="places-agent-subdomain"><?php echo $row['subdomain']; ?></td>
		<td class="places-table-actions">
			<a href="?action=edit&id=<?php echo $row['id']; ?>">Edit</a> | 
			<a class="place-delete" href="#" pid="<?php echo $row['id']; ?>" placename="<?php echo $row['name']; ?>">Delete</a>
		</td>
	</tr>
<?php
	};
?>
</table>
<script>
$(document).ready(function (){
	$('.place-delete').click(function (event){
		event.preventDefault();
		return deleteConfirm($(this).attr('pid'), $(this).attr('placename'));
	});
});
</script>

</form>
</body>
</html>
