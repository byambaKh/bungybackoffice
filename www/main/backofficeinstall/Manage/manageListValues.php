<?php
	include ("../includes/application_top.php");
	$action = '';
	$record_type = 'lists_items';
	if (isset($_POST['action'])) {
		$action = $_POST['action'];
	};
	if (empty($action) && isset($_GET['action'])) {
		$action = $_GET['action'];
	};
	$list_id = (int)$_GET['list_id'];
	switch ($action) {
		case 'update':
		case 'Update':
			$values = $_POST['values'];
			if (is_array($values)) {
				foreach ($values as $id => $value) {
					$data = array(
						'list_id'	=> $list_id,
						'value'		=> $value
					);
					db_perform('lists_items', $data, 'update', 'id=' . $id);
				};
			};
			if (array_key_exists('add', $_POST)) {
				$adds = $_POST['add'];
				foreach ($adds as $column_id => $values) {
					foreach ($values as $id => $value) {
						$sql = "SELECT * FROM lists_add_items WHERE item_id = '$id' and column_id = '$column_id';";
						$res = mysql_query($sql) or die(mysql_error());
						$a = 'insert';
						$w = '';
						if ($row = mysql_fetch_assoc($res)) {
							$a = 'update';
							$w = 'id = ' . $row['id'];
						};
						$data = array(
							'item_id'	=> $id,
							'column_id'	=> $column_id,
							'value'		=> $value
						);
						db_perform('lists_add_items', $data, $a, $w);
					};
				};
			};
			// new value added
			if ($_POST['value_new'] !== '') {
				$data = array(
					'list_id'	=> $list_id,
					'value'		=> $_POST['value_new']
				);
				db_perform('lists_items', $data);
				$item_id = mysql_insert_id();
				if (array_key_exists('add_new', $_POST)) {
					foreach ($_POST['add_new'] as $column_id => $value) {
						$data = array(
							'item_id'	=> $item_id,
							'column_id'	=> $column_id,
							'value'		=> $value
						);
						db_perform('lists_add_items', $data);
					};
				};
			};
			Header('Location: manageListValues.php?list_id=' . $_POST['list_id']);
			die();
			break;
		case 'delete':
			$item_id = (int)$_GET['item_id'];
			$sql = "DELETE FROM lists_items WHERE id = $item_id;";
			mysql_query($sql);
			$sql = "DELETE FROM lists_add_items WHERE item_id = $item_id;";
			mysql_query($sql);
			Header('Location: manageListValues.php?list_id=' . $_GET['list_id']);
			die();
	};
	// select available lists
	$sql = "SELECT l.id, l.name, g.name as group_name FROM lists l LEFT JOIN lists_groups g on (l.group_id = g.id) order by l.name;";
	$res = mysql_query($sql) or die(mysql_error());
	$lists = array();
	$list_found = false;
	$first_list = null;
	while ($row = mysql_fetch_assoc($res)) {
		if (!array_key_exists($row['group_name'], $lists)) {
			$lists[$row['group_name']] = array();
		};
		if (is_null($first_list)) {
			$first_list = $row;
		};
		$lists[$row['group_name']][] = array(
			'id'	=> $row['id'],
			'text'	=> $row['name'],
		);
		if ($list_id == $row['id']) {
			$list_found = true;
			$list_name = $row['name'];
		};
	} 
	if (!$list_found) {
		$list_id = $first_list['id'];
		$list_name = $first_list['name'];
	};
	// get additional fields configured
	$sql = "SELECT * FROM lists_add_columns WHERE list_id = '$list_id';";
	$res = mysql_query($sql) or die(mysql_error());
	$add_columns = array();
	while ($row = mysql_fetch_assoc($res)) {
		$add_columns[] = $row;
	};
	mysql_free_result($res);
	// eof additional columns
	$table_columns = sizeof($add_columns) + 3;
?>
<html>
<head>
<title>List Manager - <?php echo $list_name; ?> Values</title>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">

<script src="js/functions.js" type="text/javascript"></script>

<?php include '../includes/head_scripts.php'; ?>
<style>
h2 {
	width: 100%;
	text-align: center;
}
table textarea {
	width: 100%;
	height: 400px;
}
button {
	border: 1px solid silver;
	background-color: yellow;
	border-radius: 5px;
	padding: 5px;
	margin: 5px;
	font-size: 20px;
	font-family: Arial;
}
#back {
	margin-left: 0px !important;
	float: left;
}
#update {
	margin-right: 0px !important;
	float: right;
}
#list-items {
	max-width: 800px;
	margin-left: auto;
	margin-right: auto;
	width: 600px;
	border-collapse: collapse;
}
#list-items td, #list-items th {
	padding: 3px;
	font-family: Arial;
	font-size: 12px;
}
.item-name {
	min-width: 300px !important;
}
.add, .lists, .edit, .delete, .values, .update, .cancel {
	margin: 0px !important;
	padding: 2px !important;
	border-radius: 2px;
	font-size: 12px;
	margin-left: 2px !important;
	margin-right: 2px !important;
}
input {
	width: 98%;
}
.no-borders {
	border: none !important;
}
.header {
	background-color: #FFA500;
	border: 1px solid #FFA500 !important;
	text-align: center;
	padding: 5px;
}
.item-add-column {
	max-width: 100px;
	min-width: 70px;
}
</style>

</head>
<script>

	$(document).ready(function () {
		$('#back').on('click', function () {
			document.location = '/Manage/manageLists.php';
			return false;
		});
		$(".delete").click(function () {
			document.location = 'manageListValues.php?action=delete&list_id=<?php echo $list_id; ?>&item_id=' + $(this).attr('custom_id');
			return false;
		});
		$("#list-id").change(change_list);
	});
	function change_list() {
		document.location = 'manageListValues.php?list_id=' + $(this).val();
	}
</script>
<body>
<?php include '../includes/main_menu.php'; ?>
<br />
<?php
 if (array_key_exists('result_message', $_SESSION) && !empty($_SESSION['result_message'])) {
	echo "<div id='system-message'>";
	switch ($_SESSION['result_message']) {
		case "insert":
			$message = "Item added";
			break;
		case "update":
			$message = "Item updated";
			break;
		case "delete":
			$message = "Item deleted";
			break;
	};
	echo $message;
	echo "</div>";
	unset($_SESSION['result_message']);
 };
?>
<?php include "lists_navigation.php"; ?>
<form method="post" action="manageListValues.php?list_id=<?php echo $list_id; ?>">
<table id="list-items">
	<tr>
		<td colspan="<?php echo $table_columns; ?>" class="header">
			<h1><?php echo SYSTEM_SUBDOMAIN; ?> - List Manager - Values</h1>
List: 
<?php
	echo "<select name='list_id' id='list-id'>\n";
	foreach ($lists as $group => $values) {
		echo "\t<optgroup label='$group'>\n";
		$options_exists = false;
		foreach ($values as $list) {
			$options_exists = true;
			$selected = ($list['id'] == $list_id) ? ' selected' : '';
			echo "\t\t<option value='{$list['id']}'$selected>{$list['text']}</option>\n";
		};
		if (!$options_exists) {
			echo "\t\t<option disabled>No lists</option>\n";
		};
		echo "\t</optgroup>\n";
	};
	echo "</select>"
?>
		</td>
	</tr>
	<tr>
		<td colspan="<?php echo $table_columns; ?>"><h2><?php echo $list_name; ?> Values</h2></td>
	</tr>
	<tr>
		<th class="item-id">ID</th>
		<th class="item-name">Value</th>
<?php
		foreach ($add_columns as $column) {
			echo "\t\t<th class='item-add-column'>{$column['name']}</th>\n";
		};
?>
	</tr>
<?php
	$sql = "SELECT li.*";
	$from = '';
	foreach ($add_columns as $column) {
		$sql .= ", lai{$column['id']}.value as 'column_{$column['id']}'";
		$from .= " LEFT JOIN lists_add_items lai{$column['id']} on (li.id = lai{$column['id']}.item_id and lai{$column['id']}.column_id = {$column['id']}) "; 
	};
	$sql .= " FROM lists_items li $from WHERE list_id = '$list_id'";
	if ($list_name == 'Company') {
		$sql .= " ORDER BY li.value;";
	};

	//if there are no more elements mysql_fetch_assoc returns FALSE 
	//array_pop will return this value upon popping an element
	//since this is an OR only the first part is executed if it returns true

	$res = mysql_query($sql) or die(mysql_error());

	while(($resultsArray[] = mysql_fetch_assoc($res)) || array_pop($resultsArray));

	//sort the array in to a 'natural order' based on value
	usort($resultsArray, function($a, $b){
		return strnatcasecmp($a['value'], $b['value']);
	});

	$res = mysql_query($sql) or die(mysql_error());
	foreach($resultsArray as $key => $result) {
		echo "\t<tr custom-id='{$result['id']}'>\n";
		echo "\t\t<td class='item-id'>{$result['id']}</td>\n";
		echo "\t\t<td class='item-name' rel='name'><input name='values[{$result['id']}]' value='{$result['value']}'></td>\n";
		foreach ($add_columns as $column) {
			echo "\t\t<td class='item-add-column'><input name='add[{$column['id']}][{$result['id']}]' value='" . $result['column_' . $column['id']] . "'></td>\n";
		};
		echo "\t\t<td class='item-actions'><button type='button' class='delete' custom_id='{$result['id']}'>Delete</button></td>";
		echo "\t</tr>\n";
	}
?>
	<tr>
		<td class="item-id">&nbsp;</td>
		<td class="item-name"><input type="text" name="value_new" value=""></td>
<?php
		foreach ($add_columns as $column) {
			echo "\t\t<td class='item-add-column' rel='name'><input name='add_new[{$column['id']}]' value=''></td>\n";
		};
?>
		<td class="item-actions">&nbsp;</td>
	</tr>
<tr>
<td colspan="<?php echo $table_columns; ?>" class="no-borders">
<button id="back">Back</button>
<button id="update" name="action" value="update">Update</button>
</td>
</tr>
</table>
</form>
</body>
</html>
