<?php
include("../includes/application_top.php");

/**
 * @param $current_role_id
 * @param $role_row
 *
 * @return array
 */
function selectCurrentRole($current_role_id, $role_row)
{
    if (!empty($current_role_id)) {
        $sql = "select * from user_roles WHERE id = " . (int)$current_role_id;
        $res = mysql_query($sql);
        if ($res) {
            $role_row = mysql_fetch_assoc($res);

            return array($role_row);
        }

        return array($role_row);
    }

    return array($role_row);
}

/**
 * @return array
 */
function selectAllRows()
{
// select all roles
    $atypes = array();
    $atypes_drop = array();
    $sql = "SELECT * FROM access_types";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_array($res)) {
        $atypes[$row['id']] = $row;
        $atypes_drop[] = array(
            'id'   => $row['id'],
            'text' => $row['type_name'],
        );
    }
    mysql_free_result($res);

    return array($atypes, $atypes_drop);
}

/**
 * @param $action_result
 * @param $current_role_id
 *
 * @return array
 */
function handlePost($action_result, $current_role_id)
{
    if (!empty($_POST)) {
        switch (TRUE) {
            case isset($_POST['update']):
            case isset($_POST['addrole']):
                $type = 'insert';
                $parameters = '';
                if (!empty($_POST['role_id'])) {
                    $type = 'update';
                    $parameters = 'id=' . (int)$_POST['role_id'];
                }
                $action_result = $type;
                $data = array(
                    'user_id'        => $_POST['user_id'],
                    'site_id'        => $_POST['site_id'],
                    'price'          => $_POST['price'],
                    'access_type_id' => $_POST['access_type_id'],
                );
                db_perform('user_roles', $data, $type, $parameters);

                break;
        }

        return array($action_result, $current_role_id);
    } else {
        switch (TRUE) {
            case array_key_exists('action', $_GET):
                if ($_GET['action'] == 'delete') {
                    $current_role_id = (int)$_GET['role_id'];
                    $sql = "DELETE FROM user_roles WHERE id = '$current_role_id'";
                    mysql_query($sql);
                    $current_role_id = '';
                    $action_result = 'delete';
                } elseif ($_GET['action'] == 'edit') {
                    $current_role_id = (int)$_GET['role_id'];
                }
                break;
        }

        return array($action_result, $current_role_id);
    }
}

/**
 * @return array
 */
function selectAllSites()
{
    $sites = array();
    $sites_drop = array();
    $sites_drop[] = array(
        'id'   => '0',
        'text' => 'All Sites',
    );
    $sql = "SELECT * FROM sites";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_array($res)) {
        $sites[$row['id']] = $row;
        $sites_drop[] = array(
            'id'   => $row['id'],
            'text' => $row['name'],
        );
    }
    mysql_free_result($res);

    return array($sites, $sites_drop);
}

/**
 * @param $current_user_id
 *
 * @return array
 */
function selectAllUsers($current_user_id)
{
// select all users
    $users = array();
    $users_drop = array();
    $sql = "SELECT * FROM users ORDER BY UserName ASC;";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        $users[$row['UserID']] = $row;
        $users_drop[] = array(
            'id'   => $row['UserID'],
            'text' => $row['UserName'],
        );
        if (is_null($current_user_id)) {
            $current_user_id = $row['UserID'];
        }

    }

    mysql_free_result($res);

    return array($users, $users_drop, $current_user_id);
}

/**
 * @param $current_user_id
 * @param $users
 *
 * @return array
 */
function selectUserRows($current_user_id, $users)
{
    $sql = "select * from user_roles where user_id = '$current_user_id' order by user_id asc";

    $res = mysql_query($sql);
    $i = 1;
    $at = '';

    $user_roles = array();

    while ($row = mysql_fetch_assoc($res)) {
        if (!array_key_exists($row['user_id'], $user_roles)) {
            $user_roles[$row['user_id']] = array();
        }
        $user_roles[$row['user_id']][] = $row;
    }

    $user = $users[$current_user_id];

    return array($user_roles, $user);
}

$current_role_id = '';
$action_result = "";
$current_user_id = null;

if (isset($_GET['user_id'])) {
    $current_user_id = (int)$_GET['user_id'];
}

if (isset($_POST['user_id'])) {
    $current_user_id = (int)$_POST['user_id'];
}

list($action_result, $current_role_id, $sql) = handlePost($action_result, $current_role_id);

if (!empty($action_result)) {
    $_SESSION['result_message'] = $action_result;
    header("Location: manageRoles.php" . (!is_null($current_user_id) ? '?user_id=' . $current_user_id : ''));
    die();
}
// empty user record
$role_row = array(
    'id'             => '',
    'user_id'        => '',
    'site_id'        => '',
    'price'          => '',
    'access_type_id' => '',
);

list($users, $users_drop, $current_user_id) = selectAllUsers($current_user_id);

list($sites, $sites_drop) = selectAllSites();

list($atypes, $atypes_drop) = selectAllRows();

list($role_row) = selectCurrentRole($current_role_id, $role_row);

?>
<!DOCTYPE html>
<html>
<head>
    <?php include 'includes/header_tags.php'; ?>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=8">

    <script src="js/functions.js" type="text/javascript"></script>

    <?php include '../includes/head_scripts.php'; ?>
    <style>
        .users-table-row1 {
            background-color: #CCCCFF;
        }

        .users-table-row2 {
            background-color: #CCFFCC;
        }

        #system-message {
            background-color: #55FF55;
            text-align: center;
            line-height: 150%;
        }

        .user-access-type {
            background-color: #fdd;
            padding: 20px;
        }

        .user-access-type h2 {
            margin: 0px;
            padding: 0px;
        }

        .users-table-header {
            background-color: #fdd;
        }
    </style>

    <title>Bungy Japan :: Roles Management</title>

    <script type="text/javascript">

        function procAdd() {
            document.uForm.submit();
            return true;
        }

        function procBack() {
            if (document.getElementById('back')) {
                document.uForm.action = "index.php";
                document.uForm.submit();
            }
        }

        function deleteConfirm(role_id, rolename, sitename) {
            var answer = window.confirm('Are you sure to delete "' + rolename + '" role for "' + sitename + '" site?')
            if (answer == true) {
                document.location = "manageRoles.php?action=delete&user_id=<?= $current_user_id; ?>&role_id=" + role_id;
            }
            return answer;
        }

        $(document).ready(function () {
            $("#user-id-field").change(function () {
                document.location = "manageRoles.php?user_id=" + $(this).val();
            });
            $("#access-type-id").change(function () {
                if ($(this).children("option:selected").text() == 'Agent') {
                    $("#agent-price").slideDown();
                } else {
                    $("#agent-price").slideUp();
                }
            });
            $("#access-type-id").trigger('change');
        });

    </script>
</head>
<body>
<?php include '../includes/main_menu.php'; ?>
<br/>
<?php
if (array_key_exists('result_message', $_SESSION) && !empty($_SESSION['result_message'])) {
    echo "<div id='system-message'>";
    switch ($_SESSION['result_message']) {
        case "insert":
            $message = "Role added";
            break;
        case "update":
            $message = "Role updated";
            break;
        case "delete":
            $message = "Role deleted";
            break;
    }
    echo $message;
    echo "</div>";
    unset($_SESSION['result_message']);
}
?>
<?php include 'user_management_menu.php'; ?>
<form method="post" name="uForm">
    <table align="center" width="480px" bgcolor="#d3d3d3" style="border: 1px silver solid;">
        <tbody>
        <tr>
            <td colspan="6" align="right" bgcolor="#FFA500" width="150">Select User:</td>
            <td align="left" bgcolor="#FFA500">
                <?= draw_pull_down_menu('user_id', $users_drop, $current_user_id, 'id="user-id-field"'); ?>
            </td>
        </tr>
        <tr>
            <td colspan="8" align="center" bgcolor="#FFA500"><b>Roles Management</b>
            </td>
        </tr>
        <tr>
            <td colspan="6" align="right" bgcolor="#FFA500">Role ID:</td>
            <td align="left" bgcolor="#FFA500">
                <?= $role_row['id']; ?>
                <input type="hidden" name="role_id" value="<?= $role_row['id']; ?>">
            </td>
        </tr>
        <tr>
            <td colspan="6" align="right" bgcolor="#FFA500">Site :</td>
            <td align="left" bgcolor="#FFA500">
                <?= draw_pull_down_menu('site_id', $sites_drop, $role_row['site_id']); ?>
            </td>
        </tr>
        <tr>
            <td colspan="6" align="right" bgcolor="#FFA500">Access Type :</td>
            <td align="left" bgcolor="#FFA500">
                <?= draw_pull_down_menu('access_type_id', $atypes_drop, $role_row['access_type_id'], "id='access-type-id'"); ?>
            </td>
        </tr>
        <tr id="agent-price">
            <td colspan="6" align="right" bgcolor="#FFA500">Agent Price Exception:</td>
            <td align="left" bgcolor="#FFA500">
                <input type="text" name="price" value="<?= $role_row['price']; ?>">
            </td>
        </tr>
        <tr>
            <td colspan="8" align="right" bgcolor="#FFA500">
                <?php if ($role_row['id'] != '') { ?>
                    <input type="hidden" name="update" value="Update Role">
                    <input type="submit" name="updatebutton" id="update-role" value="Update Role">
                <?php } else { ?>
                    <input type="hidden" name="addrole" value="Add Role">
                    <input type="submit" name="addrolebutton" value="Add Role" id="add-role" onclick="procAdd();">
                <?php } ?>
                <input type="button" name="back" id="back" value="Back" onclick="procBack();">
            </td>
        </tr>
        </tbody>
    </table>

    <table align="center" width="600" id="users-table">

        <?php

        list($user_roles, $user) = selectUserRows($current_user_id, $users);
        ?>
        <tr>
            <th class="user-access-type" colspan="7"><h2><?= $user['UserName']; ?></h2></th>
        </tr>
        <tr class="users-table-header">
            <th class="users-table-username">Site</th>
            <th class="users-table-username">Role</th>
            <th class="users-table-username">Agent Price</th>
            <th class="users-table-actions">Manage</th>
        </tr>
        <?php
        $i = 0;
        if (array_key_exists($current_user_id, $user_roles) && !empty($user_roles[$current_user_id])) {
            $i = !$i;
            foreach ($user_roles[$current_user_id] as $row) {
                ?>
                <tr class="users-table-row<?= ($i + 1); ?>">
                    <td class="users-table-site"><?= ($row['site_id']) ? $sites[$row['site_id']]['name'] : 'All'; ?></td>
                    <td class="users-table-role"><?= $atypes[$row['access_type_id']]['type_name']; ?></td>
                    <td class="users-table-price"><?= $row['price']; ?></td>
                    <td class="users-table-actions">
                        <a href="?action=edit&user_id=<?= $current_user_id; ?>&role_id=<?= $row['id']; ?>">Edit</a> |
                        <a class="user-delete" href="#" role_id="<?= $row['id']; ?>" rolename="<?= $atypes[$row['access_type_id']]['type_name']; ?>" sitename="<?= $sites[$row['site_id']]['name']; ?>">Delete</a>
                    </td>
                </tr>
                <?php
            }
        } else {
            ?>
            <tr>
                <td colspan="3">No user roles defined</td>
            </tr>
            <?php
        }
        ?>
    </table>
    <script>
        $(document).ready(function () {
            $('.user-delete').click(function (event) {
                event.preventDefault();
                return deleteConfirm($(this).attr('role_id'), $(this).attr('rolename'), $(this).attr('sitename'));
            });
        });
    </script>

</form>
</body>
</html>
