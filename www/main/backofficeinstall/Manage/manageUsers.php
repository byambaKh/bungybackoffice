<?php
	include ("../includes/application_top.php");

	$current_uid = '';
	$action_result = "";

	if (!empty($_POST)) {
		switch (TRUE) {
			case isset($_POST['update']): 
			case isset($_POST['adduser']): 
				$type = 'insert';
				$parameters = '';
				if (!empty($_POST['userid'])) {
					$type = 'update';
					$parameters = 'UserID=' . (int)$_POST['userid'];
				};
				$action_result = $type;
				$data = array (
					'UserName'		=> $_POST['username'],
					'StaffListName'	=> $_POST['stafflistname'],
					'UserPwd'		=> $_POST['password'],
					'TypeRole'		=> ($_POST['access_type'] == 'Agent')?'900':'100',
					'access_type'	=> $_POST['access_type'],
					//'price'			=> $_POST['price'],
					'sort_order'	=> $_POST['sort_order'],
					'emailAddress1'  => $_POST['emailAddress1'],
					'emailAddress2'  => $_POST['emailAddress2'],
					'emailAddress3'  => $_POST['emailAddress3']
				);
				db_perform('users', $data, $type, $parameters);
				break;
		};
	} else {
		switch (TRUE) {
			case array_key_exists('action', $_GET):
				if ($_GET['action'] == 'delete') {
					$current_uid = (int)$_GET['uid'];
					$sql = "DELETE FROM users WHERE UserID = '$current_uid'";
					mysql_query($sql);
					$current_uid = '';
					$action_result = 'delete';
				} elseif ($_GET['action'] == 'edit') {
					$current_uid = (int)$_GET['uid'];
				};
				break;
		};
	};
	if (!empty($action_result)) {
		$_SESSION['result_message'] = $action_result;
		Header("Location: /Manage/manageUsers.php");
		die();
	};
	// empty user record
	$uinfo = array(
		'UserID'	=> '',
		'UserName'	=> '',
		'StaffListName'	=> '',
		'UserPwd'	=> '',
		'TypeRole'	=> '100',
		'access_type'	=> 'No Access',
		//'price'		=> '0',
		'sort_order'=> '0',
	);
	if (!empty($current_uid)) {
		$sql = "select * from users WHERE UserID = ". (int)$current_uid;
		$res = mysql_query($sql);
		if ($res) {
			$user_row = mysql_fetch_assoc($res);
			$uinfo = $user_row;
			if (empty($uinfo['access_type']) && $uinfo['TypeRole'] == '900') {
				$uinfo['access_type'] = 'Agent';
			};
		};
	};


?>
<html>
<head>
<?php include 'includes/header_tags.php'; ?>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=8">

<script src="js/functions.js" type="text/javascript"></script>

<?php include '../includes/head_scripts.php'; ?>
<style>
.users-table-row1 {
	background-color: #CCCCFF;
}
.users-table-row2 {
	background-color: #CCFFCC;
}
#system-message {
	background-color: #55FF55;
	text-align: center;
	line-height: 150%;
}
.user-access-type {
	background-color: #fdd;
	padding: 20px;
}
.user-access-type h2 {
	margin: 0px;
	padding:0px;
}
.users-table-header {
	background-color: #fdd;
}
</style>

<title>Bungy Japan :: User Management</title>

</head>
<script type="text/javascript">
function procAdd(){
	if(document.uForm.usrnm.value == ''){
		alert("User Name is required!!!");
		return false;
	}
	if(document.uForm.pwds.value == ''){
		alert("Password is required!!!");
		return false;
	}
	document.uForm.submit();
	return true;
}
function procBack(){
	if(document.getElementById('back') ){
		document.uForm.action = "index.php";
		document.uForm.submit();
	}
}
function deleteConfirm(uid, username) {
	var answer = window.confirm('Are you sure to delete "'+username+'" user?') 
	if (answer == true) {
		document.location = "/Manage/manageUserAccount.php?action=delete&uid=" + uid;
	};
	return answer;
}
</script>
<body>
<?php include '../includes/main_menu.php'; ?>
<br />
<?php
 if (array_key_exists('result_message', $_SESSION) && !empty($_SESSION['result_message'])) {
	echo "<div id='system-message'>";
	switch ($_SESSION['result_message']) {
		case "insert":
			$message = "User added";
			break;
		case "update":
			$message = "User updated";
			break;
		case "delete":
			$message = "User deleted";
			break;
	};
	echo $message;
	echo "</div>";
	unset($_SESSION['result_message']);
 };
?>
<form method="post" name="uForm">
<?php include 'user_management_menu.php'; ?>
<table align="center" width="480px" bgcolor="#D3D3D3">
	<tbody>
		<tr>
			<td colspan="8" align="center" bgcolor="#FFA500"><b>User Management</b>
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500">User ID:</td>
			<td align="left" bgcolor="#FFA500">
				<?php echo $uinfo['UserID']; ?>
				<input type="hidden" name="userid" value="<?php echo $uinfo['UserID']; ?>">
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500">User Name:</td>
			<td align="left" bgcolor="#FFA500">
				<input type="text" name="username" value="<?php echo $uinfo['UserName']; ?>">
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500" >User Password :</td>
			<td align="left" bgcolor="#FFA500">
				<input type="text" name="password" value="<?php echo $uinfo['UserPwd']; ?>">
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500">Staff List Name:</td>
			<td align="left" bgcolor="#FFA500">
				<input type="text" name="stafflistname" value="<?php echo $uinfo['StaffListName']; ?>">
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500">Email Address 1:</td>
			<td align="left" bgcolor="#FFA500">
				<input type="text" name="emailAddress1" value="<?php echo $uinfo['emailAddress1']; ?>">
			</td>
		</tr>
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500">Email Address 2:</td>
			<td align="left" bgcolor="#FFA500">
				<input type="text" name="emailAddress2" value="<?php echo $uinfo['emailAddress2']; ?>">
			</td>
		</tr>

		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500">Email Address 3:</td>
			<td align="left" bgcolor="#FFA500">
				<input type="text" name="emailAddress3" value="<?php echo $uinfo['emailAddress3']; ?>">
			</td>
		</tr>
		<!--tr>
			<td colspan="6" align="right" bgcolor="#FFA500" >Agent Price Exception:</td>
			<td align="left" bgcolor="#FFA500">
				<input type="text" name="price" value="<?php echo $uinfo['price']; ?>">
			</td>
		</tr-->
		<tr>
			<td colspan="6" align="right" bgcolor="#FFA500" >Agent Sort Order:</td>
			<td align="left" bgcolor="#FFA500">
				<input type="text" name="sort_order" value="<?php echo $uinfo['sort_order']; ?>">
			</td>
		</tr>
	
		<tr>
			<td colspan="8" align="right" bgcolor="#FFA500">
<?php if ($uinfo['UserID'] != '') { ?>
				<input type="submit" name="update" id="update-user" value="Update User">
<?php } else { ?>
				<input type="submit" name="adduser" id="add-user" value="Add User" onclick="procAdd();">
<?php }; ?>
				<input type="button" name="back" id="back" value="Back" onclick="procBack();">
			</td>

			
		</tr>
	</tbody>
</table>

<table align="center" width="600" id="users-table">
<tr>
	<th class="user-access-type" colspan="7"><h2>Users</h2></th>
</tr>
<tr class="users-table-header">
	<th class="users-table-delete">X</th>
	<th class="users-table-username">User Name</th>
	<th class="users-table-username">Staff Name</th>
	<!--th class="users-table-username">Agent Price Exception</th-->
	<th class="users-table-username">Sort</th>
	<th class="users-table-actions">Manage</th>
</tr>

<?php
	$sql = "select * from users order by access_type desc, username asc";
	$res = mysql_query($sql);
	$i = 1;
	$at = '';
	while($row = mysql_fetch_assoc($res)) {
		$i = !$i;
?>
	<tr class="users-table-row<?php echo ($i+1); ?>">
		<td class="users-table-delete"><input name="uid[]" value="<?php echo $row['UserID']; ?>" type="checkbox"></td>
		<td class="users-table-username"><?php echo $row['UserName']; ?></td>
		<td class="users-table-username"><?php echo $row['StaffListName']; ?></td>
		<!--td class="users-agent-price"><?php echo $row['price']; ?></td-->
		<td class="users-agent-sort"><?php echo $row['sort_order']; ?></td>
		<td class="users-table-actions">
			<a href="?action=edit&uid=<?php echo $row['UserID']; ?>">Edit</a> | 
			<a class="user-delete" href="manageUsers.php?action=delete&uid=<?php echo $row['UserID']; ?>" uid="<?php echo $row['UserID']; ?>" username="<?php echo $row['UserName']; ?>">Delete</a>
		</td>
	</tr>
<?php
	};
?>
</table>
<script>
$(document).ready(function (){
	$('.user-delete').click(function (event){
		event.preventDefault();
		if (deleteConfirm($(this).attr('uid'), $(this).attr('username'))) {
			document.location = 'manageUsers.php?action=delete&uid=' + $(this).attr('uid');
		};
		return false;
	});
});
</script>

</form>
</body>
</html>
