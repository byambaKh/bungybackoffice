$(document).ready(function () {
    $(".banking, .income, .expense, .payroll").on('click', function () {
        var dtype = $(this).prop('class');
        $('#download-type').val(dtype);
        $('#download').dialog({
            buttons: [
                {
                    text: "Close",
                    click: function () {
                        $(this).dialog("close");
                    },
                    style: 'float: left;'
                },
                {
                    text: "Download",
                    style: 'float: right; margin-right: 0px;',
                    click: function () {
                        $('#year-month-selection').prop('action', 'index.php');
                        if ($('#download-type').val() == 'income') {
                            $('#year-month-selection').prop('action', '/Bookkeeping/income.php');
                        }
                        ;
                        $('#year-month-selection').submit();
                    }
                }
            ],
            modal: true,
            title: "Please select month for " + $(this).find('div').html()
        });
    });
});