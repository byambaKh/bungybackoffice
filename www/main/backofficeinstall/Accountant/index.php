<?php
include "../includes/application_top.php";

/*
    Description "
        A file for Accountants to view and download bungy financial data in CSV or Yayoi format
        The Yayoi format appears to not be implemented yet.

    Functionality & Tests Performed:
         get income, expenses, banking and payroll data as CSV file
         Show a popup to interactively choose the month to download
         Link to other parts of the financial system. (Agent)

    Fixed issues :
        - PHPStorm Auto Reformat
        - extract css to File in /css/ current directory
        - extract js to file in /js/ current directory
        - File Encoding to UTF8 (Already)
        - Delete useless looking commented out code
        - Group code into functions
        - Removed the hardcoded country name from the file

    Problems :
        - Converts other encodings to or from UTF8
        - Contains mysql() functions
        - Some mixed PHP and HTML but not too bad

    Notes:
        This seems to have some Japanese text in it but it is not worth translating as the accountant situation
        will likely be different in China.

*/

/**
 * @param $site_id
 * @param $subdomain
 */
function createIncomeCSV($site_id, $subdomain, $delimiter)
{
    $all_data = array();
    $d = sprintf("%4d-%02d", $_POST['year'], $_POST['month']);
    // Bookings records
    $sql = "SELECT
						cr.BookingDate, 
						SUM(
							IF(cr.CollectPay = 'Onsite', cr.Rate * cr.NoOfJump + cr.2ndj
								+ cr.CancelFee * cr.CancelFeeQTY
							, 0)
							+ cr.tshirt 
							+ cr.other
						) as daily_total
					FROM customerregs1 cr WHERE cr.site_id = '$site_id' AND cr.BookingDate like '$d-%' and cr.Checked = 1 and cr.DeleteStatus = 0
					GROUP BY BookingDate
					ORDER BY BookingDate ASC";
    //$res = mysql_query($sql) or die(mysql_error());
    if ($res = mysql_query($sql)) {
        while ($row = mysql_fetch_assoc($res)) {
            $all_data[$row['BookingDate']][$subdomain] = $row['daily_total'];
        };
        mysql_free_result($res);
    };
    // Daily Tourism board
    $sql = "SELECT `date` as tdate, tboard_day_total as total FROM income WHERE site_id = '$site_id' AND `date` like '$d-%' ORDER BY tdate ASC";
    //$res = mysql_query($sql) or die(mysql_error());
    if ($res = mysql_query($sql)) {
        while ($row = mysql_fetch_assoc($res)) {
            if (!array_key_exists($row['tdate'], $all_data) || !array_key_exists($subdomain, $all_data[$row['tdate']])) {
                $all_data[$row['tdate']][$subdomain] = 0;
            };
            $all_data[$row['tdate']][$subdomain] -= $row['total'];
        };
        mysql_free_result($res);
    };
    // Merchandise sales
    $sql = "SELECT
						DATE(sale_time) as sale_date,
						SUM(sale_total) as sale_total
					FROM merchandise_sales
					WHERE site_id = '$site_id' AND sale_time like '$d-%'
					GROUP BY sale_date
					ORDER BY sale_date ASC;";
    //$res = mysql_query($sql) or die(mysql_error());
    if ($res = mysql_query($sql)) {
        while ($row = mysql_fetch_assoc($res)) {
            if (!array_key_exists($row['sale_date'], $all_data) || !array_key_exists($subdomain, $all_data[$row['sale_date']])) {
                $all_data[$row['sale_date']][$subdomain] = 0;
            };
            $all_data[$row['sale_date']][$subdomain] += $row['sale_total'];
        };
        mysql_free_result($res);
    };
    ksort($all_data);
    // Header info
    Header('Content-Type: application/csv; name="income' . $d . '.csv"');
    Header('Content-Disposition: attachment; filename="income' . $d . '.csv"');
    // building CSV
    foreach ($all_data as $sdate => $domain) {
        foreach ($domain as $subdomain => $total) {
            if ($total == 0) continue;
            list($y, $m, $d) = explode('-', $sdate);
            $row = array(
                2000,
                '',
                '',
                'H.' . ($y - 1989) . '/' . $m . '/' . $d,
                mb_convert_encoding("現金", "SJIS", 'UTF-8'),
                '',
                '',
                mb_convert_encoding("対象外", "SJIS", 'UTF-8'),
                $total,
                '',
                mb_convert_encoding("売上", "SJIS", 'UTF-8'),
                '',
                ucfirst($subdomain),
                mb_convert_encoding("対象外", "SJIS", 'UTF-8'),
                $total,
                '',
                'sales of the day',
                '',
                '',
                0,
                '',
                'Sales Data Entered By: Ami',
                '',
                '',
                'NO'
            );
            echo implode(',', $row) . "\r\n";
        };
    };
}

/**
 * @param $site_id
 * @param $subdomain
 * @param $fcontent
 * @param $headers_sent
 * @param $delimiter
 */
function createExpensesCSV($site_id, $subdomain, $delimiter)
{
    $headers_sent = false;
    $all_data = array();
    $d = sprintf("%4d-%02d", $_POST['year'], $_POST['month']);
    // load data
    $sql = "SELECT * FROM expenses WHERE site_id = '$site_id' AND `date` like '$d-%' ORDER BY `date` ASC;";
    if ($res = mysql_query($sql)) {
        while ($row = mysql_fetch_assoc($res)) {
            if (!array_key_exists($row['date'], $all_data) || !array_key_exists($subdomain, $all_data[$row['date']])) {
                $all_data[$row['date']][$subdomain] = array();
            };
            $all_data[$row['date']][$subdomain][] = $row;
        };
        mysql_free_result($res);
    };

    ksort($all_data);
    $list = BJHelper::getListWithAdditionalFields('expenses', 'description');
    foreach ($list as $v) {
        $fcontent[] = $v['value'];
    };
    // Header info
    Header('Content-Type: application/csv; name="expense-' . $subdomain . $d . '.csv"');
    Header('Content-Disposition: attachment; filename="expense-' . $subdomain . $d . '.csv"');
    // building CSV
    $fp = fopen("php://output", "w");
    $total_cost = 0;
    foreach ($all_data as $sdate => $domain) {
        foreach ($domain as $subdomain => $rows) {
            foreach ($rows as $row) {
                unset($row['id']);
                unset($row['site_id']);
                // simple file output
                if (!$headers_sent) {
                    fputcsv($fp, array_map('ucfirst', array_keys($row)), $delimiter);
                    $headers_sent = true;
                };
                $total_cost += $row['cost'];
                fputcsv($fp, $row, $delimiter);
                continue;
            };
        };
    };
    $totals_row = array('', '', 'Total Cost:', $total_cost);
    fputcsv($fp, $totals_row, $delimiter);
    fclose($fp);
}

/**
 * @param $site_id
 * @param $balance
 * @param $subdomain
 * @param $fcontent
 * @param $delimiter
 */
function createBankingCSV($site_id, $subdomain, $delimiter)
{
    $balance = 0;
    $all_data = array();
    $d = sprintf("%4d-%02d", $_POST['year'], $_POST['month']);
    // load data
    $sql = "SELECT * FROM banking WHERE site_id = '$site_id' AND `date` like '$d-%' ORDER BY `date` ASC;";
    if ($res = mysql_query($sql)) {
        while ($row = mysql_fetch_assoc($res)) {
            $balance = $row['balance'] = $balance + $row['in'] - $row['out'];
            if ($row['cf']) {
                $balance = $row['in'];
            };
            if (!array_key_exists($row['date'], $all_data) || !array_key_exists($subdomain, $all_data[$row['date']])) {
                $all_data[$row['date']][$subdomain] = array();
            };
            $all_data[$row['date']][$subdomain][] = $row;
        };
        mysql_free_result($res);
    };

    ksort($all_data);
    $list = BJHelper::getListWithAdditionalFields('banking', 'description');
    foreach ($list as $v) {
        $fcontent[] = $v['value'];
    };
    // Header info
    Header('Content-Type: application/csv; name="banking-' . $subdomain . '-' . $d . '.csv"');
    Header('Content-Disposition: attachment; filename="banking-' . $subdomain . '-' . $d . '.csv"');
    // building CSV
    $headers_sent = false;
    $fp = fopen("php://output", "w");
    foreach ($all_data as $sdate => $domain) {
        foreach ($domain as $subdomain => $rows) {
            foreach ($rows as $row) {
                if ($row['cf']) {
                    $csv_row = array_map(function ($item, $name) {
                        if (!in_array($name, array('out', 'in', 'balance'))) {
                            $item = 'C/F';
                        };
                        return $item;
                    }, $row, array_keys($row));
                    $row = array_combine(array_keys($row), $csv_row);
                };
                unset($row['id']);
                unset($row['site_id']);
                unset($row['cf']);
                // simple file output
                if (!$headers_sent) {
                    fputcsv($fp, array_map('ucfirst', array_keys($row)), $delimiter);
                    $headers_sent = true;
                };
                fputcsv($fp, $row, $delimiter);
                continue;
            };
        };
    };
    fclose($fp);
}

/**
 * @param $site_id
 * @param $subdomain
 * @param $headers_sent
 * @param $delimiter
 */
function createPayrollCSV($site_id, $subdomain, $delimiter)
{
    $headers_sent = false;
    $d = sprintf("%4d-%02d", $_POST['year'], $_POST['month']);
    $sql = "select * from payroll WHERE site_id = '$site_id' AND `date` like '$d%' order by `date` ASC;";
    Header('Content-Type: text/csv; name="payroll-' . $subdomain . '-' . $d . '.csv"');
    Header('Content-Disposition: attachment; filename="payroll-' . $subdomain . '-' . $d . '.csv"');
    $res = mysql_query($sql);
    $fp = fopen('php://output', "w");
    while ($row = mysql_fetch_assoc($res)) {
        $row['total_work'] = getTotalWork($row['start'], $row['finish']);
        //$row['day'] = preg_replace("/([\d]{4}-[\d]{2}-[0]?)/", "", $row['date']);
        unset($row['id']);
        unset($row['site_id']);
        if (!$headers_sent) {
            fputcsv($fp, array_map('ucfirst', array_keys($row)), $delimiter);
            $headers_sent = true;
        };
        fputcsv($fp, $row, $delimiter);
    };
    fclose($fp);
}

function getTotalWork($in, $out)
{
    list($in_h, $in_m) = explode(':', $in);
    list($out_h, $out_m) = explode(':', $out);
    $diff = $out_h - $in_h + ($out_m - $in_m) / 60;
    if ($diff >= 9) {
        $diff -= 0.25;
    };
    $diff -= 0.75;
    if ($in == $out) $diff = 0;
    return number_format($diff, 2);
}

if (!empty($_POST)) {
    $site_id = $_POST['site_id'];
    $places = BJHelper::getPlaces();
    $subdomain = 'unknown';
    foreach ($places as $place) {
        if ($place['id'] == $site_id) {
            $subdomain = $place['subdomain'];
        };
    };
    $delimiter = ',';
    switch ($_POST['type']) {
        case 'income':
            createIncomeCSV($site_id, $subdomain, $delimiter);
            die();
            break;

        case 'expense':
            createExpensesCSV($site_id, $subdomain, $delimiter);
            die();
            break;

        case 'banking':
            createBankingCSV($site_id, $subdomain,  $delimiter);
            die();
            break;

        case 'payroll':
            createPayrollCSV($site_id, $subdomain,  $delimiter);
            die();
            break;
    };
};


?>
<!DOCTYPE html>
<html>
<head>
    <?php include 'includes/header_tags.php'; ?>
    <title>Bungy <?= ucwords(COUNTRY) ?> Accountants Download Page</title>
    <?php include 'includes/head_scripts.php'; ?>
    <link rel="stylesheet" href="css/accountant_php.css">
    <script src="js/accountant_php.js"></script>
</head>
<body bgcolor=black>
<div>
    <img src="/img/index.jpg">

    <h1>Bungy <?= ucwords(COUNTRY) ?> Accountants Download Page</h1>
    <?php
    $buttons = array(
        'Banking' => 'banking.php',
        'Income' => 'income.php',
        'Agent' => '/Bookkeeping/agent.php',
        'Expense' => 'expense.php',
        'Payroll' => 'payroll.php',
        'Log Out' => '?logout=true',
        'Change Password' => '/User/change_password.php'
    );
    $i = 0;
    while (list($text, $url) = each($buttons)) {
        if (empty($text)) continue;
        $i++;
        ?>
        <br>
        <button <?php if ($text == 'Change Password' || $text == 'Log Out' || $text == 'Agent') {
            ?> onClick="document.location = '<?php echo $url; ?>';"<?php echo (empty($url)) ? ' disabled' : ''; ?><?php
        } else {
            ?> class="<?php echo strtolower($text); ?>"<?php
        }; ?>>
            <div><?php echo $text; ?></div>
        </button>
        <?php
    };
    ?>
    <br><br>
</div>
<div id="download" title="Please select month">
    <form id="year-month-selection" method="post">
        <label for="site-id">Site: </label>
        <select name="site_id" id="site-id">
            <?php
            $places = BJHelper::getPlaces();
            foreach ($places as $place) {
                if ($place['hidden'] == 1 && $place['subdomain'] != 'main') continue;
                echo "<option value='{$place['id']}'>{$place['name']}</option>";
            };
            ?>
        </select>
        <label for="file-type">File Type: </label>
        <select name="file_type" id="file-type">
            <option>CSV</option>
            <option>Yayoi</option> <!-- Not Implemented -->
        </select>
        <br/>
        <br/>
        <label for="year">Year: </label>
        <select name="year" id="year">
            <?php
            $current_year = date("Y");
            for ($i = 2011; $i <= $current_year; $i++) {
                $selected = ($i == $current_year) ? ' selected="selected"' : '';
                echo "<option valur='$i'$selected>$i</option>";
            };
            ?>
        </select>
        <label for="month">Month: </label>
        <select name="month" id="month">
            <?php
            $current_month = date("Y");
            for ($i = 1; $i <= 12; $i++) {
                $tmonth = date("F", mktime(0, 0, 0, $i, 1, date('Y')));
                $selected = ($i == date('n')) ? ' selected="selected"' : '';
                echo "<option value='$i'$selected>$tmonth</option>";
            };
            ?>
        </select>
        <input type="hidden" name="type" value="" id="download-type">
    </form>
</div>
</body>
</html>
