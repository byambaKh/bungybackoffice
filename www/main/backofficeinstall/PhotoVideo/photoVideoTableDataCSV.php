<?php
require_once('classes/photoVideoTableData.php');
require_once('classes/photoVideoData.php');
error_reporting(E_ALL);
//construct $dateTime and $dateTimeBefore

//new getDate() that will do all of this
if(isset($_GET['dateStart']) && isset($_GET['dateStart'])) {
    $dateStartString = $_GET['dateStart'];
    $dateEndString   = $_GET['dateEnd'];

    if (preg_match('/^[1-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]$/', $dateStartString)) { //date of format 2014-12-01
        $dateStart = DateTime::createFromFormat("Y-m-d", $dateStartString);

    } else if (preg_match('/^[1-2][0-9][0-9][0-9]-[0-1][0-9]$/', $dateStartString)) {//date of format 2014-12
        $dateStart = DateTime::createFromFormat("Y-m", $dateStartString);
        $dateStart->setDate($dateStart->format('Y'), $dateStart->format('m'), 1);

    }

    if (preg_match('/^[1-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]$/', $dateEndString)) { //date of format 2014-12-01
        $dateEnd   = DateTime::createFromFormat("Y-m-d", $dateEndString);

    } else if (preg_match('/^[1-2][0-9][0-9][0-9]-[0-1][0-9]$/', $dateEndString)) {//date of format 2014-12
        $dateEnd   = DateTime::createFromFormat("Y-m", $dateEndString);
        $dateEnd->setDate($dateEnd->format('Y'), $dateEnd->format('m'), 1);

    }
} else {
    die("Starting and Ending Date required");
}

$photoVideoData      = new PhotoVideoData(clone $dateStart, clone $dateEnd);
$photoVideoTableData = new PhotoVideoTableData($photoVideoData);

if(isset($row)) unset($row);
//$filename = "income-$subdomain-$d.csv";
$filename = "income.csv";
Header('Content-Type: text/csv; name="' . $filename . '"');
Header('Content-Disposition: attachment; filename="' . $filename . '"');
$fp = fopen('php://output', "w");

$delimiter = ',';
//
fputcsv($fp, $photoVideoTableData->columnNameToIndexes[2], $delimiter);//output the headers

foreach($photoVideoTableData->data['rows'] as $row){
    foreach($row['data'] as $cellIndex => $cell){
        if(is_array($cell))
            fputs($fp, "{$cell['value']}");
        else
            fputs($fp, "$cell");

        if($cellIndex == (count($row['data'])-1)){
            fputs($fp, "\n");
        } else {
            fputs($fp, ",");
        }
    }
}

//TODO: should we fclose php://output
die();

?>
