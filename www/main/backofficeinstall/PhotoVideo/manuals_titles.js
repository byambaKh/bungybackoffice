    function publishToggle(){
        var post_data = {"id":$(this).prop('value')};
        $.ajax({
            url: 'ajax_handler.php?action=publishToggle&type=training_manuals',
            method: 'POST',
            data: post_data,
            context: $(this)
        });
    }

	function row_update() {
		var id = $(this).parent().parent().attr('rel');
		var post_data = {};
		post_data.id = id;
		$(this).parent().parent().children("td.value").each(function () {
			var field = $(this).attr('rel');
			post_data[field] = $(this).children('[name='+field+']').prop('value');
			if (field == 'shop_name' && $(this).children('[name='+field+'_new]').prop('value') != '') {
				post_data[field+'_new'] = $(this).children('[name='+field+'_new]').prop('value');
			};
			if (field == 'company' && $(this).children('[name='+field+'_new]').prop('value') != '') {
				post_data[field+'_new'] = $(this).children('[name='+field+'_new]').prop('value');
			};
		});
		$.ajax({
			url: 'ajax_handler.php?action=update_row&type=training_manuals',
			method: 'POST', 
			data: post_data,
			context: $(this)
		}).done(function () {
			$(this).parent().parent().children("td.value").each(function () {
				var field = $(this).attr('rel');
				if ((field == 'shop_name' || field == 'company') && $(this).children('[name='+field+'_new]').prop('value') != '') {
					$(this).html($(this).children('[name='+field+'_new]').prop('value'));
					fvalues[field].push({
						'id': $(this).html(), 
						'text' :$(this).html()
					});
					fvalues[field] = fvalues[field].sort(function (item1, item2) {
						return (item1.text > item2.text);
					});
				} else {
					$(this).html($('<a>').attr('href', 'manual.php?id=' + $(this).parent().attr('rel')).text($(this).children('[name='+field+']').prop('value')));
				};
			});
			$(this).prop('value', 'Change').removeClass('update').addClass('change').unbind('click').click(row_change);
			$(this).parent().children(".cancel").prop('value', 'Delete').removeClass('cancel').addClass('delete').unbind('click').click(row_delete);
		});
	}
	function row_delete() {
		var id = $(this).parent().parent().attr('rel');
		if (window.confirm("Are you sure to delete this Manual?")) {
			//document.location = "?action=Delete&id=" + id;
			var post_data = {"id":id};
			$.ajax({
				url: 'ajax_handler.php?action=delete_row&type=training_manuals',
				method: 'POST', 
				data: post_data,
				context: $(this)
			}).done(function () {
				$(this).parent().parent().remove();
			}).fail(function () {
				alert('Record delete failed.');
			});
		};
	}
	function row_cancel() {
		var id = $(this).parent().parent().attr('rel');
		$(this).parent().parent().children("td.value").each(function () {
			var field = $(this).attr('rel');
			$(this).html($('<a>').attr('href', 'manual.php?id=' + $(this).parent().attr('rel')).text($(this).children('input[type=hidden]').prop('value')));
		});
		$(this).parent().children(".update").prop('value', 'Change').removeClass('update').addClass('change').unbind('click').click(row_change);
		$(this).prop('value', 'Delete').removeClass('camcel').addClass('delete').unbind('click').click(row_delete);
	}
	function row_change() {
		var id = $(this).parent().parent().attr('rel');
		$(this).parent().parent().children("td.value").each(function () {
			var input_field = '';
			var field = $(this).attr('rel');
			var value = $(this).html();
			if ($(this).children('a').length > 0) {
				value = $(this).children('a').text();
			};
			if (typeof field != 'undefined' && fvalues.hasOwnProperty(field) && fvalues[field].length > 0) {
				input_field = $('<select>').prop('name', field);
				for (fv in fvalues[field]) {
					if (fvalues[field].hasOwnProperty(fv)) {
						input_field.append(
							$('<option>').prop('value', fvalues[field][fv].id).text(fvalues[field][fv].text)
						);
					};	
				};
				input_field.prop('value', $(this).html());
			} else {
				input_field = $('<input>').prop('name', field).prop('value', value);
			};
			$(this).html(input_field);
			$(this).append(
				$('<input>').prop('type', 'hidden').prop('name', field + '_old').prop('value', value)
			);
			if (field == 'shop_name' || field == 'company') {
				$(this).append($('<br />')).append(
					$('<input>').prop('name', field + '_new')
				);
			};
		});
		$(this).prop('value', 'Update').removeClass('change').addClass('update').unbind('click').click(row_update);
		$(this).parent().children(".delete").prop('value', 'Cancel').removeClass('delete').addClass('cancel').unbind('click').click(row_cancel);
	};
	var fvalues = {};
