<?php
require_once('../includes/application_top.php');
echo '';
error_reporting(E_ALL);
require_once('classes/photoVideoTableData.php');


$isMonthView = false;
$isPVAdmin = false;

$user = new BJUser();
if($user->hasRole(array('SysAdmin', 'P&V Admin'))){//if daily is specified
    $isMonthView = true;
    if(isset($_GET['daily'])) $isMonthView = false;
    $isPVAdmin = true;

}

$dateTime = null;

//TODO: This code can be reused in a class on several pages
//new getDate() that will do all of this
if(isset($_GET['date'])) {
    $dateString = $_GET['date'];

    if (preg_match('/^[1-2][0-9][0-9][0-9]-[0-1][0-9]$/', $dateString)) { //date of format 2014-12
        //$dateTime = DateTime::createFromFormat("Y-m", $dateString);//For the date 2015-09 dateTime returns the October 2015-10-01
        //it looks like a bug in PHP
        $dateTime = DateTime::createFromFormat("Y-m-d", $dateString."-01");

    } else if (preg_match('/^[1-2][0-9][0-9][0-9]-[0-1][0-9]-[0-3][0-9]$/', $dateString)) {//date of format 2014-12-01
        $dateTime = DateTime::createFromFormat("Y-m-d", $dateString);

    }
}

//if time is empty or not a valid date time string assume it is now
//or if the user is not an admin only show today
if($dateTime == null || !$isPVAdmin) $dateTime = new DateTime('now');

//Calculate the dates for the next and previous pages
//Most of this logic can be wrapped up in to a class/set of functions and be reused for other pages
if($isMonthView){
    $dateTime->setDate($dateTime->format('Y'), $dateTime->format('m'), 1);
    //--------Create Strings for the URL next and previous--------------------//
    $dateTimeBefore = clone $dateTime;
    $dateTimeAfter  = clone $dateTime;

    $dateTimeBefore ->modify("-1 month");
    $dateTimeAfter  ->modify("+1 month");

    $dateAfterUrlString = $dateTimeAfter->format('Y-m');//the date string for the link to the preceding page
    $dateBeforeUrlString = $dateTimeBefore->format('Y-m');//the date string for the link to the next page

    $datePageTitleString = 'F Y';//Date string format for the page title


} else {//day view
    $dateTimeBefore = clone $dateTime;
    $dateTimeAfter  = clone $dateTime;

    $dateTimeBefore ->modify("-1 day");
    $dateTimeAfter  ->modify("+1 day");

    $dateAfterUrlString = $dateTimeAfter->format('Y-m-d');//the date string for the link to the preceding page
    $dateBeforeUrlString = $dateTimeBefore->format('Y-m-d');//the date string for the link to the next page

    $datePageTitleString = 'F jS Y';//Date string format for the page title

}

//This is the string that appears at the top of the page
$currentDateString = $dateTime->format($datePageTitleString);//if we are in admin mode this should show the current Month
//if we are in basic mode this should show the current day and month

/*************************Finish Generating Dates For The Page************************************************/
//TODO how about instead of sending in a date time, we send in a start date and end date
// and generate day entries based on that.
//that way we can calculate a blank template for the period (1 day, month etc) and allow
//the code to generate a blank month to fill in with the retrieved values.

//we pass clones as these are modified when passed by reference
$photoVideoData      = new PhotoVideoData(clone $dateTime, clone $dateTimeAfter);
$photoVideoTableData = new PhotoVideoTableData($photoVideoData);

//set up some styling for the table
$parameters = array('id' => 'photoVideoTable', 'class' => 'table table-border table-condensed' );
$photoVideoTableView = new TableView($photoVideoTableData, $parameters);

if($isMonthView) $tableTitle = "Month View";
else $tableTitle = "Day View";

?>
<!DOCTYPE html>
<html>
	<head>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" type="text/css" media="all" />

        <?php
        echo Html::css("bootstrap/3.3.2/css/bootstrap.min.css");
        echo Html::css("photoVideo.css");
        ?>
        <script>
        $(document).ready(function(){
            //click to open the Notes Editing View
            $('td').change(function(){
                $(this).css('background-color', '#5cb85c');
            })

            //click to open the Notes Editing View
            $('.note').click(function(){
                var rowDate = $(this).attr('rel');
                window.open("photoVideoNote.php?date="+rowDate, "_blank", "height=700, width=400, menubar=no, titlebar=yes");
            });

            //click to open the Daily Report Editing View
            $('.dailyReports').click(function(){
                var rowDate = $(this).attr('rel');
                window.open("photoVideoDailyReport.php?date="+rowDate, "_blank", "height=700, width=400, menubar=no, titlebar=yes");
            });

            //click to open the Daily Report Editing View
            $('.adminNotes').click(function(){
                var rowDate = $(this).attr('rel');
                window.open("photoVideoAdminNote.php?date="+rowDate, "_blank", "height=700, width=400, menubar=no, titlebar=yes");
            });
        });

    </script>
        <title><?php echo "Bungy Japan Photo & Video - ".ucwords($subdomain)?></title>
	</head>
	<body>
        <div id="pageContainer">

            <h1 class="tableTitle"><?php echo ucwords($subdomain)." P&amp;V - $tableTitle"?></h1>
            <form action="controller.php" method="POST">
            <?php
                if($isPVAdmin)
                    echo "<h2 class='tableTitle'><a href='photoVideoTableView.php?date=$dateBeforeUrlString'>&lt;&lt;&lt;</a> $currentDateString <a href='photoVideoTableView.php?date=$dateAfterUrlString'>&gt;&gt;&gt;</a></h2>";
                else
                    echo "<h2 class='tableTitle'>$currentDateString</h2>";

                echo $photoVideoTableView->drawTable();
                echo "<input type='submit' class='btn btn-success' value='Update'>";

            ?>

            </form>
            <?php
            if($isPVAdmin) {
                $dateString = $dateTime->format('Y-m');
                $dateTimeAfter->format('Y-m');
                echo "<a href='photoVideoTableDataCSV.php?dateStart=$dateString&dateEnd=$dateAfterUrlString'>Download CSV</a>";
                echo "<br><a href='photoVideoSettingsView.php'>Edit Photo and Video Settings</a>";
            }
            echo "<br><a href='{$_SERVER['SCRIPT_NAME']}?logout=true'>Logout</a>";
            ?>
        </div>
    </body>
</html>


