<?php
require_once(dirname(__FILE__).'/../../includes/application_top.php');
require_once(dirname(__FILE__).'/photoVideoData.php');
//get the data
//do all of the calculations
//then build the table for presentation
class PhotoVideoTableData extends TableData{
    public $mainPrice;
    public $data;
    public $columnNameToIndex;
    public $columnCount;
    public $photoVideoData;
    public $prices;
    public $datesAndPrices;
    public $commission;

    public function __construct($photoVideoData){
        //setUp the
        $this->photoVideoData       = $photoVideoData;
        $this->data['header']       = $this->calculateHeader();
        $this->columnNameToIndexes  = $this->parseHeaderToColumnIndex($this->data['header']);
        $this->columnNameToIndex    = $this->columnNameToIndexes[0];
        $this->prices               = $photoVideoData->prices;
        $this->datesAndPrices       = $photoVideoData->datesAndPrices;
        $this->lateAfterSalesPrices = $photoVideoData->datesAndPrices;
        $this->commission           = 500;
        $this->mainPrice            = $this->photoVideoData->priceSettings->mainPrice;

        $header             = $this->calculateHeader();
        $this->data['rows'] = $this->calculateRows();
        $this->data['rows'] = $this->calculateTotals();
        $footer             = $this->calculateFooter();

        $this->data = array(
            'header' => $header,
            'rows'   => $this->data['rows'],
            'footer' => $footer
        );

        echo '';
    }

    function calculateFooter(){
        $rows   = $this->data['rows'];
        $prices = $this->prices;

        $ci = $this->columnNameToIndex;//Shorthand columnNameToIndex
        $footer = array(//calculate an empty template for the footer
            'id'        => '',
            'classes'   => 'footerRow',
            'data'      =>  array_fill(0, count($this->columnNameToIndex), 0)
        );

        $footer['data'][0] = 'Total';
        $lastItem = count($footer['data']) - 1;
        $footer['data'][$lastItem] = '-';

        //Calculate the A Column Totals
        foreach($rows as $row){
            foreach($prices as $price){
                $columnIndex = $ci["a$price"];
                $footer['data'][$columnIndex] += $row['data'][$columnIndex]['value'];
            }
        }

        //Calculate the B Column Totals
        foreach($rows as $row){
            foreach($prices as $price){
                $columnIndex = $ci["b$price"];
                $footer['data'][$columnIndex] += $row['data'][$columnIndex]['value'];
            }
        }

        //Calculate the lateAfterSales Totals
        foreach($rows as $row){
            foreach($prices as $price){
                if(array_key_exists("b{$price}late", $ci)) {//Check if the name has come up 1
                    $columnIndex = $ci["b{$price}late"];
                    $footer['data'][$columnIndex] += $row['data'][$columnIndex]['value'];
                }
            }
        }

        foreach($rows as $row){
            $colAStandardXTotal1        = $ci['aStandardXTotal1'];
            $colAStandardXTotal2        = $ci['aStandardXTotal2'];
            $colACommissionsXTotal      = $ci['aCommissionsXTotal'];
            $colBAfterSalesTotal        = $ci['bAfterSalesTotal'];
            $colABFullPriceTotal        = $ci['aB_ATotal'];
            $colABLateAfterSalesTotal   = $ci['aB_BTotal'];
            $colABFullDiscountTotal     = $ci['aB_APlusBTotal'];
            $colABDailyCashTotal        = $ci['aBDailyCashTotal'];
            $colABNoOfJumpers           = $ci['yenJumpers'];

            // yenJumpers
            $footer['data'][$colAStandardXTotal1]       += $row['data'][$colAStandardXTotal1]['value'];
            $footer['data'][$colACommissionsXTotal]     += $row['data'][$colACommissionsXTotal]['value'];
            $footer['data'][$colAStandardXTotal2]       += $row['data'][$colAStandardXTotal2]['value'];
            $footer['data'][$colBAfterSalesTotal]       += $row['data'][$colBAfterSalesTotal]['value'];
            $footer['data'][$colABLateAfterSalesTotal]  += $row['data'][$colABLateAfterSalesTotal]['value'];
            $footer['data'][$colABNoOfJumpers]          += $row['data'][$colABNoOfJumpers]['value'];
            $footer['data'][$colABFullPriceTotal]       += $row['data'][$colABFullPriceTotal]['value'];
            $footer['data'][$colABFullDiscountTotal]    += $row['data'][$colABFullDiscountTotal]['value'];
            $footer['data'][$colABDailyCashTotal]       += $row['data'][$colABDailyCashTotal]['value'];
        }

        return $footer;
        //calculate Horizontal Totals
    }

    function calculateTotals(){
        $rows       = $this->data['rows'];
        $prices     = $this->prices;
        $commission = $this->commission;


        $ci = $this->columnNameToIndex;//Shorthand columnNameToIndex
        foreach($rows as &$row){
            foreach($prices as $price){
                $aPriceColumn = $ci["a$price"];
                $bPriceColumn = $ci["b$price"];
                //Column for the late price may not exist as not every price is/was a main price
                //Late prices are the payments for the old photos from previous jumps
                $bLatePriceColumn = isset($ci["b{$price}late"]) ? $ci["b{$price}late"] : null;

                //A Section
                //StandardTotal (quantity*price) for each price
                $row['data'][$ci['aStandardXTotal1']]['value'] += $price*$row['data'][$aPriceColumn]['value'];
                //Commissions Totals ($commission * quantity) for each row
                $row['data'][$ci['aCommissionsXTotal']]['value'] += ($row['data'][$aPriceColumn]['value'] * $commission);
                //Standard Total - Commission (Standard Total - Commission Total)
                $row['data'][$ci['aStandardXTotal2']]['value'] += ($price - $commission)*$row['data'][$aPriceColumn]['value'];

                //add the total money from a and b minus the commission

                //B Section
                //AfterSalesTotal (Sum quantity *price for each row)
                $row['data'][$ci['aBDailyCashTotal']]['value'] += $row['data'][$bPriceColumn]['value'] * $price;
                $row['data'][$ci['bAfterSalesTotal']]['value'] += $row['data'][$bPriceColumn]['value'] * $price;

                //A+B Section
                if($bLatePriceColumn) {
                    $row['data'][$ci['aBDailyCashTotal']]['value'] += $row['data'][$bLatePriceColumn]['value'] * $price;
                    $row['data'][$ci['bAfterSalesTotal']]['value'] += $row['data'][$bLatePriceColumn]['value'] * $price;
                    //B Total
                    $row['data'][$ci['aB_BTotal']]['value'] += $row['data'][$bLatePriceColumn]['value'];
                }

                $row['data'][$ci['aB_ATotal']]['value'] += $row['data'][$aPriceColumn]['value'];
                $row['data'][$ci['aB_BTotal']]['value'] += $row['data'][$bPriceColumn]['value'];
                //Full Price Total (Sum all of the quantities in A + B ) where price = full price
                $row['data'][$ci['aBDailyCashTotal']]['value'] += ($price - $commission)*$row['data'][$aPriceColumn]['value'];

                //Full Price And Discount Price Total (all of the prices)
                $row['data'][$ci['aB_APlusBTotal']]['value']   += $row['data'][$aPriceColumn]['value'] + $row['data'][$bPriceColumn]['value'];
                if($bLatePriceColumn){//if the late price column is there add that to the total
                    $row['data'][$ci['aB_APlusBTotal']]['value']   += $row['data'][$bLatePriceColumn]['value'];
                }
            }
        }

        return $rows;
    }

    /**
     * Calculates the colspans for all of the columns in the header
     *
     * @return array An array that represents the table header with corresponding spans and classes
     */
    function calculateHeader(){
        $photoVideoData = $this->photoVideoData;

        $tableDate = $this->photoVideoData->date;
        $tableMonth = strtoupper($tableDate->format('F'));
        $tableYear = $tableDate->format('Y');


        $prices = $photoVideoData->prices;
        $lateAfterSalePrices = $photoVideoData->lateAfterSalesPrices;//grab the mainPrice from settings
        $mainPrice  = $photoVideoData->priceSettings->mainPrice;//grab the mainPrice from settings
        $commission = 500;//leave this as a hardcoded value for now as adding UI might confuse

        $allPrices = $prices;

        $aPriceColumns = array();//prepend an 'a' to the value for the a price column ie 3000 = a3000
        foreach($allPrices as $price){
            if(!$photoVideoData->priceSettings->priceIsEnabledForStandard($price)){//if the price is not enabled make it readonly
                $aPriceColumns[] = array('value' => $price, 'name' => "a$price", 'classes' => 'numericColumn vertical readOnly');
            } else $aPriceColumns[] = array('value' => $price, 'name' => "a$price", 'classes' => 'numericColumn vertical');
        }

        $bPriceColumns = array();
        foreach($allPrices as $price){//prepend an 'b' to the value for the a price column ie 3000 = b3000
            if(!$photoVideoData->priceSettings->priceIsEnabledForAfterSale($price)){//if the price is not enabled make it readonly
                $bPriceColumns[] = array('value' => $price, 'name' => "b$price", 'classes' => 'numericColumn vertical readOnly');
            } else $bPriceColumns[] = array('value' => $price, 'name' => "b$price", 'classes' => 'numericColumn vertical');
        }

        $aOtherColumns = array(
            array('value' => "Standard x $mainPrice Total", 'name' => 'aStandardXTotal1'),
            array('value' => 'Commissions x 500 Total', 'name' => 'aCommissionsXTotal'),
            array('value' => "Standard x ".($mainPrice - $commission)." Total", 'name' => 'aStandardXTotal2')
        );

        //Append the Late After Sale Columns
        $bOtherColumns = array();
        foreach($lateAfterSalePrices as $lateAfterSalePrice){
            if($photoVideoData->priceSettings->priceIsEnabledForLateAfterSale($lateAfterSalePrice)){//if the price is not enabled make it readonly
                $bOtherColumns[] = array("value" => "$lateAfterSalePrice (late)", "name" => "b{$lateAfterSalePrice}late", "classes" => "numericColumn vertical smallHeader");
            } else {//not editable
                $bOtherColumns[] = array("value" => "$lateAfterSalePrice (late)", "name" => "b{$lateAfterSalePrice}late", "classes" => "numericColumn vertical smallHeader readonly");
            }
        }
        //Append the After Sales Total
        $bOtherColumns[] = array('value' => 'Water Touch', 'name' => 'bWaterTouch');
        $bOtherColumns[] = array('value' => 'After Sales Total', 'name' => 'bAfterSalesTotal');

        $aBColumns = array(//columns in the A+B section
            array('value' => 'A Total', 'classes' => '', 'name' => 'aB_ATotal'),
            array('value' => 'B Total', 'classes' => '', 'name' => 'aB_BTotal'),
            array('value' => 'A+B Total', 'classes' => '', 'name' => 'aB_APlusBTotal'),
            array('value' => 'DAILY CASH TOTAL', 'classes' => '', 'name' => 'aBDailyCashTotal')
        );

        //Calculate the column spans that we can use to dynamically adjust the width of parent columns if prices change
        $aColumnSpan    = count($aOtherColumns) + count($allPrices);//Column span for A
        $bColumnSpan    = count($bOtherColumns) + count($allPrices);//Column span for B
        $aBColumnSpan   = count($aBColumns);//Column span for A+B

        $row3 = array_merge($aPriceColumns, $aOtherColumns, $bPriceColumns, $bOtherColumns, $aBColumns);

        $row2 = array(
            array('value' => "Year $tableYear DAY", 'rowspan'=> 2, 'name' => 'year', 'classes' => '', 'id' => ''),
            array('value' => 'Standard', 'name' => 'standard', 'classes' => 'warning aSection', 'colspan' => $aColumnSpan, 'id' => ''),
            array('value' => 'After-Sales', 'name' => 'afterSales', 'classes' => 'warning bSection', 'colspan' => $bColumnSpan, 'id' => ''),
            //array('value' => "Year $tableYear DAY", 'rowspan'=> 1, 'name' => 'yearx', 'classes' => '', 'id' => ''),
            //array('value' => "Year $tableYear DAY", 'rowspan'=> 1, 'name' => 'year2', 'classes' => '', 'id' => ''),
        );

        $row1 = array(
            array('value' => "$tableMonth",'classes' => '', 'name' => 'monthName', 'id' => ''),
            array('value' => 'A', 'classes' => 'aSection', 'name' => 'a', 'colspan' => $aColumnSpan, 'id' => ''),
            array('value' => 'B', 'classes' => 'bSection', 'name' => 'b', 'colspan' => $bColumnSpan, 'id' => ''),
            array('value' => 'A+B',	'classes' => 'aB', 'name' => 'a+b', 'colspan' => $aBColumnSpan, 'rowspan' => 2, 'id' => ''),
            //array('value' => "$tableMonth", 'classes' => '', 'name' => 'monthName2', 'id' => '')
        );

        $row0 = array(
            array('value' => 'MONTH', 'name' => 'month', 'classes' => '', 'id' => ''),
            array('value' => 'INCOME RECEIVED', 'name' => 'incomeReceived', 'classes' => 'whiteOnGrey', 'colspan' => $aColumnSpan + $bColumnSpan + $aBColumnSpan, 'id' => ''),
            array('value' => 'Number of Jumpers', 'name' => 'yenJumpers', 'classes' => 'vertical yenJumpers', 'rowspan'=> 4,'id' => ''),
            array('value' => 'NAME', 'name' => 'name', 'classes' => 'vertical name', 'rowspan'=> 4,'id' => ''),
            array('value' => 'SALES NOTES', 'name' => 'notes', 'classes' => 'vertical notes',  'rowspan'=> 4,'id' => ''),
            array('value' => 'ADMIN NOTES', 'name' => 'adminNotes', 'classes' => 'vertical notes',  'rowspan'=> 4,'id' => ''),
            array('value' => 'DAILY REPORTS', 'name' => 'dailyReports', 'classes' => 'vertical dailyReports', 'rowspan'=> 4,'id' => ''),
            array('value' => 'Day', 'name' => 'dayRight', 'rowspan' => '4', 'classes' => 'vertical', 'id' => '')
        );

        $this->columnCount = $tableColumnCount = $aColumnSpan + $bColumnSpan + $aBColumnSpan + 6;//6 Columns = year+7500 Yen+Name+Sales Notes + Daily Report+dayRight

        return array($row0, $row1, $row2, $row3);
    }

    function calculateRows(){
        $columnNameToIndex = $this->columnNameToIndex;
        $datesAndPrices = $this->datesAndPrices;
        $photoVideoData = $this->photoVideoData;

        //Put the data in the correct row... This is presentational code
        $rowTemplate = array();
        //Generate Cells
        foreach($this->columnNameToIndex as $columnName => $columnIndex){
            $readOnly = array(
                'year', 'dayRight', 'aStandard', 'aCommissionsXTotal', 'aStandardXTotal2', 'aStandardXTotal1',
                'bAfterSalesTotal', 'aB_BTotal', 'aB_ATotal', 'aB_APlusBTotal', 'aBDailyCashTotal',
                'notes', 'dailyReports', 'adminNotes'
            );

            if(CURRENT_SITE_ID != 4)
                $readOnly[] = "bWaterTouch";

            if(in_array($columnName, $readOnly)){
                //readonly cells
                $rowTemplate[] = array('type' => 'readonly', 'value' => 0);
            } else {
                //editable cells
                $rowTemplate[] = array('type' => 'input', 'classes'=>'inputCell', 'value' => 0, 'name' => '');
            }
        }

        if(isset($row)) unset($row);
        if(isset($dateAndPrices))unset($dateAndPrices);

        foreach($datesAndPrices as $dateKey=> $dateAndPrices){
            $dateTimeTemp = DateTime::createFromFormat("Y-m-d", $dateKey);
            $dayNumber = (int)$dateTimeTemp->format("d");//casting to int removes the leading zero

            $rows[$dayNumber] = array('classes' => 'dataRow', 'data' => $rowTemplate);

            $row = &$rows[$dayNumber]['data'];
            $row[$columnNameToIndex['year']]['value']  = $dayNumber;
            $row[$columnNameToIndex['dayRight']]['value']  = $dayNumber;
            $row[$columnNameToIndex['year']]['link']  = "photoVideoTableView.php?daily=1&date=$dateKey";
            $row[$columnNameToIndex['dayRight']]['link']  = "photoVideoTableView.php?daily=1&date=$dateKey";
            //$row[$columnNameToIndex['year']]['rawValue']    = $dayNumber;
            $row[$columnNameToIndex['yenJumpers']]['value'] = '';
            //since this is not an array we can just write the url here instead
            $row[$columnNameToIndex['notes']] = "<a href='#' rel='$dateKey' class='note'>New</a>";
            $row[$columnNameToIndex['adminNotes']] = "<a href='#' rel='$dateKey' class='adminNotes'>New</a>";
            $row[$columnNameToIndex['dailyReports']] = "<a href='#' rel='$dateKey' class='dailyReports'>New</a>";
            $row[$columnNameToIndex['name']]['value'] = '';

            $row[$columnNameToIndex['name']]['name'] = "meta[$dateKey][i][staffName]";
            $row[$columnNameToIndex['yenJumpers']]['name'] = "meta[$dateKey][i][numberOfJumpers]";
            //$row[$columnNameToIndex['name']]['value'] = $dateAndPrices['standard'][0]['u']['staffName'];
            //get the first price from standard

            //if there is any meta data with the column, set it to that
            if(isset($dateAndPrices['meta'])){
                $row[$columnNameToIndex['name']]['value'] = $dateAndPrices['meta']['staffName'];
                $row[$columnNameToIndex['yenJumpers']]['value'] = $dateAndPrices['meta']['numberOfJumpers'];

                if(strlen($dateAndPrices['meta']['note'])) {
                    $row[$columnNameToIndex['notes']] = "<a href='#' rel='$dateKey' class='note'>Open</a>";
                }
                if(strlen($dateAndPrices['meta']['dailyReport'])) {
                    $row[$columnNameToIndex['dailyReports']] = "<a href='#' rel='$dateKey' class='dailyReports'>Open</a>";
                }
                if(strlen($dateAndPrices['meta']['adminNote'])) {
                    $row[$columnNameToIndex['adminNotes']] = "<a href='#' rel='$dateKey' class='dailyReports'>Open</a>";
                }
            }
            //get the metaI
            //It is possible for a date to have a note on it even if nothing has been saved for it
            //so we must check both in the updates (u) and the inserts (i) for an existing metaId
            if(isset($dateAndPrices['meta'])) {
                $row[$columnNameToIndex['name']]['name'] = "meta[$dateKey][u][staffName]";
                $row[$columnNameToIndex['yenJumpers']]['name'] = "meta[$dateKey][u][numberOfJumpers]";

                $waterTouchColumn = $columnNameToIndex['bWaterTouch'];
                $row[$waterTouchColumn]['value'] = 0 ? 0 : $dateAndPrices['meta']['waterTouch'];
                $row[$waterTouchColumn]['name'] = "meta[$dateKey][u][waterTouch]";

                //makes the water touch column readonly for every site except Itsuki
                if(CURRENT_SITE_ID != 4)
                $row[$waterTouchColumn]['classes'] = "readonly";

            } else {
                $row[$columnNameToIndex['name']]['name'] = "meta[$dateKey][i][staffName]";
                $row[$columnNameToIndex['yenJumpers']]['name'] = "meta[$dateKey][i][numberOfJumpers]";

                $waterTouchColumn = $columnNameToIndex['bWaterTouch'];
                $row[$waterTouchColumn]['value'] = 0;
                $row[$waterTouchColumn]['name'] = "meta[$dateKey][i][waterTouch]";

                //makes the water touch column readonly for every site except Itsuki
                if(CURRENT_SITE_ID != 4)
                $row[$waterTouchColumn]['classes'] = "readonly";

            }


            /*
            start filling in the cells with values and summing up the footer
            actionKey is used to tell the saving function what to do with the data. When we submit, we use the
            various array keys such as [2015-01-01][3000][] etc to figure out the price, year etc
            If we decided to use row id, as the key, then new rows would have no row id so we might have something
            like [2015-01-01][3000][rowid] for an exiting row but [2015-01-01][3000][] for a new row which would
            cause all kinds of weird issues in PHP appending new values
            The simplest solution is to store the value in another key that indicates if the row exists or not
            we could then decide whether to update or insert based on that
            i is for an insert
            u is for an update
            both keys will exist for u, but we take only the u key if that is there
            */
            foreach($dateAndPrices['standard'] as $priceData){

                if(isset($priceData['u'])) $actionKey = 'u';
                else $actionKey = 'i';

                $price = $priceData[$actionKey]['price'];
                $quantity = $priceData[$actionKey]['quantity'];
                if(!$photoVideoData->priceSettings->priceIsEnabledForStandard($price)){//if this price is not editable
                    //set the class & type to readonly
                    $row[$columnNameToIndex["a$price"]]['classes'] = 'readonly';
                    $row[$columnNameToIndex["a$price"]]['type'] = 'readonly';
                }

                $row[$columnNameToIndex["a$price"]]['value'] += $quantity;
                $row[$columnNameToIndex["a$price"]]['name'] = "data[$dateKey][standard][$price][$actionKey]";
            }
            unset($priceData);

            foreach($dateAndPrices['afterSale'] as $priceData){
                if(isset($priceData['u'])) $actionKey = 'u';
                else $actionKey = 'i';

                $price = $priceData[$actionKey]['price'];
                $quantity = $priceData[$actionKey]['quantity'];

                $bPriceColumn = $columnNameToIndex["b$price"];

                if(!$photoVideoData->priceSettings->priceIsEnabledForAfterSale($price)){//if this price is not editable
                    //set the class & type to readonly
                    $row[$bPriceColumn]['classes']  = 'readonly';
                    $row[$bPriceColumn]['type']     = 'readonly';
                }
                $row[$bPriceColumn]['value'] += $quantity;
                $row[$bPriceColumn]['name'] = "data[$dateKey][afterSale][$price][$actionKey]";
            }

            foreach($dateAndPrices['lateAfterSale'] as $priceData) {
                if (isset($priceData['u'])) $actionKey = 'u';
                else $actionKey = 'i';

                $price = $priceData[$actionKey]['price'];
                $quantity = $priceData[$actionKey]['quantity'];

                if($lateAfterSalesColumn = $columnNameToIndex["b{$price}late"]) {//temporary until ...
                    $row[$lateAfterSalesColumn]['value'] = $quantity;
                    $row[$lateAfterSalesColumn]['name'] = "data[$dateKey][lateAfterSale][$price][$actionKey]";

                    if(!$photoVideoData->priceSettings->priceIsEnabledForLateAfterSale($price)){//if this price is not the same as the mainPrice then make it uneditable
                        //set the class & type to readonly
                        $row[$lateAfterSalesColumn]['classes']  = 'readonly';
                        $row[$lateAfterSalesColumn]['type']     = 'readonly';
                    }
                }
            }

            unset($priceData);
        }
        return $rows;
    }
}