<?php
require_once(dirname(__FILE__).'/../../includes/application_top.php');
//require_once(dirname(__FILE__).'/photoVideoData.php');
//get the data
//do all of the calculations
//then build the table for presentation
//Create the
class testTableData extends TableData{

    public function __construct(){
        //setUp the
        $this->data['header']       = $this->calculateHeader();
        $this->columnNameToIndexes  = $this->parseHeaderToColumnIndex($this->data['header']);
        $this->columnNameToIndex    = $this->columnNameToIndexes[0];

        $header             = $this->data['header'];
        //$this->data['rows'] = $this->calculateRows();
        //$this->data['rows'] = $this->calculateTotals();
        //$footer             = $this->calculateFooter();

        $this->data = array(
            'header' => $header,
            'rows'   => array(
                        array('data' => array(0, 0, 0, 0, 0, 0, 0, 0, 0)),
                        array('data' => array(0, 0, 0, 0, 0, 0, 0, 0, 0)),
                        array('data' => array(0, 0, 0, 0, 0, 0, 0, 0, 0)),
                        array('data' => array(0, 0, 0, 0, 0, 0, 0, 0, 0)),
            ),
            'footer' => array('data' => array(0, 0, 0, 0, 0, 0, 0, 0, 0)),
        );
    }

    function calculateRows() {


    }

    function calculateHeader(){
		$row0 = array(
            array('value' => 'C1', 'classes' => '', 'rowspan' => '2', 'colspan' => '1', 'name' => 'c1'),
            array('value' => 'C2', 'classes' => '', 'rowspan' => '1', 'colspan' => '4', 'name' => 'c2'),
            array('value' => 'C4', 'classes' => '', 'rowspan' => '2', 'colspan' => '1', 'name' => 'c4'),
            array('value' => 'C5', 'classes' => '', 'rowspan' => '2', 'colspan' => '2', 'name' => 'c5'),
            array('value' => 'C6', 'classes' => '', 'rowspan' => '1', 'colspan' => '1', 'name' => 'c6')
		);

		$row1 = array(
            array('value' => 'C3', 'classes' => '', 'rowspan' => '1', 'colspan' => '4', 'name' => 'c3'),
            array('value' => 'C7', 'classes' => '', 'rowspan' => '1', 'colspan' => '1', 'name' => 'c7')
		);

		return array($row0, $row1);

    }
}

