<?php
require_once('../includes/application_top.php');
require_once('classes/photoVideoSettings.php');

class PhotoVideoData {
    public $datesAndPrices;
    public $prices;
    public $priceSettings;
    public $lateAfterSalesPrices;
    public $date;

    //this should be moved to teh settings class
    private function getLateAfterSalesPrices($siteId) {
        $sql = "SELECT price FROM photoAndVideos WHERE siteId = $siteId AND lateAfterSale = 1 GROUP BY price ASC ORDER BY `date`;";
        $lateAfterSaleResults = queryForRows($sql);
        $prices = array();
        foreach ($lateAfterSaleResults as $index => $lateAfterSaleResult) {
            $prices[] = $lateAfterSaleResult['price'];
        }
        //get the prices from settingsPhotoAndVideo.php
        $settings = new photoVideoSettings($siteId);

        foreach ($settings->prices['lateAfterSale'] as $index => $lateAfterSaleConfig) {//get the prices retrieved from settings
            $prices[] = $index;
        }

        //$prices[] = $settings->mainPrice;//append the main price from settings to all of the found prices
        //don't add main to prices, let it be manually managed by the user
        $prices = array_unique($prices);
        rsort($prices);

        return $prices;
    }

    //get all of the prices and the non late aftersale prices too.
    //this should be moved to teh settings class
    private function getRegularPrices($siteId) {
        $sql = "SELECT price FROM photoAndVideos WHERE siteId = $siteId AND lateAfterSale !=1 GROUP BY price ASC ORDER BY `date`;";
        $lateAfterSaleResults = queryForRows($sql);
        $prices = array();
        foreach ($lateAfterSaleResults as $index => $lateAfterSaleResult) {
            $prices[] = $lateAfterSaleResult['price'];
        }
        //get the prices from settingsPhotoAndVideo.php
        $prices = array_unique($prices);
        sort($prices);

        return $prices;
    }

    //get all of the prices and the non late aftersale prices too.
    private function getMeta($siteId, $dateTimeStart, $dateTimeEnd) {
        $dateTimeStartString = $dateTimeStart->format("Y-m-d");
        $dateTimeEndString = $dateTimeEnd->format('Y-m-d');

        $sql = "
            SELECT *
              FROM photoAndVideoMeta
              WHERE `date` >= '$dateTimeStartString'
                AND `date` <'$dateTimeEndString'
                AND siteId = $siteId ORDER BY `date`;";

        $meta = queryForRows($sql);
        $allMeta = array();
        foreach ($meta as $index => $info) {
            $allMeta[$info['date']] = array(
                'id' => $info['id'],
                'siteId' => $info['siteId'],
                'date' => $info['date'],
                'note' => $info['note'],
                'staffName' => $info['staffName'],
                'dailyReport' => $info['dailyReport'],
                'waterTouch' => $info['waterTouch'],
                'adminNote' => $info['adminNote'],
                'numberOfJumpers' => $info['numberOfJumpers']
            );
        }

        return $allMeta;
    }

    function __construct($dateTimeStart, $dateTimeEnd, $options = null) {
        $siteId = CURRENT_SITE_ID;
        $this->date = clone $dateTimeStart;
        $this->priceSettings = new photoVideoSettings($siteId);
        $this->lateAfterSalesPrices = $this->getLateAfterSalesPrices($siteId);

        //this is the problem for the daily view. I think we should specify this outside of the class
        $dateTimeStartString = $dateTimeStart->format("Y-m-d");
        $dateTimeEndString = $dateTimeEnd->format('Y-m-d');

        $sql = "
            SELECT pv.id, pv.price, pv.date, pv.quantity, pv.siteId, pv.standard, pv.afterSale, pv.lateAfterSale, pm.note, pm.numberOfJumpers, pm.staffName, pm.id AS metaId, pm.adminNote, pm.waterTouch
              FROM photoAndVideos as pv
              LEFT JOIN photoAndVideoMeta AS pm ON (pv.date = pm.date)
            WHERE pv.date >= '$dateTimeStartString'
            AND pv.date <'$dateTimeEndString'
            AND pv.siteId = $siteId
            ORDER BY pv.date;";
        $photosAndVideos = queryForRows($sql);

        $datesAndPrices = array();

        $prices = $this->getRegularPrices($siteId);
        $meta = $this->getMeta($siteId, $dateTimeStart, $dateTimeEnd);
        /*
            take the rows from the database and make format them as below:
            2015-02-01
                   lateAfterSale
                          [3000][u]
                               id  => 661, date => date, price => price, quantity => 3, siteId  => 1
                          [2000][u]
                               id  => 661, date => date, price => price, quantity => 3, siteId  => 1
                   standard
                          [3000][u]
                               id  => 661, date => date, price => price, quantity => 3, siteId  => 1
                   afterSale
                          [3000][u]
                               id  => 661, date => date, price => price, quantity => 3, siteId  => 1,
                          [2000][u]
                               id  => 661, date => date, price => price, quantity => 3, siteId  => 1,
                          [1000][u]
                               id  => 661, date => date, price => price, quantity => 3, siteId  => 1,
        */


        foreach ($photosAndVideos as $key => $priceAndQuantity) {
            $price = $priceAndQuantity['price'];

            $type = null;
            if ($priceAndQuantity['standard']) {
                $type = 'standard';
            } else if ($priceAndQuantity['afterSale']) {
                $type = 'afterSale';
            } else if ($priceAndQuantity['lateAfterSale']) {
                $type = 'lateAfterSale';
            }

            if($type) $datesAndPrices[$priceAndQuantity['date']][$type][$price]['u'] = array(//u is for updating rows on submit
                'id' => $priceAndQuantity['id'],
                'date' => $priceAndQuantity['date'],
                'price' => $priceAndQuantity['price'],
                'quantity' => $priceAndQuantity['quantity'],
                'siteId' => $priceAndQuantity['siteId'],
                'note' => $priceAndQuantity['note'],
                'adminNote' => $priceAndQuantity['adminNote'],
                'waterTouch' => $priceAndQuantity['waterTouch'],
                'staffName' => $priceAndQuantity['staffName'],
                'numberOfJumpers' => $priceAndQuantity['numberOfJumpers'],
                'metaId' => $priceAndQuantity['metaId']
            );

            /*
            //old non DRY code... delete when ready
            if ($priceAndQuantity['standard']) {
                $datesAndPrices[$priceAndQuantity['date']]['standard'][$price]['u'] = array(//u is for updating rows on submit
                    'id' => $priceAndQuantity['id'],
                    'date' => $priceAndQuantity['date'],
                    'price' => $priceAndQuantity['price'],
                    'quantity' => $priceAndQuantity['quantity'],
                    'siteId' => $priceAndQuantity['siteId'],
                    'note' => $priceAndQuantity['note'],
                    'adminNote' => $priceAndQuantity['adminNote'],
                    'waterTouch' => $priceAndQuantity['waterTouch'],
                    'staffName' => $priceAndQuantity['staffName'],
                    'numberOfJumpers' => $priceAndQuantity['numberOfJumpers'],
                    'metaId' => $priceAndQuantity['metaId']

                );
            } else if ($priceAndQuantity['afterSale']) {
                $datesAndPrices[$priceAndQuantity['date']]['afterSale'][$price]['u'] = array(
                    'id' => $priceAndQuantity['id'],
                    'date' => $priceAndQuantity['date'],
                    'price' => $priceAndQuantity['price'],
                    'quantity' => $priceAndQuantity['quantity'],
                    'siteId' => $priceAndQuantity['siteId'],
                    'note' => $priceAndQuantity['note'],
                    'adminNote' => $priceAndQuantity['adminNote'],
                    'waterTouch' => $priceAndQuantity['waterTouch'],
                    'staffName' => $priceAndQuantity['staffName'],
                    'numberOfJumpers' => $priceAndQuantity['numberOfJumpers'],
                    'metaId' => $priceAndQuantity['metaId']

                );
            } else if ($priceAndQuantity['lateAfterSale']) {
                $datesAndPrices[$priceAndQuantity['date']]['lateAfterSale'][$price]['u'] = array(
                    'id' => $priceAndQuantity['id'],
                    'date' => $priceAndQuantity['date'],
                    'price' => $priceAndQuantity['price'],
                    'quantity' => $priceAndQuantity['quantity'],
                    'siteId' => $priceAndQuantity['siteId'],
                    'note' => $priceAndQuantity['note'],
                    'adminNote' => $priceAndQuantity['adminNote'],
                    'waterTouch' => $priceAndQuantity['waterTouch'],
                    'staffName' => $priceAndQuantity['staffName'],
                    'numberOfJumpers' => $priceAndQuantity['numberOfJumpers'],
                    'metaId' => $priceAndQuantity['metaId']

                );
            }
            */
        }

        $savedPrices = $this->priceSettings->getAllPrices();

        $prices = array_merge($prices, $savedPrices);
        $prices = array_unique($prices);//an array of all possible prices

        //reverse sort the prices as they appear in the table in descending order
        rsort($prices);

        //generate an empty template for prices
        $pricesTemplate = array(
            'standard' => array(),
            'afterSale' => array(),
            'lateAfterSale' => array()
        );

        foreach ($prices as $price) {//create a blank template for prices
            $pricesTemplate['standard'][(int)$price]['i'] = array(//[i] is for insertion of new rows on submit
                'id' => null,
                'date' => null,
                'price' => $price,
                'quantity' => 0,
                'siteId' => CURRENT_SITE_ID,
                'note' => '',
                'adminNote' => '',
                'staffName' => '',
                'numberOfJumpers' => '',
                'metaId' => null

            );

            $pricesTemplate['afterSale'][(int)$price]['i'] = array(//[i] is for insertion of new rows on submit
                'id' => null,
                'date' => null,
                'price' => $price,
                'quantity' => 0,
                'siteId' => CURRENT_SITE_ID,
                'note' => '',
                'adminNote' => '',
                'staffName' => '',
                'numberOfJumpers' => '',
                'metaId' => null

            );
            //if the price is a lateAfterSales price then ignore
            if (in_array($price, $this->lateAfterSalesPrices)) {
                $pricesTemplate['lateAfterSale'][(int)$price]['i'] = array(//[i] is for insertion of new rows on submit
                    'id' => null,
                    'date' => null,
                    'price' => $price,
                    'quantity' => 0,
                    'siteId' => CURRENT_SITE_ID,
                    'lateAfterSales' => 1,
                    'note' => '',
                    'adminNote' => '',
                    'staffName' => '',
                    'numberOfJumpers' => '',
                    'metaId' => null

                );
            }
        }

        //Generate an empty template for the time interval
        $emptyTemplate = array();

        while ($dateTimeStart < $dateTimeEnd) {
            $date = $dateTimeStart->format('Y-m-d');

            foreach ($pricesTemplate['standard'] as $priceKey => $priceTemplate) {
                $priceTemplate['i']['date'] = $date;
                //unset($priceTemplate);
            }
            //unset($priceTemplate);

            foreach ($pricesTemplate['afterSale'] as $priceKey => $priceTemplate) {
                $priceTemplate['i']['date'] = $date;
            }

            foreach ($pricesTemplate['lateAfterSale'] as $priceKey => $priceTemplate) {
                $priceTemplate['i']['date'] = $date;
            }
            //unset($priceTemplate);

            $emptyTemplate[$dateTimeStart->format('Y-m-d')] = $pricesTemplate;
            $dateTimeStart->modify('+1 day');
        }

        //these are prices specified to always show in the project specification
        $datesAndPrices = array_replace_recursive($emptyTemplate, $datesAndPrices);

        foreach ($meta as $metaDate => $metaInfo) {
            if (array_key_exists($metaDate, $datesAndPrices)) {
                $datesAndPrices[$metaDate]['meta'] = $metaInfo;
            }
        }

        $this->datesAndPrices = $datesAndPrices;
        $this->prices = $prices;
    }
}
