<?php

class photoVideoSettings {
    public $mainPrice;
    public $siteId;
    public $prices;//An array with prices as the key and if it is enabled as the value
    public $lateAfterSalesPrices;
    public $pricesCount;

    function __construct($siteId) {
        $this->siteId = $siteId;
        $this->loadSettings($siteId);
    }

    function priceIsEnabledForStandard($price){
        if(array_key_exists($price, $this->prices['standard'])){
           return $this->prices['standard'][$price] ? true : false;
        } else return false;
    }

    function priceIsEnabledForAfterSale($price){
        if(array_key_exists($price, $this->prices['afterSale'])){
            return $this->prices['afterSale'][$price] ? true : false;
        } else return false;
    }

    function priceIsEnabledForLateAfterSale($price){
        if(array_key_exists($price, $this->prices['lateAfterSale'])){
            //if the main price is equal to the price being tested or the price is enabled in setting, enable it
            $enabled = ($this->prices['lateAfterSale'][$price] || ($this->mainPrice == $price)) ? true : false;
            return $enabled;
        } else return false;
    }

    function getAllPrices(){//returns all of the prices as an array of values
        $pricesArray = array();
        foreach($this->prices['standard'] as $price => $isEnabled){
            $pricesArray[] = $price;
        }

        foreach($this->prices['afterSale'] as $price => $isEnabled){
            $pricesArray[] = $price;
        }
        return $pricesArray;
    }

    public function saveSettings($postArray) {
        if (!isset($this->siteId)) return false;

        if (count($postArray)) {
            //we need to check that there duplicate prices are not being entered
            if(isset($postArray['newPriceStandardAndAfterSale']) && is_numeric($postArray['newPriceStandardAndAfterSale'])){//if the newPrice has a length
                $newPrice = $postArray['newPriceStandardAndAfterSale'];
                $postArray['prices']['standard'][$newPrice] = 1;//Append the new price to both A section and B section
                $postArray['prices']['afterSale'][$newPrice] = 1;//Append the new price to both A section and B section
                krsort($postArray['prices']['standard']);
                krsort($postArray['prices']['afterSale']);
            }

            if(isset($postArray['newPriceLateAfterSale']) && is_numeric($postArray['newPriceLateAfterSale'])){//if the newPriceLateAfterSale has a length
                $newPrice = $postArray['newPriceLateAfterSale'];
                $postArray['prices']['lateAfterSale'][$newPrice] = 1;//Append the new price to late after sale
                krsort($postArray['prices']['lateAfterSale']);
            }

            $pricesInfoSerialized = json_encode($postArray);
            $data = array('key' => 'photoAndVideoSettings', 'values' => $pricesInfoSerialized, 'site_id' => $this->siteId);

            if (count(queryForRows("SELECT * FROM configuration WHERE site_id = $this->siteId AND `key`= 'photoAndVideoSettings'"))) {
                $where = "site_id = $this->siteId AND `key`= 'photoAndVideoSettings'";
                db_perform('configuration', $data, 'update', $where);
                //update the existing row
            } else {
                db_perform('configuration', $data);
                //insert a new row
            }
            //TODO db perform does not tell you if a save failed it would be good to return this information and act on it

            return true;
        }
        return false;
    }

    public function loadSettings($siteId = null) {
        if ($siteId == null) $siteId = $this->siteId;

        $pricesInfo = array(//default prices and settings
            'prices' => array(
                'standard' => array(
                    5000 => 1,
                    4500 => 1,
                    4000 => 1,
                    3500 => 1,
                    3000 => 1,
                    2500 => 1,
                    2000 => 1,
                    1500 => 1,
                    1000 => 1,
                    500  => 1,
                    0    => 1
                ),

                'afterSale' => array(
                    5000 => 1,
                    4500 => 1,
                    4000 => 1,
                    3500 => 1,
                    3000 => 1,
                    2500 => 1,
                    2000 => 1,
                    1500 => 1,
                    1000 => 1,
                    500  => 1,
                    0    => 1
                ),

                'lateAfterSale' => array(
                ),
            ),
            'mainPrice' => 0,
            'newPrice' => null
        );

        $loadedData = queryForRows("SELECT * FROM configuration WHERE site_id = $siteId AND `key`= 'photoAndVideoSettings'");
        if(!isset($loadedData[0]['value'])){ //Nothing is loaded so use the default settings
            $this->pricesCount = count($pricesInfo['prices']);
            $this->saveSettings($pricesInfo);
            //we could reload the settings but if saving keeps failing we will end in an infinite loop
            //instead just call load settings separately after loadSettings

            $this->prices    = $pricesInfo['prices'];
            $this->mainPrice = $pricesInfo['mainPrice'];

        } else {//Unserialize the data from the database
            $savedSettings = json_decode($loadedData[0]['values'], true);
            $this->prices = $savedSettings['prices'];

            if (!isset($savedSettings['mainPrice'])) {
                $this->mainPrice['mainPrice'] = 0;
                //if(!array_key_exists(0, $this->prices))
                    //$this->prices['lateAfterSale'][0] = 1;//Append the mainPrice to the prices array
            } else {
                $this->mainPrice = $savedSettings['mainPrice'];
                //if(!array_key_exists($savedSettings['mainPrice'], $this->prices))
                    //$this->prices['lateAfterSale'][$savedSettings['mainPrice']] = 1;//Append the mainPrice to the prices array
            }
            $this->pricesCount = count($savedSettings['prices']);
        }
        echo '';
    }
}
?>
