<?php
require_once('../includes/application_top.php');
//get the date from the url
$noteDate = null;
if(isset($_GET['date'])) $noteDate = $_GET['date'];

//POST is being used to submit data for saving
if(isset($_POST['date'])) {
    $note           = mysql_real_escape_string($_POST['note']);
    $siteId         = $_POST['siteId'];
    $date           = $_POST['date'];

    $sql = "SELECT * FROM photoAndVideoMeta WHERE date = '$date' AND siteId = $siteId;";
    $alreadyExists = count(queryForRows($sql)) != 0;

    $action = 'insert';
    $parameters = '';

    if($alreadyExists){
       $parameters = "date = '$noteDate' AND siteId = $siteId;";
       $action = 'update';
    }

    db_perform('photoAndVideoMeta', $_POST, $action, $parameters);
}

$note['date']   = $noteDate;
$note['siteId'] = CURRENT_SITE_ID;
$note['note']   = '';

$sql	= "SELECT * FROM photoAndVideoMeta WHERE date = '$noteDate' AND siteId = $siteId;";
$res	= mysql_query($sql);

if(mysql_num_rows($res)){
    $note	= mysql_fetch_assoc($res);
}
echo '';
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Photo &amp; Video Note: <?php echo $note['date']; ?> </title>
    <?php include "../includes/head_scripts.php"; ?>
    <link rel="stylesheet" href="./bootstrap-3.3.2-dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="css/photoAndVideoBootstrap.css">
    <style>
        #photo-video-note{
            width: 100%;
            height: 100%;
            font-size: 2em;
            white-space:pre-wrap;
            background-color: white;
        }
        #save{
            width: 100%;
            height: 3em;
            font-size: 2.5em;
        }
    </style>
    <title>Supplier Notes</title>
</head>
<body>
<div id="purchase-container">
    <form method="post">
        <h1>Photo &amp; Video Note for: <?php echo $note['date']; ?></h1>
        <h2>Note:</h2>
        <textarea name="note" id="photo-video-note" rows="10" ><?php echo $note['note'];?></textarea>

        <div class="buttons-container">
            <button type="submit" id="save" class="btn btn-success">Save</button>
        </div>
        <input type="hidden" name="date" value="<?php echo $note['date']; ?>">
        <input type="hidden" name="siteId" value="<?php echo $note['siteId']; ?>">
    </form>
</div>
</body>
