<?php
require_once('../includes/application_top.php');

$siteId = CURRENT_SITE_ID;

//log the saved data to a file
$debugOutputFile = __DIR__ . "/{$subdomain}_data.log";

$handle = fopen($debugOutputFile, 'a');
$data = $_POST['data'];

fwrite($handle, date('Y-m-d H:i:s')."\n");
fwrite($handle, $_SERVER['REMOTE_ADDR']."\n");

if ($handle) {
    //echo "file found";
    if (is_array($data)) {
        //write print_r($data);
        $dataArrayAsString = print_r($data, true);
        $dataArrayAsString .= "\n\n";

        fwrite($handle, $dataArrayAsString);
    } else {
        fwrite($handle, $data . "\n\n");
    }
}

foreach ($data as $date => $row) {//we really need a batch save function
    foreach ($row['standard'] as $price => $quantityData) {//if it has an id update or else insert
        if (isset($quantityData['u'])) {
            $action = 'update';
            $actionKey = 'u';
            $parameters = "`date` = '$date' AND price = $price AND siteId = $siteId AND standard = 1 AND afterSale = 0 AND lateAfterSale = 0;";
            $priceAndValue = array('price' => $price, 'quantity' => $quantityData[$actionKey]);

            db_perform('photoAndVideos', $priceAndValue, $action, $parameters);
        } else {//It is not an update and is now an update
            $action = 'insert';
            $actionKey = 'i';
            $parameters = '';

            $priceAndValue = array(
                'date'     => $date,
                'price'    => $price,
                'quantity' => $quantityData[$actionKey],
                'siteId'   => $siteId,
                'standard' => 1
            );

            db_perform('photoAndVideos', $priceAndValue, $action, $parameters);
        }
    }

    foreach ($row['afterSale'] as $price => $quantityData) {
        //if it has an id update or else insert
        if (isset($quantityData['u'])) {
            $action = 'update';
            $actionKey = 'u';
            $parameters = "`date` = '$date' AND price = $price AND siteId = $siteId AND standard = 0 AND afterSale = 1 AND lateAfterSale = 0;";
            $priceAndValue = array('price' => $price, 'quantity' => $quantityData[$actionKey]);

            db_perform('photoAndVideos', $priceAndValue, $action, $parameters);
        } else {//It is not an update and is now an update
            $action = 'insert';
            $actionKey = 'i';
            $parameters = '';

            $priceAndValue = array(
                'date'      => $date,
                'price'     => $price,
                'quantity'  => $quantityData[$actionKey],
                'siteId'    => $siteId,
                'afterSale' => 1
            );

            db_perform('photoAndVideos', $priceAndValue, $action, $parameters);
        }
    }

    foreach ($row['lateAfterSale'] as $price => $quantityData) {//if it has an id update or else insert
        if (isset($quantityData['u'])) {
            $action = 'update';
            $actionKey = 'u';
            $parameters = "`date` = '$date' AND price = $price AND siteId = $siteId AND standard = 0 AND afterSale = 0 AND lateAfterSale = 1;";
            $priceAndValue = array('price' => $price, 'quantity' => $quantityData[$actionKey]);

            db_perform('photoAndVideos', $priceAndValue, $action, $parameters);
        } else {//It is not an update and is now an update
            $action = 'insert';
            $actionKey = 'i';
            $parameters = '';

            $priceAndValue = array(
                'date'          => $date,
                'price'         => $price,
                'quantity'      => $quantityData[$actionKey],
                'siteId'        => $siteId,
                'lateAfterSale' => 1
            );

            db_perform('photoAndVideos', $priceAndValue, $action, $parameters);
        }
    }
}

$meta = $_POST['meta'];
foreach ($meta as $date => $row) {
    if (isset($row['u'])) {
        $photoAndVideoMeta = array(
            'staffName'       => $row['u']['staffName'],
            'numberOfJumpers' => $row['u']['numberOfJumpers'],
            'waterTouch' => $row['u']['waterTouch']
        );
        $action = 'update';
        $parameters = "`date` = '$date' AND siteId = $siteId;";

        db_perform('photoAndVideoMeta', $photoAndVideoMeta, $action, $parameters);
    } else if (isset($row['i'])) {
        $photoAndVideoMeta = array(
            'siteId'          => $siteId,
            'date'            => $date,
            'staffName'       => $row['i']['staffName'],
            'numberOfJumpers' => $row['i']['numberOfJumpers'],
            'waterTouch' => $row['i']['waterTouch']
        );

        db_perform('photoAndVideoMeta', $photoAndVideoMeta);
    }
}

header("Location: {$_SERVER['HTTP_REFERER']}");//go back to the page that sent us here
