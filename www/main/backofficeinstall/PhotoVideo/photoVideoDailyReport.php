<?php
require_once('../includes/application_top.php');
//get the date from the url
$reportDate = null;
if(isset($_GET['date'])) $reportDate = $_GET['date'];

//POST is being used to submit data for saving
if(isset($_POST['date'])) {
    $dailyReport           = mysql_real_escape_string($_POST['dailyReport']);
    $siteId         = $_POST['siteId'];
    $date           = $_POST['date'];

    $sql = "SELECT * FROM photoAndVideoMeta WHERE date = '$date' AND siteId = $siteId;";
    $alreadyExists = count(queryForRows($sql)) != 0;

    $action = 'insert';
    $parameters = '';

    if($alreadyExists){
       $parameters = "date = '$reportDate' AND siteId = $siteId;";
       $action = 'update';
    }

    db_perform('photoAndVideoMeta', $_POST, $action, $parameters);
}

$dailyReport['date']   = $reportDate;
$dailyReport['siteId'] = CURRENT_SITE_ID;
$dailyReport['dailyReport']   = '';

$sql	= "SELECT * FROM photoAndVideoMeta WHERE date = '$reportDate' AND siteId = $siteId;";
$res	= mysql_query($sql);

if(mysql_num_rows($res)){
    $dailyReport	= mysql_fetch_assoc($res);
}
echo '';
?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>Photo &amp; Video Daily Report For: <?php echo $dailyReport['date']; ?> </title>
    <?php
    include "../includes/head_scripts.php";
    echo Html::css("bootstrap/3.3.2/css/bootstrap.min.css");
    echo Html::css("photoVideo.css");
    ?>
    <style>
        #photo-video-note{
            width: 100%;
            height: 100%;
            font-size: 2em;
            white-space:pre-wrap;
            background-color: white;
        }

        #save{
            width: 100%;
            height: 3em;
            font-size: 2.5em;
        }

    </style>
    <title>P&amp;V Daily Report</title>
</head>
<body>
<div id="purchase-container">
    <form method="post">
        <h1>Photo &amp; Video Daily Report for: <?php echo $dailyReport['date']; ?></h1>
        <h2>Report:</h2>
        <textarea name="dailyReport" id="photo-video-note" rows="10" ><?php echo $dailyReport['dailyReport'];?></textarea>

        <div class="buttons-container">
            <button type="submit" id="save" class="btn btn-success">Save</button>
        </div>
        <input type="hidden" name="date" value="<?php echo $dailyReport['date']; ?>">
        <input type="hidden" name="siteId" value="<?php echo $dailyReport['siteId']; ?>">
    </form>
</div>
</body>
