<?php
require_once('../includes/application_top.php');
require_once('classes/photoVideoSettings.php');

if(!$user->hasRole(array('SysAdmin', 'P&V'))){//if daily is specified
    echo "You do not have permission to view this page";
    die();
}

$pricesSettings = new photoVideoSettings(CURRENT_SITE_ID);//constructing auto loads the settings
$previousURL = "photoVideoTableView.php";

if(isset($_POST['prices'])) { //if the user logs in, not checking for prices results in saving the users login info
    $pricesSettings->saveSettings($_POST);
    $pricesSettings->loadSettings(CURRENT_SITE_ID);//reload the settings
    $previousURL = $_SESSION['previousURL'];

} else { //Don't store the referrer URL if we have just submitted data, as the referrer will be the submitting page (this page)
    $previousURL = $_SESSION['previousURL'] = $_SERVER['HTTP_REFERER'];

}

//just go back to the regular page, the part at the top is acting funnily
$previousURL = "photoVideoTableView.php";

$prices    = $pricesSettings->prices;
$mainPrice = $pricesSettings->mainPrice;

?>
<!DOCTYPE html>
<html>
<head>
    <?php
    echo Html::css("bootstrap/3.3.2/css/bootstrap.min.css");
    echo Html::css("photoVideo.css");
    ?>
    <title><?echo ucwords($subdomain)?> P&amp;V Settings</title>
</head>
<body>
<div id="pageContainer">

    <h1 class="tableTitle"><?php echo ucwords($subdomain) ?> - P&amp;V Settings</h1>

    <form action="#" method="POST">
        <!--<form action="controller.php" method="POST">-->
        <table class="settingsTable">
            <colgroup>
                <col style="width:40%">
                <col style="width:30%">
                <col style="width:30%">
            </colgroup>
            <tr>
                <th class="" colspan="3"> A - Standard</th>
            </tr>
            <tr>
                <th class="priceColumn"></th>
                <th class="inputColumn">Enable Input</th>
                <th class="inputColumn">Main Price</th>
            </tr>
            <?php
            foreach ($prices['standard'] as $price => $priceSettings) {
                $isAllowedInput = $priceSettings ? true : false;//This price may be edited in the tableView
                $isMainPrice = ($mainPrice == $price);//This is the main price of purchased items

                $enabledPriceOptions = $mainPriceOptions = array();
                if($isAllowedInput) $enabledPriceOptions['checked'] = true;
                if($isMainPrice)    $mainPriceOptions['checked']    = true;

                $checkBoxString = Form::checkBox("prices[standard][$price]", 1, '', $enabledPriceOptions);
                $radioBoxString = Form::radioButton("mainPrice", $price, '', $mainPriceOptions);

                echo '';
                echo "<tr>
                                <td class='priceCell'>$price</td>
                                <td>$checkBoxString</td>
                                <td>$radioBoxString</td>
                      </tr>\n";
            }

            echo '</table>';

            echo '<br>';

            echo '<table class="settingsTable">
            <tr>
                <th class="" colspan="3"> B - After Sales</th>
            </tr>
            <tr>
                <th class="priceColumn">Price</th>
                <th class="inputColumn">Enable Input</th>
            </tr>';

            foreach ($prices['afterSale'] as $price => $priceSettings) {
                $isAllowedInput = $priceSettings ? true : false;//This price may be edited in the tableView
                $isMainPrice = ($mainPrice == $price);//This is the main price of purchased items

                $enabledPriceOptions = $mainPriceOptions = array();
                if($isAllowedInput) $enabledPriceOptions['checked'] = true;
                if($isMainPrice)    $mainPriceOptions['checked']    = true;

                $checkBoxString = Form::checkBox("prices[afterSale][$price]", 1, '', $enabledPriceOptions);
                //$radioBoxString = Form::radioButton("mainPrice", $price, '', $mainPriceOptions);

                echo '';
                echo "<tr>
                                <td class='priceCell'>$price</td>
                                <td>$checkBoxString</td>
                      </tr>\n";
            }
            ?>

            </table>


            <br>
            <table class="settingsTable">
                <tr>
                    <td class='priceCell'> Add New Price Point To A &amp; B:</td>
                    <td ><input type="text" name="newPriceStandardAndAfterSale" id="settingsTableNewPrice"></td>
                </tr>
            </table>
            <br>
            <?php

            $lateAfterSalesHeaders = '<table class="settingsTable">
                <tr>
                    <th class="" colspan="3"> B - Late After Sales</th>
                </tr>'.$lateAfterSalesHeaders;

            //if there are prices show the headings (price heading and enable input) or else hide them
            if(count($prices['lateAfterSale'])) {
                $lateAfterSalesHeaders .='<tr>
                    <th class="priceColumn"></th>
                    <th class="inputColumn">Enable Input</th>
                </tr>';
            }

            echo $lateAfterSalesHeaders;

            foreach ($prices['lateAfterSale'] as $price => $priceSettings) {
                $isAllowedInput = $priceSettings ? true : false;//This price may be edited in the tableView
                $isMainPrice = ($mainPrice == $price);//This is the main price of purchased items

                $enabledPriceOptions = $mainPriceOptions = array();
                if($isAllowedInput) $enabledPriceOptions['checked'] = true;
                if($isMainPrice)    $mainPriceOptions['checked']    = true;

                $checkBoxString = Form::checkBox("prices[lateAfterSale][$price]", 1, '', $enabledPriceOptions);
                //$radioBoxString = Form::radioButton("mainPrice", $price, '', $mainPriceOptions);

                echo '';
                echo "<tr>
                                    <td class='priceCell'>$price</td>
                                    <td>$checkBoxString</td>
                          </tr>\n";
            }
            ?>
            </table>

        <br>
        <table class="settingsTable">
            <tr>
                <td class='priceCell'> Add New Price Point To Late After Sales:</td>
                <td ><input type="text" name="newPriceLateAfterSale" id="settingsTableNewPrice"></td>
            </tr>
        </table>

        <input id='settingsTableSubmit' type="submit" class="btn btn-success" value="Update Settings">
        <center><a href="<?php echo $previousURL?>">Back to Photo & Video</a></center>

    </form>
</div>
</body>
</html>


