<?php
require_once('../includes/application_top.php');
error_reporting(E_ALL);
ini_set('display_errors', 'On');
require_once('classes/testTableData.php');

$tableViewData = new TestTableData();
$testTableView = new TableView($tableViewData);
?>
<!DOCTYPE html>
<html>
	<head>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js"></script>
        <script src="//ajax.googleapis.com/ajax/libs/jqueryui/1.10.1/jquery-ui.min.js"></script>
        <link rel="stylesheet" href="http://code.jquery.com/ui/1.10.1/themes/base/jquery-ui.css" type="text/css" media="all" />

        <?php
        echo Html::css("bootstrap/3.3.2/css/bootstrap.min.css");
        echo Html::css("photoVideo.css");
        ?>
        <script>
        $(document).ready(function(){
            //click to open the Notes Editing View
            $('td').change(function(){
                $(this).css('background-color', '#5cb85c');
            })

        });

    </script>
	</head>
	<body>
        <div id="pageContainer">

            <?php
                echo $testTableView->drawTable();
            ?>
        </div>
    </body>
</html>


