<?
require_once('../includes/application_top.php');
require_once('functions.php');
mysql_query("SET NAMES UTF8");
//parse the get request in to ...

$sites      = $_GET['sites'];
$startDate  = $_GET['startDate'];
$endDate    = $_GET['endDate'];
$duration   = $_GET['duration'];
$comparison = $_GET['comparison'];
$metrics    = $_GET['metrics'];
$timeUnit   = $_GET['timeUnit'];

//$_GET['date'] = str_replace(['(',')'], '',$_GET['date']);

//$sites = 0;
//$comparison = 2;
$yAxisLabels = [
    "getIncome" => "Income (Yen)",
    "getIncomeABC" => "Income (Yen)",
    "getBookings" => "Bookings Received",
    "getJumpsBooked" => "Jumps Booked on Day",
    "getJumpNumbers" => "Jumps Performed on Day",
    "getCancellations" => "Cancellations",
    "getSecondJumps" => "Second Jumps",
    "getWalkIn" => "Walk In Bookings",
    "getNonJumpers" => "Non-Jumpers",
];

//TODO: Custom Start and End date handling
$startDate      = DateTime::createFromFormat("Y-m-d", $_GET['startDate']);
$endDate      = DateTime::createFromFormat("Y-m-d", $_GET['endDate']);
$years          = rangeOfYearsForComparison($startDate->format("Y"), $comparison);
$yearDateRanges = calculateStartAndEndDates($years, $duration, clone $startDate, clone $endDate);

$dataSetsBySite = [];

//TODO: Replace this with a function that gets this information from the database
$siteIdToName = [
    '1' => 'Minakami',
    '2' => 'Sarugakyo',
    '3' => 'Ryujin',
    '4' => 'Itsuki',
    '10' => 'Nara',
];

if($sites == 0) $allSites = [1, 2, 3, 4, 10];
else $allSites = [$sites];

$yAxisLabel = 0;

foreach($metrics as $functionName => $metric) {
    if($metric === "false") continue;
    $yAxisLabel = $yAxisLabels[$functionName];

    $graphs = [];
    foreach($allSites as $site) {
        $siteName = $siteIdToName[$site];
        $dataSetCount = 0;
        //Get data for each year

        //This causes an issue where when the year changes, the end of the week change, so we
        //end up with differing beginning and end dates
        //an alternative solution is to compare day numbers for a year however we need to consider the
        //or calculate an extra week before and after and crop the results
        //Comparing years...
        //If we compare weeks, this problem occurs


        foreach($yearDateRanges as $year => $yearDateRange) {

            //TODO: when the data functions are moved to an object, call_user_func needs to be restricted to only that
            //set of functions to prevent requests for other functions being made.
            //graphs is going to be
                //site
                    //....
                        //graphs by name
                            //
            $graphs[$site][$year]["graphs"] = callDispatcher($functionName, $site, $yearDateRange['begin'], $yearDateRange['end'], $timeUnit, $comparison,$options);
        }

        unset($index);
        $firstGraph = true;
        $currentYearGraphs =  reset($graphs[$site]);//get the first item

        foreach($graphs[$site] as $previousYear => $previousYearGraphs)
        {
            //ignore the first graph (it will be the current year) as we dont need to cmpare it to itself
            if($firstGraph == true) {
                $firstGraph = false;
                continue;
            }

            foreach($currentYearGraphs['graphs'] as $graphType => $currentYearGraph)
            {
                $comparisonGraph = &$graphs[$site][$previousYear]["comparisonsGraphs"][$graphType];
                $comparisonGraph['data'] =  calculatePercentageChangeDataSet($currentYearGraphs['graphs'][$graphType]['data'], $previousYearGraphs['graphs'][$graphType]['data']);

                $group = $comparisonGraph['group'];
                if($group)
                    $graphs[$site][$previousYear]["comparisonsGraphs"][$graphType]['group'] = $group."_comparison";
            }

            echo '';

        }
        /*
        foreach($dataSetsBySite['sites'][$site] as $index => $dataSetForYear)
        {
            if($index == 0) continue;//zero is the maximum year and since we have already have data for it, ignore it.
            //generate sequence for the maximum year and the previous year

            $graphs[$site][$year]["graphs"] = calculatePercentageChangeDataSet($dataSetsBySite['sites'][$site][0]['data'], $dataSetForYear['data']);;
            $dataSetCount = count($dataSetsBySite['sites'][$site]);

            $yearOld    =  ($dataSetsBySite['sites'][$site][0]['year']);//the maximum year
            $yearNew    =  ($dataSetForYear['year']);
            $dataSetsBySite['sites'][$site][$dataSetCount]['data'] = calculatePercentageChangeDataSet($dataSetsBySite['sites'][$site][0]['data'], $dataSetForYear['data']);
            $dataSetsBySite['sites'][$site][$dataSetCount]['title'] = "$yearNew - {$dataSetForYear['year']} % $siteName";
            $dataSetsBySite['sites'][$site][$dataSetCount]['site'] = $site;
            $dataSetsBySite['sites'][$site][$dataSetCount]['year'] = null;
            $dataSetsBySite['sites'][$site][$dataSetCount]['type'] = 'percentage';
        }
        */
    }
}

function graphNamer($functionName, $site, $begin, $end)
{
    $name = str_replace("get", "", $functionName);
    $begin = str_replace("-", "", $begin);
    $end = str_replace("-", "", $end);

    return $site.$name.$begin."-".$end;
}

function graphMaker($functionName, $functionTitle, $site, $begin, $end, $groupName, $data)
{
    global $siteIdToName;

    $graph['title'] = "{$siteIdToName[$site]} $functionTitle";
    $graph['name'] = graphNamer($functionName, $site, $begin, $end);
    $graph['site'] = $site;
    //$graph['year'] = null;
    $graph['type'] = 'value';//TODO pass this in
    $graph['group'] = $groupName;
    $graph['data'] = $data;

    return $graph;
}

function callDispatcher($functionName, $site, $startDate, $endDate, $timeUnit, $comparison, $options = [])
{
    $groupName = "";
    $graphs = [];
    $titleSuffix= '';

    if($comparison > -1) {
        $year = substr($startDate, 0, 4);
        $titleSuffix = " - $year";
    }
    switch($functionName){
        case "getIncome":
            $graphs["getIncome"] = graphMaker("getIncome", "All Income", $site, $startDate, $endDate, $groupName, getIncome($site, $startDate, $endDate, $timeUnit, $options));
            //TODO: add roster projections
            break;
        case "getIncomeABC":
            $groupName = $functionName."_".$site."_".$startDate;//give it a name unique to the group
            $graphs["getAIncome"] = graphMaker("getAIncome", "Onsite Income", $site, $startDate, $endDate, $groupName, getAIncome($site, $startDate, $endDate, $timeUnit, $options));
            $graphs["getBIncome"] = graphMaker("getBIncome", "Merchandise Income", $site, $startDate, $endDate, $groupName, getBIncome($site, $startDate, $endDate, $timeUnit, $options));
            $graphs["getCIncome"] = graphMaker("getCIncome", "Offsite Income", $site, $startDate, $endDate, $groupName, getCIncome($site, $startDate, $endDate, $timeUnit, $options));
            //TODO: add roster projections
            break;
        case "getCancellations":
            $graphs["getCancellations"] = graphMaker("getCancellations", 'Cancellations', $site, $startDate, $endDate, $groupName, getCancellations($site, $startDate, $endDate, $timeUnit, $options));
            break;
        case "getJumpProjections":
            $graphs["getJumpProjections"] = graphMaker("getJumpProjections","Jump Projections", $site, $startDate, $endDate, $groupName, getJumpProjections($site, $startDate, $endDate, $timeUnit, $options));
            break;
        case "getBookings":
            $graphs[$functionName] = graphMaker("getBookings", "Bookings", $site, $startDate, $endDate, $groupName, getBookings($site, $startDate, $endDate, $timeUnit, $options));
            break;
        case "getJumpsBooked":
            $graphs[$functionName] = graphMaker("getJumpsBooked", "Jumps Booked", $site, $startDate, $endDate, $groupName, getJumpsBooked($site, $startDate, $endDate, $timeUnit, $options));
            break;
        case "getJumpNumbers":
            $graphs[$functionName] = graphMaker("getJumpNumbers", "Jump Numbers", $site, $startDate, $endDate, $groupName, getJumpNumbers($site, $startDate, $endDate, $timeUnit, $options));
            $graphs["getJumpProjections"] = graphMaker("getJumpProjections","Jump Projections", $site, $startDate, $endDate, $groupName, getJumpProjections($site, $startDate, $endDate, $timeUnit, $options));
            //TODO: what if I want to show jump numbers and projections on the same graph
            break;
        case "getSecondJumps":
            $graphs[$functionName] = graphMaker("getSecondJumps", "Second Jumps", $site, $startDate, $endDate, $groupName, getSecondJumps($site, $startDate, $endDate, $timeUnit, $options));
            break;
        case "getWalkIn":
            $graphs[$functionName] = graphMaker("getWalkIn", "Walk-Ins", $site, $startDate, $endDate, $groupName, getWalkIns($site, $startDate, $endDate, $timeUnit, $options));
            break;
        case "getNonJumpers":
            $graphs[$functionName] = graphMaker("getNonJumpers", "Non-Jumpers", $site, $startDate, $endDate, $groupName, getNonJumpers($site, $startDate, $endDate, $timeUnit, $options));
            break;
    }

    foreach($graphs as $graphIndex => &$graph)
    {
        $graph['title'] = $graph['title'].$titleSuffix;
    }

    return $graphs;
}

//reindex from zero so that the json is an array not an object. Arrays in json must be 0, 1, 2, 3 or they will be objects
//which causes array.map to fail in the javascript
//$dataSetsBySite['sites'] = array_values($dataSetsBySite['sites']);
$graphs = array_values($graphs);
$allData = [
    'meta' => [
        'yAxisLabel' => $yAxisLabel,
    ],
    'sites' => $graphs
];

Header("Content-type: application/json; encoding: utf-8");
$jsonOutput = json_encode($allData);
echo $jsonOutput;
