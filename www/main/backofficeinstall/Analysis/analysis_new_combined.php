<?php
include '../includes/application_top.php';
$current_year = date("Y");
$total = isset($_GET['total']) ? $_GET['total'] : null;

if (isset($_GET['year'])) {
    $current_year = (int)$_GET['year'];
}
if($total == -1)
    $site_name = "All Sites";

$profitAndLoss = new profitAndLossCollection([0, 1, 2, 3, 4, 10, 12], $current_year, $total);

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>P&L Analysis <?php echo ucfirst($site_name) . ' ' . $current_year; ?></title>
    <?php include "../includes/head_scripts.php"; ?>
    <!--<link rel="stylesheet" type="text/css" href="css/analysis_php.css" media="all"/>-->
    <link rel="stylesheet" type="text/css" href="/css/bootstrap/3.3.6/css/bootstrap.css"/>
    <script src="js/foldtable.js"></script>
    <link rel="stylesheet" type="text/css" href="css/analysis_new_combined_php.css"/>
    <script>
        //script to bind hiding and showing to classes
        $(document).ready(function () {
            $("#back").click(function () {
                document.location = '/Analysis/?action=analysis';
            });
            $("#home").click(function () {
                document.location = '/';
            });
        });
    </script>
</head>
<body>
<div id="container">
    <h1><img src="images/company_logo.png" width="104" height="72">Combined Profit &amp; Loss For All Sites</h1>
    <?=$profitAndLoss->draw();?>
    <button id="back">Back</button>
    <button id="home">Home</button>
</div>
</body>
</html>
