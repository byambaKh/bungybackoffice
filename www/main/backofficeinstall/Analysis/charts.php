<?
require_once('../includes/application_top.php');
?>

<!DOCTYPE html>
<html>
<head>
    <title></title>
    <meta charset="UTF-8">
    <?php
    echo Html::css("bootstrap/3.3.2/css/bootstrap.min.css");
    ?>
    <link href="charts.css" rel="stylesheet" type="text/css">
    <link href="c3/c3.css" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="../js/bookings/jqueryui/1.8.10/themes/base/jquery-ui.css" type="text/css" media="all"/>
    <link rel="stylesheet" href="../js/bookings/css/demo-docs-theme/ui.theme.css" type="text/css" media="all"/>

    <script src="tinycolor/tinycolor.js" charset="utf-8"></script>
    <script src="d3/d3.min.js" charset="utf-8"></script>
    <script src="c3/c3.min.js"></script>
    <script src="../js/bookings/jquery/1.9.1/jquery.min.js" type="text/javascript"></script>
    <script src="../js/bookings/jqueryui/1.10.1/jquery-ui.min.js" type="text/javascript"></script>
    <script src="charts.js"></script>

</head>
<body>
<h1>Data Visualisation - Bungy Japan</h1>

<!--<h2 id="chart-name">Graph Name</h2>-->

<div class="chart" id="chart"></div>

<!--<button id="controller-hider" type="button">Hide Graph Settings</button>-->
<div id="controller">
    <div class="controller-section-header">Sites:</div>
    <label class="   "><input type="radio" class="site-input" name="sites" value="0" checked/>All Sites</label>
    <label class="   "><input type="radio" class="site-input" name="sites" value="1"/>Minakami</label>
    <label class="   "><input type="radio" class="site-input" name="sites" value="2"/>Sarugakyo</label>
    <label class="   "><input type="radio" class="site-input" name="sites" value="3"/>Ryujin</label>
    <label class="   "><input type="radio" class="site-input" name="sites" value="4"/>Itsuki</label>
    <label class="   "><input type="radio" class="site-input" name="sites" value="10"/>Nara</label>
    <!--
    <label class="off"><input type="radio" class="site-input" name="sites" disabled value="5"/>New 1</label>
    <label class="off"><input type="radio" class="site-input" name="sites" disabled value="6"/>New 2</label>
    -->

    <br>

    <div class="controller-section-header">Duration:</div>
    <label class=""><input type="radio" class="duration-input" name="duration" value="day"/>Day</label>
    <label class=""><input type="radio" class="duration-input" name="duration" value="week" />Week</label>
    <label class=""><input type="radio" class="duration-input" name="duration" value="month"/>Month</label>
    <label class=""><input type="radio" class="duration-input" name="duration" value="year" checked/>Year</label>
    <label class=""><input type="radio" class="duration-input" name="duration" value="custom"/>Custom</label>

    <div class="date-duration-input">Start Date:</div>
    <input type="text" class="date-duration-input" id="date-start-input" value="<?= date("Y-01-01"); ?>"/>

    <div class="custom-duration">End Date:</div>
    <input type="text" class="custom-duration" id="date-end-input" value="<?= date("Y-01-01"); ?>"/>

    <br>

    <!-- TODO: enable roster projections -->
    <div class="controller-section-header">Comparison:</div>
    <select class="comparison-select">
        <option value="-1" selected>None</option>
        <option value="0">All</option>
        <option value="1">1 Year</option>
        <option value="2">2 Years</option>
        <option value="3">3 Years</option>
        <option value="4">4 Years</option>
    </select>

    <div class="controller-section-header">Time Unit:</div>
    <select class="time-unit-select">
        <option value="day">Daily</option>
        <option value="week" selected>Weekly</option>
        <option value="month">Monthly</option>
        <option value="year">Yearly</option>
    </select>

    <div class="controller-section-header">Special Charts:</div>
    <select class="special-charts-select" disabled>
        <option value="average-group-size">Average Group Size</option>
        <option value="sex">Sex</option>
        <option value="age">Age</option>
        <option value="prefecture">From Prefecture</option>
        <option value="rainy-days">Rainy Days</option>
        <option value="operational-days">Operational Days</option>
        <option value="traffic-problems">Traffic Problems</option>
    </select>

    <div class="controller-section-header">Metrics:</div>
    <!--When this is clicked, add the value to an array of Metrics-->
    <label class="   "><input type="radio" class="getIncome" name="metrics" value="" checked/>Income Total</label>
    <label class="   "><input type="radio" class="getIncomeABC" name="metrics" value="" >Income as Onsite, Merchandise, Offsite</label>
    <!--<label class="   "><input type="radio" class="getProjections" name="metrics" value=""/>Projections</label>-->
    <label class="   "><input type="radio" class="getBookings" name="metrics" value=""/>Daily Bookings Received</label>
    <label class="   "><input type="radio" class="getJumpsBooked" name="metrics" value=""/>Total Jumps Received</label>
    <label class="   "><input type="radio" class="getJumpNumbers" name="metrics" value=""/>Actual Jump Numbers &amp; Projections</label>
    <label class="   "><input type="radio" class="getCancellations" name="metrics" value=""/>Cancellations</label>
    <label class="   "><input type="radio" class="getSecondJumps" name="metrics" value=""/>Second Jumpers</label>
    <label class="off"><input type="radio" class="getWalkIn" name="metrics" value=""/>Walk-Ins's</label>
    <label class="   "><input type="radio" class="getNonJumpers" name="metrics" value=""/>Non-Jumpers</label>

    <br>

    <div class="controller-section-header">Chart Type:</div>
    <label class=""><input type="radio" class="chart-type-input" name="chart-type" checked value="area-spline"/>Area Line (smoothed)</label>
    <label class=""><input type="radio" class="chart-type-input" name="chart-type" value="area"/>Area Line</label>
    <label class=""><input type="radio" class="chart-type-input" name="chart-type" value="bar"/>Bar</label>
    <label class=""><input type="radio" class="chart-type-input" name="chart-type" value="line"/>Line</label>
    <label class=""><input type="radio" class="chart-type-input" name="chart-type" value="spline"/>Line (smoothed)</label>
    <!--<label class="   "><input type="radio" class="chart-type-input" name="chart-type" disabled value="radar"/>Radar</label>
    <label class="   "><input type="radio" class="chart-type-input" name="chart-type" disabled value="donut"/>Donut</label>
	<label class="   "><input type="radio" class="chart-type-input" name="chart-type" disabled value="column"/>Column</label>-->

    <br>

    <button id="generate-graph" name="generate-graph" type="button" value="generate-graph">Visualize Data</button>
    <br>
</div>
<!--
<table>
    <tr>
        <td>Site:</td>
        <td>Total Income:</td>
    </tr>
    <tr>
        <td>Minakami</td>
        <td>XYZ</td>
    </tr>
    <tr>
        <td>Sarugakyo</td>
        <td>XYZ</td>
    </tr>
    <tr>
        <td>Ryujin</td>
        <td>XYZ</td>
    </tr>
    <tr>
        <td>Itsuki</td>
        <td>XYZ</td>
    </tr>
</table>
-->
</body>
</html>
