$(document).ready(function(){
	$('tr.foldable > th.first-col > span.expander').bind('click', function(){
		var wholeTable = $(this).parent().parent().parent().parent();
		var tableHeader = $(this).parent().parent().parent();
		var tableBody   = wholeTable.children('tbody');
		var tableFooter = wholeTable.children('tfoot');
		var expander = tableHeader.find('.expander');
		var headerTotals = tableHeader.find('.header-totals');

		if(tableBody.is(':visible')) {
			headerTotals.fadeIn();
			tableBody.fadeOut();
			tableFooter.fadeOut();
			expander.html('[+]');

		} else {
			headerTotals.fadeOut();
			tableBody.fadeIn();
			tableFooter.fadeIn();
			expander.html('[-]');

		}
	})
});
