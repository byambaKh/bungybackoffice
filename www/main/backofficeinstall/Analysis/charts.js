//TODO I need to make the form represent the state stored here dynamically
var dataRequest = {
    "sites": 1,
    "startDate": null,
    "endDate": null,
    "timeUnit": 'week',
    "duration": "year",
    "comparison": -1,
    //"chartType": "line",
    "metrics": {
        "getIncome": true,
        "getIncomeABC": false,
        "getBookings": false,
        "getJumpsBooked": false,
        "getJumpNumbers": false,
        "getCancellations": false,
        "getSecondJumps": false,
        "getWalkIn": false,
        "getNonJumpers": false,
    }
};

var chart;//the c3 chart


var graphConfiguration = {
    bindto: '#chart',
    data: {
        x: 'x',
        columns: [],
        colors: {},
        type: 'area-spline',
        types: {},
        axes: {},
        groups:[],
        names: {},
    },
    point: {
        r: 1.5,
    },
    legend: {
        position: 'bottom'
    },
    axis: {
        x: {
            type: 'timeseries',
            tick: {
                format: '%Y-%m-%d'
            },
            label: { // ADD
                text: 'Date',
                position: 'outer-middle'
            }
        },
        y: {
            label: { // ADD
                text: '',
                position: 'outer-middle',
            },
        },

        /*
         y2:{
         show:false,
         label: { // ADD
         text: 'Percentage Change',
         position: 'outer-middle'
         },
         }
         */
    },

    grid: {
        /*Add a zero line to the grid*/
        y: {
            lines: [{value: 0}]
        }
    },
    names: {}
};

function extractXValuesFromDataSet(dataSet) {
    var axisData = [];
    dataSet.data.map(function (xYPair) {
        axisData.push(xYPair.x);
    });
    return axisData;
}

function extractYValuesFromDataSet(dataSet) {
    var axisData = [];
    dataSet.data.map(function (xYPair) {
        axisData.push(xYPair.y);
    });
    return axisData;
}

function colorForGraph(site, colorIndex) {
    var colorsArray = ['#BC0000', '#BD9C00', '#00BA01', '#0009B4', '#9109B4'];
    var baseColor = tinycolor(colorsArray[site - 1]);

    return baseColor.lighten(colorIndex * 12).toString();
}

function createGraph(graphObject) {
    //get all of the possible dates
    var graphData = graphObject.sites;
    var graphMeta = graphObject.meta;
    var graphGroups = {};

    graphConfiguration.data.columns = [];//reset the data so that old graphs are deleted
    graphConfiguration.data.groups  = [];

    var year = Object.keys(graphData[0])[0];
    var firstGraphType = Object.keys(graphData[0][year]['graphs'])[0];
    var firstDataSet = graphData[0][year]['graphs'][firstGraphType];//['graphs']['getAIncome'];

    var xAxis = ['x'].concat(extractXValuesFromDataSet(firstDataSet));
    graphConfiguration.data.columns.push(xAxis);

    graphData.map(function (site) {
        var colorIndex = 0;

        for(var graphYear in site) {
            if (site.hasOwnProperty(graphYear)) {//check that we are not iterating over prototype properties
                for(var graphProperty in site[graphYear]['graphs']) {
                    if (site[graphYear]['graphs'].hasOwnProperty(graphProperty)) {

                        var graph = site[graphYear]['graphs'][graphProperty];
                        //set axes
                        //set set title
                        graphConfiguration.data.axes[graph.name] = 'y';
                        graphConfiguration.data.names[graph.name] = graph.title;
                        //set color
                        //push the y dataset
                        var yDataSet = [graph.name].concat(extractYValuesFromDataSet(graph));
                        graphConfiguration.data.columns.push(yDataSet);
                        if(graph.group !== '') {
                            if (graphGroups.hasOwnProperty(graph.group)) { //if the graph group already exists
                                graphGroups[graph.group].push(graph.name); //push the new group on to the array
                            } else {
                                graphGroups[graph.group] = [graph.name];
                            }
                        }
                        //set the group
                    }
                }

                graphProperty = yDataSet = graph = null;//null these out just to be safe
                //TODO: this is copy and paste code, should refactor
                for(var graphProperty in site[graphYear]['comparisons']) {
                    if (site[graphYear]['comparisons'].hasOwnProperty(graphProperty)) {

                        var graph = site[graphYear]['comparisons'][graphProperty];
                        //set axes
                        //set set title
                        graphConfiguration.data.axes[graph.name] = 'y';
                        //set color
                        //push the y dataset
                        var yDataSet = [graph.name].concat(extractYValuesFromDataSet(graph));
                        graphConfiguration.data.columns.push(yDataSet);
                        if(graph.group !== '') {
                            if (graphGroups.hasOwnProperty(graph.group)) { //if the graph group already exists
                                graphGroups[graph.group].push(graph.name); //push the new group on to the array
                            } else {
                                graphGroups[graph.group] = [graph.name];
                            }
                        }
                        //set the group
                    }
                }
            }
        }

        graphConfiguration.axis.y.label = graphMeta.yAxisLabel;
        graphConfiguration.data.type = $('.chart-type-input:checked').val();
        //push the graph groups on to the array of groups
        for(var graphGroup in graphGroups) {
            if(graphGroups.hasOwnProperty(graphGroup)) {
                graphConfiguration.data.groups.push(graphGroups[graphGroup]);
            }
        }
    });

    chart = c3.generate(graphConfiguration);

}


function getGraphDataAsJSON(options, site) {
    //modify the dom to have new graphs
    dataRequest.startDate = $('#date-start-input').val();
    dataRequest.endDate = $('#date-end-input').val();
    dataRequest.chartType = $(".chart-type-input").val();
    dataRequest.sites = $(".site-input:checked").val();
    dataRequest.timeUnit = $(".time-unit-select").val();
    dataRequest.comparison = $(".comparison-select").val();

    dataRequest.metrics['getIncome'] = $(".getIncome").prop("checked");
    dataRequest.metrics['getIncomeABC'] = $(".getIncomeABC").prop("checked");
    dataRequest.metrics['getProjections'] = $(".getProjections").prop("checked");
    dataRequest.metrics['getBookings'] = $(".getBookings").prop("checked");
    dataRequest.metrics['getJumpsBooked'] = $(".getJumpsBooked").prop("checked");
    dataRequest.metrics['getJumpNumbers'] = $(".getJumpNumbers").prop("checked");
    dataRequest.metrics['getCancellations'] = $(".getCancellations").prop("checked");
    dataRequest.metrics['getSecondJumps'] = $(".getSecondJumps").prop("checked");
    dataRequest.metrics['getWalkIn'] = $(".getWalkIn").prop("checked");
    dataRequest.metrics['getNonJumpers'] = $(".getNonJumpers").prop("checked");

    $.ajax(
        'ajax.php',
        {
            data: dataRequest,
            dataType: 'json',
            type: 'GET',
            async: true,
            cache: false,
            contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
            success: function (data, textStatus, jqXHR) {
                //process the data from the server
                console.log('success');
                createGraph(data);
                return true;
            },
            error: function (jqXHR, textStatus, errorThrown) {
                console.log("error: " + jqXHR.responseText);
            },
            complete: function () {
                console.log("complete");
            }
        }
    );
}


$(document).ready(function () {
    //dataRequest.startDate = new Date().toUTCString();//we need to set this manually
    //console.log(dataRequest.startDate);
    getGraphDataAsJSON({}, 1);

    //$("#chart-name").innerText
    $('#date-start-input').datepicker({
        numberOfMonths: 1,
        dateFormat: 'yy-mm-dd',
        altField: '#alternate',
        altFormat: 'yy-mm-dd',
        minDate: new Date(2013, 1, 1),
        //showButtonPanel: true,
        //dateFormat: 'dd/mm/yy',
        //altFormat: 'dd/mm/yy',
        //maxDate: new Date(<?php echo $maxDate;?>),
    });

    $('#date-end-input').datepicker({
        numberOfMonths: 1,
        dateFormat: 'yy-mm-dd',
        altField: '#alternate',
        altFormat: 'yy-mm-dd',
        minDate: new Date(2013, 1, 1),
        //showButtonPanel: true,
        //dateFormat: 'dd/mm/yy',
        //altFormat: 'dd/mm/yy',
        //maxDate: new Date(<?php echo $maxDate;?>),
    });

    $("#generate-graph").click(function (e) {
        e.preventDefault();
        getGraphDataAsJSON({}, 1);
    });

    var animationDuration = 400;
    $(".duration-input").click(function (e) {
        dataRequest.duration = $(this).val();
        if ($(this).val() == 'custom') {
            $('.custom-duration').slideDown(animationDuration);
        } else {
            $('.custom-duration').slideUp(animationDuration);
        }
    });

    $("#controller-hider").click(function () {
        var controller = $("#controller");

        if (controller.is(":visible")) {
            $(this).html("Show Graph Settings");
            //controller.fadeOut();
            controller.slideUp();
        } else {
            $(this).html("Hide Graph Settings");
            //controller.fadeIn();
            controller.slideDown();
        }
    });

    $(".chart-type-input").click(function () {
        //graphConfiguration.data.type = $('#chart-type-input:checked').val();
        chart.transform($('.chart-type-input:checked').val());
    });

    //toggle the value of the metrics
    /*

     $(".metrics-input").click(function (e) {
     for(var metric in dataRequest.metrics){
     dataRequest.metrics[metric] = false;
     }

     var metricFunctionName = $(this).val();
     graphName = yAxisLabels[metricFunctionName];

     dataRequest.metrics[metricFunctionName] = !dataRequest.metrics[metricFunctionName];
     });

     $(".site-input").click(function (e) {
     dataRequest.sites = $(this).val();
     });

     $(".chart-type-input").click(function (e) {
     dataRequest.chartType = $(this).val();
     });

     $(".time-unit-select").change(function (e) {
     dataRequest.timeUnit = $(this).val();
     });

     $(".comparison-input").click(function (e) {
     dataRequest.comparison = $(this).val();
     });
     */

    /*
     $("input").change(function (e) {
     dataRequest.date = new Date();//we need to set this manually
     getGraphDataAsJSON({}, 1);
     });
     */

    /*
     $(".special-charts-select").click(function (e) {
     dataRequest.sites = $(this).val();
     });
     */

});

