<?php
	include '../includes/application_top.php';
	
	$site_id = 0;
	$sql = "SELECT * FROM sites where subdomain = '{$_GET['subdomain']}';";
	$res = mysql_query($sql) or die(mysql_error());
	if ($row = mysql_fetch_assoc($res)) {
		$site_name = $row['subdomain'];
		$site_id = $row['id'];
	};


    $sql = "select * from configuration where site_id = $site_id;";
    $res = mysql_query($sql) or die(mysql_error());
    while ($row = mysql_fetch_assoc($res)) {
        $config[$row['key']] = $row['value'];
    };

	$headers = array("INCOME");
	for ($i = 1; $i < 13; $i++) {
		$headers[] = strtoupper(date("M", mktime(0, 0, 0, $i, 15, 2013)));
	};
	$headers[] = 'TOTALS';

	$current_year = date("Y");

	if (isset($_GET['year'])) {
		$current_year = (int)$_GET['year'];
	};
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>P&L Analysis <?php echo ucfirst($site_name) . ' ' . $current_year; ?></title>
<?php include "../includes/head_scripts.php"; ?>
<style>
html * {
	font-family: Arial;
}
#container {
	margin-left: 0px;
	margin-right: 0px;
}
#main-table {
	border-collapse: collapse;
	margin: 0px;
}
.main-header {
	border: 2px solid black;
	background-color: black;
	color: white;
}
.main-header h1 {
	color: white;
	width: 100%;
	text-align: center;
	margin-top: 10px;
	margin-bottom: 10px;
}
.month-headers, .month-headers th {
	border: 1px solid black;
	background-color: #9FC234;
	font-size: 12px;
}
.expenses-month-headers th {
    border: 1px solid black;
    background-color: red;
    font-size: 12px;
}
.column-headers th {
	background-color: white;
	font-size: 10px;
}
.pax {
	border: 1px solid black;
	min-width: 40px;
	width: 40px;
}
.amount {
	border: 1px solid black;
	width: 60px;
	min-width: 60px;
}
.first-col {
	width: 130px;
	min-width: 120px;
	font-size: 11px;
	white-space: nowrap;
}
.tboard {
	text-align: right !important;
}
td.pax, td.amount {
	font-size: 11px;
	text-align: right;
	border-top: none !important;
	border-bottom: none !important;
}
.subtotal th {
	padding-top: 3px;
	padding-bottom: 3px;
	font-size: 12px;
	text-align : right;
	border-top: 2px solid black !important;
	border-bottom: 2px solid black !important;
	font-weight: normal;
}
.subtotal th.subtotal-header {
	font-size: 11px;
	color: blue;
	font-weight: normal;
}
.bungy-total th {
	padding-top: 5px;
	padding-bottom: 5px;
	font-size: 12px;
	text-align : right;
	border-top: 3px solid black !important;
	border-bottom: 3px solid black !important;
	font-weight: bold;
}
.bungy-total th.subtotal-header {
	font-size: 12px;
	color: black;
	font-weight: bold;
}
.income-total th {
	padding-top: 5px;
	padding-bottom: 5px;
	font-size: 12px;
	text-align : right;
	border-top: 3px solid black !important;
	border-bottom: 3px solid black !important;
	font-weight: bold;
	background-color: green;
}
.income-total th.subtotal-header {
	font-size: 12px;
	color: black;
	font-weight: bold;
}
.income-total-tboard th {
	padding-top: 5px;
	padding-bottom: 5px;
	font-size: 12px;
	text-align : right;
	border-top: 3px solid black !important;
	border-bottom: 3px solid black !important;
	font-weight: bold;
}
.income-total-tboard th.subtotal-header {
	font-size: 12px;
	color: black;
	font-weight: bold;
	white-space: nowrap;
}
.pink, .pink td, .pink th {
	background-color: pink;
}
.staff-salary-total th {
	padding-top: 5px;
	padding-bottom: 5px;
	font-size: 12px;
	text-align : right;
	border-top: 1px solid black !important;
	border-bottom: 1px solid black !important;
	font-weight: bold;
}
.staff-salary-total th.subtotal-header {
	font-size: 12px;
	color: black;
	font-weight: bold;
	text-align: right;
}
.expenses-sub-total th {
	padding-top: 5px;
	padding-bottom: 5px;
	font-size: 12px;
	text-align : right;
	border-top: 1px solid black !important;
	border-bottom: 3px double black !important;
	font-weight: bold;
}
.expenses-sub-total th.subtotal-header {
	font-size: 12px;
	color: black;
	font-weight: bold;
	text-align: right;
}
.expenses-total th {
	padding-top: 5px;
	padding-bottom: 5px;
	font-size: 12px;
	text-align : right;
	border-top: 3px solid black !important;
	border-bottom: 1px solid black !important;
	font-weight: bold;
}
.expenses-total th.subtotal-header {
	font-size: 12px;
	color: black;
	font-weight: bold;
	text-align: right;
}
.profit-total th {
	padding-top: 5px;
	padding-bottom: 5px;
	font-size: 12px;
	text-align : right;
	border-top: 3px solid black !important;
	border-bottom: 1px solid black !important;
	font-weight: bold;
	background-color: black;
	color: white;
}
.profit-total th.subtotal-header {
	font-size: 12px;
	color: black;
	font-weight: bold;
	text-align: left;
	color: white;
}
.floatleft {
	float: left;
}
.floatright {
	float: right;
}
#home {
	margin-left: 30px;
}
</style>
<script>
	$(document).ready(function () {
		$("#back").click(function () {
			document.location = '/Analysis/?action=analysis';
		});
		$("#home").click(function () {
			document.location = '/';
		});
	});
</script>
</head>
<body>
	<div id="container">
	<table id="main-table">
	<tr>
		<th class="main-header" colspan="27"><h1>P&L Analysis <?php echo ucfirst($site_name) . ' ' . $current_year; ?></h1></th>
	</tr>
	<tr>
		<td class="divider" colspan="27">
			<a href="analysis.php?year=<?php echo $current_year - 1; echo ($_GET['subdomain'] != '') ? '&subdomain=' . $_GET['subdomain'] : ''; ?>" class="floatleft">&lt;&lt;&lt; <?php echo $current_year - 1; ?></a>
	<?php if ($current_year < date("Y")) { ?>
			<a href="analysis.php?year=<?php echo $current_year + 1; echo ($_GET['subdomain'] != '') ? '&subdomain=' . $_GET['subdomain'] : ''; ?>" class="floatright"><?php echo $current_year + 1; ?> &gt;&gt;&gt; </a>
	<?php }; ?>
		</td>
	</tr>
	<tr class="month-headers">
<?php 
	foreach ($headers as $num => $header) {
		$colspan = '';
		if ($num > 0) $colspan = ' colspan="2"';
		echo "<th$colspan>$header</th>\n";
	};
?>
	</tr>
	<tr class='column-headers'>
<?php 
	foreach ($headers as $num => $header) {
		if ($num > 0) {
			echo "<th class='pax'>pax</th>\n";
			echo "<th class='amount'>amount</th>\n";
		} else {
			echo "<th class='first-col'>&nbsp;</th>\n";
		};
	};
?>
	</tr>
<?php
	$sql = "SELECT DISTINCT Rate 
		FROM customerregs1 
		WHERE 
			" . (($site_id > 0) ? " site_id = '".$site_id."' AND " : "site_id > 0 AND ") . "
			DeleteStatus = 0
			AND Checked = 1
			AND BookingDate like '$current_year-%' 
			AND NoOfJump > 0
			AND CollectPay = 'Onsite'
		ORDER BY Rate DESC;";
	$res = mysql_query($sql) or die(mysql_error());
	$subtotals = array();
	while ($row = mysql_fetch_assoc($res)) {
		// process each online Rate
		$rate = $row['Rate'];
		if ($rate == 0) continue;
		echo "<tr>";
		echo "<td class='first-col'>Bungy Onsite - $rate</td>\n";
		$sql = "SELECT 
				DATE_FORMAT(BookingDate, '%m') as yearmonth,
				sum(NoOfJump) as pax, 
				sum(NoOfJump * Rate) as amount
			FROM customerregs1 
			WHERE 
				" . (($site_id > 0) ? " site_id = '".$site_id."' AND " : "site_id > 0 AND ") . "
				DeleteStatus = 0
				AND Checked = 1
				AND Rate = $rate
				AND BookingDate like '$current_year-%' 
				AND NoOfJump > 0
				AND CollectPay = 'Onsite'
			GROUP BY yearmonth
			ORDER BY yearmonth ASC;
		";
		$res2 = mysql_query($sql) or die(mysql_error());
		$data = array();
		while ($row2 = mysql_fetch_assoc($res2)) {
			$data[$row2['yearmonth']] = $row2;
		};
		if ($rate == $config['second_jump_rate']) {
			//add second jumps data
			$sql = "SELECT 
					DATE_FORMAT(BookingDate, '%m') as yearmonth,
					sum(2ndj / {$config['second_jump_rate']}) as pax, 
					sum(2ndj) as amount
				FROM customerregs1 
				WHERE 
					" . (($site_id > 0) ? " site_id = '".$site_id."' AND " : "site_id > 0 AND ") . "
					DeleteStatus = 0
					AND Checked = 1
					AND BookingDate like '$current_year-%' 
				/*	AND CollectPay = 'Onsite'*/
				GROUP BY yearmonth
				ORDER BY yearmonth ASC;
			";
			$res2 = mysql_query($sql) or die(mysql_error());
			while ($row2 = mysql_fetch_assoc($res2)) {
				if (!array_key_exists($row2['yearmonth'], $data)) {
					$data[$row2['yearmonth']] = array(
						'pax'		=> 0,
						'amount'	=> 0
					);
				};
				$data[$row2['yearmonth']]['pax'] += $row2['pax'];
				$data[$row2['yearmonth']]['amount'] += $row2['amount'];
			};
			// add second jumps from merchandise
			$sql = "SELECT  DATE_FORMAT(sale_time, '%m') as yearmonth,
        			sum(sale_total_2nd_jump) as amount,
        			sum(sale_total_qty_2nd_jump) as pax
				FROM merchandise_sales
				WHERE 
					" . (($site_id > 0) ? " site_id = '".$site_id."' AND " : "site_id > 0 AND ") . "
					sale_time like '$current_year-%'
				GROUP BY yearmonth 
				ORDER BY yearmonth ASC;
			";
			$res2 = mysql_query($sql) or die(mysql_error());
			while ($row2 = mysql_fetch_assoc($res2)) {
				if (!array_key_exists($row2['yearmonth'], $data)) {
					$data[$row2['yearmonth']] = array(
						'pax'		=> 0,
						'amount'	=> 0
					);
				};
				$data[$row2['yearmonth']]['pax'] += $row2['pax'];
				$data[$row2['yearmonth']]['amount'] += $row2['amount'];
			};
		};
		$totals = array (
			'pax'		=> 0,
			'amount'	=> 0
		);
		foreach ($headers as $num => $val) {
			if ($num > 0) {
				$key = sprintf("%02d", $num);
				$pax = array_key_exists($key, $data) ? $data[$key]['pax'] : '-';
				$amount = array_key_exists($key, $data) ? $data[$key]['amount'] : '-';
				if ($num == 13) {
					$pax = $totals['pax'];
					$amount = $totals['amount'];
				} else {
					if (array_key_exists($key, $data)) {
						$totals['pax'] += $data[$key]['pax'];
						$totals['amount'] += $data[$key]['amount'];
					};
				};
				if (!array_key_exists($key, $subtotals)) {
					$subtotals[$key] = array(
						'pax'		=> 0,
						'amount'	=> 0
					);
				};
				$subtotals[$key]['pax'] += array_key_exists($key, $data) ? $data[$key]['pax'] : 0;
				$subtotals[$key]['amount'] += array_key_exists($key, $data) ? $data[$key]['amount'] : 0;
				$subtotals[$key]['pax'] += ($num == 13) ? $totals['pax'] : 0;
				$subtotals[$key]['amount'] += ($num == 13) ? $totals['amount'] : 0;
				echo "<td class='pax'>$pax</td>\n";
				echo "<td class='amount'>$amount</td>\n";
			};
		};
		echo "</tr>";
	};
	// Cancellation Onsite
    $sql = "SELECT 
			DATE_FORMAT(BookingDate, '%m') as yearmonth, 
			SUM(CancelFeeQTY) as pax, 
			SUM(CancelFee * CancelFeeQTY) as amount 
		FROM customerregs1 
		WHERE 
			" . (($site_id > 0) ? " site_id = '".$site_id."' AND " : "site_id > 0 AND ") . "
			BookingDate like '$current_year-%' 
			and Checked = 1 
			and DeleteStatus = 0 
			and CancelFeeCollect = 'Onsite' 
			and CancelFee > 0
			and CancelFeeQTY > 0
		GROUP BY yearmonth 
		ORDER BY yearmonth ASC;";
    $res = mysql_query($sql) or (die(mysql_error()));
	$data = array();
    while ($row = mysql_fetch_assoc($res)) {
        $data[$row['yearmonth']] = $row;
    };
    mysql_free_result($res);
	$totals = array(
		'pax'		=> 0,
		'amount'	=> 0
	);
	echo "<tr>";
	echo "<td class='first-col'>Bungy Onsite Cancellation</td>";
	foreach ($headers as $num => $val) {
		if ($num > 0) {
			$key = sprintf("%02d", $num);
			$pax = array_key_exists($key, $data) ? $data[$key]['pax'] : '-';
			$amount = array_key_exists($key, $data) ? $data[$key]['amount'] : '-';
			if ($num == 13) {
				$pax = $totals['pax'];
				$amount = $totals['amount'];
			} else {
				if (array_key_exists($key, $data)) {
					$totals['pax'] += $data[$key]['pax'];
					$totals['amount'] += $data[$key]['amount'];
				};
			};
			$subtotals[$key]['pax'] += array_key_exists($key, $data) ? $data[$key]['pax'] : 0;
			$subtotals[$key]['amount'] += array_key_exists($key, $data) ? $data[$key]['amount'] : 0;
			$subtotals[$key]['pax'] += ($num == 13) ? $totals['pax'] : 0;
			$subtotals[$key]['amount'] += ($num == 13) ? $totals['amount'] : 0;
			echo "<td class='pax'>$pax</td>\n";
			echo "<td class='amount'>$amount</td>\n";
		};
	};
	echo "</tr>";
	// ToPay Onsite
    $sql = "SELECT 
			DATE_FORMAT(BookingDate, '%m') as yearmonth, 
			SUM(RateToPayQTY) as pax, 
			SUM(RateToPay * RateToPayQTY) as amount 
		FROM customerregs1 
		WHERE 
			" . (($site_id > 0) ? " site_id = '".$site_id."' AND " : "site_id > 0 AND ") . "
			BookingDate like '$current_year-%' 
			and Checked = 1 
			and DeleteStatus = 0 
			and CollectPay = 'Onsite' 
			and RateToPay > 0
			and RateToPayQTY > 0
		GROUP BY yearmonth 
		ORDER BY yearmonth ASC;";
    $res = mysql_query($sql) or (die(mysql_error()));
	$data = array();
    while ($row = mysql_fetch_assoc($res)) {
        $data[$row['yearmonth']] = $row;
    };
    mysql_free_result($res);
	$totals = array(
		'pax'		=> 0,
		'amount'	=> 0
	);
	echo "<tr>";
	echo "<td class='first-col'>Bungy Onsite - ToPay</td>";
	foreach ($headers as $num => $val) {
		if ($num > 0) {
			$key = sprintf("%02d", $num);
			$pax = array_key_exists($key, $data) ? $data[$key]['pax'] : '-';
			$amount = array_key_exists($key, $data) ? $data[$key]['amount'] : '-';
			if ($num == 13) {
				$pax = $totals['pax'];
				$amount = $totals['amount'];
			} else {
				if (array_key_exists($key, $data)) {
					$totals['pax'] += $data[$key]['pax'];
					$totals['amount'] += $data[$key]['amount'];
				};
			};
			$subtotals[$key]['pax'] += array_key_exists($key, $data) ? $data[$key]['pax'] : 0;
			$subtotals[$key]['amount'] += array_key_exists($key, $data) ? $data[$key]['amount'] : 0;
			$subtotals[$key]['pax'] += ($num == 13) ? $totals['pax'] : 0;
			$subtotals[$key]['amount'] += ($num == 13) ? $totals['amount'] : 0;
			echo "<td class='pax'>$pax</td>\n";
			echo "<td class='amount'>$amount</td>\n";
		};
	};
	echo "</tr>";
	// OnSite subtotal
	echo "<tr class='subtotal'>";
	echo "<th class='subtotal-header'>ON SITE SUBTOTAL</th>";
	foreach ($headers as $num => $val) {
		if ($num > 0) {
			$key = sprintf("%02d", $num);
			$pax = $subtotals[$key]['pax'];
			$amount = $subtotals[$key]['amount'];
			echo "<th class='pax'>$pax</th>\n";
			echo "<th class='amount'>$amount</th>\n";
		};	
	};
	echo "</tr>";
	$onsite_subtotals = $subtotals;
// Offsite Bungy
	$sql = "SELECT DISTINCT Rate 
		FROM customerregs1 
		WHERE 
			" . (($site_id > 0) ? " site_id = '".$site_id."' AND " : "site_id > 0 AND ") . "
			DeleteStatus = 0
			AND Checked = 1
			AND BookingDate like '$current_year-%' 
			AND NoOfJump > 0
			AND CollectPay = 'Offsite'
		ORDER BY Rate DESC;";
	$res = mysql_query($sql) or die(mysql_error());
	$subtotals = array();
	while ($row = mysql_fetch_assoc($res)) {
		// process each online Rate
		$rate = $row['Rate'];
		if ($rate == 0) continue;
		echo "<tr>";
		echo "<td class='first-col'>Bungy Offsite - $rate</td>\n";
		$sql = "SELECT 
				DATE_FORMAT(BookingDate, '%m') as yearmonth,
				sum(NoOfJump) as pax, 
				sum(NoOfJump * Rate) as amount
			FROM customerregs1 
			WHERE 
				" . (($site_id > 0) ? " site_id = '".$site_id."' AND " : "site_id > 0 AND ") . "
				DeleteStatus = 0
				AND Checked = 1
				AND Rate = $rate
				AND BookingDate like '$current_year-%' 
				AND NoOfJump > 0
				AND CollectPay = 'Offsite'
			GROUP BY yearmonth
			ORDER BY yearmonth ASC;
		";
		$res2 = mysql_query($sql) or die(mysql_error());
		$data = array();
		while ($row2 = mysql_fetch_assoc($res2)) {
			$data[$row2['yearmonth']] = $row2;
		};
		$totals = array (
			'pax'		=> 0,
			'amount'	=> 0
		);
		foreach ($headers as $num => $val) {
			if ($num > 0) {
				$key = sprintf("%02d", $num);
				$pax = array_key_exists($key, $data) ? $data[$key]['pax'] : '-';
				$amount = array_key_exists($key, $data) ? $data[$key]['amount'] : '-';
				if ($num == 13) {
					$pax = $totals['pax'];
					$amount = $totals['amount'];
				} else {
					if (array_key_exists($key, $data)) {
						$totals['pax'] += $data[$key]['pax'];
						$totals['amount'] += $data[$key]['amount'];
					};
				};
				if (!array_key_exists($key, $subtotals)) {
					$subtotals[$key] = array(
						'pax'		=> 0,
						'amount'	=> 0
					);
				};
				$subtotals[$key]['pax'] += array_key_exists($key, $data) ? $data[$key]['pax'] : 0;
				$subtotals[$key]['amount'] += array_key_exists($key, $data) ? $data[$key]['amount'] : 0;
				$subtotals[$key]['pax'] += ($num == 13) ? $totals['pax'] : 0;
				$subtotals[$key]['amount'] += ($num == 13) ? $totals['amount'] : 0;
				echo "<td class='pax'>$pax</td>\n";
				echo "<td class='amount'>$amount</td>\n";
			};
		};
		echo "</tr>";
	};
	// Cancellation Offsite
    $sql = "SELECT 
			DATE_FORMAT(BookingDate, '%m') as yearmonth, 
			SUM(CancelFeeQTY) as pax, 
			SUM(CancelFee * CancelFeeQTY) as amount 
		FROM customerregs1 
		WHERE 
			" . (($site_id > 0) ? " site_id = '".$site_id."' AND " : "site_id > 0 AND ") . "
			BookingDate like '$current_year-%' 
			and Checked = 1 
			and DeleteStatus = 0 
			and CancelFeeCollect = 'Offsite' 
			and CancelFee > 0
			and CancelFeeQTY > 0
		GROUP BY yearmonth 
		ORDER BY yearmonth ASC;";
    $res = mysql_query($sql) or (die(mysql_error()));
	$data = array();
    while ($row = mysql_fetch_assoc($res)) {
        $data[$row['yearmonth']] = $row;
    };
    mysql_free_result($res);
	$totals = array(
		'pax'		=> 0,
		'amount'	=> 0
	);
	echo "<tr>";
	echo "<td class='first-col'>Bungy Offsite Cancellation</td>";
	foreach ($headers as $num => $val) {
		if ($num > 0) {
			$key = sprintf("%02d", $num);
			$pax = array_key_exists($key, $data) ? $data[$key]['pax'] : '-';
			$amount = array_key_exists($key, $data) ? $data[$key]['amount'] : '-';
			if ($num == 13) {
				$pax = $totals['pax'];
				$amount = $totals['amount'];
			} else {
				if (array_key_exists($key, $data)) {
					$totals['pax'] += $data[$key]['pax'];
					$totals['amount'] += $data[$key]['amount'];
				};
			};
			$subtotals[$key]['pax'] += array_key_exists($key, $data) ? $data[$key]['pax'] : 0;
			$subtotals[$key]['amount'] += array_key_exists($key, $data) ? $data[$key]['amount'] : 0;
			$subtotals[$key]['pax'] += ($num == 13) ? $totals['pax'] : 0;
			$subtotals[$key]['amount'] += ($num == 13) ? $totals['amount'] : 0;
			echo "<td class='pax'>$pax</td>\n";
			echo "<td class='amount'>$amount</td>\n";
		};
	};
	echo "</tr>";
	// Offsite subtotal
	echo "<tr class='subtotal'>";
	echo "<th class='subtotal-header'>OFF SITE SUBTOTAL</th>";
	foreach ($headers as $num => $val) {
		if ($num > 0) {
			$key = sprintf("%02d", $num);
			$pax = $subtotals[$key]['pax'];
			$amount = $subtotals[$key]['amount'];
			echo "<th class='pax'>$pax</th>\n";
			echo "<th class='amount'>$amount</th>\n";
		};	
	};
	echo "</tr>";
	// BUNGY TOTAL
	echo "<tr class='bungy-total'>";
	echo "<th class='subtotal-header'>BUNGY TOTAL</th>";
	$b_total = array();
	foreach ($headers as $num => $val) {
		if ($num > 0) {
			$key = sprintf("%02d", $num);
			$pax = $subtotals[$key]['pax'] + $onsite_subtotals[$key]['pax'];
			$amount = $subtotals[$key]['amount'] + $onsite_subtotals[$key]['amount'];
			$b_total[$key] = array(
				'pax'		=> $pax,
				'amount'	=> $amount
			);
			echo "<th class='pax'>$pax</th>\n";
			echo "<th class='amount'>$amount</th>\n";
		};	
	};
	echo "</tr>";
	// Merchandise Section
	$data = array(
		'T-Shirts'	=> array(),
		'Photo'	=> array(),
		'Other'	=> array()
	);
	// add second jumps from merchandise
	$sql = "SELECT  DATE_FORMAT(sale_time, '%m') as yearmonth,
      			sum(sale_total_tshirt) as amount,
      			sum(sale_total_qty_tshirt) as pax,
      			sum(sale_total_photo) as amount_photo,
      			sum(sale_total_qty_photo) as pax_photo,
      			sum(sale_total-sale_total_photo-sale_total_tshirt-sale_total_2nd_jump) as amount_other,
      			sum(sale_total_qty-sale_total_qty_photo-sale_total_qty_tshirt-sale_total_qty_2nd_jump) as pax_other
		FROM merchandise_sales
		WHERE 
			" . (($site_id > 0) ? " site_id = '".$site_id."' AND " : "site_id > 0 AND ") . "
			sale_time like '$current_year-%'
		GROUP BY yearmonth 
		ORDER BY yearmonth ASC;
	";
	$res = mysql_query($sql) or die(mysql_error());
	while ($row = mysql_fetch_assoc($res)) {
		if ($row['pax'] > 0) {
			$data['T-Shirts'][$row['yearmonth']] = array(
				'pax'		=> $row['pax'],
				'amount'	=> $row['amount']
			);
		};
		if ($row['pax_photo'] > 0) {
			$data['Photo'][$row['yearmonth']] = array(
				'pax'		=> $row['pax_photo'],
				'amount'	=> $row['amount_photo']
			);
		};
		if ($row['pax_other'] > 0) {
			$data['Other'][$row['yearmonth']] = array(
				'pax'		=> $row['pax_other'],
				'amount'	=> $row['amount_other']
			);
		};
	};
	// tshirt from customerregs1
    $sql = "SELECT 
			DATE_FORMAT(BookingDate, '%m') as yearmonth, 
			SUM(tshirt_qty) as pax, 
			SUM(tshirt) as amount,
			SUM(photos / {$config['price_photo']}) as pax_photo, 
			SUM(photos) as amount_photo,
			SUM(other / 100 + video / {$config['price_video']} + gopro / {$config['price_gopro']}) as pax_other, 
			SUM(other + video + gopro) as amount_other
		FROM customerregs1 
		WHERE 
			" . (($site_id > 0) ? " site_id = '".$site_id."' AND " : "site_id > 0 AND ") . "
			BookingDate like '$current_year-%' 
			and Checked = 1 
			and DeleteStatus = 0 
		GROUP BY yearmonth 
		ORDER BY yearmonth ASC;";
    $res = mysql_query($sql) or (die(mysql_error()));
    while ($row = mysql_fetch_assoc($res)) {
		if ($row['pax'] > 0) {
			$f = 'T-Shirts';
			if (!array_key_exists($row['yearmonth'], $data[$f])) {
				$data[$f][$row['yearmonth']] = array('pax'	=> 0, 'amount'	=> 0);
			};
        	$data['T-Shirts'][$row['yearmonth']]['pax'] += $row['pax'];
        	$data['T-Shirts'][$row['yearmonth']]['amount'] += $row['amount'];
		};
		if ($row['pax_photo'] > 0) {
			$f = 'Photo';
			if (!array_key_exists($row['yearmonth'], $data[$f])) {
				$data[$f][$row['yearmonth']] = array('pax'	=> 0, 'amount'	=> 0);
			};
        	$data['Photo'][$row['yearmonth']]['pax'] += $row['pax_photo'];
        	$data['Photo'][$row['yearmonth']]['amount'] += $row['amount_photo'];
		};
		if ($row['pax_other'] > 0) {
			$f = 'Other';
			if (!array_key_exists($row['yearmonth'], $data[$f])) {
				$data[$f][$row['yearmonth']] = array('pax'	=> 0, 'amount'	=> 0);
			};
        	$data['Other'][$row['yearmonth']]['pax'] += $row['pax_other'];
        	$data['Other'][$row['yearmonth']]['amount'] += $row['amount_other'];
		};
    };
    mysql_free_result($res);
	$subtotals = array();
	$totals = array();
	$all_data = $data;
	foreach ($all_data as $title => $data) {
		echo "<tr>";
		echo "<td class='first-col'>$title</td>";
		foreach ($headers as $num => $val) {
			if ($num > 0) {
				$key = sprintf("%02d", $num);
				$pax = array_key_exists($key, $data) ? $data[$key]['pax'] : '-';
				$amount = array_key_exists($key, $data) ? $data[$key]['amount'] : '-';
				if ($num == 13) {
					$pax = $totals['pax'];
					$amount = $totals['amount'];
				} else {
					if (array_key_exists($key, $data)) {
						$totals['pax'] += $data[$key]['pax'];
						$totals['amount'] += $data[$key]['amount'];
					};
				};
				$subtotals[$key]['pax'] += array_key_exists($key, $data) ? $data[$key]['pax'] : 0;
				$subtotals[$key]['amount'] += array_key_exists($key, $data) ? $data[$key]['amount'] : 0;
				$subtotals[$key]['pax'] += ($num == 13) ? $totals['pax'] : 0;
				$subtotals[$key]['amount'] += ($num == 13) ? $totals['amount'] : 0;
				echo "<td class='pax'>$pax</td>\n";
				echo "<td class='amount'>$amount</td>\n";
			};
		};
		echo "</tr>";
	};
	// Merchandise total
	echo "<tr class='bungy-total'>";
	echo "<th class='subtotal-header'>MERCHANDISE TOTAL</th>";
	foreach ($headers as $num => $val) {
		if ($num > 0) {
			$key = sprintf("%02d", $num);
			$pax = $subtotals[$key]['pax'];
			$amount = $subtotals[$key]['amount'];
			echo "<th class='pax'>$pax</th>\n";
			echo "<th class='amount'>$amount</th>\n";
		};	
	};
	echo "</tr>";
	// INCOME TOTAL
	$i_total = array();
	echo "<tr class='income-total'>";
	echo "<th class='subtotal-header'>TOTAL INCOME</th>";
	foreach ($headers as $num => $val) {
		if ($num > 0) {
			$key = sprintf("%02d", $num);
			$pax = $subtotals[$key]['pax'] + $b_total[$key]['pax'];
			$amount = $subtotals[$key]['amount'] + $b_total[$key]['amount'];
			$i_total[$key] = $amount;
			echo "<th class='pax'>$pax</th>\n";
			echo "<th class='amount'>$amount</th>\n";
		};	
	};
	echo "</tr>";

?>
	<tr>
		<td class="divider" colspan="27">&nbsp;</td>
	</tr>
	<tr class="expenses-month-headers">
<?php 
	$headers[0] = "EXPENSES";
	foreach ($headers as $num => $header) {
		$colspan = '';
		if ($num > 0) $colspan = ' colspan="2"';
		echo "<th$colspan>$header</th>\n";
	};
?>
	</tr>
	<tr class='column-headers'>
<?php 
	foreach ($headers as $num => $header) {
		if ($num > 0) {
			echo "<th class='amount' colspan='2'>amount</th>\n";
		} else {
			echo "<th class='first-col'>&nbsp;</th>\n";
		};
	};
?>
	</tr>
<?php
	// Tourism Board
    $sql = "SELECT 
			DATE_FORMAT(`date`, '%m') as yearmonth, 
			SUM(tboard_day_total) as amount
		FROM income 
		WHERE 
			" . (($site_id > 0) ? " site_id = '".$site_id."' AND " : "site_id > 0 AND ") . "
			`date` like '$current_year-%' 
		GROUP BY yearmonth 
		ORDER BY yearmonth ASC;";
    $res = mysql_query($sql) or (die(mysql_error()));
	$data = array();
    while ($row = mysql_fetch_assoc($res)) {
        $data[$row['yearmonth']] = $row;
    };
    mysql_free_result($res);
	$totals = array(
		'amount'	=> 0
	);
	$subtotals = array();
	echo "<tr class='pink'>";
	echo "<td class='first-col tboard'>TOURISM BOARD PAYMENTS</td>";
	foreach ($headers as $num => $val) {
		if ($num > 0) {
			$key = sprintf("%02d", $num);
			$amount = array_key_exists($key, $data) ? $data[$key]['amount'] : '-';
			if ($num == 13) {
				$amount = $totals['amount'];
			} else {
				if (array_key_exists($key, $data)) {
					$totals['amount'] += $data[$key]['amount'];
				};
			};
			$subtotals[$key]['amount'] += array_key_exists($key, $data) ? $data[$key]['amount'] : 0;
			$subtotals[$key]['amount'] += ($num == 13) ? $totals['amount'] : 0;
			echo "<td colspan='2' class='amount'>$amount</td>\n";
		};
	};
	echo "</tr>";
	// INCOME TOTAL MINUS TBOARD TOTAL
	echo "<tr class='income-total-tboard pink'>";
	echo "<th class='subtotal-header'>INCOME MINUS TOURISM BOARD</th>";
	foreach ($headers as $num => $val) {
		if ($num > 0) {
			$key = sprintf("%02d", $num);
			$amount = -$subtotals[$key]['amount'] + $i_total[$key];
			echo "<th colspan='2' class='amount'>$amount</th>\n";
		};	
	};
	echo "</tr>";
	// Staff Salary
	$sql = "SELECT 
		staff,
		DATE_FORMAT(`date`, '%m') as yearmonth,
		SUM(TIME_TO_SEC(finish) - TIME_TO_SEC(start)) as amount
	FROM payroll
	WHERE
		" . (($site_id > 0) ? " site_id = '".$site_id."' AND " : "site_id > 0 AND ") . "
		`date` like '$current_year-%'
	GROUP BY yearmonth, staff;";
    $res = mysql_query($sql) or (die(mysql_error()));
	$staff = array();
    while ($row = mysql_fetch_assoc($res)) {
        $staff[$row['staff']][$row['yearmonth']] = $row;
    };
    mysql_free_result($res);
	$sql = "SELECT DATE_FORMAT(`date`, '%m') as yearmonth, who, SUM(`out`) as amount
		FROM banking 
		WHERE 
			" . (($site_id > 0) ? " site_id = '".$site_id."' AND " : "site_id > 0 AND ") . "
			`date` like '$current_year-%'
			and description = 'Wages for Non directors'
	GROUP BY yearmonth, who";
    $res = mysql_query($sql) or (die(mysql_error()));
	$salary = array();
    while ($row = mysql_fetch_assoc($res)) {
        $salary[$row['who']][$row['yearmonth']] = $row;
    };
    mysql_free_result($res);
	$subtotals = array();
	foreach ($staff as $s => $data) {
		$totals = array(
			'amount'	=> 0
		);
		echo "<tr>";
		echo "<td class='first-col'>$s</td>";
		$data = (array_key_exists($s, $salary)) ? $salary[$s] : array();
		foreach ($headers as $num => $val) {
			if ($num > 0) {
				$key = sprintf("%02d", $num);
				$amount = array_key_exists($key, $data) ? $data[$key]['amount'] : '-';
				if ($num == 13) {
					$amount = $totals['amount'];
				} else {
					if (array_key_exists($key, $data)) {
						$totals['amount'] += $data[$key]['amount'];
					};
				};
				$subtotals[$key]['amount'] += array_key_exists($key, $data) ? $data[$key]['amount'] : 0;
				$subtotals[$key]['amount'] += ($num == 13) ? $totals['amount'] : 0;
				echo "<td colspan='2' class='amount'>$amount</td>\n";
			};
		};
		echo "</tr>";
	};
	// Staff Salary Total
	echo "<tr class='staff-salary-total'>";
	echo "<th class='subtotal-header'>STAFF SALARY TOTAL</th>";
	foreach ($headers as $num => $val) {
		if ($num > 0) {
			$key = sprintf("%02d", $num);
			$amount = $subtotals[$key]['amount'];
			echo "<th colspan='2' class='amount'>$amount</th>\n";
		};	
	};
	echo "</tr>";
	$staff_salary = $subtotals;
	// other expenses
	$sql = "SELECT DATE_FORMAT(`date`, '%m') as yearmonth, description, SUM(`out`) as amount
		FROM banking 
		WHERE 
			" . (($site_id > 0) ? " site_id = '".$site_id."' AND " : "site_id > 0 AND ") . "
			`date` like '$current_year-%'
			and `out` > 0
			and description <> 'Wages for Non directors'
	GROUP BY yearmonth, description";
    $res = mysql_query($sql) or (die(mysql_error()));
	$expenses = array();
    while ($row = mysql_fetch_assoc($res)) {
        $expenses[$row['description']][$row['yearmonth']] = $row;
    };
    mysql_free_result($res);
	// expenses table
	$sql = "SELECT DATE_FORMAT(`date`, '%m') as yearmonth, description, SUM(`cost`) as amount
		FROM expenses 
		WHERE 
			" . (($site_id > 0) ? " site_id = '".$site_id."' AND " : "site_id > 0 AND ") . "
			`date` like '$current_year-%'
			and `cost` > 0
			and description <> 'Wages for Non directors'
	GROUP BY yearmonth, description";
    $res = mysql_query($sql) or (die(mysql_error()));
	$expenses = array();
    while ($row = mysql_fetch_assoc($res)) {
		if (!array_key_exists($row['description'], $expenses)) {
			$expenses[$row['description']] = array();
		};
		if (!array_key_exists($row['yearmonth'], $expenses[$row['description']])) {
			$expenses[$row['description']][$row['yearmonth']] = $row;
		} else {
        	$expenses[$row['description']][$row['yearmonth']]['amount'] += $row['amount'];
		};
    };
    mysql_free_result($res);
	$subtotals = array();
	foreach ($expenses as $e => $data) {
		$totals = array(
			'amount'	=> 0
		);
		echo "<tr>";
		echo "<td class='first-col'>$e</td>";
		foreach ($headers as $num => $val) {
			if ($num > 0) {
				$key = sprintf("%02d", $num);
				$amount = array_key_exists($key, $data) ? $data[$key]['amount'] : '-';
				if ($num == 13) {
					$amount = $totals['amount'];
				} else {
					if (array_key_exists($key, $data)) {
						$totals['amount'] += $data[$key]['amount'];
					};
				};
				$subtotals[$key]['amount'] += array_key_exists($key, $data) ? $data[$key]['amount'] : 0;
				$subtotals[$key]['amount'] += ($num == 13) ? $totals['amount'] : 0;
				echo "<td colspan='2' class='amount'>$amount</td>\n";
			};
		};
		echo "</tr>";
	};
	// Banking and expenses subtotal
	echo "<tr class='expenses-sub-total'>";
	echo "<th class='subtotal-header'>&nbsp;</th>";
	foreach ($headers as $num => $val) {
		if ($num > 0) {
			$key = sprintf("%02d", $num);
			$amount = $subtotals[$key]['amount'];
			echo "<th colspan='2' class='amount'>$amount</th>\n";
		};	
	};
	echo "</tr>";
	echo "<tr class='staff-salary-total'>";
	echo "<th class='subtotal-header'>&nbsp;</th>";
	foreach ($headers as $num => $val) {
		if ($num == 0) continue;
		$amount = "&nbsp;";
		echo "<th colspan='2' class='amount'>$amount</th>\n";
	};
	echo "</tr>";
	// expenses total
	echo "<tr class='expenses-total'>";
	echo "<th class='subtotal-header'>TOTAL EXPENSE</th>";
	foreach ($headers as $num => $val) {
		if ($num > 0) {
			$key = sprintf("%02d", $num);
			$amount = $subtotals[$key]['amount'] + $staff_salary[$key]['amount'];
			echo "<th colspan='2' class='amount'>$amount</th>\n";
		};	
	};
	echo "</tr>";
	// profit or loss total
	echo "<tr class='profit-total'>";
	echo "<th class='subtotal-header'>Total Profit Or Loss</th>";
	$key = '13';
	$amount = $i_total[$key] - $subtotals[$key]['amount'] - $staff_salary[$key]['amount'];
	echo "<th colspan='2' class='amount'>$amount</th>\n";
	echo "<th colspan='24' class='amount'>&nbsp;</th>\n";
	echo "</tr>";
	
?>
	</table>
<button id="back">Back</button>
<button id="home">Home</button>
	</div>
</body>
</html>
