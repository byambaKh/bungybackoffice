<?php
	include "../includes/application_top.php";
?>
<!DOCTYPE html>
<html>
  <head>
<?php include '../includes/header_tags.php'; ?>
	<title><?php echo SYSTEM_SUBDOMAIN; ?> System </title>
<script>
function procLogout(){
  document.location ="?logout=true";
  return true;
}
function open_page(url, newwin) {
	if (newwin) {
		var nwin = window.open(url, '_blank');
		nwin.focus();
	} else {
		document.location = url;
	}
}
</script>
<style>
h1 {
    color: white;
}
button:disabled div {
    color: silver;
}
div {
    text-align: center;
}
button {
    background-image: url('../img/blank-button.png');
    width: 417px;
    height: 63px;
    border: none;
    background-position: center;
    margin: 1px;
	overflow: hidden;
	padding: 0px;
}
button div {
    width: 100%;
    height: 100%;
    font-size: 40px;
    color: white;
    font-weight: normal;
    font-family: Verdana;
	margin: 0px;
	padding: 0px;
}
</style>
</head>
<body bgcolor=black>
<div>
<img src="/img/index.jpg">
<h1><?php echo SYSTEM_SUBDOMAIN; ?> System for SysAdmin</h1>
<?php
	$buttons = array();
	switch ($_SESSION['access_type']) {
		case ($user->hasRole('Sysadmin')):
			switch ($_GET['action']) {
				case 'analysis':
					$sql = "SELECT * FROM sites WHERE subdomain NOT IN ('test', 'media');";
					$res = mysql_query($sql) or die(mysql_error());
					$sites_buttons = array();
					while ($row = mysql_fetch_assoc($res)) {
						$sites_buttons[ucfirst($row['subdomain'])] = 'analysis.php?subdomain=' . $row['subdomain'];
					};
					$buttons = array_merge($sites_buttons, array(
						'Total P&L'	=> 'analysis.php?subdomain=minakami&total=all',
						'Back'	=> '/Analysis/',
					));
					break;
				default:
					$buttons = array(
						'P&L Analysis'	=> '?action=analysis',
						'Charts'	=> 'charts.php',
						'Expenses'		=> '/Bookkeeping/expenses.php?site=main',
						'Banking'		=> '/Bookkeeping/banking.php?site=main',
						'-'	=> '-',
						'Back'	=> '/menu.php',
					);
					break;
			};
			break;
	};
	$i = 0;
	while (list($text, $url) = each($buttons)) {
		if (empty($text)) continue;
		$i++;
?>
<br><button onClick="open_page('<?php echo $url; ?>', <?php echo (int)(strstr($url, 'http://') !== FALSE); ?>);"<?php echo (empty($url))?' disabled':''; ?>><div><?php echo $text; ?></div></button>
<?php
	};
?>
<br><br>
</div>
</body></html>
