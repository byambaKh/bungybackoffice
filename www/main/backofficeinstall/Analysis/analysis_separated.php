<?php
include '../includes/application_top.php';
$site_id = null;
$sqlPlace = "SELECT * FROM sites where subdomain = '$subdomain';";
$placesResults = queryForRows($sqlPlace);
if (array_key_exists(0, $placesResults)) {
    $site_name = $placesResults[0]['subdomain'];
    $site_id = $placesResults[0]['id'];
}

$sqlConfiguration = "select * from configuration where site_id = $site_id;";
$configurationResults = queryForRows($sqlConfiguration);
foreach ($configurationResults as $configurationResult) {
    $config[$configurationResult['key']] = $configurationResult['value'];
}
$tableData = [
    'income' => [
        'headers' =>[],
        'bungy' => [
            'onsite' =>[
                'jumps' =>[],
                'cancellations' => [],
                'subtotal' => [],
            ],
            'offsite' => [
                'jumps' => [],
                'cancellations' => [],
                'subtotal' => [],
            ],
            'bungyTotal' => [],
        ],
        'merchandise' => [
            'tShirt' => [],
            'photo' => [],
            'other' => [],
            'merchandiseTotal' => [],
        ],
        'incomeTotal' => [],
    ],
    'Expenses' => [
        'headers' => [],
        'tourismBoardPayments' => [],
        'incomeMinusTourism' => [],
        'staffSalary' => [],
            'breakdowns, ' => [
                 //by site and by person for head office
        ],
        'staffSalaryTotal' => [],
        'expenseItems' => [],
            'expenseTotal' => [],
    ],
    'profitAndLossTotal' => [],
];



$headers = ["INCOME", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "DEC", "TOTALS"];
//$headers[] = 'TOTALS';//Create headers for the table, INCOME, JAN ..... DEC, TOTALS
$current_year = date("Y");

if (isset($_GET['year'])) {
    $current_year = (int)$_GET['year'];
}
//Month Headers
$monthHeaders = '';
foreach ($headers as $num => $header) {
    $colspan = '';
    if ($num > 0) $colspan = ' colspan="2"';
    $monthHeaders .= "<th$colspan>$header</th>\n";
}

//Column Headers
$columnHeaders = '';
foreach ($headers as $num => $header) {
    if ($num > 0) {
        $columnHeaders .= "<th class='pax'>pax</th>\n";
        $columnHeaders .= "<th class='amount'>amount</th>\n";
    } else {
        $columnHeaders .= "<th class='first-col'>&nbsp;</th>\n";
    }
}
//Onsite Jumps
$sql = "SELECT DISTINCT Rate
		FROM customerregs1
		WHERE
			" . (($site_id > 0) ? " site_id = '" . $site_id . "' AND " : "site_id > 0 AND ") . "
			DeleteStatus = 0
			AND Checked = 1
			AND BookingDate like '$current_year-%'
			AND NoOfJump > 0
			AND CollectPay = 'Onsite'
		ORDER BY Rate DESC;";
$res = mysql_query($sql) or die(mysql_error());
$subtotals = [];
//get each of the prices charged for jumps during that month
//then for each of those prices, perform a calculation

$ratesChargedInThisMonth = queryForRows($sql);//Put this in to the loop;

$htmlOnsiteJumps = '';
foreach ($ratesChargedInThisMonth as $thisMonth) {
    // process each online Rate
    $rate = $thisMonth['Rate'];
    if ($rate == 0) continue;
    $htmlOnsiteJumps .= "<tr>";
    $htmlOnsiteJumps .= "<td class='first-col'>Bungy Onsite - $rate</td>\n";
    //Get all of number of jumps for each rate
    $sqlPaxAndAmount = "SELECT
				DATE_FORMAT(BookingDate, '%m') as yearmonth,
				sum(NoOfJump) as pax,
				sum(NoOfJump * Rate) as amount
			FROM customerregs1
			WHERE
				" . (($site_id > 0) ? " site_id = '" . $site_id . "' AND " : "site_id > 0 AND ") . "
				DeleteStatus = 0
				AND Checked = 1
				AND Rate = $rate
				AND BookingDate like '$current_year-%'
				AND NoOfJump > 0
				AND CollectPay = 'Onsite'
			GROUP BY yearmonth
			ORDER BY yearmonth ASC;
		";

    //
    $data = [];

    $paxAndAmounts = queryForRows($sqlPaxAndAmount);
    foreach ($paxAndAmounts as $paxAndAmount) {
        $data[$paxAndAmount['yearmonth']] = $paxAndAmount;
    }

    if ($rate == $config['second_jump_rate']) {
        //add second jumps data
        $sqlSecondJumps = "SELECT
					DATE_FORMAT(BookingDate, '%m') as yearmonth,
					sum(2ndj / {$config['second_jump_rate']}) as pax,
					sum(2ndj) as amount
				FROM customerregs1
				WHERE
					" . (($site_id > 0) ? " site_id = '" . $site_id . "' AND " : "site_id > 0 AND ") . "
					DeleteStatus = 0
					AND Checked = 1
					AND BookingDate like '$current_year-%'
				/*	AND CollectPay = 'Onsite'*/
				GROUP BY yearmonth
				ORDER BY yearmonth ASC;
			";
        //$res2 = mysql_query($sql) or die(mysql_error());
        $secondJumps = queryForRows($sqlSecondJumps);
        foreach ($secondJumps as $secondJump) {
            if (!array_key_exists($secondJump['yearmonth'], $data)) {
                $data[$secondJump['yearmonth']] = [
                    'pax'    => 0,
                    'amount' => 0
                ];
            }
            $data[$secondJump['yearmonth']]['pax'] += $secondJump['pax'];
            $data[$secondJump['yearmonth']]['amount'] += $secondJump['amount'];
        }
        // add second jumps from merchandise
        $sqlMerchandiseJumps = "SELECT  DATE_FORMAT(sale_time, '%m') as yearmonth,
        			sum(sale_total_2nd_jump) as amount,
        			sum(sale_total_qty_2nd_jump) as pax
				FROM merchandise_sales
				WHERE
					" . (($site_id > 0) ? " site_id = '" . $site_id . "' AND " : "site_id > 0 AND ") . "
					sale_time like '$current_year-%'
				GROUP BY yearmonth
				ORDER BY yearmonth ASC;
			";
        $merchandiseJumps = queryForRows($sqlMerchandiseJumps);
        foreach ($merchandiseJumps as $merchandiseJump) {
            if (!array_key_exists($merchandiseJump['yearmonth'], $data)) {
                $data[$merchandiseJump['yearmonth']] = [
                    'pax'    => 0,
                    'amount' => 0
                ];
            }
            $data[$merchandiseJump['yearmonth']]['pax'] += $merchandiseJump['pax'];
            $data[$merchandiseJump['yearmonth']]['amount'] += $merchandiseJump['amount'];
        }
    }
    $totals = [
        'pax'    => 0,
        'amount' => 0
    ];

    foreach ($headers as $num => $val) {
        if ($num > 0) {
            $key = sprintf("%02d", $num);
            $pax = array_key_exists($key, $data) ? $data[$key]['pax'] : '-';
            $amount = array_key_exists($key, $data) ? $data[$key]['amount'] : '-';
            if ($num == 13) {
                $pax = $totals['pax'];
                $amount = $totals['amount'];
            } else {
                if (array_key_exists($key, $data)) {
                    $totals['pax'] += $data[$key]['pax'];
                    $totals['amount'] += $data[$key]['amount'];
                }
            }
            if (!array_key_exists($key, $subtotals)) {
                $subtotals[$key] = [
                    'pax'    => 0,
                    'amount' => 0
                ];
            }
            $subtotals[$key]['pax'] += array_key_exists($key, $data) ? $data[$key]['pax'] : 0;
            $subtotals[$key]['amount'] += array_key_exists($key, $data) ? $data[$key]['amount'] : 0;
            $subtotals[$key]['pax'] += ($num == 13) ? $totals['pax'] : 0;
            $subtotals[$key]['amount'] += ($num == 13) ? $totals['amount'] : 0;
            $htmlOnsiteJumps .= "<td class='pax'>$pax</td>\n";
            $htmlOnsiteJumps .= "<td class='amount'>$amount</td>\n";
        }
    }
    $htmlOnsiteJumps .= "</tr>";
}

// Cancellation Onsite
$htmlCancellationsOnsite = '';

$sqlOnsiteCancellations = "SELECT
			DATE_FORMAT(BookingDate, '%m') as yearmonth,
			SUM(CancelFeeQTY) as pax,
			SUM(CancelFee * CancelFeeQTY) as amount
		FROM customerregs1
		WHERE
			" . (($site_id > 0) ? " site_id = '" . $site_id . "' AND " : "site_id > 0 AND ") . "
			BookingDate like '$current_year-%'
			and Checked = 1
			and CancelFeeCollect = 'Onsite'
			and NoOfJump > 0
		GROUP BY yearmonth
		ORDER BY yearmonth ASC;";

$onsiteCancellations = queryForRows($sqlOnsiteCancellations);
foreach ($onsiteCancellations as $onsiteCancellation) {
    $data[$onsiteCancellation['yearmonth']] = $onsiteCancellation;
}

$totals = [
    'pax'    => 0,
    'amount' => 0
];
$htmlCancellationsOnsite .= "<tr>";
$htmlCancellationsOnsite .= "<td class='first-col'>Bungy Onsite Cancellation</td>";
foreach ($headers as $num => $val) {
    if ($num > 0) {
        $key = sprintf("%02d", $num);
        $pax = array_key_exists($key, $data) ? $data[$key]['pax'] : '-';
        $amount = array_key_exists($key, $data) ? $data[$key]['amount'] : '-';
        if ($num == 13) {
            $pax = $totals['pax'];
            $amount = $totals['amount'];
        } else {
            if (array_key_exists($key, $data)) {
                $totals['pax'] += $data[$key]['pax'];
                $totals['amount'] += $data[$key]['amount'];
            }
        }
        $subtotals[$key]['pax'] += array_key_exists($key, $data) ? $data[$key]['pax'] : 0;
        $subtotals[$key]['amount'] += array_key_exists($key, $data) ? $data[$key]['amount'] : 0;
        $subtotals[$key]['pax'] += ($num == 13) ? $totals['pax'] : 0;
        $subtotals[$key]['amount'] += ($num == 13) ? $totals['amount'] : 0;
        $htmlCancellationsOnsite .= "<td class='pax'>$pax</td>\n";
        $htmlCancellationsOnsite .= "<td class='amount'>$amount</td>\n";
    }
}
$htmlCancellationsOnsite .= "</tr>";

// OnSite subtotal
$htmlOnsiteSubtotals = '';
$htmlOnsiteSubtotals .= "<tr class='subtotal'>";
$htmlOnsiteSubtotals .= "<th class='subtotal-header'>ON SITE SUBTOTAL</th>";
foreach ($headers as $num => $val) {
    if ($num > 0) {
        $key = sprintf("%02d", $num);
        $pax = $subtotals[$key]['pax'];
        $amount = $subtotals[$key]['amount'];
        $htmlOnsiteSubtotals .= "<th class='pax'>$pax</th>\n";
        $htmlOnsiteSubtotals .= "<th class='amount'>$amount</th>\n";
    }
}
$htmlOnsiteSubtotals .= "</tr>";
$onsite_subtotals = $subtotals;
// Offsite Bungy
$sqlOffsiteJumpRates = "SELECT DISTINCT Rate
		FROM customerregs1
		WHERE
			" . (($site_id > 0) ? " site_id = '" . $site_id . "' AND " : "site_id > 0 AND ") . "
			DeleteStatus = 0
			AND Checked = 1
			AND BookingDate like '$current_year-%'
			AND NoOfJump > 0
			AND CollectPay = 'Offsite'
		ORDER BY Rate DESC;";
$offsiteJumpRates = queryForRows($sqlOffsiteJumpRates);
$subtotals = [];
foreach ($offsiteJumpRates as $offsiteJumpRate) {
    // process each online Rate
    $rate = $offsiteJumpRate['Rate'];
    if ($rate == 0) continue;
    $htmlOffsiteBungy = "<tr>";
    $htmlOffsiteBungy .= "<td class='first-col'>Bungy Offsite - $rate</td>\n";
    $sqlOffsitePaxAndAmounts = "SELECT
				DATE_FORMAT(BookingDate, '%m') as yearmonth,
				sum(NoOfJump) as pax,
				sum(NoOfJump * Rate) as amount
			FROM customerregs1
			WHERE
				" . (($site_id > 0) ? " site_id = '" . $site_id . "' AND " : "site_id > 0 AND ") . "
				DeleteStatus = 0
				AND Checked = 1
				AND Rate = $rate
				AND BookingDate like '$current_year-%'
				AND NoOfJump > 0
				AND CollectPay = 'Offsite'
			GROUP BY yearmonth
			ORDER BY yearmonth ASC;
		";
    $data = [];

    $offsitePaxAndAmounts = queryForRows($sqlOffsitePaxAndAmounts);
    foreach ($offsitePaxAndAmounts as $offsitePaxAndAmount) {
        $data[$offsitePaxAndAmount['yearmonth']] = $offsitePaxAndAmount;
    }
    $totals = [
        'pax'    => 0,
        'amount' => 0
    ];
    foreach ($headers as $num => $val) {
        if ($num > 0) {
            $key = sprintf("%02d", $num);
            $pax = array_key_exists($key, $data) ? $data[$key]['pax'] : '-';
            $amount = array_key_exists($key, $data) ? $data[$key]['amount'] : '-';
            if ($num == 13) {
                $pax = $totals['pax'];
                $amount = $totals['amount'];
            } else {
                if (array_key_exists($key, $data)) {
                    $totals['pax'] += $data[$key]['pax'];
                    $totals['amount'] += $data[$key]['amount'];
                }
            }
            if (!array_key_exists($key, $subtotals)) {
                $subtotals[$key] = [
                    'pax'    => 0,
                    'amount' => 0
                ];
            }
            $subtotals[$key]['pax'] += array_key_exists($key, $data) ? $data[$key]['pax'] : 0;
            $subtotals[$key]['amount'] += array_key_exists($key, $data) ? $data[$key]['amount'] : 0;
            $subtotals[$key]['pax'] += ($num == 13) ? $totals['pax'] : 0;
            $subtotals[$key]['amount'] += ($num == 13) ? $totals['amount'] : 0;
            $htmlOffsiteBungy .= "<td class='pax'>$pax</td>\n";
            $htmlOffsiteBungy .= "<td class='amount'>$amount</td>\n";
        }
    }
    $htmlOffsiteBungy .= "</tr>";
}

/*
//ToPay Onsite
//This has been removed at the Financial officers request (Hua) 2015-01-20
$sql = "SELECT
        DATE_FORMAT(BookingDate, '%m') as yearmonth,
        SUM(RateToPayQTY) as pax,
        SUM(RateToPay * RateToPayQTY) as amount
    FROM customerregs1
    WHERE
        " . (($site_id > 0) ? " site_id = '".$site_id."' AND " : "site_id > 0 AND ") . "
        BookingDate like '$current_year-%'
        and Checked = 1
        and DeleteStatus = 0
        and CollectPay = 'Onsite'
        and RateToPay > 0
        and RateToPayQTY > 0
    GROUP BY yearmonth
    ORDER BY yearmonth ASC;";
$res = mysql_query($sql) or (die(mysql_error()));
$data = array();
while ($row = mysql_fetch_assoc($res)) {
    $data[$row['yearmonth']] = $row;
}
mysql_free_result($res);
$totals = array(
    'pax'		=> 0,
    'amount'	=> 0
);
echo "<tr>";
echo "<td class='first-col'>Bungy Onsite - ToPay</td>";
foreach ($headers as $num => $val) {
    if ($num > 0) {
        $key = sprintf("%02d", $num);
        $pax = array_key_exists($key, $data) ? $data[$key]['pax'] : '-';
        $amount = array_key_exists($key, $data) ? $data[$key]['amount'] : '-';
        if ($num == 13) {
            $pax = $totals['pax'];
            $amount = $totals['amount'];
        } else {
            if (array_key_exists($key, $data)) {
                $totals['pax'] += $data[$key]['pax'];
                $totals['amount'] += $data[$key]['amount'];
            }
        }
        $subtotals[$key]['pax'] += array_key_exists($key, $data) ? $data[$key]['pax'] : 0;
        $subtotals[$key]['amount'] += array_key_exists($key, $data) ? $data[$key]['amount'] : 0;
        $subtotals[$key]['pax'] += ($num == 13) ? $totals['pax'] : 0;
        $subtotals[$key]['amount'] += ($num == 13) ? $totals['amount'] : 0;
        echo "<td class='pax'>$pax</td>\n";
        echo "<td class='amount'>$amount</td>\n";
    }
}
echo "</tr>";
*/

// Cancellation Offsite
$htmlOffsiteCancellations = '';
$sqlOffsiteCancellations = "SELECT
			DATE_FORMAT(BookingDate, '%m') AS yearmonth,
			SUM(CancelFeeQTY) AS pax,
			SUM(CancelFee * CancelFeeQTY) AS amount
		FROM customerregs1
		WHERE
			" . (($site_id > 0) ? " site_id = '" . $site_id . "' AND " : "site_id > 0 AND ") . "
			BookingDate LIKE '$current_year-%'
			#AND Checked = 1
			#AND DeleteStatus = 0
			AND CancelFeeCollect = 'Offsite'
			#AND CancelFee > 0
			#AND CancelFeeQTY > 0
		GROUP BY yearmonth
		ORDER BY yearmonth ASC;";
$offsiteCancellations = queryForRows($sqlOffsiteCancellations);

$data = [];
foreach ($offsiteCancellations as $cancellationOffsite) {
    $data[$cancellationOffsite['yearmonth']] = $cancellationOffsite;
}

$totals = [
    'pax'    => 0,
    'amount' => 0
];

$htmlOffsiteCancellations .= "<tr>";
$htmlOffsiteCancellations .= "<td class='first-col'>Bungy Offsite Cancellation</td>";
foreach ($headers as $num => $val) {
    if ($num > 0) {
        $key = sprintf("%02d", $num);
        $pax = array_key_exists($key, $data) ? $data[$key]['pax'] : '-';
        $amount = array_key_exists($key, $data) ? $data[$key]['amount'] : '-';
        if ($num == 13) {
            $pax = $totals['pax'];
            $amount = $totals['amount'];
        } else {
            if (array_key_exists($key, $data)) {
                $totals['pax'] += $data[$key]['pax'];
                $totals['amount'] += $data[$key]['amount'];
            }
        }
        $subtotals[$key]['pax'] += array_key_exists($key, $data) ? $data[$key]['pax'] : 0;
        $subtotals[$key]['amount'] += array_key_exists($key, $data) ? $data[$key]['amount'] : 0;
        $subtotals[$key]['pax'] += ($num == 13) ? $totals['pax'] : 0;
        $subtotals[$key]['amount'] += ($num == 13) ? $totals['amount'] : 0;
        $htmlOffsiteCancellations .= "<td class='pax'>$pax</td>\n";
        $htmlOffsiteCancellations .= "<td class='amount'>$amount</td>\n";
    }
}
$htmlOffsiteCancellations .= "</tr>";
// Offsite subtotal
$htmlOffsiteCancellations .= "<tr class='subtotal'>";
$htmlOffsiteCancellations .= "<th class='subtotal-header'>OFF SITE SUBTOTAL</th>";
foreach ($headers as $num => $val) {
    if ($num > 0) {
        $key = sprintf("%02d", $num);
        $pax = $subtotals[$key]['pax'];
        $amount = $subtotals[$key]['amount'];
        $htmlOffsiteCancellations .= "<th class='pax'>$pax</th>\n";
        $htmlOffsiteCancellations .= "<th class='amount'>$amount</th>\n";
    }
}
$htmlOffsiteCancellations .= "</tr>";

// BUNGY TOTAL
$bungyTotal = '';
$htmlBungyTotal .= "<tr class='bungy-total'>";
$htmlBungyTotal .= "<th class='subtotal-header'>BUNGY TOTAL</th>";
$b_total = [];
foreach ($headers as $num => $val) {
    if ($num > 0) {
        $key = sprintf("%02d", $num);
        $pax = $subtotals[$key]['pax'] + $onsite_subtotals[$key]['pax'];
        $amount = $subtotals[$key]['amount'] + $onsite_subtotals[$key]['amount'];
        $b_total[$key] = [
            'pax'    => $pax,
            'amount' => $amount
        ];
        $htmlBungyTotal .= "<th class='pax'>$pax</th>\n";
        $htmlBungyTotal .= "<th class='amount'>$amount</th>\n";
    }
}
$htmlBungyTotal .= "</tr>";

//***********************************************Merchandise Section***************************************************
$htmlMerchandise = '';
$data = [
    'T-Shirts' => [],
    'Photo'    => [],
    'Other'    => []
];
// add second jumps from merchandise
$sqlMerchandiseSecondJumps = "SELECT  DATE_FORMAT(sale_time, '%m') as yearmonth,
      			sum(sale_total_tshirt) as amount,
      			sum(sale_total_qty_tshirt) as pax,
      			sum(sale_total_qty_photo * 500) as amount_photo,#company collects 500 yen for every sale
      			sum(sale_total_qty_photo) as pax_photo,
      			sum(sale_total-sale_total_photo-sale_total_tshirt-sale_total_2nd_jump) as amount_other,
      			sum(sale_total_qty-sale_total_qty_photo-sale_total_qty_tshirt-sale_total_qty_2nd_jump) as pax_other
		FROM merchandise_sales
		WHERE
			" . (($site_id > 0) ? " site_id = '" . $site_id . "' AND " : "site_id > 0 AND ") . "
			sale_time like '$current_year-%'
		GROUP BY yearmonth
		ORDER BY yearmonth ASC;
	";

$merchandiseSecondJumps = queryForRows($sqlMerchandiseSecondJumps);
foreach ($merchandiseSecondJumps as $merchandiseSecondJump) {
    if ($merchandiseSecondJump['pax'] > 0) {
        $data['T-Shirts'][$merchandiseSecondJump['yearmonth']] = [
            'pax'    => $merchandiseSecondJump['pax'],
            'amount' => $merchandiseSecondJump['amount']
        ];
    }
    if ($merchandiseSecondJump['pax_photo'] > 0) {
        $data['Photo'][$merchandiseSecondJump['yearmonth']] = [
            'pax'    => $merchandiseSecondJump['pax_photo'],
            'amount' => $merchandiseSecondJump['amount_photo']
        ];
    }
    if ($merchandiseSecondJump['pax_other'] > 0) {
        $data['Other'][$merchandiseSecondJump['yearmonth']] = [
            'pax'    => $merchandiseSecondJump['pax_other'],
            'amount' => $merchandiseSecondJump['amount_other']
        ];
    }
}
//get the photo price for the selected domain
//note that using the config value will use the price from the current domain not the
//selected domain


$photoPrice = configValueForKeyAndLocation("price_photo", $subdomain);

// tshirt from customerregs1
$sqlTShirts = "SELECT
			DATE_FORMAT(BookingDate, '%m') as yearmonth,
			SUM(tshirt_qty) as pax,
			SUM(tshirt) as amount,
			SUM(photos / $photoPrice) as pax_photo,#
			SUM(photos*500/$photoPrice) as amount_photo,
			SUM(other / 100 + video / {$config['price_video']} + gopro / {$config['price_gopro']}) as pax_other,
			SUM(other + video + gopro) as amount_other
		FROM customerregs1
		WHERE
			" . (($site_id > 0) ? " site_id = '" . $site_id . "' AND " : "site_id > 0 AND ") . "
			BookingDate like '$current_year-%'
			AND Checked = 1
			AND NoOfJump > 0
			AND DeleteStatus = 0
		GROUP BY yearmonth
		ORDER BY yearmonth ASC;";

$tShirts = queryForRows($sqlTShirts);

foreach ($tShirts as $tShirt) {

    if ($tShirt['pax'] > 0) {
        $f = 'T-Shirts';
        if (!array_key_exists($tShirt['yearmonth'], $data[$f])) {
            $data[$f][$tShirt['yearmonth']] = ['pax' => 0, 'amount' => 0];
        }
        $data['T-Shirts'][$tShirt['yearmonth']]['pax'] += $tShirt['pax'];
        $data['T-Shirts'][$tShirt['yearmonth']]['amount'] += $tShirt['amount'];
    }
    if ($tShirt['pax_photo'] > 0) {
        $f = 'Photo';
        if (!array_key_exists($tShirt['yearmonth'], $data[$f])) {
            $data[$f][$tShirt['yearmonth']] = ['pax' => 0, 'amount' => 0];
        }
        $data['Photo'][$tShirt['yearmonth']]['pax'] += $tShirt['pax_photo'];
        $data['Photo'][$tShirt['yearmonth']]['amount'] += $tShirt['amount_photo'];
    }
    if ($tShirt['pax_other'] > 0) {
        $f = 'Other';
        if (!array_key_exists($tShirt['yearmonth'], $data[$f])) {
            $data[$f][$tShirt['yearmonth']] = ['pax' => 0, 'amount' => 0];
        }
        $data['Other'][$tShirt['yearmonth']]['pax'] += $tShirt['pax_other'];
        $data['Other'][$tShirt['yearmonth']]['amount'] += $tShirt['amount_other'];
    }
}

$subtotals = [];
$totals = [];
$all_data = $data;
foreach ($all_data as $title => $data) {
    $htmlMerchandise .= "<tr>";
    $htmlMerchandise .= "<td class='first-col'>$title</td>";
    foreach ($headers as $num => $val) {
        if ($num > 0) {
            $key = sprintf("%02d", $num);
            $pax = array_key_exists($key, $data) ? $data[$key]['pax'] : '-';
            $amount = array_key_exists($key, $data) ? $data[$key]['amount'] : '-';
            if ($num == 13) {
                $pax = $totals['pax'];
                $amount = $totals['amount'];
            } else {
                if (array_key_exists($key, $data)) {
                    $totals['pax'] += $data[$key]['pax'];
                    $totals['amount'] += $data[$key]['amount'];
                }
            }
            $subtotals[$key]['pax'] += array_key_exists($key, $data) ? $data[$key]['pax'] : 0;
            $subtotals[$key]['amount'] += array_key_exists($key, $data) ? $data[$key]['amount'] : 0;
            $subtotals[$key]['pax'] += ($num == 13) ? $totals['pax'] : 0;
            $subtotals[$key]['amount'] += ($num == 13) ? $totals['amount'] : 0;
            $htmlMerchandise .= "<td class='pax'>$pax</td>\n";
            $htmlMerchandise .= "<td class='amount'>$amount</td>\n";
        }
    }
    $htmlMerchandise .= "</tr>";
}

// Merchandise total
$htmlMerchandiseTotal = '';
$htmlMerchandiseTotal .= "<tr class='bungy-total'>";
$htmlMerchandiseTotal .= "<th class='subtotal-header'>MERCHANDISE TOTAL</th>";
foreach ($headers as $num => $val) {
    if ($num > 0) {
        $key = sprintf("%02d", $num);
        $pax = $subtotals[$key]['pax'];
        $amount = $subtotals[$key]['amount'];
        $htmlMerchandiseTotal .= "<th class='pax'>$pax</th>\n";
        $htmlMerchandiseTotal .= "<th class='amount'>$amount</th>\n";
    }
}
$htmlMerchandiseTotal .= "</tr>";

// INCOME TOTAL
$htmlIncomeTotal = '';
$i_total = [];
$htmlIncomeTotal .= "<tr class='income-total'>";
$htmlIncomeTotal .= "<th class='subtotal-header'>TOTAL INCOME</th>";
foreach ($headers as $num => $val) {
    if ($num > 0) {
        $key = sprintf("%02d", $num);
        $pax = $subtotals[$key]['pax'] + $b_total[$key]['pax'];
        $amount = $subtotals[$key]['amount'] + $b_total[$key]['amount'];
        $i_total[$key] = $amount;
        $htmlIncomeTotal .= "<th class='pax'>$pax</th>\n";
        $htmlIncomeTotal .= "<th class='amount'>$amount</th>\n";
    }
}
$htmlIncomeTotal .= "</tr>";

//**********************************************************************************************************************
//**********************************************EXPENSES SECTION********************************************************
//**********************************************************************************************************************
$htmlExpensesMonthHeaders = '';
$headers[0] = "EXPENSES";
foreach ($headers as $num => $header) {
    $colspan = '';
    if ($num > 0) $colspan = ' colspan="2"';
    $htmlExpensesMonthHeaders .= "<th$colspan>$header</th>\n";
}

$htmlExpensesColumnHeaders = '';
foreach ($headers as $num => $header) {
    if ($num > 0) {
        $htmlExpensesColumnHeaders .= "<th class='amount' colspan='2'>amount</th>\n";
    } else {
        $htmlExpensesColumnHeaders .= "<th class='first-col'>&nbsp;</th>\n";
    }
}

// Tourism Board
$htmlTourismBoardPayments = '';
$sqlTourismBoardPayments = "SELECT
			DATE_FORMAT(`date`, '%m') as yearmonth,
			SUM(tboard_day_total) as amount
		FROM income
		WHERE
			" . (($site_id > 0) ? " site_id = '" . $site_id . "' AND " : "site_id > 0 AND ") . "
			`date` like '$current_year-%'
		GROUP BY yearmonth
		ORDER BY yearmonth ASC;";

$tourismBoardPayments = queryForRows($sqlTourismBoardPayments);
$data = [];

foreach ($tourismBoardPayments as $tourismBoardPayment) {
    $data[$tourismBoardPayment['yearmonth']] = $tourismBoardPayment;
}
$totals = [
    'amount' => 0
];
$subtotals = [];
$htmlTourismBoardPayments .= "<tr class='pink'>";
$htmlTourismBoardPayments .= "<td class='first-col tboard'>TOURISM BOARD PAYMENTS</td>";
foreach ($headers as $num => $val) {
    if ($num > 0) {
        $key = sprintf("%02d", $num);
        $amount = array_key_exists($key, $data) ? $data[$key]['amount'] : '-';
        if ($num == 13) {
            $amount = $totals['amount'];
        } else {
            if (array_key_exists($key, $data)) {
                $totals['amount'] += $data[$key]['amount'];
            }
        }
        $subtotals[$key]['amount'] += array_key_exists($key, $data) ? $data[$key]['amount'] : 0;
        $subtotals[$key]['amount'] += ($num == 13) ? $totals['amount'] : 0;
        $htmlTourismBoardPayments .= "<td colspan='2' class='amount'>$amount</td>\n";
    }
}
$htmlTourismBoardPayments .= "</tr>";
// INCOME TOTAL MINUS TBOARD TOTAL
$htmlIncomeTotalMinusTourismBoardTotal = '';
$htmlIncomeTotalMinusTourismBoardTotal .= "<tr class='income-total-tboard pink'>";
$htmlIncomeTotalMinusTourismBoardTotal .= "<th class='subtotal-header'>INCOME MINUS TOURISM BOARD</th>";
foreach ($headers as $num => $val) {
    if ($num > 0) {
        $key = sprintf("%02d", $num);
        $amount = -$subtotals[$key]['amount'] + $i_total[$key];
        $htmlIncomeTotalMinusTourismBoardTotal .= "<th colspan='2' class='amount'>$amount</th>\n";
    }
}
$htmlIncomeTotalMinusTourismBoardTotal .= "</tr>";

// Staff Salary
$htmlStaffSalary .= '';
$sqlStaffSalaries = "SELECT
		staff,
		DATE_FORMAT(`date`, '%m') as yearmonth,
		SUM(TIME_TO_SEC(finish) - TIME_TO_SEC(start)) as amount
	FROM payroll
	WHERE
		" . (($site_id > 0) ? " site_id = '" . $site_id . "' AND " : "site_id > 0 AND ") . "
		`date` like '$current_year-%'
	GROUP BY yearmonth, staff;";
$staffSalaries = queryForRows($sqlStaffSalaries);
$staff = [];
foreach ($staffSalaries as $staffSalary) {
    $staff[$staffSalary['staff']][$staffSalary['yearmonth']] = $staffSalary;
}

$sqlStaffSalariesBanking = "SELECT DATE_FORMAT(`date`, '%m') as yearmonth, who, SUM(`out`) as amount
		FROM banking
		WHERE
			" . (($site_id > 0) ? " site_id = '" . $site_id . "' AND " : "site_id > 0 AND ") . "
			`date` like '$current_year-%'
			and description = 'Wages for Non directors'
	GROUP BY yearmonth, who";
$staffSalariesBanking = queryForRows($sqlStaffSalariesBanking);
$salary = [];

foreach ($staffSalariesBanking as $staffSalaryBanking) {
    $salary[$staffSalaryBanking['who']][$staffSalaryBanking['yearmonth']] = $staffSalaryBanking;
}

$subtotals = [];
foreach ($staff as $s => $data) {
    $totals = [
        'amount' => 0
    ];
    $htmlStaffSalary .= "<tr>";
    $htmlStaffSalary .= "<td class='first-col'>$s</td>";
    $data = (array_key_exists($s, $salary)) ? $salary[$s] : [];
    foreach ($headers as $num => $val) {
        if ($num > 0) {
            $key = sprintf("%02d", $num);
            $amount = array_key_exists($key, $data) ? $data[$key]['amount'] : '-';
            if ($num == 13) {
                $amount = $totals['amount'];
            } else {
                if (array_key_exists($key, $data)) {
                    $totals['amount'] += $data[$key]['amount'];
                }
            }
            $subtotals[$key]['amount'] += array_key_exists($key, $data) ? $data[$key]['amount'] : 0;
            $subtotals[$key]['amount'] += ($num == 13) ? $totals['amount'] : 0;
            $htmlStaffSalary .= "<td colspan='2' class='amount'>$amount</td>\n";
        }
    }
    $htmlStaffSalary .= "</tr>";
}

// Staff Salary Total
$htmlStaffSalaryTotal = '';
$htmlStaffSalaryTotal .= "<tr class='staff-salary-total'>";
$htmlStaffSalaryTotal .= "<th class='subtotal-header'>STAFF SALARY TOTAL</th>";
foreach ($headers as $num => $val) {
    if ($num > 0) {
        $key = sprintf("%02d", $num);
        $amount = $subtotals[$key]['amount'];
        $htmlStaffSalaryTotal .= "<th colspan='2' class='amount'>$amount</th>\n";
    }
}
$htmlStaffSalaryTotal .= "</tr>";
$staff_salary = $subtotals;
// other expenses
$htmlExpenses = '';
$sqlOtherExpenses = "SELECT DATE_FORMAT(`date`, '%m') as yearmonth, description, SUM(`out`) as amount
		FROM banking
		WHERE
			" . (($site_id > 0) ? " site_id = '" . $site_id . "' AND " : "site_id > 0 AND ") . "
			`date` like '$current_year-%'
			and `out` > 0
			and description <> 'Wages for Non directors'
	GROUP BY yearmonth, description";
$otherExpenses = queryForRows($sqlOtherExpenses);
$expenses = [];

foreach ($otherExpenses as $otherExpense) {
    $expenses[$otherExpense['description']][$otherExpense['yearmonth']] = $otherExpense;
}
// expenses table
$sqlExpensesFromExpensesTable = "SELECT DATE_FORMAT(`date`, '%m') as yearmonth, description, SUM(`cost`) as amount
		FROM expenses
		WHERE
			" . (($site_id > 0) ? " site_id = '" . $site_id . "' AND " : "site_id > 0 AND ") . "
			`date` like '$current_year-%'
			and `cost` > 0
			and description <> 'Wages for Non directors'
        GROUP BY yearmonth, description";

$expensesFromExpensesTable = queryForRows($sqlExpensesFromExpensesTable);
//$expenses = [];

foreach ($expensesFromExpensesTable as $expenseFromExpensesTable) {
    if (!array_key_exists($expenseFromExpensesTable['description'], $expenses)) {
        $expenses[$expenseFromExpensesTable['description']] = [];
    }
    if (!array_key_exists($expenseFromExpensesTable['yearmonth'], $expenses[$expenseFromExpensesTable['description']])) {
        $expenses[$expenseFromExpensesTable['description']][$expenseFromExpensesTable['yearmonth']] = $expenseFromExpensesTable;
    } else {
        $expenses[$expenseFromExpensesTable['description']][$expenseFromExpensesTable['yearmonth']]['amount'] += $expenseFromExpensesTable['amount'];
    }
}
$subtotals = [];
foreach ($expenses as $e => $data) {
    $totals = [
        'amount' => 0
    ];
    $htmlExpenses .= "<tr>";
    $htmlExpenses .= "<td class='first-col'>$e</td>";
    foreach ($headers as $num => $val) {
        if ($num > 0) {
            $key = sprintf("%02d", $num);
            $amount = array_key_exists($key, $data) ? $data[$key]['amount'] : '-';
            if ($num == 13) {
                $amount = $totals['amount'];
            } else {
                if (array_key_exists($key, $data)) {
                    $totals['amount'] += $data[$key]['amount'];
                }
            }
            $subtotals[$key]['amount'] += array_key_exists($key, $data) ? $data[$key]['amount'] : 0;
            $subtotals[$key]['amount'] += ($num == 13) ? $totals['amount'] : 0;
            $htmlExpenses .= "<td colspan='2' class='amount'>$amount</td>\n";
        }
    }
    $htmlExpenses .= "</tr>";
}

// Banking and expenses subtotal
$htmlExpenses .= "<tr class='expenses-sub-total'>";
$htmlExpenses .= "<th class='subtotal-header'>&nbsp;</th>";
foreach ($headers as $num => $val) {
    if ($num > 0) {
        $key = sprintf("%02d", $num);
        $amount = $subtotals[$key]['amount'];
        $htmlExpenses .= "<th colspan='2' class='amount'>$amount</th>\n";
    }
}
$htmlExpenses .= "</tr>";
$htmlExpenses .= "<tr class='staff-salary-total'>";
$htmlExpenses .= "<th class='subtotal-header'>&nbsp;</th>";
foreach ($headers as $num => $val) {
    if ($num == 0) continue;
    $amount = "&nbsp;";
    $htmlExpenses .= "<th colspan='2' class='amount'>$amount</th>\n";
}
$htmlExpenses .= "</tr>";
// expenses total
$htmlExpenses .= "<tr class='expenses-total'>";
$htmlExpenses .= "<th class='subtotal-header'>TOTAL EXPENSE</th>";
foreach ($headers as $num => $val) {
    if ($num > 0) {
        $key = sprintf("%02d", $num);
        $amount = $subtotals[$key]['amount'] + $staff_salary[$key]['amount'];
        $htmlExpenses .= "<th colspan='2' class='amount'>$amount</th>\n";
    }
}
$htmlExpenses .= "</tr>";
// profit or loss total
$htmlExpenses .= "<tr class='profit-total'>";
$htmlExpenses .= "<th class='subtotal-header'>Total Profit Or Loss</th>";
$key = '13';
$amount = $i_total[$key] - $subtotals[$key]['amount'] - $staff_salary[$key]['amount'];
$htmlExpenses .= "<th colspan='2' class='amount'>$amount</th>\n";
$htmlExpenses .= "<th colspan='24' class='amount'>&nbsp;</th>\n";
$htmlExpense  .= "</tr>";
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>P&L Analysis <?php echo ucfirst($site_name) . ' ' . $current_year; ?></title>
    <?php include "../includes/head_scripts.php"; ?>
    <link rel="stylesheet" type="text/css" href="css/analysis_php.css" media="all"/>
    <script>
        $(document).ready(function () {
            $("#back").click(function () {
                document.location = '/Analysis/?action=analysis';
            });
            $("#home").click(function () {
                document.location = '/';
            });
        });
    </script>
</head>
<body>
<div id="container">
    <table id="main-table">
        <tr>
            <th class="main-header" colspan="27">
                <h1>P&L Analysis <?php echo ucfirst($site_name) . ' ' . $current_year; ?></h1></th>
        </tr>
        <tr>
            <td class="divider" colspan="27">
                <a href="analysis.php?year=<?php echo $current_year - 1; ?>" class="floatleft">&lt;&lt;&lt; <?php echo $current_year - 1; ?></a>
                <?php if ($current_year < date("Y")) { ?>
                    <a href="analysis.php?year=<?php echo $current_year + 1; ?>" class="floatright"><?php echo $current_year + 1; ?> &gt;&gt;&gt; </a>
                <?php } ?>
            </td>
        </tr>
        <tr class="month-headers">
            <?= $monthHeaders ?>
        </tr>
        <tr class='column-headers'>
            <?= $columnHeaders ?>
        </tr>
        <?php
        echo $htmlOnsiteJumps;
        echo $htmlCancellationsOnsite;
        echo $htmlOnsiteSubtotals;
        echo $htmlOffsiteBungy;
        echo $htmlOffsiteCancellations;
        echo $htmlBungyTotal;
        echo $htmlMerchandise;
        echo $htmlMerchandiseTotal;
        echo $htmlIncomeTotal;
        ?>
        <tr>
            <td class="divider" colspan="27">&nbsp;</td>
        </tr>
        <tr class="expenses-month-headers">
            <?=$htmlExpensesMonthHeaders?>
        </tr>
        <tr class='column-headers'>
            <?=$htmlExpensesColumnHeaders ?>
        </tr>
        <?php
        echo $htmlTourismBoardPayments;
        echo $htmlIncomeTotalMinusTourismBoardTotal;
        echo $htmlStaffSalary;
        echo $htmlStaffSalaryTotal;
        echo $htmlExpenses;
        ?>
    </table>
    <button id="back">Back</button>
    <button id="home">Home</button>
</div>
</body>
</html>
