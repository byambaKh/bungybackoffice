<?php

/**
 * Returns an array of years that will be
 * $limitYear int The lowest year for comparison (2013 is the lowest year in the database)
 *
 * @param $year int The current year
 * @param
 * @param $comparison int the number of years to go back or -1 for none, 0 for all
 *
 * @return array an array containing the years to compare
 */
function rangeOfYearsForComparison($year, $comparison)
{
    $year = (int)$year;
    if ($comparison == '') $comparison = -1;//if comparison is not set, set it to be 'none'

    if ($comparison == -1) { //none
        $years = [$year];

    } else {//all dates
        $yearCounter = $year;//make a copy of this year

        if ($comparison == 0) $limitYear = 2013;
        else $limitYear = $year - $comparison;//if we want to look 3 year prior comparison is 3 and limit year is 2015-3

        while ($yearCounter >= $limitYear) {
            $years[] = $yearCounter;
            $yearCounter--;
        }

        //years = this year
        //and all previous years until 2010
    }

    return $years;
}

function calculateStartAndEndDates($years, $duration, $startDate, $endDate)
{

    foreach ($years as $yearIndex => $year) {
        //set the year to be different but keep the month and day the same
        //We use setISODate so we can specify date by year, week number and day of the week, this way
        //leap years are not an issue
        $startDateYear      = (int)$year;
        $startDateWeek      = (int)$startDate->format('W');
        if($startDateWeek > 52) $startDateWeek -=52;//if we are in week 53, start at week 1 of the year

        $startDateDayOfWeek = (int)$startDate->format('N');

        $startDate->setISODate($startDateYear, $startDateWeek, $startDateDayOfWeek);//this may have issues on leap days
        //$dateModified->setDate((int)$year, (int)$date->format('n'), (int)$date->format('d'));//this may have issues on leap days
        $endDateCalculated = clone $startDate;

        if($duration == "custom") {
            $difference = $startDate->diff($endDate);

            $days = $difference->format("%a");
        } else {
            $days = durationAsDays($endDate->format('Y-m-d'), $duration);
        }

        $endDateCalculated->modify("+$days days");
        $yearDateRanges[$year]['begin'] = $startDate->format("Y-m-d");
        $yearDateRanges[$year]['end'] = $endDateCalculated->format("Y-m-d");
    }

    return $yearDateRanges;
}

/**
 * @param $referenceDataSet
 * @param $comparedDataSet
 *
 * @return array
 */
function calculatePercentageChangeDataSet($referenceDataSet, $comparedDataSet)
{
    //if the old value is 10 and new value is 20
    //calculation is 20/10 *100
    $percentageDataSet = [];
    $length = count($referenceDataSet);
    for ($i = 0; $i < $length; $i++) {
        $percentage = (($referenceDataSet[$i]['y'] - $comparedDataSet[$i]['y'])/*/$referenceDataSet[$i]['y']) * 100*/);
        $percentageDataSet[$i] = [
            'x' => $referenceDataSet[$i]['x'],
            'y' => $percentage,
        ];
    }

    return $percentageDataSet;
}

//The problem...
//I want to beable to get a date range for multiple years that are comparable
//I need to beable to account for leap years...
//If I go purely by dates we end up comparing non like days so we compare Monday to Sunday etc...
//If I go by week I can compare like days for like, but with the graphs
// my current calcualtion was to take the requested date range and round calculate the beginngin of the week for the start
//and the end of the week for the beginning... this resulted in some years having shorter durations than others as rounding up and down can produce extra days

//An alternative solution is to take the initial range, calculate the number of days between them and add these on to the start date for each year....
//this gets rid of leap years concerns
//To compare like for like days...


function durationAsDays($startDate, $durationType)
{
    $startDate = DateTime::createFromFormat('Y-m-d', $startDate);
    $endDate = clone $startDate;
    $days = 0;
    //calculate the end date based on the duration
    switch ($durationType) {
        case "month":
            $endDate->modify("+1 month");
            break;

        case "week":
            $endDate->modify("+6 days");
            break;

        case "year":
        default:
            $endDate->modify("+1 year");
            break;
    }

    //calculate the difference between the two
    $delta = $startDate->diff($endDate);

    return $delta->days;
}

/**
 * Returns the start and end dates of when given a starting date and a duration.
 *
 * @param $date string the date we are starting from
 * @param $durationType string duration type [year, month, week, day]
 *
 * @return array the start and end dates as an array
 *
 */
function dateRangeForDateAndDuration($date, $durationType)
{
    $day = DateTime::createFromFormat('Y-m-d', $date);

    $start = null;
    $end = null;

    switch ($durationType) {
        case "week":
            //http://stackoverflow.com/questions/1897727/get-first-day-of-week-in-php
            $day->setISODate((int)$day->format('o'), (int)$day->format('W'), 1);
            $start = $day->format('Y-m-d');

            $day->modify("+6 days");
            $end = $day->format('Y-m-d');
            break;

        case "month":
            $day->modify("first day of this month");
            $start = $day->format('Y-m-d');

            $day->modify("last day of this month");
            $end = $day->format('Y-m-d');
            break;

        case "year":
            $day->setDate((int)$day->format('Y'), 1, 1);
            $start = $day->format('Y-m-d');

            $day->setDate((int)$day->format('Y'), 12, 31);
            $end = $day->format('Y-m-d');
            break;
    }

    return ['start' => $start, 'end' => $end];
}

/**
 * Returns the date at the beginning of the week for the input date
 *
 * @param string $date The date to be processed
 *
 * @return string the date at the beginning of the week
 */
function beginningOfWeekDate($date)
{
    $day = DateTime::createFromFormat('Y-m-d', $date);
    //http://stackoverflow.com/questions/1897727/get-first-day-of-week-in-php
    $day->setISODate((int)$day->format('o'), (int)$day->format('W'), 1);

    return $day->format('Y-m-d');
}

/**
 * Returns the date at the end of the week for the input date
 *
 * @param string $date The date to be processed
 *
 * @return string the date at the end of the week
 */
function endOfWeekDate($date)
{
    $day = DateTime::createFromFormat('Y-m-d', $date);
    //http://stackoverflow.com/questions/1897727/get-first-day-of-week-in-php
    $day->setISODate((int)$day->format('o'), (int)$day->format('W'), 7);

    return $day->format('Y-m-d');
}

function beginningOfMonthDate($date)
{
    $day = DateTime::createFromFormat('Y-m-d', $date);
    $day->modify("first day of this month");

    return $day->format('Y-m-d');
}

function endOfMonthDate($date)
{
    $day = DateTime::createFromFormat('Y-m-d', $date);
    $day->modify("last day of this month");

    return $day->format('Y-m-d');

}

function endOfLastMonthDate($date)
{
    $day = DateTime::createFromFormat('Y-m-d', $date);
    $day->modify("-1 month");
    $day->modify("last day of this month");

    return $day->format('Y-m-d');
}

function dateSequenceToXYPair($sequence)
{
    $xYPair = [];
    foreach ($sequence as $index => $x) {
        $xYPair[$index]["x"] = $x;
        $xYPair[$index]["y"] = 0;
    }

    return $xYPair;
}

/**
 * @param $sequence
 * @param $results
 *
 * @return array
 */
function mergeDateSequencePairWithResults($sequence, $results)
{
    //it would be a good idea to sort both arrays just in case
    //turn results in to a array of 'date' => 'key in results array'
    $datesInResults = [];

    foreach ($results as $key => $result) {
        $datesInResults[$result['x']] = $key;
    }

    $merged = [];
    //if the data exists in results, grab that one or else grabe it from sequence
    foreach ($sequence as $sequenceIndex => $sequenceDate) {
        $currentDate = $sequenceDate;//The date of the current
        if (array_key_exists($currentDate, $datesInResults)) {
            $merged[] = $results[$datesInResults[$sequenceDate]];
        } else {
            $merged[] = ['x' => $sequenceDate, 'y' => "0"];
        }
    }

    return $merged;
}

/**
 * Wraps SQL column in WEEK(), MONTH(), YEAR() so that data can be presented as the sum of data for days months and years.
 * The data functions will by default return data for each day but it can be useful to present the data by week. This
 * function allows the modification of the SQL query so that it becomes possible to select how the data is returned.
 *
 * --DEPRICATED--
 *
 * @param string $columnName
 * @param string $timeUnit
 *
 * @return string
 */
function timeUnitSql($columnName, $timeUnit)
{
    switch ($timeUnit) {
        case 'week':
            $returnString = "STR_TO_DATE(CONCAT(YEARWEEK($columnName, 3), ' Monday'), '%x%v %W')";
            break;
        case 'month':
            $returnString = "MONTH($columnName)";
            break;
        case 'year':
            $returnString = "YEAR($columnName)";
            break;
        case 'day':
        default:
            $returnString = "$columnName";
            break;
    }

    return $returnString;
}

/**
 * Generate an array of dates between $start and $end.
 *
 * Generates an array of dates between $start and $end incrementing using the $timeUnit specified in
 * $timeUnit.
 *
 * @param string $start The starting date
 * @param string $end The ending date
 * @param string $timeUnit the type of
 *
 * @return array An array of all of the dates generated between start and end based on timeUnit
 */
function generateDateSequenceFromRange($start, $end, $timeUnit = 'year')
{
    $sequence = [];

    //TODO if date is not a leap year add 1st of january next year...

    switch ($timeUnit) {
        case 'day':
            $increment = '+1 day';
            break;

        case 'week':
            //
            $increment = '+1 week';
            break;

        case 'month':
            $increment = '+1 month';
            break;

        case 'year':
        default:
            $increment = '+1 year';
            break;
    }

    if ($end < $start) return $sequence;

    $startDateTime = DateTime::createFromFormat("Y-m-d", $start);
    $endDateTime = DateTime::createFromFormat("Y-m-d", $end);

    while ($startDateTime->format('U') <= $endDateTime->format('U')) {
        $sequence[] = $startDateTime->format('Y-m-d');
        $startDateTime->modify($increment);
    }

    return $sequence;
}

//write the slow version for now

function groupResultsByTimeUnit($results, $timeSequence, $timeUnit = '', $summable = '')
{   //Take a sequence of dates etc, group and sum by the timeUnit
    //Sum the results that are between the timeSequence...
    //Summable is an array of keys that can be added for each

    /*
     * We have an array of dates for timeSequence
     * ie:
     *
     * [
     *     2015-01-01,
     *     2015-02-01,
     *     2015-03-01,
     *     2015-04-01,
     * ]
     *
     * Then an array of dates and values
     *
     * 1  => [date => 2015-01-01, y => foo],
     * 2  => [date => 2015-01-03, y => foo],
     * 3  => [date => 2015-01-04, y => foo],
     * 4  => [date => 2015-02-10, y => foo],
     * 5  => [date => 2015-02-11, y => foo],
     * 6  => [date => 2015-02-12, y => foo],
     * 7  => [date => 2015-03-02, y => foo],
     * 8  => [date => 2015-03-03, y => foo],
     * 9  => [date => 2015-04-23, y => foo],
     * 10 => [date => 2015-04-24, y => foo],
     * 11 => [date => 2015-05-01, y => foo],
     * 12 => [date => 2015-05-02, y => foo]
     */
    $outputStructure = [];
    foreach ($timeSequence as $timeSequenceIndex => $timeSequenceValue) {

        //TODO: when I am checking the values against the income sheet it might make more sense to
        //assign the caculated value to [timeSequenceIndex + 1 as that is the end of the month/week
        //might not apply for days though

        //if we are on the last item, quit as that is the end
        if (count($timeSequence) - 1 == $timeSequenceIndex) break;
        $lowerLimitTimestamp = strtotime($timeSequence[$timeSequenceIndex]);
        $upperLimitTimeStamp = strtotime($timeSequence[$timeSequenceIndex + 1]);

        //Output Structure
        $upperBound = $timeSequence[$timeSequenceIndex];//add a -1 to shift things right
        $outputStructure[$timeSequenceIndex] = [
            'x' => $upperBound,
            'y' => 0
        ];//

        foreach ($results as $resultsIndex => $result) {
            $resultDateTimestamp = strtotime($result['x']);
            if ($resultDateTimestamp >= $lowerLimitTimestamp && $resultDateTimestamp < $upperLimitTimeStamp) {
                //Add it to the output structure
                //sum all of the other keys
                $outputStructure[$timeSequenceIndex]['y'] += $result['y'];
                //this would speed things up but mutating an array while iterating over it is bad
                //unset($results[$resultsIndex]);
            }
        }
    }

    return $outputStructure;
}

//---------------------------------Single Output Data Functions------------------------------------------------

//get the projections shown in the roster
function getJumpProjections($site, $startDate, $endDate, $timeUnit = 'year', $options = [])
{
    $query = "
        SELECT roster_date AS x, jumps AS y
        FROM
            roster_target
        WHERE
            roster_date BETWEEN '$startDate' AND '$endDate'
            AND site_id = $site
        ORDER BY x;
    ";

    $dateSequence = generateDateSequenceFromRange($startDate, $endDate, $timeUnit);
    $results = queryForRows($query);
    $resultsGrouped = groupResultsByTimeUnit($results, $dateSequence);

    return $resultsGrouped;
}

//jumps made on the day
function getJumpNumbers($site, $startDate, $endDate, $timeUnit = 'year', $options = [])
{
    $query = "
        SELECT BookingDate AS `x`,
            sum(NoOfJump) AS `y`
        FROM `customerregs1`
        WHERE
            site_id = $site
            AND `BookingDate` BETWEEN '$startDate' AND '$endDate'
            AND NoOfJump > 0
            AND DeleteStatus = 0
            AND checked = 1
        GROUP BY `x`
        ORDER BY `x`
    ";

    $dateSequence = generateDateSequenceFromRange($startDate, $endDate, $timeUnit);
    $results = queryForRows($query);
    $resultsGrouped = groupResultsByTimeUnit($results, $dateSequence);

    return $resultsGrouped;
}

//
function getCancellations($site, $startDate = NULL, $endDate = NULL, $timeUnit = 'year', $options = [])
{
    $query = "
            SELECT
                BookingDate AS `x`,
                SUM(
                    CancelFeeQTY + IF(DeleteStatus = 1, 0, 0) #What to do if NoOfJump = 0 as it may not be a cancellation but a group booking
                ) AS `y`
                FROM `customerregs1`
                WHERE
                    site_id = $site
                    AND `BookingDate` BETWEEN '$startDate' AND '$endDate'
            GROUP BY x
    ";

    $dateSequence = generateDateSequenceFromRange($startDate, $endDate, $timeUnit);
    $results = queryForRows($query);
    $resultsGrouped = groupResultsByTimeUnit($results, $dateSequence);

    return $resultsGrouped;
}

//On the day second jumps
function getSecondJumps($site, $startDate = NULL, $endDate = NULL, $timeUnit = 'year', $options = [])
{
    $query = "
        SELECT x, SUM(y) AS y FROM (
            SELECT
                BookingDate AS `x`,
                SUM(
                    2ndj_qty
                ) AS `y`
                FROM `customerregs1`
                WHERE
                    site_id = $site
                    AND `BookingDate` BETWEEN '$startDate' AND '$endDate'
                    AND NoOfJump > 0
                    AND DeleteStatus = 0
                    AND checked = 1
            GROUP BY x

            UNION

            SELECT sales_date AS x,
                SUM(
                    sale_total_qty_2nd_jump
                ) as y
            FROM (
            SELECT
                date_format(sale_time, '%Y-%m-%d') as 'sales_date',
                merchandise_sales.*
                FROM merchandise_sales
                WHERE site_id = $site AND sale_time BETWEEN '$startDate' AND '$endDate'

                ) as photoSales
            GROUP BY x
        ) AS aValues
        GROUP BY aValues.x
        ORDER BY aValues.x;

    ";

    $dateSequence = generateDateSequenceFromRange($startDate, $endDate, $timeUnit);
    $results = queryForRows($query);
    $resultsGrouped = groupResultsByTimeUnit($results, $dateSequence);

    return $resultsGrouped;
}

//
function getWalkIns($site, $startDate = NULL, $endDate = NULL, $timeUnit = 'year', $options = [])
{
    $query = " ";

    $dateSequence = generateDateSequenceFromRange($startDate, $endDate, $timeUnit);
    $results = queryForRows($query);
    $resultsGrouped = groupResultsByTimeUnit($results, $dateSequence);

    return $resultsGrouped;
}

//People that arrive but don't jump
function getNonJumpers($site, $startDate = NULL, $endDate = NULL, $timeUnit = 'year', $options = [])
{
    $query = "
        SELECT
            COUNT(CustomerRegID) as y,
        	BookingDate AS x
        FROM non_jumpers INNER JOIN waivers ON (waiver_id = waivers.id)
        INNER JOIN `customerregs1` ON (bid = CustomerRegID)
        WHERE
            customerregs1.site_id = $site
            AND BookingDate BETWEEN '$startDate' AND '$endDate'
            AND NoOfJump > 0
            AND DeleteStatus = 0
            AND checked = 1
        GROUP BY x
        ORDER BY x;
    ";

    $dateSequence = generateDateSequenceFromRange($startDate, $endDate, $timeUnit);
    $results = queryForRows($query);
    $resultsGrouped = groupResultsByTimeUnit($results, $dateSequence);

    return $resultsGrouped;
}


//bookings made online or via agents
function getBookings($site, $startDate, $endDate, $timeUnit = 'year', $options = [])
{
    $query = "
    SELECT DATE(`BookingReceived`) AS `x`,
        COUNT('CustomerRegID') AS `y`
    FROM `customerregs1`
    WHERE
        site_id = $site
        AND `BookingReceived` BETWEEN '{$startDate} 00:00:00' AND '{$endDate} 00:00:00'
    AND NoOfJump > 0
    AND DeleteStatus = 0
    GROUP BY `x`
    ORDER BY `x`
    ";

    $dateSequence = generateDateSequenceFromRange($startDate, $endDate, $timeUnit);
    $results = queryForRows($query);
    $resultsGrouped = groupResultsByTimeUnit($results, $dateSequence);

    return $resultsGrouped;
}

//Number of jumps booked online or by agents on the day
function getJumpsBooked($site, $startDate, $endDate, $timeUnit = 'year', $options = [])
{
    $query = "
        SELECT DATE(`BookingReceived`) AS `x`,
            sum(NoOfJump) AS `y`
        FROM `customerregs1`
        WHERE
            site_id = $site
            AND `BookingReceived` BETWEEN '{$startDate} 00:00:00' AND '{$endDate} 00:00:00'
            AND NoOfJump > 0
            AND DeleteStatus = 0
        GROUP BY `x`
        ORDER BY `x`
    ";

    $dateSequence = generateDateSequenceFromRange($startDate, $endDate, $timeUnit);
    $results = queryForRows($query);
    $resultsGrouped = groupResultsByTimeUnit($results, $dateSequence);

    return $resultsGrouped;
}

//--------------------------------Multiple Output Data Functions------------------------------------------------

//DATA functions, these return the X and Y values as a pairs of 'value' and 'dates'
/**
 * @param        $site
 * @param        $startDate
 * @param        $endDate
 * @param string $timeUnit
 * @param        $options
 *
 * @return array
 */

function getAIncome($site, $startDate, $endDate, $timeUnit = '', $options = []){
    $query = "
        SELECT x, SUM(y) AS y FROM (
            #Income Sheet A section
            /*
            The income sheet does not make a distinction between collectpay = onsite/offsite for photos
            so this calculates the sum for photos that are either condition.
            */
            SELECT
                BookingDate AS `x`,
                sum(
                    500 * photos_qty  /*photos purchased with jumps*/
                ) AS `y`
                FROM `customerregs1`
                WHERE
                    site_id = $site
                    AND `BookingDate` BETWEEN '$startDate' AND '$endDate'
                    AND NoOfJump > 0
                    AND DeleteStatus = 0
                    AND checked = 1
            GROUP BY x

            UNION

            SELECT
                BookingDate AS `x`,
                sum(
                NoOfJump * Rate + /*First Jumps total money*/
                    IF(CancelFeeQTY, CancelFee * CancelFeeQTY, 0) /*Cancellations*/
                ) AS `y`
            FROM `customerregs1`
            WHERE
                site_id = $site
                AND `BookingDate` BETWEEN '$startDate' AND '$endDate'
                AND CollectPay = 'Onsite'
                AND NoOfJump > 0
                AND DeleteStatus = 0
                AND checked = 1
            GROUP BY x

            UNION
            /*Money from Merchandise and Second Jumps*/
            SELECT sales_date AS x,
                sum(
                    sale_total_qty_photo * 500
                    + sale_total_2nd_jump * sale_total_qty_2nd_jump
                ) as y
            FROM (
            SELECT
                date_format(sale_time, '%Y-%m-%d') as 'sales_date',
                merchandise_sales.*
                FROM merchandise_sales
                WHERE site_id = $site AND sale_time BETWEEN '$startDate' AND '$endDate'

                ) as photoSales
            GROUP BY x

        ) AS aValues
        GROUP BY aValues.x
        ORDER BY aValues.x;
    ";


    $dateSequence = generateDateSequenceFromRange($startDate, $endDate, $timeUnit);
    $results = queryForRows($query);

    $resultsGrouped = groupResultsByTimeUnit($results, $dateSequence);

    return $resultsGrouped;
}

function getBIncome($site, $startDate, $endDate, $timeUnit = '', $options = []){
    $query = "
        SELECT x, SUM(y) AS y FROM (
            #Income Sheet B Section
            #Goods
            SELECT x,
                sum(
                    sale_total
                    - sale_total_2nd_jump
                    - sale_total_photo
                ) as y #Remove 2nd jumps and total photos as they were calculated in A
            FROM (
                SELECT
                    date_format(sale_time, '%Y-%m-%d') as 'x',
                    merchandise_sales.*
                    FROM merchandise_sales
                    WHERE site_id = $site AND sale_time BETWEEN '$startDate' AND '$endDate'
                    ORDER BY 'sales_date'
            ) as t1
            GROUP BY x
        ) AS aValues
        GROUP BY aValues.x
        ORDER BY aValues.x;

    ";

    $dateSequence = generateDateSequenceFromRange($startDate, $endDate, $timeUnit);
    $results = queryForRows($query);
    $resultsGrouped = groupResultsByTimeUnit($results, $dateSequence);

    return $resultsGrouped;
}

function getCIncome($site, $startDate, $endDate, $timeUnit = '', $options = []){
    $query = "
            SELECT
            BookingDate AS `x`,
            sum(
                NoOfJump * Rate + /*First Jumps total money*/
                IF(CancelFeeQTY, CancelFee * CancelFeeQTY, 0) /*Cancellations*/
            ) AS `y`
            FROM `customerregs1`
            WHERE
                site_id = $site
                AND `BookingDate` BETWEEN '$startDate' AND '$endDate'
                AND CollectPay = 'Offsite'
                AND NoOfJump > 0
                AND DeleteStatus = 0
                AND checked = 1
            GROUP BY x
            ORDER BY x;
        ";

    $dateSequence = generateDateSequenceFromRange($startDate, $endDate, $timeUnit);
    $results = queryForRows($query);
    $resultsGrouped = groupResultsByTimeUnit($results, $dateSequence);

    return $resultsGrouped;
}

function getIncome($site, $startDate, $endDate, $timeUnit = '', $options = [])
{
    $query = "
        SELECT x, SUM(y) AS y FROM (
            #Income Sheet A section
            /*
            The income sheet does not make a distinction between collectpay = onsite/offsite for photos
            so this calculates the sum for photos that are either condition.
            */
            SELECT
                BookingDate AS `x`,
                sum(
                    500 * photos_qty  /*photos purchased with jumps*/
                ) AS `y`
                FROM `customerregs1`
                WHERE
                    site_id = $site
                    AND `BookingDate` BETWEEN '$startDate' AND '$endDate'
                    AND NoOfJump > 0
                    AND DeleteStatus = 0
                    AND checked = 1
            GROUP BY x

            UNION

            SELECT
                BookingDate AS `x`,
                sum(
                NoOfJump * Rate + /*First Jumps total money*/
                    IF(CancelFeeQTY, CancelFee * CancelFeeQTY, 0) /*Cancellations*/
                ) AS `y`
            FROM `customerregs1`
            WHERE
                site_id = $site
                AND `BookingDate` BETWEEN '$startDate' AND '$endDate'
                AND CollectPay = 'Onsite'
                AND NoOfJump > 0
                AND DeleteStatus = 0
                AND checked = 1
            GROUP BY x

            UNION
            /*Money from Merchandise and Second Jumps*/
            SELECT sales_date AS x,
                sum(
                    sale_total_qty_photo * 500
                    + sale_total_2nd_jump * sale_total_qty_2nd_jump
                ) as y
            FROM (
            SELECT
                date_format(sale_time, '%Y-%m-%d') as 'sales_date',
                merchandise_sales.*
                FROM merchandise_sales
                WHERE site_id = $site AND sale_time BETWEEN '$startDate' AND '$endDate'

                ) as photoSales
            GROUP BY x

            UNION

            #Income Sheet B Section
            #Goods
            SELECT x,
                sum(
                    sale_total
                    - sale_total_2nd_jump
                    - sale_total_photo
                ) as y #Remove 2nd jumps and total photos as they were calculated in A
            FROM (
                SELECT
                    date_format(sale_time, '%Y-%m-%d') as 'x',
                    merchandise_sales.*
                    FROM merchandise_sales
                    WHERE site_id = $site AND sale_time BETWEEN '$startDate' AND '$endDate'
                    ORDER BY 'sales_date'
            ) as t1
            GROUP BY x

            UNION

            #Income Sheet C Section
            SELECT
            BookingDate AS `x`,
            sum(
                NoOfJump * Rate + /*First Jumps total money*/
                IF(CancelFeeQTY, CancelFee * CancelFeeQTY, 0) /*Cancellations*/
            ) AS `y`
            FROM `customerregs1`
            WHERE
            site_id = $site
                AND `BookingDate` BETWEEN '$startDate' AND '$endDate'
                AND CollectPay = 'Offsite'
                AND NoOfJump > 0
                AND DeleteStatus = 0
                AND checked = 1
            GROUP BY x
        ) AS aValues
        GROUP BY aValues.x
        ORDER BY aValues.x;

    ";

    $dateSequence = generateDateSequenceFromRange($startDate, $endDate, $timeUnit);
    $results = queryForRows($query);
    $resultsGrouped = groupResultsByTimeUnit($results, $dateSequence);

    //this is unnecessary as we have already done so
    //$resultsMerged = mergeDateSequencePairWithResults($dateSequence, $resultsGrouped);
    //TODO merge this array in to the results from query so that we do not miss empty days in our data

    return $resultsGrouped;
}

function getTotalRevenue($site, $startDate, $endDate, $timeUnit = 'year', $options = [])
{
}

//Returns the number of men and women on a day
function getSex($site, $startDate, $endDate, $timeUnit = 'year', $options = [])
{
    $query = "
        SELECT BookingDate AS `x`,
            SUM(IF(sex = 'male',1 ,0)) AS male,
            SUM(IF(sex = 'female',1 ,0)) AS female,
            SUM(IF(sex LIKE '%', 1, 0)) as total
        FROM `customerregs1` INNER JOIN waivers ON (bid = CustomerRegID)
        WHERE
            customerregs1.site_id = $site
            AND BookingDate BETWEEN '$startDate' AND '$endDate'
            AND NoOfJump > 0
            AND DeleteStatus = 0
        GROUP BY `x`
        ORDER BY `x`;
    ";

    $dateSequence = generateDateSequenceFromRange($startDate, $endDate, $timeUnit);
    $results = queryForRows($query);

    $resultsGrouped = groupResultsByTimeUnit($results, $dateSequence);

    return $resultsGrouped;
}




#Special Charts
/*
function specialChartAverageGroupSize($site_id, $startDate = NULL, $endDate = NULL, $options = NULL)
{
}

function specialChartSex($site_id, $startDate = NULL, $endDate = NULL, $options = NULL)
{
}

function specialChartAge($site_id, $startDate = NULL, $endDate = NULL, $options = NULL)
{
}

function specialChartPrefecture($site_id, $startDate = NULL, $endDate = NULL, $options = NULL)
{
}

function specialChartRainyDays($site_id, $startDate = NULL, $endDate = NULL, $options = NULL)
{
}

function specialChartTrafficProblems($site_id, $startDate = NULL, $endDate = NULL, $options = NULL)
{
}

function specialChartTotalIncome($site_id, $startDate = NULL, $endDate = NULL, $options = NULL)
{
}
*/

//print_r(generateDateSequenceFromRange('2015-01-01', '2018-12-31', 'year'));

//echo dateToWeekEnd("2015-12-31");
//durationAsDays('2015-08-15', 'year');