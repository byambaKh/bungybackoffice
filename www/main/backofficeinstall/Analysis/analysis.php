<?php
include '../includes/application_top.php';

$currentYear = date("Y");
$total = isset($_GET['total']) ? $_GET['total'] : null;

if (isset($_GET['year'])) {
    $currentYear = (int)$_GET['year'];
}

$siteName = ucfirst($_GET['subdomain']);

$siteId = siteNameToId($_GET['subdomain']);

if($siteId === null) exit("Undefined Bungy Site");

if($total)
    $siteName = "All Sites";
$profitAndLoss = new ProfitAndLoss($siteId, $currentYear, $total);

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>P&L Analysis <?php echo ucfirst($site_name) . ' ' . $currentYear; ?></title>
    <?php include "../includes/head_scripts.php"; ?>
    <script src="js/foldtable.js" type="text/javascript" charset="utf-8"></script>
    <link rel="stylesheet" type="text/css" href="css/analysis_php.css?v=1" media="all"/>
    <script>
        $(document).ready(function () {
            $("#back").click(function () {
                document.location = '/Analysis/?action=analysis';
            });
            $("#home").click(function () {
                document.location = '/';
            });
        });
    </script>
</head>
<body>
<div id="container">
    <div class="main-header">
        <h1>P&L Analysis <?="$siteName - $currentYear"?></h1>
        <h4><a href='analysis_advanced.php'>Advanced &gt;&gt;</a></h4>
    </div>
    <?=$profitAndLoss->draw();?>
    <button id="back">Back</button>
    <button id="home">Home</button>
</div>
</body>
</html>
