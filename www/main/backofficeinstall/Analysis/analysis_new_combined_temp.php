<?php
include '../includes/application_top.php';
$current_year = date("Y");
$total = isset($_GET['total']) ? $_GET['total'] : null;

if (isset($_GET['year'])) {
    $current_year = (int)$_GET['year'];
}
if($total == -1)
    $site_name = "All Sites";

$profitAndLoss = new profitAndLossCollectionTemp([0, 1, 2, 3, 4, 10], $current_year, $total);

?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <title>P&L Analysis <?php echo ucfirst($site_name) . ' ' . $current_year; ?></title>
    <?php include "../includes/head_scripts.php"; ?>
    <!--<link rel="stylesheet" type="text/css" href="css/analysis_php.css" media="all"/>-->
    <link rel="stylesheet" type="text/css" href="css/analysis_new_combined.css"/>
    <script src="js/foldtable.js"></script>
    <script>
        //script to bind hiding and showing to classes
        $(document).ready(function () {
            $("#back").click(function () {
                document.location = '/Analysis/?action=analysis';
            });
            $("#home").click(function () {
                document.location = '/';
            });
        });
    </script>
</head>
<body>
<div id="container">
    <?=$profitAndLoss->draw();?>
    <button id="back">Back</button>
    <button id="home">Home</button>
</div>
</body>
</html>
