<?
require '../includes/application_top.php';
require_once('defineStrings.php');
//require_once("../includes/pageParts/standardHeader.php");
require_once('functions.php');
error_reporting(E_ALL & ~E_DEPRECATED & ~E_NOTICE);
ini_set('display_errors', 'On');

echo Html::head("Head Office - $definedTitle", array("main_menu.css", "companyCharter.css"));
?>
<div>
	<img src="../img/index.jpg">
	<h1>Head Office <br> <?= $definedTitle ?></h1>

	<?
    //hide download buttons if no existing uploads
	if(checkForExistingUploads($definedCategory)) {
		echo Html::menuPageLink("Download Latest", "downloadFile.php?latest=true");
		echo Html::menuPageLink("Older Versions", "oldVersions.php");
		echo Html::menuPageLink("-", "#");
	}

	echo Form::menuPageUpload("Upload", "uploadFile.php");
	echo Html::menuPageLink("Log Out", "/?logout=true");
	echo Html::menuPageLink("Back", "http://main.".SYSTEM_DOMAIN."/menu.php") ;
	?>
</div>
<? require_once("../includes/pageParts/standardFooter.php");?>
