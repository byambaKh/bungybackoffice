<?
require_once('../includes/application_top.php');
require_once('defineStrings.php');
?>
<html>
<head>
<link rel="stylesheet" type="text/css" href="stylesheet.css">
</head>
<body>
<?
$temp = explode(".", $_FILES["file"]["name"]);
$originalName = $_FILES["file"]["name"];
$extension = end($temp);

$uploadDirectory = $_SERVER['DOCUMENT_ROOT']."/uploads/$definedDirectory/";

if ($extension == "xls") {
	if ($_FILES["file"]["error"] > 0) {
		//echo "Return Code: " . $_FILES["file"]["error"] . "<br>";
	} else {
        $storageName = $user->getID()."_".date('YmdHis').".xls";//A unique name for storing the file
		move_uploaded_file($_FILES["file"]["tmp_name"], $uploadDirectory . $storageName);
        $user = new BJUser();

        $data = array(
            'originalName' => $originalName,
            'storageName' => $storageName,
            'filename' => "$definedFileName",
            'userId' => $user->getID(),
            'dateTime' => date('Y-m-d H:i:s'),
            'category' => "$definedCategory"
        );

        $mail = new PHPMailer;
        $mail->From = 'noreply@bungyjapan.com';
        $mail->FromName = "$definedTitle";

        $recipients = [
            "beau@bungyjapan.com",
            "dez@bungyjapan.com",
            "developer@standardmove.com",
        ];

        foreach ($recipients as $recipient) {
            $mail->addAddress($recipient); // Site Manager
        }

        $mail->addReplyTo('noreply@bungyjapan.com', "$definedTitle Update");
        $mail->isHTML(true); // Set email format to HTML
        $mail->CharSet = 'UTF-8';

        $mail->Subject = "Updated $definedTitle";
        $mail->Body = "The $definedTitle has been updated. Please go to
        <a href='http://main.bungyjapan.com/$definedUrlDirectory/index.php'>$definedTitle Page</a> and Download The Latest Version.";

        $mail->send();

        db_perform('uploads', $data);

		echo "$definedTitle Successfully Uploaded.<br> ";
		echo "<a href = 'index.php'>&lt;&lt; Back</a> ";
	}

} else {
	echo "Invalid file";
}
?> 
</body>
</html>

