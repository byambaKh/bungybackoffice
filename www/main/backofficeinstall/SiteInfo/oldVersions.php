<?
require_once('../includes/application_top.php');
//require_once("../includes/pageParts/standardHeader.php");
require('defineStrings.php');
echo Html::head("Head Office - $definedTitle Old Versions", array("main_menu.css", "companyCharter.css"));

//find all of the existing files
$filesSql = " SELECT up.id, filename, url, `dateTime`, storageName, UserName FROM uploads AS up LEFT JOIN users AS us ON up.userId = us.UserID WHERE category = '$definedCategory' ORDER BY up.id DESC;
;";

$files = queryForRows($filesSql);
?>
<div>
    <img src="../img/index.jpg">
    <h1>Head Office <br> <?= $definedTitle ?> - Old Versions</h1>
    <table class='uploadTable' style="width:417px; margin-left: auto; margin-right: auto; margin-bottom: 10px;">
        <tr>
            <th>File Name</th>
            <th>Date Time</th>
            <th>Uploader</th>
        </tr>
        <?
        foreach($files as $file){
        echo "<tr>
                <td><a href='downloadFile.php?fileId={$file['id']}'>{$file['filename']}</a></td>
                <td>{$file['dateTime']}</td>
                <td>{$file['UserName']}</td>
            </tr> ";
            echo '';
        }
    ?>
    </table>
    <? echo Html::menuPageLink("Back", "http://main.".SYSTEM_DOMAIN."/$definedDirectory/") ?>
</div>
<? require_once("../includes/pageParts/standardFooter.php");?>

